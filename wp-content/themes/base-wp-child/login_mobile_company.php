<?php
// Template Name: Login mobile company
get_header(); ?>
<?php
$redirect_url = (isset($_GET['redirect_url']) ? $_GET['redirect_url'] : get_site_url());
?>

<main class="col-xs-12 col-xs-12 listing-page-border transport-page">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="main-content col-lg-12 col-xs-12">
                    <div class="row">
                        <div class="login-register-page">
                            <h4 class="title-page">Đăng nhập tài khoản đại lý</h4>
                            <form role="form" action="" class="form-login" method="post" id="form-login-ajax-mobile">
                                <div class="form-group">
                                    <input type="email"  name="log" class="form-control"  required id="InputEmail1"placeholder="Số điện thoại hoặc địa chỉ email *">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="pwd" required class="form-control" id="InputPassword1" placeholder="Mật khẩu Trekvn.com *">
                                    <input type="hidden" name="redirect_to" class="redirect_to" value="<?php echo $redirect_url;?>" />
                                    <input type="hidden" name="action" value="ajax_login" />
                                    <input type="hidden" name="type" value="company" />
                                    <?php wp_nonce_field( 'ajax-login-nonce', 'security_login' ); ?>
                                </div>
                                <div class="forgot-password">
                                    <a href="#" title="#">Quên mật khẩu?</a>
                                </div>
                                <div class="button-login">
                                    <button type="submit" class="btn btn-default">Đăng nhập</button>
                                </div>
                                <div class="register-link">
                                    Bạn chưa có tài khoản? Hãy <a href="<?php echo get_site_url().'/dang-ky-mobile?redirect_url='.$redirect_url;?>" title="#">Đăng ký</a>
                                </div>
                                <div class="term-agree">
                                    <p>Qua việc Đăng nhập tài khoản, bạn đồng ý với các <a href="#">Điều khoản và Điều kiện</a>
                                        cũng như <a href="#">Chính sách an toàn và bảo mật</a> của chúng tôi</p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php
get_footer(); ?>
