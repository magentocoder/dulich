<?php
// Template Name: Transport
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}

get_header('shop');

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
//do_action( 'woocommerce_before_main_content' );
global $wpdb;
$city = $wpdb->get_results('SELECT DISTINCT meta_value FROM wp_postmeta WHERE meta_key LIKE "city" AND meta_value IS NOT NULL', ARRAY_A);
?>
<?php

if (is_singular('product')): ?>
    <main class="col-xs-12 col-xs-12 listing-page-border">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="main-content col-lg-12 col-xs-12">
                        <div class="row">
                            <aside class="col-lg-3 col-xs-12 ">
                                <div class="row">
                                    <div class="menu-left hidden-sm hidden-xs">
                                        <div class="sidebar customer-care">
                                            <div class="customer-care-call-text">
                                                Gọi điện để tư vấn
                                            </div>
                                            <div class="customer-number-phone">
                                                <span class="icon-telephone"><i class="fas fa-phone"></i></span>
                                                090 123 456
                                            </div>
                                            <div class="customer-care-call-text">
                                                Hoặc để lại số điện thoại Trekvn sẽ gọi cho bạn
                                            </div>
                                            <input type="text" value="" placeholder="Số điện thoại của bạn...">
                                        </div>
                                        <div class="register-information">
                                            <div class="register-info-title">Đăng kí nhận thông tin</div>
                                            <div class="register-info-text">Hãy là người đầu tiên nhận được những tin
                                                tức thú vị và những chương trình khuyến mãi nóng hổi nhất
                                            </div>
                                            <div class="form-register-info">
                                                <?php $base_url = get_site_url(); ?>
                                                <form role="form" method="post" action="<?php echo $base_url ?>/?na=s"
                                                      id="ajax-newsletter-left">
                                                    <div class="form-group">
                                                        <label for="name">Họ và tên</label>
                                                        <input type="text" class="form-control" id="name"
                                                               placeholder="Name" name="nn" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input type="email" class="form-control" id="email"
                                                               placeholder="Email" name="ne" required>
                                                    </div>
                                                    <button type="submit"
                                                            class="btn btn-default register ajax-newsletter-left">Đăng
                                                        kí
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <?php
                            while (have_posts()) :
                                the_post();
                                wc_get_template_part('content', 'single-product');
                            endwhile;

                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php else: ?>
    <main class="col-xs-12 col-xs-12 listing-page-border">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="main-content col-lg-12 col-xs-12">
                        <div class="row">

                            <aside class="col-lg-3 col-xs-12 ">
                                <div class="row">
                                    <div class="menu-left hidden-sm hidden-xs">
                                        <div class="sidebar">
                                            <div class="mapp-wappar">
                                                <div class="map-place">
                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703"
                                                            width="100%" height="150px" frameborder="0" style="border:0"
                                                            allowfullscreen></iframe>
                                                </div>
                                            </div>
                                            <div class="map-sticker">
                                                <span>Hiển thị bản đồ</span>
                                            </div>
                                        </div>
                                        <div class="filter-border-sub">
                                            <div class="filter-title">Chọn lọc theo</div>
                                            <br/>
                                            <select id="city" class="form-control">
                                                <option>Thành Phố</option>
                                                <?php
                                                foreach ($city as $item) {
                                                    ?>
                                                    <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                        <option><?= $item['meta_value']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <br/>
                                            <select id="district" class="form-control">
                                                <option>Quận - Huyện</option>
                                            </select>
                                            <br/>
                                            <select id="commune" class="form-control">
                                                <option>Phường - Xã</option>
                                            </select>
                                            <br/>
                                            <select id="price" class="form-control">
                                                <option>Giá tiền</option>
                                                <option>0 đ - 1.000.000 đ</option>
                                                <option>1.000.000 đ - 2.000.000 đ</option>
                                                <option>2.000.000 đ - 3.000.000 đ</option>
                                                <option>3.000.000 đ - 4.000.000 đ</option>
                                                <option>4.000.000 đ trở lên</option>
                                            </select>
                                            <br/>
                                            <select id="rate" class="form-control">
                                                <option value="0">Đánh giá của khách sạn</option>
                                                <option>Tuyệt hảo</option>
                                                <option>Rất tốt</option>
                                                <option>Tốt</option>
                                                <option>Dẽ chịu</option>
                                            </select>
                                            <br/>
                                            <select id="rank" class="form-control">
                                                <option>Hạng của khách sạn</option>
                                                <option>1 sao</option>
                                                <option>2 sao</option>
                                                <option>3 sao</option>
                                                <option>4 sao</option>
                                                <option>5 sao</option>
                                            </select>
                                            <br/>
                                            <button type="button" class="btn btn-default" id="search-list">
                                                <span class="glyphicon glyphicon-search"></span> Tìm
                                            </button>
                                            <br/><br/>
                                            <span>Tìm theo tên</span><br/>
                                            <input type="text" class="form-control" id="ten-khach-san"
                                                   placeholder="Tìm theo tên ..."/>
                                            <!--                                                <div class="filter-content"></div>-->
                                        </div>
                                    </div>
                                    <div class="menu-left-mobile hidden-lg hidden-md">
                                        <div class="col-sm-3">
                                            <div class="row">
                                                <div class="button-filter">
                                                    Bộ lọc
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="all-province-city">
                                                <select class="form-control">
                                                    <option>Đà Lạt</option>
                                                    <option>Hà Nội</option>
                                                    <option>Sa Pa</option>
                                                    <option>Đà Nẵng</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <section class="col-lg-9 col-xs-12">
                                <div class="row">
                                    <div class="listing-page" id="old-listing-page">
                                        <div class="toolbar hidden-sm hidden-xs">
                                            <?php woocommerce_result_count(); ?>
                                            <?php woocommerce_catalog_ordering(); ?>

                                        </div>
                                        <?php

                                        if (have_posts()) {

                                            /**
                                             * Hook: woocommerce_before_shop_loop.
                                             *
                                             * @hooked wc_print_notices - 10
                                             * @hooked woocommerce_result_count - 20
                                             * @hooked woocommerce_catalog_ordering - 30
                                             */
//                                            do_action( 'woocommerce_before_shop_loop' );

                                            woocommerce_product_loop_start();

                                            if (wc_get_loop_prop('total')) {
                                                while (have_posts()) {
                                                    the_post();

                                                    /**
                                                     * Hook: woocommerce_shop_loop.
                                                     *
                                                     * @hooked WC_Structured_Data::generate_product_data() - 10
                                                     */
                                                    do_action('woocommerce_shop_loop');

                                                    wc_get_template_part('content', 'product');
                                                }
                                            }

                                            woocommerce_product_loop_end();

                                            /**
                                             * Hook: woocommerce_after_shop_loop.
                                             *
                                             * @hooked woocommerce_pagination - 10
                                             */
                                            do_action('woocommerce_after_shop_loop');
                                        } else {
                                            /**
                                             * Hook: woocommerce_no_products_found.
                                             *
                                             * @hooked wc_no_products_found - 10
                                             */
                                            do_action('woocommerce_no_products_found');
                                        }
                                        ?>
                                        <div class="pagination-border">
                                            <ul class="pagination">
                                                <li><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                                                </li>
                                                <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a>
                                                </li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li><a href="#">4</a></li>
                                                <li><a href="#">5</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="note-bottom"><?php _e('[:en]The room price show on Trakevn calculate by each night for all room choosen[:vi]Giá phòng hiển thị trên Trekvn tính theo từng đêm cho
                                            tổng số phòng đã chọn[:]') ?>
                                        </div>
                                    </div>
                                    <div class="listing-page woocommerce" id="search-listing-page" style="display:none;">
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php endif; ?>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $("#search-list").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#ten-khach-san").val();
                var price = $("#price").val();
                var rate = $("#rate").val();
                var rank = $("#rank").val();
                var url = '<?php echo get_site_url(); ?>/search-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                            'rate': rate,
                            'rank': rank,
                            'ten': ten
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
        });
    </script>
<?php
/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
//do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action('woocommerce_sidebar');

get_footer('shop');
