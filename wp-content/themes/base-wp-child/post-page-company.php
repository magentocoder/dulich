<?php
// Template Name: Post page company
get_header();
?>
    <main class="col-xs-12 col-xs-12 listing-page-border dangbai-daily">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="main-content col-lg-12 col-xs-12">
                        <div class="row">

                            <aside class="col-lg-3 col-xs-12 ">
                                <div class="row">
                                    <div class="menu-left hidden-sm hidden-xs">
                                        <div class="filter-border tab-information-manager">
                                            <div class="filter-border-sub">
                                                <div class="avatar">
                                                    <div class="image-avatar">
                                                        <img src="image/man-human-person.png" title="" alt="">
                                                    </div>
                                                    <div class="fullname">Lê Văn AAA</div>
                                                    <a href="#" title="" class="logout-dangbai">Đăng xuất</a>
                                                </div>
                                                <div class="filter-content">
                                                    <ul>
                                                        <li>
                                                            <a href="#" title="#" class="">
                                                                <span class="icon-manager"><i class="fas fa-address-book"></i></span>
                                                                Quản lý tài khoản
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" title="#" class="">
                                                                <span class="icon-manager"></span>
                                                                Quản lý bài viết
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" title="#" class="">
                                                                <span class="icon-manager"></span>
                                                                Bài viết đã đăng
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" title="#" class="">
                                                                <span class="icon-manager"></span>
                                                                Bài viết chờ duyệt
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" title="#" class="">
                                                                <span class="icon-manager"></span>
                                                                Bài viết đã lưu
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" title="#" class="">
                                                                <span class="icon-manager"><i class="fas fa-bell"></i></span>
                                                                Thông báo
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" title="#" class="">
                                                                <span class="icon-manager"><i class="fas fa-trash-alt"></i></span>
                                                                Bài viết đã xóa
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <section class="col-lg-9 col-xs-12">
                                <div class="row">
                                    <div class="listing-page">
                                        <div class="border-list-item-tourist form-book-tour-wapper">
                                            <article class="list-tourist col-lg-12 col-xs-12">
                                                <h4 class="title-form-book-tour">Quý vị muốn đăng gì?</h4>
                                                <form role="form" action="">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">
                                                            <div class="form-book-tour-right">
                                                                <div class="form-group">
                                                                    <select class="form-control">
                                                                        <option value="">Khách sạn</option>
                                                                        <option value="">Khách sạn</option>
                                                                        <option value="">Khách sạn</option>
                                                                        <option value="">Khách sạn</option>
                                                                        <option value="">Khách sạn</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="tieude" placeholder="Tiêu đề">
                                                                </div>
                                                                <div class="form-group">
                                                                    <textarea type="text" class="form-control" rows="10" name="content" placeholder="Nội dung..."></textarea>
                                                                </div>
                                                                <div class="upload-image">
                                                                    <div class="image-upload">
                                                                        <img src="image/default-product-image.png" title="" alt="">
                                                                    </div>
                                                                    <input type="file" alt="" name="upload" title="" class="button-upload">
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="form-group columns">
                                                                            <input type="text" class="form-control" name="address" placeholder="Địa chỉ">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="form-group columns">
                                                                            <input type="email" class="form-control" name="email" placeholder="Email">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="form-group columns">
                                                                            <input type="text" class="form-control" name="telephone" placeholder="Số điện thoại">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="form-group columns">
                                                                            <input type="text" class="form-control" name="price" placeholder="Giá tiền">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="form-group columns">
                                                                            <input type="text" class="form-control" name="price" placeholder="Giá trước khi giảm">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="form-group columns">
                                                                            <input type="text" class="form-control" name="price" placeholder="Giá sau khi giảm">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="button-form-tour">
                                                                    <a href="#" title="" class="btn btn-default back-button">Quay lại</a>
                                                                    <button type="submit" class="btn btn-default submit-button">Submit</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
get_footer();
