<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}

get_header('shop');

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
//do_action( 'woocommerce_before_main_content' );
global $wpdb;
$city = $wpdb->get_results('SELECT DISTINCT meta_value FROM wp_postmeta WHERE meta_key LIKE "city" AND meta_value IS NOT NULL', ARRAY_A);
$cat_obj = get_queried_object();
$template = (int)get_field('template', $cat_obj);
$filter_template = (int)get_field('template_filter', $cat_obj);
$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$is_giaitri = strpos($actual_link,"giai-tri");
$section_class = 'col-lg-9 col-xs-12';
$section_row_div_class = 'listing-page';
$section_row_div_id = 'old-listing-page';
$main_class = '';
if ($template == 2) {
    $section_class = 'col-lg-9  col-xs-12 col-md-9 col-sm-12';
    $section_row_div_class = 'entertainment-page-content news-page col-lg-12 col-xs-12';
    $section_row_div_id = '';
} elseif ($template == 1) {
    $main_class = 'transport-page';
}

?>
<?php

if (is_singular('product')): ?>

    <main class="col-xs-12 col-xs-12 listing-page-border book-tour-detail">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="main-content col-lg-12 col-xs-12">
                        <div class="row">
                            <aside class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                                <div class="row">
                                    <div class="menu-left hidden-sm hidden-xs">
                                        <div class="sidebar">
                                            <div class="mapp-wappar">
                                                <div class="map-place">
                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703"
                                                            width="100%" height="150px" frameborder="0"
                                                            style="border:0"
                                                            allowfullscreen></iframe>
                                                </div>
                                            </div>
                                            <div class="map-sticker">
                                                <span><?php _e('[:en]Show map[:vi]Hiển thị bản đồ[:]')?></span>
                                            </div>
                                        </div>
                                        <div class="filter-border hidden-sm hidden-xs">
                                            <div class="filter-title"><?php _e('[:en]Filter by[:vi]Chọn lọc theo[:]')?></div>
                                            <br/>
                                            <select id="city" class="form-control">
                                                <option>Thành Phố</option>
                                                <?php
                                                foreach ($city as $item) {
                                                    ?>
                                                    <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                        <option><?= $item['meta_value']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <br/>
                                            <select id="district" class="form-control">
                                                <option>Quận - Huyện</option>
                                            </select>
                                            <br/>
                                            <select id="commune" class="form-control">
                                                <option>Phường - Xã</option>
                                            </select>
                                            <br/>
                                            <select id="price" class="form-control">
                                                <option>Giá tiền</option>
                                                <option>0 đ - 1.000.000 đ</option>
                                                <option>1.000.000 đ - 2.000.000 đ</option>
                                                <option>2.000.000 đ - 3.000.000 đ</option>
                                                <option>3.000.000 đ - 4.000.000 đ</option>
                                                <option>4.000.000 đ trở lên</option>
                                            </select>
                                            <br/>
                                            <select id="rate" class="form-control">
                                                <option value="0">Đánh giá</option>
                                                <option>Tuyệt hảo</option>
                                                <option>Rất tốt</option>
                                                <option>Tốt</option>
                                                <option>Dẽ chịu</option>
                                            </select>
                                            <br/>
                                            <select id="rank" class="form-control">
                                                <option>Hạng</option>
                                                <option>1 sao</option>
                                                <option>2 sao</option>
                                                <option>3 sao</option>
                                                <option>4 sao</option>
                                                <option>5 sao</option>
                                            </select>
                                            <br/>
                                            <button type="button" class="btn btn-default" id="search-list-tour">
                                                <span class="glyphicon glyphicon-search"></span> <?php _e('[:en]Search[:vi]Tìm[:]')?>
                                            </button>
                                            <br/><br/>
                                            <span><?php _e('[:en]Search by name[:vi]Tìm theo tên[:]')?></span><br/>
                                            <input type="text" class="form-control" id="ten-tour"
                                                   placeholder="<?php _e('[:en]Search by name ...[:vi]Tìm theo tên ...[:]')?>"/>
                                            <!--                                                <div class="filter-content"></div>-->
                                        </div>
                                    </div>
                                    <div class="menu-left-mobile hidden-lg hidden-md">
                                        <div class="col-sm-3">
                                            <div class="row">
                                                <div class="button-filter">
                                                    Bộ lọc
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="all-province-city">
                                                <select id="city" class="form-control">
                                                    <option>Thành Phố</option>
                                                    <?php
                                                    foreach ($city as $item) {
                                                        ?>
                                                        <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                            <option><?= $item['meta_value']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <br/>
                                                <select id="district" class="form-control">
                                                    <option>Quận - Huyện</option>
                                                </select>
                                                <br/>
                                                <select id="commune" class="form-control">
                                                    <option>Phường - Xã</option>
                                                </select>
                                                <br/>
                                                <select id="price" class="form-control">
                                                    <option>Giá tiền</option>
                                                    <option>0 đ - 1.000.000 đ</option>
                                                    <option>1.000.000 đ - 2.000.000 đ</option>
                                                    <option>2.000.000 đ - 3.000.000 đ</option>
                                                    <option>3.000.000 đ - 4.000.000 đ</option>
                                                    <option>4.000.000 đ trở lên</option>
                                                </select>
                                                <br/>
                                                <select id="rate" class="form-control">
                                                    <option value="0">Đánh giá</option>
                                                    <option>Tuyệt hảo</option>
                                                    <option>Rất tốt</option>
                                                    <option>Tốt</option>
                                                    <option>Dẽ chịu</option>
                                                </select>
                                                <br/>
                                                <select id="rank" class="form-control">
                                                    <option>Hạng</option>
                                                    <option>1 sao</option>
                                                    <option>2 sao</option>
                                                    <option>3 sao</option>
                                                    <option>4 sao</option>
                                                    <option>5 sao</option>
                                                </select>
                                                <br/>
                                                <button type="button" class="btn btn-default" id="search-list-tour">
                                                    <span class="glyphicon glyphicon-search"></span> <?php _e('[:en]Search[:vi]Tìm[:]')?>
                                                </button>
                                                <br/><br/>
                                                <span><?php _e('[:en]Search by name[:vi]Tìm theo tên[:]')?></span><br/>
                                                <input type="text" class="form-control" id="ten-tour"
                                                       placeholder="<?php _e('[:en]Search by name ...[:vi]Tìm theo tên ...[:]')?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <?php
                            while (have_posts()) :
                                the_post();
                                wc_get_template_part('content', 'single-product');
                            endwhile;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php else: ?>
    <main class="col-xs-12 col-xs-12 listing-page-border <?php echo $main_class ?>">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="main-content col-lg-12 col-xs-12">
                        <div class="row">

                            <aside class="col-lg-3 col-xs-12 ">
                                <div class="row">
                                    <?php if ($is_giaitri): ?>
                                    <div class="menu-left-mobile">
                                        <div class="col-xs-12 hidden-lg hidden-md">
                                            <div class="button-filter">Bộ lọc</div>
                                        </div>
                                        <div class="all-province-city">
                                            <div class="menu-left">
                                                <div class="filter-border">
                                                    <div class="filter-border">
                                                        <div class="filter-title">Thành Phố</div>
                                                        <div class=""><br/>
                                                            <select id="city" class="form-control">
                                                                <option>Thành Phố</option>
                                                                <?php
                                                                foreach ($city as $item) {
                                                                    ?>
                                                                    <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                        <option><?= $item['meta_value']; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                            <br/>
                                                            <select id="district" class="form-control">
                                                                <option>Quận - Huyện</option>
                                                            </select>
                                                            <br/>
                                                            <select id="commune" class="form-control">
                                                                <option>Phường - Xã</option>
                                                            </select>
                                                            <br/>
                                                            <span>GIẢI TRÍ</span><br/>
                                                            <input class="giai_tri" type="checkbox" value="Điểm thăm quan"/>Điểm thăm quan<br/>
                                                            <input class="giai_tri" type="checkbox" value="Karaoke - Club"/>Karaoke - Club<br/>
                                                            <input class="giai_tri" type="checkbox" value="Công viên"/>Công viên<br/>
                                                            <input class="giai_tri" type="checkbox" value="Vườn thú"/>Vườn thú<br/>
                                                            <span>LÀM ĐẸP</span><br/>
                                                            <input class="lam_dep" type="checkbox" value="Spa thẩm mỹ"/>Spa thẩm mỹ<br/>
                                                            <input class="lam_dep" type="checkbox" value="Nail"/>Nail<br/>
                                                            <input class="lam_dep" type="checkbox" value="Salon tóc"/>Salon tóc<br/>
                                                            <input class="lam_dep" type="checkbox" value="Massage"/>Massage<br/>
                                                            <br/>
                                                            <span>MUA SẮM</span><br/>
                                                            <input class="mua_sam" type="checkbox" value="Chợ"/>Chợ<br/>
                                                            <input class="mua_sam" type="checkbox" value="Siêu thị"/>Siêu thị<br/>
                                                            <input class="mua_sam" type="checkbox" value="Cửa hàng"/>Cửa hàng<br/>
                                                            <br/>
                                                            <button type="button" class="btn btn-default"
                                                                    id="search-list-giaitri">
                                                                <span class="glyphicon glyphicon-search"></span> <?php _e('[:en]Search[:vi]Tìm kiếm[:]')?>
                                                            </button>
                                                            <br/>
                                                        </div>
                                                        <br/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php elseif ($template == 1): ?>
                                    <div class="menu-left-mobile">
                                        <div class="col-xs-12 hidden-lg hidden-md">
                                            <div class="button-filter">Bộ lọc</div>
                                        </div>
                                        <div class="all-province-city">
                                            <div class="menu-left">
                                                <div class="filter-border">
                                                    <div class="filter-border">
                                                        <div class="filter-title"><?php _e('[:en]Search car[:vi]Tìm xe cho thuê[:]')?></div>
                                                        <div class=""><br/>
                                                            <select id="city" class="form-control">
                                                                <option>Thành Phố</option>
                                                                <?php
                                                                foreach ($city as $item) {
                                                                    ?>
                                                                    <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                        <option><?= $item['meta_value']; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                            <br/>
                                                            <select id="district" class="form-control">
                                                                <option>Quận - Huyện</option>
                                                            </select>
                                                            <br/>
                                                            <select id="commune" class="form-control">
                                                                <option>Phường - Xã</option>
                                                            </select>
                                                            <br/>
                                                            <span>Ngày nhận xe</span>
                                                            <input id="time_start" type="date" class="form-control">
                                                            <br/>
                                                            <span>Ngày trả xe</span>
                                                            <input id="time_end" type="date" class="form-control">
                                                            <br/>
                                                            <span>Kiểu thuê xe</span><br/>
                                                            <input class="kieu_thue" type="checkbox" value="Xe tự lái"/>Xe
                                                            tự lái<br/>
                                                            <input class="kieu_thue" type="checkbox" value="Xe tự lái"/>Xe
                                                            có người lái<br/>
                                                            <br/>
                                                            <button type="button" class="btn btn-default"
                                                                    id="search-list-transport">
                                                                <span class="glyphicon glyphicon-search"></span> <?php _e('[:en]Search[:vi]Tìm kiếm[:]')?>
                                                            </button>
                                                            <br/>
                                                        </div>
                                                        <br/>
                                                        <div class="filter-title"><?php _e('[:en]Search[:vi]Tìm kiếm[:]')?></div>
                                                        <span>Dòng xe</span><br/>
                                                        <input class="dong_xe" type="checkbox" value="Xe thường"/>Xe
                                                        thường<br/>
                                                        <input class="dong_xe" type="checkbox" value="Dcar Limousine"/>Dcar
                                                        Limousine<br/>
                                                        <br/>
                                                        <span>Loại xe</span><br/>
                                                        <input class="loai_xe" type="checkbox" value="4 chỗ"/>4 chỗ<br/>
                                                        <input class="loai_xe" type="checkbox" value="7 chỗ"/>7 chỗ<br/>
                                                        <input class="loai_xe" type="checkbox" value="9 chỗ"/>9 chỗ<br/>
                                                        <input class="loai_xe" type="checkbox" value="16 chỗ"/>16 chỗ<br/>
                                                        <input class="loai_xe" type="checkbox" value="29 chỗ"/>29 chỗ<br/>
                                                        <input class="loai_xe" type="checkbox" value="45 chỗ"/>45 chỗ<br/>
                                                        <br/>
                                                        <span>Xếp hạng</span><br/>
                                                        <input class="xep_hang_xe" type="checkbox" value="Tuyệt vời"/>Tuyệt
                                                        vời<br/>
                                                        <input class="xep_hang_xe" type="checkbox" value="Rất tôt"/>Rât
                                                        tốt<br/>
                                                        <input class="xep_hang_xe" type="checkbox" value="Tốt"/>Tốt<br/>
                                                        <br/>
                                                        <span><?php _e('[:en]Search by car provider[:vi]Tìm theo tên nhà cung cấp xe[:]')?></span><br/>
                                                        <input type="text" class="form-control" id="nha_cung_cap"
                                                               placeholder="<?php _e('[:en]Name of provider ...[:vi]Tên nhà cung cấp ...[:]')?>"/>
                                                        <div class="filter-content"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php elseif ($filter_template == 1): ?>
                                    <div class="menu-left-mobile">
                                        <div class="col-xs-12 hidden-lg hidden-md">
                                            <div class="button-filter"><?php _e('[:en]Filter[:vi]Bộ lọc[:]')?></div>
                                        </div>
                                        <div class="all-province-city">
                                            <div class="menu-left filter_food">
                                                <div class="sidebar hidden-sm hidden-xs">
                                                    <div class="mapp-wappar">
                                                        <div class="map-place">
                                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703"
                                                                    width="100%" height="150px" frameborder="0"
                                                                    style="border:0"
                                                                    allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                    <div class="map-sticker">
                                                        <span><?php _e('[:en]Show map[:vi]Hiển thị bản đồ[:]')?></span>
                                                    </div>
                                                </div>
                                                <div class="filter-border">
                                                    <div class="filter-title"><?php _e('[:en]Filter by[:vi]Chọn lọc theo[:]')?></div>
                                                    <br/>
                                                    <select id="city" class="form-control">
                                                        <option>Thành Phố</option>
                                                        <?php
                                                        foreach ($city as $item) {
                                                            ?>
                                                            <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                <option><?= $item['meta_value']; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                    <br/>
                                                    <select id="district" class="form-control">
                                                        <option>Quận - Huyện</option>
                                                    </select>
                                                    <br/>
                                                    <select id="commune" class="form-control">
                                                        <option>Phường - Xã</option>
                                                    </select>
                                                    <br/>
                                                    <span>LOẠI CƠ SỞ</span><br/>
                                                    <input class="loai_co_so" type="checkbox" value="Nhà hàng"/>Nhà
                                                    hàng<br/>
                                                    <input class="loai_co_so" type="checkbox" value="Món tráng miệng"/>Món
                                                    tráng miệng<br/>
                                                    <input class="loai_co_so" type="checkbox" value="Cà phê và trà"/>Cà phê
                                                    và trà<br/>
                                                    <input class="loai_co_so" type="checkbox" value="Bar và quán rượu"/>Bar
                                                    và quán rượu<br/>
                                                    <br/>
                                                    <span>ẨM THỰC VÀ MÓN ĂN</span><br/>
                                                    <input class="food" type="checkbox" value="Món Á"/>Món Á<br/>
                                                    <input class="food" type="checkbox" value="Món Âu"/>Món Âu<br/>
                                                    <input class="food" type="checkbox" value="Món Trung"/>Món Trung<br/>
                                                    <input class="food" type="checkbox" value="Món Thái"/>Món Thái<br/>
                                                    <br/>
                                                    <span>PHÙ HỢP VỚI</span><br/>
                                                    <input class="fit" type="checkbox" value="Gia đình có trẻ em"/>Gia đình
                                                    có trẻ em<br/>
                                                    <input class="fit" type="checkbox" value="Hội họp"/>Hội họp<br/>
                                                    <input class="fit" type="checkbox" value="Tổ chức sự kiện"/>Tổ chức sự
                                                    kiện<br/>
                                                    <input class="fit" type="checkbox" value="Món ăn địa phương"/>Món ăn địa
                                                    phương<br/>
                                                    <br/>
                                                    <span>GÍA CẢ</span><br/>
                                                    <input class="food_price" type="checkbox" value="Đồ ăn giá rẻ"/>Đồ ăn
                                                    giá rẻ<br/>
                                                    <input class="food_price" type="checkbox" value="Giá trung bình"/>Giá
                                                    trung bình<br/>
                                                    <input class="food_price" type="checkbox" value="Cao cấp"/>Cao cấp<br/>
                                                    <br/>
                                                    <button type="button" class="btn btn-default" id="search-list-food">
                                                        <span class="glyphicon glyphicon-search"></span> Tìm
                                                    </button>
                                                    <br/><br/>
                                                    <span><?php _e('[:en]Search by name[:vi]Tìm theo tên[:]')?></span><br/>
                                                    <input type="text" class="form-control" id="ten-nha-hang"
                                                           placeholder="<?php _e('[:en]Search by name ...[:vi]Tìm theo tên ...[:]')?>"/>
                                                    <!--                                                <div class="filter-content"></div>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php elseif ($filter_template == 3): ?>
                                    <div class="menu-left-mobile">
                                        <div class="col-xs-12 hidden-lg hidden-md">
                                            <div class="button-filter">Bộ lọc</div>
                                        </div>
                                        <div class="all-province-city">
                                            <div class="menu-left">
                                                <div class="sidebar hidden-sm hidden-xs">
                                                    <div class="mapp-wappar">
                                                        <div class="map-place">
                                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703"
                                                                    width="100%" height="150px" frameborder="0"
                                                                    style="border:0"
                                                                    allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                    <div class="map-sticker">
                                                        <span><?php _e('[:en]Show map[:vi]Hiển thị bản đồ[:]')?></span>
                                                    </div>
                                                </div>
                                                <div class="filter-border ">
                                                    <div class="filter-title"><?php _e('[:en]Chọn lọc theo[:vi]Filter by[:]')?></div>
                                                    <br/>
                                                    <select id="city" class="form-control">
                                                        <option>Thành Phố</option>
                                                        <?php
                                                        foreach ($city as $item) {
                                                            ?>
                                                            <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                <option><?= $item['meta_value']; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                    <br/>
                                                    <select id="district" class="form-control">
                                                        <option>Quận - Huyện</option>
                                                    </select>
                                                    <br/>
                                                    <select id="commune" class="form-control">
                                                        <option>Phường - Xã</option>
                                                    </select>
                                                    <br/>
                                                    <button type="button" class="btn btn-default" id="search-list-ticket">
                                                        <span class="glyphicon glyphicon-search"></span> Tìm
                                                    </button>
                                                    <br/><br/>
                                                    <div class="find-name-ticket">Tìm theo tên phòng vé</div><br/>
                                                    <input type="text" class="form-control" id="post-title"
                                                           placeholder="Tên phòng vé ..."/>
                                                    <br/>
                                                </div>
                                            </div>
                                            <div class="menu-left">
                                                <div class="filter-border">
                                                    <div class="filter-border">
                                                        <div>
                                                            <?php _e('[:en]Register to receive news to take promotions (discount after login )[:vi]Đăng ký bản tin để nhận ngay những
                                                            ƯU ĐÃI BÍ MẬT
                                                            (Giảm giá ngay khi bạn đăng nhập)[:]')?>

                                                        </div>
                                                        <br/>
                                                        <input type="email" placeholder="<?php _e('[:en]Enter email ...[:vi]Nhập địa chỉ email ..[:]')?>" /><br/>
                                                        <input type="button" value="<?php _e('[:en]I want to register[:vi]Tôi muốn đăng ký[:]')?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php elseif ($filter_template == 2 && $template == 0): ?>
                                        <div class="menu-left-mobile">
                                            <div class="col-xs-12 hidden-lg hidden-md">
                                                <div class="button-filter">Bộ lọc</div>
                                            </div>
                                            <div class="all-province-city">
                                                <div class="menu-left">
                                                    <div class="sidebar hidden-sm hidden-xs">
                                                        <div class="mapp-wappar">
                                                            <div class="map-place">
                                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703"
                                                                        width="100%" height="150px" frameborder="0"
                                                                        style="border:0"
                                                                        allowfullscreen></iframe>
                                                            </div>
                                                        </div>
                                                        <div class="map-sticker">
                                                            <span><?php _e('[:en]Show map[:vi]Hiển thị bản đồ[:]')?></span>
                                                        </div>
                                                    </div>
                                                    <div class="filter-border">
                                                        <div class="filter-title"><?php _e('[:en]Filter by[:vi]Chọn lọc theo[:]')?></div>
                                                        <br/>
                                                        <select id="city" class="form-control">
                                                            <option>Thành Phố</option>
                                                            <?php
                                                            foreach ($city as $item) {
                                                                ?>
                                                                <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                    <option><?= $item['meta_value']; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                        <br/>
                                                        <select id="district" class="form-control">
                                                            <option>Quận - Huyện</option>
                                                        </select>
                                                        <br/>
                                                        <select id="commune" class="form-control">
                                                            <option>Phường - Xã</option>
                                                        </select>
                                                        <br/>
                                                        <select id="business_certificate" class="form-control">
                                                            <option value="0">Giấy phép kinh doanh</option>
                                                            <option value="Nội địa">Nội địa</option>
                                                            <option value="Quốc tế">Quốc tế</option>
                                                        </select>
                                                        <br/>
                                                        <button type="button" class="btn btn-default" id="search-list-company">
                                                            <span class="glyphicon glyphicon-search"></span> <?php _e('[:en]Search[:vi]Tìm[:]')?>
                                                        </button>
                                                        <br/><br/>
                                                        <span><?php _e('[:en]Tìm theo tên[:vi]Tìm theo tên[:]')?></span><br/>
                                                        <input type="text" class="form-control" id="ten"
                                                               placeholder="<?php _e('[:en]Tìm theo tên[:vi]Tìm theo tên[:]')?> ..."/>
                                                        <!--                                                <div class="filter-content"></div>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php elseif ($filter_template == 2): ?>
                                    <div class="menu-left-mobile">
                                        <div class="col-xs-12 hidden-lg hidden-md">
                                            <div class="button-filter"><?php _e('[:en]Filter[:vi]Filter[:]')?></div>
                                        </div>
                                        <div class="all-province-city">
                                            <div class="menu-left">
                                                <div class="sidebar hidden-sm hidden-xs">
                                                    <div class="mapp-wappar">
                                                        <div class="map-place">
                                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703"
                                                                    width="100%" height="150px" frameborder="0"
                                                                    style="border:0"
                                                                    allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                    <div class="map-sticker">
                                                        <span><?php _e('[:en]Show map[:vi]Hiển thị bản đồ[:]')?></span>
                                                    </div>
                                                </div>
                                                <div class="filter-border">
                                                    <div class="filter-title"><?php _e('[:en]Filter by[:vi]Chọn lọc theo[:]')?></div>
                                                    <br/>
                                                    <select id="city" class="form-control">
                                                        <option>Thành Phố</option>
                                                        <?php
                                                        foreach ($city as $item) {
                                                            ?>
                                                            <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                <option><?= $item['meta_value']; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                    <br/>
                                                    <select id="district" class="form-control">
                                                        <option>Quận - Huyện</option>
                                                    </select>
                                                    <br/>
                                                    <select id="commune" class="form-control">
                                                        <option>Phường - Xã</option>
                                                    </select>
                                                    <br/>
                                                    <select id="price" class="form-control">
                                                        <option>Giá tiền</option>
                                                        <option>0 đ - 1.000.000 đ</option>
                                                        <option>1.000.000 đ - 2.000.000 đ</option>
                                                        <option>2.000.000 đ - 3.000.000 đ</option>
                                                        <option>3.000.000 đ - 4.000.000 đ</option>
                                                        <option>4.000.000 đ trở lên</option>
                                                    </select>
                                                    <br/>
                                                    <select id="rate" class="form-control">
                                                        <option value="0">Đánh giá</option>
                                                        <option>Tuyệt hảo</option>
                                                        <option>Rất tốt</option>
                                                        <option>Tốt</option>
                                                        <option>Dẽ chịu</option>
                                                    </select>
                                                    <br/>
                                                    <select id="rank" class="form-control">
                                                        <option>Hạng</option>
                                                        <option>1 sao</option>
                                                        <option>2 sao</option>
                                                        <option>3 sao</option>
                                                        <option>4 sao</option>
                                                        <option>5 sao</option>
                                                    </select>
                                                    <br/>
                                                    <button type="button" class="btn btn-default" id="search-list-tour">
                                                        <span class="glyphicon glyphicon-search"></span> <?php _e('[:en]Search[:vi]Tìm[:]')?>
                                                    </button>
                                                    <br/><br/>
                                                    <span><?php _e('[:en]Tìm theo tên[:vi]Tìm theo tên[:]')?></span><br/>
                                                    <input type="text" class="form-control" id="ten-tour"
                                                           placeholder="<?php _e('[:en]Tìm theo tên[:vi]Tìm theo tên[:]')?> ..."/>
                                                    <!--                                                <div class="filter-content"></div>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php elseif ($filter_template == 0): ?>
                                    <div class="menu-left-mobile">
                                        <div class="col-xs-12 hidden-lg hidden-md">
                                            <div class="button-filter">Bộ lọc</div>
                                        </div>
                                        <div class="all-province-city">
                                            <div class="menu-left">
                                                <div class="sidebar hidden-sm hidden-xs">
                                                    <div class="mapp-wappar">
                                                        <div class="map-place">
                                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703"
                                                                    width="100%" height="150px" frameborder="0"
                                                                    style="border:0"
                                                                    allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                    <div class="map-sticker">
                                                        <span><?php _e('[:en]Show map[:vi]Hiển thị bản đồ[:]')?></span>
                                                    </div>
                                                </div>
                                                <div class="filter-border">
                                                    <div class="filter-title"><?php _e('[:en]Filter by[:vi]Chọn lọc theo[:]')?></div>
                                                    <br/>
                                                    <select id="city" class="form-control">
                                                        <option>Thành Phố</option>
                                                        <?php
                                                        foreach ($city as $item) {
                                                            ?>
                                                            <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                <option><?= $item['meta_value']; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                    <br/>
                                                    <select id="district" class="form-control">
                                                        <option>Quận - Huyện</option>
                                                    </select>
                                                    <br/>
                                                    <select id="commune" class="form-control">
                                                        <option>Phường - Xã</option>
                                                    </select>
                                                    <br/>
                                                    <span>Giá tiền</span><br/>
                                                    <input class="price" type="checkbox" value="0 đ - 1.000.000 đ">0 đ -
                                                    1.000.000 đ<br>
                                                    <input class="price" type="checkbox" value="1.000.000 đ - 2.000.000 đ">1.000.000 đ
                                                    - 2.000.000 đ<br>
                                                    <input class="price" type="checkbox" value="2.000.000 đ - 3.000.000 đ">2.000.000 đ
                                                    - 3.000.000 đ<br>
                                                    <input class="price" type="checkbox" value="3.000.000 đ - 4.000.000 đ">3.000.000 đ
                                                    - 4.000.000 đ<br>
                                                    <input class="price" type="checkbox" value="4.000.000 đ trở lên">4.000.000 đ
                                                    trở lên<br>
                                                    <br/>
                                                    <span>Đánh giá của khách sạn</span><br/>
                                                    <input class="rate" type="checkbox" value="Tuyệt hảo"/>Tuyệt hảo<br/>
                                                    <input class="rate" type="checkbox" value="Tuyệt hảo"/>Rất tốt<br/>
                                                    <input class="rate" type="checkbox" value="Tuyệt hảo"/>Tốt<br/>
                                                    <input class="rate" type="checkbox" value="Tuyệt hảo"/>Dễ chịu<br/>
                                                    <br/>
                                                    <span>Hạng của khách sạn</span><br/>
                                                    <input class="rank" type="checkbox" value="1 sao"/>1 sao<br/>
                                                    <input class="rank" type="checkbox" value="2 sao"/>2 sao<br/>
                                                    <input class="rank" type="checkbox" value="3 sao"/>3 sao<br/>
                                                    <input class="rank" type="checkbox" value="4 sao"/>4 sao<br/>
                                                    <input class="rank" type="checkbox" value="5 sao"/>5 sao<br/>
                                                    <br/>
                                                    <button type="button" class="btn btn-default" id="search-list-hotel">
                                                        <span class="glyphicon glyphicon-search"></span> <?php _e('[:en]Search[:vi]Tìm[:]')?>
                                                    </button>
                                                    <br/><br/>
                                                    <span><?php _e('[:en]Search by hotel name[:vi]Tìm theo tên khách sạn[:]')?></span><br/>
                                                    <input type="text" class="form-control" id="ten-khach-san"
                                                           placeholder="<?php _e('[:en]Search by hotel name[:vi]Tìm theo tên khách sạn[:]')?> ..."/>
                                                    <!--                                                <div class="filter-content"></div>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php else: ?>
                                    <div class="menu-left-mobile">
                                        <div class="col-xs-12 hidden-lg hidden-md">
                                            <div class="button-filter"><?php _e('[:en]Filter[:vi]Bộ lọc[:]')?></div>
                                        </div>
                                        <div class="all-province-city">
                                            <div class="menu-left">
                                                <div class="sidebar hidden-sm hidden-xs">
                                                    <div class="mapp-wappar">
                                                        <div class="map-place">
                                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703"
                                                                    width="100%" height="150px" frameborder="0"
                                                                    style="border:0"
                                                                    allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                    <div class="map-sticker">
                                                        <span><?php _e('[:en]Show map[:vi]Hiển thị bản đồ[:]')?></span>
                                                    </div>
                                                </div>
                                                <div class="filter-border">
                                                    <div class="filter-title"><?php _e('[:en]Filter by[:vi]Chọn lọc theo[:]')?></div>
                                                    <br/>
                                                    <select id="city" class="form-control">
                                                        <option>Thành Phố</option>
                                                        <?php
                                                        foreach ($city as $item) {
                                                            ?>
                                                            <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                <option><?= $item['meta_value']; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                    <br/>
                                                    <select id="district" class="form-control">
                                                        <option>Quận - Huyện</option>
                                                    </select>
                                                    <br/>
                                                    <select id="commune" class="form-control">
                                                        <option>Phường - Xã</option>
                                                    </select>
                                                    <br/>
                                                    <span>Giá tiền</span><br/>
                                                    <input class="price" type="checkbox" value="0 đ - 1.000.000 đ">0 đ -
                                                    1.000.000 đ<br>
                                                    <input class="price" type="checkbox" value="1.000.000 đ - 2.000.000 đ">1.000.000 đ
                                                    - 2.000.000 đ<br>
                                                    <input class="price" type="checkbox" value="2.000.000 đ - 3.000.000 đ">2.000.000 đ
                                                    - 3.000.000 đ<br>
                                                    <input class="price" type="checkbox" value="3.000.000 đ - 4.000.000 đ">3.000.000 đ
                                                    - 4.000.000 đ<br>
                                                    <input class="price" type="checkbox" value="4.000.000 đ trở lên">4.000.000 đ
                                                    trở lên<br>
                                                    <br/>
                                                    <button type="button" class="btn btn-default" id="search-list">
                                                        <span class="glyphicon glyphicon-search"></span> <?php _e('[:en]Search[:vi]Tìm[:]')?>
                                                    </button>
                                                    <br/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                </div>
                            </aside>
                            <section class="<?php echo $section_class; ?>">
                                <div class="row">
                                    <div class="<?php echo $section_row_div_class; ?>"
                                         id="<?php echo $section_row_div_id; ?>">
                                        <?php if ($template != 2): ?>
                                            <div class="toolbar hidden-sm hidden-xs">
                                                <?php woocommerce_result_count(); ?>
                                                <?php woocommerce_catalog_ordering(); ?>

                                            </div>
                                        <?php endif; ?>
                                        <?php

                                        if (have_posts()) {

                                            /**
                                             * Hook: woocommerce_before_shop_loop.
                                             *
                                             * @hooked wc_print_notices - 10
                                             * @hooked woocommerce_result_count - 20
                                             * @hooked woocommerce_catalog_ordering - 30
                                             */
//                                            do_action( 'woocommerce_before_shop_loop' );

                                            woocommerce_product_loop_start();

                                            if (wc_get_loop_prop('total')) {
                                                while (have_posts()) {
                                                    the_post();

                                                    /**
                                                     * Hook: woocommerce_shop_loop.
                                                     *
                                                     * @hooked WC_Structured_Data::generate_product_data() - 10
                                                     */
                                                    do_action('woocommerce_shop_loop');

                                                    wc_get_template_part('content', 'product');
                                                }
                                            }

                                            woocommerce_product_loop_end();

                                            /**
                                             * Hook: woocommerce_after_shop_loop.
                                             *
                                             * @hooked woocommerce_pagination - 10
                                             */
//                                            do_action('woocommerce_after_shop_loop');
                                        } else {
                                            /**
                                             * Hook: woocommerce_no_products_found.
                                             *
                                             * @hooked wc_no_products_found - 10
                                             */
                                            do_action('woocommerce_no_products_found');
                                        }
                                        woocommerce_pagination_revert();
                                        ?>
                                        <div class="note-bottom"><?php _e('[:en]The room price show on Trakevn calculate by each night for all room choosen[:vi]Giá phòng hiển thị trên Trekvn tính theo từng đêm cho
                                            tổng số phòng đã chọn[:]') ?>
                                        </div>
                                    </div>
                                    <div class="listing-page woocommerce" id="search-listing-page" style="display:none;">
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php endif; ?>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $("#search-list").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var price = $(".price:checked").val();
                var url = '<?php echo get_site_url(); ?>/search-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-transport").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var time_start = null;
                if ($("#time_start").val()) {
                    var time_start_date = new Date($("#time_start").val());
                    var moth = time_start_date.getMonth() + 1;
                    time_start = moth + "/" + time_start_date.getDate() + "/" + time_start_date.getFullYear();
                }
                var time_end = null;
                if ($("#time_end").val()) {
                    var time_end_date = new Date($("#time_end").val());
                    var month = time_end_date.getMonth() + 1;
                    time_end = month + "/" + time_end_date.getDate() + "/" + time_end_date.getFullYear();
                    ;
                }

                var kieu_thue = $(".kieu_thue:checked").val();
                var dong_xe = $(".dong_xe:checked").val();
                var loai_xe = $(".loai_xe:checked").val();
                var xep_hang_xe = $(".xep_hang_xe:checked").val();
                var nha_cung_cap = $("#nha_cung_cap").val();
                var url = '<?php echo get_site_url(); ?>/search-transport-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'time_start': time_start,
                            'time_end': time_end,
                            'kieu_thue': kieu_thue,
                            'dong_xe': dong_xe,
                            'loai_xe': loai_xe,
                            'xep_hang_xe': xep_hang_xe,
                            'nha_cung_cap': nha_cung_cap
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-transport-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-food").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var loai_co_so = $(".loai_co_so:checked").val();
                var food = $(".food:checked").val();
                var fit = $(".fit:checked").val();
                var food_price = $(".food_price:checked").val();
                var ten_nha_hang = $("#ten_nha_hang").val();
                var url = '<?php echo get_site_url(); ?>/search-food-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'loai_co_so': loai_co_so,
                            'food': food,
                            'fit': fit,
                            'food_price': food_price,
                            'ten_nha_hang': ten_nha_hang
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-hotel").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#ten-khach-san").val();
                var price = $(".price:checked").val();
                var rate = $(".rate:checked").val();
                var rank = $(".rank:checked").val();
                var url = '<?php echo get_site_url(); ?>/search-hotel-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                            'rate': rate,
                            'rank': rank,
                            'ten': ten
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $(".pagination").hide();
                            $(".review-comment-border").find('ul.pagination').show();
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-tour").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#ten-tour").val();
                var price = $("#price").val();
                var rate = $("#rate").val();
                var rank = $("#rank").val();
                var url = '<?php echo get_site_url(); ?>/search-tour-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                            'rate': rate,
                            'rank': rank,
                            'ten': ten
                        },
                        success: function (response) {
                            $(".entertainment-page-content").remove();
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-company").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#ten").val();
                var certificate = $("#business_certificate").val();
                var url = '<?php echo get_site_url(); ?>/search-company-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'ten': ten,
                            'certificate':certificate
                        },
                        success: function (response) {
                            $(".entertainment-page-content").remove();
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-ticket").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#post-title").val();
                var url = '<?php echo get_site_url(); ?>/search-ticket-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'ten': ten
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $(".pagination").hide();
                            $(".review-comment-border").find('ul.pagination').show();
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-giaitri").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var giai_tri = $(".giai_tri:checked").val();
                var lam_dep = $(".lam_dep:checked").val();
                var mua_sam = $(".mua_sam:checked").val();
                var url = '<?php echo get_site_url(); ?>/search-entertainment-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'giai_tri': giai_tri,
                            'lam_dep': lam_dep,
                            'mua_sam': mua_sam
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $(".pagination").hide();
                            $(".review-comment-border").find('ul.pagination').show();
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
        });
    </script>
<?php
/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
//do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action('woocommerce_sidebar');

get_footer('shop');
