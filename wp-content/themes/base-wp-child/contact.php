<?php
// Template Name: Liên hệ
get_header(); ?>
    <main class="col-xs-12 col-xs-12 listing-page-border">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="main-content col-lg-12 col-xs-12">
                        <div class="row">
                            <aside class="col-lg-3 col-xs-12 ">
                                <div class="row">
                                    <div class="menu-left hidden-sm hidden-xs">
                                        <div class="sidebar customer-care">
                                            <div class="customer-care-call-text">
                                                Gọi điện để tư vấn
                                            </div>
                                            <div class="customer-number-phone">
                                                <span class="icon-telephone"><i class="fas fa-phone"></i></span>
                                                090 123 456
                                            </div>
                                            <div class="customer-care-call-text">
                                                Hoặc để lại số điện thoại Trekvn sẽ gọi cho bạn
                                            </div>
                                            <input type="text" value="" placeholder="Số điện thoại của bạn...">
                                        </div>
                                        <div class="register-information">
                                            <div class="register-info-title">Đăng kí nhận thông tin</div>
                                            <div class="register-info-text">Hãy là người đầu tiên nhận được những tin tức thú vị và những chương trình khuyến mãi nóng hổi nhất</div>
                                            <div class="form-register-info">
                                                <?php $base_url = get_site_url(); ?>
                                                <form role="form" method="post" action="<?php echo $base_url ?>/?na=s"
                                                      id="ajax-newsletter-left">
                                                    <div class="form-group">
                                                        <label for="name">Họ và tên</label>
                                                        <input type="text" class="form-control" id="name"
                                                               placeholder="Name" required name="nn">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input type="email" class="form-control" id="email"
                                                               placeholder="Email" required name="ne">
                                                    </div>
                                                    <button type="submit"
                                                            class="btn btn-default register ajax-newsletter-left">Đăng
                                                        kí
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <section class=" col-lg-9 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-12 col-xs-12">
                                        <div class="row">
                                            <div class="contact-page">
                                                <div class="border-contact">
                                                    <div class="form-questions">
                                                        <h4 class="title-form-question">Vui lòng để lại câu hỏi, Trekvn liên hệ với bạn</h4>
                                                        <div class="form-questions-border">
                                                            <?php
                                                            echo do_shortcode('[contact-form-7 id="388" title="Liên hệ"]');
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="information-contact">
                                                        <div class="label-contact">Thông tin liên hệ</div>
                                                        <div class="name-company-contact">Công ty du lịch Trekvn</div>
                                                        <div class="address-contact">
                                                            <span class="title-pre">Địa chỉ :</span>
                                                            thành phố Hà Nội
                                                        </div>
                                                        <div class="telephonr-contact">
                                                            <span class="title-pre">Điện thoại :</span>
                                                            (04) 24 00 00 00 - Email: info@trekvn.com
                                                        </div>
                                                    </div>
                                                    <div class="information-order">
                                                        <div class="label-contact">THông tin viết hóa đơn</div>
                                                        <div class="name-company-contact">Công ty TNHH du lịch Trekvn</div>
                                                        <div class="address-contact">
                                                            <span class="title-pre">Mã số thuế :</span>
                                                            01010000000 - Sở kế hoạch và đầu tư Thành phố Hà Nội cấp ngày 29/03/2011
                                                        </div>
                                                        <div class="telephonr-contact">
                                                            <span class="title-pre">Địa chỉ ĐKKD :</span>
                                                            thành phố Hà Nội<br>
                                                            Giấy phép Kinh doanh Lữ hành Quốc tế số 01000/2015/TCDL-GPLHQT
                                                        </div>
                                                    </div>
                                                    <div class="information-order">
                                                        <div class="label-contact">Bản đồ</div>
                                                        <div class="map-contact">
                                                            <p>Trụ sở chính tại Hà Nội</p>
                                                        </div>
                                                        <div class="map-detail">
                                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703" width="100%" height="400px" frameborder="0" style="border:0" allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
get_footer();
