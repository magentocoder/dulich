<?php
// Template Name: Giải trí
get_header(); ?>
    <main class="col-xs-12 col-xs-12 listing-page-border">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="main-content col-lg-12 col-xs-12">
                        <div class="row">
                            <aside class="col-lg-3 col-xs-12 ">
                                <div class="row">
                                    <div class="menu-left hidden-sm hidden-xs">
                                        <div class="sidebar customer-care">
                                            <div class="customer-care-call-text">
                                                Gọi điện để tư vấn
                                            </div>
                                            <div class="customer-number-phone">
                                                <span class="icon-telephone"><i class="fas fa-phone"></i></span>
                                                090 123 456
                                            </div>
                                            <div class="customer-care-call-text">
                                                Hoặc để lại số điện thoại Trekvn sẽ gọi cho bạn
                                            </div>
                                            <input type="text" value="" placeholder="Số điện thoại của bạn...">
                                        </div>
                                        <div class="register-information">
                                            <div class="register-info-title">Đăng kí nhận thông tin</div>
                                            <div class="register-info-text">Hãy là người đầu tiên nhận được những tin
                                                tức thú vị và những chương trình khuyến mãi nóng hổi nhất
                                            </div>
                                            <div class="form-register-info">
                                                <?php $base_url = get_site_url(); ?>
                                                <form role="form" method="post" action="<?php echo $base_url ?>/?na=s"
                                                      id="ajax-newsletter-left">
                                                    <div class="form-group">
                                                        <label for="name">Họ và tên</label>
                                                        <input type="text" class="form-control" id="name"
                                                               placeholder="Name" required name="nn">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input type="email" class="form-control" id="email"
                                                               placeholder="Email" required name="ne">
                                                    </div>
                                                    <button type="submit"
                                                            class="btn btn-default register ajax-newsletter-left">Đăng
                                                        kí
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <section class=" col-lg-9 col-xs-12">
                                <div class="row">
                                    <?php
                                    $paged = (get_query_var('paged') ? get_query_var('paged') : 1);
                                    $has_block_top = ($paged == 1 ? true : false);
                                    $top_post = array();
                                    $child_post = array();
                                    if ($has_block_top): ?>
                                        <?php
                                        $arg = array(
                                            'posts_per_page' => 1,
                                            'orderby' => 'term_id',
                                            'order' => 'DESC',
                                            'cat' => 22,
                                            'limit' => 1,
                                            'meta_query' => [
                                                [
                                                    'key' => 'noi_bat',
                                                    'value' => 1,
                                                    'compare' => '='
                                                ]
                                            ]
                                        );
                                        $r = new WP_Query($arg);
                                        ?>
                                        <?php if ($r->have_posts()): ?>
                                            <?php
                                            foreach ($r->posts as $p) {
                                                $id = $p->ID;
                                                $top_post['image'] = get_the_post_thumbnail_url($p);
                                                $top_post['title'] = $p->post_title;
                                                $short_description = get_field('short_description', $id);
                                                if (strlen($short_description) > 500) {
                                                    $short_description = mb_substr($short_description, 0, 500) . '...';
                                                }
                                                $top_post['short_description'] = $short_description;
                                                $top_post['link'] = get_permalink($id);
                                            }
                                            ?>
                                        <?php else: ?>
                                            <?php
                                            $arg = array(
                                                'posts_per_page' => 4,
                                                'orderby' => 'term_id',
                                                'order' => 'DESC',
                                                'cat' => 22,
                                                'paged' => $paged,
                                                'limit' => 4
                                            );
                                            $r = new WP_Query($arg);
                                            ?>
                                            <?php if ($r->have_posts()): ?>
                                                <?php
                                                $i = 1;
                                                foreach ($r->posts as $p) {
                                                    $id = $p->ID;
                                                    if ($i == 1) {
                                                        $top_post['image'] = get_the_post_thumbnail_url($p);
                                                        $top_post['title'] = $p->post_title;
                                                        $short_description = get_field('short_description', $id);
                                                        if (strlen($short_description) > 500) {
                                                            $short_description = mb_substr($short_description, 0, 500) . '...';
                                                        }
                                                        $top_post['short_description'] = $short_description;
                                                        $top_post['link'] = get_permalink($id);
                                                    } else {
                                                        $child_post[$id]['image'] = get_the_post_thumbnail_url($p);
                                                        $child_post[$id]['title'] = $p->post_title;
                                                        $short_description = get_field('short_description', $id);
                                                        if (strlen($short_description) > 300) {
                                                            $short_description = mb_substr($short_description, 0, 300) . '...';
                                                        }
                                                        $child_post[$id]['short_description'] = $short_description;
                                                        $child_post[$id]['link'] = get_permalink($id);
                                                    }
                                                    $i++;
                                                }
                                                ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php
                                    if (empty($child_post) || !$has_block_top) {
                                        $arg = array('posts_per_page' => 3,
                                            'orderby' => 'term_id',
                                            'order' => 'DESC',
                                            'cat' => 22,
                                            'paged' => $paged,
                                            'limit' => 3
                                        );
                                        $r = new WP_Query($arg);
                                        if ($r->have_posts()) {
                                            foreach ($r->posts as $p) {
                                                $id = $p->ID;
                                                $child_post[$id]['image'] = get_the_post_thumbnail_url($p);
                                                $child_post[$id]['title'] = $p->post_title;
                                                $short_description = get_field('short_description', $id);
                                                if (strlen($short_description) > 300) {
                                                    $short_description = mb_substr($short_description, 0, 300) . '...';
                                                }
                                                $child_post[$id]['short_description'] = $short_description;
                                                $child_post[$id]['link'] = get_permalink($id);
                                            }
                                        }

                                    }

                                    ?>
                                    <div class="entertainment-page-content col-lg-12 col-xs-12">
                                        <div class="row">
                                            <?php if ($has_block_top): ?>
                                                <article class="entertainment-main col-lg-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="col-lg-7 col-xs-12">
                                                        <div class="row">
                                                            <div class="entertainment-image">
                                                                <a href="<?php echo $top_post['link']; ?>"
                                                                   title="#">
                                                                    <img src="<?php echo $top_post['image']; ?>"
                                                                         title="#" alt="#">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-5 col-xs-12">
                                                        <div class="row">
                                                            <div class="entertainment-content">
                                                                <div class="entertainment-content-label">
                                                                    <a href="<?php echo $top_post['link']; ?>"
                                                                       title="#">
                                                                        <?php echo $top_post['title']; ?>
                                                                    </a>
                                                                </div>
                                                                <div class="entertainment-content-detail">
                                                                    <?php echo $top_post['short_description']; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                </article><?php endif; ?>
                                            <?php foreach ($child_post as $p): ?>
                                                <article class="entertainment-sub col-lg-4 col-sm-6 col-xs-12">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-xs-12">
                                                            <div class="row">
                                                                <div class="entertainment-image">
                                                                    <a href="<?php echo $p['link'] ?>" title="#">
                                                                        <img src="<?php echo $p['image'] ?>"
                                                                             title="#"
                                                                             alt="#">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-xs-12">
                                                            <div class="row">
                                                                <div class="entertainment-content">
                                                                    <div class="entertainment-content-label">
                                                                        <a href="<?php echo $p['link'] ?>"
                                                                           title="#">
                                                                            <?php echo $p['title'] ?>
                                                                        </a>
                                                                    </div>
                                                                    <div class="entertainment-content-detail">
                                                                        <?php echo $p['short_description'] ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="product-detail-page entertainment-page col-xs-12 col-lg-12">
                                        <div class="pagination-border">
                                            <ul class="pagination">
                                                <?php
                                                $total = $r->max_num_pages;
                                                $current = get_query_var('paged');
                                                $base = isset($base) ? $base : esc_url_raw(str_replace(999999999, '%#%', remove_query_arg('add-to-cart', get_pagenum_link(999999999, false))));
                                                $format = isset($format) ? $format : '';
                                                echo paginate_links(apply_filters('woocommerce_pagination_args', array( // WPCS: XSS ok.
                                                    'base' => $base,
                                                    'format' => $format,
                                                    'add_args' => false,
                                                    'current' => max(1, $current),
                                                    'total' => $total,
                                                    'prev_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                                                    'next_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                                                    'type' => 'list',
                                                    'end_size' => 3,
                                                    'mid_size' => 3,
                                                )));
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
get_footer();
