<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Hook Woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
global $post;

global $wp;
$current_url = home_url($wp->request) . "?comment=s";
$base_url = get_site_url();

$cat_parent = get_the_terms(get_the_ID(), 'product_cat');
$cat_parent_id = $cat_parent[0]->term_id;
$template = (int)get_field('template', 'product_cat_' . $cat_parent_id);
?>
<?php if ($template == 3): ?>
    <section class=" col-lg-9 col-xs-12">
        <div class="row">
            <div class="col-xs-12 col-lg-12">
                <div class="row">
                    <div class="product-detail-page">
                        <aside class="content-tourist">
                            <div class="title-tourist"><?php the_title() ?></div>
                            <div class="info-tourist">
                                <div class="header-tourist">
                                    <div class="image-info-first"><?php woocommerce_template_loop_product_thumbnail() ?></div>
                                    <div class="short-description">
                                        <?php
                                        echo $post->post_excerpt;
                                        ?>
                                    </div>
                                </div>
                                <?php the_content() ?>
                            </div>
                        </aside>
                        <div class="related-tourist">
                            <div class="related-tourist-label"><?php _e('[:en]Maybe you are interested?[:vi]Có thể bạn quan tâm?[:]') ?></div>
                            <div class="related-list-tourist">
                                <?php


                                $arg = array(
                                    'post_type' => 'product',
                                    'orderby' => 'term_id',
                                    'order' => 'DESC',
                                    'posts_per_page' => 3,
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'product_cat',
                                            'field' => 'id',
                                            'terms' => $cat_parent_id
                                        ))
                                );
                                $r = new WP_Query($arg);
                                ?>
                                <?php while ($r->have_posts()): ?>
                                    <?php $r->the_post(); ?>
                                    <div class="col-lg-4 col-xs-4">
                                        <div class="row">
                                            <div class="related-item-tourist">
                                                <div class="image-item-related">
                                                    <a href="<?php echo get_the_permalink() ?>">
                                                        <?php the_post_thumbnail(); ?>
                                                    </a>

                                                </div>
                                                <div class="title-item-related">
                                                    <a href="<?php echo get_the_permalink() ?>">
                                                        <?php the_title(); ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php wp_reset_postdata(); ?>
                                <?php endwhile; ?>
                            </div>
                        </div>
                        <div class="col-lg-12 col-xs-12">
                            <div class="row">
                                <div class="comment-fb">
                                    <!--<div id="fb-root"></div>-->
                                    <script>(function (d, s, id) {
                                            var js, fjs = d.getElementsByTagName(s)[0];
                                            if (d.getElementById(id)) return;
                                            js = d.createElement(s);
                                            js.id = id;
                                            js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.12&appId=1262722123757247&autoLogAppEvents=1';
                                            fjs.parentNode.insertBefore(js, fjs);
                                        }(document, 'script', 'facebook-jssdk'));</script>

                                    <div class="comment-fb-like">
                                        <?php _e('[:en]Like to receive the latest information from Trekvn[:vi]Bấm like để nhận thông tin mới nhất từ Trekvn[:]') ?>
                                        <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/"
                                             data-layout="button" data-action="like" data-size="small"
                                             data-show-faces="false"
                                             data-share="false"></div>
                                    </div>
                                    <div class="comment-fb-place">
                                        <div id="fb-root"></div>
                                        <script>(function (d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id)) return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.12&appId=148991269073677&autoLogAppEvents=1';
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>
                                        <div class="fb-comments"
                                             data-href="<?php echo home_url($wp->request);?>"
                                             data-width="100%" data-numposts="5"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php else: ?>
    <section class=" col-lg-9  col-xs-12 col-md-9 col-sm-12">
        <div class="row">
            <div class="entertainment-page-content news-page col-lg-12 col-xs-12">
                <div class="row">
                    <article class="entertainment-main col-lg-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="row">
                                    <div class="entertainment-image">
                                        <!--                                    <a href="#" title="#">-->
                                        <?php woocommerce_template_loop_product_thumbnail(); ?>
                                        <!--                                    </a>-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div class="row">
                                    <div class="entertainment-content">
                                        <div class="entertainment-content-label">
                                            <!--                                        <a href="#" title="#">Góc review chi tiết kinh nghiệm vui chơi tại Bangkok - Thái Lan</a>-->
                                            <?php the_title() ?>
                                            <div class="short-description">
                                                <?php
                                                $dia_diem = get_field('dia_diem', get_the_ID());
                                                $thoi_han = get_field('dia_diem', get_the_ID());
                                                $tg_khoi_hanh = get_field('tg_khoi_hanh', get_the_ID());
                                                $phay = '';
                                                if (trim($dia_diem) != '' && trim($thoi_han)) {
                                                    $phay = ', ';
                                                }
                                                ?>
                                                <span class="short-information">
                                                <?php echo $dia_diem . $phay . $thoi_han; ?>
                                            </span>
                                                <?php
                                                $product = wc_get_product(get_the_ID());
                                                $starEx = round($product->get_average_rating()); ?>
                                                <span class="review-icon">
                                                <?php if ($starEx):
                                                    for ($i = 0; $i < $starEx; $i++) {
                                                        ?>
                                                        <i class="fas fa-star"></i>
                                                    <?php }
                                                endif;
                                                ?>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="entertainment-content-detail">
                                            <div class="col-lg-9 col-xs-12">
                                                <div class="row">
                                                    <div class="entertainment-short-description">
                                                        <?php if (trim($dia_diem) != '' || trim($thoi_han) != '' || trim($tg_khoi_hanh) != ''): ?>
                                                            <h5 class="label-tour"><?php _e('[:en]Tour information[:vi]Thông tin tour[:]') ?></h5>
                                                            <?php if (trim($dia_diem) != ''): ?>
                                                                <div>Điểm
                                                                    đến: <?php echo $dia_diem ?></div>
                                                            <?php endif; ?>
                                                            <?php if (trim($thoi_han) != ''): ?>
                                                                <div>Thời
                                                                    gian: <?php echo $thoi_han ?></div>
                                                            <?php endif; ?>

                                                            <?php if (trim($tg_khoi_hanh) != ''): ?>
                                                                <div>Khởi
                                                                    hành: <?php echo $tg_khoi_hanh ?></div>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-xs-12">
                                                <div class="row">
                                                    <?php $price = get_post_meta(get_the_ID(), '_regular_price', true); ?>
                                                    <div class="price-tour"><?php echo number_format($price, 0, '.', ',') ?>
                                                        đ
                                                    </div>
                                                    <div class="button-order-tour">
                                                        <a type="submit" Lịch trình
                                                           href="<?php echo $base_url ?>/book-tour/?id=<?php echo get_the_ID() ?>"
                                                           class="btn btn-default" name="tour-book"
                                                           value="<?php echo get_the_ID() ?>">
                                                            <?php _e('[:en]Book tour[:vi]Đặt tour[:]') ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <div class="row">
                                    <div class="container-tab-quickview">
                                        <ul class="tabs-quickview">
                                            <li class="" rel="tab1"><?php _e('[:en]Schedule[:vi]Lịch trình[:]') ?></li>
                                            <li class=""
                                                rel="tab2"><?php _e('[:en]Customer reviews[:vi]Đánh giá của khách hàng[:]') ?></li>
                                        </ul>
                                        <div class="tab_container-quickview">
                                            <div class="tab1 tab_content-quickview">
                                                <div class="information-tour">
                                                    <?php the_content(); ?>
                                                </div>

                                            </div><!-- #tab2 -->
                                            <div class="tab2 tab_content-quickview">
                                                <div class="review-comment-border">
                                                    <h4 class="review-conmment-label"><?php _e('[:en]Customer reviews[:vi]Đánh giá của khách hàng[:]') ?></h4>
                                                    <ul class="review-comment-content"
                                                        id="comment_content_<?php echo get_the_ID(); ?>">
                                                        <?php
                                                        $args = array(
                                                            'status' => 'approve',
                                                            'number' => '3',
                                                            'post_id' => get_the_ID(), // use post_id, not post_ID
                                                        );
                                                        $comments = get_comments($args);
                                                        ?>
                                                        <?php foreach ($comments as $cm): ?>
                                                            <li class="review-comment-item">
                                                                <ul>

                                                                    <li class="review-comment-rating">
                                                                        <p>
                                                                            <?php $rate = ((int)get_comment_meta($cm->comment_ID, 'rating', true) * 2); ?>
                                                                            <span class="review-percent"><?php echo number_format($rate, 1); ?></span>
                                                                        </p>
                                                                        <p class="name-customer">
                                                                            <b><?php echo(trim($cm->comment_author) == '' ? _x('[:en]Tourists[:vi]Khách du lịch[:]', 'noun') : $cm->comment_author) ?></b>
                                                                        </p>
                                                                        <p class="review-date"> <?php echo($cm->comment_date) ?></p>
                                                                    </li>
                                                                    <li class="review-comment-description">
                                                                        <p>
                                                                            <?php _e($cm->comment_content) ?>
                                                                        </p>
                                                                    </li>

                                                                </ul>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                    <div class="pagination-border">
                                                        <ul class="pagination">
                                                            <?php
                                                            $total = ceil($comments_count->total_comments / 3);
                                                            $current = get_query_var('paged');
                                                            $base = isset($base) ? $base : esc_url_raw(str_replace(999999999, '%#%', remove_query_arg('add-to-cart', get_pagenum_link(999999999, false))));
                                                            $format = isset($format) ? $format : '';
                                                            echo paginate_links(apply_filters('woocommerce_pagination_args', array( // WPCS: XSS ok.
                                                                'base' => $base,
                                                                'format' => $format,
                                                                'add_args' => false,
                                                                'current' => max(1, $current),
                                                                'total' => $total,
                                                                'show_all' => false,
                                                                'prev_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                                                                'next_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                                                                'type' => 'list',
                                                                'end_size' => 2,
                                                                'mid_size' => 0,
                                                            )));
                                                            ?>
                                                        </ul>
                                                    </div>
                                                    <div class="comment_contect_product"
                                                         id="comment_contect_product_<?php echo get_the_ID() ?>">
                                                        <?php
                                                        $comments_args = array(
                                                            // Change the title of send button
                                                            'label_submit' => __('Send', 'textdomain'),
                                                            // Remove "Text or HTML to be displayed after the set of comment fields".
                                                            'comment_notes_after' => '',
                                                            // Redefine your own textarea (the comment body).
                                                            'comment_field' => '
                                                                <div class="comment-form-rating hotel_star"><label for="rating">' . _x('[:en]Your review[:vi]Đánh giá của bạn[:]', 'noun') . '</label>
                                                                    <p class="stars"><span>
                                                                    <a class="star-1" href="#">1</a>
                                                                    <a class="star-2" href="#">2</a>
                                                                    <a class="star-3" href="#">3</a>
                                                                    <a class="star-4" href="#">4</a>
                                                                    <a class="star-5" href="#">5</a>
                                                                </span>
                                                                </p>
                                                                <select name="rating" id="rating' . get_the_ID() . '" aria-required="true" required="" style="display: none;">
                                                                    <option value="">' . _x('[:en]Rank ...[:vi]Xếp hạng…[:]', 'noun') . '</option>
                                                                    <option value="5">5</option>
                                                                    <option value="4">4</option>
                                                                    <option value="3">3</option>
                                                                    <option value="2">2</option>
                                                                    <option value="1">1</option>
                                                                </select></div>
                                                                    <p class="comment-form-comment">
                                                                        <label for="comment">' . _x('Comment', 'noun') . '</label><br />
                                                                        <textarea id="comment' . get_the_ID() . '" name="comment" aria-required="true"></textarea>
                                                                    </p>
                                                                    <input type="hidden" name="my_redirect_to" id="my_redirect_to' . get_the_ID() . '" value="' . $current_url . '">',
                                                            'submit_button' => '<input name="%1$s" type="submit" id="%2$s' . get_the_ID() . '" class="%3$s" value="' . _x('[:en]Send[:vi]Gửi[:]', 'noun') . '" />',
                                                            'id_form' => 'commentform' . get_the_ID()
                                                        );
                                                        comment_form($comments_args, get_the_ID());
                                                        ?>
                                                    </div>
                                                </div>
                                            </div><!-- #tab2 -->
                                        </div> <!-- .tab_container -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <div class="related-tourist">
                        <div class="related-tourist-label"><?php _e('[:en]Maybe you are interested?[:vi]Có thể bạn quan tâm?[:]') ?></div>
                        <div class="related-list-tourist">
                            <?php


                            $arg = array(
                                'post_type' => 'product',
                                'orderby' => 'term_id',
                                'order' => 'DESC',
                                'posts_per_page' => 3,
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'product_cat',
                                        'field' => 'id',
                                        'terms' => $cat_parent_id
                                    ))
                            );
                            $r = new WP_Query($arg);
                            ?>
                            <?php while ($r->have_posts()): ?>
                                <?php $r->the_post(); ?>
                                <div class="col-lg-4 col-xs-4">
                                    <div class="row">
                                        <div class="related-item-tourist">
                                            <div class="image-item-related">
                                                <a href="<?php echo get_the_permalink() ?>">
                                                    <?php the_post_thumbnail(); ?>
                                                </a>

                                            </div>
                                            <div class="title-item-related">
                                                <a href="<?php echo get_the_permalink() ?>">
                                                    <?php the_title(); ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php wp_reset_postdata(); ?>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xs-12">
                        <div class="row">
                            <div class="comment-fb">
                                <!--<div id="fb-root"></div>-->
                                <script>(function (d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.12&appId=1262722123757247&autoLogAppEvents=1';
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));</script>

                                <div class="comment-fb-like">
                                    <?php _e('[:en]Like to receive the latest information from Trekvn[:vi]Bấm like để nhận thông tin mới nhất từ Trekvn[:]') ?>
                                    <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/"
                                         data-layout="button" data-action="like" data-size="small"
                                         data-show-faces="false"
                                         data-share="false"></div>
                                </div>
                                <div class="comment-fb-place">
                                    <div id="fb-root"></div>
                                    <script>(function (d, s, id) {
                                            var js, fjs = d.getElementsByTagName(s)[0];
                                            if (d.getElementById(id)) return;
                                            js = d.createElement(s);
                                            js.id = id;
                                            js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.12&appId=148991269073677&autoLogAppEvents=1';
                                            fjs.parentNode.insertBefore(js, fjs);
                                        }(document, 'script', 'facebook-jssdk'));</script>
                                    <div class="fb-comments"
                                         data-href="<?php echo home_url($wp->request)?>"
                                         data-width="100%" data-numposts="5"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="entertainment-page-content-info news-page col-lg-12 col-xs-12" id="search-listing-page"
                 style="display:none;">
            </div>
        </div>
    </section>
<?php endif; ?>