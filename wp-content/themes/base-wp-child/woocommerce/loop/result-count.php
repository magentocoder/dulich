<?php
/**
 * Result Count
 *
 * Shows text: Showing x - x of x results.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/result-count.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}
?>
<div class="col-lg-7 col-xs-12">
    <div class="row">
        <?php
        $location = get_locale();
        if ($total <= $per_page || -1 === $per_page) {
            /* translators: %d: total results */
            if ($location == 'vi') {
                printf(_n('Hiển thị kết quả duy nhất', 'Hiển thị tất cả<span><b> %d </b></span> kết quả', $total, 'woocommerce'), $total);
            } else {
                printf(_n('Show only results', 'Show all<span><b> %d </b></span> result', $total, 'woocommerce'), $total);
            }

        } else {
            $first = ($per_page * $current) - $per_page + 1;
            $last = min($total, $per_page * $current);
            if ($location == 'vi') {
                printf(_nx('Hiển thị kết quả duy nhất', '<span><b>%1$d</b> trong số %2$d kết quả</span>', $total, 'với kết quả đầu tiên và cuối cùng', 'woocommerce'), $last, $total);
            } else {
                printf(_nx('Show only results', '<span><b>%1$d</b> of %2$d results</span>', $total, 'with first and last result', 'woocommerce'), $last, $total);
            }

        }
        ?>
    </div>
</div>
