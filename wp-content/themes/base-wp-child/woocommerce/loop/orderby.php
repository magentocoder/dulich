<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}
$order_by = (isset($_GET['orderby']) ? $_GET['orderby'] : 'name-asc');
?>
<div class="col-lg-5 col-xs-12">
    <div class="row">
        <label class="toolbar-sort-title"><?php _e('[:en]Sort By[:vi]Sắp xếp theo[:]') ?></label>
        <div class="select-form-sort">
            <form method="get" class="woocommerce-ordering">
                <select class="form-control orderby" name="orderby">
                    <option value="name-asc" <?php selected($order_by, 'name-asc'); ?>><?php _e('[:en]Name (A - Z)[:vi]Tên (A - Z)[:]') ?></option>
                    <option value="name-desc" <?php selected($order_by, 'name-desc'); ?>><?php _e('[:en]Name (Z - A)[:vi]Tên (Z - A)[:]') ?></option>
                    <option value="price-desc" <?php selected($order_by, 'price-desc'); ?>><?php _e('[:en]Price decreases[:vi]Giá giảm dần[:]') ?></option>
                    <option value="price-asc" <?php selected($order_by, 'price-asc'); ?>><?php _e('[:en]Price increases[:vi]Giá tăng dần[:]') ?></option>
                    <option value="popularity" <?php selected($order_by, 'popularity'); ?>><?php _e('[:en]My Account[:vi]Mức độ phổ biến[:]') ?></option>
                </select>
                <input type="hidden" name="paged" value="1"/>
                <?php wc_query_string_form_fields(null, array('orderby', 'submit', 'paged', 'product-page')); ?>
            </form>
        </div>
        <a href="#" data-toggle="tooltip" title="Hooray!"><i class="fas fa-info-circle"></i></a>
    </div>
</div>
