<?php
/**
 * Wishlist page template
 *
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Wishlist
 * @version 2.0.12
 */

if (!defined('YITH_WCWL')) {
    exit;
} // Exit if accessed directly
?>

<div class="row">
    <div class="container">
        <div class="row">
            <div class="main-content col-lg-12 col-xs-12">
                <div class="row">
                    <aside class="col-lg-3 col-xs-12 ">
                        <div class="row">
                            <div class="menu-left hidden-sm hidden-xs">
                                <div class="sidebar customer-care">
                                    <div class="customer-care-call-text">
                                        <?php _e('[:en]Call for advice[:vi]Gọi điện để tư vấn[:]')?>
                                    </div>
                                    <div class="customer-number-phone">
                                        <span class="icon-telephone"><i class="fas fa-phone"></i></span>
                                        090 123 456
                                    </div>
                                    <div class="customer-care-call-text">
                                        <?php _e('[:en]Or leave a phone number Trekvn will call you[:vi]Hoặc để lại số điện thoại Trekvn sẽ gọi cho bạn[:]');?>

                                    </div>
                                    <input type="text" value="" placeholder="<?php _e('[:en]Your phone . . .[:vi]Số điện thoại của bạn...[:]')?>">
                                </div>
                                <div class="register-information">
                                    <div class="register-info-title"><?php _e('[:en]Sign up for information[:vi]Đăng ký nhận thông tin[:]')?></div>
                                    <div class="register-info-text"><?php _e('[:en]Be the first to receive exciting news and the hottest promotions[:vi]Hãy là người đầu tiên nhận được những tin
                                                tức thú vị và những chương trình khuyến mãi nóng hổi nhất[:]')?>
                                    </div>
                                    <div class="form-register-info">
                                        <?php $base_url = get_site_url(); ?>
                                        <form role="form" method="post" action="<?php echo $base_url ?>/?na=s"
                                              id="ajax-newsletter-left">
                                            <div class="form-group">
                                                <label for="name"><?php _e('[:en]Full name[:vi]Họ và tên[:]')?></label>
                                                <input type="text" class="form-control" id="name"
                                                       placeholder="<?php _e('[:en]Full name[:vi]Họ và tên[:]')?>" required name="nn">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="email" class="form-control" id="email"
                                                       placeholder="Email"  required name="ne">
                                            </div>
                                            <button type="submit"
                                                    class="btn btn-default register ajax-newsletter-left">
                                                <?php _e('[:en]Sign up[:vi]Đăng kí[:]')?>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </aside>
                    <section class="col-lg-9 col-xs-12">
                        <div class="row">
                            <div class="listing-page">
                                <div class="border-list-item-tourist">
                                    <form id="yith-wcwl-form" action="<?php echo $form_action ?>" method="post"
                                          class="woocommerce">
                                        <?php wp_nonce_field('yith-wcwl-form', 'yith_wcwl_form_nonce') ?>
                                        <table class="shop_table cart wishlist_table"
                                               data-pagination="<?php echo esc_attr($pagination) ?>"
                                               data-per-page="<?php echo esc_attr($per_page) ?>"
                                               data-page="<?php echo esc_attr($current_page) ?>"
                                               data-id="<?php echo $wishlist_id ?>"
                                               data-token="<?php echo $wishlist_token ?>">
                                            <?php $column_count = 2; ?>
                                            <tbody>
                                            <?php
                                            if (count($wishlist_items) > 0) :
                                                $added_items = array();
                                                $base_url = get_site_url();
                                                foreach ($wishlist_items as $item) :
                                                    global $product;

                                                    $item['prod_id'] = yit_wpml_object_id($item['prod_id'], 'product', true);

                                                    if (in_array($item['prod_id'], $added_items)) {
                                                        continue;
                                                    }

                                                    $added_items[] = $item['prod_id'];
                                                    $product = wc_get_product($item['prod_id']);
                                                    $availability = $product->get_availability();
                                                    $stock_status = $availability['class'];

                                                    if ($product && $product->exists()) :
                                                        ?>
                                                        <?php
                                                        $title = $product->get_title();
                                                        $cat_parent = get_the_terms($item['prod_id'], 'product_cat');
                                                        $cat_parent_id = $cat_parent[0]->term_id;
                                                        $template = (int)get_field('template', 'product_cat_' . $cat_parent_id);
                                                        $use_book_link = (int)get_field('use_book_link', 'product_cat_' . $cat_parent_id);
                                                        $book_link = get_field('book_link', 'product_cat_' . $cat_parent_id);
                                                        ?>
                                                        <article class="list-tourist col-lg-12 col-xs-12">
                                                            <div class="item-tourist ">
                                                                <div class="item-content-tourist col-lg-3 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="image-item-tour-warrap">
                                                                            <div class="image-item">
                                                                                <?php if ($use_book_link): ?>
                                                                                    <a href="<?php echo $base_url ?>/<?php echo $book_link; ?>/?id=<?php echo $item['prod_id'] ?>"
                                                                                       title="<?php the_title(); ?>">
                                                                                        <?php woocommerce_template_loop_product_thumbnail(); ?>
                                                                                    </a>
                                                                                <?php else: ?>
                                                                                    <?php woocommerce_template_loop_product_thumbnail(); ?>
                                                                                <?php endif; ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="item-list-price-tourist col-lg-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-lg-5 col-xs-12">
                                                                            <div class="row">
                                                                                <div class="item-detail-tourist">
                                                                                    <div class="item-name-tourist item-detail-sub">
                                    <span class="name-tourist">
                                    <?php echo $title; ?>
                                    </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-7 col-xs-12">
                                                                            <div class="row">
                                                                                <div class="item-detail-price-border">
                                                                                    <div class="item-detail-price-label"><?php _e('[:en]Price[:vi]Giá[:]') ?></div>
                                                                                    <?php
                                                                                    $price = get_post_meta($item['prod_id'], '_regular_price', true);
                                                                                    $sale = get_post_meta($item['prod_id'], '_sale_price', true);
                                                                                    $has_sale = false;
                                                                                    if ($sale != 0 && $sale < $price) {
                                                                                        $has_sale = true;
                                                                                    }
                                                                                    ?>

                                                                                    <div class="price-special-border">
                                                                                        <?php if ($has_sale): ?>
                                                                                            <div class="item-detail-price-special"><?php echo number_format($price, 0, '.', ','); ?>
                                                                                                đ
                                                                                            </div>
                                                                                            <div class="item-detail-price"><?php echo number_format($sale, 0, '.', ','); ?>
                                                                                                đ
                                                                                            </div>
                                                                                        <?php else: ?>
                                                                                            <div class="item-detail-price price-not-special"><?php echo number_format($price, 0, '.', ',') ?>
                                                                                                đ
                                                                                            </div>
                                                                                        <?php endif; ?>
                                                                                    </div>
                                                                                    <div class="button-order-service order-service">
                                                                                        <?php if ($use_book_link): ?>
                                                                                            <a href="<?php echo $base_url ?>/<?php echo $book_link; ?>/?id=<?php echo $item['prod_id'] ?>"
                                                                                               title=" <?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?>">
                                                                                                <div class="text-button"> <?php _e('[:en]Book[:vi]Đặt[:]'); ?></div>
                                                                                            </a>
                                                                                        <?php else: ?>
                                                                                            <a data-toggle="modal"
                                                                                               data-target="#order_<?php echo $item['prod_id']; ?>"
                                                                                               title="Đặt Xe">
                                                                                                <div class="text-button"><?php _e('[:en]Book[:vi]Đặt[:]'); ?></div>
                                                                                            </a>
                                                                                        <?php endif; ?>
                                                                                    </div>
                                                                                    <div class="button-order-service">
                                                                                        <a class="rmtowishlist"
                                                                                           data-id="<?php echo $item['prod_id'] ?>"
                                                                                           data-type="simple"
                                                                                           data-action="remove_from_wishlist">
                                                                                            <div class="text-button"><?php _e('[:en]Delete from list[:vi]Xóa khỏi danh sách[:]', 'noun') ?></div>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </article>
                                                    <?php
                                                    endif;
                                                endforeach;
                                            else: ?>
                                                <tr>
                                                    <td colspan="<?php echo esc_attr($column_count) ?>"
                                                        class="wishlist-empty"><?php echo apply_filters('yith_wcwl_no_product_to_remove_message', __('No products were added to the wishlist', 'yith-woocommerce-wishlist')) ?></td>
                                                </tr>
                                            <?php
                                            endif;

                                            if (!empty($page_links)) : ?>
                                                <tr class="pagination-row">
                                                    <td colspan="<?php echo esc_attr($column_count) ?>"><?php echo $page_links ?></td>
                                                </tr>
                                            <?php endif ?>
                                            </tbody>
                                        </table>

                                        <?php wp_nonce_field('yith_wcwl_edit_wishlist_action', 'yith_wcwl_edit_wishlist'); ?>

                                        <?php if (!$is_default): ?>
                                            <input type="hidden" value="<?php echo $wishlist_token ?>"
                                                   name="wishlist_id"
                                                   id="wishlist_id">
                                        <?php endif; ?>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        </section>
    </div>
</div>
</div>
</div>
</div>

