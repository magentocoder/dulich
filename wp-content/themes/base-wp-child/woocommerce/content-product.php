<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if (empty($product) || !$product->is_visible()) {
    return;
}
$price = get_post_meta(get_the_ID(), '_regular_price', true);
$sale = get_post_meta(get_the_ID(), '_sale_price', true);
$has_sale = false;
if ($sale != 0 && $sale < $price) {
    $has_sale = true;
}
$cat_obj = get_queried_object();
$template = (int)get_field('template', $cat_obj);
$use_book_link = (int)get_field('use_book_link', $cat_obj);
$book_link = get_field('book_link', $cat_obj);
$qty = $product->get_stock_quantity();
global $wp;
$current_url = home_url($wp->request).'?comment=s';
$base_url = get_site_url();
$default_wishlists = is_user_logged_in() ? YITH_WCWL()->get_wishlists(array('is_default' => true)) : false;
if (!empty($default_wishlists)) {
    $default_wishlist = $default_wishlists[0]['ID'];
} else {
    $default_wishlist = false;
}

// exists in default wishlist
$exists = YITH_WCWL()->is_product_in_wishlist(get_the_ID(), $default_wishlist);


if ($exists) {
    $action = 'remove_from_wishlist';
    $la = _x('[:en]Delete from list[:vi]Xóa khỏi danh sách[:]', 'noun');
    $cl = 'rmtowishlist';
} else {
    $action = 'add_to_wishlist';
    $la = _x('[:en]Save to after[:vi]Lưu vào để sau[:]', 'noun');
    $cl = 'addtowishlist';
}
?>
<?php if ($template == 0): ?>
    <article class="list-tourist col-lg-12 col-xs-12">

        <div class="item-tourist ">
            <div class="item-content-tourist col-lg-3 col-xs-12">
                <div class="row">
                    <div class="image-item-tour-warrap">
                        <div class="image-item">
                            <?php if ($use_book_link): ?>
                                <a href="<?php echo $base_url ?>/<?php echo $book_link; ?>/?id=<?php echo get_the_ID() ?>"
                                   title="<?php the_title(); ?>">
                                    <?php woocommerce_template_loop_product_thumbnail(); ?>
                                </a>
                            <?php else: ?>
                                <?php woocommerce_template_loop_product_thumbnail(); ?>
                            <?php endif; ?>
                        </div>
                        <div class="wishlist <?php echo $cl ?>" data-id="<?php echo get_the_ID() ?>" data-type="simple"
                             data-action="<?php echo $action; ?>">
                            <i class="fa fa-heart <?php echo($exists ? 'active' : '') ?>" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-list-price-tourist col-lg-9 col-xs-12">
                <div class="row">
                    <div class="col-lg-6 col-xs-12">
                        <div class="row">
                            <div class="item-detail-tourist">
                                <div class="item-name-tourist item-detail-sub">

                                    <?php if ($use_book_link): ?>
                                        <a href="<?php echo $base_url ?>/<?php echo $book_link; ?>/?id=<?php echo get_the_ID() ?>"
                                           title="<?php the_title(); ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    <?php else: ?>
                                        <?php the_title(); ?>
                                    <?php endif; ?>
                                </div>
                                <?php
                                $address = get_field('address', get_the_ID());
                                $phone_number = get_field('phone_number', get_the_ID());
                                $email = get_field('email', get_the_ID());
                                $website = get_field('website', get_the_ID());
                                ?>
                                <?php if (trim($address) != ''): ?>
                                    <div class="item-address item-detail-sub hidden-sm hidden-xs">
                                        <i class="fa fa-map-marker icon" aria-hidden="true"></i>
                                        <?php echo $address ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (trim($phone_number) != ''): ?>
                                    <div class="item-phone-number item-detail-sub hidden-sm hidden-xs">
                                        <span class="glyphicon glyphicon-phone-alt icon"></span>
                                        <?php echo $phone_number ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (trim($email) != ''): ?>
                                    <div class="item-email item-detail-sub hidden-sm hidden-xs">
                                        <i class="fa fa-envelope icon" aria-hidden="true"></i>
                                        <?php echo $email; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (trim($website) != ''): ?>
                                    <div class="item-website item-detail-sub hidden-sm hidden-xs">
                                        <span class="glyphicon glyphicon-globe icon"></span></i>
                                        <?php echo $website ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="row">
                            <div class="item-detail-price-border">
                                <div class="item-detail-price-label"><?php _e('[:en]Price[:vi]Giá[:]') ?></div>
                                <?php woocommerce_template_loop_price(); ?>
                                <div class="review">
                                    <?php
                                    $avg_rate = $product->get_average_rating() * 2;
                                    $avg_rate = round($avg_rate, 1);
                                    ?>
                                    <span class="review-percent"><?php echo number_format($avg_rate, 1); ?></span>
                                    <?php $comments_count = wp_count_comments(get_the_ID()); ?>
                                    <span class="review-text">(<b><?php echo (int)$comments_count->total_comments; ?></b> <?php _e('[:en]comment[:vi]nhận xét[:]') ?>
                                        )</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-6 col-xs-6">
                                <div class="row">
                                    <div class="button-order-service order-service">
                                        <?php if ($use_book_link): ?>
                                            <a href="<?php echo $base_url ?>/<?php echo $book_link; ?>/?id=<?php echo get_the_ID() ?>"
                                               title=" <?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?>">
                                                <div class="text-button"> <?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?></div>
                                            </a>
                                        <?php else: ?>
                                            <a data-toggle="modal" data-target="#order_<?php echo get_the_ID(); ?>"
                                               title=" <?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?>">
                                                <div class="text-button"> <?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?></div>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xs-6">
                                <div class="row">
                                    <div class="button-order-service">
                                        <div class="text-button" onclick="showQuickView(this.id)"
                                             id="quickview-<?php echo get_the_ID(); ?>">
                                            <?php _e('[:en]Read more[:vi]Xem thêm[:]'); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="quickview col-lg-12 col-xs-12" data-index="quickview-<?php echo get_the_ID(); ?>">
                <div class="row">
                    <div class="container-tab-quickview quickview-mobile">

                        <ul class="tabs-quickview hidden-xs hidden-sm">
                            <li class="" rel="tab1"><?php _e('[:en]Image[:vi]Hình ảnh[:]') ?></li>
                            <li class="" rel="tab2"><?php _e('[:en]Details[:vi]Thông tin chi tiết[:]') ?></li>
                            <li rel="tab3"><?php _e('[:en]Map[:vi]Bản đồ[:]') ?></li>
                            <li rel="tab4" class="comment_tab_product"
                                id="<?php echo get_the_ID();?>"
                            data-url="<?php echo $current_url?>"
                            ><?php _e('[:en]Comment[:vi]Nhận xét[:]') ?></li>
                        </ul>
                        <div class="tab_container-quickview">
                            <button class="accordion-quickview hidden-lg hidden-md">Hình ảnh</button>
                            <div class="tab1 tab_content-quickview panel-quickview show-image">

                                <div class="show-image">
                                    <?php
                                    $i = 0;
                                    $attachment_ids = $product->get_gallery_attachment_ids(); ?>

                                    <div class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item <?php echo $active; ?>">
                                                <?php woocommerce_template_loop_product_thumbnail(); ?>
                                            </div>
                                            <?php if (!empty($attachment_ids)): ?>
                                                <?php foreach ($attachment_ids as $attachment_id): ?>
                                                    <?php
                                                    $image_link = wp_get_attachment_url($attachment_id);
                                                    $active = (($i == 0 ? ' active' : ''));
                                                    $i++;
                                                    ?>
                                                    <div class="item <?php echo $active; ?>">
                                                        <img src="<?php echo $image_link; ?>" alt="" title="#">
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                        <?php if (!empty($attachment_ids)): ?>
                                            <a class="right carousel-control"
                                               href="#carousel-simple-<?php echo get_the_ID() ?>" role="button"
                                               data-slide="next">
                                                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                            </a>
                                        <?php endif; ?>
                                    </div>

                                </div>


                            </div><!-- #tab1 -->
                            <button class="accordion-quickview hidden-lg hidden-md">Thông tin chi tiết</button>
                            <div class="tab2 tab_content-quickview panel-quickview">
                                <div class="information-company">
                                    <?php echo get_field('thong_tin_dai_ly', get_the_ID()); ?>
                                </div>

                            </div><!-- #tab2 -->
                            <button class="accordion-quickview hidden-lg hidden-md">Bản đồ</button>
                            <div class="tab3 tab_content-quickview panel-quickview">
                                <div class="place-map">
                                    <div class="map-place">
                                        <?php if (trim(get_field('google_map_url', get_the_ID())) != ''): ?>
                                            <iframe src="<?php echo get_field('google_map_url', get_the_ID()); ?>"
                                                    width="100%" height="300px" frameborder="0" style="border:0"
                                                    allowfullscreen></iframe>
                                        <?php endif; ?>
                                    </div>
                                    <div class="info-hotel-wappar">
                                        <?php $thong_tin = get_field('thong_tin_gioi_thieu_ve_vi_tri', get_the_ID()); ?>
                                        <?php if (trim($thong_tin) != ''): ?>
                                            <h4 class="info-hotel"><?php _e('[:en]Hotel Information[:vi]Thông tin khách sạn[:]') ?></h4>
                                            <div class="info-hotel-content">
                                                <ul>
                                                    <?php
                                                    $thong_tin = explode(PHP_EOL, $thong_tin);
                                                    $size = ceil(count($thong_tin) / 4);
                                                    $thong_tin = array_chunk($thong_tin, $size);
                                                    ?>
                                                    <?php foreach ($thong_tin as $tins): ?>
                                                        <li class="info-hotel-columns">
                                                            <ul class="">
                                                                <?php foreach ($tins as $tin): ?>
                                                                    <li class=""><a href="#"
                                                                                    title="#"><?php _e($tin) ?></a>
                                                                    </li>                                                        </li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div><!-- #tab3 -->
                            <button class="accordion-quickview hidden-lg hidden-md">Nhận xét</button>
                            <div class="tab4 tab_content-quickview panel-quickview review-mobile">
                                <div class="review-comment-border">
                                    <h4 class="review-conmment-label"><?php _e('[:en]Customer Reviews[:vi]Nhận xét của khách hàng[:]') ?></h4>
                                    <ul class="review-comment-content" id="comment_content_<?php echo get_the_ID();?>">
                                        <?php
                                        $args = array(
                                            'status' => 'approve',
                                            'number' => '3',
                                            'post_id' => get_the_ID(), // use post_id, not post_ID
                                        );
                                        $comments = get_comments($args);
                                        ?>
                                        <?php foreach ($comments as $cm): ?>
                                            <li class="review-comment-item">
                                                <ul>

                                                    <li class="review-comment-rating">
                                                        <p>
                                                            <?php $rate = ((int)get_comment_meta($cm->comment_ID, 'rating', true) * 2); ?>
                                                            <span class="review-percent"><?php echo number_format($rate, 1); ?></span>
                                                        </p>
                                                        <p class="name-customer">
                                                            <b><?php echo(trim($cm->comment_author) == '' ? _x('[:en]Tourists[:vi]Khách du lịch[:]', 'noun') : $cm->comment_author) ?></b>
                                                        </p>
                                                        <p class="review-date"> <?php echo($cm->comment_date) ?></p>
                                                    </li>
                                                    <li class="review-comment-description">
                                                        <p>
                                                            <?php _e($cm->comment_content) ?>
                                                        </p>
                                                    </li>

                                                </ul>
                                            </li>
                                        <?php endforeach; ?>

                                    </ul>
                                    <div class="pagination-border">
                                        <ul class="pagination">
                                            <?php
                                            $total = ceil($comments_count->total_comments/3);
                                            $current = get_query_var('paged');
                                            $base = isset($base) ? $base : esc_url_raw(str_replace(999999999, '%#%', remove_query_arg('add-to-cart', get_pagenum_link(999999999, false))));
                                            $format = isset($format) ? $format : '';
                                            echo paginate_links(apply_filters('woocommerce_pagination_args', array( // WPCS: XSS ok.
                                                'base' => $base,
                                                'format' => $format,
                                                'add_args' => false,
                                                'current' => max(1, $current),
                                                'total' => $total,
                                                'show_all'           => false,
                                                'prev_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                                                'next_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                                                'type' => 'list',
                                                'end_size' => 2,
                                                'mid_size' => 1,
                                            )));
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="comment_contect_product" id="comment_contect_product_<?php echo get_the_ID()?>">

                                    </div>
                                </div>
                            </div><!-- #tab4 -->

                        </div> <!-- .tab_container -->
                        <div class="button-close"
                             onclick="closeQuickView(this)"><?php _e('[:en]Close[:vi]Đóng[:]') ?></div>
                    </div>
                </div>
            </div>
            <div id="order_<?php echo get_the_ID(); ?>" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title"><?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?></h3>
                        </div>
                        <div class="modal-body">
                            <h4 class="modal-title"><?php the_title(); ?></h4>
                            <p><?php echo get_field('address', get_the_ID()); ?></p>
                            <p><?php echo get_field('phone_number', get_the_ID()); ?></p>
                            <p><?php echo get_field('email', get_the_ID()); ?></p>
                            <p><?php echo get_field('website', get_the_ID()); ?></p>
                            <?php echo do_shortcode('[contact-form-7 id="292" title="Đặt dịch vụ"]') ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal"><?php _e('[:en]Close[:vi]Đóng[:]') ?></button>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <?php if ($has_sale): ?>
            <div class="sales-percent">-<?php echo (int)(100 - ($sale * 100 / $price)) ?>%</div>
        <?php endif; ?>
    </article>
<?php elseif ($template == 1): ?>
    <article class="list-tourist col-lg-12 col-xs-12">
        <div class="item-tourist ">
            <div class="item-content-tourist col-lg-3 col-xs-12">
                <div class="row">
                    <div class="image-item-tour-warrap">
                        <div class="image-item">
                            <?php if ($use_book_link): ?>
                                <a href="<?php echo $base_url ?>/<?php echo $book_link; ?>/?id=<?php echo get_the_ID() ?>"
                                   title="<?php the_title(); ?>">
                                    <?php woocommerce_template_loop_product_thumbnail(); ?>
                                </a>
                            <?php else: ?>
                                <?php woocommerce_template_loop_product_thumbnail(); ?>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="item-list-price-tourist col-lg-9 col-xs-12">
                <div class="row">
                    <div class="col-lg-5 col-xs-12">
                        <div class="row">
                            <div class="item-detail-tourist">
                                <div class="item-name-tourist item-detail-sub">
                                    <span class="name-tourist">
                                        <?php if ($use_book_link): ?>
                                            <a href="<?php echo $base_url ?>/<?php echo $book_link; ?>/?id=<?php echo get_the_ID() ?>"
                                               title="<?php the_title(); ?>">
                                   <?php the_title(); ?>
                                </a>
                                        <?php else: ?>
                                            <?php the_title(); ?>
                                        <?php endif; ?>

                                    </span>
                                </div>
                                <?php
                                $address = get_field('address', get_the_ID());
                                $phone_number = get_field('phone_number', get_the_ID());
                                $email = get_field('email', get_the_ID());
                                $website = get_field('website', get_the_ID());
                                ?>
                                <?php if (trim($address) != ''): ?>
                                    <div class="item-address item-detail-sub hidden-sm hidden-xs">
                                        <i class="fa fa-map-marker icon" aria-hidden="true"></i>
                                        <?php echo $address ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (trim($phone_number) != ''): ?>
                                    <div class="item-phone-number item-detail-sub hidden-sm hidden-xs">
                                        <span class="glyphicon glyphicon-phone-alt icon"></span>
                                        <?php echo $phone_number ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (trim($email) != ''): ?>
                                    <div class="item-email item-detail-sub hidden-sm hidden-xs">
                                        <i class="fa fa-envelope icon" aria-hidden="true"></i>
                                        <?php echo $email; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (trim($website) != ''): ?>
                                    <div class="item-website item-detail-sub hidden-sm hidden-xs">
                                        <span class="glyphicon glyphicon-globe icon"></span></i>
                                        <?php echo $website ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 col-xs-12">
                        <div class="row">
                            <div class="item-detail-price-border">
                                <div class="item-detail-price-label"><?php _e('[:en]Price[:vi]Giá[:]') ?></div>
                                <?php woocommerce_template_loop_price(); ?>
                                <div class="review"><?php
                                    $avg_rate = $product->get_average_rating() * 2;
                                    $avg_rate = round($avg_rate, 1);
                                    ?>
                                    <span class="review-percent"><?php echo number_format($avg_rate, 1); ?></span>
                                    <?php $comments_count = wp_count_comments(get_the_ID()); ?>
                                    <span class="review-text">(<b><?php echo (int)$comments_count->total_comments; ?></b> <?php _e('[:en]comment[:vi]nhận xét[:]') ?>
                                        )</span>
                                </div>
                                <div class="button-order-service order-service">
                                    <?php if ($use_book_link): ?>
                                        <a href="<?php echo $base_url ?>/<?php echo $book_link; ?>/?id=<?php echo get_the_ID() ?>"
                                           title=" <?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?>">
                                            <div class="text-button"> <?php _e('[:en]Order transport[:vi]Đặt Xe[:]'); ?></div>
                                        </a>
                                    <?php else: ?>
                                        <a data-toggle="modal" data-target="#order_<?php echo get_the_ID(); ?>"
                                           title="Đặt Xe">
                                            <div class="text-button"><?php _e('[:en]Book a car[:vi]Đặt Xe[:]'); ?></div>
                                        </a>
                                    <?php endif; ?>
                                </div>

                                <div class="button-order-service">
                                    <a class="<?php echo $cl ?>" data-id="<?php echo get_the_ID() ?>" data-type="simple"
                                       data-action="<?php echo $action; ?>">
                                        <div class="text-button"><?php echo $la; ?></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="order_<?php echo get_the_ID(); ?>" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title"><?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?></h3>
                        </div>
                        <div class="modal-body">
                            <h4 class="modal-title"><?php the_title(); ?></h4>
                            <p><?php echo get_field('address', get_the_ID()); ?></p>
                            <p><?php echo get_field('phone_number', get_the_ID()); ?></p>
                            <p><?php echo get_field('email', get_the_ID()); ?></p>
                            <p><?php echo get_field('website', get_the_ID()); ?></p>
                            <?php echo do_shortcode('[contact-form-7 id="292" title="Đặt dịch vụ"]') ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal"><?php _e('[:en]Close[:vi]Đóng[:]') ?></button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <?php if ($qty < 5): ?>
            <!--            <div class="sales-percent quantity-low">Số lượng ít</div>-->
        <?php elseif ($qty > 15): ?>
            <!--            <div class="sales-percent customer-like">Khách hàng ưu thích</div>-->
        <?php endif; ?>
    </article>
<?php elseif ($template == 2): ?>
    <?php
    $thoi_han = get_field('thoi_han', get_the_ID());
    $dia_diem = get_field('dia_diem', get_the_ID());
    $phay = '';
    if (trim($thoi_han) != '' && trim($dia_diem) != '') {
        $phay = ', ';
    }
    $star = get_field('rank', get_the_ID());
    $starEx = round($product->get_average_rating());
    ?>
    <article class="entertainment-main col-lg-12 col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="row">
                    <div class="entertainment-image">
                        <a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
                            <?php woocommerce_template_loop_product_thumbnail(); ?>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="row">
                    <div class="entertainment-content">
                        <div class="entertainment-content-label">
                            <a href="<?php echo get_permalink(); ?>" title="<?php the_title() ?>">
                                <?php //echo get_permalink(); ?><!--" title="#">-->
                                <?php the_title() ?>
                            </a>
                            <div class="short-description">
                                <span class="short-information">
                                    <?php echo $dia_diem . $phay . $thoi_han; ?>
                                </span>
                                <span class="review-icon">
                                    <?php if ($starEx):
                                        for ($i = 0; $i < $starEx; $i++) {
                                            ?>
                                            <i class="fas fa-star"></i>
                                        <?php }
                                    endif;
                                    ?>
                                </span>
                            </div>
                        </div>
                        <div class="entertainment-content-detail">
                            <div class="col-lg-9 col-xs-12">
                                <div class="row">
                                    <div class="entertainment-short-description">
                                        <?php the_excerpt() ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="row">
                                    <div class="price-tour"><?php echo number_format($price, 0, '.', ',') ?> đ</div>
                                    <div class="button-order-tour">
                                        <a type="submit"
                                           href="<?php echo $base_url ?>/<?php echo $book_link; ?>/?id=<?php echo get_the_ID() ?>"
                                           class="btn btn-default" name="tour-book" value="<?php echo get_the_ID() ?>">
                                            <?php _e('[:en]Book tour[:vi]Đặt tour[:]') ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
<?php elseif ($template == 3): ?>
    <article class="list-tourist col-lg-12 col-xs-12">
        <div class="item-tourist ">
            <div class="item-content-tourist col-lg-4 col-xs-12">
                <div class="row">
                    <div class="image-item-tour-warrap">
                        <div class="image-item">
                            <a href="<?php echo get_permalink(); ?>"
                               title="<?php the_title(); ?>">
                                <?php the_post_thumbnail(); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-list-price-tourist col-lg-8 col-xs-12">
                <div class="row">
                    <div class="company-information">
                        <div class="company-label">
                            <a href="<?php echo get_permalink(); ?>"
                               title="<?php the_title(); ?>"><?php the_title(); ?>
                            </a>
                        </div>
                        <div class="company-description">
                            <?php
                            $short_description = $product->post_excerpt;
                            if (strlen($short_description) > 100) {
                                $short_description = mb_substr($short_description, 0, 100) . '...';
                            }
                            echo($short_description);
                            ?>
                        </div>
                        <div class="button-view-detail">
                            <a href="<?php echo get_permalink(); ?>"
                               title="<?php the_title(); ?>">[<?php _e('[:en]See details[:vi]Xem chi tiế t[:]')?>]</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
<?php endif; ?>
<!-- <li <?php post_class(); ?>> -->
<?php
/**
 * woocommerce_before_shop_loop_item hook.
 *
 * @hooked woocommerce_template_loop_product_link_open - 10
 */
// do_action( 'woocommerce_before_shop_loop_item' );

/**
 * woocommerce_before_shop_loop_item_title hook.
 *
 * @hooked woocommerce_show_product_loop_sale_flash - 10
 * @hooked woocommerce_template_loop_product_thumbnail - 10
 */
// do_action( 'woocommerce_before_shop_loop_item_title' );

/**
 * woocommerce_shop_loop_item_title hook.
 *
 * @hooked woocommerce_template_loop_product_title - 10
 */
// do_action( 'woocommerce_shop_loop_item_title' );

/**
 * woocommerce_after_shop_loop_item_title hook.
 *
 * @hooked woocommerce_template_loop_rating - 5
 * @hooked woocommerce_template_loop_price - 10
 */
// do_action( 'woocommerce_after_shop_loop_item_title' );

/**
 * woocommerce_after_shop_loop_item hook.
 *
 * @hooked woocommerce_template_loop_product_link_close - 5
 * @hooked woocommerce_template_loop_add_to_cart - 10
 */
// do_action( 'woocommerce_after_shop_loop_item' );
?>
<!-- </li> -->
