<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package base-wp
 */
$cat_obj = get_queried_object();
$is_transport_template =  (int)get_field('template',$cat_obj);
$filter_template =  (int)get_field('template_filter',$cat_obj);
global $wp;
$current_url = home_url($wp->request);
$base_url = get_site_url();
$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$is_hotel = strpos($actual_link,"hotel");
$is_giaitri = strpos($actual_link,"giai-tri");
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/image/logo.png" rel="shortcut icon" type="image/x-icon" />
    <?php wp_head(); ?>
</head>
<style type="text/css">
    input[type='checkbox']{
        margin-right: 5px;
    }
</style>
<body <?php body_class(); ?>>

<div class="container-fluid">
    <div class="row">
        <div class="warrp-border-header col-lg-12">
            <div class="row">
                <header class="header-warrap hidden-md">
                    <div class="container">
                        <div class="row">
                            <div class="top-link col-lg-6">
                                <?php $user = wp_get_current_user(); ?>
                                <?php if((!is_user_logged_in()) || (is_user_logged_in() && !in_array('author',$user->roles))): ?>
                                    <div class="account-daily">
                                        <div class="selectric daily-guess">
                                            <span class="label-account">
                                                <?php _e('[:en]Merchant registration[:vi]Đăng kí đại lý[:]')?>
                                            </span>
                                            <i class="fas fa-angle-down"></i>
                                        </div>
                                        <div class="selectric-items daily-guess-item" tabindex="-1" style="display: none">
                                            <div class="selectric-scroll">
                                                <div class="border-login">
                                                    <div class="close-login"></div>
                                                    <div class="login-dl-form">
                                                        <h5><?php _e('[:en]Merchant login[:vi]Đăng nhập đại lý[:]')?></h5>
                                                        <form role="form" action="" class="form-logindl" id="form-login-ajax-company" method="post">
                                                            <div class="form-group">
                                                                <label for="InputEmail1"><?php _e('[:en]Merchant email[:vi]Địa chỉ email đăng ký đại lý[:]')?></label>
                                                                <input type="email"  name="log" class="form-control"  required id="InputEmail1" placeholder="Enter email">
                                                            </div>
                                                            <div class="form-group password">
                                                                <label for="InputPassword1"><?php _e('[:en]Password[:vi]Mật khẩu[:]')?></label>
                                                                <input type="password" name="pwd" required class="form-control" id="InputPassword1" placeholder="Password">
                                                                <input type="hidden" name="redirect_to" value="<?php echo $current_url;?>" class="redirect_to"/>
                                                                <input type="hidden" name="action" value="ajax_login" />
                                                                <input type="hidden" name="type" value="company" />
                                                                <?php wp_nonce_field( 'ajax-login-nonce', 'security_login' ); ?>
                                                            </div>
                                                            <div class="forgot-password-border">
                                                                <a href="<?php echo $base_url; ?>/forgot-password" title="#" class="forgot-password"><?php _e('[:en]Forgot password[:vi]Quên mật khẩu[:]')?></a>
                                                            </div>
                                                            <div class="login-button">
                                                                <div class="button-login-form button-login">
                                                                    <button type="submit" class="btn btn-default"><?php _e('[:en]Login[:vi]Đăng nhập[:]')?></button>
                                                                </div>
                                                                <div class="login-button-register button-login">
                                                                    <span><?php _e('[:en]You don\'t have account?[:vi]Bạn chưa có tài khoản?[:]')?></span>
                                                                    <br><?php _e('[:en]Let\'s[:vi]Hãy [:]')?> <span class="redirect-register"><a href="<?php echo $base_url?>/dang-ky-dai-ly" title="#"><?php _e('[:en]register a merchant account[:vi]Đăng ký tài khoản đại lý[:]')?></a></span>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <div class="dieukhoan-border">
                                                            <?php _e('[:en]When you create account, you must agree with[:vi]Qua việc đăng nhập tài khoản, bạn đồng ý với các [:]')?>
                                                            <a href="#" title="#" class=""><?php _e('[:en]Privacy[:vi]Điều khoản và điều kiện[:]')?></a>
                                                            <?php _e('[:en]and[:vi]cũng như[:]')?>
                                                            <a href="#" title="#" class=""><?php _e('[:en]Policy[:vi]Chính sách An toàn và Bảo mật[:]')?></a>
                                                            <?php _e('[:en][:vi]của chúng tôi[:]')?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if (is_user_logged_in()): ?>
                                    <div class="link-wishlist">
                                        <a href="<?php echo $base_url?>/wishlist" title="Mục yêu thích"><?php _e('[:en]My favorite[:vi]Mục yêu thích[:]')?></a>
                                    </div>
                                <?php endif;?>
                                <div class="header-account">

                                        <?php if (is_user_logged_in()):?>
                                            <div class="selectric customer">
                                                <span class="label-account"><?php _e('[:en]My Account[:vi]Tài khoản của tôi[:]')?></span><i
                                                        class="fas fa-angle-down"></i>
                                            </div>
                                            <div class="selectric-items customer-item" tabindex="-1" style="display: none">
                                                <div class="selectric-scroll">
                                                    <ul>
                                                        <li data-index="0" class="selected highlighted">
                                                            <a href="<?php echo wp_logout_url($current_url); ?> " title="<?php _e('[:en]Logout[:vi]Đăng xuất[:]')?>">
                                                                <span title=""><?php _e('[:en]Logout[:vi]Đăng xuất[:]')?></span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        <?php else:?>
                                            <div class="selectric guess">
                                            <span class="label-account"><?php _e('[:en]My Account[:vi]Tài khoản của tôi[:]')?></span><i
                                                    class="fas fa-angle-down"></i>
                                            </div>
                                        <?php endif;?>

                                    <div class="selectric-items guess-item" tabindex="-1" style="display: none">
                                        <div class="selectric-scroll">
                                            <div class="border-login">
                                                <div class="close-login"></div>
                                                <div class="login-form">
                                                    <h5><?php _e('[:en]Login[:vi]Đăng nhập tài khoản[:]')?></h5>
                                                    <form role="form" action="" class="form-login" id="form-login-ajax" method="post">
                                                        <div class="form-group">
                                                            <label for="InputEmail1"><?php _e('[:en]Phone number or email address[:vi]Số điện thoại hoặc địa chỉ email[:]')?></label>
                                                            <input type="email"  name="log" class="form-control"  required id="InputEmail1" placeholder="Enter email">
                                                        </div>
                                                        <div class="form-group password">
                                                            <label for="InputPassword1"><?php _e('[:en]Password Trekvn.com[:vi]Mật khẩu Trekvn.com[:]')?></label>
                                                            <input type="password" name="pwd" required class="form-control" id="InputPassword1" placeholder="Password">
                                                            <input type="hidden" name="redirect_to" value="<?php echo $current_url;?>" class="redirect_to"/>
                                                            <input type="hidden" name="action" value="ajax_login" />
                                                            <?php wp_nonce_field( 'ajax-login-nonce', 'security_login' ); ?>
                                                        </div>
                                                        <div class="forgot-password-border">
                                                            <a href="<?php echo $base_url; ?>/forgot-password" title="#" class="forgot-password"><?php _e('[:en]Forgot Password[:vi]Quên mật khẩu[:]')?></a>
                                                        </div>
                                                        <div class="login-button">
                                                            <div class="button-login-form button-login">
                                                                <button type="submit" class="btn btn-default"><?php _e('[:en]Login[:vi]Đăng nhập[:]')?></button>
                                                            </div>
                                                            <div class="login-button-register button-login">
                                                                    <span><?php _e('[:en]You don\'t have account?[:vi]Bạn chưa có tài khoản?[:]')?></span>
                                                                    <br><?php _e('[:en]Let\'s[:vi]Hãy [:]')?> <span class="redirect-register"><a href="<?php echo $base_url?>/dang-ky-dai-ly" title="#"><?php _e('[:en]register a merchant account[:vi]Đăng ký tài khoản đại lý[:]')?></a></span>
                                                                </div>
                                                        </div>
                                                    </form>
                                                    <div class="dieukhoan-border">
                                                            <?php _e('[:en]When you create account, you must agree with[:vi]Qua việc đăng nhập tài khoản, bạn đồng ý với các [:]')?>
                                                            <a href="#" title="#" class=""><?php _e('[:en]Privacy[:vi]Điều khoản và điều kiện[:]')?></a>
                                                            <?php _e('[:en]and[:vi]cũng như[:]')?>
                                                            <a href="#" title="#" class=""><?php _e('[:en]Policy[:vi]Chính sách An toàn và Bảo mật[:]')?></a>
                                                            <?php _e('[:en][:vi]của chúng tôi[:]')?>
                                                        </div>
                                                </div>
                                                <div class="register-form">
                                                    <h5><?php _e('[:en]Regiter an account[:vi]Đăng ký tài khoản[:]')?></h5>
                                                    <div class="border-form-register">
                                                        <form role="form" method="post" action="" class="form-login" id="reg-form-ajax">
                                                            <div class="form-group">
                                                                <label ><?php _e('[:en]Email address[:vi]Địa chị email[:]')?><span class="required">*</span></label>
                                                                <input type="email" required class="form-control" name="user_email" id="dk_user_email" placeholder="Enter email">
                                                                <input type="hidden" class="form-control" name="user_login" placeholder="Enter email" id="dk_user_login">
                                                            </div>
                                                            <div class="form-group password">
                                                                <label ><?php _e('[:en]Password[:vi]Mật khẩu[:]')?><span class="required">*</span></label>
                                                                <input type="hidden" required name="redirect_to" value="<?php echo $current_url;?>" class="redirect_to"/>
                                                                <input type="password" name="user_pass" class="form-control"  placeholder="Password">
                                                                <input type="hidden" name="action" value="reg_new_user_ajax" />
                                                            </div>
                                                            <div class="form-group password">
                                                                <label ><?php _e('[:en]Confirm password[:vi]Nhập lại mật khẩu[:]')?><span class="required">*</span></label>
                                                                <input type="password"  required class="form-control" name="pass1_re"  placeholder="Password">
                                                            </div>
                                                            <div class="login-button">
                                                                <div class="login-button-register button-login">
                                                                    <span><?php _e('[:en]When you register, you need to agree with[:vi]Chọn đăng ký bạn đồng ý với các [:]')?></span>
                                                                    <a href="<?php echo $base_url; ?>/forgot-password" title="#" class=""><?php _e('[:en]Privacy and Policy[:vi]Điều khoản dịch vụ [:]')?></a>
                                                                    <?php _e('[:en]of Trekvn[:vi]của Trekvn [:]')?>
                                                                </div>
                                                                <div class="button-login-form button-login">
                                                                    <button type="submit" class="btn btn-default"><?php _e('[:en]Register[:vi]Đăng ký [:]')?></button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="langague">
                                    <?php

                                    $current_language = get_bloginfo('language');
                                    if ($current_language == 'vi') {
                                        $label = 'VND';
                                        $img = '/image/Flag-of-VN.ico';
                                    } else {
                                        $label = 'EN';
                                        $img = '/image/british-flag.jpg';
                                    }
                                    ?>
                                    <div class="selectric">
                                            <span class="label-language">
                                                <img src="<?php echo get_stylesheet_directory_uri() . $img; ?>" alt="#"
                                                     title="#" class="image-logo">
                                                <?php echo $label; ?>
                                            </span>
                                        <i class="fas fa-angle-down"></i>
                                    </div>
                                    <div class="selectric-items" tabindex="-1" style="display: none">
                                        <div class="selectric-scroll">
                                            <ul>
                                                <li data-index="0" class="selected highlighted">
                                                    <a href="<?php echo $base_url .DIRECTORY_SEPARATOR . 'vi'; ?>" title="VND">
                                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/Flag-of-VN.ico" alt="#" title="#" class="image-logo">
                                                        <span class="">VND</span>
                                                    </a>
                                                </li>
                                                <li data-index="1" class="last">
                                                    <a href="<?php echo $base_url .DIRECTORY_SEPARATOR . 'en'; ?>" title="EN">
                                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/british-flag.jpg" alt="#" title="#" class="image-logo">
                                                        <span class="">EN</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="header-logo col-lg-6">
                                <a href="<?php echo $base_url;?>">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/Logo_Trekvn.png" alt="Trekvn" title="Trekvn" class="logo-image">
                                </a>
                            </div>
                        </div>
                    </div>
                </header>
                <nav>
                    <div id="mod-mobile-menu" class="hidden-md hidden-lg">
                        <div id="pullMenu" class=""><i class="fas fa-align-justify"></i> </div>
                        <div class="mini-logo-wappar">
                            <a href="<?php echo $base_url;?>" class="mini-logo" title="Trekvn">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/Logo_Trekvn.png" title="Trekvn">
                            </a>
                        </div>
                        <?php if (!is_user_logged_in()):?>
                        <div class="icon-my-account">
                            <div class="selectric link-login-mobile">
                                <i class="far fa-user-circle"></i>
                            </div>
                            <div class="selectric-items popup-link" tabindex="-1" style="display: none">
                                <div class="selectric-scroll">
                                    <ul>
                                        <li class="selected login-customer">
                                            <a href="<?php echo $base_url.'/dang-nhap-mobile?redirect_url='.$current_url?>" title="My account">
                                                <span title=""><?php _e('[:en]Login[:vi]Đăng nhập[:]')?></span>
                                            </a>
                                        </li>
                                        <li class="selected login-agent">
                                            <a href="<?php echo $base_url.'/dang-nhap-mobile-dai-ly?redirect_url='.$current_url?>" title="<?php _e('[:en]Login Agent[:vi]Đăng nhập đại lý[:]')?>">
                                                <span title=""><?php _e('[:en]Login Agent[:vi]Đăng nhập đại lý[:]')?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <?php endif;?>
                        <div class="clearfix"></div>
                         <?php wp_nav_menu(array(
                            'theme_location' => 'primary',
                            'menu' => '',
                            'container' => 'div',
                            'container_class' => '',
                            'container_id' => 'combineMenu',
                            'menu_class' => 'mobileMain',
                            'menu_id' => 'menu-main-menu',
                            'echo' => true,
                            'fallback_cb' => 'wp_page_menu',
                            'before' => '', 'after' => '',
                            'link_before' => '',
                            'link_after' => '',
                            'items_wrap' => '<div id="closeMenu"></div><ul id="%1$s" class="%2$s">%3$s</ul>',
                            'item_spacing' => 'preserve',
                            'depth' => 0,
                            'walker' => '',
                        )); ?>
                    </div>
                    <div id="module-mainmenu">
                        <?php wp_nav_menu(array(
                            'theme_location' => 'primary',
                            'menu' => '',
                            'container' => 'div',
                            'container_class' => 'container',
                            'container_id' => '',
                            'menu_class' => 'myMenu',
                            'menu_id' => 'menu-main-menu-1',
                            'echo' => true,
                            'fallback_cb' => 'wp_page_menu',
                            'before' => '', 'after' => '',
                            'link_before' => '',
                            'link_after' => '',
                            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            'item_spacing' => 'preserve',
                            'depth' => 0,
                            'walker' => '',
                        )); ?>
                    </div>
                </nav>
                <div class="search-warrap">
                    <div class="container">
                        <h4 class="label-search"><?php _e('[:en]Share the link at [:vi]Chia sẻ kết nối đam mê tại [:]')?><a href="<?php echo $base_url;?>" title="#"><span class="trekvn"><b>Trekvn</b></span><span class="com">.com</span></a></h4>
                        <h6><?php _e('[:en]City, place or hotel name:[:vi]Thành phố, địa điểm hoặc tên khách sạn:[:]')?></h6>
                        <div class="topSearch">
                            <div id="formsearchtop">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fas fa-search"></i></div>
                                        <?php
                                        $list = array(0 => '-hotel', 1 => '-transport', 2 => '-food', 3 => '-company', 4 => '-tour', 5 => '-ticket',6=> '-giaitri');
                                        if($is_giaitri){ $name = $list[6]; }
                                        elseif ($is_transport_template==1){ $name = $list[1]; }
                                        elseif ($filter_template==3){ $name = $list[5]; }
                                        elseif($filter_template==0){ $name = $list[0];}
                                        elseif ($filter_template==1){ $name = $list[2]; }
                                        elseif ($filter_template==2 && $is_transport_template ==0){ $name = $list[3]; }
                                        elseif ($filter_template==2){ $name = $list[4]; }
                                        else{$name = '';}
                                        ?>
                                         <input id="search-text<?php echo $name; ?>" style="width: 93%;height: 41px;border-radius: 0 3px 3px 0;" autocomplete="off" type="text" class="textbox txtTopSearch" placeholder="<?php _e('[:en]For example: Phu Quoc[:vi]Ví dụ: Phú Quốc[:]')?>" list="browsers"/>
                                        <select id="btn-search-dropdown">
                                            <option value=""><span><?php _e('[:en]Search by[:vi]Tìm kiếm theo[:]') ?></span></option>
                                            <option value="hotel"><span><?php _e('[:en]Restaurant[:vi]Khách sạn[:]') ?></span></option>
                                            <option value="car"><span><?php _e('[:en]Transport[:vi]Xe[:]') ?></span></option>
                                            <option value="giaitri"><span><?php _e('[:en]Entertainment[:vi]Giải trí[:]') ?></span></option>
                                            <option value="food"><?php _e('[:en]Food[:vi]Ăn uống[:]') ?></option>
                                            <option value="company"><?php _e('[:en]Company[:vi]Công ty du lịch[:]') ?></option>
                                            <option value="tour"><?php _e('[:en]Travel Tour[:vi]Tour du lịch[:]') ?></option>
                                            <option value="ticket"><?php _e('[:en]Ticket[:vi]Vé máy bay[:]') ?></option>
                                        </select>
                                         <input id="btn-search<?php echo $name; ?>" style="border: none;line-height: inherit;" type="button" class="btn btnTopSearch" value="Tìm"/>
                                    </div>
                                </div>
                                <div  id="ajax_suggest">
                                </div>
                                <script type="text/javascript">
                                    jQuery(document).ready(function ($){
                                        $('input[type="checkbox"]').dblclick(function(){
                                            var $thischeckbox = $(this);
                                            $thischeckbox.prop('checked',false);
                                        });
                                        $(".price").click(function(){
                                            var $thischeckbox = $(this);
                                            $(".price").prop('checked',false);
                                            $thischeckbox.prop('checked',true);
                                        });
                                        $(".rate").click(function(){
                                            var $thischeckbox = $(this);
                                            $(".rate").prop('checked',false);
                                            $thischeckbox.prop('checked',true);
                                        });
                                        $(".rank").click(function(){
                                            var $thischeckbox = $(this);
                                            $(".rank").prop('checked',false);
                                            $thischeckbox.prop('checked',true);
                                        });
                                        $(".loai_co_so").click(function(){
                                            var $thischeckbox = $(this);
                                            $(".loai_co_so").prop('checked',false);
                                            $thischeckbox.prop('checked',true);
                                        });
                                        $(".food").click(function(){
                                            var $thischeckbox = $(this);
                                            $(".food").prop('checked',false);
                                            $thischeckbox.prop('checked',true);
                                        });
                                        $(".fit").click(function(){
                                            var $thischeckbox = $(this);
                                            $(".fit").prop('checked',false);
                                            $thischeckbox.prop('checked',true);
                                        });
                                        $(".food_price").click(function(){
                                            var $thischeckbox = $(this);
                                            $(".food_price").prop('checked',false);
                                            $thischeckbox.prop('checked',true);
                                        });
                                        $(".dong_xe").click(function(){
                                            var $thischeckbox = $(this);
                                            $(".dong_xe").prop('checked',false);
                                            $thischeckbox.prop('checked',true);
                                        });
                                        $(".loai_xe").click(function(){
                                            var $thischeckbox = $(this);
                                            $(".loai_xe").prop('checked',false);
                                            $thischeckbox.prop('checked',true);
                                        });
                                        $(".xep_hang_xe").click(function(){
                                            var $thischeckbox = $(this);
                                            $(".xep_hang_xe").prop('checked',false);
                                            $thischeckbox.prop('checked',true);
                                        });
                                        $(".giai_tri").click(function(){
                                            var $thischeckbox = $(this);
                                            $(".giai_tri").prop('checked',false);
                                            $thischeckbox.prop('checked',true);
                                        });
                                        $(".lam_dep").click(function(){
                                            var $thischeckbox = $(this);
                                            $(".lam_dep").prop('checked',false);
                                            $thischeckbox.prop('checked',true);
                                        });
                                        $(".mua_sam").click(function(){
                                            var $thischeckbox = $(this);
                                            $(".mua_sam").prop('checked',false);
                                            $thischeckbox.prop('checked',true);
                                        });
                                        $(".kieu_thue").click(function(){
                                            var $thischeckbox = $(this);
                                            $(".kieu_thue").prop('checked',false);
                                            $thischeckbox.prop('checked',true);
                                        });

                                        var timkiem='<?=(!empty($_GET['timkiem']))?$_GET['timkiem']:'null'; ?>';
                                        var timkiemVanchuyen='<?=(!empty($_GET['timkiem-vanchuyen']))?$_GET['timkiem-vanchuyen']:'null'; ?>';
                                        var timkiemAnuong='<?=(!empty($_GET['timkiem-anuong']))?$_GET['timkiem-anuong']:'null'; ?>';
                                        var timkiemKhachsan='<?=(!empty($_GET['timkiem-khachsan']))?$_GET['timkiem-khachsan']:'null'; ?>';
                                        var timkiemTour='<?=(!empty($_GET['timkiem-tour']))?$_GET['timkiem-tour']:'null'; ?>';
                                        var timkiemCompany='<?=(!empty($_GET['timkiem-company']))?$_GET['timkiem-company']:'null'; ?>';
                                        var timkiemTicket='<?=(!empty($_GET['timkiem-ve']))?$_GET['timkiem-ve']:'null'; ?>';
                                        var timkiemGiaitri='<?=(!empty($_GET['timkiem-giaitri']))?$_GET['timkiem-giaitri']:'null'; ?>';
                                        if(timkiem !== 'null'){
                                            $("#main-container").hide();
                                            $("#search-text").val(timkiem);
                                            var url = '<?php echo get_site_url(); ?>/search-custom.php';
                                            $.ajax({
                                                    'type':'post',
                                                    'url':  url,
                                                    'data'  : {
                                                        's':timkiem
                                                    },
                                                    success : function(response) {
                                                        $("#main-container").hide();
                                                        $("#ajax_suggest").hide();
                                                        $("#search-custom").show();
                                                        if($('#old-listing-page').length){
                                                            var res = $(response);
                                                            $('#old-listing-page').find('.border-list-item-tourist').replaceWith(res);
                                                        }else{
                                                            var result = $('<div />').append(response).find('#search-result').html();
                                                            $("#custom-search-result").html(result);
                                                        }

                                                    }
                                                }
                                            );
                                            var city = $("#city").val();
                                            var url1 = '<?php echo get_site_url(); ?>/address-ajax.php';
                                            if(city !== 'Thành Phố') {
                                                $.ajax({
                                                        'type':'post',
                                                        'url':  url1,
                                                        'data'  : {
                                                            'city':city
                                                        },
                                                        success : function(response) {
                                                            var result = $('<div />').append(response).find('#district').html();
                                                            $("#district").html(result);
                                                        }
                                                    }
                                                );
                                            }
                                        }
                                        if(timkiemKhachsan !== 'null'){
                                            $("#main-container").hide();
                                            $("#search-text-hotel").val(timkiemKhachsan);
                                            var url = '<?php echo get_site_url(); ?>/search-hotel-custom.php';
                                            $.ajax({
                                                    'type':'post',
                                                    'url':  url,
                                                    'data'  : {
                                                        'timkiem-khachsan':timkiemKhachsan
                                                    },
                                                    success : function(response) {
                                                        $("#main-container").hide();
                                                        $("#ajax_suggest").hide();
                                                        $("#search-custom").show();
                                                        $(".pagination").hide();
                                                        $(".review-comment-border").find('ul.pagination').show();
                                                        $(".toolbar.hidden-sm.hidden-xs").hide();
                                                        if($('#old-listing-page').length){
                                                            var res = $(response);
                                                            $('#old-listing-page').find('.border-list-item-tourist').replaceWith(res);
                                                        }else{
                                                            var result = $('<div />').append(response).find('#search-result').html();
                                                            $("#custom-search-result").html(result);
                                                        }

                                                    }
                                                }
                                            );
                                            var city = $("#city").val();
                                            var url1 = '<?php echo get_site_url(); ?>/address-ajax.php';
                                            if(city !== 'Thành Phố') {
                                                $.ajax({
                                                        'type':'post',
                                                        'url':  url1,
                                                        'data'  : {
                                                            'city':city
                                                        },
                                                        success : function(response) {
                                                            var result = $('<div />').append(response).find('#district').html();
                                                            $("#district").html(result);
                                                        }
                                                    }
                                                );
                                            }
                                        }
                                        if(timkiemVanchuyen !== 'null'){
                                            $("#search-text-transport").val(timkiemVanchuyen);
                                            var url = '<?php echo get_site_url(); ?>/search-transport-custom.php';
                                            $.ajax({
                                                    'type':'post',
                                                    'url':  url,
                                                    'data'  : {
                                                        's':timkiemVanchuyen
                                                    },
                                                    success : function(response) {
                                                        $("#main-container").hide();
                                                        $(".note-bottom").hide();
                                                        $(".pagination").hide();
                                                        $(".review-comment-border").find('ul.pagination').show();
                                                        $("#ajax_suggest").hide();
                                                        $("#search-custom").show();
                                                        $(".toolbar.hidden-sm.hidden-xs").hide();
                                                        if($('#old-listing-page').length){
                                                            var res = $(response);
                                                            $('#old-listing-page').find('.border-list-item-tourist').replaceWith(res);
                                                        }else{
                                                            var result = $('<div />').append(response).find('#search-result').html();
                                                            $("#custom-search-result").html(result);
                                                        }

                                                    }
                                                }
                                            );
                                            var city = $("#city").val();
                                            var url1 = '<?php echo get_site_url(); ?>/address-ajax.php';
                                            if(city !== 'Thành Phố') {
                                                $.ajax({
                                                        'type':'post',
                                                        'url':  url1,
                                                        'data'  : {
                                                            'city':city
                                                        },
                                                        success : function(response) {
                                                            var result = $('<div />').append(response).find('#district').html();
                                                            $("#district").html(result);
                                                        }
                                                    }
                                                );
                                            }
                                        }
                                        if(timkiemAnuong !== 'null'){
                                            $("#search-text-food").val(timkiemAnuong);
                                            var url = '<?php echo get_site_url(); ?>/search-food-custom.php';
                                            $.ajax({
                                                    'type':'post',
                                                    'url':  url,
                                                    'data'  : {
                                                        's':timkiemAnuong
                                                    },
                                                    success : function(response) {
                                                        $("#main-container").hide();
                                                        $(".note-bottom").hide();
                                                        $("#ajax_suggest").hide();
                                                        $(".pagination").hide();
                                                        $(".review-comment-border").find('ul.pagination').show();
                                                        $("#search-custom").show();
                                                        $(".toolbar.hidden-sm.hidden-xs").hide();
                                                        if($('#old-listing-page').length){
                                                            var res = $(response);
                                                            $('#old-listing-page').find('.border-list-item-tourist').replaceWith(res);
                                                        }else{
                                                            var result = $('<div />').append(response).find('#search-result').html();
                                                            $("#custom-search-result").html(result);
                                                        }

                                                    }
                                                }
                                            );
                                            var city = $("#city").val();
                                            var url1 = '<?php echo get_site_url(); ?>/address-ajax.php';
                                            if(city !== 'Thành Phố') {
                                                $.ajax({
                                                        'type':'post',
                                                        'url':  url1,
                                                        'data'  : {
                                                            'city':city
                                                        },
                                                        success : function(response) {
                                                            var result = $('<div />').append(response).find('#district').html();
                                                            $("#district").html(result);
                                                        }
                                                    }
                                                );
                                            }
                                        }
                                        if(timkiemTour !== 'null'){
                                            $("#search-text-tour").val(timkiemTour);
                                            var url = '<?php echo get_site_url(); ?>/search-tour-custom.php';
                                            $.ajax({
                                                    'type':'post',
                                                    'url':  url,
                                                    'data'  : {
                                                        's':timkiemTour
                                                    },
                                                    success : function(response) {
                                                        $("#main-container").hide();
                                                        $(".toolbar.hidden-sm.hidden-xs").hide();
                                                        $(".note-bottom").hide();
                                                        $("#ajax_suggest").hide();
                                                        $(".pagination").hide();
                                                        $(".review-comment-border").find('ul.pagination').show();
                                                        $(".entertainment-page-content").remove();
                                                        $("#search-listing-page").show();
                                                        if($('#old-listing-page').length){
                                                            var res = $(response);
                                                            $('#old-listing-page').find('.border-list-item-tourist').replaceWith(res);
                                                        }else{
                                                            var result = $('<div />').append(response).find('#search-result').html();
                                                            $("#search-listing-page").html(result);
                                                        }

                                                    }
                                                }
                                            );
                                            var city = $("#city").val();
                                            var url1 = '<?php echo get_site_url(); ?>/address-ajax.php';
                                            if(city !== 'Thành Phố') {
                                                $.ajax({
                                                        'type':'post',
                                                        'url':  url1,
                                                        'data'  : {
                                                            'city':city
                                                        },
                                                        success : function(response) {
                                                            var result = $('<div />').append(response).find('#district').html();
                                                            $("#district").html(result);
                                                        }
                                                    }
                                                );
                                            }
                                        }
                                        if(timkiemCompany !== 'null'){
                                            $("#search-text-company").val(timkiemCompany);
                                            var url = '<?php echo get_site_url(); ?>/search-company-custom.php';
                                            $.ajax({
                                                    'type':'post',
                                                    'url':  url,
                                                    'data'  : {
                                                        's':timkiemCompany
                                                    },
                                                    success : function(response) {
                                                        $(".toolbar.hidden-sm.hidden-xs").hide();
                                                        $(".note-bottom").hide();
                                                        $("#main-container").hide();
                                                        $("#ajax_suggest").hide();
                                                        $(".pagination").hide();
                                                        $(".review-comment-border").find('ul.pagination').show();
                                                        $(".entertainment-page-content").remove();
                                                        $("#search-listing-page").show();
                                                        if($('#old-listing-page').length){
                                                            var res = $(response);
                                                            $('#old-listing-page').find('.border-list-item-tourist').replaceWith(res);
                                                        }else{
                                                            var result = $('<div />').append(response).find('#search-result').html();
                                                            $("#search-listing-page").html(result);
                                                        }

                                                    }
                                                }
                                            );
                                            var city = $("#city").val();
                                            var url1 = '<?php echo get_site_url(); ?>/address-ajax.php';
                                            if(city !== 'Thành Phố') {
                                                $.ajax({
                                                        'type':'post',
                                                        'url':  url1,
                                                        'data'  : {
                                                            'city':city
                                                        },
                                                        success : function(response) {
                                                            var result = $('<div />').append(response).find('#district').html();
                                                            $("#district").html(result);
                                                        }
                                                    }
                                                );
                                            }
                                        }
                                        if(timkiemTicket !== 'null'){
                                            $("#main-container").hide();
                                            $("#search-text-ticket").val(timkiemTicket);
                                            var url = '<?php echo get_site_url(); ?>/search-ticket-custom.php';
                                            $.ajax({
                                                    'type':'post',
                                                    'url':  url,
                                                    'data'  : {
                                                        'timkiem-ve':timkiemTicket
                                                    },
                                                    success : function(response) {
                                                        var result = $('<div />').append(response).find('#search-result').html();
                                                        $(".toolbar.hidden-sm.hidden-xs").hide();
                                                        $(".note-bottom").hide();
                                                        $("#main-container").hide();
                                                        $("#ajax_suggest").hide();
                                                        $("#search-custom").show();
                                                        $(".pagination").hide();
                                                        $(".review-comment-border").find('ul.pagination').show();
                                                        $("#old-listing-page").remove();
                                                        $("#search-listing-page").html(result);
                                                        $("#search-listing-page").show();

                                                    }
                                                }
                                            );
                                            var city = $("#city").val();
                                            var url1 = '<?php echo get_site_url(); ?>/address-ajax.php';
                                            if(city !== 'Thành Phố') {
                                                $.ajax({
                                                        'type':'post',
                                                        'url':  url1,
                                                        'data'  : {
                                                            'city':city
                                                        },
                                                        success : function(response) {
                                                            var result = $('<div />').append(response).find('#district').html();
                                                            $("#district").html(result);
                                                        }
                                                    }
                                                );
                                            }
                                        }
                                        if(timkiemGiaitri !== 'null'){
                                            $("#main-container").hide();
                                            $("#search-text-giaitri").val(timkiemGiaitri);
                                            var url = '<?php echo get_site_url(); ?>/search-entertainment-custom.php';
                                            $.ajax({
                                                    'type':'post',
                                                    'url':  url,
                                                    'data'  : {
                                                        'timkiem-giaitri':timkiemGiaitri
                                                    },
                                                    success : function(response) {
                                                        var result = $('<div />').append(response).find('#search-result').html();
                                                        $(".toolbar.hidden-sm.hidden-xs").hide();
                                                        $(".note-bottom").hide();
                                                        $("#main-container").hide();
                                                        $("#ajax_suggest").hide();
                                                        $("#search-custom").show();
                                                        $(".pagination").hide();
                                                        $(".review-comment-border").find('ul.pagination').show();
                                                        $("#old-listing-page").remove();
                                                        $("#search-listing-page").html(result);
                                                        $("#search-listing-page").show();

                                                    }
                                                }
                                            );
                                            var city = $("#city").val();
                                            var url1 = '<?php echo get_site_url(); ?>/address-ajax.php';
                                            if(city !== 'Thành Phố') {
                                                $.ajax({
                                                        'type':'post',
                                                        'url':  url1,
                                                        'data'  : {
                                                            'city':city
                                                        },
                                                        success : function(response) {
                                                            var result = $('<div />').append(response).find('#district').html();
                                                            $("#district").html(result);
                                                        }
                                                    }
                                                );
                                            }
                                        }
                                        $('#search-text').keypress(function(e) {
                                            if(e.which == 10 || e.which == 13) {
                                                    var textSearch = $(".txtTopSearch").val();
                                                    var type = $("#btn-search-dropdown").val();
                                                    if(type == 'hotel'){
                                                        window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                    }else if(type == 'car'){
                                                        window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                    }else if(type == 'food'){
                                                        window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                    }else if(type == 'giaitri'){
                                                        window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                    }else if(type == 'company'){
                                                        window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                    }else if(type == 'tour'){
                                                        window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                    }else if(type == 'ticket'){
                                                        window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-ve=';?>"+textSearch;
                                                    }else{
                                                        window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                    }
                                            }
                                        });

                                        $("#btn-search").click(
                                            function () {
                                                var textSearch = jQuery("#search-text").val();
                                                var type = $("#btn-search-dropdown").val();
                                                if(type == 'hotel'){
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }else if(type == 'car'){
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }else if(type == 'food'){
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }else if(type == 'giaitri'){
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }else if(type == 'company'){
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }else if(type == 'tour'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }else if(type == 'ticket'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }else{
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }

                                            }
                                        );
                                        $('#search-text-transport').keypress(function(e) {
                                            if(e.which == 10 || e.which == 13) {
                                                var textSearch = $("#search-text-transport").val();
                                                var type = $("#btn-search-dropdown").val();
                                                if(type == 'hotel'){
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }else if(type == 'car'){
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }else if(type == 'food'){
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }else if(type == 'giaitri'){
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }else if(type == 'company'){
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }else if(type == 'tour'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }else if(type == 'ticket'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }else{
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }
                                            }
                                        });

                                        $("#btn-search-transport").click(
                                            function () {
                                                var textSearch = $("#search-text-transport").val();
                                                var type = $("#btn-search-dropdown").val();
                                                if(type == 'hotel'){
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }else if(type == 'car'){
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }else if(type == 'food'){
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }else if(type == 'giaitri'){
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }else if(type == 'company'){
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }else if(type == 'tour'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }else if(type == 'ticket'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }else{
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }

                                            }
                                        );

                                        $("#btn-search-food").click(
                                            function () {
                                                var textSearch = $("#search-text-food").val();
                                                var type = $("#btn-search-dropdown").val();
                                                if(type == 'hotel'){
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }else if(type == 'car'){
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }else if(type == 'food'){
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }else if(type == 'giaitri'){
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }else if(type == 'company'){
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }else if(type == 'tour'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }else if(type == 'ticket'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }else{
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }
                                            }
                                        );

                                        $('#search-text-food').keypress(function(e) {
                                            if(e.which == 10 || e.which == 13) {
                                                var textSearch = $("#search-text-food").val();
                                                var type = $("#btn-search-dropdown").val();
                                                if(type == 'hotel'){
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }else if(type == 'car'){
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }else if(type == 'food'){
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }else if(type == 'giaitri'){
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }else if(type == 'company'){
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }else if(type == 'tour'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }else if(type == 'ticket'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }else{
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }
                                            }
                                        });

                                        $("#btn-search-hotel").click(
                                            function () {
                                                var textSearch = $("#search-text-hotel").val();
                                                var type = $("#btn-search-dropdown").val();
                                                if(type == 'hotel'){
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }else if(type == 'car'){
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }else if(type == 'food'){
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }else if(type == 'giaitri'){
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }else if(type == 'company'){
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }else if(type == 'tour'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }else if(type == 'ticket'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }else{
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }
                                            }
                                        );

                                        $('#search-text-hotel').keypress(function(e) {
                                            if(e.which == 10 || e.which == 13) {
                                                var textSearch = $("#search-text-hotel").val();
                                                var type = $("#btn-search-dropdown").val();
                                                if(type == 'hotel'){
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }else if(type == 'car'){
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }else if(type == 'food'){
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }else if(type == 'giaitri'){
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }else if(type == 'company'){
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }else if(type == 'tour'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }else if(type == 'ticket'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }else{
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }
                                            }
                                        });

                                        $("#btn-search-tour").click(
                                            function () {
                                                var textSearch = $("#search-text-tour").val();
                                                var type = $("#btn-search-dropdown").val();
                                                if(type == 'hotel'){
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }else if(type == 'car'){
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }else if(type == 'food'){
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }else if(type == 'giaitri'){
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }else if(type == 'company'){
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }else if(type == 'tour'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }else if(type == 'ticket'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }else{
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }
                                            }
                                        );

                                        $('#search-text-tour').keypress(function(e) {
                                            if(e.which == 10 || e.which == 13) {
                                                var textSearch = $("#search-text-tour").val();
                                                var type = $("#btn-search-dropdown").val();
                                                if(type == 'hotel'){
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }else if(type == 'car'){
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }else if(type == 'food'){
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }else if(type == 'giaitri'){
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }else if(type == 'company'){
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }else if(type == 'tour'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }else if(type == 'ticket'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }else{
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }
                                            }
                                        });

                                        $("#btn-search-company").click(
                                            function () {
                                                var textSearch = $("#search-text-company").val();
                                                var type = $("#btn-search-dropdown").val();
                                                if(type == 'hotel'){
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }else if(type == 'car'){
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }else if(type == 'food'){
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }else if(type == 'giaitri'){
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }else if(type == 'company'){
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }else if(type == 'tour'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }else if(type == 'ticket'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }else{
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }
                                            }
                                        );

                                        $('#search-text-company').keypress(function(e) {
                                            if(e.which == 10 || e.which == 13) {
                                                var textSearch = $("#search-text-company").val();
                                                var type = $("#btn-search-dropdown").val();
                                                if(type == 'hotel'){
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }else if(type == 'car'){
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }else if(type == 'food'){
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }else if(type == 'giaitri'){
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }else if(type == 'company'){
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }else if(type == 'tour'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }else if(type == 'ticket'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }else{
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }
                                            }
                                        });

                                        $("#btn-search-ticket").click(
                                            function () {
                                                var textSearch = $("#search-text-ticket").val();
                                                var type = $("#btn-search-dropdown").val();
                                                if(type == 'hotel'){
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }else if(type == 'car'){
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }else if(type == 'food'){
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }else if(type == 'giaitri'){
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }else if(type == 'company'){
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }else if(type == 'tour'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }else if(type == 'ticket'){
                                                    window.location.href="<?=$base_url.'/danh-muc/ve-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }else{
                                                    window.location.href="<?=$base_url.'/danh-muc/ve-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }
                                            }
                                        );

                                        $('#search-text-ticket').keypress(function(e) {
                                            if(e.which == 10 || e.which == 13) {
                                                var textSearch = $("#search-text-ticket").val();
                                                var type = $("#btn-search-dropdown").val();
                                                if(type == 'hotel'){
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }else if(type == 'car'){
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }else if(type == 'food'){
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }else if(type == 'giaitri'){
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }else if(type == 'company'){
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }else if(type == 'tour'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }else if(type == 'ticket'){
                                                    window.location.href="<?=$base_url.'/danh-muc/ve-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }else{
                                                    window.location.href="<?=$base_url.'/danh-muc/ve-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }
                                            }
                                        });

                                        $("#btn-search-giaitri").click(
                                            function () {
                                                var textSearch = $("#search-text-giaitri").val();
                                                var type = $("#btn-search-dropdown").val();
                                                if(type == 'hotel'){
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }else if(type == 'car'){
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }else if(type == 'food'){
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }else if(type == 'giaitri'){
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }else if(type == 'company'){
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }else if(type == 'tour'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }else if(type == 'ticket'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }else{
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }
                                            }
                                        );

                                        $('#search-text-giaitri').keypress(function(e) {
                                            if(e.which == 10 || e.which == 13) {
                                                var textSearch = $("#search-text-giaitri").val();
                                                var type = $("#btn-search-dropdown").val();
                                                if(type == 'hotel'){
                                                    window.location.href="<?=$base_url.'/danh-muc/hotel/?timkiem-khachsan=';?>"+textSearch;
                                                }else if(type == 'car'){
                                                    window.location.href="<?=$base_url.'/danh-muc/van-chuyen/?timkiem-vanchuyen=';?>"+textSearch;
                                                }else if(type == 'food'){
                                                    window.location.href="<?=$base_url.'/danh-muc/an-uong/?timkiem-anuong=';?>"+textSearch;
                                                }else if(type == 'giaitri'){
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }else if(type == 'company'){
                                                    window.location.href="<?=$base_url.'/danh-muc/cong-ty-du-lich/?timkiem-company=';?>"+textSearch;
                                                }else if(type == 'tour'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-tour=';?>"+textSearch;
                                                }else if(type == 'ticket'){
                                                    window.location.href="<?=$base_url.'/danh-muc/tour-du-lich/?timkiem-ve=';?>"+textSearch;
                                                }else{
                                                    window.location.href="<?=$base_url.'/danh-muc/giai-tri/?timkiem-giaitri=';?>"+textSearch;
                                                }
                                            }
                                        });


                                        $("#search-text").keydown(function(e) {
                                            var suggest = this.value;
                                            var url = '<?php echo get_site_url(); ?>/search-suggest.php';
                                            $.ajax({
                                                    'type':'post',
                                                    'url':  url,
                                                    'data'  : {
                                                        'suggest':suggest
                                                    },
                                                    success : function(response) {
                                                        var result = $('<div />').append(response).find('#search-result').html();
                                                        $("#ajax_suggest").html(result);
                                                        $("#ajax_suggest").show();
                                                    }
                                                }
                                            );
                                        });
                                        $("#search-list").click(function () {
                                            var city = $("#city").val();
                                            var district = $("#district").val();
                                            var commune = $("#commune").val();
                                            var price = $(".price:checked").val();
                                            var rate = $(".rate:checked").val();
                                            var rank = $(".rank:checked").val();
                                            var ten = $("#ten-khach-san").val();
                                            var url = '<?php echo get_site_url(); ?>/search-listing.php';
                                            $.ajax({
                                                    'type': 'post',
                                                    'url': url,
                                                    'data': {
                                                        'city': city,
                                                        'district': district,
                                                        'commune': commune,
                                                        'price': price,
                                                        'rate': rate,
                                                        'rank': rank,
                                                        'ten':ten
                                                    },
                                                    success: function (response) {
                                                        var result = $('<div />').append(response).find('#search-result').html();
                                                        $("#custom-search-result").html(result);
                                                        var w = window.innerWidth;
                                                        if(w < 991){
                                                            $('.menu-left-mobile .all-province-city').hide();
                                                        }
                                                    }
                                                }
                                            );
                                        });
                                        $("#city").change(function(){
                                            var city = $(this).val();
                                            var url = '<?php echo get_site_url(); ?>/address-ajax.php';
                                            if(city !== 'Thành Phố') {
                                                $.ajax({
                                                        'type':'post',
                                                        'url':  url,
                                                        'data'  : {
                                                            'city':city
                                                        },
                                                        success : function(response) {
                                                            var result = $('<div />').append(response).find('#district').html();
                                                            $("#district").html(result);
                                                        }
                                                    }
                                                );
                                            }
                                        });
                                        $("#district").change(function(){
                                            var district = $(this).val();
                                            var url = '<?php echo get_site_url(); ?>/address-ajax.php';
                                            if(district !== 'Quận - Huyện'){
                                                $.ajax({
                                                        'type':'post',
                                                        'url':  url,
                                                        'data'  : {
                                                            'district':district
                                                        },
                                                        success : function(response) {
                                                            var result = $('<div />').append(response).find('#commnune').html();
                                                            $("#commune").html(result);
                                                        }
                                                    }
                                                );
                                            }

                                        });
                                    });
                                </script>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <script>
            var ADMIN_AJAX = '<?php echo admin_url('admin-ajax.php');?>';
            var CURRENT_URL = '<?php echo $current_url;?>';
        </script>
        <?php if(!is_front_page()): ?>
            <div class="container">
                <div class="row">
                    <div id="breadcrumb" class="container_12 breadcrumb custome_breadcrumb">
                        <?php bcdonline_breadcrumbs(); ?>
                    </div>
                </div>
            </div>
        <?php endif;?>