<?php
// Template Name: Forgot Password
get_header(); ?>
<?php
$errors = '';
if (isset($_POST['forgot_pass_Sbumit'])) {
    if (isset($_POST['email_forgot']) && empty($_POST['email_forgot']))
        $errors = "Email không để trống";
    else {
        $emailToreceive = $_POST['email_forgot'];
        $user_input = esc_sql(trim($emailToreceive));
        if ($user_input == '') {
            $errors = "Email không để trống";
        } else {
            $user_data = get_user_by('email', $user_input);
            if (empty($user_data)) {
                $errors = 'Địa chỉ mail không đúng';
            } else {
                if (kv_forgot_password_reset_email($user_data->user_email)) {
                    $success['reset_email'] = "Chúng tôi vừa gửi cho bạn email làm mới mật khẩu.";
                } else {
                    $errors = "Email bị lỗi";
                }
            }
        }
    }
}
function kv_forgot_password_reset_email($user_input)
{
    global $wpdb;
    $user_data = get_user_by('email', $user_input);
    $user_login = $user_data->user_login;
    $user_email = $user_data->user_email;

    $key = $wpdb->get_var("SELECT user_activation_key FROM $wpdb->users WHERE user_login ='" . $user_login . "'");
    if (empty($key)) {
        $key = wp_generate_password(20, false);
        $wpdb->update($wpdb->users, array('user_activation_key' => $key), array('user_login' => $user_login));
    }

    $message = __('Ai đó đã yêu cầu đặt lại mật khẩu cho tài khoản sau:') . "<br><br><br>";
    $message .= get_option('siteurl') . "<br><br>";
    $message .= sprintf(__('Tài khoản: %s'), $user_login) . "<br><br><br>";
    $message .= __('Nếu đây là lỗi, chỉ cần bỏ qua email này vì sẽ không có hành động nào được thực hiện.') . "<br><br>";
    $message .= __('Để đặt lại mật khẩu của bạn, hãy truy cập vào địa chỉ sau:') . "<br><br>";
    $message .= '<a href="' . tg_validate_url() . "action=reset_pwd&key=$key&login=" . rawurlencode($user_login) . '" > ' . tg_validate_url() . "action=reset_pwd&key={$key}&login=" . rawurlencode($user_login) . "</a><br><br>";
    if ($message && !wp_mail($user_email, 'Password Reset Request', $message)) {
        $msg = false;
    } else {
        $msg = true;
    }

    return $msg;
}

function tg_validate_url()
{
    $base_url = get_site_url();
    return $base_url . '/forgot-password?';
}

if (isset($_GET['key']) && $_GET['action'] == "reset_pwd") {
    $reset_key = $_GET['key'];
    $user_login = $_GET['login'];
    $user_data = $wpdb->get_row("SELECT ID, user_login, user_email FROM $wpdb->users WHERE user_activation_key = '" . $reset_key . "' AND user_login = '" . $user_login . "'");
    $user_login = $user_data->user_login;
    $user_email = $user_data->user_email;
    if (!empty($reset_key) && !empty($user_data)) {
        if (kv_rest_setting_password($reset_key, $user_login, $user_email, $user_data->ID)) {
            $errors['emailError'] = "Send email fail";
        } else {
            $redirect_to = get_site_url();
            wp_safe_redirect($redirect_to);
            exit();
        }
    } else {
        exit('Key không đúng');
    };
}
function kv_rest_setting_password($reset_key, $user_login, $user_email, $ID)
{

    $new_password = wp_generate_password(7, false); //you can change the number 7 to whatever length needed for the new password
    wp_set_password($new_password, $ID); //mailing the reset details to the user

    $message = __('Mật khẩu mới:') . "<br><br>";
    $message .= get_bloginfo('name') . "<br><br>";
    $message .= sprintf(__('Tài khoản: %s'), $user_login) . "<br><br>";
    $message .= sprintf(__('Mật khẩu: %s'), $new_password) . "<br><br>";
    if ( $message && !wp_mail($user_email, 'Mật khẩu mới.', $message) ) {
        $msg = false; 
     } else {
        $msg = true; 
        $redirect_to = get_site_url();
        wp_safe_redirect($redirect_to);
        exit();
    } 

    return $msg; 
}

?>
<div style="color: red">
    <?php echo $errors; ?>
</div>
<form role="form" action="" class="form-forgot-password" method="post">
    <div class="form-group">
        <label for="email_forgot"><?php _e('[:en]Please input email[:vi]Xin mời nhập email[:]') ?></label>
        <input type="email" name="email_forgot" class="form-control" required id="email_forgot"
               placeholder="Enter email">
    </div>
    <div class="login-button">
        <div class="button-login-form button-login">
            <button type="submit"
                    class="btn btn-default"><?php _e('[:en]Reset Password[:vi]Tạo mới mật khẩu[:]') ?></button>
        </div>
    </div>
    <input type="hidden" name="forgot_pass_Sbumit" value="kv_yes">
</form>
<?php
get_footer(); ?>
