<?php
// Template Name: Blog page
get_header(); ?>
    <main class="col-xs-12 col-xs-12 listing-page-border ">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="blog-page ">
                        <div class="blog-picture col-lg-12 col-xs-12">
                            <div class="row">
                                <div class="blog-picture-top">
                                    <?php
                                    $id = 32;
                                    $img_url = get_field('image', 'category_' . $id);
                                    $link = get_category_link($id);
                                    $name = get_cat_name($id);
                                    ?>
                                    <article class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <div class="row">
                                            <div class="blog-picture-top-border">
                                                <div class="blog-picure-image">
                                                    <img src="<?php echo $img_url ?>" title="<?php echo $name ?>"
                                                         alt="<?php echo $name ?>">
                                                </div>
                                                <div class="blog-picture-image-background"></div>
                                                <div class="view-button">
                                                    <a href="<?php echo $link; ?>" title="#"><?php echo $name ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <?php
                                    $id = 33;
                                    $img_url = get_field('image', 'category_' . $id);
                                    $link = get_category_link($id);
                                    $name = get_cat_name($id);
                                    ?>
                                    <article class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <div class="row">
                                            <div class="blog-picture-top-border">
                                                <div class="blog-picure-image">
                                                    <img src="<?php echo $img_url ?>" title="<?php echo $name ?>"
                                                         alt="<?php echo $name ?>">
                                                </div>
                                                <div class="blog-picture-image-background"></div>
                                                <div class="view-button">
                                                    <a href="<?php echo $link; ?>" title="#"><?php echo $name ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <?php
                                    $id = 34;
                                    $img_url = get_field('image', 'category_' . $id);
                                    $link = get_category_link($id);
                                    $name = get_cat_name($id);
                                    ?>
                                    <article class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <div class="row">
                                            <div class="blog-picture-top-border">
                                                <div class="blog-picure-image">
                                                    <img src="<?php echo $img_url ?>" title="<?php echo $name ?>"
                                                         alt="<?php echo $name ?>">
                                                </div>
                                                <div class="blog-picture-image-background"></div>
                                                <div class="view-button">
                                                    <a href="<?php echo $link; ?>" title="#"><?php echo $name ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <?php
                                    $id = 35;
                                    $img_url = get_field('image', 'category_' . $id);
                                    $link = get_category_link($id);
                                    $name = get_cat_name($id);
                                    ?>
                                    <article class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <div class="row">
                                            <div class="blog-picture-top-border">
                                                <div class="blog-picure-image">
                                                    <img src="<?php echo $img_url ?>" title="<?php echo $name ?>"
                                                         alt="<?php echo $name ?>">
                                                </div>
                                                <div class="blog-picture-image-background"></div>
                                                <div class="view-button">
                                                    <a href="<?php echo $link; ?>" title="#"><?php echo $name ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <div class="blog-picture-bottom">
                                    <?php
                                    $arg = array(
                                        'posts_per_page' => 5,
                                        'orderby' => 'term_id',
                                        'order' => 'DESC',
                                        'cat' => 36,
                                        'limit' => 5,
                                        'meta_query' => [
                                            [
                                                'key' => 'noi_bat',
                                                'value' => 1,
                                                'compare' => '='
                                            ]
                                        ]
                                    );
                                    $r = new WP_Query($arg);
                                    $i = 1;
                                    ?>

                                    <?php if ($r->have_posts()): ?>
                                        <?php foreach ($r->posts as $p): ?>
                                            <?php
                                            $id = $p->ID;
                                            $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'single-post-thumbnail');
                                            $title = $p->post_title;
                                            $link = get_permalink($id);
                                            $post_date = date('d/m/Y', strtotime($p->post_date));
                                            $comment_count = $p->comment_count;
                                            ?>
                                            <?php if ($i == 1): ?>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="row">
                                                        <article class="blog-picture-left-border">
                                                            <div class="blog-picure-image">
                                                                <a href="<?php echo $link ?>"
                                                                   title="<?php echo $title ?>"
                                                                   alt="<?php echo $title ?>">
                                                                    <img src="<?php echo $image[0]; ?>"
                                                                         title="<?php echo $title ?>"
                                                                         alt="<?php echo $title ?>">
                                                                </a>

                                                            </div>
                                                            <div class="blog-picture-left-content">
                                                                <div class="blog-left-label">
                                                                    <a href="<?php echo $link ?>"
                                                                       title="<?php echo $title ?>"><?php echo $title ?></a>
                                                                </div>
                                                                <div class="blog-left-date-comment">
                                                                    <span class="blog-left-date"><?php echo $post_date ?></span>
                                                                    <span class="blog-left-comment"><?php echo (int)$comment_count; ?>
                                                                        comment</span>
                                                                </div>
                                                            </div>
                                                        </article>
                                                    </div>
                                                </div>
                                            <?php else: ?>
                                                <?php if ($i == 2): ?>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="row">
                                                    <div class="blog-picture-right">
                                                <?php endif; ?>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="row">
                                                        <article class="blog-picture-sub">
                                                            <div class="blog-picure-image">
                                                                <a href="<?php echo $link ?>"
                                                                   title="<?php echo $title ?>"
                                                                   alt="<?php echo $title ?>">
                                                                    <img src="<?php echo $image[0]; ?>"
                                                                         title="<?php echo $title ?>"
                                                                         alt="<?php echo $title ?>">
                                                                </a>
                                                            </div>
                                                            <div class="blog-picture-left-content">
                                                                <div class="blog-left-label">
                                                                    <a href="<?php echo $link ?>"
                                                                       title="<?php echo $title ?>"><?php echo $title ?></a>
                                                                </div>
                                                                <div class="blog-left-date-comment">
                                                                    <span class="blog-left-date"><?php echo $post_date ?></span>
                                                                    <span class="blog-left-comment"><?php echo (int)$comment_count; ?>
                                                                        comment</span>
                                                                </div>
                                                            </div>
                                                        </article>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <?php $i++; ?>
                                        <?php endforeach; ?>
                                        <?php if ($r->post_count > 1): ?>
                                            </div>
                                            </div>
                                            </div>
                                        <?php endif; ?>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="row">
                                <div class="article-information">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="row">
                                            <article class="article-new">
                                                <div class="article-label"><?php _e('[:en]New post[:vi]Bài viết mới nhất[:]')?></div>
                                                <ul class="list-article">
                                                    <?php
                                                    $arg = array(
                                                        'posts_per_page' => 2,
                                                        'orderby' => 'term_id',
                                                        'order' => 'DESC',
                                                        'cat' => 36,
                                                        'limit' => 2
                                                    );
                                                    $r = new WP_Query($arg);
                                                    $i = 1;
                                                    ?>
                                                    <?php if ($r->have_posts()): ?>
                                                        <?php foreach ($r->posts as $p): ?>
                                                            <?php
                                                            $id = $p->ID;
                                                            $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'single-post-thumbnail');
                                                            $title = $p->post_title;
                                                            $link = get_permalink($id);
                                                            $post_date = date('d/m/Y', strtotime($p->post_date));
                                                            $comment_count = $p->comment_count;
                                                            $article_short = get_field('article_short', $id);
                                                            if (strlen($article_short) > 50) {
                                                                $article_short = mb_substr($article_short, 0, 50) . '...';
                                                            }
                                                            $article_by = get_field('article_by', $id);
                                                            $short_description = get_field('short_description', $id);
                                                            if (strlen($short_description) > 50) {
                                                                $short_description = mb_substr($short_description, 0, 50) . '...';
                                                            }
                                                            ?>
                                                            <li class="list-article-sub">
                                                                <ul class="article-border">
                                                                    <li class="article-image">
                                                                        <div class="article-image-border">
                                                                            <a href="<?php echo $link ?>"
                                                                               title="<?php echo $title ?>"
                                                                               alt="<?php echo $title ?>">
                                                                                <img src="<?php echo $image[0]; ?>"
                                                                                     title="<?php echo $title ?>"
                                                                                     alt="<?php echo $title ?>">
                                                                            </a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="article-information-content">
                                                                        <div class="article-title"><a
                                                                                    href="<?php echo $link ?>"
                                                                                    title="<?php echo $title ?>"
                                                                                    alt="<?php echo $title ?>"><?php echo $title ?></a>
                                                                        </div>
                                                                        <div class="article-short"><?php echo $article_short ?>
                                                                        </div>
                                                                        <div class="article-by">
                                                                            by <?php echo $article_by ?> <?php echo $post_date ?>
                                                                        </div>
                                                                        <div class="article-content">
                                                                            <?php echo $short_description ?>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </ul>
                                            </article>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="row">
                                            <article class="article-new">
                                                <div class="article-label"><?php _e('[:en]Popular Travel Blogger[:vi]Blogger du lịch nổi tiếng[:]')?></div>
                                                <ul class="list-article">
                                                    <?php
                                                    $arg = array(
                                                        'posts_per_page' => 2,
                                                        'orderby' => 'term_id',
                                                        'order' => 'DESC',
                                                        'cat' => 36,
                                                        'limit' => 2,
                                                        'meta_query' => [
                                                            [
                                                                'key' => 'blog_noi_tieng',
                                                                'value' => 1,
                                                                'compare' => '='
                                                            ]
                                                        ]
                                                    );
                                                    $r = new WP_Query($arg);
                                                    $i = 1;
                                                    ?>
                                                    <?php if ($r->have_posts()): ?>
                                                        <?php foreach ($r->posts as $p): ?>
                                                            <?php
                                                            $id = $p->ID;
                                                            $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'single-post-thumbnail');
                                                            $title = $p->post_title;
                                                            $link = get_permalink($id);
                                                            $post_date = date('d/m/Y', strtotime($p->post_date));
                                                            $comment_count = $p->comment_count;
                                                            $article_short = get_field('article_short', $id);
                                                            if (strlen($article_short) > 50) {
                                                                $article_short = mb_substr($article_short, 0, 50) . '...';
                                                            }
                                                            $article_by = get_field('article_by', $id);
                                                            $short_description = get_field('short_description', $id);
                                                            if (strlen($short_description) > 50) {
                                                                $short_description = mb_substr($short_description, 0, 50) . '...';
                                                            }
                                                            ?>
                                                            <li class="list-article-sub">
                                                                <ul class="article-border">
                                                                    <li class="article-image">
                                                                        <div class="article-image-border">
                                                                            <a href="<?php echo $link ?>"
                                                                               title="<?php echo $title ?>"
                                                                               alt="<?php echo $title ?>">
                                                                                <img src="<?php echo $image[0]; ?>"
                                                                                     title="<?php echo $title ?>"
                                                                                     alt="<?php echo $title ?>">
                                                                            </a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="article-information-content">
                                                                        <div class="article-title"><a
                                                                                    href="<?php echo $link ?>"
                                                                                    title="<?php echo $title ?>"
                                                                                    alt="<?php echo $title ?>"><?php echo $title ?>
                                                                            </a></div>
                                                                        <div class="article-short"><?php echo $article_short ?>
                                                                        </div>
                                                                        <div class="article-by">
                                                                            by <?php echo $article_by ?> <?php echo $post_date ?>
                                                                        </div>
                                                                        <div class="article-content">
                                                                            <?php echo $short_description ?>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </ul>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
get_footer();
