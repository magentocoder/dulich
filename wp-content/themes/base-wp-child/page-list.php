<?php
// Template Name: Page List
get_header();
?>
    <main class="col-xs-12 col-xs-12 listing-page-border">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="main-content col-lg-12 col-xs-12">
                        <div class="row">
                            <aside class="col-lg-3 col-xs-12 ">
                                <div class="row">
                                    <div class="menu-left hidden-sm hidden-xs">
                                        <div class="sidebar customer-care">
                                            <div class="customer-care-call-text">
                                                Gọi điện để tư vấn
                                            </div>
                                            <div class="customer-number-phone">
                                                <span class="icon-telephone"><i class="fas fa-phone"></i></span>
                                                090 123 456
                                            </div>
                                            <div class="customer-care-call-text">
                                                Hoặc để lại số điện thoại Trekvn sẽ gọi cho bạn
                                            </div>
                                            <input type="text" value="" placeholder="Số điện thoại của bạn...">
                                        </div>
                                        <div class="register-information">
                                            <div class="register-info-title">Đăng kí nhận thông tin</div>
                                            <div class="register-info-text">Hãy là người đầu tiên nhận được những tin
                                                tức thú vị và những chương trình khuyến mãi nóng hổi nhất
                                            </div>
                                            <div class="form-register-info">
                                                <?php $base_url = get_site_url(); ?>
                                                <form role="form" method="post" action="<?php echo $base_url ?>/?na=s"
                                                      id="ajax-newsletter-left">
                                                    <div class="form-group">
                                                        <label for="name">Họ và tên</label>
                                                        <input type="text" class="form-control" id="name"
                                                               placeholder="Name" name="nn" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input type="email" class="form-control" id="email"
                                                               placeholder="Email" name="ne" required>
                                                    </div>
                                                    <button type="submit"
                                                            class="btn btn-default register ajax-newsletter-left">Đăng
                                                        kí
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <section class=" col-lg-9  col-xs-12 col-md-9 col-sm-12">
                                <div class="row">
                                    <div class="entertainment-page-content news-page col-lg-12 col-xs-12">
                                        <div class="row">
                                            <?php
                                            $categories = get_the_category();
                                            $catID = $categories[0]->term_id;
                                            $arg = array('orderby' => 'term_id', 'order' => 'DESC', 'cat' => 21);
                                            $r = new WP_Query($arg);
                                            ?>
                                            <?php while ($r->have_posts()): ?>
                                                <?php $r->the_post(); ?>
                                                <article class="entertainment-main col-lg-12 col-sm-12 col-xs-12">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                            <div class="row">
                                                                <div class="entertainment-image">
                                                                    <a href="<?php echo get_permalink(); ?>"
                                                                       title="<?php the_title(); ?>">
                                                                        <?php the_post_thumbnail(); ?>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                            <div class="row">
                                                                <div class="entertainment-content">
                                                                    <div class="entertainment-content-label">
                                                                        <a href="<?php echo get_permalink(); ?>"
                                                                           title="<?php the_title(); ?>"><?php the_title(); ?></a>
                                                                    </div>
                                                                    <div class="entertainment-content-detail">
                                                                        <div class="entertainment-short-description">
                                                                            <?php
                                                                            $short_description = get_field('short_description', get_the_ID());
                                                                            if (strlen($short_description) > 100) {
                                                                                $short_description = mb_substr($short_description, 0, 100) . '...';
                                                                            }
                                                                            echo($short_description);
                                                                            ?>
                                                                        </div>
                                                                        <div class="news-page-button">
                                                                            <span class="news-time">
                                                                                <i class="far fa-calendar-alt"></i>
                                                                                <?php $post = get_post(); ?>
                                                                                <?php echo date('d/m/Y', strtotime($post->post_date)) ?>
                                                                            </span>
                                                                            <span class="news-button-view-detail">
                                                                                <a href="<?php echo get_permalink(); ?>"
                                                                                   title="<?php the_title(); ?>">[Xem chi tiết]</a>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                                <?php wp_reset_postdata(); ?>
                                            <?php endwhile; ?>
                                        </div>
                                    </div>
                                    <div class="product-detail-page entertainment-page col-xs-12 col-lg-12">
                                        <div class="pagination-border">
                                            <ul class="pagination">
                                                <li><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                                                </li>
                                                <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a>
                                                </li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li><a href="#">4</a></li>
                                                <li><a href="#">5</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
get_footer(); ?>