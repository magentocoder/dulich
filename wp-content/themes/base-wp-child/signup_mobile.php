<?php
// Template Name: Sign up mobile
get_header(); ?>
<?php
$redirect_url = (isset($_GET['redirect_url']) ? $_GET['redirect_url'] : get_site_url());
?>

<main class="col-xs-12 col-xs-12 listing-page-border transport-page">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="main-content col-lg-12 col-xs-12">
                    <div class="row">
                        <div class="login-register-page">
                            <h4 class="title-page">Đăng ký tài khoản</h4>
                            <form role="form" method="post" action="" class="form-login" id="reg-form-ajax-mobile">
                                <div class="form-group">
                                    <input type="email" required class="form-control" name="user_email" id="dk_user_email_m" placeholder="Số điện thoại hoặc địa chỉ email *">
                                    <input type="hidden" class="form-control" name="user_login" placeholder="Enter email" id="dk_user_login_m">
                                </div>
                                <div class="form-group">
                                    <input type="hidden" required name="redirect_to" class="redirect_to" value="<?php echo $redirect_url;?>" />
                                    <input type="password" name="user_pass" class="form-control"  placeholder="Mật khẩu">
                                    <input type="hidden" name="action" value="reg_new_user_ajax" />
                                </div>
                                <div class="form-group">
                                    <input type="password"  required class="form-control" name="pass1_re"  placeholder="Nhập lại mật khẩu *">
                                </div>
                                <div class="term-agree">
                                    <p>Chọn đăng ký bạn đồng ý với các <a href="#">Điều khoản và dịch vụ</a> của Trekvn</p>
                                </div>
                                <div class="button-login">
                                    <button type="submit" class="btn btn-default">Đăng ký</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    jQuery(document).ready(function ($) {
        $("#dk_user_email_m").keyup(function () {
            var user = $("#dk_user_email_m").val();
            $("#dk_user_login_m").val(user);
        });
    });
</script>
<?php
get_footer(); ?>
