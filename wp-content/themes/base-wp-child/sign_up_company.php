<?php
// Template Name: Đăng ký đại lý
get_header(); ?>
<?php

?>

    <main class="col-xs-12 col-lg-12">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="form-dangky-daily">
                        <h4 class="dangki-title">Đăng ký để trở thành đại lý của chúng tôi</h4>
                        <div class="dangky-daily-border">
                            <form role="form" action="" method="post" enctype="multipart/form-data" id="sign_up_company">
                                <ul>
                                    <li class="form-group language-availabel">
                                        <ul>
                                            <li class="label-dangky">
                                                <label>Ngôn ngữ hỗ trợ *</label>
                                            </li>
                                            <li class="dangky-content">
                                                <div class="langague hidden-md hidden-lg">
                                                    <div class="selectric">
                                                        <span class="label-language">
                                                            Ngôn ngữ hỗ trợ
                                                        </span>
                                                        <i class="fas fa-angle-down"></i>
                                                    </div>
                                                    <div class="selectric-items" tabindex="-1" style="display: none">
                                                        <div class="selectric-scroll">
                                                            <ul>
                                                                <li data-index="0" class="selected highlighted">
                                                                    <a href="#" title="VND">
                                                                        <img src="<?php echo get_stylesheet_directory_uri();?>/image/flag_vn.png">
                                                                    </a>
                                                                </li>
                                                                <li data-index="1" class="last">
                                                                    <a href="#" title="EN">
                                                                        <img src="<?php echo get_stylesheet_directory_uri();?>/image/flag_england.png">
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="flag-icon hidden-sm hidden-xs">
                                                    <img src="<?php echo get_stylesheet_directory_uri();?>/image/flag_vn.png"></div>
                                                <div class="flag-icon hidden-sm hidden-xs"><img src="<?php echo get_stylesheet_directory_uri();?>/image/flag_england.png"></div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="form-group">
                                        <ul>
                                            <li class="label-dangky">
                                                <label>Tên công ty/ Đại lý *</label>
                                            </li>
                                            <li class="dangky-content">
                                                <input type="text" class="form-control company" required placeholder="Tên công ty" name="ten_cong_ty">
                                                <input type="text" class="form-control daily-input" required placeholder="Đại lý" name="ten_dai_ly">
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="form-group">
                                        <ul>
                                            <li class="label-dangky">
                                                <label>Email *</label>
                                            </li>
                                            <li class="dangky-content">
                                                <div class="dangky-content">
                                                    <input type="email" class="form-control" required placeholder="Email" name="thu_dt">
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="form-group">
                                        <ul>
                                            <li class="label-dangky">
                                                <label>Mật khẩu *</label>
                                            </li>
                                            <li class="dangky-content">
                                                <input type="password" class="form-control mk" required placeholder="Mât khẩu" name="mk">
                                                <input type="password" class="form-control rmk" required placeholder="Nhập lại mật khẩu" name="rmk">
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="form-group">
                                        <ul>
                                            <li class="label-dangky">
                                                <label>Logo Công ty *</label>
                                            </li>
                                            <li class="dangky-content">
                                                <div class="humman-person">
                                                    <img src="<?php echo get_stylesheet_directory_uri();?>/image/man-human-person.png" title="" alt="#">
                                                    <input type="file" name="logo_cong_ty" required class="upload-file">
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="form-group">
                                        <ul>
                                            <li class="label-dangky">
                                                <label>Mô tả</label>
                                            </li>
                                            <li class="dangky-content">
                                                <div class="dangky-content">
                                                    <textarea class="form-control" rows="3" name="mo_ta" placeholder="Mô tả"></textarea>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="form-group">
                                        <ul>
                                            <li class="label-dangky">
                                                <label>Tỉnh thành phố</label>
                                            </li>
                                            <li class="dangky-content">
                                                <div class="dangky-content">
                                                    <input type="text" name="tinh_pho" class="form-control" placeholder="Tỉnh thành phố">
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="form-group">
                                        <ul>
                                            <li class="label-dangky">
                                                <label>Điện thoại *</label>
                                            </li>
                                            <li class="dangky-content">
                                                <div class="dangky-content">
                                                    <input type="text" name="dien_thoai" required class="form-control" placeholder="Số điện thoại">
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="form-group">
                                        <ul>
                                            <li class="label-dangky">
                                                <label>Hotline *</label>
                                            </li>
                                            <li class="dangky-content">
                                                <div class="dangky-content">
                                                    <input type="text" name="hotline" required class="form-control" placeholder="Hotline">
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="form-group">
                                        <ul>
                                            <li class="label-dangky">
                                                <label>Website *</label>
                                            </li>
                                            <li class="dangky-content">
                                                <div class="dangky-content">
                                                    <input type="text" name="website" required class="form-control" placeholder="http://www.example.com">
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="form-group">
                                        <ul>
                                            <li class="label-dangky">
                                                <label>Loại hình kinh doanh</label>
                                            </li>
                                            <li class="dangky-content">
                                                <div class="dangky-content">
                                                    <textarea class="form-control" rows="3" name="loai_hinh_khinh_doanh" placeholder="Loại hình kinh doanh"></textarea>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="checkbox-button">
                                        <input type="checkbox" id="dk_company"> Tôi xin cam đoan mọi thông tin cung cấp đều đúng sự thật
                                    </li>
                                </ul>
                                <div class="button-dangky-daily">
                                    <button type="submit" disabled id="company_sub" class="btn btn-default">Đăng ký</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script>
        jQuery(document).ready(function ($) {
            $('#dk_company').change(function () {
                if($(this).val()){
                    $('#company_sub').removeAttr('disabled');
                }else{
                    $('#company_sub').attr("disabled", "disabled");
                }
            });
            $("#sign_up_company").on('submit',function () {
                var mk = $(".mk").first().val();
                var rmk = $(".rmk").first().val();
                if (mk != rmk) {
                    $('<p class="msg">Không chính xác</p>').insertAfter($(".rmk"));
                    return false;
                }else{
                    $(".msg").remove();
                }
                $.ajax({
                    method:'post',
                    url:'<?php echo get_site_url()?>/signup_company.php',
                    data:$(this).serialize(),
                    success:function (re) {
                        if (re==1){
                            alert('Đăng ký thành công');
                            document.location.href = '<?php echo get_site_url()?>';
                        } else{
                            alert(re);
                        }
                    }
                });
                return false;
            });

        });
    </script>
<?php
get_footer();
