<?php
// Template Name: Food
get_header(); ?>
<main class="col-xs-12 col-xs-12 listing-page-border">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="main-content col-lg-12 col-xs-12">
                    <div class="row">

                        <aside class="col-lg-3 col-xs-12 ">
                            <div class="row">
                                <div class="menu-left-mobile">
                                    <div class="col-xs-12 hidden-lg hidden-md">
                                        <div class="button-filter">Bộ lọc</div>
                                    </div>
                                    <div class="all-province-city">
                                        <div class="menu-left">
                                            <div class="sidebar hidden-sm hidden-xs">
                                                <div class="mapp-wappar">
                                                    <div class="map-place">
                                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703"
                                                                width="100%" height="150px" frameborder="0" style="border:0"
                                                                allowfullscreen></iframe>
                                                    </div>
                                                </div>
                                                <div class="map-sticker">
                                                    <span>Hiển thị bản đồ</span>
                                                </div>
                                            </div>
                                            <div class="filter-border-sub">
                                                <div class="filter-title">Chọn lọc theo</div>
                                                <br/>
                                                <select id="city" class="form-control">
                                                    <option>Thành Phố</option>
                                                    <?php
                                                    foreach ($city as $item) {
                                                        ?>
                                                        <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                            <option><?= $item['meta_value']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <br/>
                                                <select id="district" class="form-control">
                                                    <option>Quận - Huyện</option>
                                                </select>
                                                <br/>
                                                <select id="commune" class="form-control">
                                                    <option>Phường - Xã</option>
                                                </select>
                                                <br/>
                                                <span>LOẠI CƠ SỞ</span><br/>
                                                <input class="loai_co_so" type="checkbox" value="Nhà hàng"/>Nhà hàng<br/>
                                                <input class="loai_co_so" type="checkbox" value="Món tráng miệng"/>Món tráng miệng<br/>
                                                <input class="loai_co_so" type="checkbox" value="Cà phê và trà"/>Cà phê và trà<br/>
                                                <input class="loai_co_so" type="checkbox" value="Bar và quán rượu"/>Bar và quán rượu<br/>
                                                <br/>
                                                <span>ẨM THỰC VÀ MÓN ĂN</span><br/>
                                                <input class="food" type="checkbox" value="Món Á"/>Món Á<br/>
                                                <input class="food" type="checkbox" value="Món Âu"/>Món Âu<br/>
                                                <input class="food" type="checkbox" value="Món Trung"/>Món Trung<br/>
                                                <input class="food" type="checkbox" value="Món Thái"/>Món Thái<br/>
                                                <br/>
                                                <span>PHÙ HỢP VỚI</span><br/>
                                                <input class="fit" type="checkbox" value="Gia đình có trẻ em"/>Gia đình có trẻ em<br/>
                                                <input class="fit" type="checkbox" value="Hội họp"/>Hội họp<br/>
                                                <input class="fit" type="checkbox" value="Tổ chức sự kiện"/>Tổ chức sự kiện<br/>
                                                <input class="fit" type="checkbox" value="Món ăn địa phương"/>Món ăn địa phương<br/>
                                                <br/>
                                                <span>GÍA CẢ</span><br/>
                                                <input class="food_price" type="checkbox" value="Đồ ăn giá rẻ"/>Đồ ăn giá rẻ<br/>
                                                <input class="food_price" type="checkbox" value="Giá trung bình"/>Giá trung bình<br/>
                                                <input class="food_price" type="checkbox" value="Cao cấp"/>Cao cấp<br/>
                                                <br/>
                                                <button type="button" class="btn btn-default" id="search-list-food">
                                                    <span class="glyphicon glyphicon-search"></span> Tìm
                                                </button>
                                                <br/><br/>
                                                <span>Tìm theo tên</span><br/>
                                                <input type="text" class="form-control" id="ten-nha-hang"
                                                       placeholder="Tìm theo tên ..."/>
                                                <!--                                                <div class="filter-content"></div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </aside>
                        <section class="col-lg-9 col-xs-12">
                            <div class="row">
                                <div class="listing-page">
                                    <div class="border-list-item-tourist">
                                        <article class="list-tourist list-food col-lg-12 col-xs-12">
                                            <div class="follow-by-food">
                                                <h3 class="list-food-label">Duyệt theo đồ ăn</h3>
                                                <div class="follow-by-food-content">
                                                    <?php
                                                    $parent_term_id = 23;
                                                    $taxonomies = array('category');
                                                    $args = array('parent' => $parent_term_id);
                                                    $categories = get_terms($taxonomies, $args);
                                                    $total_show = floor(count($categories) / 3) * 3;
                                                    $i = 0;
                                                    if ($total_show == 0) $total_show = 3;
                                                    ?>
                                                    <?php foreach ($categories as $child): ?>

                                                        <div class="col-lg-4 col-md-4 col-xs-6">
                                                            <div class="row">
                                                                <div class="follow-by-food-content-item">
                                                                    <div class="follow-food-image">
                                                                        <img src="<?php echo get_field('image', $child) ?>"
                                                                             title="<?php echo $child->name ?>"
                                                                             alt="<?php echo $child->name ?>">
                                                                    </div>
                                                                    <div class="follow-food-name-border">
                                                                        <div class="follow-food-name">
                                                                            <a href="<?php echo get_category_link($child->term_id) ?>"
                                                                               title="#"><?php echo $child->name ?>
                                                                                <span> (<?php echo $child->count ?>
                                                                                    )</span></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php $i++;
                                                        if ($i == $total_show) break;
                                                        ?>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                            <div class="food-sub">
                                                <h3 class="list-food-label">Món ăn địa phương</h3>
                                                <div class="food-sub-content">
                                                    <?php
                                                    $arg = array(
                                                        'posts_per_page' => 3,
                                                        'orderby' => 'term_id',
                                                        'order' => 'DESC',
                                                        'cat' => 27,
                                                        'limit' => 3
                                                    );
                                                    $r = new WP_Query($arg);
                                                    ?>
                                                    <?php if ($r->have_posts()): ?>
                                                        <?php foreach ($r->posts as $p): ?>
                                                            <div class="col-lg-4 col-md-4 col-xs-12">
                                                                <div class="row">
                                                                    <div class="food-sub-border">
                                                                        <div class="food-sub-content-image-border">
                                                                            <div class="food-item-image">
                                                                                <a href="<?php echo get_permalink($p->ID) ?>"
                                                                                   title="<?php echo $p->post_title; ?>"><img
                                                                                            src="<?php echo get_the_post_thumbnail_url($p); ?>"
                                                                                            title="<?php echo $p->post_title; ?>"
                                                                                            alt="#"></a>
                                                                            </div>
                                                                            <div class="review-place">
                                                                                <span class="review-percent">8.9</span>
                                                                                <span class="review-comment">(<?php echo (int)$p->comment_count; ?> nhận xét 
                                                                                    )</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="food-sub-content-description">
                                                                            <div class="food-sub-content-description-name">
                                                                                <a href="<?php echo get_permalink($p->ID) ?>"
                                                                                   title="<?php echo $p->post_title; ?>">
                                                                                    <?php echo $p->post_title; ?>
                                                                                </a>
                                                                            </div>
                                                                            <div class="food-sub-content-short-description">
                                                                                <?php
                                                                                $short_description = get_field('short_description', $p->ID);
                                                                                if (strlen($short_description) > 100) {
                                                                                    $short_description = mb_substr($short_description, 0, 100) . '...';
                                                                                }
                                                                                echo($short_description);
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="food-sub">
                                                <h3 class="list-food-label">Giá cả phải chăng</h3>
                                                <div class="food-sub-content">
                                                    <?php
                                                    $arg = array(
                                                        'posts_per_page' => 3,
                                                        'orderby' => 'term_id',
                                                        'order' => 'DESC',
                                                        'cat' => 28,
                                                        'limit' => 3
                                                    );
                                                    $r = new WP_Query($arg);
                                                    ?>
                                                    <?php if ($r->have_posts()): ?>
                                                        <?php foreach ($r->posts as $p): ?>
                                                            <div class="col-lg-4 col-md-4 col-xs-12">
                                                                <div class="row">
                                                                    <div class="food-sub-border">
                                                                        <div class="food-sub-content-image-border">
                                                                            <div class="food-item-image">
                                                                                <a href="<?php echo get_permalink($p->ID) ?>"
                                                                                   title="<?php echo $p->post_title; ?>"><img
                                                                                            src="<?php echo get_the_post_thumbnail_url($p); ?>"
                                                                                            title="<?php echo $p->post_title; ?>"
                                                                                            alt="#"></a>
                                                                            </div>
                                                                            <div class="review-place">
                                                                                <span class="review-percent">8.9</span>
                                                                                <span class="review-comment">(<?php echo (int)$p->comment_count; ?> nhận xét 
                                                                                    )</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="food-sub-content-description">
                                                                            <div class="food-sub-content-description-name">
                                                                                <a href="<?php echo get_permalink($p->ID) ?>"
                                                                                   title="<?php echo $p->post_title; ?>">
                                                                                    <?php echo $p->post_title; ?>
                                                                                </a>
                                                                            </div>
                                                                            <div class="food-sub-content-short-description">
                                                                                <?php
                                                                                $short_description = get_field('short_description', $p->ID);
                                                                                if (strlen($short_description) > 100) {
                                                                                    $short_description = mb_substr($short_description, 0, 100) . '...';
                                                                                }
                                                                                echo($short_description);
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="food-sub">
                                                <h3 class="list-food-label">Đồ ăn giá rẻ</h3>
                                                <div class="food-sub-content">
                                                    <?php
                                                    $arg = array(
                                                        'posts_per_page' => 3,
                                                        'orderby' => 'term_id',
                                                        'order' => 'DESC',
                                                        'cat' => 29,
                                                        'limit' => 3
                                                    );
                                                    $r = new WP_Query($arg);
                                                    ?>
                                                    <?php if ($r->have_posts()): ?>
                                                        <?php foreach ($r->posts as $p): ?>
                                                            <div class="col-lg-4 col-md-4 col-xs-12">
                                                                <div class="row">
                                                                    <div class="food-sub-border">
                                                                        <div class="food-sub-content-image-border">
                                                                            <div class="food-item-image">
                                                                                <a href="<?php echo get_permalink($p->ID) ?>"
                                                                                   title="<?php echo $p->post_title; ?>"><img
                                                                                            src="<?php echo get_the_post_thumbnail_url($p); ?>"
                                                                                            title="<?php echo $p->post_title; ?>"
                                                                                            alt="#"></a>
                                                                            </div>
                                                                            <div class="review-place">
                                                                                <span class="review-percent">8.9</span>
                                                                                <span class="review-comment">(<?php echo (int)$p->comment_count; ?> nhận xét 
                                                                                    )</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="food-sub-content-description">
                                                                            <div class="food-sub-content-description-name">
                                                                                <a href="<?php echo get_permalink($p->ID) ?>"
                                                                                   title="<?php echo $p->post_title; ?>">
                                                                                    <?php echo $p->post_title; ?>
                                                                                </a>
                                                                            </div>
                                                                            <div class="food-sub-content-short-description">
                                                                                <?php
                                                                                $short_description = get_field('short_description', $p->ID);
                                                                                if (strlen($short_description) > 100) {
                                                                                    $short_description = mb_substr($short_description, 0, 100) . '...';
                                                                                }
                                                                                echo($short_description);
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="food-sub">
                                                <h3 class="list-food-label">Bữa sáng</h3>
                                                <div class="food-sub-content">
                                                    <?php
                                                    $arg = array(
                                                        'posts_per_page' => 3,
                                                        'orderby' => 'term_id',
                                                        'order' => 'DESC',
                                                        'cat' => 30,
                                                        'limit' => 3
                                                    );
                                                    $r = new WP_Query($arg);
                                                    ?>
                                                    <?php if ($r->have_posts()): ?>
                                                        <?php foreach ($r->posts as $p): ?>
                                                            <div class="col-lg-4 col-md-4 col-xs-12">
                                                                <div class="row">
                                                                    <div class="food-sub-border">
                                                                        <div class="food-sub-content-image-border">
                                                                            <div class="food-item-image">
                                                                                <a href="<?php echo get_permalink($p->ID) ?>"
                                                                                   title="<?php echo $p->post_title; ?>"><img
                                                                                            src="<?php echo get_the_post_thumbnail_url($p); ?>"
                                                                                            title="<?php echo $p->post_title; ?>"
                                                                                            alt="#"></a>
                                                                            </div>
                                                                            <div class="review-place">
                                                                                <span class="review-percent">8.9</span>
                                                                                <span class="review-comment">(<?php echo (int)$p->comment_count; ?> nhận xét 
                                                                                    )</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="food-sub-content-description">
                                                                            <div class="food-sub-content-description-name">
                                                                                <a href="<?php echo get_permalink($p->ID) ?>"
                                                                                   title="<?php echo $p->post_title; ?>">
                                                                                    <?php echo $p->post_title; ?>
                                                                                </a>
                                                                            </div>
                                                                            <div class="food-sub-content-short-description">
                                                                                <?php
                                                                                $short_description = get_field('short_description', $p->ID);
                                                                                if (strlen($short_description) > 100) {
                                                                                    $short_description = mb_substr($short_description, 0, 100) . '...';
                                                                                }
                                                                                echo($short_description);
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $("#search-list-food").click(function () {
            var city = $("#city").val();
            var district = $("#district").val();
            var commune = $("#commune").val();
            var loai_co_so = $(".loai_co_so:checked").val();
            var food = $(".food:checked").val();
            var fit = $(".fit:checked").val();
            var food_price = $(".food_price:checked").val();
            var url = '<?php echo get_site_url(); ?>/search-food-listing.php';
            $.ajax({
                    'type':'post',
                    'url':  url,
                    'data'  : {
                        'city': city,
                        'district': district,
                        'commune': commune,
                        'loai_co_so':loai_co_so,
                        'food':food,
                        'fit':fit,
                        'food_price':food_price
                    },
                    success : function(response) {
                        $("#old-listing-page").remove();
                        var result = $('<div />').append(response).find('#search-transport-result').html();
                        $("#search-listing-page").html(result);
                        $("#search-listing-page").show();
                    }
                }
            );
        });
    });
</script>
<?php
get_footer(); ?>
