<?php
// Template Name: Book Food
get_header(); ?>
<?php
$id = isset($_GET['id']) ? $_GET['id'] : '';
if ($id == '') {
    echo '<script> window.location.href= "' . home_url() . '";</script>';
}
$product = wc_get_product($id);
$name = $product->name;
$price = (int)get_post_meta($id, '_regular_price', true);;
$image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'single-post-thumbnail');
$imageUrl = $image[0];
$address = get_field('address', $id);
$phone_number = get_field('phone_number', $id);
$email = get_field('email', $id);
$website = get_field('website', $id);
$ten_nha_hang = get_field('ten_nha_hang', $id);
global $wpdb;
$city = $wpdb->get_results('SELECT DISTINCT meta_value FROM wp_postmeta WHERE meta_key LIKE "city" AND meta_value IS NOT NULL', ARRAY_A);

$content = $product->description;
?>
    <main class="listing-page col-xs-12 col-xs-12 listing-page-border book-tour-detail">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="main-content col-lg-12 col-xs-12">
                        <div class="row">
                            <aside class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                                <div class="row">
                                    <div class="menu-left-mobile">
                                        <div class="col-xs-12 hidden-lg hidden-md">
                                            <div class="button-filter">Bộ lọc</div>
                                        </div>
                                        <div class="all-province-city">
                                            <div class="menu-left ">
                                                <div class="menu-left-mobile">
                                                    <div class="col-xs-12 hidden-lg hidden-md">
                                                        <div class="button-filter">Bộ lọc</div>
                                                    </div>
                                                    <div class="all-province-city">
                                                        <div class="menu-left filter_food">
                                                            <div class="sidebar hidden-sm hidden-xs">
                                                                <div class="mapp-wappar">
                                                                    <div class="map-place">
                                                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703"
                                                                                width="100%" height="150px"
                                                                                frameborder="0"
                                                                                style="border:0"
                                                                                allowfullscreen></iframe>
                                                                    </div>
                                                                </div>
                                                                <div class="map-sticker">
                                                                    <span>Hiển thị bản đồ</span>
                                                                </div>
                                                            </div>
                                                            <div class="filter-border">
                                                                <div class="filter-title">Chọn lọc theo</div>
                                                                <br/>
                                                                <select id="city" class="form-control">
                                                                    <option>Thành Phố</option>
                                                                    <?php
                                                                    foreach ($city as $item) {
                                                                        ?>
                                                                        <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                            <option><?= $item['meta_value']; ?></option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <br/>
                                                                <select id="district" class="form-control">
                                                                    <option>Quận - Huyện</option>
                                                                </select>
                                                                <br/>
                                                                <select id="commune" class="form-control">
                                                                    <option>Phường - Xã</option>
                                                                </select>
                                                                <br/>
                                                                <span>LOẠI CƠ SỞ</span><br/>
                                                                <input class="loai_co_so" type="checkbox"
                                                                       value="Nhà hàng"/>Nhà
                                                                hàng<br/>
                                                                <input class="loai_co_so" type="checkbox"
                                                                       value="Món tráng miệng"/>Món
                                                                tráng miệng<br/>
                                                                <input class="loai_co_so" type="checkbox"
                                                                       value="Cà phê và trà"/>Cà phê
                                                                và trà<br/>
                                                                <input class="loai_co_so" type="checkbox"
                                                                       value="Bar và quán rượu"/>Bar
                                                                và quán rượu<br/>
                                                                <br/>
                                                                <span>ẨM THỰC VÀ MÓN ĂN</span><br/>
                                                                <input class="food" type="checkbox" value="Món Á"/>Món Á<br/>
                                                                <input class="food" type="checkbox" value="Món Âu"/>Món
                                                                Âu<br/>
                                                                <input class="food" type="checkbox" value="Món Trung"/>Món
                                                                Trung<br/>
                                                                <input class="food" type="checkbox" value="Món Thái"/>Món
                                                                Thái<br/>
                                                                <br/>
                                                                <span>PHÙ HỢP VỚI</span><br/>
                                                                <input class="fit" type="checkbox"
                                                                       value="Gia đình có trẻ em"/>Gia đình
                                                                có trẻ em<br/>
                                                                <input class="fit" type="checkbox" value="Hội họp"/>Hội
                                                                họp<br/>
                                                                <input class="fit" type="checkbox"
                                                                       value="Tổ chức sự kiện"/>Tổ chức sự
                                                                kiện<br/>
                                                                <input class="fit" type="checkbox"
                                                                       value="Món ăn địa phương"/>Món ăn địa
                                                                phương<br/>
                                                                <br/>
                                                                <span>GÍA CẢ</span><br/>
                                                                <input class="food_price" type="checkbox"
                                                                       value="Đồ ăn giá rẻ"/>Đồ ăn
                                                                giá rẻ<br/>
                                                                <input class="food_price" type="checkbox"
                                                                       value="Giá trung bình"/>Giá
                                                                trung bình<br/>
                                                                <input class="food_price" type="checkbox"
                                                                       value="Cao cấp"/>Cao cấp<br/>
                                                                <br/>
                                                                <button type="button" class="btn btn-default"
                                                                        id="search-list-food">
                                                                    <span class="glyphicon glyphicon-search"></span> Tìm
                                                                </button>
                                                                <br/><br/>
                                                                <span>Tìm theo tên</span><br/>
                                                                <input type="text" class="form-control"
                                                                       id="ten-nha-hang"
                                                                       placeholder="Tìm theo tên ..."/>
                                                                <!--                                                <div class="filter-content"></div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <section class=" col-lg-9 col-xs-12 col-md-9 col-sm-12">
                                <div class="row">
                                    <div class="listing-page entertainment-page-content news-page col-lg-12 col-xs-12"
                                         id="old-listing-page">
                                        <div class="row">
                                            <?php if (isset($_POST) && !empty($_POST)): ?>
                                                <?php
                                                $address = array(
                                                    'first_name' => $_POST['ten'],
                                                    'last_name' => $_POST['ten'],
                                                    'phone' => $_POST['sdt'],
                                                );
                                                $custom_field = array(
                                                    'ngay_dat' => 'ngay_dat',
                                                    'thoi_dan_dat' => 'thoi_dan_dat',
                                                    'so_nguoi_lon' => 'so_nguoi_lon',
                                                    'so_tre_em' => 'so_tre_em'
                                                );
                                                $order = wc_create_order();
                                                $order->add_product($product, 1); //(get_product with id and next is for quantity)
                                                $order->set_address($address, 'billing');
                                                $order->set_address($address, 'shipping');
                                                $order->set_payment_method('cod');
                                                $order->calculate_totals();
                                                foreach ($custom_field as $k => $f) {
                                                    update_field($k, $_POST[$f], $order->get_id());
                                                }

                                                WC()->cart->empty_cart();
                                                echo "Bạn đặt nhà hàng thành công";
                                                echo '<script>setTimeout(function () {window.location.href= "' . get_site_url() . '";},5000);</script>';
                                                ?>
                                            <?php else: ?>
                                                <article class="entertainment-main book-food-wapper">
                                                    <div class="book-food-title">
                                                        <div class="col-lg-8 col-md-8 col-xs-12">
                                                            <div class="row">
                                                                <div class="book-food">
                                                                    <div class="name-restaurant"><?php echo _e($ten_nha_hang); ?></div>
                                                                    <div class="item-detail-tourist">
                                                                        <div class="item-address item-detail-sub">
                                                                            <i class="fa fa-map-marker icon"
                                                                               aria-hidden="true"></i>
                                                                            <?php echo $address ?>
                                                                        </div>
                                                                        <div class="item-phone-number item-detail-sub">
                                                                            <span class="glyphicon glyphicon-phone-alt icon"></span>
                                                                            <?php echo $phone_number ?>
                                                                        </div>
                                                                        <div class="item-email item-detail-sub">
                                                                            <i class="fa fa-envelope icon"
                                                                               aria-hidden="true"></i>
                                                                            <?php echo $email ?>
                                                                        </div>
                                                                        <div class="item-website item-detail-sub">
                                                                            <span class="glyphicon glyphicon-globe icon"></span></i>
                                                                            <?php echo $website ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-xs-12">
                                                            <div class="row">
                                                                <div class="book-food">
                                                                    <div class="item-detail-price-border">
                                                                        <div class="item-detail-price-label"><?php _e('[:en]Price[:vi]Giá[:]') ?></div>
                                                                        <div class="item-detail-price price-not-special">
                                                                            <?php echo number_format($price, 0, '.', ','); ?>
                                                                            đ
                                                                        </div>
                                                                        <div class="review">
                                                                            <?php $comments_count = wp_count_comments($id); ?>
                                                                            <span class="review-text">(<b><?php echo (int)$comments_count->total_comments; ?></b> <?php _e('[:en] comment[:vi] nhận xét[:]') ?>
                                                                                )</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="book-food-form-register">
                                                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                                            <div class="row">
                                                                <?php
                                                                $i = 0;
                                                                $attachment_ids = $product->get_gallery_attachment_ids(); ?>
                                                                <?php if (!empty($attachment_ids)): ?>
                                                                    <?php foreach ($attachment_ids as $attachment_id): ?>
                                                                        <?php $i++; ?>
                                                                        <div class="book-food-left">
                                                                            <?php $image_link = wp_get_attachment_url($attachment_id); ?>
                                                                            <img src="<?php echo $image_link; ?>" alt=""
                                                                                 title="#">
                                                                        </div>
                                                                        <?php if ($i == 3) break; ?>
                                                                    <?php endforeach; ?>
                                                                <?php else: ?>
                                                                    <?php for ($i = 1; $i <= 3; $i++): ?>
                                                                        <div class="book-food-left">
                                                                            <img src="<?php echo $imageUrl ?>"
                                                                                 title="<?php echo $name ?>"
                                                                                 alt="<?php echo $name ?>">
                                                                        </div>
                                                                    <?php endfor; ?>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div class="row">
                                                                <div class="book-food-middle">
                                                                    <img src="<?php echo $imageUrl ?>"
                                                                         title="<?php echo $name ?>"
                                                                         alt="<?php echo $name ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <div class="row">
                                                                <div class="label-form-register-food"><?php _e('[:en]Set up a table to receive incentives[:vi]Đặt bàn nhận ngay ưu đãi[:]') ?>
                                                                </div>
                                                                <div class="form-book-food">
                                                                    <?php echo do_shortcode('[contact-form-7 id="430" title="Book food"]') ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="book-food-content">
                                                        <?php _e($content); ?>
                                                    </div>
                                                </article>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="listing-page entertainment-page-content news-page col-lg-12 col-xs-12 woocommerce"
                                         id="search-listing-page" style="display:none;">
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $("#search-list").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var price = $(".price:checked").val();
                var url = '<?php echo get_site_url(); ?>/search-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if (w < 991) {
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-transport").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var time_start = null;
                if ($("#time_start").val()) {
                    var time_start_date = new Date($("#time_start").val());
                    var moth = time_start_date.getMonth() + 1;
                    time_start = moth + "/" + time_start_date.getDate() + "/" + time_start_date.getFullYear();
                }
                var time_end = null;
                if ($("#time_end").val()) {
                    var time_end_date = new Date($("#time_end").val());
                    var month = time_end_date.getMonth() + 1;
                    time_end = month + "/" + time_end_date.getDate() + "/" + time_end_date.getFullYear();
                    ;
                }

                var kieu_thue = $(".kieu_thue:checked").val();
                var dong_xe = $(".dong_xe:checked").val();
                var loai_xe = $(".loai_xe:checked").val();
                var xep_hang_xe = $(".xep_hang_xe:checked").val();
                var nha_cung_cap = $("#nha_cung_cap").val();
                var url = '<?php echo get_site_url(); ?>/search-transport-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'time_start': time_start,
                            'time_end': time_end,
                            'kieu_thue': kieu_thue,
                            'dong_xe': dong_xe,
                            'loai_xe': loai_xe,
                            'xep_hang_xe': xep_hang_xe,
                            'nha_cung_cap': nha_cung_cap
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-transport-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if (w < 991) {
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-food").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var loai_co_so = $(".loai_co_so:checked").val();
                var food = $(".food:checked").val();
                var fit = $(".fit:checked").val();
                var food_price = $(".food_price:checked").val();
                var ten_nha_hang = $("#ten_nha_hang").val();
                var url = '<?php echo get_site_url(); ?>/search-food-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'loai_co_so': loai_co_so,
                            'food': food,
                            'fit': fit,
                            'food_price': food_price,
                            'ten_nha_hang': ten_nha_hang
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-transport-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if (w < 991) {
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-hotel").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#ten-khach-san").val();
                var price = $(".price:checked").val();
                var rate = $(".rate:checked").val();
                var rank = $(".rank:checked").val();
                var url = '<?php echo get_site_url(); ?>/search-hotel-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                            'rate': rate,
                            'rank': rank,
                            'ten': ten
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $(".pagination").hide();
                            $(".review-comment-border").find('ul.pagination').show();
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if (w < 991) {
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-tour").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#ten-khach-san").val();
                var price = $("#price").val();
                var rate = $("#rate").val();
                var rank = $("#rank").val();
                var url = '<?php echo get_site_url(); ?>/search-tour-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                            'rate': rate,
                            'rank': rank,
                            'ten': ten
                        },
                        success: function (response) {
                            $(".entertainment-page-content").remove();
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if (w < 991) {
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-company").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#ten-khach-san").val();
                var price = $("#price").val();
                var rate = $("#rate").val();
                var rank = $("#rank").val();
                var url = '<?php echo get_site_url(); ?>/search-company-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                            'rate': rate,
                            'rank': rank,
                            'ten': ten
                        },
                        success: function (response) {
                            $(".entertainment-page-content").remove();
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if (w < 991) {
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
        });
    </script>
<?php
get_footer();
