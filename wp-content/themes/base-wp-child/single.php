<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Base WP
 */

get_header(); ?>
<?php
global $wp;
$current_url = home_url($wp->request);

?>
    <main class="col-xs-12 col-xs-12 listing-page-border">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="main-content col-lg-12 col-xs-12">
                        <div class="row">
                            <aside class="col-lg-3 col-xs-12 ">
                                <div class="row">
                                    <div class="menu-left hidden-sm hidden-xs">
                                        <div class="sidebar customer-care">
                                            <div class="customer-care-call-text">
                                                Gọi điện để tư vấn
                                            </div>
                                            <div class="customer-number-phone">
                                                <span class="icon-telephone"><i class="fas fa-phone"></i></span>
                                                090 123 456
                                            </div>
                                            <div class="customer-care-call-text">
                                                Hoặc để lại số điện thoại Trekvn sẽ gọi cho bạn
                                            </div>
                                            <input type="text" value="" placeholder="Số điện thoại của bạn...">
                                        </div>
                                        <div class="register-information">
                                            <div class="register-info-title">Đăng kí nhận thông tin</div>
                                            <div class="register-info-text">Hãy là người đầu tiên nhận được những tin
                                                tức thú vị và những chương trình khuyến mãi nóng hổi nhất
                                            </div>
                                            <div class="form-register-info">
                                                <?php $base_url = get_site_url(); ?>
                                                <form role="form" method="post" action="<?php echo $base_url ?>/?na=s"
                                                      id="ajax-newsletter-left">
                                                    <div class="form-group">
                                                        <label for="name">Họ và tên</label>
                                                        <input type="text" class="form-control" id="name"
                                                               placeholder="Name" name="nn" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input type="email" class="form-control" id="email"
                                                               placeholder="Email" name="ne" required>
                                                    </div>
                                                    <button type="submit"
                                                            class="btn btn-default register ajax-newsletter-left">Đăng
                                                        kí
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <section class=" col-lg-9 col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12 col-lg-12">
                                        <div class="row">
                                            <div class="product-detail-page">
                                                <?php
                                                while (have_posts()) : the_post(); ?>
                                                    <aside class="content-tourist">
                                                        <div class="title-tourist"><?php the_title() ?></div>
                                                        <div class="info-tourist">
                                                            <div class="header-tourist">
                                                                <div class="image-info-first"><?php the_post_thumbnail() ?></div>
                                                                <div class="short-description">
                                                                    <?php echo get_field('short_description', get_the_ID()) ?>
                                                                </div>
                                                            </div>
                                                            <?php the_content() ?>
                                                        </div>
                                                    </aside>
                                                <?php endwhile; ?>
                                                <div class="related-tourist">
                                                    <div class="related-tourist-label">Có thể bạn quan tâm?</div>
                                                    <div class="related-list-tourist">
                                                        <?php
                                                        $cat_obj = get_queried_object();
                                                        $cat_parent = get_the_category($cat_obj->ID);
                                                        $cat_parent_id = isset($cat_parent[0]) ? $cat_parent[0]->term_id : $cat_obj->ID;
                                                        $arg = array(
                                                            'posts_per_page' => 3,
                                                            'orderby' => 'term_id',
                                                            'order' => 'DESC',
                                                            'cat' => $cat_parent_id);
                                                        $r = new WP_Query($arg);
                                                        ?>
                                                        <?php while ($r->have_posts()): ?>
                                                            <?php $r->the_post(); ?>
                                                            <div class="col-lg-4 col-xs-12">
                                                                <div class="row">
                                                                    <div class="related-item-tourist">
                                                                        <div class="image-item-related">
                                                                            <a href="<?php echo get_permalink(); ?>"
                                                                               title="<?php the_title(); ?>">
                                                                                <?php the_post_thumbnail(); ?>
                                                                            </a>
                                                                        </div>
                                                                        <div class="title-item-related">
                                                                            <a href="<?php echo get_permalink(); ?>"
                                                                               title="<?php the_title(); ?>">
                                                                                <?php the_title(); ?>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php wp_reset_postdata(); ?>
                                                        <?php endwhile; ?>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-xs-12">
                                                    <div class="row">
                                                        <div class="comment-fb">
                                                            <!--<div id="fb-root"></div>-->
                                                            <script>(function (d, s, id) {
                                                                    var js, fjs = d.getElementsByTagName(s)[0];
                                                                    if (d.getElementById(id)) return;
                                                                    js = d.createElement(s);
                                                                    js.id = id;
                                                                    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.12&appId=1262722123757247&autoLogAppEvents=1';
                                                                    fjs.parentNode.insertBefore(js, fjs);
                                                                }(document, 'script', 'facebook-jssdk'));</script>

                                                            <div class="comment-fb-like">
                                                                Bấm like để nhận thông tin mới nhất từ Trekvn
                                                                <div class="fb-like"
                                                                     data-href="https://developers.facebook.com/docs/plugins/"
                                                                     data-layout="button" data-action="like" data-size="small"
                                                                     data-show-faces="false" data-share="false"></div>
                                                            </div>
                                                            <div class="comment-fb-place">
                                                                <div id="fb-root"></div>
                                                                <script>(function (d, s, id) {
                                                                        var js, fjs = d.getElementsByTagName(s)[0];
                                                                        if (d.getElementById(id)) return;
                                                                        js = d.createElement(s);
                                                                        js.id = id;
                                                                        js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.12&appId=1749609138652186&autoLogAppEvents=1';
                                                                        fjs.parentNode.insertBefore(js, fjs);
                                                                    }(document, 'script', 'facebook-jssdk'));</script>
                                                                <div class="fb-comments"
                                                                     data-href="<?php echo $current_url;?>"
                                                                     data-width="100%" data-numposts="5"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
get_footer();
