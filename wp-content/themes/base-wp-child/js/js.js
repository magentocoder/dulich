
window.onload = function () {
    $(document).ready(function () {
        $("img").unveil();
    });
};

jQuery(".langague" ).click(function() {
    jQuery('.langague .selectric-items').show();
});

$(document).ready(function() {

    $(".langague").hover(function() {
        if( $('.langague .selectric').hasClass('active') ){
            $('.langague .selectric-items').fadeOut();
            $('.langague .selectric').removeClass('active');
        }else {
            $('.langague .selectric-items').fadeIn();
            $('.langague .selectric').addClass('active');
        }
    });
    $(".language-availabel .dangky-content .langague").click(function() {
        if( $('.language-availabel .selectric').hasClass('active') ){
            $('.language-availabel .selectric-items').fadeOut();
            $('.language-availabel .selectric').removeClass('active');
        }else {
            $('.language-availabel .selectric-items').fadeIn();
            $('.language-availabel .selectric').addClass('active');
        }
    });

    var account = $(".header-account .selectric.guess ");
    var login_dl = $(".account-daily .daily-guess ");
    account.click(function() {
        if( !account.hasClass('active') ){
            $('.login-form').show();
            $('.selectric-items').hide();
            $('.register-form').hide();
            login_dl.removeClass('active');
            $('.header-account .selectric-items.guess-item').fadeIn();
            account.addClass('active');
            $('.header-account .register-form').hide();
        }
    });
    $('.header-account .redirect-register').click(function () {
        $('.login-form').hide();
        $('.register-form').show();
    });
    $(".header-account .close-login").click(function() {
        account.removeClass('active');
        $('.header-account .selectric-items.guess-item').fadeOut();
    });
    var customer = $(".header-account .customer");
    customer.click(function() {
        if( !customer.hasClass('active') ){
            $('.header-account .customer-item').fadeIn();
            customer.addClass('active');
        }else{
            $('.header-account .customer-item').fadeOut();
            customer.removeClass('active');
        }
    });

    // Đại lý popup

    login_dl.click(function() {
        if( !login_dl.hasClass('active') ){
            $('.login-dl-form').show();
            $('.selectric-items').hide();
            account.removeClass('active');
            $('.account-daily .daily-guess-item').fadeIn();
            login_dl.addClass('active');
            // $('.header-account .register-form').hide();
        }
    });
    $(".account-daily .close-login").click(function() {
        login_dl.removeClass('active');
        $('.account-daily .selectric-items.daily-guess-item').fadeOut();
    });

    //popup link login mobile
    var link_popup = $("#mod-mobile-menu .icon-my-account");

    link_popup.click(function() {
        if( !link_popup.hasClass('active') ){
            $('#mod-mobile-menu .icon-my-account .popup-link').fadeIn();
            link_popup.addClass('active');
        }else{
            $('#mod-mobile-menu .icon-my-account .popup-link').fadeOut();
            link_popup.removeClass('active');
        }
    });

    $(".locator-dropdown .selectric").click(function() {
        if( $(this).hasClass('active') ){
            $('.locator-dropdown .selectric-items').fadeOut();
            $(this).removeClass('active');
            $('.locator-dropdown').removeClass('show-popup');
        }else {
            $('.locator-dropdown .selectric-items').fadeIn();
            $(this).addClass('active');
            $('.locator-dropdown').addClass('show-popup');
        }
    });
    $(".menu-left-mobile .filter-mobile-wappar").click(function() {
        var e = $(".menu-left-mobile .filter-mobile-wappar .filter-mobile-content-");
        if( $(this).hasClass('active') ){
            e.fadeOut();
            $(this).removeClass('active');
            // $('.locator-dropdown').removeClass('show-popup');
        }else {
            e.fadeIn();
            $(this).addClass('active');
            // $('.locator-dropdown').addClass('show-popup');
        }
    });
    // $('[data-toggle="tooltip"]').tooltip();
    $(".menu-left-mobile .button-filter").click(function() {
        if( $(this).hasClass('active') ){
            $('.menu-left-mobile .all-province-city').fadeOut();
            $(this).removeClass('active');
            // $('.locator-dropdown').removeClass('show-popup');
        }else {
            $('.menu-left-mobile .all-province-city').fadeIn();
            $(this).addClass('active');
            // $('.locator-dropdown').addClass('show-popup');
        }
    });
    var w = window.innerWidth;
    if(w < 991){
        $('.menu-left-mobile .all-province-city').hide();
    }
});

$(document).ready(function() {
    // ẩn tất cả các thẻ div với class="tab_content-quickview".
    $(".tab_content-quickview").hide();
    // Mặc định nội dung thẻ tab đầu tiên sẽ được hiển thị
    $(".tab_content-quickview:first").show();
    $("ul.tabs-quickview li:first").addClass("active");

    $("ul.tabs-quickview li").click(function() {
        // gỡ bỏ class="active" cho tất cả các thẻ <li>
        $("ul.tabs-quickview li").removeClass("active");
        // chèn class="active" vào phần tử <li> vừa được click
        $(this).addClass("active");
        // ẩn tất cả thẻ <div> với class="tab_content-quickview"
        $(".tab_content-quickview").hide();
        //Hiển thị nội dung thẻ tab được click với hiệu ứng Fade In
        var activeTab = $(this).attr("rel");
        $("."+activeTab).fadeIn();
    });

});
function showQuickView(id) {
    var e = $('.item-tourist').find('[data-index='+id+']');
    $('.item-tourist  .quickview').hide();
    $('.item-tourist .carousel').removeAttr('id');
    if(e.hasClass('active')){
        e.hide();
        e.removeClass('active');
        e.find('.carousel').removeAttr('id');
    }else {
        e.show();
        e.addClass('active');
        e.find('.tab_content-quickview').hide();
        e.find('.tab_content-quickview:first').show();
        e.find("ul.tabs-quickview li").removeClass("active");
        e.find("ul.tabs-quickview li:first").addClass("active");
        var  p_id  = id.split("-");
        e.find('.carousel').attr('id', 'carousel-simple-'+p_id[1]);
    }
}
function closeQuickView(element){
    var e = $(element).closest('.quickview');
    e.removeAttr('id');
    e.removeClass('active');
    e.fadeOut();
}
/** Slide show image*/
$(window).load(function() {
    $("#flexiselDemo1").flexisel({
        visibleItems: 3,
        enableResponsiveBreakpoints: true,
        responsiveBreakpoints: {
            portrait: {
                changePoint:480,
                visibleItems: 1
            },
            landscape: {
                changePoint:640,
                visibleItems: 2
            },
            tablet: {
                changePoint:768,
                visibleItems: 3
            }
        }
    });
    $("#flexiselDemo2").flexisel({
        visibleItems: 3,
        enableResponsiveBreakpoints: true,
        responsiveBreakpoints: {
            portrait: {
                changePoint:480,
                visibleItems: 1
            },
            landscape: {
                changePoint:640,
                visibleItems: 2
            },
            tablet: {
                changePoint:768,
                visibleItems: 3
            }
        }
    });
    $("#flexiselDemo3").flexisel({
        visibleItems: 3,
        enableResponsiveBreakpoints: true,
        responsiveBreakpoints: {
            portrait: {
                changePoint:480,
                visibleItems: 1
            },
            landscape: {
                changePoint:640,
                visibleItems: 2
            },
            tablet: {
                changePoint:768,
                visibleItems: 3
            }
        }
    });
    $("#flexiselDemo4").flexisel({
        visibleItems: 3,
        enableResponsiveBreakpoints: true,
        responsiveBreakpoints: {
            portrait: {
                changePoint:480,
                visibleItems: 1
            },
            landscape: {
                changePoint:640,
                visibleItems: 2
            },
            tablet: {
                changePoint:768,
                visibleItems: 3
            }
        }
    });
});
var acc = document.getElementsByClassName("accordion-quickview");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}