function debugScreen() {
    var a = $(document).width();
    $("#debug").text("").text(a)
}
function calculateAspectRatioFit(a, b, c, d) {
    var e = Math.min(c / a, d / b);
    return {width: a * e, height: b * e}
}
function checkFlashInstalled(a, b) {
    try {
        a = new ActiveXObject(a + b + "." + a + b)
    } catch (c) {
        a = navigator.plugins[a + " " + b]
    }
    return !!a
}
function checkIeVersion(a) {
    var b = !1;
    switch (a) {
        case 7:
            b = /MSIE 7/.test(navigator.userAgent);
            break;
        case 8:
            b = /MSIE 8/.test(navigator.userAgent);
            break;
        case 9:
            b = /MSIE 9/.test(navigator.userAgent);
            break;
        case 10:
            b = /MSIE 10/.test(navigator.userAgent);
            break;
        case 11:
            b = /rv:11/.test(navigator.userAgent)
    }
    return b
}
function browserDetect() {
    var a = (navigator.userAgent.indexOf("MSIE") || navigator.userAgent.indexOf("rv:11")) > -1, b = navigator.userAgent.indexOf("Firefox") > -1, c = navigator.userAgent.indexOf("Safari") > -1, d = navigator.userAgent.indexOf("Chrome") > -1, e = navigator.userAgent.indexOf("OPR") > -1, f = navigator.userAgent.indexOf("Mac OS") > -1;
    f && b && $("html").addClass("macFirefox"), f && d && $("html").addClass("macChrome"), a && $("html").addClass("allIe"), b && $("html").addClass("firefox"), d && c && !e && $("html").addClass("chrome"), !d && c && $("html").addClass("safari"), e && $("html").addClass("opera"), checkIeVersion(7) && $("html").addClass("ie7"), checkIeVersion(8) && $("html").addClass("ie8"), checkIeVersion(9) && $("html").addClass("ie9"), checkIeVersion(10) && $("html").addClass("ie10"), checkIeVersion(11) && $("html").addClass("ie11")
}
function myPopupVideo(a) {
    if ("undefined" == typeof $.magnificPopup)return void alert("myPopupVideo: Không tìm thấy thư viện magnificPopup");
    if ("undefined" == typeof jwplayer)return void alert("myPopupVideo: Không tìm thấy thư viện jwplayer");
    var b = $.extend({
        title: "Video",
        videoUrl: "",
        pictureUrl: "",
        width: "100%",
        height: 439,
        autoPlay: !1,
        playerSkin: "scripts/libs/jwplayer/five.xml",
        closeOnBg: !0,
        delay: 0
    }, a), c = ['<div id="popupVideoWrap" class="animated magnificPopup magnificPopupVideo">', '<div class="headControls">', "<h4>" + b.title + "</h4>", "</div>", '<div id="popupVideoPlayer"></div>', '<div class="footControls">', '<a class="btn btnClosePopup">Đóng</a>', "</div>", "</div>"].join("");
    $.magnificPopup.open({
        removalDelay: b.delay, callbacks: {
            open: function () {
                this.content.addClass("flipInX")
            }, beforeClose: function () {
                this.content.addClass("flipOutX")
            }, close: function () {
                this.content.removeClass("flipOutX")
            }
        }, items: {src: c, type: "inline"}, closeOnBgClick: b.closeOnBg
    }), jwplayer("popupVideoPlayer").setup({
        file: b.videoUrl,
        image: b.pictureUrl,
        width: b.width,
        height: b.height,
        autostart: b.autoPlay,
        skin: b.playerSkin
    })
}
function myPopupAlert(a) {
    if ("undefined" == typeof $.magnificPopup)return void alert("myPopupAlert: Không tìm thấy thư viện magnificPopup");
    var b = $.extend({
        title: "Thông báo",
        content: "",
        btnText: "Đóng",
        closeOnBg: !0,
        delay: 0,
        redirecTo: ""
    }, a), c = ['<div id="popupAlertWrap" class="magnificPopup magnificPopupAlert">', '<div class="headControls">', "<h4>" + b.title + "</h4>", "</div>", "" === b.content ? "" : '<p class="content">' + b.content + "</p>", '<div class="footControls">', '<a class="btn btnClosePopupLink">' + b.btnText + "</a>", "</div>", "</div>"].join("");
    $.magnificPopup.open({
        removalDelay: b.delay, callbacks: {
            open: function () {
                this.content.addClass("flipInX")
            }, beforeClose: function () {
                this.content.addClass("flipOutX")
            }, close: function () {
                this.content.removeClass("flipOutX")
            }
        }, items: {src: c, type: "inline"}, closeOnBgClick: b.closeOnBg
    }), $(document).delegate(".btnClosePopupLink", "click", function () {
        "" === b.redirecTo && $(".mfp-close").trigger("click")
    })
}
function myPopupAlertEn(a) {
    if ("undefined" == typeof $.magnificPopup)return void alert("myPopupAlert: Không tìm thấy thư viện magnificPopup");
    var b = $.extend({
        title: "Thông báo",
        content: "",
        closeOnBg: !0,
        delay: 0,
        redirecTo: ""
    }, a), c = ['<div id="popupAlertWrap" class="magnificPopup magnificPopupAlert">', '<div class="headControls">', "<h4>" + b.title + "</h4>", "</div>", "" === b.content ? "" : '<p class="content">' + b.content + "</p>", '<div class="footControls">', '<a class="btn btnClosePopupLink">Close</a>', "</div>", "</div>"].join("");
    $.magnificPopup.open({
        removalDelay: b.delay, callbacks: {
            open: function () {
                this.content.addClass("flipInX")
            }, beforeClose: function () {
                this.content.addClass("flipOutX")
            }, close: function () {
                this.content.removeClass("flipOutX")
            }
        }, items: {src: c, type: "inline"}, closeOnBgClick: b.closeOnBg
    }), $(document).delegate(".btnClosePopupLink", "click", function () {
        "" === b.redirecTo && $(".mfp-close").trigger("click")
    })
}
function myPopupContent(a) {
    if ("undefined" == typeof $.magnificPopup)return void alert("myPopupContent: Không tìm thấy thư viện magnificPopup");
    var b = $.extend({rel: "", width: 700, delay: 0, closeOnBg: !0, customClass: ""}, a), c = $(b.rel);
    return c.length > 0 ? (c.css({maxWidth: b.width}), void $.magnificPopup.open({
        mainClass: b.customClass,
        removalDelay: b.delay,
        callbacks: {
            open: function () {
                this.content.addClass("flipInX")
            }, beforeClose: function () {
                this.content.addClass("flipOutX")
            }, close: function () {
                this.content.removeClass("flipOutX")
            }
        },
        items: {src: b.rel, type: "inline"},
        closeOnBgClick: b.closeOnBg
    })) : void alert("myPopupContent: Không tìm thấy nội dung!")
}
function setCookie(a, b, c, d, e, f) {
    var g = new Date;
    g.setTime(g.getTime()), c && (c = 1e3 * c * 60);
    var h = new Date(g.getTime() + c);
    document.cookie = a + "=" + escape(b) + (c ? ";expires=" + h.toGMTString() : "") + (d ? ";path=" + d : "") + (e ? ";domain=" + e : "") + (f ? ";secure" : "")
}
function getCookie(a) {
    var b = document.cookie.indexOf(a + "="), c = b + a.length + 1;
    if (!b && a != document.cookie.substring(0, a.length))return null;
    if (-1 == b)return null;
    var d = document.cookie.indexOf(";", c);
    return -1 == d && (d = document.cookie.length), unescape(document.cookie.substring(c, d))
}
function deleteCookie(a, b, c) {
    getCookie(a) && (document.cookie = a + "=" + (b ? ";path=" + b : "") + (c ? ";domain=" + c : "") + ";expires=Thu, 01-Jan-1970 00:00:01 GMT")
}
function getDocumentSize() {
    return [Math.max(document.body.scrollWidth, document.documentElement.scrollWidth), Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)]
}
function getWindowSize() {
    var a = 0, b = 0;
    return "number" == typeof window.innerWidth ? (a = window.innerWidth, b = window.innerHeight) : document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight) ? (a = document.documentElement.clientWidth, b = document.documentElement.clientHeight) : document.body && (document.body.clientWidth || document.body.clientHeight) && (a = document.body.clientWidth, b = document.body.clientHeight), [a, b]
}
function embedYoutube(a, b) {
    var c = $.extend({width: 480, height: 320, params: ""}, b), d = /\?v\=(\w+)/.exec(a)[1];
    return '<iframe style="visibility:hidden;" onload="this.style.visibility=\'visible\';" class="youtube-video" type="text/html" width="' + c.width + '" height="' + c.height + ' "src="http://www.youtube.com/embed/' + d + "?" + c.params + '&amp;wmode=transparent" frameborder="0" />'
}
function limitWords(a, b) {
    var c = a.split(" ");
    return c.splice(b, c.length - 1), c.join(" ") + (c.length !== a.split(" ").length ? "..." : "")
}
function RandomRangeFloat(a, b) {
    return Math.random() * (b - a) + a
}
function RandomRangeInt(a, b) {
    return Math.floor(Math.random() * (b - a)) + a
}
function parseIntEx(a) {
    var b = isNaN(parseInt(a)) ? 0 : parseInt(a);
    return b
}
function parseFloatEx(a) {
    var b = isNaN(a) ? 0 : a;
    return b
}
function getQueryString(a, b) {
    b = b.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var c = "[\\?&]" + b + "=([^&#]*)", d = new RegExp(c), e = d.exec(a);
    return null == e ? "" : decodeURIComponent(e[1].replace(/\+/g, " "))
}
TweenMax.defaultEase = Linear.easeNone;
var jDevices = jRespond([{label: "mobile", enter: 0, exit: 767}, {
    label: "tablet",
    enter: 768,
    exit: 1023
}, {label: "desktop", enter: 1024, exit: 1e4}]), jDevicesSlider = jRespond([{
    label: "tablet",
    enter: 0,
    exit: 300
}, {label: "desktop", enter: 301, exit: 1e4}]), jDevicesAppear = jRespond([{
    label: "tablet",
    enter: 0,
    exit: 1023
}, {label: "desktop", enter: 1024, exit: 1e4}]);
$.fn.gMap = function (a, b, c) {
    try {
        var d = new google.maps.LatLng(a[0], a[1]), e = {
            zoom: b,
            center: d,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [{
                featureType: "landscape.natural",
                elementType: "geometry.fill",
                stylers: [{color: "#004483"}]
            }, {featureType: "road.arterial", elementType: "geometry", stylers: [{color: "#fff"}]}]
        }, f = new google.maps.Map($(this).get(0), e), g = new google.maps.InfoWindow({
            content: c,
            maxWidth: 350
        }), h = new google.maps.MarkerImage("images/pointer.png", new google.maps.Size(30, 48), new google.maps.Point(0, 0), new google.maps.Point(24, 30)), i = new google.maps.Marker({
            position: d,
            map: f,
            icon: h
        });
        google.maps.event.addListener(i, "click", function () {
            g.open(f, i)
        })
    } catch (j) {
    }
// }, $.fn.customRadioCheckbox = function () {
//     return this.each(function () {
//         var a = $(this), b = $("<span/>");
//         b.addClass("custom-" + (a.is(":checkbox") ? "check" : "radio")), a.is(":checked") && b.addClass("checked"), b.insertAfter(a), a.parent("label").addClass("custom-label").attr("onclick", ""), a.css({
//             position: "absolute",
//             left: "-9999px"
//         }), a.on({
//             change: function () {
//                 a.is(":radio") && a.parent().siblings("label").find(".custom-radio").removeClass("checked"), b.toggleClass("checked", a.is(":checked"))
//             }, focus: function () {
//                 b.addClass("focus")
//             }, blur: function () {
//                 b.removeClass("focus")
//             }
//         })
//     })
}, $.fn.myArrcordion = function (a, b) {
    var c = this.find(".item");
    b && (c.first().find(".question").addClass("expanded"), c.first().find(".answer").show()), c.find(".question").click(function () {
        var b = $(this);
        b.hasClass("expanded") ? (b.removeClass("expanded"), b.next().slideUp(200)) : (a && $.each(c.find(">.question"), function () {
            $(this).hasClass("expanded") && ($(this).removeClass("expanded"), $(this).parent().find(".answer").slideUp(200))
        }), b.addClass("expanded"), b.next().slideDown(200))
    })
}, $.fn.onlyNumber = function () {
    return this.each(function () {
        $(this).bind("keydown paste", function (a) {
            var b = a.which || a.keyCode;
            return !a.shiftKey && !a.altKey && !a.ctrlKey && b >= 48 && 57 >= b || b >= 96 && 105 >= b || 109 === b || 110 === b || 8 === b || 9 === b || 13 === b || 35 === b || 36 === b || 37 === b || 39 === b || 46 === b || 45 === b ? !0 : !1
        })
    })
}, $.fn.lockInput = function (a) {
    this.each(function () {
        a ? $(this).keydown(function (a) {
            a.preventDefault()
        }) : $(this).unbind("keydown")
    })
}, String.format = function (a) {
    if (arguments.length <= 1)return a;
    for (var b = arguments.length - 2, c = 0; b >= c; c++)a = a.replace(new RegExp("\\{" + c + "\\}", "gi"), arguments[c + 1]);
    return a
};
var mainJs = function () {
    function a() {
        $('input[type="text"],textarea').placeholder()
    }

    // function b() {
    //     $('input[type="radio"],input[type="checkbox"]').customRadioCheckbox()
    // }

    function c() {
        $(".onlyNumber").onlyNumber()
    }

    function d() {
        var a = window.location.hash;
        a.length && TweenMax.to($("html,body"), .7, {scrollTop: $(a).position().top - 130})
    }

    function e() {
        setTimeout(function () {
            $("html,body").scrollTop(0)
        }, 200)
    }

    function f(a) {
        for (var b, c = $.extend({
            object: "",
            itemInRow: 4,
            className: "myRow",
            addClearfix: !0
        }, a), d = c.object.length, e = 0; d > e; e += c.itemInRow) {
            b = e + c.itemInRow;
            var f = c.object.slice(e, e + c.itemInRow).wrapAll('<div class="' + c.className + '"></div>');
            c.addClearfix && f.parent().append("<div class='clearfix'></div>")
        }
    }

    function g() {
        $(".cate-list").find(".inner").hover(function () {
            TweenMax.to($(this).find("img"), .4, {scale: 1.04})
        }, function () {
            TweenMax.to($(this).find("img"), .4, {scale: 1})
        })
    }

    function h() {
        $(document).delegate(".btnClosePopup", "click", function () {
            $(".mfp-close").trigger("click")
        })
    }

    function i(a, b) {
        var c = $.extend({selector: ".customScroll", theme: "dark-thick", scrollButtons: {enable: !0}}, a);
        $(c.selector).length >= 1 && $(c.selector).mCustomScrollbar(b ? "update" : c)
    }

    function j() {
        $(".combobox").select2({openOnEnter: !1, dropdownAutoWidth: !0})
    }

    function k(a) {
        var b = $(a), c = _.map(b, function (a) {
            return $(a).offset().left
        }), d = _.max(c);
        b.length > 1 && _.each(b, function (a, b) {
            $(a).css("z-index", 1e3 - b), $(a).offset().left === d && $(a).addClass("lastItem")
        })
    }

    function l(a) {
        var b = $(a), c = b.attr("cname"), d = b.attr("ccode"), e = b.attr("cstate"), f = b.attr("cprice"), g = b.attr("cdesc"), h = b.attr("clink"), i = b.attr("cview"), j = b.find(".img-responsive").attr("src");
        return {name: c, code: d, state: e, price: f, desc: g, link: h, pic: j, view: i}
    }

    function m(a, b) {
        $(".product-list").find(".list-item>.item:not(.special)").on("click", function () {
            if (getWindowSize()[0] <= 1023)if ($(this).hasClass("cateItem")) {
                var a = $(this), b = a.find(".hoverCate").clone().removeClass("hoverCate").addClass("mobileCate").show();
                $("#collect-cate-tooltip").html(b), myPopupContent({
                    rel: "#collect-cate-tooltip",
                    closeOnBg: !0,
                    width: 800,
                    delay: 0
                })
            } else {
                var a = $(this), b = l(a), c = $("#collect-tooltip");
                c.find(".name").text(b.name), c.find(".code").text(b.code), c.find(".price").text(b.price), c.find(".state").text(b.state), c.find(".desc").text(b.desc), c.find(".btnProductDetail").text(b.view), c.find(".link").attr("href", b.link), c.find(".pic>img").attr("src", b.pic), myPopupContent({
                    rel: "#collect-tooltip",
                    closeOnBg: !0,
                    width: 800,
                    delay: 0
                })
            }
        }), $(".product-list").find(".list-item>.item:not(.special)").on("mouseenter", function () {
            function c() {
                setTimeout(function () {
                    $(".hoverScale").imageScale(), $(".hoverScale").delay(300).css("opacity", 1)
                }, 300)
            }

            if (getWindowSize()[0] >= 1024) {
                var d = $(this), e = d.position(), f = $(this).hasClass("lastItem"), g = l(d), h = ['<div class="pic">', '<img src="' + g.pic + '" style="opacity:0" class="hoverScale img-responsive" data-scale="best-fit-down" data-align="center">', "</div>", '<div class="info">', '<h3 class="name">' + g.name + "</h3>", '<span class="block code">' + g.code + "</span>", '<span class="price">' + g.price + "</span>", '<span class="block state">' + g.state + "</span>", '<p class="desc">' + g.desc + "</p>", g.link ? '<a target="_blank" href="' + g.link + '" class="btn btnProductDetail link">' + g.view + "</a>" : "", "</div>", '<div class="clearfix"></div>'].join("");
                $(this).hasClass("cateItem") ? f ? d.find(".hoverCate").addClass("right").css({
                    left: e.left - a - 476,
                    top: e.top + b
                }).stop().fadeIn(200, c()) : d.find(".hoverCate").addClass("left").css({
                    left: e.left + a,
                    top: e.top + b
                }).stop().fadeIn(200, c()) : (d.find(".hoverTip").empty().append(h), $(".hoverScale").load(function () {
                    f ? d.find(".hoverTip").addClass("right").css({
                        left: e.left - a - 221,
                        top: e.top + b
                    }).stop().fadeIn(200, c()) : d.find(".hoverTip,.hoverCate").addClass("left").css({
                        left: e.left + a,
                        top: e.top + b
                    }).stop().fadeIn(200, c()), c()
                }))
            }
        }).on("mouseleave", function () {
            if (getWindowSize()[0] >= 1024) {
                var a = $(this);
                TweenMax.delayedCall(.2, function () {
                    a.find(".hoverTip,.hoverCate").stop().hide()
                })
            }
        })
    }

    function n(a) {
        var b, c = $.extend({
            selector: "",
            fade: !0,
            delay: 3e3,
            auto: !0,
            speed: 700,
            pauseOnHover: !0,
            dot: !0,
            afterChange: function () {
            },
            beforeChange: function () {
            }
        }, a), d = $(c.selector);
        return d.length && (b = d.find(".lightSlider").slick({
            dots: c.dot,
            slidesToShow: 1,
            fade: c.fade,
            autoplay: c.auto,
            pauseOnHover: c.pauseOnHover,
            speed: c.speed,
            autoplaySpeed: c.delay,
            slidesToScroll: 1,
            onBeforeChange: c.beforeChange,
            onAfterChange: c.afterChange
        }), d.find(".controls a").on("click", function (a) {
            $(this).hasClass("prev") ? d.find(".slick-prev").trigger("click") : d.find(".slick-next").trigger("click"), a.preventDefault()
        })), b
    }

    function o() {
        var a = $(".slide-show"), b = a.height() / 2;
        a.find(".controls a").css("top", b)
    }

    function p(a) {
        var b = $(".microDropdown");
        switch (a) {
            case"handheld":
                $(".micro-menu").addClass("micro-mobile-menu").removeClass("micro-menu"), b.find(">ul").hide(), b.on("click", function () {
                    b.find(">ul").slideToggle()
                });
                break;
            case"desktop":
                $(".micro-mobile-menu").addClass("micro-menu").removeClass("micro-mobile-menu"), b.unbind("click").find(">ul").show();
                break;
            default:
                b.find("li").on("click", function () {
                    if ($(this).hasClass("active"))$(this).removeClass("active"); else {
                        var a = $(this).text();
                        b.find(">span").text(a)
                    }
                })
        }
    }

    function q() {
        p(), jDevices.addFunc({
            breakpoint: ["tablet,mobile"], enter: function () {
                p("handheld")
            }, exit: function () {
                p("desktop")
            }
        }, {
            breakpoint: "desktop", enter: function () {
                p("desktop")
            }, exit: function () {
                p("handheld")
            }
        })
    }

    function r() {
        $(".animated").appear(), $(document.body).on("appear", ".animated", function () {
            var a = $(this), b = a.data("animation");
            if (!a.hasClass("visible")) {
                var c = a.data("animation-delay");
                c ? setTimeout(function () {
                    a.addClass(b + " visible")
                }, c) : a.addClass(b + " visible")
            }
        })
    }

    function s(a) {
        TweenMax.to($("html,body"), .5, {scrollTop: a})
    }

    function t() {
        var a = $("html,body");
        $("#crollTop").click(function () {
            TweenMax.to(a, .8, {scrollTop: 0})
        })
    }

    return {
        fixPlaceHolderIe: a,
        // customRadioCheckbox: b,
        onlyInputNumber: c,
        resetTop: e,
        closeAllPopup: h,
        customScroll: i,
        comboboxStyle: j,
        calcHoverProduct: k,
        productTooltip: m,
        gotoHash: d,
        makeSlider: n,
        autoRow: f,
        scaleTo: g,
        autoCenterControl: o,
        responsive: q,
        animate: r,
        scrollTop: t,
        scrollTo: s
    }
}();
$(document).ready(function () {
    mainJs.fixPlaceHolderIe(), mainJs.closeAllPopup(), mainJs.customScroll(), mainJs.comboboxStyle(), mainJs.responsive(), mainJs.scrollTop(), mainJs.animate(), browserDetect(), jDevicesAppear.addFunc({
        breakpoint: "tablet",
        enter: function () {
            $(".animated").addClass("visible")
        },
        exit: function () {
        }
    })
}), $(window).resize(_.debounce(function () {
    $("img.scale").imageScale(), mainJs.autoCenterControl()
}, 400)), $(window).load(function () {
    $("img.scale").imageScale(), mainJs.productTooltip(15, 0), mainJs.scaleTo(), mainJs.gotoHash(), mainJs.autoCenterControl(), TweenMax.to($(".slide-show>.controls a"), .5, {opacity: 1}), mainJs.calcHoverProduct(".product-list .item:not(.cateItem)"), mainJs.calcHoverProduct(".product-list .cateItem")
}), $(window).scroll(_.debounce(function () {
    var a = $(window).scrollTop();
    a > 10 ? ($("#layout-header").addClass("hidemenu"), TweenMax.staggerTo($("#module-topmenu"), .4, {height: "0px"}), TweenMax.staggerTo($("#module-mainmenu #logo"), .4, {top: "-47px"}), TweenMax.staggerTo($("#module-mainmenu #logo img"), .4, {"max-width": "90px"})) : ($("#layout-header").removeClass("hidemenu"), TweenMax.staggerTo($("#module-topmenu"), .4, {height: "37px"}), TweenMax.staggerTo($("#module-mainmenu #logo"), .4, {top: "-90px"}), TweenMax.staggerTo($("#module-mainmenu #logo img"), .4, {"max-width": "153px"})), a > 300 ? TweenMax.staggerTo($("#crollTop"), .5, {
        display: "block",
        bottom: "20px",
        opacity: 1
    }) : TweenMax.staggerTo($("#crollTop"), .5, {display: "none", bottom: "0px", opacity: 0})
}, 500)), function () {
    function a() {
        f.on("click", function () {
            $(this).addClass(i), TweenMax.to(h, .4, {css: {x: 250}, ease: Cubic.easeOut})
        })
    }

    function b() {
        g.on("click", function () {
            f.removeClass(i), TweenMax.to(h, .4, {css: {x: -250}, ease: Cubic.easeIn})
        }), $("body").click(function (a) {
            $(a.target).closest("#pullMenu,#combineMenu").length || (f.removeClass(i), TweenMax.to(h, .4, {
                css: {x: -250},
                ease: Cubic.easeIn
            }))
        })
    }

    function c() {
        h.on("click", "li>a", function (a) {
            var b = $(this);
            b.hasClass(i) ? ($(this).find(">i").text("+"), $(this).removeClass(i).parent().find(">ul").slideUp(j)) : (h.find("li>ul").slideUp(j), h.find("li>a").removeClass(i).find(">i").text("+"), $(this).find(">i").text("-"), $(this).addClass(i).parent().find(">ul").slideDown(j)), $(this).parent().find(">ul").length && a.preventDefault()
        })
    }

    function d() {
        h.find("li").has("ul").find(">a").append("<i>+</i>")
    }

    function e() {
    }

    var f = $("#pullMenu"), g = $("#closeMenu"), h = $("#combineMenu"), i = "expanded", j = 300;
    $(document).ready(function () {
        a(), b(), c(), d(), e()
    })
}(), $.fn.tmenu1 = function (a) {
    var b = {
        showArrow: !0,
        fadeSpeed: 200,
        resposiveBreak: 900,
        desktopWrapClass: "",
        mobileWrapClass: "",
        expandMenuClass: "",
        closeMenuClass: ""
    }, c = $.extend(b, a || {}), d = $(this), e = d.find("li"), f = d.find("li.active"), g = "tMenuHover", h = "tmExpd", i = "menuShow", j = ($(document).width(), $(window).height()), k = {
        _unbindEvents: function () {
            e.unbind("mouseenter mouseleave"), e.find("i").unbind("click")
        }, AddHeight: function () {
            $("body").css({height: j, overflow: "hidden"}), $("." + c.mobileWrapClass).css({height: j})
        }, RemoveHeight: function () {
            e.removeClass(h), $("." + c.expandMenuClass).removeClass(i), $("body,." + c.mobileWrapClass + ",." + c.desktopWrapClass).removeAttr("style")
        }, ShowArrow: function () {
            c.showArrow && e.has("ul").find(">a").append("<i>&nbsp</i>")
        }, AutoActive: function () {
            f.parents("li").addClass("active"), f.append("<b>&nbsp</b>")
        }, ExpandMobileMenu: function () {
            $("." + c.expandMenuClass).bind("click", function () {
                $(this).addClass(i).hide(), $("." + c.mobileWrapClass).show(), k.AddHeight()
            })
        }, CloseMenu: function () {
            $("." + c.closeMenuClass).bind("click", function () {
                $("." + c.expandMenuClass).removeClass(i).show(), $("." + c.mobileWrapClass).hide(), k.RemoveHeight()
            })
        }, BindEventMobile: function () {
            e.find("i").bind("click", function () {
                $(this).parent().parent().parent().hasClass(h) ? $(this).parent().parent().parent().removeClass(h).find(">ul").slideUp(c.fadeSpeed) : $(this).parent().parent().parent().addClass(h).find(">ul").slideDown(c.fadeSpeed)
            })
        }, BindEventDesktop: function () {
            e.bind("mouseenter", function () {
                $(this).addClass(g), $(this).find(">ul").fadeIn(c.fadeSpeed)
            }), e.bind("mouseleave", function () {
                $(this).removeClass(g), $(this).find(">ul").fadeOut(c.fadeSpeed)
            })
        }
    };
    k.BindEventDesktop()
}, $(document).ready(function () {
    $("#module-topmenu").find(".myMenu").tmenu1()
}), function () {
    $.fn.tMegaMenu = function (a) {
        var b = {fadeSpeed: 300, padding: 13}, c = $.extend(b, a || {}), d = $(this);
        $.each(d.find(">li"), function (a, b) {
            {
                var c = $(b).find(">ul"), d = $(b).attr("id");
                c.find("li")
            }
            c.appendTo($(".mainSub" + a).attr("parentid", d))
        }), d.find("li").on("mouseenter", function (a) {
            // a.preventDefault();
            // var b = ($(this).attr("id"), $(this).index()), d = $(this).find("a").attr("bg"), e = $(this).find("a").attr("pclass"), f = $(this).offset().left;
            // if ($(this).hasClass("menu-item-has-children")) {
            //     $(".mainSubmenu").hide(), $(this).hasClass("lastMenu") ? $(".mainSub" + b).find("ul").css("left", f + 2 * c.padding - $(this).outerWidth()) : $(".mainSub" + b).find("ul").css("left", f + c.padding);
            //     for (var g = Math.max($(window).width(), window.innerWidth), h = 0, i = e.split(","), j = 0; j < i.length; j++) {
            //         var k = i[j].split(":");
            //         if (g >= parseIntEx(k[0])) {
            //             h = k[1];
            //             break
            //         }
            //     }
            //     $(".mainSub" + b).css({
            //         backgroundImage: "url(" + d + ")",
            //         backgroundPosition: h + "px top"
            //     }).fadeIn(c.fadeSpeed)
            // } else $(".mainSubmenu").fadeOut(c.fadeSpeed)
        });
        var e = $(window).width();
        e >= 1100 && $(".mainSubmenu").on("mouseleave", function (a) {
            a.preventDefault(), $(this).fadeOut(c.fadeSpeed)
        }), $("#top-button").on("mouseenter", function (a) {
            a.preventDefault(), $(".mainSubmenu").fadeOut(c.fadeSpeed)
        })
    }, $(document).ready(function () {
        $("#module-mainmenu").find(".myMenu").tMegaMenu(), $("#layout-content").click(function (a) {
            $(a.target).closest(".mainSubmenu ul li").length || $(".mainSubmenu").fadeOut(300)
        }), $("#layout-header").click(function (a) {
            $(a.target).closest(".mainSubmenu ul li").length || $(".mainSubmenu").fadeOut(300)
        }), mainJs.autoRow({object: $(".mainSub4 li")}), mainJs.autoRow({object: $(".mainSub5 li")})
    })
}(), function () {
    function a() {
        var a = $(window).height(), b = ($("#module-home-slider .slick-slider .item").height(), $("#layout-header").height());
        $("#module-home-slider .slick-slider .item").css({height: a - b});
        var c = $(window).width();
        c >= 1100 && 1290 >= c && $("#module-home-slider .slick-slider .item").css({
            height: "525px",
            overflow: "hidden"
        }), c >= 740 && 1040 >= c && $("#module-home-slider .slick-slider .item").css({
            height: "425px",
            overflow: "hidden"
        }), $("#module-home-slider .slick-slider .item").css(c >= 480 && 570 > c ? {
            height: "270px",
            "background-size": "auto 160%"
        } : {"background-size": "cover"}), 480 > c && $("#module-home-slider .slick-slider .item").css({
            height: "250px",
            "background-size": "auto 135%"
        })
        //768 === c && $("#module-home-slider .slick-slider .item").css({"background-size": "auto 163%"}), console.log(c)
    }

    $("#module-home-slider").length && (mainJs.makeSlider({
        selector: "#module-home-slider", afterChange: function () {
            BackgroundCheck.refresh(), $("#module-home-slider .slick-dots .background--dark").hasClass("slick-active") ? ($("#module-home-slider .slick-dots li").css({"background-color": "#074888"}), $("#module-home-slider .slick-dots li.slick-active").css({"background-color": "#E3C390"})) : $("#module-home-slider .slick-dots .background--light").hasClass("slick-active") && ($("#module-home-slider .slick-dots li").css({"background-color": "#E3C390"}), $("#module-home-slider .slick-dots li.slick-active").css({"background-color": "#074888"}))
        }
    }), BackgroundCheck.init({
        targets: "#module-home-slider .slick-dots li",
        images: "#module-home-slider .lightSlider .item"
    })), $(document).ready(function () {
        $("#module-home-slider").find(".item").click(function () {
            location.href = $(this).attr("link-href")
        }), jDevicesSlider.addFunc({
            breakpoint: "desktop", enter: function () {
                a()
            }, exit: function () {
                $("#module-home-slider .slick-slider .item").css({height: "auto"})
            }
        })
    }), $(window).resize(function () {
        jDevicesSlider.addFunc({
            breakpoint: "desktop", enter: function () {
                a()
            }, exit: function () {
                $("#module-home-slider .slick-slider .item").css({height: "auto"})
            }
        })
    })
}(), function () {
    function a() {
        b.find(".steps .stepItem").bind("click", function (a) {
            if (a.preventDefault(), !g) {
                b.find(".steps .stepItem .overlay").show(), $(this).find(".overlay").hide();
                var c = $(this).index();
                h.slickGoTo(c)
            }
        })
    }

    var b = $("#module-parallax-slider"), c = new TimelineMax({paused: !0}), d = new TimelineMax({paused: !0}), e = new TimelineMax({paused: !0}), f = new TimelineMax({paused: !0}), g = !1, h = mainJs.makeSlider({
        selector: "#module-parallax-slider",
        auto: !0,
        delay: 5e3,
        dot: !1,
        fade: !0,
        speed: 1e3,
        beforeChange: function (a, h, i) {
            c.stop(), c.progress(0), d.stop(), d.progress(0), e.stop(), e.progress(0), f.stop(), f.progress(0), b.find(".steps .stepItem .overlay").show(), b.find(".steps .stepItem").eq(i).find(".overlay").hide(), g = !0
        },
        afterChange: function (a, b) {
            switch (b) {
                case 0:
                    c.play();
                    break;
                case 1:
                    d.play();
                    break;
                case 2:
                    e.play();
                    break;
                case 3:
                    f.play()
            }
            g = !1
        }
    });
    $(document).ready(function () {
        a()
    }), $(window).scroll(_.throttle(function () {
    }, 400))
}(), function () {
    function a() {
        $("#module-product-cate .inner").hover(function () {
            TweenMax.to($(this).find("img"), .4, {scale: 1.2}), TweenMax.to($(this).find(".readmore"), .4, {opacity: 1})
        }, function () {
            TweenMax.to($(this).find("img"), .4, {scale: 1}), TweenMax.to($(this).find(".readmore"), .4, {opacity: 0})
        })
    }

    function b() {
        var a = $(window).width();
        a >= 999 && 1290 >= a && ($("#module-product-cate .h1Title ").removeClass("animated"), $("#module-product-cate .width1280 ").removeClass("animated"))
    }

    $(document).ready(function () {
        a(), b()
    })
}(), function () {
    mainJs.makeSlider({selector: "#module-slider-gold"})
}(), function () {
    mainJs.makeSlider({selector: "#module-slider-silver"})
}(), function () {
    function a() {
        $("#mod-nhanhang-phongcach .inner").hover(function () {
            TweenMax.to($(this).find("img"), .4, {scale: 1.04}), TweenMax.to($(this).find(".mask"), .4, {
                opacity: 1,
                top: 0
            })
        }, function () {
            TweenMax.to($(this).find("img"), .4, {scale: 1}), TweenMax.to($(this).find(".mask"), .4, {
                opacity: 0,
                top: "100%"
            })
        })
    }

    $(document).ready(function () {
        a()
    })
}(), function () {
    function a(a) {
        var b = a.attr("ayear"), c = a.attr("aname"), d = a.attr("adesc");
        return {year: b, name: c, desc: d}
    }

    function b(a) {
        var b = $(".carouselWrap");
        b.find(".year").text(a.year), b.find(".name").text(a.name), b.find(".desc").text(a.desc)
    }

    function c(c) {
        return d = $("#achievementCarousel").waterwheelCarousel({
            opacityMultiplier: 1,
            flankingItems: c,
            separation: 220,
            sizeMultiplier: .6,
            horizonOffset: -10,
            movedToCenter: function (c) {
                data = a(c), b(data)
            }
        })
    }

    var d = null, e = null;
    $(document).ready(function () {
        var f = a($("#achievementCarousel").find("a:first>img"));
        b(f), e = $("#achievementCarousel").html(), d = c(2), $("#achiPrev").bind("click", function () {
            return d.prev(), !1
        }), $("#achiNext").bind("click", function () {
            return d.next(), !1
        })
    })
}(), function () {
    function a() {
        var a = $("#module-pnj-history"), b = Math.max($(window).width(), window.innerWidth);
        b >= 569 ? _.each(a.find(".item"), function (a) {
            var b = $(a).find(".pic"), c = b.height() / 2;
            TweenMax.to(b, .01, {opacity: 1, marginTop: -c})
        }) : _.each(a.find(".item"), function (a) {
            var b = $(a).find(".pic");
            TweenMax.set(b, {clearProps: "all"}), TweenMax.to(b, .6, {opacity: 1})
        })
    }

    function b() {
        var a = $("#module-pnj-history").find(".item:last");
        a.append('<div class="vline" style="height:' + a.outerHeight() / 2 + 'px"></div>')
    }

    function c() {
        $("#module-pnj-history").find(".vline").css("height", $("#module-pnj-history").find(".item:last").outerHeight() / 2)
    }

    $(window).load(function () {
        a(), b()
    }), $(window).resize(_.debounce(function () {
        a(), c()
    }, 500))
}(), function () {
    $("#module-collect-slider").length && (mainJs.makeSlider({
        selector: "#module-collect-slider",
        afterChange: function () {
            BackgroundCheck.refresh(), $("#module-collect-slider .slick-dots .background--dark").hasClass("slick-active") ? ($("#module-collect-slider .slick-dots li").css({"background-color": "#074888"}), $("#module-collect-slider .slick-dots li.slick-active").css({"background-color": "#E3C390"})) : $("#module-collect-slider .slick-dots .background--light").hasClass("slick-active") && ($("#module-collect-slider .slick-dots li").css({"background-color": "#E3C390"}), $("#module-collect-slider .slick-dots li.slick-active").css({"background-color": "#074888"}))
        }
    }), BackgroundCheck.init({
        targets: "#module-collect-slider .slick-dots li",
        images: "#module-collect-slider .lightSlider .item"
    }))
}(), function () {
    function a() {
        var a = $("#mod-collect-other");
        if (a.length) {
            a.find(".bxslider").slick({
                dots: !1,
                infinite: !1,
                centerMode: !1,
                autoplay: !1,
                autoplaySpeed: 3e3,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [{breakpoint: 1024, settings: {slidesToShow: 3}}, {
                    breakpoint: 569,
                    settings: {slidesToShow: 2}
                }, {breakpoint: 415, settings: {slidesToShow: 1}}],
                onSetPosition: function () {
                    var b = Math.max($(window).width(), window.innerWidth), c = 4, d = a.find(".slick-track");
                    1024 >= b && (c = 4), 569 >= b && (c = 2), 415 >= b && (c = 1), d.find(".item").length < c && d.css({
                        left: "50%",
                        marginLeft: -d.width() / 2
                    })
                }
            })
        }
    }

    $(document).ready(function () {
        a()
    })
}(), function () {
    $(document).ready(function () {
        mainJs.autoRow({object: $("#mod-gift-aboutus").find(".item"), itemInRow: 2, className: "myRow"})
    })
}(), function () {
    function a() {
        $(".modules-hover .wedding1 li").click(function () {
            if ($(this).hasClass("active"))$(this).removeClass("active"); else {
                $("modules-hover .wedding1 li").removeClass("active"), $(this).addClass("active");
                var a = $(this).text();
                $(".modules-hover #menu-item-543 a .text-Sp").text(a), $(".modules-hover #menu-item-543 a span").css({display: "block"})
            }
        }), $(".modules-hover .wedding2 li").click(function () {
            if ($(this).hasClass("active"))$(this).removeClass("active"); else {
                $(".modules-hover .wedding2 li").removeClass("active"), $(this).addClass("active");
                var a = $(this).text();
                $(".modules-hover #menu-item-344 a .text-Sp").text(a), $(".modules-hover #menu-item-344 a span").css({display: "block"})
            }
        }), $(".modules-hover .wedding3 li").click(function () {
            if ($(this).hasClass("active"))$(this).removeClass("active"); else {
                $(".modules-hover .wedding3 li").removeClass("active"), $(this).addClass("active");
                var a = $(this).text();
                $(".modules-hover #menu-item-867 a .text-Sp").text(a), $(".modules-hover #menu-item-867 a span").css({display: "block"})
            }
        }), $(".modules-hover .wedding4 li").click(function () {
            if ($(this).hasClass("active"))$(this).removeClass("active"); else {
                $(".modules-hover .wedding4 li").removeClass("active"), $(this).addClass("active");
                var a = $(this).text();
                $(".modules-hover #menu-item-867 a .text-Sp").text(a), $(".modules-hover #menu-item-867 a .closeX").css({display: "block"})
            }
        }), $(".modules-hover .wedding6 li").click(function () {
            if ($(this).hasClass("active"))$(this).removeClass("active"); else {
                $(".modules-hover .wedding6 li").removeClass("active"), $(this).addClass("active");
                var a = $(this).text();
                $(".modules-hover #menu-item-999 a .text-Sp").text(a), $(".modules-hover #menu-item-999 a span").css({display: "block"})
            }
        })
    }

    $.fn.lMegamenu = function (a) {
        var b = {fadeSpeed: 300, padding: 28}, c = $.extend(b, a || {}), d = $(this);
        $.each(d.find(">li"), function (a, b) {
            {
                var c = $(b).find(">ul"), d = $(b).attr("id");
                c.find("li")
            }
            c.appendTo($(".wedding" + a).attr("parentid", d))
        }), d.find("li").on("click", function (a) {
            a.preventDefault();
            var b = ($(this).attr("id"), $(this).index()), d = $(this).find("a").attr("pclass"), e = $(this).offset().left, f = $(".modules-hover .listSP").offset().top;
            if ($(".modules-hover .filterWrapSP").css({top: f - 77}), $(".modules-hover .filterWrapSP").css({display: "block"}), $(this).hasClass("menu-item-has-children")) {
                $(".filterSanpham").hide(), $(this).hasClass("lastMenu") ? $(".wedding" + b).find("ul").css("left", e + 4 * c.padding - $(this).outerWidth()) : $(".wedding" + b).find("ul").css("left", e + c.padding);
                for (var g = 980, h = 0, i = d.split(","), j = 0; j < i.length; j++) {
                    var k = i[j].split(":");
                    if (g >= parseIntEx(k[0])) {
                        h = k[1];
                        break
                    }
                }
                $(".wedding" + b).fadeIn(c.fadeSpeed)
            } else $(".filterSanpham").fadeOut(c.fadeSpeed)
        });
        var e = $(window).width();
        e >= 1100 && $(".filterSanpham").on("mouseleave", function (a) {
            a.preventDefault(), $(this).fadeOut(c.fadeSpeed), $(".modules-hover .filterWrapSP").css({display: "none"})
        })
    }, $(document).ready(function () {
        $(".modules-hover").find(".sanPhamWedding").lMegamenu(), a(), $(".modules-hover .icon-close").click(function (a) {
            $(a.target).closest(".filterSanpham ul li").length || $(".filterSanpham").fadeOut(300)
        }), mainJs.autoRow({object: $(".wedding1 li")}), mainJs.autoRow({object: $(".wedding2 li")}), mainJs.autoRow({object: $(".wedding3 li")}), mainJs.autoRow({object: $(".wedding5 li")})
    })
}(), function () {
    var a = $("#storeMap"), b = parseFloatEx(a.attr("lat")), c = parseFloatEx(a.attr("lon"));
    a.gMap([b, c], 17, "<p>Nội dung nhập vào đây</p>", "")
}(), function () {
    function a() {
        $(".storeWrap .pic").slick({slidesToShow: 1, slidesToScroll: 1, arrow: !0})
    }

    function b() {
        var a = $(".storeRow .item").index();
        //console.log(a)
    }

    $(document).ready(function () {
        a(), b()
    })
}(), function () {
    function a() {
        b.find(".myAccordion").myArrcordion(!0, !0)
    }

    var b = $("#mod-sp-consultant");
    $(document).ready(function () {
        a()
    })
}(), function () {
    $("#mod-sp-partner");
    $(document).ready(function () {
    })
}(), function () {
    function a() {
        var a = 0;
        $("#viewMap").on("click", function (b) {
            $(this).hide(), $(".picCompany").hide(), $("#viewPic").show(), $("#gmap").show(0, function () {
                $("#gmap").gMap([10.800844, 106.682213], 17, "<p>Nội dung nhập vào đây</p>", "")
            }), a > 0 || (a = 1, b.preventDefault())
        }), $("#viewPic").on("click", function (a) {
            a.preventDefault(), $(this).hide(), $(".picCompany").show(), $("#viewMap").show(), $("#gmap").hide()
        })
    }

    $(window).load(function () {
        a()
    })
}(), $(window).load(function () {
    $("#mod-news-detail").find(".detailL img").addClass("img-responsive")
}), function () {
    function a() {
        b.find(".accordion").myArrcordion(!0, !1)
    }

    var b = $("#mod-shareholders");
    $(document).ready(function () {
        a()
    })
}(), function () {
    function a() {
        $("#mod-brochure .inner").hover(function () {
            TweenMax.to($(this).find("img"), .4, {scale: 1.04})
        }, function () {
            TweenMax.to($(this).find("img"), .4, {scale: 1})
        })
    }

    $(document).ready(function () {
        a()
    })
}(), function () {
    function a() {
        var a = $(window).width();
        500 > a && $("#mod-footer-menu #menu-item-2775").after('<div class="clearfix hiddendestop"></div>')
    }

    $(document).ready(function () {
        a()
    })
}();