$(document).ready(function () {
    $('.hotel_star > .stars > span > a').on('click', function () {
        var index = $(this).index();
        var parent_div = $(this).closest('.hotel_star');
        parent_div.find('p').addClass('selected').end();
        parent_div.find('a').removeClass('active').end();
        parent_div.find('select').val(index + 1);
        $(this).addClass('active');
        return false;
    });
    $("#dk_user_email").keyup(function () {
        var user = $("#dk_user_email").val();
        $("#dk_user_login").val(user);
    });
    $("#payment_checkbox").on('change', function () {
        if ($(this).prop('checked')) {
            $(".cc_payment").hide();
        } else {
            $(".cc_payment").show();
        }
    });
});