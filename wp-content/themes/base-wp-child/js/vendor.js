!function (a, b) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = a.document ? b(a, !0) : function (a) {
        if (!a.document)throw new Error("jQuery requires a window with a document");
        return b(a)
    } : b(a)
}("undefined" != typeof window ? window : this, function (a, b) {
    function c(a) {
        var b = a.length, c = eb.type(a);
        return "function" === c || eb.isWindow(a) ? !1 : 1 === a.nodeType && b ? !0 : "array" === c || 0 === b || "number" == typeof b && b > 0 && b - 1 in a
    }

    function d(a, b, c) {
        if (eb.isFunction(b))return eb.grep(a, function (a, d) {
            return !!b.call(a, d, a) !== c
        });
        if (b.nodeType)return eb.grep(a, function (a) {
            return a === b !== c
        });
        if ("string" == typeof b) {
            if (mb.test(b))return eb.filter(b, a, c);
            b = eb.filter(b, a)
        }
        return eb.grep(a, function (a) {
            return eb.inArray(a, b) >= 0 !== c
        })
    }

    function e(a, b) {
        do a = a[b]; while (a && 1 !== a.nodeType);
        return a
    }

    function f(a) {
        var b = ub[a] = {};
        return eb.each(a.match(tb) || [], function (a, c) {
            b[c] = !0
        }), b
    }

    function g() {
        ob.addEventListener ? (ob.removeEventListener("DOMContentLoaded", h, !1), a.removeEventListener("load", h, !1)) : (ob.detachEvent("onreadystatechange", h), a.detachEvent("onload", h))
    }

    function h() {
        (ob.addEventListener || "load" === event.type || "complete" === ob.readyState) && (g(), eb.ready())
    }

    function i(a, b, c) {
        if (void 0 === c && 1 === a.nodeType) {
            var d = "data-" + b.replace(zb, "-$1").toLowerCase();
            if (c = a.getAttribute(d), "string" == typeof c) {
                try {
                    c = "true" === c ? !0 : "false" === c ? !1 : "null" === c ? null : +c + "" === c ? +c : yb.test(c) ? eb.parseJSON(c) : c
                } catch (e) {
                }
                eb.data(a, b, c)
            } else c = void 0
        }
        return c
    }

    function j(a) {
        var b;
        for (b in a)if (("data" !== b || !eb.isEmptyObject(a[b])) && "toJSON" !== b)return !1;
        return !0
    }

    function k(a, b, c, d) {
        if (eb.acceptData(a)) {
            var e, f, g = eb.expando, h = a.nodeType, i = h ? eb.cache : a, j = h ? a[g] : a[g] && g;
            if (j && i[j] && (d || i[j].data) || void 0 !== c || "string" != typeof b)return j || (j = h ? a[g] = W.pop() || eb.guid++ : g), i[j] || (i[j] = h ? {} : {toJSON: eb.noop}), ("object" == typeof b || "function" == typeof b) && (d ? i[j] = eb.extend(i[j], b) : i[j].data = eb.extend(i[j].data, b)), f = i[j], d || (f.data || (f.data = {}), f = f.data), void 0 !== c && (f[eb.camelCase(b)] = c), "string" == typeof b ? (e = f[b], null == e && (e = f[eb.camelCase(b)])) : e = f, e
        }
    }

    function l(a, b, c) {
        if (eb.acceptData(a)) {
            var d, e, f = a.nodeType, g = f ? eb.cache : a, h = f ? a[eb.expando] : eb.expando;
            if (g[h]) {
                if (b && (d = c ? g[h] : g[h].data)) {
                    eb.isArray(b) ? b = b.concat(eb.map(b, eb.camelCase)) : b in d ? b = [b] : (b = eb.camelCase(b), b = b in d ? [b] : b.split(" ")), e = b.length;
                    for (; e--;)delete d[b[e]];
                    if (c ? !j(d) : !eb.isEmptyObject(d))return
                }
                (c || (delete g[h].data, j(g[h]))) && (f ? eb.cleanData([a], !0) : cb.deleteExpando || g != g.window ? delete g[h] : g[h] = null)
            }
        }
    }

    function m() {
        return !0
    }

    function n() {
        return !1
    }

    function o() {
        try {
            return ob.activeElement
        } catch (a) {
        }
    }

    function p(a) {
        var b = Kb.split("|"), c = a.createDocumentFragment();
        if (c.createElement)for (; b.length;)c.createElement(b.pop());
        return c
    }

    function q(a, b) {
        var c, d, e = 0, f = typeof a.getElementsByTagName !== xb ? a.getElementsByTagName(b || "*") : typeof a.querySelectorAll !== xb ? a.querySelectorAll(b || "*") : void 0;
        if (!f)for (f = [], c = a.childNodes || a; null != (d = c[e]); e++)!b || eb.nodeName(d, b) ? f.push(d) : eb.merge(f, q(d, b));
        return void 0 === b || b && eb.nodeName(a, b) ? eb.merge([a], f) : f
    }

    function r(a) {
        Eb.test(a.type) && (a.defaultChecked = a.checked)
    }

    function s(a, b) {
        return eb.nodeName(a, "table") && eb.nodeName(11 !== b.nodeType ? b : b.firstChild, "tr") ? a.getElementsByTagName("tbody")[0] || a.appendChild(a.ownerDocument.createElement("tbody")) : a
    }

    function t(a) {
        return a.type = (null !== eb.find.attr(a, "type")) + "/" + a.type, a
    }

    function u(a) {
        var b = Vb.exec(a.type);
        return b ? a.type = b[1] : a.removeAttribute("type"), a
    }

    function v(a, b) {
        for (var c, d = 0; null != (c = a[d]); d++)eb._data(c, "globalEval", !b || eb._data(b[d], "globalEval"))
    }

    function w(a, b) {
        if (1 === b.nodeType && eb.hasData(a)) {
            var c, d, e, f = eb._data(a), g = eb._data(b, f), h = f.events;
            if (h) {
                delete g.handle, g.events = {};
                for (c in h)for (d = 0, e = h[c].length; e > d; d++)eb.event.add(b, c, h[c][d])
            }
            g.data && (g.data = eb.extend({}, g.data))
        }
    }

    function x(a, b) {
        var c, d, e;
        if (1 === b.nodeType) {
            if (c = b.nodeName.toLowerCase(), !cb.noCloneEvent && b[eb.expando]) {
                e = eb._data(b);
                for (d in e.events)eb.removeEvent(b, d, e.handle);
                b.removeAttribute(eb.expando)
            }
            "script" === c && b.text !== a.text ? (t(b).text = a.text, u(b)) : "object" === c ? (b.parentNode && (b.outerHTML = a.outerHTML), cb.html5Clone && a.innerHTML && !eb.trim(b.innerHTML) && (b.innerHTML = a.innerHTML)) : "input" === c && Eb.test(a.type) ? (b.defaultChecked = b.checked = a.checked, b.value !== a.value && (b.value = a.value)) : "option" === c ? b.defaultSelected = b.selected = a.defaultSelected : ("input" === c || "textarea" === c) && (b.defaultValue = a.defaultValue)
        }
    }

    function y(b, c) {
        var d, e = eb(c.createElement(b)).appendTo(c.body), f = a.getDefaultComputedStyle && (d = a.getDefaultComputedStyle(e[0])) ? d.display : eb.css(e[0], "display");
        return e.detach(), f
    }

    function z(a) {
        var b = ob, c = _b[a];
        return c || (c = y(a, b), "none" !== c && c || ($b = ($b || eb("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement), b = ($b[0].contentWindow || $b[0].contentDocument).document, b.write(), b.close(), c = y(a, b), $b.detach()), _b[a] = c), c
    }

    function A(a, b) {
        return {
            get: function () {
                var c = a();
                if (null != c)return c ? void delete this.get : (this.get = b).apply(this, arguments)
            }
        }
    }

    function B(a, b) {
        if (b in a)return b;
        for (var c = b.charAt(0).toUpperCase() + b.slice(1), d = b, e = mc.length; e--;)if (b = mc[e] + c, b in a)return b;
        return d
    }

    function C(a, b) {
        for (var c, d, e, f = [], g = 0, h = a.length; h > g; g++)d = a[g], d.style && (f[g] = eb._data(d, "olddisplay"), c = d.style.display, b ? (f[g] || "none" !== c || (d.style.display = ""), "" === d.style.display && Cb(d) && (f[g] = eb._data(d, "olddisplay", z(d.nodeName)))) : (e = Cb(d), (c && "none" !== c || !e) && eb._data(d, "olddisplay", e ? c : eb.css(d, "display"))));
        for (g = 0; h > g; g++)d = a[g], d.style && (b && "none" !== d.style.display && "" !== d.style.display || (d.style.display = b ? f[g] || "" : "none"));
        return a
    }

    function D(a, b, c) {
        var d = ic.exec(b);
        return d ? Math.max(0, d[1] - (c || 0)) + (d[2] || "px") : b
    }

    function E(a, b, c, d, e) {
        for (var f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0, g = 0; 4 > f; f += 2)"margin" === c && (g += eb.css(a, c + Bb[f], !0, e)), d ? ("content" === c && (g -= eb.css(a, "padding" + Bb[f], !0, e)), "margin" !== c && (g -= eb.css(a, "border" + Bb[f] + "Width", !0, e))) : (g += eb.css(a, "padding" + Bb[f], !0, e), "padding" !== c && (g += eb.css(a, "border" + Bb[f] + "Width", !0, e)));
        return g
    }

    function F(a, b, c) {
        var d = !0, e = "width" === b ? a.offsetWidth : a.offsetHeight, f = ac(a), g = cb.boxSizing && "border-box" === eb.css(a, "boxSizing", !1, f);
        if (0 >= e || null == e) {
            if (e = bc(a, b, f), (0 > e || null == e) && (e = a.style[b]), dc.test(e))return e;
            d = g && (cb.boxSizingReliable() || e === a.style[b]), e = parseFloat(e) || 0
        }
        return e + E(a, b, c || (g ? "border" : "content"), d, f) + "px"
    }

    function G(a, b, c, d, e) {
        return new G.prototype.init(a, b, c, d, e)
    }

    function H() {
        return setTimeout(function () {
            nc = void 0
        }), nc = eb.now()
    }

    function I(a, b) {
        var c, d = {height: a}, e = 0;
        for (b = b ? 1 : 0; 4 > e; e += 2 - b)c = Bb[e], d["margin" + c] = d["padding" + c] = a;
        return b && (d.opacity = d.width = a), d
    }

    function J(a, b, c) {
        for (var d, e = (tc[b] || []).concat(tc["*"]), f = 0, g = e.length; g > f; f++)if (d = e[f].call(c, b, a))return d
    }

    function K(a, b, c) {
        var d, e, f, g, h, i, j, k, l = this, m = {}, n = a.style, o = a.nodeType && Cb(a), p = eb._data(a, "fxshow");
        c.queue || (h = eb._queueHooks(a, "fx"), null == h.unqueued && (h.unqueued = 0, i = h.empty.fire, h.empty.fire = function () {
            h.unqueued || i()
        }), h.unqueued++, l.always(function () {
            l.always(function () {
                h.unqueued--, eb.queue(a, "fx").length || h.empty.fire()
            })
        })), 1 === a.nodeType && ("height"in b || "width"in b) && (c.overflow = [n.overflow, n.overflowX, n.overflowY], j = eb.css(a, "display"), k = "none" === j ? eb._data(a, "olddisplay") || z(a.nodeName) : j, "inline" === k && "none" === eb.css(a, "float") && (cb.inlineBlockNeedsLayout && "inline" !== z(a.nodeName) ? n.zoom = 1 : n.display = "inline-block")), c.overflow && (n.overflow = "hidden", cb.shrinkWrapBlocks() || l.always(function () {
            n.overflow = c.overflow[0], n.overflowX = c.overflow[1], n.overflowY = c.overflow[2]
        }));
        for (d in b)if (e = b[d], pc.exec(e)) {
            if (delete b[d], f = f || "toggle" === e, e === (o ? "hide" : "show")) {
                if ("show" !== e || !p || void 0 === p[d])continue;
                o = !0
            }
            m[d] = p && p[d] || eb.style(a, d)
        } else j = void 0;
        if (eb.isEmptyObject(m))"inline" === ("none" === j ? z(a.nodeName) : j) && (n.display = j); else {
            p ? "hidden"in p && (o = p.hidden) : p = eb._data(a, "fxshow", {}), f && (p.hidden = !o), o ? eb(a).show() : l.done(function () {
                eb(a).hide()
            }), l.done(function () {
                var b;
                eb._removeData(a, "fxshow");
                for (b in m)eb.style(a, b, m[b])
            });
            for (d in m)g = J(o ? p[d] : 0, d, l), d in p || (p[d] = g.start, o && (g.end = g.start, g.start = "width" === d || "height" === d ? 1 : 0))
        }
    }

    function L(a, b) {
        var c, d, e, f, g;
        for (c in a)if (d = eb.camelCase(c), e = b[d], f = a[c], eb.isArray(f) && (e = f[1], f = a[c] = f[0]), c !== d && (a[d] = f, delete a[c]), g = eb.cssHooks[d], g && "expand"in g) {
            f = g.expand(f), delete a[d];
            for (c in f)c in a || (a[c] = f[c], b[c] = e)
        } else b[d] = e
    }

    function M(a, b, c) {
        var d, e, f = 0, g = sc.length, h = eb.Deferred().always(function () {
            delete i.elem
        }), i = function () {
            if (e)return !1;
            for (var b = nc || H(), c = Math.max(0, j.startTime + j.duration - b), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; i > g; g++)j.tweens[g].run(f);
            return h.notifyWith(a, [j, f, c]), 1 > f && i ? c : (h.resolveWith(a, [j]), !1)
        }, j = h.promise({
            elem: a,
            props: eb.extend({}, b),
            opts: eb.extend(!0, {specialEasing: {}}, c),
            originalProperties: b,
            originalOptions: c,
            startTime: nc || H(),
            duration: c.duration,
            tweens: [],
            createTween: function (b, c) {
                var d = eb.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
                return j.tweens.push(d), d
            },
            stop: function (b) {
                var c = 0, d = b ? j.tweens.length : 0;
                if (e)return this;
                for (e = !0; d > c; c++)j.tweens[c].run(1);
                return b ? h.resolveWith(a, [j, b]) : h.rejectWith(a, [j, b]), this
            }
        }), k = j.props;
        for (L(k, j.opts.specialEasing); g > f; f++)if (d = sc[f].call(j, a, k, j.opts))return d;
        return eb.map(k, J, j), eb.isFunction(j.opts.start) && j.opts.start.call(a, j), eb.fx.timer(eb.extend(i, {
            elem: a,
            anim: j,
            queue: j.opts.queue
        })), j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always)
    }

    function N(a) {
        return function (b, c) {
            "string" != typeof b && (c = b, b = "*");
            var d, e = 0, f = b.toLowerCase().match(tb) || [];
            if (eb.isFunction(c))for (; d = f[e++];)"+" === d.charAt(0) ? (d = d.slice(1) || "*", (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c)
        }
    }

    function O(a, b, c, d) {
        function e(h) {
            var i;
            return f[h] = !0, eb.each(a[h] || [], function (a, h) {
                var j = h(b, c, d);
                return "string" != typeof j || g || f[j] ? g ? !(i = j) : void 0 : (b.dataTypes.unshift(j), e(j), !1)
            }), i
        }

        var f = {}, g = a === Rc;
        return e(b.dataTypes[0]) || !f["*"] && e("*")
    }

    function P(a, b) {
        var c, d, e = eb.ajaxSettings.flatOptions || {};
        for (d in b)void 0 !== b[d] && ((e[d] ? a : c || (c = {}))[d] = b[d]);
        return c && eb.extend(!0, a, c), a
    }

    function Q(a, b, c) {
        for (var d, e, f, g, h = a.contents, i = a.dataTypes; "*" === i[0];)i.shift(), void 0 === e && (e = a.mimeType || b.getResponseHeader("Content-Type"));
        if (e)for (g in h)if (h[g] && h[g].test(e)) {
            i.unshift(g);
            break
        }
        if (i[0]in c)f = i[0]; else {
            for (g in c) {
                if (!i[0] || a.converters[g + " " + i[0]]) {
                    f = g;
                    break
                }
                d || (d = g)
            }
            f = f || d
        }
        return f ? (f !== i[0] && i.unshift(f), c[f]) : void 0
    }

    function R(a, b, c, d) {
        var e, f, g, h, i, j = {}, k = a.dataTypes.slice();
        if (k[1])for (g in a.converters)j[g.toLowerCase()] = a.converters[g];
        for (f = k.shift(); f;)if (a.responseFields[f] && (c[a.responseFields[f]] = b), !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), i = f, f = k.shift())if ("*" === f)f = i; else if ("*" !== i && i !== f) {
            if (g = j[i + " " + f] || j["* " + f], !g)for (e in j)if (h = e.split(" "), h[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]])) {
                g === !0 ? g = j[e] : j[e] !== !0 && (f = h[0], k.unshift(h[1]));
                break
            }
            if (g !== !0)if (g && a["throws"])b = g(b); else try {
                b = g(b)
            } catch (l) {
                return {state: "parsererror", error: g ? l : "No conversion from " + i + " to " + f}
            }
        }
        return {state: "success", data: b}
    }

    function S(a, b, c, d) {
        var e;
        if (eb.isArray(b))eb.each(b, function (b, e) {
            c || Vc.test(a) ? d(a, e) : S(a + "[" + ("object" == typeof e ? b : "") + "]", e, c, d)
        }); else if (c || "object" !== eb.type(b))d(a, b); else for (e in b)S(a + "[" + e + "]", b[e], c, d)
    }

    function T() {
        try {
            return new a.XMLHttpRequest
        } catch (b) {
        }
    }

    function U() {
        try {
            return new a.ActiveXObject("Microsoft.XMLHTTP")
        } catch (b) {
        }
    }

    function V(a) {
        return eb.isWindow(a) ? a : 9 === a.nodeType ? a.defaultView || a.parentWindow : !1
    }

    var W = [], X = W.slice, Y = W.concat, Z = W.push, $ = W.indexOf, _ = {}, ab = _.toString, bb = _.hasOwnProperty, cb = {}, db = "1.11.1", eb = function (a, b) {
        return new eb.fn.init(a, b)
    }, fb = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, gb = /^-ms-/, hb = /-([\da-z])/gi, ib = function (a, b) {
        return b.toUpperCase()
    };
    eb.fn = eb.prototype = {
        jquery: db, constructor: eb, selector: "", length: 0, toArray: function () {
            return X.call(this)
        }, get: function (a) {
            return null != a ? 0 > a ? this[a + this.length] : this[a] : X.call(this)
        }, pushStack: function (a) {
            var b = eb.merge(this.constructor(), a);
            return b.prevObject = this, b.context = this.context, b
        }, each: function (a, b) {
            return eb.each(this, a, b)
        }, map: function (a) {
            return this.pushStack(eb.map(this, function (b, c) {
                return a.call(b, c, b)
            }))
        }, slice: function () {
            return this.pushStack(X.apply(this, arguments))
        }, first: function () {
            return this.eq(0)
        }, last: function () {
            return this.eq(-1)
        }, eq: function (a) {
            var b = this.length, c = +a + (0 > a ? b : 0);
            return this.pushStack(c >= 0 && b > c ? [this[c]] : [])
        }, end: function () {
            return this.prevObject || this.constructor(null)
        }, push: Z, sort: W.sort, splice: W.splice
    }, eb.extend = eb.fn.extend = function () {
        var a, b, c, d, e, f, g = arguments[0] || {}, h = 1, i = arguments.length, j = !1;
        for ("boolean" == typeof g && (j = g, g = arguments[h] || {}, h++), "object" == typeof g || eb.isFunction(g) || (g = {}), h === i && (g = this, h--); i > h; h++)if (null != (e = arguments[h]))for (d in e)a = g[d], c = e[d], g !== c && (j && c && (eb.isPlainObject(c) || (b = eb.isArray(c))) ? (b ? (b = !1, f = a && eb.isArray(a) ? a : []) : f = a && eb.isPlainObject(a) ? a : {}, g[d] = eb.extend(j, f, c)) : void 0 !== c && (g[d] = c));
        return g
    }, eb.extend({
        expando: "jQuery" + (db + Math.random()).replace(/\D/g, ""), isReady: !0, error: function (a) {
            throw new Error(a)
        }, noop: function () {
        }, isFunction: function (a) {
            return "function" === eb.type(a)
        }, isArray: Array.isArray || function (a) {
            return "array" === eb.type(a)
        }, isWindow: function (a) {
            return null != a && a == a.window
        }, isNumeric: function (a) {
            return !eb.isArray(a) && a - parseFloat(a) >= 0
        }, isEmptyObject: function (a) {
            var b;
            for (b in a)return !1;
            return !0
        }, isPlainObject: function (a) {
            var b;
            if (!a || "object" !== eb.type(a) || a.nodeType || eb.isWindow(a))return !1;
            try {
                if (a.constructor && !bb.call(a, "constructor") && !bb.call(a.constructor.prototype, "isPrototypeOf"))return !1
            } catch (c) {
                return !1
            }
            if (cb.ownLast)for (b in a)return bb.call(a, b);
            for (b in a);
            return void 0 === b || bb.call(a, b)
        }, type: function (a) {
            return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? _[ab.call(a)] || "object" : typeof a
        }, globalEval: function (b) {
            b && eb.trim(b) && (a.execScript || function (b) {
                a.eval.call(a, b)
            })(b)
        }, camelCase: function (a) {
            return a.replace(gb, "ms-").replace(hb, ib)
        }, nodeName: function (a, b) {
            return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase()
        }, each: function (a, b, d) {
            var e, f = 0, g = a.length, h = c(a);
            if (d) {
                if (h)for (; g > f && (e = b.apply(a[f], d), e !== !1); f++); else for (f in a)if (e = b.apply(a[f], d), e === !1)break
            } else if (h)for (; g > f && (e = b.call(a[f], f, a[f]), e !== !1); f++); else for (f in a)if (e = b.call(a[f], f, a[f]), e === !1)break;
            return a
        }, trim: function (a) {
            return null == a ? "" : (a + "").replace(fb, "")
        }, makeArray: function (a, b) {
            var d = b || [];
            return null != a && (c(Object(a)) ? eb.merge(d, "string" == typeof a ? [a] : a) : Z.call(d, a)), d
        }, inArray: function (a, b, c) {
            var d;
            if (b) {
                if ($)return $.call(b, a, c);
                for (d = b.length, c = c ? 0 > c ? Math.max(0, d + c) : c : 0; d > c; c++)if (c in b && b[c] === a)return c
            }
            return -1
        }, merge: function (a, b) {
            for (var c = +b.length, d = 0, e = a.length; c > d;)a[e++] = b[d++];
            if (c !== c)for (; void 0 !== b[d];)a[e++] = b[d++];
            return a.length = e, a
        }, grep: function (a, b, c) {
            for (var d, e = [], f = 0, g = a.length, h = !c; g > f; f++)d = !b(a[f], f), d !== h && e.push(a[f]);
            return e
        }, map: function (a, b, d) {
            var e, f = 0, g = a.length, h = c(a), i = [];
            if (h)for (; g > f; f++)e = b(a[f], f, d), null != e && i.push(e); else for (f in a)e = b(a[f], f, d), null != e && i.push(e);
            return Y.apply([], i)
        }, guid: 1, proxy: function (a, b) {
            var c, d, e;
            return "string" == typeof b && (e = a[b], b = a, a = e), eb.isFunction(a) ? (c = X.call(arguments, 2), d = function () {
                return a.apply(b || this, c.concat(X.call(arguments)))
            }, d.guid = a.guid = a.guid || eb.guid++, d) : void 0
        }, now: function () {
            return +new Date
        }, support: cb
    }), eb.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function (a, b) {
        _["[object " + b + "]"] = b.toLowerCase()
    });
    var jb = function (a) {
        function b(a, b, c, d) {
            var e, f, g, h, i, j, l, n, o, p;
            if ((b ? b.ownerDocument || b : O) !== G && F(b), b = b || G, c = c || [], !a || "string" != typeof a)return c;
            if (1 !== (h = b.nodeType) && 9 !== h)return [];
            if (I && !d) {
                if (e = sb.exec(a))if (g = e[1]) {
                    if (9 === h) {
                        if (f = b.getElementById(g), !f || !f.parentNode)return c;
                        if (f.id === g)return c.push(f), c
                    } else if (b.ownerDocument && (f = b.ownerDocument.getElementById(g)) && M(b, f) && f.id === g)return c.push(f), c
                } else {
                    if (e[2])return _.apply(c, b.getElementsByTagName(a)), c;
                    if ((g = e[3]) && v.getElementsByClassName && b.getElementsByClassName)return _.apply(c, b.getElementsByClassName(g)), c
                }
                if (v.qsa && (!J || !J.test(a))) {
                    if (n = l = N, o = b, p = 9 === h && a, 1 === h && "object" !== b.nodeName.toLowerCase()) {
                        for (j = z(a), (l = b.getAttribute("id")) ? n = l.replace(ub, "\\$&") : b.setAttribute("id", n), n = "[id='" + n + "'] ", i = j.length; i--;)j[i] = n + m(j[i]);
                        o = tb.test(a) && k(b.parentNode) || b, p = j.join(",")
                    }
                    if (p)try {
                        return _.apply(c, o.querySelectorAll(p)), c
                    } catch (q) {
                    } finally {
                        l || b.removeAttribute("id")
                    }
                }
            }
            return B(a.replace(ib, "$1"), b, c, d)
        }

        function c() {
            function a(c, d) {
                return b.push(c + " ") > w.cacheLength && delete a[b.shift()], a[c + " "] = d
            }

            var b = [];
            return a
        }

        function d(a) {
            return a[N] = !0, a
        }

        function e(a) {
            var b = G.createElement("div");
            try {
                return !!a(b)
            } catch (c) {
                return !1
            } finally {
                b.parentNode && b.parentNode.removeChild(b), b = null
            }
        }

        function f(a, b) {
            for (var c = a.split("|"), d = a.length; d--;)w.attrHandle[c[d]] = b
        }

        function g(a, b) {
            var c = b && a, d = c && 1 === a.nodeType && 1 === b.nodeType && (~b.sourceIndex || W) - (~a.sourceIndex || W);
            if (d)return d;
            if (c)for (; c = c.nextSibling;)if (c === b)return -1;
            return a ? 1 : -1
        }

        function h(a) {
            return function (b) {
                var c = b.nodeName.toLowerCase();
                return "input" === c && b.type === a
            }
        }

        function i(a) {
            return function (b) {
                var c = b.nodeName.toLowerCase();
                return ("input" === c || "button" === c) && b.type === a
            }
        }

        function j(a) {
            return d(function (b) {
                return b = +b, d(function (c, d) {
                    for (var e, f = a([], c.length, b), g = f.length; g--;)c[e = f[g]] && (c[e] = !(d[e] = c[e]))
                })
            })
        }

        function k(a) {
            return a && typeof a.getElementsByTagName !== V && a
        }

        function l() {
        }

        function m(a) {
            for (var b = 0, c = a.length, d = ""; c > b; b++)d += a[b].value;
            return d
        }

        function n(a, b, c) {
            var d = b.dir, e = c && "parentNode" === d, f = Q++;
            return b.first ? function (b, c, f) {
                for (; b = b[d];)if (1 === b.nodeType || e)return a(b, c, f)
            } : function (b, c, g) {
                var h, i, j = [P, f];
                if (g) {
                    for (; b = b[d];)if ((1 === b.nodeType || e) && a(b, c, g))return !0
                } else for (; b = b[d];)if (1 === b.nodeType || e) {
                    if (i = b[N] || (b[N] = {}), (h = i[d]) && h[0] === P && h[1] === f)return j[2] = h[2];
                    if (i[d] = j, j[2] = a(b, c, g))return !0
                }
            }
        }

        function o(a) {
            return a.length > 1 ? function (b, c, d) {
                for (var e = a.length; e--;)if (!a[e](b, c, d))return !1;
                return !0
            } : a[0]
        }

        function p(a, c, d) {
            for (var e = 0, f = c.length; f > e; e++)b(a, c[e], d);
            return d
        }

        function q(a, b, c, d, e) {
            for (var f, g = [], h = 0, i = a.length, j = null != b; i > h; h++)(f = a[h]) && (!c || c(f, d, e)) && (g.push(f), j && b.push(h));
            return g
        }

        function r(a, b, c, e, f, g) {
            return e && !e[N] && (e = r(e)), f && !f[N] && (f = r(f, g)), d(function (d, g, h, i) {
                var j, k, l, m = [], n = [], o = g.length, r = d || p(b || "*", h.nodeType ? [h] : h, []), s = !a || !d && b ? r : q(r, m, a, h, i), t = c ? f || (d ? a : o || e) ? [] : g : s;
                if (c && c(s, t, h, i), e)for (j = q(t, n), e(j, [], h, i), k = j.length; k--;)(l = j[k]) && (t[n[k]] = !(s[n[k]] = l));
                if (d) {
                    if (f || a) {
                        if (f) {
                            for (j = [], k = t.length; k--;)(l = t[k]) && j.push(s[k] = l);
                            f(null, t = [], j, i)
                        }
                        for (k = t.length; k--;)(l = t[k]) && (j = f ? bb.call(d, l) : m[k]) > -1 && (d[j] = !(g[j] = l))
                    }
                } else t = q(t === g ? t.splice(o, t.length) : t), f ? f(null, g, t, i) : _.apply(g, t)
            })
        }

        function s(a) {
            for (var b, c, d, e = a.length, f = w.relative[a[0].type], g = f || w.relative[" "], h = f ? 1 : 0, i = n(function (a) {
                return a === b
            }, g, !0), j = n(function (a) {
                return bb.call(b, a) > -1
            }, g, !0), k = [function (a, c, d) {
                return !f && (d || c !== C) || ((b = c).nodeType ? i(a, c, d) : j(a, c, d))
            }]; e > h; h++)if (c = w.relative[a[h].type])k = [n(o(k), c)]; else {
                if (c = w.filter[a[h].type].apply(null, a[h].matches), c[N]) {
                    for (d = ++h; e > d && !w.relative[a[d].type]; d++);
                    return r(h > 1 && o(k), h > 1 && m(a.slice(0, h - 1).concat({value: " " === a[h - 2].type ? "*" : ""})).replace(ib, "$1"), c, d > h && s(a.slice(h, d)), e > d && s(a = a.slice(d)), e > d && m(a))
                }
                k.push(c)
            }
            return o(k)
        }

        function t(a, c) {
            var e = c.length > 0, f = a.length > 0, g = function (d, g, h, i, j) {
                var k, l, m, n = 0, o = "0", p = d && [], r = [], s = C, t = d || f && w.find.TAG("*", j), u = P += null == s ? 1 : Math.random() || .1, v = t.length;
                for (j && (C = g !== G && g); o !== v && null != (k = t[o]); o++) {
                    if (f && k) {
                        for (l = 0; m = a[l++];)if (m(k, g, h)) {
                            i.push(k);
                            break
                        }
                        j && (P = u)
                    }
                    e && ((k = !m && k) && n--, d && p.push(k))
                }
                if (n += o, e && o !== n) {
                    for (l = 0; m = c[l++];)m(p, r, g, h);
                    if (d) {
                        if (n > 0)for (; o--;)p[o] || r[o] || (r[o] = Z.call(i));
                        r = q(r)
                    }
                    _.apply(i, r), j && !d && r.length > 0 && n + c.length > 1 && b.uniqueSort(i)
                }
                return j && (P = u, C = s), p
            };
            return e ? d(g) : g
        }

        var u, v, w, x, y, z, A, B, C, D, E, F, G, H, I, J, K, L, M, N = "sizzle" + -new Date, O = a.document, P = 0, Q = 0, R = c(), S = c(), T = c(), U = function (a, b) {
            return a === b && (E = !0), 0
        }, V = "undefined", W = 1 << 31, X = {}.hasOwnProperty, Y = [], Z = Y.pop, $ = Y.push, _ = Y.push, ab = Y.slice, bb = Y.indexOf || function (a) {
                for (var b = 0, c = this.length; c > b; b++)if (this[b] === a)return b;
                return -1
            }, cb = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", db = "[\\x20\\t\\r\\n\\f]", eb = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", fb = eb.replace("w", "w#"), gb = "\\[" + db + "*(" + eb + ")(?:" + db + "*([*^$|!~]?=)" + db + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + fb + "))|)" + db + "*\\]", hb = ":(" + eb + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + gb + ")*)|.*)\\)|)", ib = new RegExp("^" + db + "+|((?:^|[^\\\\])(?:\\\\.)*)" + db + "+$", "g"), jb = new RegExp("^" + db + "*," + db + "*"), kb = new RegExp("^" + db + "*([>+~]|" + db + ")" + db + "*"), lb = new RegExp("=" + db + "*([^\\]'\"]*?)" + db + "*\\]", "g"), mb = new RegExp(hb), nb = new RegExp("^" + fb + "$"), ob = {
            ID: new RegExp("^#(" + eb + ")"),
            CLASS: new RegExp("^\\.(" + eb + ")"),
            TAG: new RegExp("^(" + eb.replace("w", "w*") + ")"),
            ATTR: new RegExp("^" + gb),
            PSEUDO: new RegExp("^" + hb),
            CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + db + "*(even|odd|(([+-]|)(\\d*)n|)" + db + "*(?:([+-]|)" + db + "*(\\d+)|))" + db + "*\\)|)", "i"),
            bool: new RegExp("^(?:" + cb + ")$", "i"),
            needsContext: new RegExp("^" + db + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + db + "*((?:-\\d)?\\d*)" + db + "*\\)|)(?=[^-]|$)", "i")
        }, pb = /^(?:input|select|textarea|button)$/i, qb = /^h\d$/i, rb = /^[^{]+\{\s*\[native \w/, sb = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, tb = /[+~]/, ub = /'|\\/g, vb = new RegExp("\\\\([\\da-f]{1,6}" + db + "?|(" + db + ")|.)", "ig"), wb = function (a, b, c) {
            var d = "0x" + b - 65536;
            return d !== d || c ? b : 0 > d ? String.fromCharCode(d + 65536) : String.fromCharCode(d >> 10 | 55296, 1023 & d | 56320)
        };
        try {
            _.apply(Y = ab.call(O.childNodes), O.childNodes), Y[O.childNodes.length].nodeType
        } catch (xb) {
            _ = {
                apply: Y.length ? function (a, b) {
                    $.apply(a, ab.call(b))
                } : function (a, b) {
                    for (var c = a.length, d = 0; a[c++] = b[d++];);
                    a.length = c - 1
                }
            }
        }
        v = b.support = {}, y = b.isXML = function (a) {
            var b = a && (a.ownerDocument || a).documentElement;
            return b ? "HTML" !== b.nodeName : !1
        }, F = b.setDocument = function (a) {
            var b, c = a ? a.ownerDocument || a : O, d = c.defaultView;
            return c !== G && 9 === c.nodeType && c.documentElement ? (G = c, H = c.documentElement, I = !y(c), d && d !== d.top && (d.addEventListener ? d.addEventListener("unload", function () {
                F()
            }, !1) : d.attachEvent && d.attachEvent("onunload", function () {
                F()
            })), v.attributes = e(function (a) {
                return a.className = "i", !a.getAttribute("className")
            }), v.getElementsByTagName = e(function (a) {
                return a.appendChild(c.createComment("")), !a.getElementsByTagName("*").length
            }), v.getElementsByClassName = rb.test(c.getElementsByClassName) && e(function (a) {
                return a.innerHTML = "<div class='a'></div><div class='a i'></div>", a.firstChild.className = "i", 2 === a.getElementsByClassName("i").length
            }), v.getById = e(function (a) {
                return H.appendChild(a).id = N, !c.getElementsByName || !c.getElementsByName(N).length
            }), v.getById ? (w.find.ID = function (a, b) {
                if (typeof b.getElementById !== V && I) {
                    var c = b.getElementById(a);
                    return c && c.parentNode ? [c] : []
                }
            }, w.filter.ID = function (a) {
                var b = a.replace(vb, wb);
                return function (a) {
                    return a.getAttribute("id") === b
                }
            }) : (delete w.find.ID, w.filter.ID = function (a) {
                var b = a.replace(vb, wb);
                return function (a) {
                    var c = typeof a.getAttributeNode !== V && a.getAttributeNode("id");
                    return c && c.value === b
                }
            }), w.find.TAG = v.getElementsByTagName ? function (a, b) {
                return typeof b.getElementsByTagName !== V ? b.getElementsByTagName(a) : void 0
            } : function (a, b) {
                var c, d = [], e = 0, f = b.getElementsByTagName(a);
                if ("*" === a) {
                    for (; c = f[e++];)1 === c.nodeType && d.push(c);
                    return d
                }
                return f
            }, w.find.CLASS = v.getElementsByClassName && function (a, b) {
                return typeof b.getElementsByClassName !== V && I ? b.getElementsByClassName(a) : void 0
            }, K = [], J = [], (v.qsa = rb.test(c.querySelectorAll)) && (e(function (a) {
                a.innerHTML = "<select msallowclip=''><option selected=''></option></select>", a.querySelectorAll("[msallowclip^='']").length && J.push("[*^$]=" + db + "*(?:''|\"\")"), a.querySelectorAll("[selected]").length || J.push("\\[" + db + "*(?:value|" + cb + ")"), a.querySelectorAll(":checked").length || J.push(":checked")
            }), e(function (a) {
                var b = c.createElement("input");
                b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("name", "D"), a.querySelectorAll("[name=d]").length && J.push("name" + db + "*[*^$|!~]?="), a.querySelectorAll(":enabled").length || J.push(":enabled", ":disabled"), a.querySelectorAll("*,:x"), J.push(",.*:")
            })), (v.matchesSelector = rb.test(L = H.matches || H.webkitMatchesSelector || H.mozMatchesSelector || H.oMatchesSelector || H.msMatchesSelector)) && e(function (a) {
                v.disconnectedMatch = L.call(a, "div"), L.call(a, "[s!='']:x"), K.push("!=", hb)
            }), J = J.length && new RegExp(J.join("|")), K = K.length && new RegExp(K.join("|")), b = rb.test(H.compareDocumentPosition), M = b || rb.test(H.contains) ? function (a, b) {
                var c = 9 === a.nodeType ? a.documentElement : a, d = b && b.parentNode;
                return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)))
            } : function (a, b) {
                if (b)for (; b = b.parentNode;)if (b === a)return !0;
                return !1
            }, U = b ? function (a, b) {
                if (a === b)return E = !0, 0;
                var d = !a.compareDocumentPosition - !b.compareDocumentPosition;
                return d ? d : (d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1, 1 & d || !v.sortDetached && b.compareDocumentPosition(a) === d ? a === c || a.ownerDocument === O && M(O, a) ? -1 : b === c || b.ownerDocument === O && M(O, b) ? 1 : D ? bb.call(D, a) - bb.call(D, b) : 0 : 4 & d ? -1 : 1)
            } : function (a, b) {
                if (a === b)return E = !0, 0;
                var d, e = 0, f = a.parentNode, h = b.parentNode, i = [a], j = [b];
                if (!f || !h)return a === c ? -1 : b === c ? 1 : f ? -1 : h ? 1 : D ? bb.call(D, a) - bb.call(D, b) : 0;
                if (f === h)return g(a, b);
                for (d = a; d = d.parentNode;)i.unshift(d);
                for (d = b; d = d.parentNode;)j.unshift(d);
                for (; i[e] === j[e];)e++;
                return e ? g(i[e], j[e]) : i[e] === O ? -1 : j[e] === O ? 1 : 0
            }, c) : G
        }, b.matches = function (a, c) {
            return b(a, null, null, c)
        }, b.matchesSelector = function (a, c) {
            if ((a.ownerDocument || a) !== G && F(a), c = c.replace(lb, "='$1']"), !(!v.matchesSelector || !I || K && K.test(c) || J && J.test(c)))try {
                var d = L.call(a, c);
                if (d || v.disconnectedMatch || a.document && 11 !== a.document.nodeType)return d
            } catch (e) {
            }
            return b(c, G, null, [a]).length > 0
        }, b.contains = function (a, b) {
            return (a.ownerDocument || a) !== G && F(a), M(a, b)
        }, b.attr = function (a, b) {
            (a.ownerDocument || a) !== G && F(a);
            var c = w.attrHandle[b.toLowerCase()], d = c && X.call(w.attrHandle, b.toLowerCase()) ? c(a, b, !I) : void 0;
            return void 0 !== d ? d : v.attributes || !I ? a.getAttribute(b) : (d = a.getAttributeNode(b)) && d.specified ? d.value : null
        }, b.error = function (a) {
            throw new Error("Syntax error, unrecognized expression: " + a)
        }, b.uniqueSort = function (a) {
            var b, c = [], d = 0, e = 0;
            if (E = !v.detectDuplicates, D = !v.sortStable && a.slice(0), a.sort(U), E) {
                for (; b = a[e++];)b === a[e] && (d = c.push(e));
                for (; d--;)a.splice(c[d], 1)
            }
            return D = null, a
        }, x = b.getText = function (a) {
            var b, c = "", d = 0, e = a.nodeType;
            if (e) {
                if (1 === e || 9 === e || 11 === e) {
                    if ("string" == typeof a.textContent)return a.textContent;
                    for (a = a.firstChild; a; a = a.nextSibling)c += x(a)
                } else if (3 === e || 4 === e)return a.nodeValue
            } else for (; b = a[d++];)c += x(b);
            return c
        }, w = b.selectors = {
            cacheLength: 50,
            createPseudo: d,
            match: ob,
            attrHandle: {},
            find: {},
            relative: {
                ">": {dir: "parentNode", first: !0},
                " ": {dir: "parentNode"},
                "+": {dir: "previousSibling", first: !0},
                "~": {dir: "previousSibling"}
            },
            preFilter: {
                ATTR: function (a) {
                    return a[1] = a[1].replace(vb, wb), a[3] = (a[3] || a[4] || a[5] || "").replace(vb, wb), "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4)
                }, CHILD: function (a) {
                    return a[1] = a[1].toLowerCase(), "nth" === a[1].slice(0, 3) ? (a[3] || b.error(a[0]), a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && b.error(a[0]), a
                }, PSEUDO: function (a) {
                    var b, c = !a[6] && a[2];
                    return ob.CHILD.test(a[0]) ? null : (a[3] ? a[2] = a[4] || a[5] || "" : c && mb.test(c) && (b = z(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), a[2] = c.slice(0, b)), a.slice(0, 3))
                }
            },
            filter: {
                TAG: function (a) {
                    var b = a.replace(vb, wb).toLowerCase();
                    return "*" === a ? function () {
                        return !0
                    } : function (a) {
                        return a.nodeName && a.nodeName.toLowerCase() === b
                    }
                }, CLASS: function (a) {
                    var b = R[a + " "];
                    return b || (b = new RegExp("(^|" + db + ")" + a + "(" + db + "|$)")) && R(a, function (a) {
                        return b.test("string" == typeof a.className && a.className || typeof a.getAttribute !== V && a.getAttribute("class") || "")
                    })
                }, ATTR: function (a, c, d) {
                    return function (e) {
                        var f = b.attr(e, a);
                        return null == f ? "!=" === c : c ? (f += "", "=" === c ? f === d : "!=" === c ? f !== d : "^=" === c ? d && 0 === f.indexOf(d) : "*=" === c ? d && f.indexOf(d) > -1 : "$=" === c ? d && f.slice(-d.length) === d : "~=" === c ? (" " + f + " ").indexOf(d) > -1 : "|=" === c ? f === d || f.slice(0, d.length + 1) === d + "-" : !1) : !0
                    }
                }, CHILD: function (a, b, c, d, e) {
                    var f = "nth" !== a.slice(0, 3), g = "last" !== a.slice(-4), h = "of-type" === b;
                    return 1 === d && 0 === e ? function (a) {
                        return !!a.parentNode
                    } : function (b, c, i) {
                        var j, k, l, m, n, o, p = f !== g ? "nextSibling" : "previousSibling", q = b.parentNode, r = h && b.nodeName.toLowerCase(), s = !i && !h;
                        if (q) {
                            if (f) {
                                for (; p;) {
                                    for (l = b; l = l[p];)if (h ? l.nodeName.toLowerCase() === r : 1 === l.nodeType)return !1;
                                    o = p = "only" === a && !o && "nextSibling"
                                }
                                return !0
                            }
                            if (o = [g ? q.firstChild : q.lastChild], g && s) {
                                for (k = q[N] || (q[N] = {}), j = k[a] || [], n = j[0] === P && j[1], m = j[0] === P && j[2], l = n && q.childNodes[n]; l = ++n && l && l[p] || (m = n = 0) || o.pop();)if (1 === l.nodeType && ++m && l === b) {
                                    k[a] = [P, n, m];
                                    break
                                }
                            } else if (s && (j = (b[N] || (b[N] = {}))[a]) && j[0] === P)m = j[1]; else for (; (l = ++n && l && l[p] || (m = n = 0) || o.pop()) && ((h ? l.nodeName.toLowerCase() !== r : 1 !== l.nodeType) || !++m || (s && ((l[N] || (l[N] = {}))[a] = [P, m]), l !== b)););
                            return m -= e, m === d || m % d === 0 && m / d >= 0
                        }
                    }
                }, PSEUDO: function (a, c) {
                    var e, f = w.pseudos[a] || w.setFilters[a.toLowerCase()] || b.error("unsupported pseudo: " + a);
                    return f[N] ? f(c) : f.length > 1 ? (e = [a, a, "", c], w.setFilters.hasOwnProperty(a.toLowerCase()) ? d(function (a, b) {
                        for (var d, e = f(a, c), g = e.length; g--;)d = bb.call(a, e[g]), a[d] = !(b[d] = e[g])
                    }) : function (a) {
                        return f(a, 0, e)
                    }) : f
                }
            },
            pseudos: {
                not: d(function (a) {
                    var b = [], c = [], e = A(a.replace(ib, "$1"));
                    return e[N] ? d(function (a, b, c, d) {
                        for (var f, g = e(a, null, d, []), h = a.length; h--;)(f = g[h]) && (a[h] = !(b[h] = f))
                    }) : function (a, d, f) {
                        return b[0] = a, e(b, null, f, c), !c.pop()
                    }
                }), has: d(function (a) {
                    return function (c) {
                        return b(a, c).length > 0
                    }
                }), contains: d(function (a) {
                    return function (b) {
                        return (b.textContent || b.innerText || x(b)).indexOf(a) > -1
                    }
                }), lang: d(function (a) {
                    return nb.test(a || "") || b.error("unsupported lang: " + a), a = a.replace(vb, wb).toLowerCase(), function (b) {
                        var c;
                        do if (c = I ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang"))return c = c.toLowerCase(), c === a || 0 === c.indexOf(a + "-"); while ((b = b.parentNode) && 1 === b.nodeType);
                        return !1
                    }
                }), target: function (b) {
                    var c = a.location && a.location.hash;
                    return c && c.slice(1) === b.id
                }, root: function (a) {
                    return a === H
                }, focus: function (a) {
                    return a === G.activeElement && (!G.hasFocus || G.hasFocus()) && !!(a.type || a.href || ~a.tabIndex)
                }, enabled: function (a) {
                    return a.disabled === !1
                }, disabled: function (a) {
                    return a.disabled === !0
                }, checked: function (a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && !!a.checked || "option" === b && !!a.selected
                }, selected: function (a) {
                    return a.parentNode && a.parentNode.selectedIndex, a.selected === !0
                }, empty: function (a) {
                    for (a = a.firstChild; a; a = a.nextSibling)if (a.nodeType < 6)return !1;
                    return !0
                }, parent: function (a) {
                    return !w.pseudos.empty(a)
                }, header: function (a) {
                    return qb.test(a.nodeName)
                }, input: function (a) {
                    return pb.test(a.nodeName)
                }, button: function (a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && "button" === a.type || "button" === b
                }, text: function (a) {
                    var b;
                    return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || "text" === b.toLowerCase())
                }, first: j(function () {
                    return [0]
                }), last: j(function (a, b) {
                    return [b - 1]
                }), eq: j(function (a, b, c) {
                    return [0 > c ? c + b : c]
                }), even: j(function (a, b) {
                    for (var c = 0; b > c; c += 2)a.push(c);
                    return a
                }), odd: j(function (a, b) {
                    for (var c = 1; b > c; c += 2)a.push(c);
                    return a
                }), lt: j(function (a, b, c) {
                    for (var d = 0 > c ? c + b : c; --d >= 0;)a.push(d);
                    return a
                }), gt: j(function (a, b, c) {
                    for (var d = 0 > c ? c + b : c; ++d < b;)a.push(d);
                    return a
                })
            }
        }, w.pseudos.nth = w.pseudos.eq;
        for (u in{radio: !0, checkbox: !0, file: !0, password: !0, image: !0})w.pseudos[u] = h(u);
        for (u in{submit: !0, reset: !0})w.pseudos[u] = i(u);
        return l.prototype = w.filters = w.pseudos, w.setFilters = new l, z = b.tokenize = function (a, c) {
            var d, e, f, g, h, i, j, k = S[a + " "];
            if (k)return c ? 0 : k.slice(0);
            for (h = a, i = [], j = w.preFilter; h;) {
                (!d || (e = jb.exec(h))) && (e && (h = h.slice(e[0].length) || h), i.push(f = [])), d = !1, (e = kb.exec(h)) && (d = e.shift(), f.push({
                    value: d,
                    type: e[0].replace(ib, " ")
                }), h = h.slice(d.length));
                for (g in w.filter)!(e = ob[g].exec(h)) || j[g] && !(e = j[g](e)) || (d = e.shift(), f.push({
                    value: d,
                    type: g,
                    matches: e
                }), h = h.slice(d.length));
                if (!d)break
            }
            return c ? h.length : h ? b.error(a) : S(a, i).slice(0)
        }, A = b.compile = function (a, b) {
            var c, d = [], e = [], f = T[a + " "];
            if (!f) {
                for (b || (b = z(a)), c = b.length; c--;)f = s(b[c]), f[N] ? d.push(f) : e.push(f);
                f = T(a, t(e, d)), f.selector = a
            }
            return f
        }, B = b.select = function (a, b, c, d) {
            var e, f, g, h, i, j = "function" == typeof a && a, l = !d && z(a = j.selector || a);
            if (c = c || [], 1 === l.length) {
                if (f = l[0] = l[0].slice(0), f.length > 2 && "ID" === (g = f[0]).type && v.getById && 9 === b.nodeType && I && w.relative[f[1].type]) {
                    if (b = (w.find.ID(g.matches[0].replace(vb, wb), b) || [])[0], !b)return c;
                    j && (b = b.parentNode), a = a.slice(f.shift().value.length)
                }
                for (e = ob.needsContext.test(a) ? 0 : f.length; e-- && (g = f[e], !w.relative[h = g.type]);)if ((i = w.find[h]) && (d = i(g.matches[0].replace(vb, wb), tb.test(f[0].type) && k(b.parentNode) || b))) {
                    if (f.splice(e, 1), a = d.length && m(f), !a)return _.apply(c, d), c;
                    break
                }
            }
            return (j || A(a, l))(d, b, !I, c, tb.test(a) && k(b.parentNode) || b), c
        }, v.sortStable = N.split("").sort(U).join("") === N, v.detectDuplicates = !!E, F(), v.sortDetached = e(function (a) {
            return 1 & a.compareDocumentPosition(G.createElement("div"))
        }), e(function (a) {
            return a.innerHTML = "<a href='#'></a>", "#" === a.firstChild.getAttribute("href")
        }) || f("type|href|height|width", function (a, b, c) {
            return c ? void 0 : a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2)
        }), v.attributes && e(function (a) {
            return a.innerHTML = "<input/>", a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value")
        }) || f("value", function (a, b, c) {
            return c || "input" !== a.nodeName.toLowerCase() ? void 0 : a.defaultValue
        }), e(function (a) {
            return null == a.getAttribute("disabled")
        }) || f(cb, function (a, b, c) {
            var d;
            return c ? void 0 : a[b] === !0 ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null
        }), b
    }(a);
    eb.find = jb, eb.expr = jb.selectors, eb.expr[":"] = eb.expr.pseudos, eb.unique = jb.uniqueSort, eb.text = jb.getText, eb.isXMLDoc = jb.isXML, eb.contains = jb.contains;
    var kb = eb.expr.match.needsContext, lb = /^<(\w+)\s*\/?>(?:<\/\1>|)$/, mb = /^.[^:#\[\.,]*$/;
    eb.filter = function (a, b, c) {
        var d = b[0];
        return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === d.nodeType ? eb.find.matchesSelector(d, a) ? [d] : [] : eb.find.matches(a, eb.grep(b, function (a) {
            return 1 === a.nodeType
        }))
    }, eb.fn.extend({
        find: function (a) {
            var b, c = [], d = this, e = d.length;
            if ("string" != typeof a)return this.pushStack(eb(a).filter(function () {
                for (b = 0; e > b; b++)if (eb.contains(d[b], this))return !0
            }));
            for (b = 0; e > b; b++)eb.find(a, d[b], c);
            return c = this.pushStack(e > 1 ? eb.unique(c) : c), c.selector = this.selector ? this.selector + " " + a : a, c
        }, filter: function (a) {
            return this.pushStack(d(this, a || [], !1))
        }, not: function (a) {
            return this.pushStack(d(this, a || [], !0))
        }, is: function (a) {
            return !!d(this, "string" == typeof a && kb.test(a) ? eb(a) : a || [], !1).length
        }
    });
    var nb, ob = a.document, pb = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, qb = eb.fn.init = function (a, b) {
        var c, d;
        if (!a)return this;
        if ("string" == typeof a) {
            if (c = "<" === a.charAt(0) && ">" === a.charAt(a.length - 1) && a.length >= 3 ? [null, a, null] : pb.exec(a), !c || !c[1] && b)return !b || b.jquery ? (b || nb).find(a) : this.constructor(b).find(a);
            if (c[1]) {
                if (b = b instanceof eb ? b[0] : b, eb.merge(this, eb.parseHTML(c[1], b && b.nodeType ? b.ownerDocument || b : ob, !0)), lb.test(c[1]) && eb.isPlainObject(b))for (c in b)eb.isFunction(this[c]) ? this[c](b[c]) : this.attr(c, b[c]);
                return this
            }
            if (d = ob.getElementById(c[2]), d && d.parentNode) {
                if (d.id !== c[2])return nb.find(a);
                this.length = 1, this[0] = d
            }
            return this.context = ob, this.selector = a, this
        }
        return a.nodeType ? (this.context = this[0] = a, this.length = 1, this) : eb.isFunction(a) ? "undefined" != typeof nb.ready ? nb.ready(a) : a(eb) : (void 0 !== a.selector && (this.selector = a.selector, this.context = a.context), eb.makeArray(a, this))
    };
    qb.prototype = eb.fn, nb = eb(ob);
    var rb = /^(?:parents|prev(?:Until|All))/, sb = {children: !0, contents: !0, next: !0, prev: !0};
    eb.extend({
        dir: function (a, b, c) {
            for (var d = [], e = a[b]; e && 9 !== e.nodeType && (void 0 === c || 1 !== e.nodeType || !eb(e).is(c));)1 === e.nodeType && d.push(e), e = e[b];
            return d
        }, sibling: function (a, b) {
            for (var c = []; a; a = a.nextSibling)1 === a.nodeType && a !== b && c.push(a);
            return c
        }
    }), eb.fn.extend({
        has: function (a) {
            var b, c = eb(a, this), d = c.length;
            return this.filter(function () {
                for (b = 0; d > b; b++)if (eb.contains(this, c[b]))return !0
            })
        }, closest: function (a, b) {
            for (var c, d = 0, e = this.length, f = [], g = kb.test(a) || "string" != typeof a ? eb(a, b || this.context) : 0; e > d; d++)for (c = this[d]; c && c !== b; c = c.parentNode)if (c.nodeType < 11 && (g ? g.index(c) > -1 : 1 === c.nodeType && eb.find.matchesSelector(c, a))) {
                f.push(c);
                break
            }
            return this.pushStack(f.length > 1 ? eb.unique(f) : f)
        }, index: function (a) {
            return a ? "string" == typeof a ? eb.inArray(this[0], eb(a)) : eb.inArray(a.jquery ? a[0] : a, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        }, add: function (a, b) {
            return this.pushStack(eb.unique(eb.merge(this.get(), eb(a, b))))
        }, addBack: function (a) {
            return this.add(null == a ? this.prevObject : this.prevObject.filter(a))
        }
    }), eb.each({
        parent: function (a) {
            var b = a.parentNode;
            return b && 11 !== b.nodeType ? b : null
        }, parents: function (a) {
            return eb.dir(a, "parentNode")
        }, parentsUntil: function (a, b, c) {
            return eb.dir(a, "parentNode", c)
        }, next: function (a) {
            return e(a, "nextSibling")
        }, prev: function (a) {
            return e(a, "previousSibling")
        }, nextAll: function (a) {
            return eb.dir(a, "nextSibling")
        }, prevAll: function (a) {
            return eb.dir(a, "previousSibling")
        }, nextUntil: function (a, b, c) {
            return eb.dir(a, "nextSibling", c)
        }, prevUntil: function (a, b, c) {
            return eb.dir(a, "previousSibling", c)
        }, siblings: function (a) {
            return eb.sibling((a.parentNode || {}).firstChild, a)
        }, children: function (a) {
            return eb.sibling(a.firstChild)
        }, contents: function (a) {
            return eb.nodeName(a, "iframe") ? a.contentDocument || a.contentWindow.document : eb.merge([], a.childNodes)
        }
    }, function (a, b) {
        eb.fn[a] = function (c, d) {
            var e = eb.map(this, b, c);
            return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = eb.filter(d, e)), this.length > 1 && (sb[a] || (e = eb.unique(e)), rb.test(a) && (e = e.reverse())), this.pushStack(e)
        }
    });
    var tb = /\S+/g, ub = {};
    eb.Callbacks = function (a) {
        a = "string" == typeof a ? ub[a] || f(a) : eb.extend({}, a);
        var b, c, d, e, g, h, i = [], j = !a.once && [], k = function (f) {
            for (c = a.memory && f, d = !0, g = h || 0, h = 0, e = i.length, b = !0; i && e > g; g++)if (i[g].apply(f[0], f[1]) === !1 && a.stopOnFalse) {
                c = !1;
                break
            }
            b = !1, i && (j ? j.length && k(j.shift()) : c ? i = [] : l.disable())
        }, l = {
            add: function () {
                if (i) {
                    var d = i.length;
                    !function f(b) {
                        eb.each(b, function (b, c) {
                            var d = eb.type(c);
                            "function" === d ? a.unique && l.has(c) || i.push(c) : c && c.length && "string" !== d && f(c)
                        })
                    }(arguments), b ? e = i.length : c && (h = d, k(c))
                }
                return this
            }, remove: function () {
                return i && eb.each(arguments, function (a, c) {
                    for (var d; (d = eb.inArray(c, i, d)) > -1;)i.splice(d, 1), b && (e >= d && e--, g >= d && g--)
                }), this
            }, has: function (a) {
                return a ? eb.inArray(a, i) > -1 : !(!i || !i.length)
            }, empty: function () {
                return i = [], e = 0, this
            }, disable: function () {
                return i = j = c = void 0, this
            }, disabled: function () {
                return !i
            }, lock: function () {
                return j = void 0, c || l.disable(), this
            }, locked: function () {
                return !j
            }, fireWith: function (a, c) {
                return !i || d && !j || (c = c || [], c = [a, c.slice ? c.slice() : c], b ? j.push(c) : k(c)), this
            }, fire: function () {
                return l.fireWith(this, arguments), this
            }, fired: function () {
                return !!d
            }
        };
        return l
    }, eb.extend({
        Deferred: function (a) {
            var b = [["resolve", "done", eb.Callbacks("once memory"), "resolved"], ["reject", "fail", eb.Callbacks("once memory"), "rejected"], ["notify", "progress", eb.Callbacks("memory")]], c = "pending", d = {
                state: function () {
                    return c
                }, always: function () {
                    return e.done(arguments).fail(arguments), this
                }, then: function () {
                    var a = arguments;
                    return eb.Deferred(function (c) {
                        eb.each(b, function (b, f) {
                            var g = eb.isFunction(a[b]) && a[b];
                            e[f[1]](function () {
                                var a = g && g.apply(this, arguments);
                                a && eb.isFunction(a.promise) ? a.promise().done(c.resolve).fail(c.reject).progress(c.notify) : c[f[0] + "With"](this === d ? c.promise() : this, g ? [a] : arguments)
                            })
                        }), a = null
                    }).promise()
                }, promise: function (a) {
                    return null != a ? eb.extend(a, d) : d
                }
            }, e = {};
            return d.pipe = d.then, eb.each(b, function (a, f) {
                var g = f[2], h = f[3];
                d[f[1]] = g.add, h && g.add(function () {
                    c = h
                }, b[1 ^ a][2].disable, b[2][2].lock), e[f[0]] = function () {
                    return e[f[0] + "With"](this === e ? d : this, arguments), this
                }, e[f[0] + "With"] = g.fireWith
            }), d.promise(e), a && a.call(e, e), e
        }, when: function (a) {
            var b, c, d, e = 0, f = X.call(arguments), g = f.length, h = 1 !== g || a && eb.isFunction(a.promise) ? g : 0, i = 1 === h ? a : eb.Deferred(), j = function (a, c, d) {
                return function (e) {
                    c[a] = this, d[a] = arguments.length > 1 ? X.call(arguments) : e, d === b ? i.notifyWith(c, d) : --h || i.resolveWith(c, d)
                }
            };
            if (g > 1)for (b = new Array(g), c = new Array(g), d = new Array(g); g > e; e++)f[e] && eb.isFunction(f[e].promise) ? f[e].promise().done(j(e, d, f)).fail(i.reject).progress(j(e, c, b)) : --h;
            return h || i.resolveWith(d, f), i.promise()
        }
    });
    var vb;
    eb.fn.ready = function (a) {
        return eb.ready.promise().done(a), this
    }, eb.extend({
        isReady: !1, readyWait: 1, holdReady: function (a) {
            a ? eb.readyWait++ : eb.ready(!0)
        }, ready: function (a) {
            if (a === !0 ? !--eb.readyWait : !eb.isReady) {
                if (!ob.body)return setTimeout(eb.ready);
                eb.isReady = !0, a !== !0 && --eb.readyWait > 0 || (vb.resolveWith(ob, [eb]), eb.fn.triggerHandler && (eb(ob).triggerHandler("ready"), eb(ob).off("ready")))
            }
        }
    }), eb.ready.promise = function (b) {
        if (!vb)if (vb = eb.Deferred(), "complete" === ob.readyState)setTimeout(eb.ready); else if (ob.addEventListener)ob.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1); else {
            ob.attachEvent("onreadystatechange", h), a.attachEvent("onload", h);
            var c = !1;
            try {
                c = null == a.frameElement && ob.documentElement
            } catch (d) {
            }
            c && c.doScroll && !function e() {
                if (!eb.isReady) {
                    try {
                        c.doScroll("left")
                    } catch (a) {
                        return setTimeout(e, 50)
                    }
                    g(), eb.ready()
                }
            }()
        }
        return vb.promise(b)
    };
    var wb, xb = "undefined";
    for (wb in eb(cb))break;
    cb.ownLast = "0" !== wb, cb.inlineBlockNeedsLayout = !1, eb(function () {
        var a, b, c, d;
        c = ob.getElementsByTagName("body")[0], c && c.style && (b = ob.createElement("div"), d = ob.createElement("div"), d.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", c.appendChild(d).appendChild(b), typeof b.style.zoom !== xb && (b.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", cb.inlineBlockNeedsLayout = a = 3 === b.offsetWidth, a && (c.style.zoom = 1)), c.removeChild(d))
    }), function () {
        var a = ob.createElement("div");
        if (null == cb.deleteExpando) {
            cb.deleteExpando = !0;
            try {
                delete a.test
            } catch (b) {
                cb.deleteExpando = !1
            }
        }
        a = null
    }(), eb.acceptData = function (a) {
        var b = eb.noData[(a.nodeName + " ").toLowerCase()], c = +a.nodeType || 1;
        return 1 !== c && 9 !== c ? !1 : !b || b !== !0 && a.getAttribute("classid") === b
    };
    var yb = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, zb = /([A-Z])/g;
    eb.extend({
        cache: {},
        noData: {"applet ": !0, "embed ": !0, "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"},
        hasData: function (a) {
            return a = a.nodeType ? eb.cache[a[eb.expando]] : a[eb.expando], !!a && !j(a)
        },
        data: function (a, b, c) {
            return k(a, b, c)
        },
        removeData: function (a, b) {
            return l(a, b)
        },
        _data: function (a, b, c) {
            return k(a, b, c, !0)
        },
        _removeData: function (a, b) {
            return l(a, b, !0)
        }
    }), eb.fn.extend({
        data: function (a, b) {
            var c, d, e, f = this[0], g = f && f.attributes;
            if (void 0 === a) {
                if (this.length && (e = eb.data(f), 1 === f.nodeType && !eb._data(f, "parsedAttrs"))) {
                    for (c = g.length; c--;)g[c] && (d = g[c].name, 0 === d.indexOf("data-") && (d = eb.camelCase(d.slice(5)), i(f, d, e[d])));
                    eb._data(f, "parsedAttrs", !0)
                }
                return e
            }
            return "object" == typeof a ? this.each(function () {
                eb.data(this, a)
            }) : arguments.length > 1 ? this.each(function () {
                eb.data(this, a, b)
            }) : f ? i(f, a, eb.data(f, a)) : void 0
        }, removeData: function (a) {
            return this.each(function () {
                eb.removeData(this, a)
            })
        }
    }), eb.extend({
        queue: function (a, b, c) {
            var d;
            return a ? (b = (b || "fx") + "queue", d = eb._data(a, b), c && (!d || eb.isArray(c) ? d = eb._data(a, b, eb.makeArray(c)) : d.push(c)), d || []) : void 0
        }, dequeue: function (a, b) {
            b = b || "fx";
            var c = eb.queue(a, b), d = c.length, e = c.shift(), f = eb._queueHooks(a, b), g = function () {
                eb.dequeue(a, b)
            };
            "inprogress" === e && (e = c.shift(), d--), e && ("fx" === b && c.unshift("inprogress"), delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire()
        }, _queueHooks: function (a, b) {
            var c = b + "queueHooks";
            return eb._data(a, c) || eb._data(a, c, {
                empty: eb.Callbacks("once memory").add(function () {
                    eb._removeData(a, b + "queue"), eb._removeData(a, c)
                })
            })
        }
    }), eb.fn.extend({
        queue: function (a, b) {
            var c = 2;
            return "string" != typeof a && (b = a, a = "fx", c--), arguments.length < c ? eb.queue(this[0], a) : void 0 === b ? this : this.each(function () {
                var c = eb.queue(this, a, b);
                eb._queueHooks(this, a), "fx" === a && "inprogress" !== c[0] && eb.dequeue(this, a)
            })
        }, dequeue: function (a) {
            return this.each(function () {
                eb.dequeue(this, a)
            })
        }, clearQueue: function (a) {
            return this.queue(a || "fx", [])
        }, promise: function (a, b) {
            var c, d = 1, e = eb.Deferred(), f = this, g = this.length, h = function () {
                --d || e.resolveWith(f, [f])
            };
            for ("string" != typeof a && (b = a, a = void 0), a = a || "fx"; g--;)c = eb._data(f[g], a + "queueHooks"), c && c.empty && (d++, c.empty.add(h));
            return h(), e.promise(b)
        }
    });
    var Ab = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, Bb = ["Top", "Right", "Bottom", "Left"], Cb = function (a, b) {
        return a = b || a, "none" === eb.css(a, "display") || !eb.contains(a.ownerDocument, a)
    }, Db = eb.access = function (a, b, c, d, e, f, g) {
        var h = 0, i = a.length, j = null == c;
        if ("object" === eb.type(c)) {
            e = !0;
            for (h in c)eb.access(a, b, h, c[h], !0, f, g)
        } else if (void 0 !== d && (e = !0, eb.isFunction(d) || (g = !0), j && (g ? (b.call(a, d), b = null) : (j = b, b = function (a, b, c) {
                return j.call(eb(a), c)
            })), b))for (; i > h; h++)b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
        return e ? a : j ? b.call(a) : i ? b(a[0], c) : f
    }, Eb = /^(?:checkbox|radio)$/i;
    !function () {
        var a = ob.createElement("input"), b = ob.createElement("div"), c = ob.createDocumentFragment();
        if (b.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", cb.leadingWhitespace = 3 === b.firstChild.nodeType, cb.tbody = !b.getElementsByTagName("tbody").length, cb.htmlSerialize = !!b.getElementsByTagName("link").length, cb.html5Clone = "<:nav></:nav>" !== ob.createElement("nav").cloneNode(!0).outerHTML, a.type = "checkbox", a.checked = !0, c.appendChild(a), cb.appendChecked = a.checked, b.innerHTML = "<textarea>x</textarea>", cb.noCloneChecked = !!b.cloneNode(!0).lastChild.defaultValue, c.appendChild(b), b.innerHTML = "<input type='radio' checked='checked' name='t'/>", cb.checkClone = b.cloneNode(!0).cloneNode(!0).lastChild.checked, cb.noCloneEvent = !0, b.attachEvent && (b.attachEvent("onclick", function () {
                cb.noCloneEvent = !1
            }), b.cloneNode(!0).click()), null == cb.deleteExpando) {
            cb.deleteExpando = !0;
            try {
                delete b.test
            } catch (d) {
                cb.deleteExpando = !1
            }
        }
    }(), function () {
        var b, c, d = ob.createElement("div");
        for (b in{
            submit: !0,
            change: !0,
            focusin: !0
        })c = "on" + b, (cb[b + "Bubbles"] = c in a) || (d.setAttribute(c, "t"), cb[b + "Bubbles"] = d.attributes[c].expando === !1);
        d = null
    }();
    var Fb = /^(?:input|select|textarea)$/i, Gb = /^key/, Hb = /^(?:mouse|pointer|contextmenu)|click/, Ib = /^(?:focusinfocus|focusoutblur)$/, Jb = /^([^.]*)(?:\.(.+)|)$/;
    eb.event = {
        global: {},
        add: function (a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, n, o, p, q = eb._data(a);
            if (q) {
                for (c.handler && (i = c, c = i.handler, e = i.selector), c.guid || (c.guid = eb.guid++), (g = q.events) || (g = q.events = {}), (k = q.handle) || (k = q.handle = function (a) {
                    return typeof eb === xb || a && eb.event.triggered === a.type ? void 0 : eb.event.dispatch.apply(k.elem, arguments)
                }, k.elem = a), b = (b || "").match(tb) || [""], h = b.length; h--;)f = Jb.exec(b[h]) || [], n = p = f[1], o = (f[2] || "").split(".").sort(), n && (j = eb.event.special[n] || {}, n = (e ? j.delegateType : j.bindType) || n, j = eb.event.special[n] || {}, l = eb.extend({
                    type: n,
                    origType: p,
                    data: d,
                    handler: c,
                    guid: c.guid,
                    selector: e,
                    needsContext: e && eb.expr.match.needsContext.test(e),
                    namespace: o.join(".")
                }, i), (m = g[n]) || (m = g[n] = [], m.delegateCount = 0, j.setup && j.setup.call(a, d, o, k) !== !1 || (a.addEventListener ? a.addEventListener(n, k, !1) : a.attachEvent && a.attachEvent("on" + n, k))), j.add && (j.add.call(a, l), l.handler.guid || (l.handler.guid = c.guid)), e ? m.splice(m.delegateCount++, 0, l) : m.push(l), eb.event.global[n] = !0);
                a = null
            }
        },
        remove: function (a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, n, o, p, q = eb.hasData(a) && eb._data(a);
            if (q && (k = q.events)) {
                for (b = (b || "").match(tb) || [""], j = b.length; j--;)if (h = Jb.exec(b[j]) || [], n = p = h[1], o = (h[2] || "").split(".").sort(), n) {
                    for (l = eb.event.special[n] || {}, n = (d ? l.delegateType : l.bindType) || n, m = k[n] || [], h = h[2] && new RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)"), i = f = m.length; f--;)g = m[f], !e && p !== g.origType || c && c.guid !== g.guid || h && !h.test(g.namespace) || d && d !== g.selector && ("**" !== d || !g.selector) || (m.splice(f, 1), g.selector && m.delegateCount--, l.remove && l.remove.call(a, g));
                    i && !m.length && (l.teardown && l.teardown.call(a, o, q.handle) !== !1 || eb.removeEvent(a, n, q.handle), delete k[n])
                } else for (n in k)eb.event.remove(a, n + b[j], c, d, !0);
                eb.isEmptyObject(k) && (delete q.handle, eb._removeData(a, "events"))
            }
        },
        trigger: function (b, c, d, e) {
            var f, g, h, i, j, k, l, m = [d || ob], n = bb.call(b, "type") ? b.type : b, o = bb.call(b, "namespace") ? b.namespace.split(".") : [];
            if (h = k = d = d || ob, 3 !== d.nodeType && 8 !== d.nodeType && !Ib.test(n + eb.event.triggered) && (n.indexOf(".") >= 0 && (o = n.split("."), n = o.shift(), o.sort()), g = n.indexOf(":") < 0 && "on" + n, b = b[eb.expando] ? b : new eb.Event(n, "object" == typeof b && b), b.isTrigger = e ? 2 : 3, b.namespace = o.join("."), b.namespace_re = b.namespace ? new RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, b.result = void 0, b.target || (b.target = d), c = null == c ? [b] : eb.makeArray(c, [b]), j = eb.event.special[n] || {}, e || !j.trigger || j.trigger.apply(d, c) !== !1)) {
                if (!e && !j.noBubble && !eb.isWindow(d)) {
                    for (i = j.delegateType || n, Ib.test(i + n) || (h = h.parentNode); h; h = h.parentNode)m.push(h), k = h;
                    k === (d.ownerDocument || ob) && m.push(k.defaultView || k.parentWindow || a)
                }
                for (l = 0; (h = m[l++]) && !b.isPropagationStopped();)b.type = l > 1 ? i : j.bindType || n, f = (eb._data(h, "events") || {})[b.type] && eb._data(h, "handle"), f && f.apply(h, c), f = g && h[g], f && f.apply && eb.acceptData(h) && (b.result = f.apply(h, c), b.result === !1 && b.preventDefault());
                if (b.type = n, !e && !b.isDefaultPrevented() && (!j._default || j._default.apply(m.pop(), c) === !1) && eb.acceptData(d) && g && d[n] && !eb.isWindow(d)) {
                    k = d[g], k && (d[g] = null), eb.event.triggered = n;
                    try {
                        d[n]()
                    } catch (p) {
                    }
                    eb.event.triggered = void 0, k && (d[g] = k)
                }
                return b.result
            }
        },
        dispatch: function (a) {
            a = eb.event.fix(a);
            var b, c, d, e, f, g = [], h = X.call(arguments), i = (eb._data(this, "events") || {})[a.type] || [], j = eb.event.special[a.type] || {};
            if (h[0] = a, a.delegateTarget = this, !j.preDispatch || j.preDispatch.call(this, a) !== !1) {
                for (g = eb.event.handlers.call(this, a, i), b = 0; (e = g[b++]) && !a.isPropagationStopped();)for (a.currentTarget = e.elem, f = 0; (d = e.handlers[f++]) && !a.isImmediatePropagationStopped();)(!a.namespace_re || a.namespace_re.test(d.namespace)) && (a.handleObj = d, a.data = d.data, c = ((eb.event.special[d.origType] || {}).handle || d.handler).apply(e.elem, h), void 0 !== c && (a.result = c) === !1 && (a.preventDefault(), a.stopPropagation()));
                return j.postDispatch && j.postDispatch.call(this, a), a.result
            }
        },
        handlers: function (a, b) {
            var c, d, e, f, g = [], h = b.delegateCount, i = a.target;
            if (h && i.nodeType && (!a.button || "click" !== a.type))for (; i != this; i = i.parentNode || this)if (1 === i.nodeType && (i.disabled !== !0 || "click" !== a.type)) {
                for (e = [], f = 0; h > f; f++)d = b[f], c = d.selector + " ", void 0 === e[c] && (e[c] = d.needsContext ? eb(c, this).index(i) >= 0 : eb.find(c, this, null, [i]).length), e[c] && e.push(d);
                e.length && g.push({elem: i, handlers: e})
            }
            return h < b.length && g.push({elem: this, handlers: b.slice(h)}), g
        },
        fix: function (a) {
            if (a[eb.expando])return a;
            var b, c, d, e = a.type, f = a, g = this.fixHooks[e];
            for (g || (this.fixHooks[e] = g = Hb.test(e) ? this.mouseHooks : Gb.test(e) ? this.keyHooks : {}), d = g.props ? this.props.concat(g.props) : this.props, a = new eb.Event(f), b = d.length; b--;)c = d[b], a[c] = f[c];
            return a.target || (a.target = f.srcElement || ob), 3 === a.target.nodeType && (a.target = a.target.parentNode), a.metaKey = !!a.metaKey, g.filter ? g.filter(a, f) : a
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "), filter: function (a, b) {
                return null == a.which && (a.which = null != b.charCode ? b.charCode : b.keyCode), a
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function (a, b) {
                var c, d, e, f = b.button, g = b.fromElement;
                return null == a.pageX && null != b.clientX && (d = a.target.ownerDocument || ob, e = d.documentElement, c = d.body, a.pageX = b.clientX + (e && e.scrollLeft || c && c.scrollLeft || 0) - (e && e.clientLeft || c && c.clientLeft || 0), a.pageY = b.clientY + (e && e.scrollTop || c && c.scrollTop || 0) - (e && e.clientTop || c && c.clientTop || 0)), !a.relatedTarget && g && (a.relatedTarget = g === a.target ? b.toElement : g), a.which || void 0 === f || (a.which = 1 & f ? 1 : 2 & f ? 3 : 4 & f ? 2 : 0), a
            }
        },
        special: {
            load: {noBubble: !0}, focus: {
                trigger: function () {
                    if (this !== o() && this.focus)try {
                        return this.focus(), !1
                    } catch (a) {
                    }
                }, delegateType: "focusin"
            }, blur: {
                trigger: function () {
                    return this === o() && this.blur ? (this.blur(), !1) : void 0
                }, delegateType: "focusout"
            }, click: {
                trigger: function () {
                    return eb.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : void 0
                }, _default: function (a) {
                    return eb.nodeName(a.target, "a")
                }
            }, beforeunload: {
                postDispatch: function (a) {
                    void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result)
                }
            }
        },
        simulate: function (a, b, c, d) {
            var e = eb.extend(new eb.Event, c, {type: a, isSimulated: !0, originalEvent: {}});
            d ? eb.event.trigger(e, null, b) : eb.event.dispatch.call(b, e), e.isDefaultPrevented() && c.preventDefault()
        }
    }, eb.removeEvent = ob.removeEventListener ? function (a, b, c) {
        a.removeEventListener && a.removeEventListener(b, c, !1)
    } : function (a, b, c) {
        var d = "on" + b;
        a.detachEvent && (typeof a[d] === xb && (a[d] = null), a.detachEvent(d, c))
    }, eb.Event = function (a, b) {
        return this instanceof eb.Event ? (a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && a.returnValue === !1 ? m : n) : this.type = a, b && eb.extend(this, b), this.timeStamp = a && a.timeStamp || eb.now(), void(this[eb.expando] = !0)) : new eb.Event(a, b)
    }, eb.Event.prototype = {
        isDefaultPrevented: n,
        isPropagationStopped: n,
        isImmediatePropagationStopped: n,
        preventDefault: function () {
            var a = this.originalEvent;
            this.isDefaultPrevented = m, a && (a.preventDefault ? a.preventDefault() : a.returnValue = !1)
        },
        stopPropagation: function () {
            var a = this.originalEvent;
            this.isPropagationStopped = m, a && (a.stopPropagation && a.stopPropagation(), a.cancelBubble = !0)
        },
        stopImmediatePropagation: function () {
            var a = this.originalEvent;
            this.isImmediatePropagationStopped = m, a && a.stopImmediatePropagation && a.stopImmediatePropagation(), this.stopPropagation()
        }
    }, eb.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function (a, b) {
        eb.event.special[a] = {
            delegateType: b, bindType: b, handle: function (a) {
                var c, d = this, e = a.relatedTarget, f = a.handleObj;
                return (!e || e !== d && !eb.contains(d, e)) && (a.type = f.origType, c = f.handler.apply(this, arguments), a.type = b), c
            }
        }
    }), cb.submitBubbles || (eb.event.special.submit = {
        setup: function () {
            return eb.nodeName(this, "form") ? !1 : void eb.event.add(this, "click._submit keypress._submit", function (a) {
                var b = a.target, c = eb.nodeName(b, "input") || eb.nodeName(b, "button") ? b.form : void 0;
                c && !eb._data(c, "submitBubbles") && (eb.event.add(c, "submit._submit", function (a) {
                    a._submit_bubble = !0
                }), eb._data(c, "submitBubbles", !0))
            })
        }, postDispatch: function (a) {
            a._submit_bubble && (delete a._submit_bubble, this.parentNode && !a.isTrigger && eb.event.simulate("submit", this.parentNode, a, !0))
        }, teardown: function () {
            return eb.nodeName(this, "form") ? !1 : void eb.event.remove(this, "._submit")
        }
    }), cb.changeBubbles || (eb.event.special.change = {
        setup: function () {
            return Fb.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (eb.event.add(this, "propertychange._change", function (a) {
                "checked" === a.originalEvent.propertyName && (this._just_changed = !0)
            }), eb.event.add(this, "click._change", function (a) {
                this._just_changed && !a.isTrigger && (this._just_changed = !1), eb.event.simulate("change", this, a, !0)
            })), !1) : void eb.event.add(this, "beforeactivate._change", function (a) {
                var b = a.target;
                Fb.test(b.nodeName) && !eb._data(b, "changeBubbles") && (eb.event.add(b, "change._change", function (a) {
                    !this.parentNode || a.isSimulated || a.isTrigger || eb.event.simulate("change", this.parentNode, a, !0)
                }), eb._data(b, "changeBubbles", !0))
            })
        }, handle: function (a) {
            var b = a.target;
            return this !== b || a.isSimulated || a.isTrigger || "radio" !== b.type && "checkbox" !== b.type ? a.handleObj.handler.apply(this, arguments) : void 0
        }, teardown: function () {
            return eb.event.remove(this, "._change"), !Fb.test(this.nodeName)
        }
    }), cb.focusinBubbles || eb.each({focus: "focusin", blur: "focusout"}, function (a, b) {
        var c = function (a) {
            eb.event.simulate(b, a.target, eb.event.fix(a), !0)
        };
        eb.event.special[b] = {
            setup: function () {
                var d = this.ownerDocument || this, e = eb._data(d, b);
                e || d.addEventListener(a, c, !0), eb._data(d, b, (e || 0) + 1)
            }, teardown: function () {
                var d = this.ownerDocument || this, e = eb._data(d, b) - 1;
                e ? eb._data(d, b, e) : (d.removeEventListener(a, c, !0), eb._removeData(d, b))
            }
        }
    }), eb.fn.extend({
        on: function (a, b, c, d, e) {
            var f, g;
            if ("object" == typeof a) {
                "string" != typeof b && (c = c || b, b = void 0);
                for (f in a)this.on(f, b, c, a[f], e);
                return this
            }
            if (null == c && null == d ? (d = b, c = b = void 0) : null == d && ("string" == typeof b ? (d = c, c = void 0) : (d = c, c = b, b = void 0)), d === !1)d = n; else if (!d)return this;
            return 1 === e && (g = d, d = function (a) {
                return eb().off(a), g.apply(this, arguments)
            }, d.guid = g.guid || (g.guid = eb.guid++)), this.each(function () {
                eb.event.add(this, a, d, c, b)
            })
        }, one: function (a, b, c, d) {
            return this.on(a, b, c, d, 1)
        }, off: function (a, b, c) {
            var d, e;
            if (a && a.preventDefault && a.handleObj)return d = a.handleObj, eb(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler), this;
            if ("object" == typeof a) {
                for (e in a)this.off(e, b, a[e]);
                return this
            }
            return (b === !1 || "function" == typeof b) && (c = b, b = void 0), c === !1 && (c = n), this.each(function () {
                eb.event.remove(this, a, c, b)
            })
        }, trigger: function (a, b) {
            return this.each(function () {
                eb.event.trigger(a, b, this)
            })
        }, triggerHandler: function (a, b) {
            var c = this[0];
            return c ? eb.event.trigger(a, b, c, !0) : void 0
        }
    });
    var Kb = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video", Lb = / jQuery\d+="(?:null|\d+)"/g, Mb = new RegExp("<(?:" + Kb + ")[\\s/>]", "i"), Nb = /^\s+/, Ob = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, Pb = /<([\w:]+)/, Qb = /<tbody/i, Rb = /<|&#?\w+;/, Sb = /<(?:script|style|link)/i, Tb = /checked\s*(?:[^=]|=\s*.checked.)/i, Ub = /^$|\/(?:java|ecma)script/i, Vb = /^true\/(.*)/, Wb = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, Xb = {
        option: [1, "<select multiple='multiple'>", "</select>"],
        legend: [1, "<fieldset>", "</fieldset>"],
        area: [1, "<map>", "</map>"],
        param: [1, "<object>", "</object>"],
        thead: [1, "<table>", "</table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: cb.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
    }, Yb = p(ob), Zb = Yb.appendChild(ob.createElement("div"));
    Xb.optgroup = Xb.option, Xb.tbody = Xb.tfoot = Xb.colgroup = Xb.caption = Xb.thead, Xb.th = Xb.td, eb.extend({
        clone: function (a, b, c) {
            var d, e, f, g, h, i = eb.contains(a.ownerDocument, a);
            if (cb.html5Clone || eb.isXMLDoc(a) || !Mb.test("<" + a.nodeName + ">") ? f = a.cloneNode(!0) : (Zb.innerHTML = a.outerHTML, Zb.removeChild(f = Zb.firstChild)), !(cb.noCloneEvent && cb.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || eb.isXMLDoc(a)))for (d = q(f), h = q(a), g = 0; null != (e = h[g]); ++g)d[g] && x(e, d[g]);
            if (b)if (c)for (h = h || q(a), d = d || q(f), g = 0; null != (e = h[g]); g++)w(e, d[g]); else w(a, f);
            return d = q(f, "script"), d.length > 0 && v(d, !i && q(a, "script")), d = h = e = null, f
        }, buildFragment: function (a, b, c, d) {
            for (var e, f, g, h, i, j, k, l = a.length, m = p(b), n = [], o = 0; l > o; o++)if (f = a[o], f || 0 === f)if ("object" === eb.type(f))eb.merge(n, f.nodeType ? [f] : f); else if (Rb.test(f)) {
                for (h = h || m.appendChild(b.createElement("div")), i = (Pb.exec(f) || ["", ""])[1].toLowerCase(), k = Xb[i] || Xb._default, h.innerHTML = k[1] + f.replace(Ob, "<$1></$2>") + k[2], e = k[0]; e--;)h = h.lastChild;
                if (!cb.leadingWhitespace && Nb.test(f) && n.push(b.createTextNode(Nb.exec(f)[0])), !cb.tbody)for (f = "table" !== i || Qb.test(f) ? "<table>" !== k[1] || Qb.test(f) ? 0 : h : h.firstChild, e = f && f.childNodes.length; e--;)eb.nodeName(j = f.childNodes[e], "tbody") && !j.childNodes.length && f.removeChild(j);
                for (eb.merge(n, h.childNodes), h.textContent = ""; h.firstChild;)h.removeChild(h.firstChild);
                h = m.lastChild
            } else n.push(b.createTextNode(f));
            for (h && m.removeChild(h), cb.appendChecked || eb.grep(q(n, "input"), r), o = 0; f = n[o++];)if ((!d || -1 === eb.inArray(f, d)) && (g = eb.contains(f.ownerDocument, f), h = q(m.appendChild(f), "script"), g && v(h), c))for (e = 0; f = h[e++];)Ub.test(f.type || "") && c.push(f);
            return h = null, m
        }, cleanData: function (a, b) {
            for (var c, d, e, f, g = 0, h = eb.expando, i = eb.cache, j = cb.deleteExpando, k = eb.event.special; null != (c = a[g]); g++)if ((b || eb.acceptData(c)) && (e = c[h], f = e && i[e])) {
                if (f.events)for (d in f.events)k[d] ? eb.event.remove(c, d) : eb.removeEvent(c, d, f.handle);
                i[e] && (delete i[e], j ? delete c[h] : typeof c.removeAttribute !== xb ? c.removeAttribute(h) : c[h] = null, W.push(e))
            }
        }
    }), eb.fn.extend({
        text: function (a) {
            return Db(this, function (a) {
                return void 0 === a ? eb.text(this) : this.empty().append((this[0] && this[0].ownerDocument || ob).createTextNode(a))
            }, null, a, arguments.length)
        }, append: function () {
            return this.domManip(arguments, function (a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = s(this, a);
                    b.appendChild(a)
                }
            })
        }, prepend: function () {
            return this.domManip(arguments, function (a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = s(this, a);
                    b.insertBefore(a, b.firstChild)
                }
            })
        }, before: function () {
            return this.domManip(arguments, function (a) {
                this.parentNode && this.parentNode.insertBefore(a, this)
            })
        }, after: function () {
            return this.domManip(arguments, function (a) {
                this.parentNode && this.parentNode.insertBefore(a, this.nextSibling)
            })
        }, remove: function (a, b) {
            for (var c, d = a ? eb.filter(a, this) : this, e = 0; null != (c = d[e]); e++)b || 1 !== c.nodeType || eb.cleanData(q(c)), c.parentNode && (b && eb.contains(c.ownerDocument, c) && v(q(c, "script")), c.parentNode.removeChild(c));
            return this
        }, empty: function () {
            for (var a, b = 0; null != (a = this[b]); b++) {
                for (1 === a.nodeType && eb.cleanData(q(a, !1)); a.firstChild;)a.removeChild(a.firstChild);
                a.options && eb.nodeName(a, "select") && (a.options.length = 0)
            }
            return this
        }, clone: function (a, b) {
            return a = null == a ? !1 : a, b = null == b ? a : b, this.map(function () {
                return eb.clone(this, a, b)
            })
        }, html: function (a) {
            return Db(this, function (a) {
                var b = this[0] || {}, c = 0, d = this.length;
                if (void 0 === a)return 1 === b.nodeType ? b.innerHTML.replace(Lb, "") : void 0;
                if (!("string" != typeof a || Sb.test(a) || !cb.htmlSerialize && Mb.test(a) || !cb.leadingWhitespace && Nb.test(a) || Xb[(Pb.exec(a) || ["", ""])[1].toLowerCase()])) {
                    a = a.replace(Ob, "<$1></$2>");
                    try {
                        for (; d > c; c++)b = this[c] || {}, 1 === b.nodeType && (eb.cleanData(q(b, !1)), b.innerHTML = a);
                        b = 0
                    } catch (e) {
                    }
                }
                b && this.empty().append(a)
            }, null, a, arguments.length)
        }, replaceWith: function () {
            var a = arguments[0];
            return this.domManip(arguments, function (b) {
                a = this.parentNode, eb.cleanData(q(this)), a && a.replaceChild(b, this)
            }), a && (a.length || a.nodeType) ? this : this.remove()
        }, detach: function (a) {
            return this.remove(a, !0)
        }, domManip: function (a, b) {
            a = Y.apply([], a);
            var c, d, e, f, g, h, i = 0, j = this.length, k = this, l = j - 1, m = a[0], n = eb.isFunction(m);
            if (n || j > 1 && "string" == typeof m && !cb.checkClone && Tb.test(m))return this.each(function (c) {
                var d = k.eq(c);
                n && (a[0] = m.call(this, c, d.html())), d.domManip(a, b)
            });
            if (j && (h = eb.buildFragment(a, this[0].ownerDocument, !1, this), c = h.firstChild, 1 === h.childNodes.length && (h = c), c)) {
                for (f = eb.map(q(h, "script"), t), e = f.length; j > i; i++)d = h, i !== l && (d = eb.clone(d, !0, !0), e && eb.merge(f, q(d, "script"))), b.call(this[i], d, i);
                if (e)for (g = f[f.length - 1].ownerDocument, eb.map(f, u), i = 0; e > i; i++)d = f[i], Ub.test(d.type || "") && !eb._data(d, "globalEval") && eb.contains(g, d) && (d.src ? eb._evalUrl && eb._evalUrl(d.src) : eb.globalEval((d.text || d.textContent || d.innerHTML || "").replace(Wb, "")));
                h = c = null
            }
            return this
        }
    }), eb.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function (a, b) {
        eb.fn[a] = function (a) {
            for (var c, d = 0, e = [], f = eb(a), g = f.length - 1; g >= d; d++)c = d === g ? this : this.clone(!0), eb(f[d])[b](c), Z.apply(e, c.get());
            return this.pushStack(e)
        }
    });
    var $b, _b = {};
    !function () {
        var a;
        cb.shrinkWrapBlocks = function () {
            if (null != a)return a;
            a = !1;
            var b, c, d;
            return c = ob.getElementsByTagName("body")[0], c && c.style ? (b = ob.createElement("div"), d = ob.createElement("div"), d.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", c.appendChild(d).appendChild(b), typeof b.style.zoom !== xb && (b.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", b.appendChild(ob.createElement("div")).style.width = "5px", a = 3 !== b.offsetWidth), c.removeChild(d), a) : void 0
        }
    }();
    var ac, bc, cc = /^margin/, dc = new RegExp("^(" + Ab + ")(?!px)[a-z%]+$", "i"), ec = /^(top|right|bottom|left)$/;
    a.getComputedStyle ? (ac = function (a) {
        return a.ownerDocument.defaultView.getComputedStyle(a, null)
    }, bc = function (a, b, c) {
        var d, e, f, g, h = a.style;
        return c = c || ac(a), g = c ? c.getPropertyValue(b) || c[b] : void 0, c && ("" !== g || eb.contains(a.ownerDocument, a) || (g = eb.style(a, b)), dc.test(g) && cc.test(b) && (d = h.width, e = h.minWidth, f = h.maxWidth, h.minWidth = h.maxWidth = h.width = g, g = c.width, h.width = d, h.minWidth = e, h.maxWidth = f)), void 0 === g ? g : g + ""
    }) : ob.documentElement.currentStyle && (ac = function (a) {
        return a.currentStyle
    }, bc = function (a, b, c) {
        var d, e, f, g, h = a.style;
        return c = c || ac(a), g = c ? c[b] : void 0, null == g && h && h[b] && (g = h[b]), dc.test(g) && !ec.test(b) && (d = h.left, e = a.runtimeStyle, f = e && e.left, f && (e.left = a.currentStyle.left), h.left = "fontSize" === b ? "1em" : g, g = h.pixelLeft + "px", h.left = d, f && (e.left = f)), void 0 === g ? g : g + "" || "auto"
    }), function () {
        function b() {
            var b, c, d, e;
            c = ob.getElementsByTagName("body")[0], c && c.style && (b = ob.createElement("div"), d = ob.createElement("div"), d.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", c.appendChild(d).appendChild(b), b.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", f = g = !1, i = !0, a.getComputedStyle && (f = "1%" !== (a.getComputedStyle(b, null) || {}).top, g = "4px" === (a.getComputedStyle(b, null) || {width: "4px"}).width, e = b.appendChild(ob.createElement("div")), e.style.cssText = b.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", e.style.marginRight = e.style.width = "0", b.style.width = "1px", i = !parseFloat((a.getComputedStyle(e, null) || {}).marginRight)), b.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", e = b.getElementsByTagName("td"), e[0].style.cssText = "margin:0;border:0;padding:0;display:none", h = 0 === e[0].offsetHeight, h && (e[0].style.display = "", e[1].style.display = "none", h = 0 === e[0].offsetHeight), c.removeChild(d))
        }

        var c, d, e, f, g, h, i;
        c = ob.createElement("div"), c.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", e = c.getElementsByTagName("a")[0], d = e && e.style, d && (d.cssText = "float:left;opacity:.5", cb.opacity = "0.5" === d.opacity, cb.cssFloat = !!d.cssFloat, c.style.backgroundClip = "content-box", c.cloneNode(!0).style.backgroundClip = "", cb.clearCloneStyle = "content-box" === c.style.backgroundClip, cb.boxSizing = "" === d.boxSizing || "" === d.MozBoxSizing || "" === d.WebkitBoxSizing, eb.extend(cb, {
            reliableHiddenOffsets: function () {
                return null == h && b(), h
            }, boxSizingReliable: function () {
                return null == g && b(), g
            }, pixelPosition: function () {
                return null == f && b(), f
            }, reliableMarginRight: function () {
                return null == i && b(), i
            }
        }))
    }(), eb.swap = function (a, b, c, d) {
        var e, f, g = {};
        for (f in b)g[f] = a.style[f], a.style[f] = b[f];
        e = c.apply(a, d || []);
        for (f in b)a.style[f] = g[f];
        return e
    };
    var fc = /alpha\([^)]*\)/i, gc = /opacity\s*=\s*([^)]*)/, hc = /^(none|table(?!-c[ea]).+)/, ic = new RegExp("^(" + Ab + ")(.*)$", "i"), jc = new RegExp("^([+-])=(" + Ab + ")", "i"), kc = {
        position: "absolute",
        visibility: "hidden",
        display: "block"
    }, lc = {letterSpacing: "0", fontWeight: "400"}, mc = ["Webkit", "O", "Moz", "ms"];
    eb.extend({
        cssHooks: {
            opacity: {
                get: function (a, b) {
                    if (b) {
                        var c = bc(a, "opacity");
                        return "" === c ? "1" : c
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {"float": cb.cssFloat ? "cssFloat" : "styleFloat"},
        style: function (a, b, c, d) {
            if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
                var e, f, g, h = eb.camelCase(b), i = a.style;
                if (b = eb.cssProps[h] || (eb.cssProps[h] = B(i, h)), g = eb.cssHooks[b] || eb.cssHooks[h], void 0 === c)return g && "get"in g && void 0 !== (e = g.get(a, !1, d)) ? e : i[b];
                if (f = typeof c, "string" === f && (e = jc.exec(c)) && (c = (e[1] + 1) * e[2] + parseFloat(eb.css(a, b)), f = "number"), null != c && c === c && ("number" !== f || eb.cssNumber[h] || (c += "px"), cb.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (i[b] = "inherit"), !(g && "set"in g && void 0 === (c = g.set(a, c, d)))))try {
                    i[b] = c
                } catch (j) {
                }
            }
        },
        css: function (a, b, c, d) {
            var e, f, g, h = eb.camelCase(b);
            return b = eb.cssProps[h] || (eb.cssProps[h] = B(a.style, h)), g = eb.cssHooks[b] || eb.cssHooks[h], g && "get"in g && (f = g.get(a, !0, c)), void 0 === f && (f = bc(a, b, d)), "normal" === f && b in lc && (f = lc[b]), "" === c || c ? (e = parseFloat(f), c === !0 || eb.isNumeric(e) ? e || 0 : f) : f
        }
    }), eb.each(["height", "width"], function (a, b) {
        eb.cssHooks[b] = {
            get: function (a, c, d) {
                return c ? hc.test(eb.css(a, "display")) && 0 === a.offsetWidth ? eb.swap(a, kc, function () {
                    return F(a, b, d)
                }) : F(a, b, d) : void 0
            }, set: function (a, c, d) {
                var e = d && ac(a);
                return D(a, c, d ? E(a, b, d, cb.boxSizing && "border-box" === eb.css(a, "boxSizing", !1, e), e) : 0)
            }
        }
    }), cb.opacity || (eb.cssHooks.opacity = {
        get: function (a, b) {
            return gc.test((b && a.currentStyle ? a.currentStyle.filter : a.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : b ? "1" : ""
        }, set: function (a, b) {
            var c = a.style, d = a.currentStyle, e = eb.isNumeric(b) ? "alpha(opacity=" + 100 * b + ")" : "", f = d && d.filter || c.filter || "";
            c.zoom = 1, (b >= 1 || "" === b) && "" === eb.trim(f.replace(fc, "")) && c.removeAttribute && (c.removeAttribute("filter"), "" === b || d && !d.filter) || (c.filter = fc.test(f) ? f.replace(fc, e) : f + " " + e)
        }
    }), eb.cssHooks.marginRight = A(cb.reliableMarginRight, function (a, b) {
        return b ? eb.swap(a, {display: "inline-block"}, bc, [a, "marginRight"]) : void 0
    }), eb.each({margin: "", padding: "", border: "Width"}, function (a, b) {
        eb.cssHooks[a + b] = {
            expand: function (c) {
                for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [c]; 4 > d; d++)e[a + Bb[d] + b] = f[d] || f[d - 2] || f[0];
                return e
            }
        }, cc.test(a) || (eb.cssHooks[a + b].set = D)
    }), eb.fn.extend({
        css: function (a, b) {
            return Db(this, function (a, b, c) {
                var d, e, f = {}, g = 0;
                if (eb.isArray(b)) {
                    for (d = ac(a), e = b.length; e > g; g++)f[b[g]] = eb.css(a, b[g], !1, d);
                    return f
                }
                return void 0 !== c ? eb.style(a, b, c) : eb.css(a, b)
            }, a, b, arguments.length > 1)
        }, show: function () {
            return C(this, !0)
        }, hide: function () {
            return C(this)
        }, toggle: function (a) {
            return "boolean" == typeof a ? a ? this.show() : this.hide() : this.each(function () {
                Cb(this) ? eb(this).show() : eb(this).hide()
            })
        }
    }), eb.Tween = G, G.prototype = {
        constructor: G, init: function (a, b, c, d, e, f) {
            this.elem = a, this.prop = c, this.easing = e || "swing", this.options = b, this.start = this.now = this.cur(), this.end = d, this.unit = f || (eb.cssNumber[c] ? "" : "px")
        }, cur: function () {
            var a = G.propHooks[this.prop];
            return a && a.get ? a.get(this) : G.propHooks._default.get(this)
        }, run: function (a) {
            var b, c = G.propHooks[this.prop];
            return this.pos = b = this.options.duration ? eb.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration) : a, this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), c && c.set ? c.set(this) : G.propHooks._default.set(this), this
        }
    }, G.prototype.init.prototype = G.prototype, G.propHooks = {
        _default: {
            get: function (a) {
                var b;
                return null == a.elem[a.prop] || a.elem.style && null != a.elem.style[a.prop] ? (b = eb.css(a.elem, a.prop, ""), b && "auto" !== b ? b : 0) : a.elem[a.prop]
            }, set: function (a) {
                eb.fx.step[a.prop] ? eb.fx.step[a.prop](a) : a.elem.style && (null != a.elem.style[eb.cssProps[a.prop]] || eb.cssHooks[a.prop]) ? eb.style(a.elem, a.prop, a.now + a.unit) : a.elem[a.prop] = a.now
            }
        }
    }, G.propHooks.scrollTop = G.propHooks.scrollLeft = {
        set: function (a) {
            a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now)
        }
    }, eb.easing = {
        linear: function (a) {
            return a
        }, swing: function (a) {
            return .5 - Math.cos(a * Math.PI) / 2
        }
    }, eb.fx = G.prototype.init, eb.fx.step = {};
    var nc, oc, pc = /^(?:toggle|show|hide)$/, qc = new RegExp("^(?:([+-])=|)(" + Ab + ")([a-z%]*)$", "i"), rc = /queueHooks$/, sc = [K], tc = {
        "*": [function (a, b) {
            var c = this.createTween(a, b), d = c.cur(), e = qc.exec(b), f = e && e[3] || (eb.cssNumber[a] ? "" : "px"), g = (eb.cssNumber[a] || "px" !== f && +d) && qc.exec(eb.css(c.elem, a)), h = 1, i = 20;
            if (g && g[3] !== f) {
                f = f || g[3], e = e || [], g = +d || 1;
                do h = h || ".5", g /= h, eb.style(c.elem, a, g + f); while (h !== (h = c.cur() / d) && 1 !== h && --i)
            }
            return e && (g = c.start = +g || +d || 0, c.unit = f, c.end = e[1] ? g + (e[1] + 1) * e[2] : +e[2]), c
        }]
    };
    eb.Animation = eb.extend(M, {
        tweener: function (a, b) {
            eb.isFunction(a) ? (b = a, a = ["*"]) : a = a.split(" ");
            for (var c, d = 0, e = a.length; e > d; d++)c = a[d], tc[c] = tc[c] || [], tc[c].unshift(b)
        }, prefilter: function (a, b) {
            b ? sc.unshift(a) : sc.push(a)
        }
    }), eb.speed = function (a, b, c) {
        var d = a && "object" == typeof a ? eb.extend({}, a) : {
            complete: c || !c && b || eb.isFunction(a) && a,
            duration: a,
            easing: c && b || b && !eb.isFunction(b) && b
        };
        return d.duration = eb.fx.off ? 0 : "number" == typeof d.duration ? d.duration : d.duration in eb.fx.speeds ? eb.fx.speeds[d.duration] : eb.fx.speeds._default, (null == d.queue || d.queue === !0) && (d.queue = "fx"), d.old = d.complete, d.complete = function () {
            eb.isFunction(d.old) && d.old.call(this), d.queue && eb.dequeue(this, d.queue)
        }, d
    }, eb.fn.extend({
        fadeTo: function (a, b, c, d) {
            return this.filter(Cb).css("opacity", 0).show().end().animate({opacity: b}, a, c, d)
        }, animate: function (a, b, c, d) {
            var e = eb.isEmptyObject(a), f = eb.speed(b, c, d), g = function () {
                var b = M(this, eb.extend({}, a), f);
                (e || eb._data(this, "finish")) && b.stop(!0)
            };
            return g.finish = g, e || f.queue === !1 ? this.each(g) : this.queue(f.queue, g)
        }, stop: function (a, b, c) {
            var d = function (a) {
                var b = a.stop;
                delete a.stop, b(c)
            };
            return "string" != typeof a && (c = b, b = a, a = void 0), b && a !== !1 && this.queue(a || "fx", []), this.each(function () {
                var b = !0, e = null != a && a + "queueHooks", f = eb.timers, g = eb._data(this);
                if (e)g[e] && g[e].stop && d(g[e]); else for (e in g)g[e] && g[e].stop && rc.test(e) && d(g[e]);
                for (e = f.length; e--;)f[e].elem !== this || null != a && f[e].queue !== a || (f[e].anim.stop(c), b = !1, f.splice(e, 1));
                (b || !c) && eb.dequeue(this, a)
            })
        }, finish: function (a) {
            return a !== !1 && (a = a || "fx"), this.each(function () {
                var b, c = eb._data(this), d = c[a + "queue"], e = c[a + "queueHooks"], f = eb.timers, g = d ? d.length : 0;
                for (c.finish = !0, eb.queue(this, a, []), e && e.stop && e.stop.call(this, !0), b = f.length; b--;)f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), f.splice(b, 1));
                for (b = 0; g > b; b++)d[b] && d[b].finish && d[b].finish.call(this);
                delete c.finish
            })
        }
    }), eb.each(["toggle", "show", "hide"], function (a, b) {
        var c = eb.fn[b];
        eb.fn[b] = function (a, d, e) {
            return null == a || "boolean" == typeof a ? c.apply(this, arguments) : this.animate(I(b, !0), a, d, e)
        }
    }), eb.each({
        slideDown: I("show"),
        slideUp: I("hide"),
        slideToggle: I("toggle"),
        fadeIn: {opacity: "show"},
        fadeOut: {opacity: "hide"},
        fadeToggle: {opacity: "toggle"}
    }, function (a, b) {
        eb.fn[a] = function (a, c, d) {
            return this.animate(b, a, c, d)
        }
    }), eb.timers = [], eb.fx.tick = function () {
        var a, b = eb.timers, c = 0;
        for (nc = eb.now(); c < b.length; c++)a = b[c], a() || b[c] !== a || b.splice(c--, 1);
        b.length || eb.fx.stop(), nc = void 0
    }, eb.fx.timer = function (a) {
        eb.timers.push(a), a() ? eb.fx.start() : eb.timers.pop()
    }, eb.fx.interval = 13, eb.fx.start = function () {
        oc || (oc = setInterval(eb.fx.tick, eb.fx.interval))
    }, eb.fx.stop = function () {
        clearInterval(oc), oc = null
    }, eb.fx.speeds = {slow: 600, fast: 200, _default: 400}, eb.fn.delay = function (a, b) {
        return a = eb.fx ? eb.fx.speeds[a] || a : a, b = b || "fx", this.queue(b, function (b, c) {
            var d = setTimeout(b, a);
            c.stop = function () {
                clearTimeout(d)
            }
        })
    }, function () {
        var a, b, c, d, e;
        b = ob.createElement("div"), b.setAttribute("className", "t"), b.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", d = b.getElementsByTagName("a")[0], c = ob.createElement("select"), e = c.appendChild(ob.createElement("option")), a = b.getElementsByTagName("input")[0], d.style.cssText = "top:1px", cb.getSetAttribute = "t" !== b.className, cb.style = /top/.test(d.getAttribute("style")), cb.hrefNormalized = "/a" === d.getAttribute("href"), cb.checkOn = !!a.value, cb.optSelected = e.selected, cb.enctype = !!ob.createElement("form").enctype, c.disabled = !0, cb.optDisabled = !e.disabled, a = ob.createElement("input"), a.setAttribute("value", ""), cb.input = "" === a.getAttribute("value"), a.value = "t", a.setAttribute("type", "radio"), cb.radioValue = "t" === a.value
    }();
    var uc = /\r/g;
    eb.fn.extend({
        val: function (a) {
            var b, c, d, e = this[0];
            {
                if (arguments.length)return d = eb.isFunction(a), this.each(function (c) {
                    var e;
                    1 === this.nodeType && (e = d ? a.call(this, c, eb(this).val()) : a, null == e ? e = "" : "number" == typeof e ? e += "" : eb.isArray(e) && (e = eb.map(e, function (a) {
                        return null == a ? "" : a + ""
                    })), b = eb.valHooks[this.type] || eb.valHooks[this.nodeName.toLowerCase()], b && "set"in b && void 0 !== b.set(this, e, "value") || (this.value = e))
                });
                if (e)return b = eb.valHooks[e.type] || eb.valHooks[e.nodeName.toLowerCase()], b && "get"in b && void 0 !== (c = b.get(e, "value")) ? c : (c = e.value, "string" == typeof c ? c.replace(uc, "") : null == c ? "" : c)
            }
        }
    }), eb.extend({
        valHooks: {
            option: {
                get: function (a) {
                    var b = eb.find.attr(a, "value");
                    return null != b ? b : eb.trim(eb.text(a))
                }
            }, select: {
                get: function (a) {
                    for (var b, c, d = a.options, e = a.selectedIndex, f = "select-one" === a.type || 0 > e, g = f ? null : [], h = f ? e + 1 : d.length, i = 0 > e ? h : f ? e : 0; h > i; i++)if (c = d[i], !(!c.selected && i !== e || (cb.optDisabled ? c.disabled : null !== c.getAttribute("disabled")) || c.parentNode.disabled && eb.nodeName(c.parentNode, "optgroup"))) {
                        if (b = eb(c).val(), f)return b;
                        g.push(b)
                    }
                    return g
                }, set: function (a, b) {
                    for (var c, d, e = a.options, f = eb.makeArray(b), g = e.length; g--;)if (d = e[g], eb.inArray(eb.valHooks.option.get(d), f) >= 0)try {
                        d.selected = c = !0
                    } catch (h) {
                        d.scrollHeight
                    } else d.selected = !1;
                    return c || (a.selectedIndex = -1), e
                }
            }
        }
    }), eb.each(["radio", "checkbox"], function () {
        eb.valHooks[this] = {
            set: function (a, b) {
                return eb.isArray(b) ? a.checked = eb.inArray(eb(a).val(), b) >= 0 : void 0
            }
        }, cb.checkOn || (eb.valHooks[this].get = function (a) {
            return null === a.getAttribute("value") ? "on" : a.value
        })
    });
    var vc, wc, xc = eb.expr.attrHandle, yc = /^(?:checked|selected)$/i, zc = cb.getSetAttribute, Ac = cb.input;
    eb.fn.extend({
        attr: function (a, b) {
            return Db(this, eb.attr, a, b, arguments.length > 1)
        }, removeAttr: function (a) {
            return this.each(function () {
                eb.removeAttr(this, a)
            })
        }
    }), eb.extend({
        attr: function (a, b, c) {
            var d, e, f = a.nodeType;
            if (a && 3 !== f && 8 !== f && 2 !== f)return typeof a.getAttribute === xb ? eb.prop(a, b, c) : (1 === f && eb.isXMLDoc(a) || (b = b.toLowerCase(), d = eb.attrHooks[b] || (eb.expr.match.bool.test(b) ? wc : vc)), void 0 === c ? d && "get"in d && null !== (e = d.get(a, b)) ? e : (e = eb.find.attr(a, b), null == e ? void 0 : e) : null !== c ? d && "set"in d && void 0 !== (e = d.set(a, c, b)) ? e : (a.setAttribute(b, c + ""), c) : void eb.removeAttr(a, b))
        }, removeAttr: function (a, b) {
            var c, d, e = 0, f = b && b.match(tb);
            if (f && 1 === a.nodeType)for (; c = f[e++];)d = eb.propFix[c] || c, eb.expr.match.bool.test(c) ? Ac && zc || !yc.test(c) ? a[d] = !1 : a[eb.camelCase("default-" + c)] = a[d] = !1 : eb.attr(a, c, ""), a.removeAttribute(zc ? c : d)
        }, attrHooks: {
            type: {
                set: function (a, b) {
                    if (!cb.radioValue && "radio" === b && eb.nodeName(a, "input")) {
                        var c = a.value;
                        return a.setAttribute("type", b), c && (a.value = c), b
                    }
                }
            }
        }
    }), wc = {
        set: function (a, b, c) {
            return b === !1 ? eb.removeAttr(a, c) : Ac && zc || !yc.test(c) ? a.setAttribute(!zc && eb.propFix[c] || c, c) : a[eb.camelCase("default-" + c)] = a[c] = !0, c
        }
    }, eb.each(eb.expr.match.bool.source.match(/\w+/g), function (a, b) {
        var c = xc[b] || eb.find.attr;
        xc[b] = Ac && zc || !yc.test(b) ? function (a, b, d) {
            var e, f;
            return d || (f = xc[b], xc[b] = e, e = null != c(a, b, d) ? b.toLowerCase() : null, xc[b] = f), e
        } : function (a, b, c) {
            return c ? void 0 : a[eb.camelCase("default-" + b)] ? b.toLowerCase() : null
        }
    }), Ac && zc || (eb.attrHooks.value = {
        set: function (a, b, c) {
            return eb.nodeName(a, "input") ? void(a.defaultValue = b) : vc && vc.set(a, b, c)
        }
    }), zc || (vc = {
        set: function (a, b, c) {
            var d = a.getAttributeNode(c);
            return d || a.setAttributeNode(d = a.ownerDocument.createAttribute(c)), d.value = b += "", "value" === c || b === a.getAttribute(c) ? b : void 0
        }
    }, xc.id = xc.name = xc.coords = function (a, b, c) {
        var d;
        return c ? void 0 : (d = a.getAttributeNode(b)) && "" !== d.value ? d.value : null
    }, eb.valHooks.button = {
        get: function (a, b) {
            var c = a.getAttributeNode(b);
            return c && c.specified ? c.value : void 0
        }, set: vc.set
    }, eb.attrHooks.contenteditable = {
        set: function (a, b, c) {
            vc.set(a, "" === b ? !1 : b, c)
        }
    }, eb.each(["width", "height"], function (a, b) {
        eb.attrHooks[b] = {
            set: function (a, c) {
                return "" === c ? (a.setAttribute(b, "auto"), c) : void 0
            }
        }
    })), cb.style || (eb.attrHooks.style = {
        get: function (a) {
            return a.style.cssText || void 0
        }, set: function (a, b) {
            return a.style.cssText = b + ""
        }
    });
    var Bc = /^(?:input|select|textarea|button|object)$/i, Cc = /^(?:a|area)$/i;
    eb.fn.extend({
        prop: function (a, b) {
            return Db(this, eb.prop, a, b, arguments.length > 1)
        }, removeProp: function (a) {
            return a = eb.propFix[a] || a, this.each(function () {
                try {
                    this[a] = void 0, delete this[a]
                } catch (b) {
                }
            })
        }
    }), eb.extend({
        propFix: {"for": "htmlFor", "class": "className"}, prop: function (a, b, c) {
            var d, e, f, g = a.nodeType;
            if (a && 3 !== g && 8 !== g && 2 !== g)return f = 1 !== g || !eb.isXMLDoc(a), f && (b = eb.propFix[b] || b, e = eb.propHooks[b]), void 0 !== c ? e && "set"in e && void 0 !== (d = e.set(a, c, b)) ? d : a[b] = c : e && "get"in e && null !== (d = e.get(a, b)) ? d : a[b]
        }, propHooks: {
            tabIndex: {
                get: function (a) {
                    var b = eb.find.attr(a, "tabindex");
                    return b ? parseInt(b, 10) : Bc.test(a.nodeName) || Cc.test(a.nodeName) && a.href ? 0 : -1
                }
            }
        }
    }), cb.hrefNormalized || eb.each(["href", "src"], function (a, b) {
        eb.propHooks[b] = {
            get: function (a) {
                return a.getAttribute(b, 4)
            }
        }
    }), cb.optSelected || (eb.propHooks.selected = {
        get: function (a) {
            var b = a.parentNode;
            return b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex), null
        }
    }), eb.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
        eb.propFix[this.toLowerCase()] = this
    }), cb.enctype || (eb.propFix.enctype = "encoding");
    var Dc = /[\t\r\n\f]/g;
    eb.fn.extend({
        addClass: function (a) {
            var b, c, d, e, f, g, h = 0, i = this.length, j = "string" == typeof a && a;
            if (eb.isFunction(a))return this.each(function (b) {
                eb(this).addClass(a.call(this, b, this.className))
            });
            if (j)for (b = (a || "").match(tb) || []; i > h; h++)if (c = this[h], d = 1 === c.nodeType && (c.className ? (" " + c.className + " ").replace(Dc, " ") : " ")) {
                for (f = 0; e = b[f++];)d.indexOf(" " + e + " ") < 0 && (d += e + " ");
                g = eb.trim(d), c.className !== g && (c.className = g)
            }
            return this
        }, removeClass: function (a) {
            var b, c, d, e, f, g, h = 0, i = this.length, j = 0 === arguments.length || "string" == typeof a && a;
            if (eb.isFunction(a))return this.each(function (b) {
                eb(this).removeClass(a.call(this, b, this.className))
            });
            if (j)for (b = (a || "").match(tb) || []; i > h; h++)if (c = this[h], d = 1 === c.nodeType && (c.className ? (" " + c.className + " ").replace(Dc, " ") : "")) {
                for (f = 0; e = b[f++];)for (; d.indexOf(" " + e + " ") >= 0;)d = d.replace(" " + e + " ", " ");
                g = a ? eb.trim(d) : "", c.className !== g && (c.className = g)
            }
            return this
        }, toggleClass: function (a, b) {
            var c = typeof a;
            return "boolean" == typeof b && "string" === c ? b ? this.addClass(a) : this.removeClass(a) : this.each(eb.isFunction(a) ? function (c) {
                eb(this).toggleClass(a.call(this, c, this.className, b), b)
            } : function () {
                if ("string" === c)for (var b, d = 0, e = eb(this), f = a.match(tb) || []; b = f[d++];)e.hasClass(b) ? e.removeClass(b) : e.addClass(b); else(c === xb || "boolean" === c) && (this.className && eb._data(this, "__className__", this.className), this.className = this.className || a === !1 ? "" : eb._data(this, "__className__") || "")
            })
        }, hasClass: function (a) {
            for (var b = " " + a + " ", c = 0, d = this.length; d > c; c++)if (1 === this[c].nodeType && (" " + this[c].className + " ").replace(Dc, " ").indexOf(b) >= 0)return !0;
            return !1
        }
    }), eb.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (a, b) {
        eb.fn[b] = function (a, c) {
            return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b)
        }
    }), eb.fn.extend({
        hover: function (a, b) {
            return this.mouseenter(a).mouseleave(b || a)
        }, bind: function (a, b, c) {
            return this.on(a, null, b, c)
        }, unbind: function (a, b) {
            return this.off(a, null, b)
        }, delegate: function (a, b, c, d) {
            return this.on(b, a, c, d)
        }, undelegate: function (a, b, c) {
            return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c)
        }
    });
    var Ec = eb.now(), Fc = /\?/, Gc = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    eb.parseJSON = function (b) {
        if (a.JSON && a.JSON.parse)return a.JSON.parse(b + "");
        var c, d = null, e = eb.trim(b + "");
        return e && !eb.trim(e.replace(Gc, function (a, b, e, f) {
            return c && b && (d = 0), 0 === d ? a : (c = e || b, d += !f - !e, "")
        })) ? Function("return " + e)() : eb.error("Invalid JSON: " + b)
    }, eb.parseXML = function (b) {
        var c, d;
        if (!b || "string" != typeof b)return null;
        try {
            a.DOMParser ? (d = new DOMParser, c = d.parseFromString(b, "text/xml")) : (c = new ActiveXObject("Microsoft.XMLDOM"), c.async = "false", c.loadXML(b))
        } catch (e) {
            c = void 0
        }
        return c && c.documentElement && !c.getElementsByTagName("parsererror").length || eb.error("Invalid XML: " + b), c
    };
    var Hc, Ic, Jc = /#.*$/, Kc = /([?&])_=[^&]*/, Lc = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm, Mc = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, Nc = /^(?:GET|HEAD)$/, Oc = /^\/\//, Pc = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, Qc = {}, Rc = {}, Sc = "*/".concat("*");
    try {
        Ic = location.href
    } catch (Tc) {
        Ic = ob.createElement("a"), Ic.href = "", Ic = Ic.href
    }
    Hc = Pc.exec(Ic.toLowerCase()) || [], eb.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Ic,
            type: "GET",
            isLocal: Mc.test(Hc[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Sc,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {xml: /xml/, html: /html/, json: /json/},
            responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
            converters: {"* text": String, "text html": !0, "text json": eb.parseJSON, "text xml": eb.parseXML},
            flatOptions: {url: !0, context: !0}
        },
        ajaxSetup: function (a, b) {
            return b ? P(P(a, eb.ajaxSettings), b) : P(eb.ajaxSettings, a)
        },
        ajaxPrefilter: N(Qc),
        ajaxTransport: N(Rc),
        ajax: function (a, b) {
            function c(a, b, c, d) {
                var e, k, r, s, u, w = b;
                2 !== t && (t = 2, h && clearTimeout(h), j = void 0, g = d || "", v.readyState = a > 0 ? 4 : 0, e = a >= 200 && 300 > a || 304 === a, c && (s = Q(l, v, c)), s = R(l, s, v, e), e ? (l.ifModified && (u = v.getResponseHeader("Last-Modified"), u && (eb.lastModified[f] = u), u = v.getResponseHeader("etag"), u && (eb.etag[f] = u)), 204 === a || "HEAD" === l.type ? w = "nocontent" : 304 === a ? w = "notmodified" : (w = s.state, k = s.data, r = s.error, e = !r)) : (r = w, (a || !w) && (w = "error", 0 > a && (a = 0))), v.status = a, v.statusText = (b || w) + "", e ? o.resolveWith(m, [k, w, v]) : o.rejectWith(m, [v, w, r]), v.statusCode(q), q = void 0, i && n.trigger(e ? "ajaxSuccess" : "ajaxError", [v, l, e ? k : r]), p.fireWith(m, [v, w]), i && (n.trigger("ajaxComplete", [v, l]), --eb.active || eb.event.trigger("ajaxStop")))
            }

            "object" == typeof a && (b = a, a = void 0), b = b || {};
            var d, e, f, g, h, i, j, k, l = eb.ajaxSetup({}, b), m = l.context || l, n = l.context && (m.nodeType || m.jquery) ? eb(m) : eb.event, o = eb.Deferred(), p = eb.Callbacks("once memory"), q = l.statusCode || {}, r = {}, s = {}, t = 0, u = "canceled", v = {
                readyState: 0,
                getResponseHeader: function (a) {
                    var b;
                    if (2 === t) {
                        if (!k)for (k = {}; b = Lc.exec(g);)k[b[1].toLowerCase()] = b[2];
                        b = k[a.toLowerCase()]
                    }
                    return null == b ? null : b
                },
                getAllResponseHeaders: function () {
                    return 2 === t ? g : null
                },
                setRequestHeader: function (a, b) {
                    var c = a.toLowerCase();
                    return t || (a = s[c] = s[c] || a, r[a] = b), this
                },
                overrideMimeType: function (a) {
                    return t || (l.mimeType = a), this
                },
                statusCode: function (a) {
                    var b;
                    if (a)if (2 > t)for (b in a)q[b] = [q[b], a[b]]; else v.always(a[v.status]);
                    return this
                },
                abort: function (a) {
                    var b = a || u;
                    return j && j.abort(b), c(0, b), this
                }
            };
            if (o.promise(v).complete = p.add, v.success = v.done, v.error = v.fail, l.url = ((a || l.url || Ic) + "").replace(Jc, "").replace(Oc, Hc[1] + "//"), l.type = b.method || b.type || l.method || l.type, l.dataTypes = eb.trim(l.dataType || "*").toLowerCase().match(tb) || [""], null == l.crossDomain && (d = Pc.exec(l.url.toLowerCase()), l.crossDomain = !(!d || d[1] === Hc[1] && d[2] === Hc[2] && (d[3] || ("http:" === d[1] ? "80" : "443")) === (Hc[3] || ("http:" === Hc[1] ? "80" : "443")))), l.data && l.processData && "string" != typeof l.data && (l.data = eb.param(l.data, l.traditional)), O(Qc, l, b, v), 2 === t)return v;
            i = l.global, i && 0 === eb.active++ && eb.event.trigger("ajaxStart"), l.type = l.type.toUpperCase(), l.hasContent = !Nc.test(l.type), f = l.url, l.hasContent || (l.data && (f = l.url += (Fc.test(f) ? "&" : "?") + l.data, delete l.data), l.cache === !1 && (l.url = Kc.test(f) ? f.replace(Kc, "$1_=" + Ec++) : f + (Fc.test(f) ? "&" : "?") + "_=" + Ec++)), l.ifModified && (eb.lastModified[f] && v.setRequestHeader("If-Modified-Since", eb.lastModified[f]), eb.etag[f] && v.setRequestHeader("If-None-Match", eb.etag[f])), (l.data && l.hasContent && l.contentType !== !1 || b.contentType) && v.setRequestHeader("Content-Type", l.contentType), v.setRequestHeader("Accept", l.dataTypes[0] && l.accepts[l.dataTypes[0]] ? l.accepts[l.dataTypes[0]] + ("*" !== l.dataTypes[0] ? ", " + Sc + "; q=0.01" : "") : l.accepts["*"]);
            for (e in l.headers)v.setRequestHeader(e, l.headers[e]);
            if (l.beforeSend && (l.beforeSend.call(m, v, l) === !1 || 2 === t))return v.abort();
            u = "abort";
            for (e in{success: 1, error: 1, complete: 1})v[e](l[e]);
            if (j = O(Rc, l, b, v)) {
                v.readyState = 1, i && n.trigger("ajaxSend", [v, l]), l.async && l.timeout > 0 && (h = setTimeout(function () {
                    v.abort("timeout")
                }, l.timeout));
                try {
                    t = 1, j.send(r, c)
                } catch (w) {
                    if (!(2 > t))throw w;
                    c(-1, w)
                }
            } else c(-1, "No Transport");
            return v
        },
        getJSON: function (a, b, c) {
            return eb.get(a, b, c, "json")
        },
        getScript: function (a, b) {
            return eb.get(a, void 0, b, "script")
        }
    }), eb.each(["get", "post"], function (a, b) {
        eb[b] = function (a, c, d, e) {
            return eb.isFunction(c) && (e = e || d, d = c, c = void 0), eb.ajax({
                url: a,
                type: b,
                dataType: e,
                data: c,
                success: d
            })
        }
    }), eb.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (a, b) {
        eb.fn[b] = function (a) {
            return this.on(b, a)
        }
    }), eb._evalUrl = function (a) {
        return eb.ajax({url: a, type: "GET", dataType: "script", async: !1, global: !1, "throws": !0})
    }, eb.fn.extend({
        wrapAll: function (a) {
            if (eb.isFunction(a))return this.each(function (b) {
                eb(this).wrapAll(a.call(this, b))
            });
            if (this[0]) {
                var b = eb(a, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && b.insertBefore(this[0]), b.map(function () {
                    for (var a = this; a.firstChild && 1 === a.firstChild.nodeType;)a = a.firstChild;
                    return a
                }).append(this)
            }
            return this
        }, wrapInner: function (a) {
            return this.each(eb.isFunction(a) ? function (b) {
                eb(this).wrapInner(a.call(this, b))
            } : function () {
                var b = eb(this), c = b.contents();
                c.length ? c.wrapAll(a) : b.append(a)
            })
        }, wrap: function (a) {
            var b = eb.isFunction(a);
            return this.each(function (c) {
                eb(this).wrapAll(b ? a.call(this, c) : a)
            })
        }, unwrap: function () {
            return this.parent().each(function () {
                eb.nodeName(this, "body") || eb(this).replaceWith(this.childNodes)
            }).end()
        }
    }), eb.expr.filters.hidden = function (a) {
        return a.offsetWidth <= 0 && a.offsetHeight <= 0 || !cb.reliableHiddenOffsets() && "none" === (a.style && a.style.display || eb.css(a, "display"))
    }, eb.expr.filters.visible = function (a) {
        return !eb.expr.filters.hidden(a)
    };
    var Uc = /%20/g, Vc = /\[\]$/, Wc = /\r?\n/g, Xc = /^(?:submit|button|image|reset|file)$/i, Yc = /^(?:input|select|textarea|keygen)/i;
    eb.param = function (a, b) {
        var c, d = [], e = function (a, b) {
            b = eb.isFunction(b) ? b() : null == b ? "" : b, d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(b)
        };
        if (void 0 === b && (b = eb.ajaxSettings && eb.ajaxSettings.traditional), eb.isArray(a) || a.jquery && !eb.isPlainObject(a))eb.each(a, function () {
            e(this.name, this.value)
        }); else for (c in a)S(c, a[c], b, e);
        return d.join("&").replace(Uc, "+")
    }, eb.fn.extend({
        serialize: function () {
            return eb.param(this.serializeArray())
        }, serializeArray: function () {
            return this.map(function () {
                var a = eb.prop(this, "elements");
                return a ? eb.makeArray(a) : this
            }).filter(function () {
                var a = this.type;
                return this.name && !eb(this).is(":disabled") && Yc.test(this.nodeName) && !Xc.test(a) && (this.checked || !Eb.test(a))
            }).map(function (a, b) {
                var c = eb(this).val();
                return null == c ? null : eb.isArray(c) ? eb.map(c, function (a) {
                    return {name: b.name, value: a.replace(Wc, "\r\n")}
                }) : {name: b.name, value: c.replace(Wc, "\r\n")}
            }).get()
        }
    }), eb.ajaxSettings.xhr = void 0 !== a.ActiveXObject ? function () {
        return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && T() || U()
    } : T;
    var Zc = 0, $c = {}, _c = eb.ajaxSettings.xhr();
    a.ActiveXObject && eb(a).on("unload", function () {
        for (var a in $c)$c[a](void 0, !0)
    }), cb.cors = !!_c && "withCredentials"in _c, _c = cb.ajax = !!_c, _c && eb.ajaxTransport(function (a) {
        if (!a.crossDomain || cb.cors) {
            var b;
            return {
                send: function (c, d) {
                    var e, f = a.xhr(), g = ++Zc;
                    if (f.open(a.type, a.url, a.async, a.username, a.password), a.xhrFields)for (e in a.xhrFields)f[e] = a.xhrFields[e];
                    a.mimeType && f.overrideMimeType && f.overrideMimeType(a.mimeType), a.crossDomain || c["X-Requested-With"] || (c["X-Requested-With"] = "XMLHttpRequest");
                    for (e in c)void 0 !== c[e] && f.setRequestHeader(e, c[e] + "");
                    //f.send(a.hasContent && a.data || null), b = function (c, e) {
                    //    var h, i, j;
                    //    if (b && (e || 4 === f.readyState))if (delete $c[g], b = void 0, f.onreadystatechange = eb.noop, e)4 !== f.readyState && f.abort(); else {
                    //        j = {}, h = f.status, "string" == typeof f.responseText && (j.text = f.responseText);
                    //        try {
                    //            i = f.statusText
                    //        } catch (k) {
                    //            i = ""
                    //        }
                    //        h || !a.isLocal || a.crossDomain ? 1223 === h && (h = 204) : h = j.text ? 200 : 404
                    //    }
                    //    j && d(h, i, j, f.getAllResponseHeaders())
                    //}, a.async ? 4 === f.readyState ? setTimeout(b) : f.onreadystatechange = $c[g] = b : b()
                }, abort: function () {
                    b && b(void 0, !0)
                }
            }
        }
    }), eb.ajaxSetup({
        accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},
        contents: {script: /(?:java|ecma)script/},
        converters: {
            "text script": function (a) {
                return eb.globalEval(a), a
            }
        }
    }), eb.ajaxPrefilter("script", function (a) {
        void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET", a.global = !1)
    }), eb.ajaxTransport("script", function (a) {
        if (a.crossDomain) {
            var b, c = ob.head || eb("head")[0] || ob.documentElement;
            return {
                send: function (d, e) {
                    b = ob.createElement("script"), b.async = !0, a.scriptCharset && (b.charset = a.scriptCharset), b.src = a.url, b.onload = b.onreadystatechange = function (a, c) {
                        (c || !b.readyState || /loaded|complete/.test(b.readyState)) && (b.onload = b.onreadystatechange = null, b.parentNode && b.parentNode.removeChild(b), b = null, c || e(200, "success"))
                    }, c.insertBefore(b, c.firstChild)
                }, abort: function () {
                    b && b.onload(void 0, !0)
                }
            }
        }
    });
    var ad = [], bd = /(=)\?(?=&|$)|\?\?/;
    eb.ajaxSetup({
        jsonp: "callback", jsonpCallback: function () {
            var a = ad.pop() || eb.expando + "_" + Ec++;
            return this[a] = !0, a
        }
    }), eb.ajaxPrefilter("json jsonp", function (b, c, d) {
        var e, f, g, h = b.jsonp !== !1 && (bd.test(b.url) ? "url" : "string" == typeof b.data && !(b.contentType || "").indexOf("application/x-www-form-urlencoded") && bd.test(b.data) && "data");
        return h || "jsonp" === b.dataTypes[0] ? (e = b.jsonpCallback = eb.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback, h ? b[h] = b[h].replace(bd, "$1" + e) : b.jsonp !== !1 && (b.url += (Fc.test(b.url) ? "&" : "?") + b.jsonp + "=" + e), b.converters["script json"] = function () {
            return g || eb.error(e + " was not called"), g[0]
        }, b.dataTypes[0] = "json", f = a[e], a[e] = function () {
            g = arguments
        }, d.always(function () {
            a[e] = f, b[e] && (b.jsonpCallback = c.jsonpCallback, ad.push(e)), g && eb.isFunction(f) && f(g[0]), g = f = void 0
        }), "script") : void 0
    }), eb.parseHTML = function (a, b, c) {
        if (!a || "string" != typeof a)return null;
        "boolean" == typeof b && (c = b, b = !1), b = b || ob;
        var d = lb.exec(a), e = !c && [];
        return d ? [b.createElement(d[1])] : (d = eb.buildFragment([a], b, e), e && e.length && eb(e).remove(), eb.merge([], d.childNodes))
    };
    var cd = eb.fn.load;
    eb.fn.load = function (a, b, c) {
        if ("string" != typeof a && cd)return cd.apply(this, arguments);
        var d, e, f, g = this, h = a.indexOf(" ");
        return h >= 0 && (d = eb.trim(a.slice(h, a.length)), a = a.slice(0, h)), eb.isFunction(b) ? (c = b, b = void 0) : b && "object" == typeof b && (f = "POST"), g.length > 0 && eb.ajax({
            url: a,
            type: f,
            dataType: "html",
            data: b
        }).done(function (a) {
            e = arguments, g.html(d ? eb("<div>").append(eb.parseHTML(a)).find(d) : a)
        }).complete(c && function (a, b) {
            g.each(c, e || [a.responseText, b, a])
        }), this
    }, eb.expr.filters.animated = function (a) {
        return eb.grep(eb.timers, function (b) {
            return a === b.elem
        }).length
    };
    var dd = a.document.documentElement;
    eb.offset = {
        setOffset: function (a, b, c) {
            var d, e, f, g, h, i, j, k = eb.css(a, "position"), l = eb(a), m = {};
            "static" === k && (a.style.position = "relative"), h = l.offset(), f = eb.css(a, "top"), i = eb.css(a, "left"), j = ("absolute" === k || "fixed" === k) && eb.inArray("auto", [f, i]) > -1, j ? (d = l.position(), g = d.top, e = d.left) : (g = parseFloat(f) || 0, e = parseFloat(i) || 0), eb.isFunction(b) && (b = b.call(a, c, h)), null != b.top && (m.top = b.top - h.top + g), null != b.left && (m.left = b.left - h.left + e), "using"in b ? b.using.call(a, m) : l.css(m)
        }
    }, eb.fn.extend({
        offset: function (a) {
            if (arguments.length)return void 0 === a ? this : this.each(function (b) {
                eb.offset.setOffset(this, a, b)
            });
            var b, c, d = {top: 0, left: 0}, e = this[0], f = e && e.ownerDocument;
            if (f)return b = f.documentElement, eb.contains(b, e) ? (typeof e.getBoundingClientRect !== xb && (d = e.getBoundingClientRect()), c = V(f), {
                top: d.top + (c.pageYOffset || b.scrollTop) - (b.clientTop || 0),
                left: d.left + (c.pageXOffset || b.scrollLeft) - (b.clientLeft || 0)
            }) : d
        }, position: function () {
            if (this[0]) {
                var a, b, c = {top: 0, left: 0}, d = this[0];
                return "fixed" === eb.css(d, "position") ? b = d.getBoundingClientRect() : (a = this.offsetParent(), b = this.offset(), eb.nodeName(a[0], "html") || (c = a.offset()), c.top += eb.css(a[0], "borderTopWidth", !0), c.left += eb.css(a[0], "borderLeftWidth", !0)), {
                    top: b.top - c.top - eb.css(d, "marginTop", !0),
                    left: b.left - c.left - eb.css(d, "marginLeft", !0)
                }
            }
        }, offsetParent: function () {
            return this.map(function () {
                for (var a = this.offsetParent || dd; a && !eb.nodeName(a, "html") && "static" === eb.css(a, "position");)a = a.offsetParent;
                return a || dd
            })
        }
    }), eb.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function (a, b) {
        var c = /Y/.test(b);
        eb.fn[a] = function (d) {
            return Db(this, function (a, d, e) {
                var f = V(a);
                return void 0 === e ? f ? b in f ? f[b] : f.document.documentElement[d] : a[d] : void(f ? f.scrollTo(c ? eb(f).scrollLeft() : e, c ? e : eb(f).scrollTop()) : a[d] = e)
            }, a, d, arguments.length, null)
        }
    }), eb.each(["top", "left"], function (a, b) {
        eb.cssHooks[b] = A(cb.pixelPosition, function (a, c) {
            return c ? (c = bc(a, b), dc.test(c) ? eb(a).position()[b] + "px" : c) : void 0
        })
    }), eb.each({Height: "height", Width: "width"}, function (a, b) {
        eb.each({padding: "inner" + a, content: b, "": "outer" + a}, function (c, d) {
            eb.fn[d] = function (d, e) {
                var f = arguments.length && (c || "boolean" != typeof d), g = c || (d === !0 || e === !0 ? "margin" : "border");
                return Db(this, function (b, c, d) {
                    var e;
                    return eb.isWindow(b) ? b.document.documentElement["client" + a] : 9 === b.nodeType ? (e = b.documentElement, Math.max(b.body["scroll" + a], e["scroll" + a], b.body["offset" + a], e["offset" + a], e["client" + a])) : void 0 === d ? eb.css(b, c, g) : eb.style(b, c, d, g)
                }, b, f ? d : void 0, f, null)
            }
        })
    }), eb.fn.size = function () {
        return this.length
    }, eb.fn.andSelf = eb.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function () {
        return eb
    });
    var ed = a.jQuery, fd = a.$;
    return eb.noConflict = function (b) {
        return a.$ === eb && (a.$ = fd), b && a.jQuery === eb && (a.jQuery = ed), eb
    }, typeof b === xb && (a.jQuery = a.$ = eb), eb
}), (window._gsQueue || (window._gsQueue = [])).push(function () {
    "use strict";
    window._gsDefine("TweenMax", ["core.Animation", "core.SimpleTimeline", "TweenLite"], function (a, b, c) {
        var d = [].slice, e = function (a, b, d) {
            c.call(this, a, b, d), this._cycle = 0, this._yoyo = this.vars.yoyo === !0, this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._dirty = !0, this.render = e.prototype.render
        }, f = 1e-10, g = c._internals, h = g.isSelector, i = g.isArray, j = e.prototype = c.to({}, .1, {}), k = [];
        e.version = "1.12.1", j.constructor = e, j.kill()._gc = !1, e.killTweensOf = e.killDelayedCallsTo = c.killTweensOf, e.getTweensOf = c.getTweensOf, e.lagSmoothing = c.lagSmoothing, e.ticker = c.ticker, e.render = c.render, j.invalidate = function () {
            return this._yoyo = this.vars.yoyo === !0, this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._uncache(!0), c.prototype.invalidate.call(this)
        }, j.updateTo = function (a, b) {
            var d, e = this.ratio;
            b && this._startTime < this._timeline._time && (this._startTime = this._timeline._time, this._uncache(!1), this._gc ? this._enabled(!0, !1) : this._timeline.insert(this, this._startTime - this._delay));
            for (d in a)this.vars[d] = a[d];
            if (this._initted)if (b)this._initted = !1; else if (this._gc && this._enabled(!0, !1), this._notifyPluginsOfEnabled && this._firstPT && c._onPluginEvent("_onDisable", this), this._time / this._duration > .998) {
                var f = this._time;
                this.render(0, !0, !1), this._initted = !1, this.render(f, !0, !1)
            } else if (this._time > 0) {
                this._initted = !1, this._init();
                for (var g, h = 1 / (1 - e), i = this._firstPT; i;)g = i.s + i.c, i.c *= h, i.s = g - i.c, i = i._next
            }
            return this
        }, j.render = function (a, b, c) {
            this._initted || 0 === this._duration && this.vars.repeat && this.invalidate();
            var d, e, h, i, j, l, m, n, o = this._dirty ? this.totalDuration() : this._totalDuration, p = this._time, q = this._totalTime, r = this._cycle, s = this._duration, t = this._rawPrevTime;
            if (a >= o ? (this._totalTime = o, this._cycle = this._repeat, this._yoyo && 0 !== (1 & this._cycle) ? (this._time = 0, this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0) : (this._time = s, this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1), this._reversed || (d = !0, e = "onComplete"), 0 === s && (this._initted || !this.vars.lazy || c) && (this._startTime === this._timeline._duration && (a = 0), (0 === a || 0 > t || t === f) && t !== a && (c = !0, t > f && (e = "onReverseComplete")), this._rawPrevTime = n = !b || a || t === a ? a : f)) : 1e-7 > a ? (this._totalTime = this._time = this._cycle = 0, this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0, (0 !== q || 0 === s && t > 0 && t !== f) && (e = "onReverseComplete", d = this._reversed), 0 > a ? (this._active = !1, 0 === s && (this._initted || !this.vars.lazy || c) && (t >= 0 && (c = !0), this._rawPrevTime = n = !b || a || t === a ? a : f)) : this._initted || (c = !0)) : (this._totalTime = this._time = a, 0 !== this._repeat && (i = s + this._repeatDelay, this._cycle = this._totalTime / i >> 0, 0 !== this._cycle && this._cycle === this._totalTime / i && this._cycle--, this._time = this._totalTime - this._cycle * i, this._yoyo && 0 !== (1 & this._cycle) && (this._time = s - this._time), this._time > s ? this._time = s : 0 > this._time && (this._time = 0)), this._easeType ? (j = this._time / s, l = this._easeType, m = this._easePower, (1 === l || 3 === l && j >= .5) && (j = 1 - j), 3 === l && (j *= 2), 1 === m ? j *= j : 2 === m ? j *= j * j : 3 === m ? j *= j * j * j : 4 === m && (j *= j * j * j * j), this.ratio = 1 === l ? 1 - j : 2 === l ? j : .5 > this._time / s ? j / 2 : 1 - j / 2) : this.ratio = this._ease.getRatio(this._time / s)), p === this._time && !c && r === this._cycle)return void(q !== this._totalTime && this._onUpdate && (b || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || k)));
            if (!this._initted) {
                if (this._init(), !this._initted || this._gc)return;
                if (!c && this._firstPT && (this.vars.lazy !== !1 && this._duration || this.vars.lazy && !this._duration))return this._time = p, this._totalTime = q, this._rawPrevTime = t, this._cycle = r, g.lazyTweens.push(this), void(this._lazy = a);
                this._time && !d ? this.ratio = this._ease.getRatio(this._time / s) : d && this._ease._calcEnd && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1))
            }
            for (this._lazy !== !1 && (this._lazy = !1), this._active || !this._paused && this._time !== p && a >= 0 && (this._active = !0), 0 === q && (2 === this._initted && a > 0 && this._init(), this._startAt && (a >= 0 ? this._startAt.render(a, b, c) : e || (e = "_dummyGS")), this.vars.onStart && (0 !== this._totalTime || 0 === s) && (b || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || k))), h = this._firstPT; h;)h.f ? h.t[h.p](h.c * this.ratio + h.s) : h.t[h.p] = h.c * this.ratio + h.s, h = h._next;
            this._onUpdate && (0 > a && this._startAt && this._startTime && this._startAt.render(a, b, c), b || (this._totalTime !== q || d) && this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || k)), this._cycle !== r && (b || this._gc || this.vars.onRepeat && this.vars.onRepeat.apply(this.vars.onRepeatScope || this, this.vars.onRepeatParams || k)), e && (this._gc || (0 > a && this._startAt && !this._onUpdate && this._startTime && this._startAt.render(a, b, c), d && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !b && this.vars[e] && this.vars[e].apply(this.vars[e + "Scope"] || this, this.vars[e + "Params"] || k), 0 === s && this._rawPrevTime === f && n !== f && (this._rawPrevTime = 0)))
        }, e.to = function (a, b, c) {
            return new e(a, b, c)
        }, e.from = function (a, b, c) {
            return c.runBackwards = !0, c.immediateRender = 0 != c.immediateRender, new e(a, b, c)
        }, e.fromTo = function (a, b, c, d) {
            return d.startAt = c, d.immediateRender = 0 != d.immediateRender && 0 != c.immediateRender, new e(a, b, d)
        }, e.staggerTo = e.allTo = function (a, b, f, g, j, l, m) {
            g = g || 0;
            var n, o, p, q, r = f.delay || 0, s = [], t = function () {
                f.onComplete && f.onComplete.apply(f.onCompleteScope || this, arguments), j.apply(m || this, l || k)
            };
            for (i(a) || ("string" == typeof a && (a = c.selector(a) || a), h(a) && (a = d.call(a, 0))), n = a.length, p = 0; n > p; p++) {
                o = {};
                for (q in f)o[q] = f[q];
                o.delay = r, p === n - 1 && j && (o.onComplete = t), s[p] = new e(a[p], b, o), r += g
            }
            return s
        }, e.staggerFrom = e.allFrom = function (a, b, c, d, f, g, h) {
            return c.runBackwards = !0, c.immediateRender = 0 != c.immediateRender, e.staggerTo(a, b, c, d, f, g, h)
        }, e.staggerFromTo = e.allFromTo = function (a, b, c, d, f, g, h, i) {
            return d.startAt = c, d.immediateRender = 0 != d.immediateRender && 0 != c.immediateRender, e.staggerTo(a, b, d, f, g, h, i)
        }, e.delayedCall = function (a, b, c, d, f) {
            return new e(b, 0, {
                delay: a,
                onComplete: b,
                onCompleteParams: c,
                onCompleteScope: d,
                onReverseComplete: b,
                onReverseCompleteParams: c,
                onReverseCompleteScope: d,
                immediateRender: !1,
                useFrames: f,
                overwrite: 0
            })
        }, e.set = function (a, b) {
            return new e(a, 0, b)
        }, e.isTweening = function (a) {
            return c.getTweensOf(a, !0).length > 0
        };
        var l = function (a, b) {
            for (var d = [], e = 0, f = a._first; f;)f instanceof c ? d[e++] = f : (b && (d[e++] = f), d = d.concat(l(f, b)), e = d.length), f = f._next;
            return d
        }, m = e.getAllTweens = function (b) {
            return l(a._rootTimeline, b).concat(l(a._rootFramesTimeline, b))
        };
        e.killAll = function (a, c, d, e) {
            null == c && (c = !0), null == d && (d = !0);
            var f, g, h, i = m(0 != e), j = i.length, k = c && d && e;
            for (h = 0; j > h; h++)g = i[h], (k || g instanceof b || (f = g.target === g.vars.onComplete) && d || c && !f) && (a ? g.totalTime(g._reversed ? 0 : g.totalDuration()) : g._enabled(!1, !1))
        }, e.killChildTweensOf = function (a, b) {
            if (null != a) {
                var f, j, k, l, m, n = g.tweenLookup;
                if ("string" == typeof a && (a = c.selector(a) || a), h(a) && (a = d.call(a, 0)), i(a))for (l = a.length; --l > -1;)e.killChildTweensOf(a[l], b); else {
                    f = [];
                    for (k in n)for (j = n[k].target.parentNode; j;)j === a && (f = f.concat(n[k].tweens)), j = j.parentNode;
                    for (m = f.length, l = 0; m > l; l++)b && f[l].totalTime(f[l].totalDuration()), f[l]._enabled(!1, !1)
                }
            }
        };
        var n = function (a, c, d, e) {
            c = c !== !1, d = d !== !1, e = e !== !1;
            for (var f, g, h = m(e), i = c && d && e, j = h.length; --j > -1;)g = h[j], (i || g instanceof b || (f = g.target === g.vars.onComplete) && d || c && !f) && g.paused(a)
        };
        return e.pauseAll = function (a, b, c) {
            n(!0, a, b, c)
        }, e.resumeAll = function (a, b, c) {
            n(!1, a, b, c)
        }, e.globalTimeScale = function (b) {
            var d = a._rootTimeline, e = c.ticker.time;
            return arguments.length ? (b = b || f, d._startTime = e - (e - d._startTime) * d._timeScale / b, d = a._rootFramesTimeline, e = c.ticker.frame, d._startTime = e - (e - d._startTime) * d._timeScale / b, d._timeScale = a._rootTimeline._timeScale = b, b) : d._timeScale
        }, j.progress = function (a) {
            return arguments.length ? this.totalTime(this.duration() * (this._yoyo && 0 !== (1 & this._cycle) ? 1 - a : a) + this._cycle * (this._duration + this._repeatDelay), !1) : this._time / this.duration()
        }, j.totalProgress = function (a) {
            return arguments.length ? this.totalTime(this.totalDuration() * a, !1) : this._totalTime / this.totalDuration()
        }, j.time = function (a, b) {
            return arguments.length ? (this._dirty && this.totalDuration(), a > this._duration && (a = this._duration), this._yoyo && 0 !== (1 & this._cycle) ? a = this._duration - a + this._cycle * (this._duration + this._repeatDelay) : 0 !== this._repeat && (a += this._cycle * (this._duration + this._repeatDelay)), this.totalTime(a, b)) : this._time
        }, j.duration = function (b) {
            return arguments.length ? a.prototype.duration.call(this, b) : this._duration
        }, j.totalDuration = function (a) {
            return arguments.length ? -1 === this._repeat ? this : this.duration((a - this._repeat * this._repeatDelay) / (this._repeat + 1)) : (this._dirty && (this._totalDuration = -1 === this._repeat ? 999999999999 : this._duration * (this._repeat + 1) + this._repeatDelay * this._repeat, this._dirty = !1), this._totalDuration)
        }, j.repeat = function (a) {
            return arguments.length ? (this._repeat = a, this._uncache(!0)) : this._repeat
        }, j.repeatDelay = function (a) {
            return arguments.length ? (this._repeatDelay = a, this._uncache(!0)) : this._repeatDelay
        }, j.yoyo = function (a) {
            return arguments.length ? (this._yoyo = a, this) : this._yoyo
        }, e
    }, !0), window._gsDefine("TimelineLite", ["core.Animation", "core.SimpleTimeline", "TweenLite"], function (a, b, c) {
        var d = function (a) {
            b.call(this, a), this._labels = {}, this.autoRemoveChildren = this.vars.autoRemoveChildren === !0, this.smoothChildTiming = this.vars.smoothChildTiming === !0, this._sortChildren = !0, this._onUpdate = this.vars.onUpdate;
            var c, d, e = this.vars;
            for (d in e)c = e[d], g(c) && -1 !== c.join("").indexOf("{self}") && (e[d] = this._swapSelfInParams(c));
            g(e.tweens) && this.add(e.tweens, 0, e.align, e.stagger)
        }, e = 1e-10, f = c._internals.isSelector, g = c._internals.isArray, h = [], i = window._gsDefine.globals, j = function (a) {
            var b, c = {};
            for (b in a)c[b] = a[b];
            return c
        }, k = function (a, b, c, d) {
            a._timeline.pause(a._startTime), b && b.apply(d || a._timeline, c || h)
        }, l = h.slice, m = d.prototype = new b;
        return d.version = "1.12.1", m.constructor = d, m.kill()._gc = !1, m.to = function (a, b, d, e) {
            var f = d.repeat && i.TweenMax || c;
            return b ? this.add(new f(a, b, d), e) : this.set(a, d, e)
        }, m.from = function (a, b, d, e) {
            return this.add((d.repeat && i.TweenMax || c).from(a, b, d), e)
        }, m.fromTo = function (a, b, d, e, f) {
            var g = e.repeat && i.TweenMax || c;
            return b ? this.add(g.fromTo(a, b, d, e), f) : this.set(a, e, f)
        }, m.staggerTo = function (a, b, e, g, h, i, k, m) {
            var n, o = new d({
                onComplete: i,
                onCompleteParams: k,
                onCompleteScope: m,
                smoothChildTiming: this.smoothChildTiming
            });
            for ("string" == typeof a && (a = c.selector(a) || a), f(a) && (a = l.call(a, 0)), g = g || 0, n = 0; a.length > n; n++)e.startAt && (e.startAt = j(e.startAt)), o.to(a[n], b, j(e), n * g);
            return this.add(o, h)
        }, m.staggerFrom = function (a, b, c, d, e, f, g, h) {
            return c.immediateRender = 0 != c.immediateRender, c.runBackwards = !0, this.staggerTo(a, b, c, d, e, f, g, h)
        }, m.staggerFromTo = function (a, b, c, d, e, f, g, h, i) {
            return d.startAt = c, d.immediateRender = 0 != d.immediateRender && 0 != c.immediateRender, this.staggerTo(a, b, d, e, f, g, h, i)
        }, m.call = function (a, b, d, e) {
            return this.add(c.delayedCall(0, a, b, d), e)
        }, m.set = function (a, b, d) {
            return d = this._parseTimeOrLabel(d, 0, !0), null == b.immediateRender && (b.immediateRender = d === this._time && !this._paused), this.add(new c(a, 0, b), d)
        }, d.exportRoot = function (a, b) {
            a = a || {}, null == a.smoothChildTiming && (a.smoothChildTiming = !0);
            var e, f, g = new d(a), h = g._timeline;
            for (null == b && (b = !0), h._remove(g, !0), g._startTime = 0, g._rawPrevTime = g._time = g._totalTime = h._time, e = h._first; e;)f = e._next, b && e instanceof c && e.target === e.vars.onComplete || g.add(e, e._startTime - e._delay), e = f;
            return h.add(g, 0), g
        }, m.add = function (e, f, h, i) {
            var j, k, l, m, n, o;
            if ("number" != typeof f && (f = this._parseTimeOrLabel(f, 0, !0, e)), !(e instanceof a)) {
                if (e instanceof Array || e && e.push && g(e)) {
                    for (h = h || "normal", i = i || 0, j = f, k = e.length, l = 0; k > l; l++)g(m = e[l]) && (m = new d({tweens: m})), this.add(m, j), "string" != typeof m && "function" != typeof m && ("sequence" === h ? j = m._startTime + m.totalDuration() / m._timeScale : "start" === h && (m._startTime -= m.delay())), j += i;
                    return this._uncache(!0)
                }
                if ("string" == typeof e)return this.addLabel(e, f);
                if ("function" != typeof e)throw"Cannot add " + e + " into the timeline; it is not a tween, timeline, function, or string.";
                e = c.delayedCall(0, e)
            }
            if (b.prototype.add.call(this, e, f), (this._gc || this._time === this._duration) && !this._paused && this._duration < this.duration())for (n = this, o = n.rawTime() > e._startTime; n._timeline;)o && n._timeline.smoothChildTiming ? n.totalTime(n._totalTime, !0) : n._gc && n._enabled(!0, !1), n = n._timeline;
            return this
        }, m.remove = function (b) {
            if (b instanceof a)return this._remove(b, !1);
            if (b instanceof Array || b && b.push && g(b)) {
                for (var c = b.length; --c > -1;)this.remove(b[c]);
                return this
            }
            return "string" == typeof b ? this.removeLabel(b) : this.kill(null, b)
        }, m._remove = function (a, c) {
            b.prototype._remove.call(this, a, c);
            var d = this._last;
            return d ? this._time > d._startTime + d._totalDuration / d._timeScale && (this._time = this.duration(), this._totalTime = this._totalDuration) : this._time = this._totalTime = this._duration = this._totalDuration = 0, this
        }, m.append = function (a, b) {
            return this.add(a, this._parseTimeOrLabel(null, b, !0, a))
        }, m.insert = m.insertMultiple = function (a, b, c, d) {
            return this.add(a, b || 0, c, d)
        }, m.appendMultiple = function (a, b, c, d) {
            return this.add(a, this._parseTimeOrLabel(null, b, !0, a), c, d)
        }, m.addLabel = function (a, b) {
            return this._labels[a] = this._parseTimeOrLabel(b), this
        }, m.addPause = function (a, b, c, d) {
            return this.call(k, ["{self}", b, c, d], this, a)
        }, m.removeLabel = function (a) {
            return delete this._labels[a], this
        }, m.getLabelTime = function (a) {
            return null != this._labels[a] ? this._labels[a] : -1
        }, m._parseTimeOrLabel = function (b, c, d, e) {
            var f;
            if (e instanceof a && e.timeline === this)this.remove(e); else if (e && (e instanceof Array || e.push && g(e)))for (f = e.length; --f > -1;)e[f]instanceof a && e[f].timeline === this && this.remove(e[f]);
            if ("string" == typeof c)return this._parseTimeOrLabel(c, d && "number" == typeof b && null == this._labels[c] ? b - this.duration() : 0, d);
            if (c = c || 0, "string" != typeof b || !isNaN(b) && null == this._labels[b])null == b && (b = this.duration()); else {
                if (f = b.indexOf("="), -1 === f)return null == this._labels[b] ? d ? this._labels[b] = this.duration() + c : c : this._labels[b] + c;
                c = parseInt(b.charAt(f - 1) + "1", 10) * Number(b.substr(f + 1)), b = f > 1 ? this._parseTimeOrLabel(b.substr(0, f - 1), 0, d) : this.duration()
            }
            return Number(b) + c
        }, m.seek = function (a, b) {
            return this.totalTime("number" == typeof a ? a : this._parseTimeOrLabel(a), b !== !1)
        }, m.stop = function () {
            return this.paused(!0)
        }, m.gotoAndPlay = function (a, b) {
            return this.play(a, b)
        }, m.gotoAndStop = function (a, b) {
            return this.pause(a, b)
        }, m.render = function (a, b, c) {
            this._gc && this._enabled(!0, !1);
            var d, f, g, i, j, k = this._dirty ? this.totalDuration() : this._totalDuration, l = this._time, m = this._startTime, n = this._timeScale, o = this._paused;
            if (a >= k ? (this._totalTime = this._time = k, this._reversed || this._hasPausedChild() || (f = !0, i = "onComplete", 0 === this._duration && (0 === a || 0 > this._rawPrevTime || this._rawPrevTime === e) && this._rawPrevTime !== a && this._first && (j = !0, this._rawPrevTime > e && (i = "onReverseComplete"))), this._rawPrevTime = this._duration || !b || a || this._rawPrevTime === a ? a : e, a = k + 1e-4) : 1e-7 > a ? (this._totalTime = this._time = 0, (0 !== l || 0 === this._duration && this._rawPrevTime !== e && (this._rawPrevTime > 0 || 0 > a && this._rawPrevTime >= 0)) && (i = "onReverseComplete", f = this._reversed), 0 > a ? (this._active = !1, 0 === this._duration && this._rawPrevTime >= 0 && this._first && (j = !0), this._rawPrevTime = a) : (this._rawPrevTime = this._duration || !b || a || this._rawPrevTime === a ? a : e, a = 0, this._initted || (j = !0))) : this._totalTime = this._time = this._rawPrevTime = a, this._time !== l && this._first || c || j) {
                if (this._initted || (this._initted = !0), this._active || !this._paused && this._time !== l && a > 0 && (this._active = !0), 0 === l && this.vars.onStart && 0 !== this._time && (b || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || h)), this._time >= l)for (d = this._first; d && (g = d._next, !this._paused || o);)(d._active || d._startTime <= this._time && !d._paused && !d._gc) && (d._reversed ? d.render((d._dirty ? d.totalDuration() : d._totalDuration) - (a - d._startTime) * d._timeScale, b, c) : d.render((a - d._startTime) * d._timeScale, b, c)), d = g; else for (d = this._last; d && (g = d._prev, !this._paused || o);)(d._active || l >= d._startTime && !d._paused && !d._gc) && (d._reversed ? d.render((d._dirty ? d.totalDuration() : d._totalDuration) - (a - d._startTime) * d._timeScale, b, c) : d.render((a - d._startTime) * d._timeScale, b, c)), d = g;
                this._onUpdate && (b || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || h)), i && (this._gc || (m === this._startTime || n !== this._timeScale) && (0 === this._time || k >= this.totalDuration()) && (f && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !b && this.vars[i] && this.vars[i].apply(this.vars[i + "Scope"] || this, this.vars[i + "Params"] || h)))
            }
        }, m._hasPausedChild = function () {
            for (var a = this._first; a;) {
                if (a._paused || a instanceof d && a._hasPausedChild())return !0;
                a = a._next
            }
            return !1
        }, m.getChildren = function (a, b, d, e) {
            e = e || -9999999999;
            for (var f = [], g = this._first, h = 0; g;)e > g._startTime || (g instanceof c ? b !== !1 && (f[h++] = g) : (d !== !1 && (f[h++] = g), a !== !1 && (f = f.concat(g.getChildren(!0, b, d)), h = f.length))), g = g._next;
            return f
        }, m.getTweensOf = function (a, b) {
            var d, e, f = this._gc, g = [], h = 0;
            for (f && this._enabled(!0, !0), d = c.getTweensOf(a), e = d.length; --e > -1;)(d[e].timeline === this || b && this._contains(d[e])) && (g[h++] = d[e]);
            return f && this._enabled(!1, !0), g
        }, m._contains = function (a) {
            for (var b = a.timeline; b;) {
                if (b === this)return !0;
                b = b.timeline
            }
            return !1
        }, m.shiftChildren = function (a, b, c) {
            c = c || 0;
            for (var d, e = this._first, f = this._labels; e;)e._startTime >= c && (e._startTime += a), e = e._next;
            if (b)for (d in f)f[d] >= c && (f[d] += a);
            return this._uncache(!0)
        }, m._kill = function (a, b) {
            if (!a && !b)return this._enabled(!1, !1);
            for (var c = b ? this.getTweensOf(b) : this.getChildren(!0, !0, !1), d = c.length, e = !1; --d > -1;)c[d]._kill(a, b) && (e = !0);
            return e
        }, m.clear = function (a) {
            var b = this.getChildren(!1, !0, !0), c = b.length;
            for (this._time = this._totalTime = 0; --c > -1;)b[c]._enabled(!1, !1);
            return a !== !1 && (this._labels = {}), this._uncache(!0)
        }, m.invalidate = function () {
            for (var a = this._first; a;)a.invalidate(), a = a._next;
            return this
        }, m._enabled = function (a, c) {
            if (a === this._gc)for (var d = this._first; d;)d._enabled(a, !0), d = d._next;
            return b.prototype._enabled.call(this, a, c)
        }, m.duration = function (a) {
            return arguments.length ? (0 !== this.duration() && 0 !== a && this.timeScale(this._duration / a), this) : (this._dirty && this.totalDuration(), this._duration)
        }, m.totalDuration = function (a) {
            if (!arguments.length) {
                if (this._dirty) {
                    for (var b, c, d = 0, e = this._last, f = 999999999999; e;)b = e._prev, e._dirty && e.totalDuration(), e._startTime > f && this._sortChildren && !e._paused ? this.add(e, e._startTime - e._delay) : f = e._startTime, 0 > e._startTime && !e._paused && (d -= e._startTime, this._timeline.smoothChildTiming && (this._startTime += e._startTime / this._timeScale), this.shiftChildren(-e._startTime, !1, -9999999999), f = 0), c = e._startTime + e._totalDuration / e._timeScale, c > d && (d = c), e = b;
                    this._duration = this._totalDuration = d, this._dirty = !1
                }
                return this._totalDuration
            }
            return 0 !== this.totalDuration() && 0 !== a && this.timeScale(this._totalDuration / a), this
        }, m.usesFrames = function () {
            for (var b = this._timeline; b._timeline;)b = b._timeline;
            return b === a._rootFramesTimeline
        }, m.rawTime = function () {
            return this._paused ? this._totalTime : (this._timeline.rawTime() - this._startTime) * this._timeScale
        }, d
    }, !0), window._gsDefine("TimelineMax", ["TimelineLite", "TweenLite", "easing.Ease"], function (a, b, c) {
        var d = function (b) {
            a.call(this, b), this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._cycle = 0, this._yoyo = this.vars.yoyo === !0, this._dirty = !0
        }, e = 1e-10, f = [], g = new c(null, null, 1, 0), h = d.prototype = new a;
        return h.constructor = d, h.kill()._gc = !1, d.version = "1.12.1", h.invalidate = function () {
            return this._yoyo = this.vars.yoyo === !0, this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._uncache(!0), a.prototype.invalidate.call(this)
        }, h.addCallback = function (a, c, d, e) {
            return this.add(b.delayedCall(0, a, d, e), c)
        }, h.removeCallback = function (a, b) {
            if (a)if (null == b)this._kill(null, a); else for (var c = this.getTweensOf(a, !1), d = c.length, e = this._parseTimeOrLabel(b); --d > -1;)c[d]._startTime === e && c[d]._enabled(!1, !1);
            return this
        }, h.tweenTo = function (a, c) {
            c = c || {};
            var d, e, h, i = {ease: g, overwrite: c.delay ? 2 : 1, useFrames: this.usesFrames(), immediateRender: !1};
            for (e in c)i[e] = c[e];
            return i.time = this._parseTimeOrLabel(a), d = Math.abs(Number(i.time) - this._time) / this._timeScale || .001, h = new b(this, d, i), i.onStart = function () {
                h.target.paused(!0), h.vars.time !== h.target.time() && d === h.duration() && h.duration(Math.abs(h.vars.time - h.target.time()) / h.target._timeScale), c.onStart && c.onStart.apply(c.onStartScope || h, c.onStartParams || f)
            }, h
        }, h.tweenFromTo = function (a, b, c) {
            c = c || {}, a = this._parseTimeOrLabel(a), c.startAt = {
                onComplete: this.seek,
                onCompleteParams: [a],
                onCompleteScope: this
            }, c.immediateRender = c.immediateRender !== !1;
            var d = this.tweenTo(b, c);
            return d.duration(Math.abs(d.vars.time - a) / this._timeScale || .001)
        }, h.render = function (a, b, c) {
            this._gc && this._enabled(!0, !1);
            var d, g, h, i, j, k, l = this._dirty ? this.totalDuration() : this._totalDuration, m = this._duration, n = this._time, o = this._totalTime, p = this._startTime, q = this._timeScale, r = this._rawPrevTime, s = this._paused, t = this._cycle;
            if (a >= l ? (this._locked || (this._totalTime = l, this._cycle = this._repeat), this._reversed || this._hasPausedChild() || (g = !0, i = "onComplete", 0 === this._duration && (0 === a || 0 > r || r === e) && r !== a && this._first && (j = !0, r > e && (i = "onReverseComplete"))), this._rawPrevTime = this._duration || !b || a || this._rawPrevTime === a ? a : e, this._yoyo && 0 !== (1 & this._cycle) ? this._time = a = 0 : (this._time = m, a = m + 1e-4)) : 1e-7 > a ? (this._locked || (this._totalTime = this._cycle = 0), this._time = 0, (0 !== n || 0 === m && r !== e && (r > 0 || 0 > a && r >= 0) && !this._locked) && (i = "onReverseComplete", g = this._reversed), 0 > a ? (this._active = !1, 0 === m && r >= 0 && this._first && (j = !0), this._rawPrevTime = a) : (this._rawPrevTime = m || !b || a || this._rawPrevTime === a ? a : e, a = 0, this._initted || (j = !0))) : (0 === m && 0 > r && (j = !0), this._time = this._rawPrevTime = a, this._locked || (this._totalTime = a, 0 !== this._repeat && (k = m + this._repeatDelay, this._cycle = this._totalTime / k >> 0, 0 !== this._cycle && this._cycle === this._totalTime / k && this._cycle--, this._time = this._totalTime - this._cycle * k, this._yoyo && 0 !== (1 & this._cycle) && (this._time = m - this._time), this._time > m ? (this._time = m, a = m + 1e-4) : 0 > this._time ? this._time = a = 0 : a = this._time))), this._cycle !== t && !this._locked) {
                var u = this._yoyo && 0 !== (1 & t), v = u === (this._yoyo && 0 !== (1 & this._cycle)), w = this._totalTime, x = this._cycle, y = this._rawPrevTime, z = this._time;
                if (this._totalTime = t * m, t > this._cycle ? u = !u : this._totalTime += m, this._time = n, this._rawPrevTime = 0 === m ? r - 1e-4 : r, this._cycle = t, this._locked = !0, n = u ? 0 : m, this.render(n, b, 0 === m), b || this._gc || this.vars.onRepeat && this.vars.onRepeat.apply(this.vars.onRepeatScope || this, this.vars.onRepeatParams || f), v && (n = u ? m + 1e-4 : -1e-4, this.render(n, !0, !1)), this._locked = !1, this._paused && !s)return;
                this._time = z, this._totalTime = w, this._cycle = x, this._rawPrevTime = y
            }
            if (!(this._time !== n && this._first || c || j))return void(o !== this._totalTime && this._onUpdate && (b || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || f)));
            if (this._initted || (this._initted = !0), this._active || !this._paused && this._totalTime !== o && a > 0 && (this._active = !0), 0 === o && this.vars.onStart && 0 !== this._totalTime && (b || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || f)), this._time >= n)for (d = this._first; d && (h = d._next, !this._paused || s);)(d._active || d._startTime <= this._time && !d._paused && !d._gc) && (d._reversed ? d.render((d._dirty ? d.totalDuration() : d._totalDuration) - (a - d._startTime) * d._timeScale, b, c) : d.render((a - d._startTime) * d._timeScale, b, c)), d = h; else for (d = this._last; d && (h = d._prev, !this._paused || s);)(d._active || n >= d._startTime && !d._paused && !d._gc) && (d._reversed ? d.render((d._dirty ? d.totalDuration() : d._totalDuration) - (a - d._startTime) * d._timeScale, b, c) : d.render((a - d._startTime) * d._timeScale, b, c)), d = h;
            this._onUpdate && (b || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || f)), i && (this._locked || this._gc || (p === this._startTime || q !== this._timeScale) && (0 === this._time || l >= this.totalDuration()) && (g && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !b && this.vars[i] && this.vars[i].apply(this.vars[i + "Scope"] || this, this.vars[i + "Params"] || f)))
        }, h.getActive = function (a, b, c) {
            null == a && (a = !0), null == b && (b = !0), null == c && (c = !1);
            var d, e, f = [], g = this.getChildren(a, b, c), h = 0, i = g.length;
            for (d = 0; i > d; d++)e = g[d], e.isActive() && (f[h++] = e);
            return f
        }, h.getLabelAfter = function (a) {
            a || 0 !== a && (a = this._time);
            var b, c = this.getLabelsArray(), d = c.length;
            for (b = 0; d > b; b++)if (c[b].time > a)return c[b].name;
            return null
        }, h.getLabelBefore = function (a) {
            null == a && (a = this._time);
            for (var b = this.getLabelsArray(), c = b.length; --c > -1;)if (a > b[c].time)return b[c].name;
            return null
        }, h.getLabelsArray = function () {
            var a, b = [], c = 0;
            for (a in this._labels)b[c++] = {time: this._labels[a], name: a};
            return b.sort(function (a, b) {
                return a.time - b.time
            }), b
        }, h.progress = function (a) {
            return arguments.length ? this.totalTime(this.duration() * (this._yoyo && 0 !== (1 & this._cycle) ? 1 - a : a) + this._cycle * (this._duration + this._repeatDelay), !1) : this._time / this.duration()
        }, h.totalProgress = function (a) {
            return arguments.length ? this.totalTime(this.totalDuration() * a, !1) : this._totalTime / this.totalDuration()
        }, h.totalDuration = function (b) {
            return arguments.length ? -1 === this._repeat ? this : this.duration((b - this._repeat * this._repeatDelay) / (this._repeat + 1)) : (this._dirty && (a.prototype.totalDuration.call(this), this._totalDuration = -1 === this._repeat ? 999999999999 : this._duration * (this._repeat + 1) + this._repeatDelay * this._repeat), this._totalDuration)
        }, h.time = function (a, b) {
            return arguments.length ? (this._dirty && this.totalDuration(), a > this._duration && (a = this._duration), this._yoyo && 0 !== (1 & this._cycle) ? a = this._duration - a + this._cycle * (this._duration + this._repeatDelay) : 0 !== this._repeat && (a += this._cycle * (this._duration + this._repeatDelay)), this.totalTime(a, b)) : this._time
        }, h.repeat = function (a) {
            return arguments.length ? (this._repeat = a, this._uncache(!0)) : this._repeat
        }, h.repeatDelay = function (a) {
            return arguments.length ? (this._repeatDelay = a, this._uncache(!0)) : this._repeatDelay
        }, h.yoyo = function (a) {
            return arguments.length ? (this._yoyo = a, this) : this._yoyo
        }, h.currentLabel = function (a) {
            return arguments.length ? this.seek(a, !0) : this.getLabelBefore(this._time + 1e-8)
        }, d
    }, !0), function () {
        var a = 180 / Math.PI, b = [], c = [], d = [], e = {}, f = function (a, b, c, d) {
            this.a = a, this.b = b, this.c = c, this.d = d, this.da = d - a, this.ca = c - a, this.ba = b - a
        }, g = ",x,y,z,left,top,right,bottom,marginTop,marginLeft,marginRight,marginBottom,paddingLeft,paddingTop,paddingRight,paddingBottom,backgroundPosition,backgroundPosition_y,", h = function (a, b, c, d) {
            var e = {a: a}, f = {}, g = {}, h = {c: d}, i = (a + b) / 2, j = (b + c) / 2, k = (c + d) / 2, l = (i + j) / 2, m = (j + k) / 2, n = (m - l) / 8;
            return e.b = i + (a - i) / 4, f.b = l + n, e.c = f.a = (e.b + f.b) / 2, f.c = g.a = (l + m) / 2, g.b = m - n, h.b = k + (d - k) / 4, g.c = h.a = (g.b + h.b) / 2, [e, f, g, h]
        }, i = function (a, e, f, g, i) {
            var j, k, l, m, n, o, p, q, r, s, t, u, v, w = a.length - 1, x = 0, y = a[0].a;
            for (j = 0; w > j; j++)n = a[x], k = n.a, l = n.d, m = a[x + 1].d, i ? (t = b[j], u = c[j], v = .25 * (u + t) * e / (g ? .5 : d[j] || .5), o = l - (l - k) * (g ? .5 * e : 0 !== t ? v / t : 0), p = l + (m - l) * (g ? .5 * e : 0 !== u ? v / u : 0), q = l - (o + ((p - o) * (3 * t / (t + u) + .5) / 4 || 0))) : (o = l - .5 * (l - k) * e, p = l + .5 * (m - l) * e, q = l - (o + p) / 2), o += q, p += q, n.c = r = o, n.b = 0 !== j ? y : y = n.a + .6 * (n.c - n.a), n.da = l - k, n.ca = r - k, n.ba = y - k, f ? (s = h(k, y, r, l), a.splice(x, 1, s[0], s[1], s[2], s[3]), x += 4) : x++, y = p;
            n = a[x], n.b = y, n.c = y + .4 * (n.d - y), n.da = n.d - n.a, n.ca = n.c - n.a, n.ba = y - n.a, f && (s = h(n.a, y, n.c, n.d), a.splice(x, 1, s[0], s[1], s[2], s[3]))
        }, j = function (a, d, e, g) {
            var h, i, j, k, l, m, n = [];
            if (g)for (a = [g].concat(a), i = a.length; --i > -1;)"string" == typeof(m = a[i][d]) && "=" === m.charAt(1) && (a[i][d] = g[d] + Number(m.charAt(0) + m.substr(2)));
            if (h = a.length - 2, 0 > h)return n[0] = new f(a[0][d], 0, 0, a[-1 > h ? 0 : 1][d]), n;
            for (i = 0; h > i; i++)j = a[i][d], k = a[i + 1][d], n[i] = new f(j, 0, 0, k), e && (l = a[i + 2][d], b[i] = (b[i] || 0) + (k - j) * (k - j), c[i] = (c[i] || 0) + (l - k) * (l - k));
            return n[i] = new f(a[i][d], 0, 0, a[i + 1][d]), n
        }, k = function (a, f, h, k, l, m) {
            var n, o, p, q, r, s, t, u, v = {}, w = [], x = m || a[0];
            l = "string" == typeof l ? "," + l + "," : g, null == f && (f = 1);
            for (o in a[0])w.push(o);
            if (a.length > 1) {
                for (u = a[a.length - 1], t = !0, n = w.length; --n > -1;)if (o = w[n], Math.abs(x[o] - u[o]) > .05) {
                    t = !1;
                    break
                }
                t && (a = a.concat(), m && a.unshift(m), a.push(a[1]), m = a[a.length - 3])
            }
            for (b.length = c.length = d.length = 0, n = w.length; --n > -1;)o = w[n], e[o] = -1 !== l.indexOf("," + o + ","), v[o] = j(a, o, e[o], m);
            for (n = b.length; --n > -1;)b[n] = Math.sqrt(b[n]), c[n] = Math.sqrt(c[n]);
            if (!k) {
                for (n = w.length; --n > -1;)if (e[o])for (p = v[w[n]], s = p.length - 1, q = 0; s > q; q++)r = p[q + 1].da / c[q] + p[q].da / b[q], d[q] = (d[q] || 0) + r * r;
                for (n = d.length; --n > -1;)d[n] = Math.sqrt(d[n])
            }
            for (n = w.length, q = h ? 4 : 1; --n > -1;)o = w[n], p = v[o], i(p, f, h, k, e[o]), t && (p.splice(0, q), p.splice(p.length - q, q));
            return v
        }, l = function (a, b, c) {
            b = b || "soft";
            var d, e, g, h, i, j, k, l, m, n, o, p = {}, q = "cubic" === b ? 3 : 2, r = "soft" === b, s = [];
            if (r && c && (a = [c].concat(a)), null == a || q + 1 > a.length)throw"invalid Bezier data";
            for (m in a[0])s.push(m);
            for (j = s.length; --j > -1;) {
                for (m = s[j], p[m] = i = [], n = 0, l = a.length, k = 0; l > k; k++)d = null == c ? a[k][m] : "string" == typeof(o = a[k][m]) && "=" === o.charAt(1) ? c[m] + Number(o.charAt(0) + o.substr(2)) : Number(o), r && k > 1 && l - 1 > k && (i[n++] = (d + i[n - 2]) / 2), i[n++] = d;
                for (l = n - q + 1, n = 0, k = 0; l > k; k += q)d = i[k], e = i[k + 1], g = i[k + 2], h = 2 === q ? 0 : i[k + 3], i[n++] = o = 3 === q ? new f(d, e, g, h) : new f(d, (2 * e + d) / 3, (2 * e + g) / 3, g);
                i.length = n
            }
            return p
        }, m = function (a, b, c) {
            for (var d, e, f, g, h, i, j, k, l, m, n, o = 1 / c, p = a.length; --p > -1;)for (m = a[p], f = m.a, g = m.d - f, h = m.c - f, i = m.b - f, d = e = 0, k = 1; c >= k; k++)j = o * k, l = 1 - j, d = e - (e = (j * j * g + 3 * l * (j * h + l * i)) * j), n = p * c + k - 1, b[n] = (b[n] || 0) + d * d
        }, n = function (a, b) {
            b = b >> 0 || 6;
            var c, d, e, f, g = [], h = [], i = 0, j = 0, k = b - 1, l = [], n = [];
            for (c in a)m(a[c], g, b);
            for (e = g.length, d = 0; e > d; d++)i += Math.sqrt(g[d]), f = d % b, n[f] = i, f === k && (j += i, f = d / b >> 0, l[f] = n, h[f] = j, i = 0, n = []);
            return {length: j, lengths: h, segments: l}
        }, o = window._gsDefine.plugin({
            propName: "bezier", priority: -1, version: "1.3.2", API: 2, global: !0, init: function (a, b, c) {
                this._target = a, b instanceof Array && (b = {values: b}), this._func = {}, this._round = {}, this._props = [], this._timeRes = null == b.timeResolution ? 6 : parseInt(b.timeResolution, 10);
                var d, e, f, g, h, i = b.values || [], j = {}, m = i[0], o = b.autoRotate || c.vars.orientToBezier;
                this._autoRotate = o ? o instanceof Array ? o : [["x", "y", "rotation", o === !0 ? 0 : Number(o) || 0]] : null;
                for (d in m)this._props.push(d);
                for (f = this._props.length; --f > -1;)d = this._props[f], this._overwriteProps.push(d), e = this._func[d] = "function" == typeof a[d], j[d] = e ? a[d.indexOf("set") || "function" != typeof a["get" + d.substr(3)] ? d : "get" + d.substr(3)]() : parseFloat(a[d]), h || j[d] !== i[0][d] && (h = j);
                if (this._beziers = "cubic" !== b.type && "quadratic" !== b.type && "soft" !== b.type ? k(i, isNaN(b.curviness) ? 1 : b.curviness, !1, "thruBasic" === b.type, b.correlate, h) : l(i, b.type, j), this._segCount = this._beziers[d].length, this._timeRes) {
                    var p = n(this._beziers, this._timeRes);
                    this._length = p.length, this._lengths = p.lengths, this._segments = p.segments, this._l1 = this._li = this._s1 = this._si = 0, this._l2 = this._lengths[0], this._curSeg = this._segments[0], this._s2 = this._curSeg[0], this._prec = 1 / this._curSeg.length
                }
                if (o = this._autoRotate)for (this._initialRotations = [], o[0]instanceof Array || (this._autoRotate = o = [o]), f = o.length; --f > -1;) {
                    for (g = 0; 3 > g; g++)d = o[f][g], this._func[d] = "function" == typeof a[d] ? a[d.indexOf("set") || "function" != typeof a["get" + d.substr(3)] ? d : "get" + d.substr(3)] : !1;
                    d = o[f][2], this._initialRotations[f] = this._func[d] ? this._func[d].call(this._target) : this._target[d]
                }
                return this._startRatio = c.vars.runBackwards ? 1 : 0, !0
            }, set: function (b) {
                var c, d, e, f, g, h, i, j, k, l, m = this._segCount, n = this._func, o = this._target, p = b !== this._startRatio;
                if (this._timeRes) {
                    if (k = this._lengths, l = this._curSeg, b *= this._length, e = this._li, b > this._l2 && m - 1 > e) {
                        for (j = m - 1; j > e && b >= (this._l2 = k[++e]););
                        this._l1 = k[e - 1], this._li = e, this._curSeg = l = this._segments[e], this._s2 = l[this._s1 = this._si = 0]
                    } else if (this._l1 > b && e > 0) {
                        for (; e > 0 && (this._l1 = k[--e]) >= b;);
                        0 === e && this._l1 > b ? this._l1 = 0 : e++, this._l2 = k[e], this._li = e, this._curSeg = l = this._segments[e], this._s1 = l[(this._si = l.length - 1) - 1] || 0, this._s2 = l[this._si]
                    }
                    if (c = e, b -= this._l1, e = this._si, b > this._s2 && l.length - 1 > e) {
                        for (j = l.length - 1; j > e && b >= (this._s2 = l[++e]););
                        this._s1 = l[e - 1], this._si = e
                    } else if (this._s1 > b && e > 0) {
                        for (; e > 0 && (this._s1 = l[--e]) >= b;);
                        0 === e && this._s1 > b ? this._s1 = 0 : e++, this._s2 = l[e], this._si = e
                    }
                    h = (e + (b - this._s1) / (this._s2 - this._s1)) * this._prec
                } else c = 0 > b ? 0 : b >= 1 ? m - 1 : m * b >> 0, h = (b - c * (1 / m)) * m;
                for (d = 1 - h, e = this._props.length; --e > -1;)f = this._props[e], g = this._beziers[f][c], i = (h * h * g.da + 3 * d * (h * g.ca + d * g.ba)) * h + g.a, this._round[f] && (i = Math.round(i)), n[f] ? o[f](i) : o[f] = i;
                if (this._autoRotate) {
                    var q, r, s, t, u, v, w, x = this._autoRotate;
                    for (e = x.length; --e > -1;)f = x[e][2], v = x[e][3] || 0, w = x[e][4] === !0 ? 1 : a, g = this._beziers[x[e][0]], q = this._beziers[x[e][1]], g && q && (g = g[c], q = q[c], r = g.a + (g.b - g.a) * h, t = g.b + (g.c - g.b) * h, r += (t - r) * h, t += (g.c + (g.d - g.c) * h - t) * h, s = q.a + (q.b - q.a) * h, u = q.b + (q.c - q.b) * h, s += (u - s) * h, u += (q.c + (q.d - q.c) * h - u) * h, i = p ? Math.atan2(u - s, t - r) * w + v : this._initialRotations[e], n[f] ? o[f](i) : o[f] = i)
                }
            }
        }), p = o.prototype;
        o.bezierThrough = k, o.cubicToQuadratic = h, o._autoCSS = !0, o.quadraticToCubic = function (a, b, c) {
            return new f(a, (2 * b + a) / 3, (2 * b + c) / 3, c)
        }, o._cssRegister = function () {
            var a = window._gsDefine.globals.CSSPlugin;
            if (a) {
                var b = a._internals, c = b._parseToProxy, d = b._setPluginRatio, e = b.CSSPropTween;
                b._registerComplexSpecialProp("bezier", {
                    parser: function (a, b, f, g, h, i) {
                        b instanceof Array && (b = {values: b}), i = new o;
                        var j, k, l, m = b.values, n = m.length - 1, p = [], q = {};
                        if (0 > n)return h;
                        for (j = 0; n >= j; j++)l = c(a, m[j], g, h, i, n !== j), p[j] = l.end;
                        for (k in b)q[k] = b[k];
                        return q.values = p, h = new e(a, "bezier", 0, 0, l.pt, 2), h.data = l, h.plugin = i, h.setRatio = d, 0 === q.autoRotate && (q.autoRotate = !0), !q.autoRotate || q.autoRotate instanceof Array || (j = q.autoRotate === !0 ? 0 : Number(q.autoRotate), q.autoRotate = null != l.end.left ? [["left", "top", "rotation", j, !1]] : null != l.end.x ? [["x", "y", "rotation", j, !1]] : !1), q.autoRotate && (g._transform || g._enableTransforms(!1), l.autoRotate = g._target._gsTransform), i._onInitTween(l.proxy, q, g._tween), h
                    }
                })
            }
        }, p._roundProps = function (a, b) {
            for (var c = this._overwriteProps, d = c.length; --d > -1;)(a[c[d]] || a.bezier || a.bezierThrough) && (this._round[c[d]] = b)
        }, p._kill = function (a) {
            var b, c, d = this._props;
            for (b in this._beziers)if (b in a)for (delete this._beziers[b], delete this._func[b], c = d.length; --c > -1;)d[c] === b && d.splice(c, 1);
            return this._super._kill.call(this, a)
        }
    }(), window._gsDefine("plugins.CSSPlugin", ["plugins.TweenPlugin", "TweenLite"], function (a, b) {
        var c, d, e, f, g = function () {
            a.call(this, "css"), this._overwriteProps.length = 0, this.setRatio = g.prototype.setRatio
        }, h = {}, i = g.prototype = new a("css");
        i.constructor = g, g.version = "1.12.1", g.API = 2, g.defaultTransformPerspective = 0, g.defaultSkewType = "compensated", i = "px", g.suffixMap = {
            top: i,
            right: i,
            bottom: i,
            left: i,
            width: i,
            height: i,
            fontSize: i,
            padding: i,
            margin: i,
            perspective: i,
            lineHeight: ""
        };
        var j, k, l, m, n, o, p = /(?:\d|\-\d|\.\d|\-\.\d)+/g, q = /(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g, r = /(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi, s = /[^\d\-\.]/g, t = /(?:\d|\-|\+|=|#|\.)*/g, u = /opacity *= *([^)]*)/i, v = /opacity:([^;]*)/i, w = /alpha\(opacity *=.+?\)/i, x = /^(rgb|hsl)/, y = /([A-Z])/g, z = /-([a-z])/gi, A = /(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi, B = function (a, b) {
            return b.toUpperCase()
        }, C = /(?:Left|Right|Width)/i, D = /(M11|M12|M21|M22)=[\d\-\.e]+/gi, E = /progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i, F = /,(?=[^\)]*(?:\(|$))/gi, G = Math.PI / 180, H = 180 / Math.PI, I = {}, J = document, K = J.createElement("div"), L = J.createElement("img"), M = g._internals = {_specialProps: h}, N = navigator.userAgent, O = function () {
            var a, b = N.indexOf("Android"), c = J.createElement("div");
            return l = -1 !== N.indexOf("Safari") && -1 === N.indexOf("Chrome") && (-1 === b || Number(N.substr(b + 8, 1)) > 3), n = l && 6 > Number(N.substr(N.indexOf("Version/") + 8, 1)), m = -1 !== N.indexOf("Firefox"), /MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(N) && (o = parseFloat(RegExp.$1)), c.innerHTML = "<a style='top:1px;opacity:.55;'>a</a>", a = c.getElementsByTagName("a")[0], a ? /^0.55/.test(a.style.opacity) : !1
        }(), P = function (a) {
            return u.test("string" == typeof a ? a : (a.currentStyle ? a.currentStyle.filter : a.style.filter) || "") ? parseFloat(RegExp.$1) / 100 : 1
        }, Q = function (a) {
            window.console && console.log(a)
        }, R = "", S = "", T = function (a, b) {
            b = b || K;
            var c, d, e = b.style;
            if (void 0 !== e[a])return a;
            for (a = a.charAt(0).toUpperCase() + a.substr(1), c = ["O", "Moz", "ms", "Ms", "Webkit"], d = 5; --d > -1 && void 0 === e[c[d] + a];);
            return d >= 0 ? (S = 3 === d ? "ms" : c[d], R = "-" + S.toLowerCase() + "-", S + a) : null
        }, U = J.defaultView ? J.defaultView.getComputedStyle : function () {
        }, V = g.getStyle = function (a, b, c, d, e) {
            var f;
            return O || "opacity" !== b ? (!d && a.style[b] ? f = a.style[b] : (c = c || U(a)) ? f = c[b] || c.getPropertyValue(b) || c.getPropertyValue(b.replace(y, "-$1").toLowerCase()) : a.currentStyle && (f = a.currentStyle[b]), null == e || f && "none" !== f && "auto" !== f && "auto auto" !== f ? f : e) : P(a)
        }, W = M.convertToPixels = function (a, c, d, e, f) {
            if ("px" === e || !e)return d;
            if ("auto" === e || !d)return 0;
            var h, i, j, k = C.test(c), l = a, m = K.style, n = 0 > d;
            if (n && (d = -d), "%" === e && -1 !== c.indexOf("border"))h = d / 100 * (k ? a.clientWidth : a.clientHeight); else {
                if (m.cssText = "border:0 solid red;position:" + V(a, "position") + ";line-height:0;", "%" !== e && l.appendChild)m[k ? "borderLeftWidth" : "borderTopWidth"] = d + e; else {
                    if (l = a.parentNode || J.body, i = l._gsCache, j = b.ticker.frame, i && k && i.time === j)return i.width * d / 100;
                    m[k ? "width" : "height"] = d + e
                }
                l.appendChild(K), h = parseFloat(K[k ? "offsetWidth" : "offsetHeight"]), l.removeChild(K), k && "%" === e && g.cacheWidths !== !1 && (i = l._gsCache = l._gsCache || {}, i.time = j, i.width = 100 * (h / d)), 0 !== h || f || (h = W(a, c, d, e, !0))
            }
            return n ? -h : h
        }, X = M.calculateOffset = function (a, b, c) {
            if ("absolute" !== V(a, "position", c))return 0;
            var d = "left" === b ? "Left" : "Top", e = V(a, "margin" + d, c);
            return a["offset" + d] - (W(a, b, parseFloat(e), e.replace(t, "")) || 0)
        }, Y = function (a, b) {
            var c, d, e = {};
            if (b = b || U(a, null))if (c = b.length)for (; --c > -1;)e[b[c].replace(z, B)] = b.getPropertyValue(b[c]); else for (c in b)e[c] = b[c]; else if (b = a.currentStyle || a.style)for (c in b)"string" == typeof c && void 0 === e[c] && (e[c.replace(z, B)] = b[c]);
            return O || (e.opacity = P(a)), d = yb(a, b, !1), e.rotation = d.rotation, e.skewX = d.skewX, e.scaleX = d.scaleX, e.scaleY = d.scaleY, e.x = d.x, e.y = d.y, wb && (e.z = d.z, e.rotationX = d.rotationX, e.rotationY = d.rotationY, e.scaleZ = d.scaleZ), e.filters && delete e.filters, e
        }, Z = function (a, b, c, d, e) {
            var f, g, h, i = {}, j = a.style;
            for (g in c)"cssText" !== g && "length" !== g && isNaN(g) && (b[g] !== (f = c[g]) || e && e[g]) && -1 === g.indexOf("Origin") && ("number" == typeof f || "string" == typeof f) && (i[g] = "auto" !== f || "left" !== g && "top" !== g ? "" !== f && "auto" !== f && "none" !== f || "string" != typeof b[g] || "" === b[g].replace(s, "") ? f : 0 : X(a, g), void 0 !== j[g] && (h = new lb(j, g, j[g], h)));
            if (d)for (g in d)"className" !== g && (i[g] = d[g]);
            return {difs: i, firstMPT: h}
        }, $ = {
            width: ["Left", "Right"],
            height: ["Top", "Bottom"]
        }, _ = ["marginLeft", "marginRight", "marginTop", "marginBottom"], ab = function (a, b, c) {
            var d = parseFloat("width" === b ? a.offsetWidth : a.offsetHeight), e = $[b], f = e.length;
            for (c = c || U(a, null); --f > -1;)d -= parseFloat(V(a, "padding" + e[f], c, !0)) || 0, d -= parseFloat(V(a, "border" + e[f] + "Width", c, !0)) || 0;
            return d
        }, bb = function (a, b) {
            (null == a || "" === a || "auto" === a || "auto auto" === a) && (a = "0 0");
            var c = a.split(" "), d = -1 !== a.indexOf("left") ? "0%" : -1 !== a.indexOf("right") ? "100%" : c[0], e = -1 !== a.indexOf("top") ? "0%" : -1 !== a.indexOf("bottom") ? "100%" : c[1];
            return null == e ? e = "0" : "center" === e && (e = "50%"), ("center" === d || isNaN(parseFloat(d)) && -1 === (d + "").indexOf("=")) && (d = "50%"), b && (b.oxp = -1 !== d.indexOf("%"), b.oyp = -1 !== e.indexOf("%"), b.oxr = "=" === d.charAt(1), b.oyr = "=" === e.charAt(1), b.ox = parseFloat(d.replace(s, "")), b.oy = parseFloat(e.replace(s, ""))), d + " " + e + (c.length > 2 ? " " + c[2] : "")
        }, cb = function (a, b) {
            return "string" == typeof a && "=" === a.charAt(1) ? parseInt(a.charAt(0) + "1", 10) * parseFloat(a.substr(2)) : parseFloat(a) - parseFloat(b)
        }, db = function (a, b) {
            return null == a ? b : "string" == typeof a && "=" === a.charAt(1) ? parseInt(a.charAt(0) + "1", 10) * Number(a.substr(2)) + b : parseFloat(a)
        }, eb = function (a, b, c, d) {
            var e, f, g, h, i = 1e-6;
            return null == a ? h = b : "number" == typeof a ? h = a : (e = 360, f = a.split("_"), g = Number(f[0].replace(s, "")) * (-1 === a.indexOf("rad") ? 1 : H) - ("=" === a.charAt(1) ? 0 : b), f.length && (d && (d[c] = b + g), -1 !== a.indexOf("short") && (g %= e, g !== g % (e / 2) && (g = 0 > g ? g + e : g - e)), -1 !== a.indexOf("_cw") && 0 > g ? g = (g + 9999999999 * e) % e - (0 | g / e) * e : -1 !== a.indexOf("ccw") && g > 0 && (g = (g - 9999999999 * e) % e - (0 | g / e) * e)), h = b + g), i > h && h > -i && (h = 0), h
        }, fb = {
            aqua: [0, 255, 255],
            lime: [0, 255, 0],
            silver: [192, 192, 192],
            black: [0, 0, 0],
            maroon: [128, 0, 0],
            teal: [0, 128, 128],
            blue: [0, 0, 255],
            navy: [0, 0, 128],
            white: [255, 255, 255],
            fuchsia: [255, 0, 255],
            olive: [128, 128, 0],
            yellow: [255, 255, 0],
            orange: [255, 165, 0],
            gray: [128, 128, 128],
            purple: [128, 0, 128],
            green: [0, 128, 0],
            red: [255, 0, 0],
            pink: [255, 192, 203],
            cyan: [0, 255, 255],
            transparent: [255, 255, 255, 0]
        }, gb = function (a, b, c) {
            return a = 0 > a ? a + 1 : a > 1 ? a - 1 : a, 0 | 255 * (1 > 6 * a ? b + 6 * (c - b) * a : .5 > a ? c : 2 > 3 * a ? b + 6 * (c - b) * (2 / 3 - a) : b) + .5
        }, hb = function (a) {
            var b, c, d, e, f, g;
            return a && "" !== a ? "number" == typeof a ? [a >> 16, 255 & a >> 8, 255 & a] : ("," === a.charAt(a.length - 1) && (a = a.substr(0, a.length - 1)), fb[a] ? fb[a] : "#" === a.charAt(0) ? (4 === a.length && (b = a.charAt(1), c = a.charAt(2), d = a.charAt(3), a = "#" + b + b + c + c + d + d), a = parseInt(a.substr(1), 16), [a >> 16, 255 & a >> 8, 255 & a]) : "hsl" === a.substr(0, 3) ? (a = a.match(p), e = Number(a[0]) % 360 / 360, f = Number(a[1]) / 100, g = Number(a[2]) / 100, c = .5 >= g ? g * (f + 1) : g + f - g * f, b = 2 * g - c, a.length > 3 && (a[3] = Number(a[3])), a[0] = gb(e + 1 / 3, b, c), a[1] = gb(e, b, c), a[2] = gb(e - 1 / 3, b, c), a) : (a = a.match(p) || fb.transparent, a[0] = Number(a[0]), a[1] = Number(a[1]), a[2] = Number(a[2]), a.length > 3 && (a[3] = Number(a[3])), a)) : fb.black
        }, ib = "(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#.+?\\b";
        for (i in fb)ib += "|" + i + "\\b";
        ib = RegExp(ib + ")", "gi");
        var jb = function (a, b, c, d) {
            if (null == a)return function (a) {
                return a
            };
            var e, f = b ? (a.match(ib) || [""])[0] : "", g = a.split(f).join("").match(r) || [], h = a.substr(0, a.indexOf(g[0])), i = ")" === a.charAt(a.length - 1) ? ")" : "", j = -1 !== a.indexOf(" ") ? " " : ",", k = g.length, l = k > 0 ? g[0].replace(p, "") : "";
            return k ? e = b ? function (a) {
                var b, m, n, o;
                if ("number" == typeof a)a += l; else if (d && F.test(a)) {
                    for (o = a.replace(F, "|").split("|"), n = 0; o.length > n; n++)o[n] = e(o[n]);
                    return o.join(",")
                }
                if (b = (a.match(ib) || [f])[0], m = a.split(b).join("").match(r) || [], n = m.length, k > n--)for (; k > ++n;)m[n] = c ? m[0 | (n - 1) / 2] : g[n];
                return h + m.join(j) + j + b + i + (-1 !== a.indexOf("inset") ? " inset" : "")
            } : function (a) {
                var b, f, m;
                if ("number" == typeof a)a += l; else if (d && F.test(a)) {
                    for (f = a.replace(F, "|").split("|"), m = 0; f.length > m; m++)f[m] = e(f[m]);
                    return f.join(",")
                }
                if (b = a.match(r) || [], m = b.length, k > m--)for (; k > ++m;)b[m] = c ? b[0 | (m - 1) / 2] : g[m];
                return h + b.join(j) + i
            } : function (a) {
                return a
            }
        }, kb = function (a) {
            return a = a.split(","), function (b, c, d, e, f, g, h) {
                var i, j = (c + "").split(" ");
                for (h = {}, i = 0; 4 > i; i++)h[a[i]] = j[i] = j[i] || j[(i - 1) / 2 >> 0];
                return e.parse(b, h, f, g)
            }
        }, lb = (M._setPluginRatio = function (a) {
            this.plugin.setRatio(a);
            for (var b, c, d, e, f = this.data, g = f.proxy, h = f.firstMPT, i = 1e-6; h;)b = g[h.v], h.r ? b = Math.round(b) : i > b && b > -i && (b = 0), h.t[h.p] = b, h = h._next;
            if (f.autoRotate && (f.autoRotate.rotation = g.rotation), 1 === a)for (h = f.firstMPT; h;) {
                if (c = h.t, c.type) {
                    if (1 === c.type) {
                        for (e = c.xs0 + c.s + c.xs1, d = 1; c.l > d; d++)e += c["xn" + d] + c["xs" + (d + 1)];
                        c.e = e
                    }
                } else c.e = c.s + c.xs0;
                h = h._next
            }
        }, function (a, b, c, d, e) {
            this.t = a, this.p = b, this.v = c, this.r = e, d && (d._prev = this, this._next = d)
        }), mb = (M._parseToProxy = function (a, b, c, d, e, f) {
            var g, h, i, j, k, l = d, m = {}, n = {}, o = c._transform, p = I;
            for (c._transform = null, I = b, d = k = c.parse(a, b, d, e), I = p, f && (c._transform = o, l && (l._prev = null, l._prev && (l._prev._next = null))); d && d !== l;) {
                if (1 >= d.type && (h = d.p, n[h] = d.s + d.c, m[h] = d.s, f || (j = new lb(d, "s", h, j, d.r), d.c = 0), 1 === d.type))for (g = d.l; --g > 0;)i = "xn" + g, h = d.p + "_" + i, n[h] = d.data[i], m[h] = d[i], f || (j = new lb(d, i, h, j, d.rxp[i]));
                d = d._next
            }
            return {proxy: m, end: n, firstMPT: j, pt: k}
        }, M.CSSPropTween = function (a, b, d, e, g, h, i, j, k, l, m) {
            this.t = a, this.p = b, this.s = d, this.c = e, this.n = i || b, a instanceof mb || f.push(this.n), this.r = j, this.type = h || 0, k && (this.pr = k, c = !0), this.b = void 0 === l ? d : l, this.e = void 0 === m ? d + e : m, g && (this._next = g, g._prev = this)
        }), nb = g.parseComplex = function (a, b, c, d, e, f, g, h, i, k) {
            c = c || f || "", g = new mb(a, b, 0, 0, g, k ? 2 : 1, null, !1, h, c, d), d += "";
            var l, m, n, o, r, s, t, u, v, w, y, z, A = c.split(", ").join(",").split(" "), B = d.split(", ").join(",").split(" "), C = A.length, D = j !== !1;
            for ((-1 !== d.indexOf(",") || -1 !== c.indexOf(",")) && (A = A.join(" ").replace(F, ", ").split(" "), B = B.join(" ").replace(F, ", ").split(" "), C = A.length), C !== B.length && (A = (f || "").split(" "), C = A.length), g.plugin = i, g.setRatio = k, l = 0; C > l; l++)if (o = A[l], r = B[l], u = parseFloat(o), u || 0 === u)g.appendXtra("", u, cb(r, u), r.replace(q, ""), D && -1 !== r.indexOf("px"), !0); else if (e && ("#" === o.charAt(0) || fb[o] || x.test(o)))z = "," === r.charAt(r.length - 1) ? ")," : ")", o = hb(o), r = hb(r), v = o.length + r.length > 6, v && !O && 0 === r[3] ? (g["xs" + g.l] += g.l ? " transparent" : "transparent", g.e = g.e.split(B[l]).join("transparent")) : (O || (v = !1), g.appendXtra(v ? "rgba(" : "rgb(", o[0], r[0] - o[0], ",", !0, !0).appendXtra("", o[1], r[1] - o[1], ",", !0).appendXtra("", o[2], r[2] - o[2], v ? "," : z, !0), v && (o = 4 > o.length ? 1 : o[3], g.appendXtra("", o, (4 > r.length ? 1 : r[3]) - o, z, !1))); else if (s = o.match(p)) {
                if (t = r.match(q), !t || t.length !== s.length)return g;
                for (n = 0, m = 0; s.length > m; m++)y = s[m], w = o.indexOf(y, n), g.appendXtra(o.substr(n, w - n), Number(y), cb(t[m], y), "", D && "px" === o.substr(w + y.length, 2), 0 === m), n = w + y.length;
                g["xs" + g.l] += o.substr(n)
            } else g["xs" + g.l] += g.l ? " " + o : o;
            if (-1 !== d.indexOf("=") && g.data) {
                for (z = g.xs0 + g.data.s, l = 1; g.l > l; l++)z += g["xs" + l] + g.data["xn" + l];
                g.e = z + g["xs" + l]
            }
            return g.l || (g.type = -1, g.xs0 = g.e), g.xfirst || g
        }, ob = 9;
        for (i = mb.prototype, i.l = i.pr = 0; --ob > 0;)i["xn" + ob] = 0, i["xs" + ob] = "";
        i.xs0 = "", i._next = i._prev = i.xfirst = i.data = i.plugin = i.setRatio = i.rxp = null, i.appendXtra = function (a, b, c, d, e, f) {
            var g = this, h = g.l;
            return g["xs" + h] += f && h ? " " + a : a || "", c || 0 === h || g.plugin ? (g.l++, g.type = g.setRatio ? 2 : 1, g["xs" + g.l] = d || "", h > 0 ? (g.data["xn" + h] = b + c, g.rxp["xn" + h] = e, g["xn" + h] = b, g.plugin || (g.xfirst = new mb(g, "xn" + h, b, c, g.xfirst || g, 0, g.n, e, g.pr), g.xfirst.xs0 = 0), g) : (g.data = {s: b + c}, g.rxp = {}, g.s = b, g.c = c, g.r = e, g)) : (g["xs" + h] += b + (d || ""), g)
        };
        var pb = function (a, b) {
            b = b || {}, this.p = b.prefix ? T(a) || a : a, h[a] = h[this.p] = this, this.format = b.formatter || jb(b.defaultValue, b.color, b.collapsible, b.multi), b.parser && (this.parse = b.parser), this.clrs = b.color, this.multi = b.multi, this.keyword = b.keyword, this.dflt = b.defaultValue, this.pr = b.priority || 0
        }, qb = M._registerComplexSpecialProp = function (a, b, c) {
            "object" != typeof b && (b = {parser: c});
            var d, e, f = a.split(","), g = b.defaultValue;
            for (c = c || [g], d = 0; f.length > d; d++)b.prefix = 0 === d && b.prefix, b.defaultValue = c[d] || g, e = new pb(f[d], b)
        }, rb = function (a) {
            if (!h[a]) {
                var b = a.charAt(0).toUpperCase() + a.substr(1) + "Plugin";
                qb(a, {
                    parser: function (a, c, d, e, f, g, i) {
                        var j = (window.GreenSockGlobals || window).com.greensock.plugins[b];
                        return j ? (j._cssRegister(), h[d].parse(a, c, d, e, f, g, i)) : (Q("Error: " + b + " js file not loaded."), f)
                    }
                })
            }
        };
        i = pb.prototype, i.parseComplex = function (a, b, c, d, e, f) {
            var g, h, i, j, k, l, m = this.keyword;
            if (this.multi && (F.test(c) || F.test(b) ? (h = b.replace(F, "|").split("|"), i = c.replace(F, "|").split("|")) : m && (h = [b], i = [c])), i) {
                for (j = i.length > h.length ? i.length : h.length, g = 0; j > g; g++)b = h[g] = h[g] || this.dflt, c = i[g] = i[g] || this.dflt, m && (k = b.indexOf(m), l = c.indexOf(m), k !== l && (c = -1 === l ? i : h, c[g] += " " + m));
                b = h.join(", "), c = i.join(", ")
            }
            return nb(a, this.p, b, c, this.clrs, this.dflt, d, this.pr, e, f)
        }, i.parse = function (a, b, c, d, f, g) {
            return this.parseComplex(a.style, this.format(V(a, this.p, e, !1, this.dflt)), this.format(b), f, g)
        }, g.registerSpecialProp = function (a, b, c) {
            qb(a, {
                parser: function (a, d, e, f, g, h) {
                    var i = new mb(a, e, 0, 0, g, 2, e, !1, c);
                    return i.plugin = h, i.setRatio = b(a, d, f._tween, e), i
                }, priority: c
            })
        };
        var sb = "scaleX,scaleY,scaleZ,x,y,z,skewX,skewY,rotation,rotationX,rotationY,perspective".split(","), tb = T("transform"), ub = R + "transform", vb = T("transformOrigin"), wb = null !== T("perspective"), xb = M.Transform = function () {
            this.skewY = 0
        }, yb = M.getTransform = function (a, b, c, d) {
            if (a._gsTransform && c && !d)return a._gsTransform;
            var e, f, h, i, j, k, l, m, n, o, p, q, r, s = c ? a._gsTransform || new xb : new xb, t = 0 > s.scaleX, u = 2e-5, v = 1e5, w = 179.99, x = w * G, y = wb ? parseFloat(V(a, vb, b, !1, "0 0 0").split(" ")[2]) || s.zOrigin || 0 : 0;
            for (tb ? e = V(a, ub, b, !0) : a.currentStyle && (e = a.currentStyle.filter.match(D), e = e && 4 === e.length ? [e[0].substr(4), Number(e[2].substr(4)), Number(e[1].substr(4)), e[3].substr(4), s.x || 0, s.y || 0].join(",") : ""), f = (e || "").match(/(?:\-|\b)[\d\-\.e]+\b/gi) || [], h = f.length; --h > -1;)i = Number(f[h]), f[h] = (j = i - (i |= 0)) ? (0 | j * v + (0 > j ? -.5 : .5)) / v + i : i;
            if (16 === f.length) {
                var z = f[8], A = f[9], B = f[10], C = f[12], E = f[13], F = f[14];
                if (s.zOrigin && (F = -s.zOrigin, C = z * F - f[12], E = A * F - f[13], F = B * F + s.zOrigin - f[14]), !c || d || null == s.rotationX) {
                    var I, J, K, L, M, N, O, P = f[0], Q = f[1], R = f[2], S = f[3], T = f[4], U = f[5], W = f[6], X = f[7], Y = f[11], Z = Math.atan2(W, B), $ = -x > Z || Z > x;
                    s.rotationX = Z * H, Z && (L = Math.cos(-Z), M = Math.sin(-Z), I = T * L + z * M, J = U * L + A * M, K = W * L + B * M, z = T * -M + z * L, A = U * -M + A * L, B = W * -M + B * L, Y = X * -M + Y * L, T = I, U = J, W = K), Z = Math.atan2(z, P), s.rotationY = Z * H, Z && (N = -x > Z || Z > x, L = Math.cos(-Z), M = Math.sin(-Z), I = P * L - z * M, J = Q * L - A * M, K = R * L - B * M, A = Q * M + A * L, B = R * M + B * L, Y = S * M + Y * L, P = I, Q = J, R = K), Z = Math.atan2(Q, U), s.rotation = Z * H, Z && (O = -x > Z || Z > x, L = Math.cos(-Z), M = Math.sin(-Z), P = P * L + T * M, J = Q * L + U * M, U = Q * -M + U * L, W = R * -M + W * L, Q = J), O && $ ? s.rotation = s.rotationX = 0 : O && N ? s.rotation = s.rotationY = 0 : N && $ && (s.rotationY = s.rotationX = 0), s.scaleX = (0 | Math.sqrt(P * P + Q * Q) * v + .5) / v, s.scaleY = (0 | Math.sqrt(U * U + A * A) * v + .5) / v, s.scaleZ = (0 | Math.sqrt(W * W + B * B) * v + .5) / v, s.skewX = 0, s.perspective = Y ? 1 / (0 > Y ? -Y : Y) : 0, s.x = C, s.y = E, s.z = F
                }
            } else if (!(wb && !d && f.length && s.x === f[4] && s.y === f[5] && (s.rotationX || s.rotationY) || void 0 !== s.x && "none" === V(a, "display", b))) {
                var _ = f.length >= 6, ab = _ ? f[0] : 1, bb = f[1] || 0, cb = f[2] || 0, db = _ ? f[3] : 1;
                s.x = f[4] || 0, s.y = f[5] || 0, k = Math.sqrt(ab * ab + bb * bb), l = Math.sqrt(db * db + cb * cb), m = ab || bb ? Math.atan2(bb, ab) * H : s.rotation || 0, n = cb || db ? Math.atan2(cb, db) * H + m : s.skewX || 0, o = k - Math.abs(s.scaleX || 0), p = l - Math.abs(s.scaleY || 0), Math.abs(n) > 90 && 270 > Math.abs(n) && (t ? (k *= -1, n += 0 >= m ? 180 : -180, m += 0 >= m ? 180 : -180) : (l *= -1, n += 0 >= n ? 180 : -180)), q = (m - s.rotation) % 180, r = (n - s.skewX) % 180, (void 0 === s.skewX || o > u || -u > o || p > u || -u > p || q > -w && w > q && !1 | q * v || r > -w && w > r && !1 | r * v) && (s.scaleX = k, s.scaleY = l, s.rotation = m, s.skewX = n), wb && (s.rotationX = s.rotationY = s.z = 0, s.perspective = parseFloat(g.defaultTransformPerspective) || 0, s.scaleZ = 1)
            }
            s.zOrigin = y;
            for (h in s)u > s[h] && s[h] > -u && (s[h] = 0);
            return c && (a._gsTransform = s), s
        }, zb = function (a) {
            var b, c, d = this.data, e = -d.rotation * G, f = e + d.skewX * G, g = 1e5, h = (0 | Math.cos(e) * d.scaleX * g) / g, i = (0 | Math.sin(e) * d.scaleX * g) / g, j = (0 | Math.sin(f) * -d.scaleY * g) / g, k = (0 | Math.cos(f) * d.scaleY * g) / g, l = this.t.style, m = this.t.currentStyle;
            if (m) {
                c = i, i = -j, j = -c, b = m.filter, l.filter = "";
                var n, p, q = this.t.offsetWidth, r = this.t.offsetHeight, s = "absolute" !== m.position, v = "progid:DXImageTransform.Microsoft.Matrix(M11=" + h + ", M12=" + i + ", M21=" + j + ", M22=" + k, w = d.x, x = d.y;
                if (null != d.ox && (n = (d.oxp ? .01 * q * d.ox : d.ox) - q / 2, p = (d.oyp ? .01 * r * d.oy : d.oy) - r / 2, w += n - (n * h + p * i), x += p - (n * j + p * k)), s ? (n = q / 2, p = r / 2, v += ", Dx=" + (n - (n * h + p * i) + w) + ", Dy=" + (p - (n * j + p * k) + x) + ")") : v += ", sizingMethod='auto expand')", l.filter = -1 !== b.indexOf("DXImageTransform.Microsoft.Matrix(") ? b.replace(E, v) : v + " " + b, (0 === a || 1 === a) && 1 === h && 0 === i && 0 === j && 1 === k && (s && -1 === v.indexOf("Dx=0, Dy=0") || u.test(b) && 100 !== parseFloat(RegExp.$1) || -1 === b.indexOf("gradient(" && b.indexOf("Alpha")) && l.removeAttribute("filter")), !s) {
                    var y, z, A, B = 8 > o ? 1 : -1;
                    for (n = d.ieOffsetX || 0, p = d.ieOffsetY || 0, d.ieOffsetX = Math.round((q - ((0 > h ? -h : h) * q + (0 > i ? -i : i) * r)) / 2 + w), d.ieOffsetY = Math.round((r - ((0 > k ? -k : k) * r + (0 > j ? -j : j) * q)) / 2 + x), ob = 0; 4 > ob; ob++)z = _[ob], y = m[z], c = -1 !== y.indexOf("px") ? parseFloat(y) : W(this.t, z, parseFloat(y), y.replace(t, "")) || 0, A = c !== d[z] ? 2 > ob ? -d.ieOffsetX : -d.ieOffsetY : 2 > ob ? n - d.ieOffsetX : p - d.ieOffsetY, l[z] = (d[z] = Math.round(c - A * (0 === ob || 2 === ob ? 1 : B))) + "px"
                }
            }
        }, Ab = M.set3DTransformRatio = function (a) {
            var b, c, d, e, f, g, h, i, j, k, l, n, o, p, q, r, s, t, u, v, w, x, y, z = this.data, A = this.t.style, B = z.rotation * G, C = z.scaleX, D = z.scaleY, E = z.scaleZ, F = z.perspective;
            if (!(1 !== a && 0 !== a || "auto" !== z.force3D || z.rotationY || z.rotationX || 1 !== E || F || z.z))return void Bb.call(this, a);
            if (m) {
                var H = 1e-4;
                H > C && C > -H && (C = E = 2e-5), H > D && D > -H && (D = E = 2e-5), !F || z.z || z.rotationX || z.rotationY || (F = 0)
            }
            if (B || z.skewX)t = Math.cos(B), u = Math.sin(B), b = t, f = u, z.skewX && (B -= z.skewX * G, t = Math.cos(B), u = Math.sin(B), "simple" === z.skewType && (v = Math.tan(z.skewX * G), v = Math.sqrt(1 + v * v), t *= v, u *= v)), c = -u, g = t; else {
                if (!(z.rotationY || z.rotationX || 1 !== E || F))return void(A[tb] = "translate3d(" + z.x + "px," + z.y + "px," + z.z + "px)" + (1 !== C || 1 !== D ? " scale(" + C + "," + D + ")" : ""));
                b = g = 1, c = f = 0
            }
            l = 1, d = e = h = i = j = k = n = o = p = 0, q = F ? -1 / F : 0, r = z.zOrigin, s = 1e5, B = z.rotationY * G, B && (t = Math.cos(B), u = Math.sin(B), j = l * -u, o = q * -u, d = b * u, h = f * u, l *= t, q *= t, b *= t, f *= t), B = z.rotationX * G, B && (t = Math.cos(B), u = Math.sin(B), v = c * t + d * u, w = g * t + h * u, x = k * t + l * u, y = p * t + q * u, d = c * -u + d * t, h = g * -u + h * t, l = k * -u + l * t, q = p * -u + q * t, c = v, g = w, k = x, p = y), 1 !== E && (d *= E, h *= E, l *= E, q *= E), 1 !== D && (c *= D, g *= D, k *= D, p *= D), 1 !== C && (b *= C, f *= C, j *= C, o *= C), r && (n -= r, e = d * n, i = h * n, n = l * n + r), e = (v = (e += z.x) - (e |= 0)) ? (0 | v * s + (0 > v ? -.5 : .5)) / s + e : e, i = (v = (i += z.y) - (i |= 0)) ? (0 | v * s + (0 > v ? -.5 : .5)) / s + i : i, n = (v = (n += z.z) - (n |= 0)) ? (0 | v * s + (0 > v ? -.5 : .5)) / s + n : n, A[tb] = "matrix3d(" + [(0 | b * s) / s, (0 | f * s) / s, (0 | j * s) / s, (0 | o * s) / s, (0 | c * s) / s, (0 | g * s) / s, (0 | k * s) / s, (0 | p * s) / s, (0 | d * s) / s, (0 | h * s) / s, (0 | l * s) / s, (0 | q * s) / s, e, i, n, F ? 1 + -n / F : 1].join(",") + ")"
        }, Bb = M.set2DTransformRatio = function (a) {
            var b, c, d, e, f, g = this.data, h = this.t, i = h.style;
            return g.rotationX || g.rotationY || g.z || g.force3D === !0 || "auto" === g.force3D && 1 !== a && 0 !== a ? (this.setRatio = Ab, void Ab.call(this, a)) : void(g.rotation || g.skewX ? (b = g.rotation * G, c = b - g.skewX * G, d = 1e5, e = g.scaleX * d, f = g.scaleY * d, i[tb] = "matrix(" + (0 | Math.cos(b) * e) / d + "," + (0 | Math.sin(b) * e) / d + "," + (0 | Math.sin(c) * -f) / d + "," + (0 | Math.cos(c) * f) / d + "," + g.x + "," + g.y + ")") : i[tb] = "matrix(" + g.scaleX + ",0,0," + g.scaleY + "," + g.x + "," + g.y + ")")
        };
        qb("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,transformPerspective,directionalRotation,parseTransform,force3D,skewType", {
            parser: function (a, b, c, d, f, h, i) {
                if (d._transform)return f;
                var j, k, l, m, n, o, p, q = d._transform = yb(a, e, !0, i.parseTransform), r = a.style, s = 1e-6, t = sb.length, u = i, v = {};
                if ("string" == typeof u.transform && tb)l = K.style, l[tb] = u.transform, l.display = "block", l.position = "absolute", J.body.appendChild(K), j = yb(K, null, !1), J.body.removeChild(K); else if ("object" == typeof u) {
                    if (j = {
                            scaleX: db(null != u.scaleX ? u.scaleX : u.scale, q.scaleX),
                            scaleY: db(null != u.scaleY ? u.scaleY : u.scale, q.scaleY),
                            scaleZ: db(u.scaleZ, q.scaleZ),
                            x: db(u.x, q.x),
                            y: db(u.y, q.y),
                            z: db(u.z, q.z),
                            perspective: db(u.transformPerspective, q.perspective)
                        }, p = u.directionalRotation, null != p)if ("object" == typeof p)for (l in p)u[l] = p[l]; else u.rotation = p;
                    j.rotation = eb("rotation"in u ? u.rotation : "shortRotation"in u ? u.shortRotation + "_short" : "rotationZ"in u ? u.rotationZ : q.rotation, q.rotation, "rotation", v), wb && (j.rotationX = eb("rotationX"in u ? u.rotationX : "shortRotationX"in u ? u.shortRotationX + "_short" : q.rotationX || 0, q.rotationX, "rotationX", v), j.rotationY = eb("rotationY"in u ? u.rotationY : "shortRotationY"in u ? u.shortRotationY + "_short" : q.rotationY || 0, q.rotationY, "rotationY", v)), j.skewX = null == u.skewX ? q.skewX : eb(u.skewX, q.skewX), j.skewY = null == u.skewY ? q.skewY : eb(u.skewY, q.skewY), (k = j.skewY - q.skewY) && (j.skewX += k, j.rotation += k)
                }
                for (wb && null != u.force3D && (q.force3D = u.force3D, o = !0), q.skewType = u.skewType || q.skewType || g.defaultSkewType, n = q.force3D || q.z || q.rotationX || q.rotationY || j.z || j.rotationX || j.rotationY || j.perspective, n || null == u.scale || (j.scaleZ = 1); --t > -1;)c = sb[t], m = j[c] - q[c], (m > s || -s > m || null != I[c]) && (o = !0, f = new mb(q, c, q[c], m, f), c in v && (f.e = v[c]), f.xs0 = 0, f.plugin = h, d._overwriteProps.push(f.n));
                return m = u.transformOrigin, (m || wb && n && q.zOrigin) && (tb ? (o = !0, c = vb, m = (m || V(a, c, e, !1, "50% 50%")) + "", f = new mb(r, c, 0, 0, f, -1, "transformOrigin"), f.b = r[c], f.plugin = h, wb ? (l = q.zOrigin, m = m.split(" "), q.zOrigin = (m.length > 2 && (0 === l || "0px" !== m[2]) ? parseFloat(m[2]) : l) || 0, f.xs0 = f.e = m[0] + " " + (m[1] || "50%") + " 0px", f = new mb(q, "zOrigin", 0, 0, f, -1, f.n), f.b = l, f.xs0 = f.e = q.zOrigin) : f.xs0 = f.e = m) : bb(m + "", q)), o && (d._transformType = n || 3 === this._transformType ? 3 : 2), f
            }, prefix: !0
        }), qb("boxShadow", {
            defaultValue: "0px 0px 0px 0px #999",
            prefix: !0,
            color: !0,
            multi: !0,
            keyword: "inset"
        }), qb("borderRadius", {
            defaultValue: "0px", parser: function (a, b, c, f, g) {
                b = this.format(b);
                var h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x = ["borderTopLeftRadius", "borderTopRightRadius", "borderBottomRightRadius", "borderBottomLeftRadius"], y = a.style;
                for (p = parseFloat(a.offsetWidth), q = parseFloat(a.offsetHeight), h = b.split(" "), i = 0; x.length > i; i++)this.p.indexOf("border") && (x[i] = T(x[i])), l = k = V(a, x[i], e, !1, "0px"), -1 !== l.indexOf(" ") && (k = l.split(" "), l = k[0], k = k[1]), m = j = h[i], n = parseFloat(l), s = l.substr((n + "").length), t = "=" === m.charAt(1), t ? (o = parseInt(m.charAt(0) + "1", 10), m = m.substr(2), o *= parseFloat(m), r = m.substr((o + "").length - (0 > o ? 1 : 0)) || "") : (o = parseFloat(m), r = m.substr((o + "").length)), "" === r && (r = d[c] || s), r !== s && (u = W(a, "borderLeft", n, s), v = W(a, "borderTop", n, s), "%" === r ? (l = 100 * (u / p) + "%", k = 100 * (v / q) + "%") : "em" === r ? (w = W(a, "borderLeft", 1, "em"), l = u / w + "em", k = v / w + "em") : (l = u + "px", k = v + "px"), t && (m = parseFloat(l) + o + r, j = parseFloat(k) + o + r)), g = nb(y, x[i], l + " " + k, m + " " + j, !1, "0px", g);
                return g
            }, prefix: !0, formatter: jb("0px 0px 0px 0px", !1, !0)
        }), qb("backgroundPosition", {
            defaultValue: "0 0", parser: function (a, b, c, d, f, g) {
                var h, i, j, k, l, m, n = "background-position", p = e || U(a, null), q = this.format((p ? o ? p.getPropertyValue(n + "-x") + " " + p.getPropertyValue(n + "-y") : p.getPropertyValue(n) : a.currentStyle.backgroundPositionX + " " + a.currentStyle.backgroundPositionY) || "0 0"), r = this.format(b);
                if (-1 !== q.indexOf("%") != (-1 !== r.indexOf("%")) && (m = V(a, "backgroundImage").replace(A, ""), m && "none" !== m)) {
                    for (h = q.split(" "), i = r.split(" "), L.setAttribute("src", m), j = 2; --j > -1;)q = h[j], k = -1 !== q.indexOf("%"), k !== (-1 !== i[j].indexOf("%")) && (l = 0 === j ? a.offsetWidth - L.width : a.offsetHeight - L.height, h[j] = k ? parseFloat(q) / 100 * l + "px" : 100 * (parseFloat(q) / l) + "%");
                    q = h.join(" ")
                }
                return this.parseComplex(a.style, q, r, f, g)
            }, formatter: bb
        }), qb("backgroundSize", {defaultValue: "0 0", formatter: bb}), qb("perspective", {
            defaultValue: "0px",
            prefix: !0
        }), qb("perspectiveOrigin", {
            defaultValue: "50% 50%",
            prefix: !0
        }), qb("transformStyle", {prefix: !0}), qb("backfaceVisibility", {prefix: !0}), qb("userSelect", {prefix: !0}), qb("margin", {parser: kb("marginTop,marginRight,marginBottom,marginLeft")}), qb("padding", {parser: kb("paddingTop,paddingRight,paddingBottom,paddingLeft")}), qb("clip", {
            defaultValue: "rect(0px,0px,0px,0px)",
            parser: function (a, b, c, d, f, g) {
                var h, i, j;
                return 9 > o ? (i = a.currentStyle, j = 8 > o ? " " : ",", h = "rect(" + i.clipTop + j + i.clipRight + j + i.clipBottom + j + i.clipLeft + ")", b = this.format(b).split(",").join(j)) : (h = this.format(V(a, this.p, e, !1, this.dflt)), b = this.format(b)), this.parseComplex(a.style, h, b, f, g)
            }
        }), qb("textShadow", {
            defaultValue: "0px 0px 0px #999",
            color: !0,
            multi: !0
        }), qb("autoRound,strictUnits", {
            parser: function (a, b, c, d, e) {
                return e
            }
        }), qb("border", {
            defaultValue: "0px solid #000", parser: function (a, b, c, d, f, g) {
                return this.parseComplex(a.style, this.format(V(a, "borderTopWidth", e, !1, "0px") + " " + V(a, "borderTopStyle", e, !1, "solid") + " " + V(a, "borderTopColor", e, !1, "#000")), this.format(b), f, g)
            }, color: !0, formatter: function (a) {
                var b = a.split(" ");
                return b[0] + " " + (b[1] || "solid") + " " + (a.match(ib) || ["#000"])[0]
            }
        }), qb("borderWidth", {parser: kb("borderTopWidth,borderRightWidth,borderBottomWidth,borderLeftWidth")}), qb("float,cssFloat,styleFloat", {
            parser: function (a, b, c, d, e) {
                var f = a.style, g = "cssFloat"in f ? "cssFloat" : "styleFloat";
                return new mb(f, g, 0, 0, e, -1, c, !1, 0, f[g], b)
            }
        });
        var Cb = function (a) {
            var b, c = this.t, d = c.filter || V(this.data, "filter"), e = 0 | this.s + this.c * a;
            100 === e && (-1 === d.indexOf("atrix(") && -1 === d.indexOf("radient(") && -1 === d.indexOf("oader(") ? (c.removeAttribute("filter"), b = !V(this.data, "filter")) : (c.filter = d.replace(w, ""), b = !0)), b || (this.xn1 && (c.filter = d = d || "alpha(opacity=" + e + ")"), -1 === d.indexOf("pacity") ? 0 === e && this.xn1 || (c.filter = d + " alpha(opacity=" + e + ")") : c.filter = d.replace(u, "opacity=" + e))
        };
        qb("opacity,alpha,autoAlpha", {
            defaultValue: "1", parser: function (a, b, c, d, f, g) {
                var h = parseFloat(V(a, "opacity", e, !1, "1")), i = a.style, j = "autoAlpha" === c;
                return "string" == typeof b && "=" === b.charAt(1) && (b = ("-" === b.charAt(0) ? -1 : 1) * parseFloat(b.substr(2)) + h), j && 1 === h && "hidden" === V(a, "visibility", e) && 0 !== b && (h = 0), O ? f = new mb(i, "opacity", h, b - h, f) : (f = new mb(i, "opacity", 100 * h, 100 * (b - h), f), f.xn1 = j ? 1 : 0, i.zoom = 1, f.type = 2, f.b = "alpha(opacity=" + f.s + ")", f.e = "alpha(opacity=" + (f.s + f.c) + ")", f.data = a, f.plugin = g, f.setRatio = Cb), j && (f = new mb(i, "visibility", 0, 0, f, -1, null, !1, 0, 0 !== h ? "inherit" : "hidden", 0 === b ? "hidden" : "inherit"), f.xs0 = "inherit", d._overwriteProps.push(f.n), d._overwriteProps.push(c)), f
            }
        });
        var Db = function (a, b) {
            b && (a.removeProperty ? ("ms" === b.substr(0, 2) && (b = "M" + b.substr(1)), a.removeProperty(b.replace(y, "-$1").toLowerCase())) : a.removeAttribute(b))
        }, Eb = function (a) {
            if (this.t._gsClassPT = this, 1 === a || 0 === a) {
                this.t.setAttribute("class", 0 === a ? this.b : this.e);
                for (var b = this.data, c = this.t.style; b;)b.v ? c[b.p] = b.v : Db(c, b.p), b = b._next;
                1 === a && this.t._gsClassPT === this && (this.t._gsClassPT = null)
            } else this.t.getAttribute("class") !== this.e && this.t.setAttribute("class", this.e)
        };
        qb("className", {
            parser: function (a, b, d, f, g, h, i) {
                var j, k, l, m, n, o = a.getAttribute("class") || "", p = a.style.cssText;
                if (g = f._classNamePT = new mb(a, d, 0, 0, g, 2), g.setRatio = Eb, g.pr = -11, c = !0, g.b = o, k = Y(a, e), l = a._gsClassPT) {
                    for (m = {}, n = l.data; n;)m[n.p] = 1, n = n._next;
                    l.setRatio(1)
                }
                return a._gsClassPT = g, g.e = "=" !== b.charAt(1) ? b : o.replace(RegExp("\\s*\\b" + b.substr(2) + "\\b"), "") + ("+" === b.charAt(0) ? " " + b.substr(2) : ""), f._tween._duration && (a.setAttribute("class", g.e), j = Z(a, k, Y(a), i, m), a.setAttribute("class", o), g.data = j.firstMPT, a.style.cssText = p, g = g.xfirst = f.parse(a, j.difs, g, h)), g
            }
        });
        var Fb = function (a) {
            if ((1 === a || 0 === a) && this.data._totalTime === this.data._totalDuration && "isFromStart" !== this.data.data) {
                var b, c, d, e, f = this.t.style, g = h.transform.parse;
                if ("all" === this.e)f.cssText = "", e = !0; else for (b = this.e.split(","), d = b.length; --d > -1;)c = b[d], h[c] && (h[c].parse === g ? e = !0 : c = "transformOrigin" === c ? vb : h[c].p), Db(f, c);
                e && (Db(f, tb), this.t._gsTransform && delete this.t._gsTransform)
            }
        };
        for (qb("clearProps", {
            parser: function (a, b, d, e, f) {
                return f = new mb(a, d, 0, 0, f, 2), f.setRatio = Fb, f.e = b, f.pr = -10, f.data = e._tween, c = !0, f
            }
        }), i = "bezier,throwProps,physicsProps,physics2D".split(","), ob = i.length; ob--;)rb(i[ob]);
        i = g.prototype, i._firstPT = null, i._onInitTween = function (a, b, h) {
            if (!a.nodeType)return !1;
            this._target = a, this._tween = h, this._vars = b, j = b.autoRound, c = !1, d = b.suffixMap || g.suffixMap, e = U(a, ""), f = this._overwriteProps;
            var i, m, o, p, q, r, s, t, u, w = a.style;
            if (k && "" === w.zIndex && (i = V(a, "zIndex", e), ("auto" === i || "" === i) && this._addLazySet(w, "zIndex", 0)), "string" == typeof b && (p = w.cssText, i = Y(a, e), w.cssText = p + ";" + b, i = Z(a, i, Y(a)).difs, !O && v.test(b) && (i.opacity = parseFloat(RegExp.$1)), b = i, w.cssText = p), this._firstPT = m = this.parse(a, b, null), this._transformType) {
                for (u = 3 === this._transformType, tb ? l && (k = !0, "" === w.zIndex && (s = V(a, "zIndex", e), ("auto" === s || "" === s) && this._addLazySet(w, "zIndex", 0)), n && this._addLazySet(w, "WebkitBackfaceVisibility", this._vars.WebkitBackfaceVisibility || (u ? "visible" : "hidden"))) : w.zoom = 1, o = m; o && o._next;)o = o._next;
                t = new mb(a, "transform", 0, 0, null, 2), this._linkCSSP(t, null, o), t.setRatio = u && wb ? Ab : tb ? Bb : zb, t.data = this._transform || yb(a, e, !0), f.pop()
            }
            if (c) {
                for (; m;) {
                    for (r = m._next, o = p; o && o.pr > m.pr;)o = o._next;
                    (m._prev = o ? o._prev : q) ? m._prev._next = m : p = m, (m._next = o) ? o._prev = m : q = m, m = r
                }
                this._firstPT = p
            }
            return !0
        }, i.parse = function (a, b, c, f) {
            var g, i, k, l, m, n, o, p, q, r, s = a.style;
            for (g in b)n = b[g], i = h[g], i ? c = i.parse(a, n, g, this, c, f, b) : (m = V(a, g, e) + "", q = "string" == typeof n, "color" === g || "fill" === g || "stroke" === g || -1 !== g.indexOf("Color") || q && x.test(n) ? (q || (n = hb(n), n = (n.length > 3 ? "rgba(" : "rgb(") + n.join(",") + ")"), c = nb(s, g, m, n, !0, "transparent", c, 0, f)) : !q || -1 === n.indexOf(" ") && -1 === n.indexOf(",") ? (k = parseFloat(m), o = k || 0 === k ? m.substr((k + "").length) : "", ("" === m || "auto" === m) && ("width" === g || "height" === g ? (k = ab(a, g, e), o = "px") : "left" === g || "top" === g ? (k = X(a, g, e), o = "px") : (k = "opacity" !== g ? 0 : 1, o = "")), r = q && "=" === n.charAt(1), r ? (l = parseInt(n.charAt(0) + "1", 10), n = n.substr(2), l *= parseFloat(n), p = n.replace(t, "")) : (l = parseFloat(n), p = q ? n.substr((l + "").length) || "" : ""), "" === p && (p = g in d ? d[g] : o), n = l || 0 === l ? (r ? l + k : l) + p : b[g], o !== p && "" !== p && (l || 0 === l) && k && (k = W(a, g, k, o), "%" === p ? (k /= W(a, g, 100, "%") / 100, b.strictUnits !== !0 && (m = k + "%")) : "em" === p ? k /= W(a, g, 1, "em") : "px" !== p && (l = W(a, g, l, p), p = "px"), r && (l || 0 === l) && (n = l + k + p)), r && (l += k), !k && 0 !== k || !l && 0 !== l ? void 0 !== s[g] && (n || "NaN" != n + "" && null != n) ? (c = new mb(s, g, l || k || 0, 0, c, -1, g, !1, 0, m, n), c.xs0 = "none" !== n || "display" !== g && -1 === g.indexOf("Style") ? n : m) : Q("invalid " + g + " tween value: " + b[g]) : (c = new mb(s, g, k, l - k, c, 0, g, j !== !1 && ("px" === p || "zIndex" === g), 0, m, n), c.xs0 = p)) : c = nb(s, g, m, n, !0, null, c, 0, f)), f && c && !c.plugin && (c.plugin = f);
            return c
        }, i.setRatio = function (a) {
            var b, c, d, e = this._firstPT, f = 1e-6;
            if (1 !== a || this._tween._time !== this._tween._duration && 0 !== this._tween._time)if (a || this._tween._time !== this._tween._duration && 0 !== this._tween._time || this._tween._rawPrevTime === -1e-6)for (; e;) {
                if (b = e.c * a + e.s, e.r ? b = Math.round(b) : f > b && b > -f && (b = 0), e.type)if (1 === e.type)if (d = e.l, 2 === d)e.t[e.p] = e.xs0 + b + e.xs1 + e.xn1 + e.xs2; else if (3 === d)e.t[e.p] = e.xs0 + b + e.xs1 + e.xn1 + e.xs2 + e.xn2 + e.xs3; else if (4 === d)e.t[e.p] = e.xs0 + b + e.xs1 + e.xn1 + e.xs2 + e.xn2 + e.xs3 + e.xn3 + e.xs4; else if (5 === d)e.t[e.p] = e.xs0 + b + e.xs1 + e.xn1 + e.xs2 + e.xn2 + e.xs3 + e.xn3 + e.xs4 + e.xn4 + e.xs5; else {
                    for (c = e.xs0 + b + e.xs1, d = 1; e.l > d; d++)c += e["xn" + d] + e["xs" + (d + 1)];
                    e.t[e.p] = c
                } else-1 === e.type ? e.t[e.p] = e.xs0 : e.setRatio && e.setRatio(a); else e.t[e.p] = b + e.xs0;
                e = e._next
            } else for (; e;)2 !== e.type ? e.t[e.p] = e.b : e.setRatio(a), e = e._next; else for (; e;)2 !== e.type ? e.t[e.p] = e.e : e.setRatio(a), e = e._next
        }, i._enableTransforms = function (a) {
            this._transformType = a || 3 === this._transformType ? 3 : 2, this._transform = this._transform || yb(this._target, e, !0)
        };
        var Gb = function () {
            this.t[this.p] = this.e, this.data._linkCSSP(this, this._next, null, !0)
        };
        i._addLazySet = function (a, b, c) {
            var d = this._firstPT = new mb(a, b, 0, 0, this._firstPT, 2);
            d.e = c, d.setRatio = Gb, d.data = this
        }, i._linkCSSP = function (a, b, c, d) {
            return a && (b && (b._prev = a), a._next && (a._next._prev = a._prev), a._prev ? a._prev._next = a._next : this._firstPT === a && (this._firstPT = a._next, d = !0), c ? c._next = a : d || null !== this._firstPT || (this._firstPT = a), a._next = b, a._prev = c), a
        }, i._kill = function (b) {
            var c, d, e, f = b;
            if (b.autoAlpha || b.alpha) {
                f = {};
                for (d in b)f[d] = b[d];
                f.opacity = 1, f.autoAlpha && (f.visibility = 1)
            }
            return b.className && (c = this._classNamePT) && (e = c.xfirst, e && e._prev ? this._linkCSSP(e._prev, c._next, e._prev._prev) : e === this._firstPT && (this._firstPT = c._next), c._next && this._linkCSSP(c._next, c._next._next, e._prev), this._classNamePT = null), a.prototype._kill.call(this, f)
        };
        var Hb = function (a, b, c) {
            var d, e, f, g;
            if (a.slice)for (e = a.length; --e > -1;)Hb(a[e], b, c); else for (d = a.childNodes, e = d.length; --e > -1;)f = d[e], g = f.type, f.style && (b.push(Y(f)), c && c.push(f)), 1 !== g && 9 !== g && 11 !== g || !f.childNodes.length || Hb(f, b, c)
        };
        return g.cascadeTo = function (a, c, d) {
            var e, f, g, h = b.to(a, c, d), i = [h], j = [], k = [], l = [], m = b._internals.reservedProps;
            for (a = h._targets || h.target, Hb(a, j, l), h.render(c, !0), Hb(a, k), h.render(0, !0), h._enabled(!0), e = l.length; --e > -1;)if (f = Z(l[e], j[e], k[e]), f.firstMPT) {
                f = f.difs;
                for (g in d)m[g] && (f[g] = d[g]);
                i.push(b.to(l[e], c, f))
            }
            return i
        }, a.activate([g]), g
    }, !0), function () {
        var a = window._gsDefine.plugin({
            propName: "roundProps", priority: -1, API: 2, init: function (a, b, c) {
                return this._tween = c, !0
            }
        }), b = a.prototype;
        b._onInitAllProps = function () {
            for (var a, b, c, d = this._tween, e = d.vars.roundProps instanceof Array ? d.vars.roundProps : d.vars.roundProps.split(","), f = e.length, g = {}, h = d._propLookup.roundProps; --f > -1;)g[e[f]] = 1;
            for (f = e.length; --f > -1;)for (a = e[f], b = d._firstPT; b;)c = b._next, b.pg ? b.t._roundProps(g, !0) : b.n === a && (this._add(b.t, a, b.s, b.c), c && (c._prev = b._prev), b._prev ? b._prev._next = c : d._firstPT === b && (d._firstPT = c), b._next = b._prev = null, d._propLookup[a] = h), b = c;
            return !1
        }, b._add = function (a, b, c, d) {
            this._addTween(a, b, c, c + d, b, !0), this._overwriteProps.push(b)
        }
    }(), window._gsDefine.plugin({
        propName: "attr", API: 2, version: "0.3.2", init: function (a, b) {
            var c, d, e;
            if ("function" != typeof a.setAttribute)return !1;
            this._target = a, this._proxy = {}, this._start = {}, this._end = {};
            for (c in b)this._start[c] = this._proxy[c] = d = a.getAttribute(c), e = this._addTween(this._proxy, c, parseFloat(d), b[c], c), this._end[c] = e ? e.s + e.c : b[c], this._overwriteProps.push(c);
            return !0
        }, set: function (a) {
            this._super.setRatio.call(this, a);
            for (var b, c = this._overwriteProps, d = c.length, e = 1 === a ? this._end : a ? this._proxy : this._start; --d > -1;)b = c[d], this._target.setAttribute(b, e[b] + "")
        }
    }), window._gsDefine.plugin({
        propName: "directionalRotation", API: 2, version: "0.2.0", init: function (a, b) {
            "object" != typeof b && (b = {rotation: b}), this.finals = {};
            var c, d, e, f, g, h, i = b.useRadians === !0 ? 2 * Math.PI : 360, j = 1e-6;
            for (c in b)"useRadians" !== c && (h = (b[c] + "").split("_"), d = h[0], e = parseFloat("function" != typeof a[c] ? a[c] : a[c.indexOf("set") || "function" != typeof a["get" + c.substr(3)] ? c : "get" + c.substr(3)]()), f = this.finals[c] = "string" == typeof d && "=" === d.charAt(1) ? e + parseInt(d.charAt(0) + "1", 10) * Number(d.substr(2)) : Number(d) || 0, g = f - e, h.length && (d = h.join("_"), -1 !== d.indexOf("short") && (g %= i, g !== g % (i / 2) && (g = 0 > g ? g + i : g - i)), -1 !== d.indexOf("_cw") && 0 > g ? g = (g + 9999999999 * i) % i - (0 | g / i) * i : -1 !== d.indexOf("ccw") && g > 0 && (g = (g - 9999999999 * i) % i - (0 | g / i) * i)), (g > j || -j > g) && (this._addTween(a, c, e, e + g, c), this._overwriteProps.push(c)));
            return !0
        }, set: function (a) {
            var b;
            if (1 !== a)this._super.setRatio.call(this, a); else for (b = this._firstPT; b;)b.f ? b.t[b.p](this.finals[b.p]) : b.t[b.p] = this.finals[b.p], b = b._next
        }
    })._autoCSS = !0, window._gsDefine("easing.Back", ["easing.Ease"], function (a) {
        var b, c, d, e = window.GreenSockGlobals || window, f = e.com.greensock, g = 2 * Math.PI, h = Math.PI / 2, i = f._class, j = function (b, c) {
            var d = i("easing." + b, function () {
            }, !0), e = d.prototype = new a;
            return e.constructor = d, e.getRatio = c, d
        }, k = a.register || function () {
            }, l = function (a, b, c, d) {
            var e = i("easing." + a, {easeOut: new b, easeIn: new c, easeInOut: new d}, !0);
            return k(e, a), e
        }, m = function (a, b, c) {
            this.t = a, this.v = b, c && (this.next = c, c.prev = this, this.c = c.v - b, this.gap = c.t - a)
        }, n = function (b, c) {
            var d = i("easing." + b, function (a) {
                this._p1 = a || 0 === a ? a : 1.70158, this._p2 = 1.525 * this._p1
            }, !0), e = d.prototype = new a;
            return e.constructor = d, e.getRatio = c, e.config = function (a) {
                return new d(a)
            }, d
        }, o = l("Back", n("BackOut", function (a) {
            return (a -= 1) * a * ((this._p1 + 1) * a + this._p1) + 1
        }), n("BackIn", function (a) {
            return a * a * ((this._p1 + 1) * a - this._p1)
        }), n("BackInOut", function (a) {
            return 1 > (a *= 2) ? .5 * a * a * ((this._p2 + 1) * a - this._p2) : .5 * ((a -= 2) * a * ((this._p2 + 1) * a + this._p2) + 2)
        })), p = i("easing.SlowMo", function (a, b, c) {
            b = b || 0 === b ? b : .7, null == a ? a = .7 : a > 1 && (a = 1), this._p = 1 !== a ? b : 0, this._p1 = (1 - a) / 2, this._p2 = a, this._p3 = this._p1 + this._p2, this._calcEnd = c === !0
        }, !0), q = p.prototype = new a;
        return q.constructor = p, q.getRatio = function (a) {
            var b = a + (.5 - a) * this._p;
            return this._p1 > a ? this._calcEnd ? 1 - (a = 1 - a / this._p1) * a : b - (a = 1 - a / this._p1) * a * a * a * b : a > this._p3 ? this._calcEnd ? 1 - (a = (a - this._p3) / this._p1) * a : b + (a - b) * (a = (a - this._p3) / this._p1) * a * a * a : this._calcEnd ? 1 : b
        }, p.ease = new p(.7, .7), q.config = p.config = function (a, b, c) {
            return new p(a, b, c)
        }, b = i("easing.SteppedEase", function (a) {
            a = a || 1, this._p1 = 1 / a, this._p2 = a + 1
        }, !0), q = b.prototype = new a, q.constructor = b, q.getRatio = function (a) {
            return 0 > a ? a = 0 : a >= 1 && (a = .999999999), (this._p2 * a >> 0) * this._p1
        }, q.config = b.config = function (a) {
            return new b(a)
        }, c = i("easing.RoughEase", function (b) {
            b = b || {};
            for (var c, d, e, f, g, h, i = b.taper || "none", j = [], k = 0, l = 0 | (b.points || 20), n = l, o = b.randomize !== !1, p = b.clamp === !0, q = b.template instanceof a ? b.template : null, r = "number" == typeof b.strength ? .4 * b.strength : .4; --n > -1;)c = o ? Math.random() : 1 / l * n, d = q ? q.getRatio(c) : c, "none" === i ? e = r : "out" === i ? (f = 1 - c, e = f * f * r) : "in" === i ? e = c * c * r : .5 > c ? (f = 2 * c, e = .5 * f * f * r) : (f = 2 * (1 - c), e = .5 * f * f * r), o ? d += Math.random() * e - .5 * e : n % 2 ? d += .5 * e : d -= .5 * e, p && (d > 1 ? d = 1 : 0 > d && (d = 0)), j[k++] = {
                x: c,
                y: d
            };
            for (j.sort(function (a, b) {
                return a.x - b.x
            }), h = new m(1, 1, null), n = l; --n > -1;)g = j[n], h = new m(g.x, g.y, h);
            this._prev = new m(0, 0, 0 !== h.t ? h : h.next)
        }, !0), q = c.prototype = new a, q.constructor = c, q.getRatio = function (a) {
            var b = this._prev;
            if (a > b.t) {
                for (; b.next && a >= b.t;)b = b.next;
                b = b.prev
            } else for (; b.prev && b.t >= a;)b = b.prev;
            return this._prev = b, b.v + (a - b.t) / b.gap * b.c
        }, q.config = function (a) {
            return new c(a)
        }, c.ease = new c, l("Bounce", j("BounceOut", function (a) {
            return 1 / 2.75 > a ? 7.5625 * a * a : 2 / 2.75 > a ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : 2.5 / 2.75 > a ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375
        }), j("BounceIn", function (a) {
            return 1 / 2.75 > (a = 1 - a) ? 1 - 7.5625 * a * a : 2 / 2.75 > a ? 1 - (7.5625 * (a -= 1.5 / 2.75) * a + .75) : 2.5 / 2.75 > a ? 1 - (7.5625 * (a -= 2.25 / 2.75) * a + .9375) : 1 - (7.5625 * (a -= 2.625 / 2.75) * a + .984375)
        }), j("BounceInOut", function (a) {
            var b = .5 > a;
            return a = b ? 1 - 2 * a : 2 * a - 1, a = 1 / 2.75 > a ? 7.5625 * a * a : 2 / 2.75 > a ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : 2.5 / 2.75 > a ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375, b ? .5 * (1 - a) : .5 * a + .5
        })), l("Circ", j("CircOut", function (a) {
            return Math.sqrt(1 - (a -= 1) * a)
        }), j("CircIn", function (a) {
            return -(Math.sqrt(1 - a * a) - 1)
        }), j("CircInOut", function (a) {
            return 1 > (a *= 2) ? -.5 * (Math.sqrt(1 - a * a) - 1) : .5 * (Math.sqrt(1 - (a -= 2) * a) + 1)
        })), d = function (b, c, d) {
            var e = i("easing." + b, function (a, b) {
                this._p1 = a || 1, this._p2 = b || d, this._p3 = this._p2 / g * (Math.asin(1 / this._p1) || 0)
            }, !0), f = e.prototype = new a;
            return f.constructor = e, f.getRatio = c, f.config = function (a, b) {
                return new e(a, b)
            }, e
        }, l("Elastic", d("ElasticOut", function (a) {
            return this._p1 * Math.pow(2, -10 * a) * Math.sin((a - this._p3) * g / this._p2) + 1
        }, .3), d("ElasticIn", function (a) {
            return -(this._p1 * Math.pow(2, 10 * (a -= 1)) * Math.sin((a - this._p3) * g / this._p2))
        }, .3), d("ElasticInOut", function (a) {
            return 1 > (a *= 2) ? -.5 * this._p1 * Math.pow(2, 10 * (a -= 1)) * Math.sin((a - this._p3) * g / this._p2) : .5 * this._p1 * Math.pow(2, -10 * (a -= 1)) * Math.sin((a - this._p3) * g / this._p2) + 1
        }, .45)), l("Expo", j("ExpoOut", function (a) {
            return 1 - Math.pow(2, -10 * a)
        }), j("ExpoIn", function (a) {
            return Math.pow(2, 10 * (a - 1)) - .001
        }), j("ExpoInOut", function (a) {
            return 1 > (a *= 2) ? .5 * Math.pow(2, 10 * (a - 1)) : .5 * (2 - Math.pow(2, -10 * (a - 1)))
        })), l("Sine", j("SineOut", function (a) {
            return Math.sin(a * h)
        }), j("SineIn", function (a) {
            return -Math.cos(a * h) + 1
        }), j("SineInOut", function (a) {
            return -.5 * (Math.cos(Math.PI * a) - 1)
        })), i("easing.EaseLookup", {
            find: function (b) {
                return a.map[b]
            }
        }, !0), k(e.SlowMo, "SlowMo", "ease,"), k(c, "RoughEase", "ease,"), k(b, "SteppedEase", "ease,"), o
    }, !0)
}), function (a) {
    "use strict";
    var b = a.GreenSockGlobals || a;
    if (!b.TweenLite) {
        var c, d, e, f, g, h = function (a) {
            var c, d = a.split("."), e = b;
            for (c = 0; d.length > c; c++)e[d[c]] = e = e[d[c]] || {};
            return e
        }, i = h("com.greensock"), j = 1e-10, k = [].slice, l = function () {
        }, m = function () {
            var a = Object.prototype.toString, b = a.call([]);
            return function (c) {
                return null != c && (c instanceof Array || "object" == typeof c && !!c.push && a.call(c) === b)
            }
        }(), n = {}, o = function (c, d, e, f) {
            this.sc = n[c] ? n[c].sc : [], n[c] = this, this.gsClass = null, this.func = e;
            var g = [];
            this.check = function (i) {
                for (var j, k, l, m, p = d.length, q = p; --p > -1;)(j = n[d[p]] || new o(d[p], [])).gsClass ? (g[p] = j.gsClass, q--) : i && j.sc.push(this);
                if (0 === q && e)for (k = ("com.greensock." + c).split("."), l = k.pop(), m = h(k.join("."))[l] = this.gsClass = e.apply(e, g), f && (b[l] = m, "function" == typeof define && define.amd ? define((a.GreenSockAMDPath ? a.GreenSockAMDPath + "/" : "") + c.split(".").join("/"), [], function () {
                    return m
                }) : "undefined" != typeof module && module.exports && (module.exports = m)), p = 0; this.sc.length > p; p++)this.sc[p].check()
            }, this.check(!0)
        }, p = a._gsDefine = function (a, b, c, d) {
            return new o(a, b, c, d)
        }, q = i._class = function (a, b, c) {
            return b = b || function () {
            }, p(a, [], function () {
                return b
            }, c), b
        };
        p.globals = b;
        var r = [0, 0, 1, 1], s = [], t = q("easing.Ease", function (a, b, c, d) {
            this._func = a, this._type = c || 0, this._power = d || 0, this._params = b ? r.concat(b) : r
        }, !0), u = t.map = {}, v = t.register = function (a, b, c, d) {
            for (var e, f, g, h, j = b.split(","), k = j.length, l = (c || "easeIn,easeOut,easeInOut").split(","); --k > -1;)for (f = j[k], e = d ? q("easing." + f, null, !0) : i.easing[f] || {}, g = l.length; --g > -1;)h = l[g], u[f + "." + h] = u[h + f] = e[h] = a.getRatio ? a : a[h] || new a
        };
        for (e = t.prototype, e._calcEnd = !1, e.getRatio = function (a) {
            if (this._func)return this._params[0] = a, this._func.apply(null, this._params);
            var b = this._type, c = this._power, d = 1 === b ? 1 - a : 2 === b ? a : .5 > a ? 2 * a : 2 * (1 - a);
            return 1 === c ? d *= d : 2 === c ? d *= d * d : 3 === c ? d *= d * d * d : 4 === c && (d *= d * d * d * d), 1 === b ? 1 - d : 2 === b ? d : .5 > a ? d / 2 : 1 - d / 2
        }, c = ["Linear", "Quad", "Cubic", "Quart", "Quint,Strong"], d = c.length; --d > -1;)e = c[d] + ",Power" + d, v(new t(null, null, 1, d), e, "easeOut", !0), v(new t(null, null, 2, d), e, "easeIn" + (0 === d ? ",easeNone" : "")), v(new t(null, null, 3, d), e, "easeInOut");
        u.linear = i.easing.Linear.easeIn, u.swing = i.easing.Quad.easeInOut;
        var w = q("events.EventDispatcher", function (a) {
            this._listeners = {}, this._eventTarget = a || this
        });
        e = w.prototype, e.addEventListener = function (a, b, c, d, e) {
            e = e || 0;
            var h, i, j = this._listeners[a], k = 0;
            for (null == j && (this._listeners[a] = j = []), i = j.length; --i > -1;)h = j[i], h.c === b && h.s === c ? j.splice(i, 1) : 0 === k && e > h.pr && (k = i + 1);
            j.splice(k, 0, {c: b, s: c, up: d, pr: e}), this !== f || g || f.wake()
        }, e.removeEventListener = function (a, b) {
            var c, d = this._listeners[a];
            if (d)for (c = d.length; --c > -1;)if (d[c].c === b)return void d.splice(c, 1)
        }, e.dispatchEvent = function (a) {
            var b, c, d, e = this._listeners[a];
            if (e)for (b = e.length, c = this._eventTarget; --b > -1;)d = e[b], d.up ? d.c.call(d.s || c, {
                type: a,
                target: c
            }) : d.c.call(d.s || c)
        };
        var x = a.requestAnimationFrame, y = a.cancelAnimationFrame, z = Date.now || function () {
                return (new Date).getTime()
            }, A = z();
        for (c = ["ms", "moz", "webkit", "o"], d = c.length; --d > -1 && !x;)x = a[c[d] + "RequestAnimationFrame"], y = a[c[d] + "CancelAnimationFrame"] || a[c[d] + "CancelRequestAnimationFrame"];
        q("Ticker", function (a, b) {
            var c, d, e, h, i, k = this, m = z(), n = b !== !1 && x, o = 500, p = 33, q = function (a) {
                var b, f, g = z() - A;
                g > o && (m += g - p), A += g, k.time = (A - m) / 1e3, b = k.time - i, (!c || b > 0 || a === !0) && (k.frame++, i += b + (b >= h ? .004 : h - b), f = !0), a !== !0 && (e = d(q)), f && k.dispatchEvent("tick")
            };
            w.call(k), k.time = k.frame = 0, k.tick = function () {
                q(!0)
            }, k.lagSmoothing = function (a, b) {
                o = a || 1 / j, p = Math.min(b, o, 0)
            }, k.sleep = function () {
                null != e && (n && y ? y(e) : clearTimeout(e), d = l, e = null, k === f && (g = !1))
            }, k.wake = function () {
                null !== e ? k.sleep() : k.frame > 10 && (A = z() - o + 5), d = 0 === c ? l : n && x ? x : function (a) {
                    return setTimeout(a, 0 | 1e3 * (i - k.time) + 1)
                }, k === f && (g = !0), q(2)
            }, k.fps = function (a) {
                return arguments.length ? (c = a, h = 1 / (c || 60), i = this.time + h, void k.wake()) : c
            }, k.useRAF = function (a) {
                return arguments.length ? (k.sleep(), n = a, void k.fps(c)) : n
            }, k.fps(a), setTimeout(function () {
                n && (!e || 5 > k.frame) && k.useRAF(!1)
            }, 1500)
        }), e = i.Ticker.prototype = new i.events.EventDispatcher, e.constructor = i.Ticker;
        var B = q("core.Animation", function (a, b) {
            if (this.vars = b = b || {}, this._duration = this._totalDuration = a || 0, this._delay = Number(b.delay) || 0, this._timeScale = 1, this._active = b.immediateRender === !0, this.data = b.data, this._reversed = b.reversed === !0, Q) {
                g || f.wake();
                var c = this.vars.useFrames ? P : Q;
                c.add(this, c._time), this.vars.paused && this.paused(!0)
            }
        });
        f = B.ticker = new i.Ticker, e = B.prototype, e._dirty = e._gc = e._initted = e._paused = !1, e._totalTime = e._time = 0, e._rawPrevTime = -1, e._next = e._last = e._onUpdate = e._timeline = e.timeline = null, e._paused = !1;
        var C = function () {
            g && z() - A > 2e3 && f.wake(), setTimeout(C, 2e3)
        };
        C(), e.play = function (a, b) {
            return null != a && this.seek(a, b), this.reversed(!1).paused(!1)
        }, e.pause = function (a, b) {
            return null != a && this.seek(a, b), this.paused(!0)
        }, e.resume = function (a, b) {
            return null != a && this.seek(a, b), this.paused(!1)
        }, e.seek = function (a, b) {
            return this.totalTime(Number(a), b !== !1)
        }, e.restart = function (a, b) {
            return this.reversed(!1).paused(!1).totalTime(a ? -this._delay : 0, b !== !1, !0)
        }, e.reverse = function (a, b) {
            return null != a && this.seek(a || this.totalDuration(), b), this.reversed(!0).paused(!1)
        }, e.render = function () {
        }, e.invalidate = function () {
            return this
        }, e.isActive = function () {
            var a, b = this._timeline, c = this._startTime;
            return !b || !this._gc && !this._paused && b.isActive() && (a = b.rawTime()) >= c && c + this.totalDuration() / this._timeScale > a
        }, e._enabled = function (a, b) {
            return g || f.wake(), this._gc = !a, this._active = this.isActive(), b !== !0 && (a && !this.timeline ? this._timeline.add(this, this._startTime - this._delay) : !a && this.timeline && this._timeline._remove(this, !0)), !1
        }, e._kill = function () {
            return this._enabled(!1, !1)
        }, e.kill = function (a, b) {
            return this._kill(a, b), this
        }, e._uncache = function (a) {
            for (var b = a ? this : this.timeline; b;)b._dirty = !0, b = b.timeline;
            return this
        }, e._swapSelfInParams = function (a) {
            for (var b = a.length, c = a.concat(); --b > -1;)"{self}" === a[b] && (c[b] = this);
            return c
        }, e.eventCallback = function (a, b, c, d) {
            if ("on" === (a || "").substr(0, 2)) {
                var e = this.vars;
                if (1 === arguments.length)return e[a];
                null == b ? delete e[a] : (e[a] = b, e[a + "Params"] = m(c) && -1 !== c.join("").indexOf("{self}") ? this._swapSelfInParams(c) : c, e[a + "Scope"] = d), "onUpdate" === a && (this._onUpdate = b)
            }
            return this
        }, e.delay = function (a) {
            return arguments.length ? (this._timeline.smoothChildTiming && this.startTime(this._startTime + a - this._delay), this._delay = a, this) : this._delay
        }, e.duration = function (a) {
            return arguments.length ? (this._duration = this._totalDuration = a, this._uncache(!0), this._timeline.smoothChildTiming && this._time > 0 && this._time < this._duration && 0 !== a && this.totalTime(this._totalTime * (a / this._duration), !0), this) : (this._dirty = !1, this._duration)
        }, e.totalDuration = function (a) {
            return this._dirty = !1, arguments.length ? this.duration(a) : this._totalDuration
        }, e.time = function (a, b) {
            return arguments.length ? (this._dirty && this.totalDuration(), this.totalTime(a > this._duration ? this._duration : a, b)) : this._time
        }, e.totalTime = function (a, b, c) {
            if (g || f.wake(), !arguments.length)return this._totalTime;
            if (this._timeline) {
                if (0 > a && !c && (a += this.totalDuration()), this._timeline.smoothChildTiming) {
                    this._dirty && this.totalDuration();
                    var d = this._totalDuration, e = this._timeline;
                    if (a > d && !c && (a = d), this._startTime = (this._paused ? this._pauseTime : e._time) - (this._reversed ? d - a : a) / this._timeScale, e._dirty || this._uncache(!1), e._timeline)for (; e._timeline;)e._timeline._time !== (e._startTime + e._totalTime) / e._timeScale && e.totalTime(e._totalTime, !0), e = e._timeline
                }
                this._gc && this._enabled(!0, !1), (this._totalTime !== a || 0 === this._duration) && (this.render(a, b, !1), H.length && R())
            }
            return this
        }, e.progress = e.totalProgress = function (a, b) {
            return arguments.length ? this.totalTime(this.duration() * a, b) : this._time / this.duration()
        }, e.startTime = function (a) {
            return arguments.length ? (a !== this._startTime && (this._startTime = a, this.timeline && this.timeline._sortChildren && this.timeline.add(this, a - this._delay)), this) : this._startTime
        }, e.timeScale = function (a) {
            if (!arguments.length)return this._timeScale;
            if (a = a || j, this._timeline && this._timeline.smoothChildTiming) {
                var b = this._pauseTime, c = b || 0 === b ? b : this._timeline.totalTime();
                this._startTime = c - (c - this._startTime) * this._timeScale / a
            }
            return this._timeScale = a, this._uncache(!1)
        }, e.reversed = function (a) {
            return arguments.length ? (a != this._reversed && (this._reversed = a, this.totalTime(this._timeline && !this._timeline.smoothChildTiming ? this.totalDuration() - this._totalTime : this._totalTime, !0)), this) : this._reversed
        }, e.paused = function (a) {
            if (!arguments.length)return this._paused;
            if (a != this._paused && this._timeline) {
                g || a || f.wake();
                var b = this._timeline, c = b.rawTime(), d = c - this._pauseTime;
                !a && b.smoothChildTiming && (this._startTime += d, this._uncache(!1)), this._pauseTime = a ? c : null, this._paused = a, this._active = this.isActive(), !a && 0 !== d && this._initted && this.duration() && this.render(b.smoothChildTiming ? this._totalTime : (c - this._startTime) / this._timeScale, !0, !0)
            }
            return this._gc && !a && this._enabled(!0, !1), this
        };
        var D = q("core.SimpleTimeline", function (a) {
            B.call(this, 0, a), this.autoRemoveChildren = this.smoothChildTiming = !0
        });
        e = D.prototype = new B, e.constructor = D, e.kill()._gc = !1, e._first = e._last = null, e._sortChildren = !1, e.add = e.insert = function (a, b) {
            var c, d;
            if (a._startTime = Number(b || 0) + a._delay, a._paused && this !== a._timeline && (a._pauseTime = a._startTime + (this.rawTime() - a._startTime) / a._timeScale), a.timeline && a.timeline._remove(a, !0), a.timeline = a._timeline = this, a._gc && a._enabled(!0, !0), c = this._last, this._sortChildren)for (d = a._startTime; c && c._startTime > d;)c = c._prev;
            return c ? (a._next = c._next, c._next = a) : (a._next = this._first, this._first = a), a._next ? a._next._prev = a : this._last = a, a._prev = c, this._timeline && this._uncache(!0), this
        }, e._remove = function (a, b) {
            return a.timeline === this && (b || a._enabled(!1, !0), a.timeline = null, a._prev ? a._prev._next = a._next : this._first === a && (this._first = a._next), a._next ? a._next._prev = a._prev : this._last === a && (this._last = a._prev), this._timeline && this._uncache(!0)), this
        }, e.render = function (a, b, c) {
            var d, e = this._first;
            for (this._totalTime = this._time = this._rawPrevTime = a; e;)d = e._next, (e._active || a >= e._startTime && !e._paused) && (e._reversed ? e.render((e._dirty ? e.totalDuration() : e._totalDuration) - (a - e._startTime) * e._timeScale, b, c) : e.render((a - e._startTime) * e._timeScale, b, c)), e = d
        }, e.rawTime = function () {
            return g || f.wake(), this._totalTime
        };
        var E = q("TweenLite", function (b, c, d) {
            if (B.call(this, c, d), this.render = E.prototype.render, null == b)throw"Cannot tween a null target.";
            this.target = b = "string" != typeof b ? b : E.selector(b) || b;
            var e, f, g, h = b.jquery || b.length && b !== a && b[0] && (b[0] === a || b[0].nodeType && b[0].style && !b.nodeType), i = this.vars.overwrite;
            if (this._overwrite = i = null == i ? O[E.defaultOverwrite] : "number" == typeof i ? i >> 0 : O[i], (h || b instanceof Array || b.push && m(b)) && "number" != typeof b[0])for (this._targets = g = k.call(b, 0), this._propLookup = [], this._siblings = [], e = 0; g.length > e; e++)f = g[e], f ? "string" != typeof f ? f.length && f !== a && f[0] && (f[0] === a || f[0].nodeType && f[0].style && !f.nodeType) ? (g.splice(e--, 1), this._targets = g = g.concat(k.call(f, 0))) : (this._siblings[e] = S(f, this, !1), 1 === i && this._siblings[e].length > 1 && T(f, this, null, 1, this._siblings[e])) : (f = g[e--] = E.selector(f), "string" == typeof f && g.splice(e + 1, 1)) : g.splice(e--, 1); else this._propLookup = {}, this._siblings = S(b, this, !1), 1 === i && this._siblings.length > 1 && T(b, this, null, 1, this._siblings);
            (this.vars.immediateRender || 0 === c && 0 === this._delay && this.vars.immediateRender !== !1) && (this._time = -j, this.render(-this._delay))
        }, !0), F = function (b) {
            return b.length && b !== a && b[0] && (b[0] === a || b[0].nodeType && b[0].style && !b.nodeType)
        }, G = function (a, b) {
            var c, d = {};
            for (c in a)N[c] || c in b && "transform" !== c && "x" !== c && "y" !== c && "width" !== c && "height" !== c && "className" !== c && "border" !== c || !(!K[c] || K[c] && K[c]._autoCSS) || (d[c] = a[c], delete a[c]);
            a.css = d
        };
        e = E.prototype = new B, e.constructor = E, e.kill()._gc = !1, e.ratio = 0, e._firstPT = e._targets = e._overwrittenProps = e._startAt = null, e._notifyPluginsOfEnabled = e._lazy = !1, E.version = "1.12.1", E.defaultEase = e._ease = new t(null, null, 1, 1), E.defaultOverwrite = "auto", E.ticker = f, E.autoSleep = !0, E.lagSmoothing = function (a, b) {
            f.lagSmoothing(a, b)
        }, E.selector = a.$ || a.jQuery || function (b) {
            return a.$ ? (E.selector = a.$, a.$(b)) : a.document ? a.document.getElementById("#" === b.charAt(0) ? b.substr(1) : b) : b
        };
        var H = [], I = {}, J = E._internals = {
            isArray: m,
            isSelector: F,
            lazyTweens: H
        }, K = E._plugins = {}, L = J.tweenLookup = {}, M = 0, N = J.reservedProps = {
            ease: 1,
            delay: 1,
            overwrite: 1,
            onComplete: 1,
            onCompleteParams: 1,
            onCompleteScope: 1,
            useFrames: 1,
            runBackwards: 1,
            startAt: 1,
            onUpdate: 1,
            onUpdateParams: 1,
            onUpdateScope: 1,
            onStart: 1,
            onStartParams: 1,
            onStartScope: 1,
            onReverseComplete: 1,
            onReverseCompleteParams: 1,
            onReverseCompleteScope: 1,
            onRepeat: 1,
            onRepeatParams: 1,
            onRepeatScope: 1,
            easeParams: 1,
            yoyo: 1,
            immediateRender: 1,
            repeat: 1,
            repeatDelay: 1,
            data: 1,
            paused: 1,
            reversed: 1,
            autoCSS: 1,
            lazy: 1
        }, O = {
            none: 0,
            all: 1,
            auto: 2,
            concurrent: 3,
            allOnStart: 4,
            preexisting: 5,
            "true": 1,
            "false": 0
        }, P = B._rootFramesTimeline = new D, Q = B._rootTimeline = new D, R = function () {
            var a = H.length;
            for (I = {}; --a > -1;)c = H[a], c && c._lazy !== !1 && (c.render(c._lazy, !1, !0), c._lazy = !1);
            H.length = 0
        };
        Q._startTime = f.time, P._startTime = f.frame, Q._active = P._active = !0, setTimeout(R, 1), B._updateRoot = E.render = function () {
            var a, b, c;
            if (H.length && R(), Q.render((f.time - Q._startTime) * Q._timeScale, !1, !1), P.render((f.frame - P._startTime) * P._timeScale, !1, !1), H.length && R(), !(f.frame % 120)) {
                for (c in L) {
                    for (b = L[c].tweens, a = b.length; --a > -1;)b[a]._gc && b.splice(a, 1);
                    0 === b.length && delete L[c]
                }
                if (c = Q._first, (!c || c._paused) && E.autoSleep && !P._first && 1 === f._listeners.tick.length) {
                    for (; c && c._paused;)c = c._next;
                    c || f.sleep()
                }
            }
        }, f.addEventListener("tick", B._updateRoot);
        var S = function (a, b, c) {
            var d, e, f = a._gsTweenID;
            if (L[f || (a._gsTweenID = f = "t" + M++)] || (L[f] = {
                    target: a,
                    tweens: []
                }), b && (d = L[f].tweens, d[e = d.length] = b, c))for (; --e > -1;)d[e] === b && d.splice(e, 1);
            return L[f].tweens
        }, T = function (a, b, c, d, e) {
            var f, g, h, i;
            if (1 === d || d >= 4) {
                for (i = e.length, f = 0; i > f; f++)if ((h = e[f]) !== b)h._gc || h._enabled(!1, !1) && (g = !0); else if (5 === d)break;
                return g
            }
            var k, l = b._startTime + j, m = [], n = 0, o = 0 === b._duration;
            for (f = e.length; --f > -1;)(h = e[f]) === b || h._gc || h._paused || (h._timeline !== b._timeline ? (k = k || U(b, 0, o), 0 === U(h, k, o) && (m[n++] = h)) : l >= h._startTime && h._startTime + h.totalDuration() / h._timeScale > l && ((o || !h._initted) && 2e-10 >= l - h._startTime || (m[n++] = h)));
            for (f = n; --f > -1;)h = m[f], 2 === d && h._kill(c, a) && (g = !0), (2 !== d || !h._firstPT && h._initted) && h._enabled(!1, !1) && (g = !0);
            return g
        }, U = function (a, b, c) {
            for (var d = a._timeline, e = d._timeScale, f = a._startTime; d._timeline;) {
                if (f += d._startTime, e *= d._timeScale, d._paused)return -100;
                d = d._timeline
            }
            return f /= e, f > b ? f - b : c && f === b || !a._initted && 2 * j > f - b ? j : (f += a.totalDuration() / a._timeScale / e) > b + j ? 0 : f - b - j
        };
        e._init = function () {
            var a, b, c, d, e, f = this.vars, g = this._overwrittenProps, h = this._duration, i = !!f.immediateRender, j = f.ease;
            if (f.startAt) {
                this._startAt && (this._startAt.render(-1, !0), this._startAt.kill()), e = {};
                for (d in f.startAt)e[d] = f.startAt[d];
                if (e.overwrite = !1, e.immediateRender = !0, e.lazy = i && f.lazy !== !1, e.startAt = e.delay = null, this._startAt = E.to(this.target, 0, e), i)if (this._time > 0)this._startAt = null; else if (0 !== h)return
            } else if (f.runBackwards && 0 !== h)if (this._startAt)this._startAt.render(-1, !0), this._startAt.kill(), this._startAt = null; else {
                c = {};
                for (d in f)N[d] && "autoCSS" !== d || (c[d] = f[d]);
                if (c.overwrite = 0, c.data = "isFromStart", c.lazy = i && f.lazy !== !1, c.immediateRender = i, this._startAt = E.to(this.target, 0, c), i) {
                    if (0 === this._time)return
                } else this._startAt._init(), this._startAt._enabled(!1)
            }
            if (this._ease = j ? j instanceof t ? f.easeParams instanceof Array ? j.config.apply(j, f.easeParams) : j : "function" == typeof j ? new t(j, f.easeParams) : u[j] || E.defaultEase : E.defaultEase, this._easeType = this._ease._type, this._easePower = this._ease._power, this._firstPT = null, this._targets)for (a = this._targets.length; --a > -1;)this._initProps(this._targets[a], this._propLookup[a] = {}, this._siblings[a], g ? g[a] : null) && (b = !0); else b = this._initProps(this.target, this._propLookup, this._siblings, g);
            if (b && E._onPluginEvent("_onInitAllProps", this), g && (this._firstPT || "function" != typeof this.target && this._enabled(!1, !1)), f.runBackwards)for (c = this._firstPT; c;)c.s += c.c, c.c = -c.c, c = c._next;
            this._onUpdate = f.onUpdate, this._initted = !0
        }, e._initProps = function (b, c, d, e) {
            var f, g, h, i, j, k;
            if (null == b)return !1;
            I[b._gsTweenID] && R(), this.vars.css || b.style && b !== a && b.nodeType && K.css && this.vars.autoCSS !== !1 && G(this.vars, b);
            for (f in this.vars) {
                if (k = this.vars[f], N[f])k && (k instanceof Array || k.push && m(k)) && -1 !== k.join("").indexOf("{self}") && (this.vars[f] = k = this._swapSelfInParams(k, this)); else if (K[f] && (i = new K[f])._onInitTween(b, this.vars[f], this)) {
                    for (this._firstPT = j = {
                        _next: this._firstPT,
                        t: i,
                        p: "setRatio",
                        s: 0,
                        c: 1,
                        f: !0,
                        n: f,
                        pg: !0,
                        pr: i._priority
                    }, g = i._overwriteProps.length; --g > -1;)c[i._overwriteProps[g]] = this._firstPT;
                    (i._priority || i._onInitAllProps) && (h = !0), (i._onDisable || i._onEnable) && (this._notifyPluginsOfEnabled = !0)
                } else this._firstPT = c[f] = j = {
                    _next: this._firstPT,
                    t: b,
                    p: f,
                    f: "function" == typeof b[f],
                    n: f,
                    pg: !1,
                    pr: 0
                }, j.s = j.f ? b[f.indexOf("set") || "function" != typeof b["get" + f.substr(3)] ? f : "get" + f.substr(3)]() : parseFloat(b[f]), j.c = "string" == typeof k && "=" === k.charAt(1) ? parseInt(k.charAt(0) + "1", 10) * Number(k.substr(2)) : Number(k) - j.s || 0;
                j && j._next && (j._next._prev = j)
            }
            return e && this._kill(e, b) ? this._initProps(b, c, d, e) : this._overwrite > 1 && this._firstPT && d.length > 1 && T(b, this, c, this._overwrite, d) ? (this._kill(c, b), this._initProps(b, c, d, e)) : (this._firstPT && (this.vars.lazy !== !1 && this._duration || this.vars.lazy && !this._duration) && (I[b._gsTweenID] = !0), h)
        }, e.render = function (a, b, c) {
            var d, e, f, g, h = this._time, i = this._duration, k = this._rawPrevTime;
            if (a >= i)this._totalTime = this._time = i, this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1, this._reversed || (d = !0, e = "onComplete"), 0 === i && (this._initted || !this.vars.lazy || c) && (this._startTime === this._timeline._duration && (a = 0), (0 === a || 0 > k || k === j) && k !== a && (c = !0, k > j && (e = "onReverseComplete")), this._rawPrevTime = g = !b || a || k === a ? a : j); else if (1e-7 > a)this._totalTime = this._time = 0, this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0, (0 !== h || 0 === i && k > 0 && k !== j) && (e = "onReverseComplete", d = this._reversed), 0 > a ? (this._active = !1, 0 === i && (this._initted || !this.vars.lazy || c) && (k >= 0 && (c = !0), this._rawPrevTime = g = !b || a || k === a ? a : j)) : this._initted || (c = !0); else if (this._totalTime = this._time = a, this._easeType) {
                var l = a / i, m = this._easeType, n = this._easePower;
                (1 === m || 3 === m && l >= .5) && (l = 1 - l), 3 === m && (l *= 2), 1 === n ? l *= l : 2 === n ? l *= l * l : 3 === n ? l *= l * l * l : 4 === n && (l *= l * l * l * l), this.ratio = 1 === m ? 1 - l : 2 === m ? l : .5 > a / i ? l / 2 : 1 - l / 2
            } else this.ratio = this._ease.getRatio(a / i);
            if (this._time !== h || c) {
                if (!this._initted) {
                    if (this._init(), !this._initted || this._gc)return;
                    if (!c && this._firstPT && (this.vars.lazy !== !1 && this._duration || this.vars.lazy && !this._duration))return this._time = this._totalTime = h, this._rawPrevTime = k, H.push(this), void(this._lazy = a);
                    this._time && !d ? this.ratio = this._ease.getRatio(this._time / i) : d && this._ease._calcEnd && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1))
                }
                for (this._lazy !== !1 && (this._lazy = !1), this._active || !this._paused && this._time !== h && a >= 0 && (this._active = !0), 0 === h && (this._startAt && (a >= 0 ? this._startAt.render(a, b, c) : e || (e = "_dummyGS")), this.vars.onStart && (0 !== this._time || 0 === i) && (b || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || s))), f = this._firstPT; f;)f.f ? f.t[f.p](f.c * this.ratio + f.s) : f.t[f.p] = f.c * this.ratio + f.s, f = f._next;
                this._onUpdate && (0 > a && this._startAt && this._startTime && this._startAt.render(a, b, c), b || (this._time !== h || d) && this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || s)), e && (this._gc || (0 > a && this._startAt && !this._onUpdate && this._startTime && this._startAt.render(a, b, c), d && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !b && this.vars[e] && this.vars[e].apply(this.vars[e + "Scope"] || this, this.vars[e + "Params"] || s), 0 === i && this._rawPrevTime === j && g !== j && (this._rawPrevTime = 0)))
            }
        }, e._kill = function (a, b) {
            if ("all" === a && (a = null), null == a && (null == b || b === this.target))return this._lazy = !1, this._enabled(!1, !1);
            b = "string" != typeof b ? b || this._targets || this.target : E.selector(b) || b;
            var c, d, e, f, g, h, i, j;
            if ((m(b) || F(b)) && "number" != typeof b[0])for (c = b.length; --c > -1;)this._kill(a, b[c]) && (h = !0); else {
                if (this._targets) {
                    for (c = this._targets.length; --c > -1;)if (b === this._targets[c]) {
                        g = this._propLookup[c] || {}, this._overwrittenProps = this._overwrittenProps || [], d = this._overwrittenProps[c] = a ? this._overwrittenProps[c] || {} : "all";
                        break
                    }
                } else {
                    if (b !== this.target)return !1;
                    g = this._propLookup, d = this._overwrittenProps = a ? this._overwrittenProps || {} : "all"
                }
                if (g) {
                    i = a || g, j = a !== d && "all" !== d && a !== g && ("object" != typeof a || !a._tempKill);
                    for (e in i)(f = g[e]) && (f.pg && f.t._kill(i) && (h = !0), f.pg && 0 !== f.t._overwriteProps.length || (f._prev ? f._prev._next = f._next : f === this._firstPT && (this._firstPT = f._next), f._next && (f._next._prev = f._prev), f._next = f._prev = null), delete g[e]), j && (d[e] = 1);
                    !this._firstPT && this._initted && this._enabled(!1, !1)
                }
            }
            return h
        }, e.invalidate = function () {
            return this._notifyPluginsOfEnabled && E._onPluginEvent("_onDisable", this), this._firstPT = null, this._overwrittenProps = null, this._onUpdate = null, this._startAt = null, this._initted = this._active = this._notifyPluginsOfEnabled = this._lazy = !1, this._propLookup = this._targets ? {} : [], this
        }, e._enabled = function (a, b) {
            if (g || f.wake(), a && this._gc) {
                var c, d = this._targets;
                if (d)for (c = d.length; --c > -1;)this._siblings[c] = S(d[c], this, !0); else this._siblings = S(this.target, this, !0)
            }
            return B.prototype._enabled.call(this, a, b), this._notifyPluginsOfEnabled && this._firstPT ? E._onPluginEvent(a ? "_onEnable" : "_onDisable", this) : !1
        }, E.to = function (a, b, c) {
            return new E(a, b, c)
        }, E.from = function (a, b, c) {
            return c.runBackwards = !0, c.immediateRender = 0 != c.immediateRender, new E(a, b, c)
        }, E.fromTo = function (a, b, c, d) {
            return d.startAt = c, d.immediateRender = 0 != d.immediateRender && 0 != c.immediateRender, new E(a, b, d)
        }, E.delayedCall = function (a, b, c, d, e) {
            return new E(b, 0, {
                delay: a,
                onComplete: b,
                onCompleteParams: c,
                onCompleteScope: d,
                onReverseComplete: b,
                onReverseCompleteParams: c,
                onReverseCompleteScope: d,
                immediateRender: !1,
                useFrames: e,
                overwrite: 0
            })
        }, E.set = function (a, b) {
            return new E(a, 0, b)
        }, E.getTweensOf = function (a, b) {
            if (null == a)return [];
            a = "string" != typeof a ? a : E.selector(a) || a;
            var c, d, e, f;
            if ((m(a) || F(a)) && "number" != typeof a[0]) {
                for (c = a.length, d = []; --c > -1;)d = d.concat(E.getTweensOf(a[c], b));
                for (c = d.length; --c > -1;)for (f = d[c], e = c; --e > -1;)f === d[e] && d.splice(c, 1)
            } else for (d = S(a).concat(), c = d.length; --c > -1;)(d[c]._gc || b && !d[c].isActive()) && d.splice(c, 1);
            return d
        }, E.killTweensOf = E.killDelayedCallsTo = function (a, b, c) {
            "object" == typeof b && (c = b, b = !1);
            for (var d = E.getTweensOf(a, b), e = d.length; --e > -1;)d[e]._kill(c, a)
        };
        var V = q("plugins.TweenPlugin", function (a, b) {
            this._overwriteProps = (a || "").split(","), this._propName = this._overwriteProps[0], this._priority = b || 0, this._super = V.prototype
        }, !0);
        if (e = V.prototype, V.version = "1.10.1", V.API = 2, e._firstPT = null, e._addTween = function (a, b, c, d, e, f) {
                var g, h;
                return null != d && (g = "number" == typeof d || "=" !== d.charAt(1) ? Number(d) - c : parseInt(d.charAt(0) + "1", 10) * Number(d.substr(2))) ? (this._firstPT = h = {
                    _next: this._firstPT,
                    t: a,
                    p: b,
                    s: c,
                    c: g,
                    f: "function" == typeof a[b],
                    n: e || b,
                    r: f
                }, h._next && (h._next._prev = h), h) : void 0
            }, e.setRatio = function (a) {
                for (var b, c = this._firstPT, d = 1e-6; c;)b = c.c * a + c.s, c.r ? b = Math.round(b) : d > b && b > -d && (b = 0), c.f ? c.t[c.p](b) : c.t[c.p] = b, c = c._next
            }, e._kill = function (a) {
                var b, c = this._overwriteProps, d = this._firstPT;
                if (null != a[this._propName])this._overwriteProps = []; else for (b = c.length; --b > -1;)null != a[c[b]] && c.splice(b, 1);
                for (; d;)null != a[d.n] && (d._next && (d._next._prev = d._prev), d._prev ? (d._prev._next = d._next, d._prev = null) : this._firstPT === d && (this._firstPT = d._next)), d = d._next;
                return !1
            }, e._roundProps = function (a, b) {
                for (var c = this._firstPT; c;)(a[this._propName] || null != c.n && a[c.n.split(this._propName + "_").join("")]) && (c.r = b), c = c._next
            }, E._onPluginEvent = function (a, b) {
                var c, d, e, f, g, h = b._firstPT;
                if ("_onInitAllProps" === a) {
                    for (; h;) {
                        for (g = h._next, d = e; d && d.pr > h.pr;)d = d._next;
                        (h._prev = d ? d._prev : f) ? h._prev._next = h : e = h, (h._next = d) ? d._prev = h : f = h, h = g
                    }
                    h = b._firstPT = e
                }
                for (; h;)h.pg && "function" == typeof h.t[a] && h.t[a]() && (c = !0), h = h._next;
                return c
            }, V.activate = function (a) {
                for (var b = a.length; --b > -1;)a[b].API === V.API && (K[(new a[b])._propName] = a[b]);
                return !0
            }, p.plugin = function (a) {
                if (!(a && a.propName && a.init && a.API))throw"illegal plugin definition.";
                var b, c = a.propName, d = a.priority || 0, e = a.overwriteProps, f = {
                    init: "_onInitTween",
                    set: "setRatio",
                    kill: "_kill",
                    round: "_roundProps",
                    initAll: "_onInitAllProps"
                }, g = q("plugins." + c.charAt(0).toUpperCase() + c.substr(1) + "Plugin", function () {
                    V.call(this, c, d), this._overwriteProps = e || []
                }, a.global === !0), h = g.prototype = new V(c);
                h.constructor = g, g.API = a.API;
                for (b in f)"function" == typeof a[b] && (h[f[b]] = a[b]);
                return g.version = a.version, V.activate([g]), g
            }, c = a._gsQueue) {
            for (d = 0; c.length > d; d++)c[d]();
            for (e in n)n[e].func || a.console.log("GSAP encountered missing dependency: com.greensock." + e)
        }
        g = !1
    }
}(window), (window._gsQueue || (window._gsQueue = [])).push(function () {
    "use strict";
    var a = document.documentElement, b = window, c = function (c, d) {
        var e = "x" === d ? "Width" : "Height", f = "scroll" + e, g = "client" + e, h = document.body;
        return c === b || c === a || c === h ? Math.max(a[f], h[f]) - (b["inner" + e] || Math.max(a[g], h[g])) : c[f] - c["offset" + e]
    }, d = window._gsDefine.plugin({
        propName: "scrollTo", API: 2, version: "1.7.3", init: function (a, d, e) {
            return this._wdw = a === b, this._target = a, this._tween = e, "object" != typeof d && (d = {y: d}), this._autoKill = d.autoKill !== !1, this.x = this.xPrev = this.getX(), this.y = this.yPrev = this.getY(), null != d.x ? (this._addTween(this, "x", this.x, "max" === d.x ? c(a, "x") : d.x, "scrollTo_x", !0), this._overwriteProps.push("scrollTo_x")) : this.skipX = !0, null != d.y ? (this._addTween(this, "y", this.y, "max" === d.y ? c(a, "y") : d.y, "scrollTo_y", !0), this._overwriteProps.push("scrollTo_y")) : this.skipY = !0, !0
        }, set: function (a) {
            this._super.setRatio.call(this, a);
            var d = this._wdw || !this.skipX ? this.getX() : this.xPrev, e = this._wdw || !this.skipY ? this.getY() : this.yPrev, f = e - this.yPrev, g = d - this.xPrev;
            this._autoKill && (!this.skipX && (g > 7 || -7 > g) && c(this._target, "x") > d && (this.skipX = !0), !this.skipY && (f > 7 || -7 > f) && c(this._target, "y") > e && (this.skipY = !0), this.skipX && this.skipY && this._tween.kill()), this._wdw ? b.scrollTo(this.skipX ? d : this.x, this.skipY ? e : this.y) : (this.skipY || (this._target.scrollTop = this.y), this.skipX || (this._target.scrollLeft = this.x)), this.xPrev = this.x, this.yPrev = this.y
        }
    }), e = d.prototype;
    d.max = c, e.getX = function () {
        return this._wdw ? null != b.pageXOffset ? b.pageXOffset : null != a.scrollLeft ? a.scrollLeft : document.body.scrollLeft : this._target.scrollLeft
    }, e.getY = function () {
        return this._wdw ? null != b.pageYOffset ? b.pageYOffset : null != a.scrollTop ? a.scrollTop : document.body.scrollTop : this._target.scrollTop
    }, e._kill = function (a) {
        return a.scrollTo_x && (this.skipX = !0), a.scrollTo_y && (this.skipY = !0), this._super._kill.call(this, a)
    }
}), window._gsDefine && window._gsQueue.pop()(), function (a, b, c) {
    function d(a) {
        var b = {}, d = /^jQuery\d+$/;
        return c.each(a.attributes, function (a, c) {
            c.specified && !d.test(c.name) && (b[c.name] = c.value)
        }), b
    }

    function e(a, b) {
        var d = this, e = c(d);
        if (d.value == e.attr("placeholder") && e.hasClass("placeholder"))if (e.data("placeholder-password")) {
            if (e = e.hide().next().show().attr("id", e.removeAttr("id").data("placeholder-id")), a === !0)return e[0].value = b;
            e.focus()
        } else d.value = "", e.removeClass("placeholder"), d == g() && d.select()
    }

    function f() {
        var a, b = this, f = c(b), g = this.id;
        if ("" == b.value) {
            if ("password" == b.type) {
                if (!f.data("placeholder-textinput")) {
                    try {
                        a = f.clone().attr({type: "text"})
                    } catch (h) {
                        a = c("<input>").attr(c.extend(d(this), {type: "text"}))
                    }
                    a.removeAttr("name").data({
                        "placeholder-password": f,
                        "placeholder-id": g
                    }).bind("focus.placeholder", e), f.data({"placeholder-textinput": a, "placeholder-id": g}).before(a)
                }
                f = f.removeAttr("id").hide().prev().attr("id", g).show()
            }
            f.addClass("placeholder"), f[0].value = f.attr("placeholder")
        } else f.removeClass("placeholder")
    }

    function g() {
        try {
            return b.activeElement
        } catch (a) {
        }
    }

    var h, i, j = "[object OperaMini]" == Object.prototype.toString.call(a.operamini), k = "placeholder"in b.createElement("input") && !j, l = "placeholder"in b.createElement("textarea") && !j, m = c.fn, n = c.valHooks, o = c.propHooks;
    k && l ? (i = m.placeholder = function () {
        return this
    }, i.input = i.textarea = !0) : (i = m.placeholder = function () {
        var a = this;
        return a.filter((k ? "textarea" : ":input") + "[placeholder]").not(".placeholder").bind({
            "focus.placeholder": e,
            "blur.placeholder": f
        }).data("placeholder-enabled", !0).trigger("blur.placeholder"), a
    }, i.input = k, i.textarea = l, h = {
        get: function (a) {
            var b = c(a), d = b.data("placeholder-password");
            return d ? d[0].value : b.data("placeholder-enabled") && b.hasClass("placeholder") ? "" : a.value
        }, set: function (a, b) {
            var d = c(a), h = d.data("placeholder-password");
            return h ? h[0].value = b : d.data("placeholder-enabled") ? ("" == b ? (a.value = b, a != g() && f.call(a)) : d.hasClass("placeholder") ? e.call(a, !0, b) || (a.value = b) : a.value = b, d) : a.value = b
        }
    }, k || (n.input = h, o.value = h), l || (n.textarea = h, o.value = h), c(function () {
        c(b).delegate("form", "submit.placeholder", function () {
            var a = c(".placeholder", this).each(e);
            setTimeout(function () {
                a.each(f)
            }, 10)
        })
    }), c(a).bind("beforeunload.placeholder", function () {
        c(".placeholder").each(function () {
            this.value = ""
        })
    }))
}(this, document, jQuery), function (a) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : "object" == typeof exports ? module.exports = a : a(jQuery)
}(function (a) {
    function b(b) {
        var g = b || window.event, h = i.call(arguments, 1), j = 0, l = 0, m = 0, n = 0, o = 0, p = 0;
        if (b = a.event.fix(g), b.type = "mousewheel", "detail"in g && (m = -1 * g.detail), "wheelDelta"in g && (m = g.wheelDelta), "wheelDeltaY"in g && (m = g.wheelDeltaY), "wheelDeltaX"in g && (l = -1 * g.wheelDeltaX), "axis"in g && g.axis === g.HORIZONTAL_AXIS && (l = -1 * m, m = 0), j = 0 === m ? l : m, "deltaY"in g && (m = -1 * g.deltaY, j = m), "deltaX"in g && (l = g.deltaX, 0 === m && (j = -1 * l)), 0 !== m || 0 !== l) {
            if (1 === g.deltaMode) {
                var q = a.data(this, "mousewheel-line-height");
                j *= q, m *= q, l *= q
            } else if (2 === g.deltaMode) {
                var r = a.data(this, "mousewheel-page-height");
                j *= r, m *= r, l *= r
            }
            if (n = Math.max(Math.abs(m), Math.abs(l)), (!f || f > n) && (f = n, d(g, n) && (f /= 40)), d(g, n) && (j /= 40, l /= 40, m /= 40), j = Math[j >= 1 ? "floor" : "ceil"](j / f), l = Math[l >= 1 ? "floor" : "ceil"](l / f), m = Math[m >= 1 ? "floor" : "ceil"](m / f), k.settings.normalizeOffset && this.getBoundingClientRect) {
                var s = this.getBoundingClientRect();
                o = b.clientX - s.left, p = b.clientY - s.top
            }
            return b.deltaX = l, b.deltaY = m, b.deltaFactor = f, b.offsetX = o, b.offsetY = p, b.deltaMode = 0, h.unshift(b, j, l, m), e && clearTimeout(e), e = setTimeout(c, 200), (a.event.dispatch || a.event.handle).apply(this, h)
        }
    }

    function c() {
        f = null
    }

    function d(a, b) {
        return k.settings.adjustOldDeltas && "mousewheel" === a.type && b % 120 === 0
    }

    var e, f, g = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"], h = "onwheel"in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"], i = Array.prototype.slice;
    if (a.event.fixHooks)for (var j = g.length; j;)a.event.fixHooks[g[--j]] = a.event.mouseHooks;
    var k = a.event.special.mousewheel = {
        version: "3.1.12", setup: function () {
            if (this.addEventListener)for (var c = h.length; c;)this.addEventListener(h[--c], b, !1); else this.onmousewheel = b;
            a.data(this, "mousewheel-line-height", k.getLineHeight(this)), a.data(this, "mousewheel-page-height", k.getPageHeight(this))
        }, teardown: function () {
            if (this.removeEventListener)for (var c = h.length; c;)this.removeEventListener(h[--c], b, !1); else this.onmousewheel = null;
            a.removeData(this, "mousewheel-line-height"), a.removeData(this, "mousewheel-page-height")
        }, getLineHeight: function (b) {
            var c = a(b), d = c["offsetParent"in a.fn ? "offsetParent" : "parent"]();
            return d.length || (d = a("body")), parseInt(d.css("fontSize"), 10) || parseInt(c.css("fontSize"), 10) || 16
        }, getPageHeight: function (b) {
            return a(b).height()
        }, settings: {adjustOldDeltas: !0, normalizeOffset: !0}
    };
    a.fn.extend({
        mousewheel: function (a) {
            return a ? this.bind("mousewheel", a) : this.trigger("mousewheel")
        }, unmousewheel: function (a) {
            return this.unbind("mousewheel", a)
        }
    })
}), function (a) {
    var b, c, d, e, f, g, h, i = "Close", j = "BeforeClose", k = "AfterClose", l = "BeforeAppend", m = "MarkupParse", n = "Open", o = "Change", p = "mfp", q = "." + p, r = "mfp-ready", s = "mfp-removing", t = "mfp-prevent-close", u = function () {
    }, v = !!window.jQuery, w = a(window), x = function (a, c) {
        b.ev.on(p + a + q, c)
    }, y = function (b, c, d, e) {
        var f = document.createElement("div");
        return f.className = "mfp-" + b, d && (f.innerHTML = d), e ? c && c.appendChild(f) : (f = a(f), c && f.appendTo(c)), f
    }, z = function (c, d) {
        b.ev.triggerHandler(p + c, d), b.st.callbacks && (c = c.charAt(0).toLowerCase() + c.slice(1), b.st.callbacks[c] && b.st.callbacks[c].apply(b, a.isArray(d) ? d : [d]))
    }, A = function (c) {
        return c === h && b.currTemplate.closeBtn || (b.currTemplate.closeBtn = a(b.st.closeMarkup.replace("%title%", b.st.tClose)), h = c), b.currTemplate.closeBtn
    }, B = function () {
        a.magnificPopup.instance || (b = new u, b.init(), a.magnificPopup.instance = b)
    }, C = function () {
        var a = document.createElement("p").style, b = ["ms", "O", "Moz", "Webkit"];
        if (void 0 !== a.transition)return !0;
        for (; b.length;)if (b.pop() + "Transition"in a)return !0;
        return !1
    };
    u.prototype = {
        constructor: u, init: function () {
            var c = navigator.appVersion;
            b.isIE7 = -1 !== c.indexOf("MSIE 7."), b.isIE8 = -1 !== c.indexOf("MSIE 8."), b.isLowIE = b.isIE7 || b.isIE8, b.isAndroid = /android/gi.test(c), b.isIOS = /iphone|ipad|ipod/gi.test(c), b.supportsTransition = C(), b.probablyMobile = b.isAndroid || b.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent), d = a(document.body), e = a(document), b.popupsCache = {}
        }, open: function (c) {
            var d;
            if (c.isObj === !1) {
                b.items = c.items.toArray(), b.index = 0;
                var f, h = c.items;
                for (d = 0; d < h.length; d++)if (f = h[d], f.parsed && (f = f.el[0]), f === c.el[0]) {
                    b.index = d;
                    break
                }
            } else b.items = a.isArray(c.items) ? c.items : [c.items], b.index = c.index || 0;
            if (b.isOpen)return void b.updateItemHTML();
            b.types = [], g = "", b.ev = c.mainEl && c.mainEl.length ? c.mainEl.eq(0) : e, c.key ? (b.popupsCache[c.key] || (b.popupsCache[c.key] = {}), b.currTemplate = b.popupsCache[c.key]) : b.currTemplate = {}, b.st = a.extend(!0, {}, a.magnificPopup.defaults, c), b.fixedContentPos = "auto" === b.st.fixedContentPos ? !b.probablyMobile : b.st.fixedContentPos, b.st.modal && (b.st.closeOnContentClick = !1, b.st.closeOnBgClick = !1, b.st.showCloseBtn = !1, b.st.enableEscapeKey = !1), b.bgOverlay || (b.bgOverlay = y("bg").on("click" + q, function () {
                b.close()
            }), b.wrap = y("wrap").attr("tabindex", -1).on("click" + q, function (a) {
                b._checkIfClose(a.target) && b.close()
            }), b.container = y("container", b.wrap)), b.contentContainer = y("content"), b.st.preloader && (b.preloader = y("preloader", b.container, b.st.tLoading));
            var i = a.magnificPopup.modules;
            for (d = 0; d < i.length; d++) {
                var j = i[d];
                j = j.charAt(0).toUpperCase() + j.slice(1), b["init" + j].call(b)
            }
            z("BeforeOpen"), b.st.showCloseBtn && (b.st.closeBtnInside ? (x(m, function (a, b, c, d) {
                c.close_replaceWith = A(d.type)
            }), g += " mfp-close-btn-in") : b.wrap.append(A())), b.st.alignTop && (g += " mfp-align-top"), b.wrap.css(b.fixedContentPos ? {
                overflow: b.st.overflowY,
                overflowX: "hidden",
                overflowY: b.st.overflowY
            } : {
                top: w.scrollTop(),
                position: "absolute"
            }), (b.st.fixedBgPos === !1 || "auto" === b.st.fixedBgPos && !b.fixedContentPos) && b.bgOverlay.css({
                height: e.height(),
                position: "absolute"
            }), b.st.enableEscapeKey && e.on("keyup" + q, function (a) {
                27 === a.keyCode && b.close()
            }), w.on("resize" + q, function () {
                b.updateSize()
            }), b.st.closeOnContentClick || (g += " mfp-auto-cursor"), g && b.wrap.addClass(g);
            var k = b.wH = w.height(), l = {};
            if (b.fixedContentPos && b._hasScrollBar(k)) {
                var o = b._getScrollbarSize();
                o && (l.marginRight = o)
            }
            b.fixedContentPos && (b.isIE7 ? a("body, html").css("overflow", "hidden") : l.overflow = "hidden");
            var p = b.st.mainClass;
            return b.isIE7 && (p += " mfp-ie7"), p && b._addClassToMFP(p), b.updateItemHTML(), z("BuildControls"), a("html").css(l), b.bgOverlay.add(b.wrap).prependTo(document.body), b._lastFocusedEl = document.activeElement, setTimeout(function () {
                b.content ? (b._addClassToMFP(r), b._setFocus()) : b.bgOverlay.addClass(r), e.on("focusin" + q, b._onFocusIn)
            }, 16), b.isOpen = !0, b.updateSize(k), z(n), c
        }, close: function () {
            b.isOpen && (z(j), b.isOpen = !1, b.st.removalDelay && !b.isLowIE && b.supportsTransition ? (b._addClassToMFP(s), setTimeout(function () {
                b._close()
            }, b.st.removalDelay)) : b._close())
        }, _close: function () {
            z(i);
            var c = s + " " + r + " ";
            if (b.bgOverlay.detach(), b.wrap.detach(), b.container.empty(), b.st.mainClass && (c += b.st.mainClass + " "), b._removeClassFromMFP(c), b.fixedContentPos) {
                var d = {marginRight: ""};
                b.isIE7 ? a("body, html").css("overflow", "") : d.overflow = "", a("html").css(d)
            }
            e.off("keyup" + q + " focusin" + q), b.ev.off(q), b.wrap.attr("class", "mfp-wrap").removeAttr("style"), b.bgOverlay.attr("class", "mfp-bg"), b.container.attr("class", "mfp-container"), !b.st.showCloseBtn || b.st.closeBtnInside && b.currTemplate[b.currItem.type] !== !0 || b.currTemplate.closeBtn && b.currTemplate.closeBtn.detach(), b._lastFocusedEl && a(b._lastFocusedEl).focus(), b.currItem = null, b.content = null, b.currTemplate = null, b.prevHeight = 0, z(k)
        }, updateSize: function (a) {
            if (b.isIOS) {
                var c = document.documentElement.clientWidth / window.innerWidth, d = window.innerHeight * c;
                b.wrap.css("height", d), b.wH = d
            } else b.wH = a || w.height();
            b.fixedContentPos || b.wrap.css("height", b.wH), z("Resize")
        }, updateItemHTML: function () {
            var c = b.items[b.index];
            b.contentContainer.detach(), b.content && b.content.detach(), c.parsed || (c = b.parseEl(b.index));
            var d = c.type;
            if (z("BeforeChange", [b.currItem ? b.currItem.type : "", d]), b.currItem = c, !b.currTemplate[d]) {
                var e = b.st[d] ? b.st[d].markup : !1;
                z("FirstMarkupParse", e), b.currTemplate[d] = e ? a(e) : !0
            }
            f && f !== c.type && b.container.removeClass("mfp-" + f + "-holder");
            var g = b["get" + d.charAt(0).toUpperCase() + d.slice(1)](c, b.currTemplate[d]);
            b.appendContent(g, d), c.preloaded = !0, z(o, c), f = c.type, b.container.prepend(b.contentContainer), z("AfterChange")
        }, appendContent: function (a, c) {
            b.content = a, a ? b.st.showCloseBtn && b.st.closeBtnInside && b.currTemplate[c] === !0 ? b.content.find(".mfp-close").length || b.content.append(A()) : b.content = a : b.content = "", z(l), b.container.addClass("mfp-" + c + "-holder"), b.contentContainer.append(b.content)
        }, parseEl: function (c) {
            var d = b.items[c], e = d.type;
            if (d = d.tagName ? {el: a(d)} : {data: d, src: d.src}, d.el) {
                for (var f = b.types, g = 0; g < f.length; g++)if (d.el.hasClass("mfp-" + f[g])) {
                    e = f[g];
                    break
                }
                d.src = d.el.attr("data-mfp-src"), d.src || (d.src = d.el.attr("href"))
            }
            return d.type = e || b.st.type || "inline", d.index = c, d.parsed = !0, b.items[c] = d, z("ElementParse", d), b.items[c]
        }, addGroup: function (a, c) {
            var d = function (d) {
                d.mfpEl = this, b._openClick(d, a, c)
            };
            c || (c = {});
            var e = "click.magnificPopup";
            c.mainEl = a, c.items ? (c.isObj = !0, a.off(e).on(e, d)) : (c.isObj = !1, c.delegate ? a.off(e).on(e, c.delegate, d) : (c.items = a, a.off(e).on(e, d)))
        }, _openClick: function (c, d, e) {
            var f = void 0 !== e.midClick ? e.midClick : a.magnificPopup.defaults.midClick;
            if (f || 2 !== c.which && !c.ctrlKey && !c.metaKey) {
                var g = void 0 !== e.disableOn ? e.disableOn : a.magnificPopup.defaults.disableOn;
                if (g)if (a.isFunction(g)) {
                    if (!g.call(b))return !0
                } else if (w.width() < g)return !0;
                c.type && (c.preventDefault(), b.isOpen && c.stopPropagation()), e.el = a(c.mfpEl), e.delegate && (e.items = d.find(e.delegate)), b.open(e)
            }
        }, updateStatus: function (a, d) {
            if (b.preloader) {
                c !== a && b.container.removeClass("mfp-s-" + c), d || "loading" !== a || (d = b.st.tLoading);
                var e = {status: a, text: d};
                z("UpdateStatus", e), a = e.status, d = e.text, b.preloader.html(d), b.preloader.find("a").on("click", function (a) {
                    a.stopImmediatePropagation()
                }), b.container.addClass("mfp-s-" + a), c = a
            }
        }, _checkIfClose: function (c) {
            if (!a(c).hasClass(t)) {
                var d = b.st.closeOnContentClick, e = b.st.closeOnBgClick;
                if (d && e)return !0;
                if (!b.content || a(c).hasClass("mfp-close") || b.preloader && c === b.preloader[0])return !0;
                if (c === b.content[0] || a.contains(b.content[0], c)) {
                    if (d)return !0
                } else if (e && a.contains(document, c))return !0;
                return !1
            }
        }, _addClassToMFP: function (a) {
            b.bgOverlay.addClass(a), b.wrap.addClass(a)
        }, _removeClassFromMFP: function (a) {
            this.bgOverlay.removeClass(a), b.wrap.removeClass(a)
        }, _hasScrollBar: function (a) {
            return (b.isIE7 ? e.height() : document.body.scrollHeight) > (a || w.height())
        }, _setFocus: function () {
            (b.st.focus ? b.content.find(b.st.focus).eq(0) : b.wrap).focus()
        }, _onFocusIn: function (c) {
            return c.target === b.wrap[0] || a.contains(b.wrap[0], c.target) ? void 0 : (b._setFocus(), !1)
        }, _parseMarkup: function (b, c, d) {
            var e;
            d.data && (c = a.extend(d.data, c)), z(m, [b, c, d]), a.each(c, function (a, c) {
                if (void 0 === c || c === !1)return !0;
                if (e = a.split("_"), e.length > 1) {
                    var d = b.find(q + "-" + e[0]);
                    if (d.length > 0) {
                        var f = e[1];
                        "replaceWith" === f ? d[0] !== c[0] && d.replaceWith(c) : "img" === f ? d.is("img") ? d.attr("src", c) : d.replaceWith('<img src="' + c + '" class="' + d.attr("class") + '" />') : d.attr(e[1], c)
                    }
                } else b.find(q + "-" + a).html(c)
            })
        }, _getScrollbarSize: function () {
            if (void 0 === b.scrollbarSize) {
                var a = document.createElement("div");
                a.id = "mfp-sbm", a.style.cssText = "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;", document.body.appendChild(a), b.scrollbarSize = a.offsetWidth - a.clientWidth, document.body.removeChild(a)
            }
            return b.scrollbarSize
        }
    }, a.magnificPopup = {
        instance: null,
        proto: u.prototype,
        modules: [],
        open: function (b, c) {
            return B(), b = b ? a.extend(!0, {}, b) : {}, b.isObj = !0, b.index = c || 0, this.instance.open(b)
        },
        close: function () {
            return a.magnificPopup.instance && a.magnificPopup.instance.close()
        },
        registerModule: function (b, c) {
            c.options && (a.magnificPopup.defaults[b] = c.options), a.extend(this.proto, c.proto), this.modules.push(b)
        },
        defaults: {
            disableOn: 0,
            key: null,
            midClick: !1,
            mainClass: "",
            preloader: !0,
            focus: "",
            closeOnContentClick: !1,
            closeOnBgClick: !0,
            closeBtnInside: !0,
            showCloseBtn: !0,
            enableEscapeKey: !0,
            modal: !1,
            alignTop: !1,
            removalDelay: 0,
            fixedContentPos: "auto",
            fixedBgPos: "auto",
            overflowY: "auto",
            closeMarkup: '<button title="%title%" type="button" class="mfp-close">&times;</button>',
            tClose: "Close (Esc)",
            tLoading: "Loading..."
        }
    }, a.fn.magnificPopup = function (c) {
        B();
        var d = a(this);
        if ("string" == typeof c)if ("open" === c) {
            var e, f = v ? d.data("magnificPopup") : d[0].magnificPopup, g = parseInt(arguments[1], 10) || 0;
            f.items ? e = f.items[g] : (e = d, f.delegate && (e = e.find(f.delegate)), e = e.eq(g)), b._openClick({mfpEl: e}, d, f)
        } else b.isOpen && b[c].apply(b, Array.prototype.slice.call(arguments, 1)); else c = a.extend(!0, {}, c), v ? d.data("magnificPopup", c) : d[0].magnificPopup = c, b.addGroup(d, c);
        return d
    };
    var D, E, F, G = "inline", H = function () {
        F && (E.after(F.addClass(D)).detach(), F = null)
    };
    a.magnificPopup.registerModule(G, {
        options: {hiddenClass: "hide", markup: "", tNotFound: "Content not found"},
        proto: {
            initInline: function () {
                b.types.push(G), x(i + "." + G, function () {
                    H()
                })
            }, getInline: function (c, d) {
                if (H(), c.src) {
                    var e = b.st.inline, f = a(c.src);
                    if (f.length) {
                        var g = f[0].parentNode;
                        g && g.tagName && (E || (D = e.hiddenClass, E = y(D), D = "mfp-" + D), F = f.after(E).detach().removeClass(D)), b.updateStatus("ready")
                    } else b.updateStatus("error", e.tNotFound), f = a("<div>");
                    return c.inlineElement = f, f
                }
                return b.updateStatus("ready"), b._parseMarkup(d, {}, c), d
            }
        }
    });
    var I, J = "ajax", K = function () {
        I && d.removeClass(I)
    }, L = function () {
        K(), b.req && b.req.abort()
    };
    a.magnificPopup.registerModule(J, {
        options: {
            settings: null,
            cursor: "mfp-ajax-cur",
            tError: '<a href="%url%">The content</a> could not be loaded.'
        }, proto: {
            initAjax: function () {
                b.types.push(J), I = b.st.ajax.cursor, x(i + "." + J, L), x("BeforeChange." + J, L)
            }, getAjax: function (c) {
                I && d.addClass(I), b.updateStatus("loading");
                var e = a.extend({
                    url: c.src, success: function (d, e, f) {
                        var g = {data: d, xhr: f};
                        z("ParseAjax", g), b.appendContent(a(g.data), J), c.finished = !0, K(), b._setFocus(), setTimeout(function () {
                            b.wrap.addClass(r)
                        }, 16), b.updateStatus("ready"), z("AjaxContentAdded")
                    }, error: function () {
                        K(), c.finished = c.loadError = !0, b.updateStatus("error", b.st.ajax.tError.replace("%url%", c.src))
                    }
                }, b.st.ajax.settings);
                return b.req = a.ajax(e), ""
            }
        }
    });
    var M, N = function (c) {
        if (c.data && void 0 !== c.data.title)return c.data.title;
        var d = b.st.image.titleSrc;
        if (d) {
            if (a.isFunction(d))return d.call(b, c);
            if (c.el)return c.el.attr(d) || ""
        }
        return ""
    };
    a.magnificPopup.registerModule("image", {
        options: {
            markup: '<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',
            cursor: "mfp-zoom-out-cur",
            titleSrc: "title",
            verticalFit: !0,
            tError: '<a href="%url%">The image</a> could not be loaded.'
        }, proto: {
            initImage: function () {
                var a = b.st.image, c = ".image";
                b.types.push("image"), x(n + c, function () {
                    "image" === b.currItem.type && a.cursor && d.addClass(a.cursor)
                }), x(i + c, function () {
                    a.cursor && d.removeClass(a.cursor), w.off("resize" + q)
                }), x("Resize" + c, b.resizeImage), b.isLowIE && x("AfterChange", b.resizeImage)
            }, resizeImage: function () {
                var a = b.currItem;
                if (a && a.img && b.st.image.verticalFit) {
                    var c = 0;
                    b.isLowIE && (c = parseInt(a.img.css("padding-top"), 10) + parseInt(a.img.css("padding-bottom"), 10)), a.img.css("max-height", b.wH - c)
                }
            }, _onImageHasSize: function (a) {
                a.img && (a.hasSize = !0, M && clearInterval(M), a.isCheckingImgSize = !1, z("ImageHasSize", a), a.imgHidden && (b.content && b.content.removeClass("mfp-loading"), a.imgHidden = !1))
            }, findImageSize: function (a) {
                var c = 0, d = a.img[0], e = function (f) {
                    M && clearInterval(M), M = setInterval(function () {
                        return d.naturalWidth > 0 ? void b._onImageHasSize(a) : (c > 200 && clearInterval(M), c++, void(3 === c ? e(10) : 40 === c ? e(50) : 100 === c && e(500)))
                    }, f)
                };
                e(1)
            }, getImage: function (c, d) {
                var e = 0, f = function () {
                    c && (c.img[0].complete ? (c.img.off(".mfploader"), c === b.currItem && (b._onImageHasSize(c), b.updateStatus("ready")), c.hasSize = !0, c.loaded = !0, z("ImageLoadComplete")) : (e++, 200 > e ? setTimeout(f, 100) : g()))
                }, g = function () {
                    c && (c.img.off(".mfploader"), c === b.currItem && (b._onImageHasSize(c), b.updateStatus("error", h.tError.replace("%url%", c.src))), c.hasSize = !0, c.loaded = !0, c.loadError = !0)
                }, h = b.st.image, i = d.find(".mfp-img");
                if (i.length) {
                    var j = document.createElement("img");
                    j.className = "mfp-img", c.img = a(j).on("load.mfploader", f).on("error.mfploader", g), j.src = c.src, i.is("img") && (c.img = c.img.clone()), c.img[0].naturalWidth > 0 && (c.hasSize = !0)
                }
                return b._parseMarkup(d, {
                    title: N(c),
                    img_replaceWith: c.img
                }, c), b.resizeImage(), c.hasSize ? (M && clearInterval(M), c.loadError ? (d.addClass("mfp-loading"), b.updateStatus("error", h.tError.replace("%url%", c.src))) : (d.removeClass("mfp-loading"), b.updateStatus("ready")), d) : (b.updateStatus("loading"), c.loading = !0, c.hasSize || (c.imgHidden = !0, d.addClass("mfp-loading"), b.findImageSize(c)), d)
            }
        }
    });
    var O, P = function () {
        return void 0 === O && (O = void 0 !== document.createElement("p").style.MozTransform), O
    };
    a.magnificPopup.registerModule("zoom", {
        options: {
            enabled: !1,
            easing: "ease-in-out",
            duration: 300,
            opener: function (a) {
                return a.is("img") ? a : a.find("img")
            }
        }, proto: {
            initZoom: function () {
                var a, c = b.st.zoom, d = ".zoom";
                if (c.enabled && b.supportsTransition) {
                    var e, f, g = c.duration, h = function (a) {
                        var b = a.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"), d = "all " + c.duration / 1e3 + "s " + c.easing, e = {
                            position: "fixed",
                            zIndex: 9999,
                            left: 0,
                            top: 0,
                            "-webkit-backface-visibility": "hidden"
                        }, f = "transition";
                        return e["-webkit-" + f] = e["-moz-" + f] = e["-o-" + f] = e[f] = d, b.css(e), b
                    }, k = function () {
                        b.content.css("visibility", "visible")
                    };
                    x("BuildControls" + d, function () {
                        if (b._allowZoom()) {
                            if (clearTimeout(e), b.content.css("visibility", "hidden"), a = b._getItemToZoom(), !a)return void k();
                            f = h(a), f.css(b._getOffset()), b.wrap.append(f), e = setTimeout(function () {
                                f.css(b._getOffset(!0)), e = setTimeout(function () {
                                    k(), setTimeout(function () {
                                        f.remove(), a = f = null, z("ZoomAnimationEnded")
                                    }, 16)
                                }, g)
                            }, 16)
                        }
                    }), x(j + d, function () {
                        if (b._allowZoom()) {
                            if (clearTimeout(e), b.st.removalDelay = g, !a) {
                                if (a = b._getItemToZoom(), !a)return;
                                f = h(a)
                            }
                            f.css(b._getOffset(!0)), b.wrap.append(f), b.content.css("visibility", "hidden"), setTimeout(function () {
                                f.css(b._getOffset())
                            }, 16)
                        }
                    }), x(i + d, function () {
                        b._allowZoom() && (k(), f && f.remove(), a = null)
                    })
                }
            }, _allowZoom: function () {
                return "image" === b.currItem.type
            }, _getItemToZoom: function () {
                return b.currItem.hasSize ? b.currItem.img : !1
            }, _getOffset: function (c) {
                var d;
                d = c ? b.currItem.img : b.st.zoom.opener(b.currItem.el || b.currItem);
                var e = d.offset(), f = parseInt(d.css("padding-top"), 10), g = parseInt(d.css("padding-bottom"), 10);
                e.top -= a(window).scrollTop() - f;
                var h = {width: d.width(), height: (v ? d.innerHeight() : d[0].offsetHeight) - g - f};
                return P() ? h["-moz-transform"] = h.transform = "translate(" + e.left + "px," + e.top + "px)" : (h.left = e.left, h.top = e.top), h
            }
        }
    });
    var Q = "iframe", R = "//about:blank", S = function (a) {
        if (b.currTemplate[Q]) {
            var c = b.currTemplate[Q].find("iframe");
            c.length && (a || (c[0].src = R), b.isIE8 && c.css("display", a ? "block" : "none"))
        }
    };
    a.magnificPopup.registerModule(Q, {
        options: {
            markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',
            srcAction: "iframe_src",
            patterns: {
                youtube: {index: "youtube.com", id: "v=", src: "//www.youtube.com/embed/%id%?autoplay=1"},
                vimeo: {index: "vimeo.com/", id: "/", src: "//player.vimeo.com/video/%id%?autoplay=1"},
                gmaps: {index: "//maps.google.", src: "%id%&output=embed"}
            }
        }, proto: {
            initIframe: function () {
                b.types.push(Q), x("BeforeChange", function (a, b, c) {
                    b !== c && (b === Q ? S() : c === Q && S(!0))
                }), x(i + "." + Q, function () {
                    S()
                })
            }, getIframe: function (c, d) {
                var e = c.src, f = b.st.iframe;
                a.each(f.patterns, function () {
                    return e.indexOf(this.index) > -1 ? (this.id && (e = "string" == typeof this.id ? e.substr(e.lastIndexOf(this.id) + this.id.length, e.length) : this.id.call(this, e)), e = this.src.replace("%id%", e), !1) : void 0
                });
                var g = {};
                return f.srcAction && (g[f.srcAction] = e), b._parseMarkup(d, g, c), b.updateStatus("ready"), d
            }
        }
    });
    var T = function (a) {
        var c = b.items.length;
        return a > c - 1 ? a - c : 0 > a ? c + a : a
    }, U = function (a, b, c) {
        return a.replace(/%curr%/gi, b + 1).replace(/%total%/gi, c)
    };
    a.magnificPopup.registerModule("gallery", {
        options: {
            enabled: !1,
            arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
            preload: [0, 2],
            navigateByImgClick: !0,
            arrows: !0,
            tPrev: "Previous (Left arrow key)",
            tNext: "Next (Right arrow key)",
            tCounter: "%curr% of %total%"
        }, proto: {
            initGallery: function () {
                var c = b.st.gallery, d = ".mfp-gallery", f = Boolean(a.fn.mfpFastClick);
                return b.direction = !0, c && c.enabled ? (g += " mfp-gallery", x(n + d, function () {
                    c.navigateByImgClick && b.wrap.on("click" + d, ".mfp-img", function () {
                        return b.items.length > 1 ? (b.next(), !1) : void 0
                    }), e.on("keydown" + d, function (a) {
                        37 === a.keyCode ? b.prev() : 39 === a.keyCode && b.next()
                    })
                }), x("UpdateStatus" + d, function (a, c) {
                    c.text && (c.text = U(c.text, b.currItem.index, b.items.length))
                }), x(m + d, function (a, d, e, f) {
                    var g = b.items.length;
                    e.counter = g > 1 ? U(c.tCounter, f.index, g) : ""
                }), x("BuildControls" + d, function () {
                    if (b.items.length > 1 && c.arrows && !b.arrowLeft) {
                        var d = c.arrowMarkup, e = b.arrowLeft = a(d.replace(/%title%/gi, c.tPrev).replace(/%dir%/gi, "left")).addClass(t), g = b.arrowRight = a(d.replace(/%title%/gi, c.tNext).replace(/%dir%/gi, "right")).addClass(t), h = f ? "mfpFastClick" : "click";
                        e[h](function () {
                            b.prev()
                        }), g[h](function () {
                            b.next()
                        }), b.isIE7 && (y("b", e[0], !1, !0), y("a", e[0], !1, !0), y("b", g[0], !1, !0), y("a", g[0], !1, !0)), b.container.append(e.add(g))
                    }
                }), x(o + d, function () {
                    b._preloadTimeout && clearTimeout(b._preloadTimeout), b._preloadTimeout = setTimeout(function () {
                        b.preloadNearbyImages(), b._preloadTimeout = null
                    }, 16)
                }), void x(i + d, function () {
                    e.off(d), b.wrap.off("click" + d), b.arrowLeft && f && b.arrowLeft.add(b.arrowRight).destroyMfpFastClick(), b.arrowRight = b.arrowLeft = null
                })) : !1
            }, next: function () {
                b.direction = !0, b.index = T(b.index + 1), b.updateItemHTML()
            }, prev: function () {
                b.direction = !1, b.index = T(b.index - 1), b.updateItemHTML()
            }, goTo: function (a) {
                b.direction = a >= b.index, b.index = a, b.updateItemHTML()
            }, preloadNearbyImages: function () {
                var a, c = b.st.gallery.preload, d = Math.min(c[0], b.items.length), e = Math.min(c[1], b.items.length);
                for (a = 1; a <= (b.direction ? e : d); a++)b._preloadItem(b.index + a);
                for (a = 1; a <= (b.direction ? d : e); a++)b._preloadItem(b.index - a)
            }, _preloadItem: function (c) {
                if (c = T(c), !b.items[c].preloaded) {
                    var d = b.items[c];
                    d.parsed || (d = b.parseEl(c)), z("LazyLoad", d), "image" === d.type && (d.img = a('<img class="mfp-img" />').on("load.mfploader", function () {
                        d.hasSize = !0
                    }).on("error.mfploader", function () {
                        d.hasSize = !0, d.loadError = !0, z("LazyLoadError", d)
                    }).attr("src", d.src)), d.preloaded = !0
                }
            }
        }
    });
    var V = "retina";
    a.magnificPopup.registerModule(V, {
        options: {
            replaceSrc: function (a) {
                return a.src.replace(/\.\w+$/, function (a) {
                    return "@2x" + a
                })
            }, ratio: 1
        }, proto: {
            initRetina: function () {
                if (window.devicePixelRatio > 1) {
                    var a = b.st.retina, c = a.ratio;
                    c = isNaN(c) ? c() : c, c > 1 && (x("ImageHasSize." + V, function (a, b) {
                        b.img.css({"max-width": b.img[0].naturalWidth / c, width: "100%"})
                    }), x("ElementParse." + V, function (b, d) {
                        d.src = a.replaceSrc(d, c)
                    }))
                }
            }
        }
    }), function () {
        var b = 1e3, c = "ontouchstart"in window, d = function () {
            w.off("touchmove" + f + " touchend" + f)
        }, e = "mfpFastClick", f = "." + e;
        a.fn.mfpFastClick = function (e) {
            return a(this).each(function () {
                var g, h = a(this);
                if (c) {
                    var i, j, k, l, m, n;
                    h.on("touchstart" + f, function (a) {
                        l = !1, n = 1, m = a.originalEvent ? a.originalEvent.touches[0] : a.touches[0], j = m.clientX, k = m.clientY, w.on("touchmove" + f, function (a) {
                            m = a.originalEvent ? a.originalEvent.touches : a.touches, n = m.length, m = m[0], (Math.abs(m.clientX - j) > 10 || Math.abs(m.clientY - k) > 10) && (l = !0, d())
                        }).on("touchend" + f, function (a) {
                            d(), l || n > 1 || (g = !0, a.preventDefault(), clearTimeout(i), i = setTimeout(function () {
                                g = !1
                            }, b), e())
                        })
                    })
                }
                h.on("click" + f, function () {
                    g || e()
                })
            })
        }, a.fn.destroyMfpFastClick = function () {
            a(this).off("touchstart" + f + " click" + f), c && w.off("touchmove" + f + " touchend" + f)
        }
    }(), B()
}(window.jQuery || window.Zepto), function (a, b, c) {
    "object" == typeof module && module && "object" == typeof module.exports ? module.exports = c : (a[b] = c, "function" == typeof define && define.amd && define(b, [], function () {
        return c
    }))
}(this, "jRespond", function (a, b, c) {
    "use strict";
    return function (a) {
        var b = [], d = [], e = a, f = "", g = "", h = 0, i = 100, j = 500, k = j, l = function () {
            var a = 0;
            return a = "number" != typeof window.innerWidth ? 0 !== document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body.clientWidth : window.innerWidth
        }, m = function (a) {
            if (a.length === c)n(a); else for (var b = 0; b < a.length; b++)n(a[b])
        }, n = function (a) {
            var e = a.breakpoint, h = a.enter || c;
            b.push(a), d.push(!1), q(e) && (h !== c && h.call(null, {entering: f, exiting: g}), d[b.length - 1] = !0)
        }, o = function () {
            for (var a = [], e = [], h = 0; h < b.length; h++) {
                var i = b[h].breakpoint, j = b[h].enter || c, k = b[h].exit || c;
                "*" === i ? (j !== c && a.push(j), k !== c && e.push(k)) : q(i) ? (j === c || d[h] || a.push(j), d[h] = !0) : (k !== c && d[h] && e.push(k), d[h] = !1)
            }
            for (var l = {entering: f, exiting: g}, m = 0; m < e.length; m++)e[m].call(null, l);
            for (var n = 0; n < a.length; n++)a[n].call(null, l)
        }, p = function (a) {
            for (var b = !1, c = 0; c < e.length; c++)if (a >= e[c].enter && a <= e[c].exit) {
                b = !0;
                break
            }
            b && f !== e[c].label ? (g = f, f = e[c].label, o()) : b || "" === f || (f = "", o())
        }, q = function (a) {
            if ("object" == typeof a) {
                if (a.join().indexOf(f) >= 0)return !0
            } else {
                if ("*" === a)return !0;
                if ("string" == typeof a && f === a)return !0
            }
        }, r = function () {
            var a = l();
            a !== h ? (k = i, p(a)) : k = j, h = a, setTimeout(r, k)
        };
        return r(), {
            addFunc: function (a) {
                m(a)
            }, getBreakpoint: function () {
                return f
            }
        }
    }
}(this, this.document)), function () {
    var a = this, b = a._, c = Array.prototype, d = Object.prototype, e = Function.prototype, f = c.push, g = c.slice, h = c.concat, i = d.toString, j = d.hasOwnProperty, k = Array.isArray, l = Object.keys, m = e.bind, n = function (a) {
        return a instanceof n ? a : this instanceof n ? void(this._wrapped = a) : new n(a)
    };
    "undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = n), exports._ = n) : a._ = n, n.VERSION = "1.7.0";
    var o = function (a, b, c) {
        if (void 0 === b)return a;
        switch (null == c ? 3 : c) {
            case 1:
                return function (c) {
                    return a.call(b, c)
                };
            case 2:
                return function (c, d) {
                    return a.call(b, c, d)
                };
            case 3:
                return function (c, d, e) {
                    return a.call(b, c, d, e)
                };
            case 4:
                return function (c, d, e, f) {
                    return a.call(b, c, d, e, f)
                }
        }
        return function () {
            return a.apply(b, arguments)
        }
    };
    n.iteratee = function (a, b, c) {
        return null == a ? n.identity : n.isFunction(a) ? o(a, b, c) : n.isObject(a) ? n.matches(a) : n.property(a)
    }, n.each = n.forEach = function (a, b, c) {
        if (null == a)return a;
        b = o(b, c);
        var d, e = a.length;
        if (e === +e)for (d = 0; e > d; d++)b(a[d], d, a); else {
            var f = n.keys(a);
            for (d = 0, e = f.length; e > d; d++)b(a[f[d]], f[d], a)
        }
        return a
    }, n.map = n.collect = function (a, b, c) {
        if (null == a)return [];
        b = n.iteratee(b, c);
        for (var d, e = a.length !== +a.length && n.keys(a), f = (e || a).length, g = Array(f), h = 0; f > h; h++)d = e ? e[h] : h, g[h] = b(a[d], d, a);
        return g
    };
    var p = "Reduce of empty array with no initial value";
    n.reduce = n.foldl = n.inject = function (a, b, c, d) {
        null == a && (a = []), b = o(b, d, 4);
        var e, f = a.length !== +a.length && n.keys(a), g = (f || a).length, h = 0;
        if (arguments.length < 3) {
            if (!g)throw new TypeError(p);
            c = a[f ? f[h++] : h++]
        }
        for (; g > h; h++)e = f ? f[h] : h, c = b(c, a[e], e, a);
        return c
    }, n.reduceRight = n.foldr = function (a, b, c, d) {
        null == a && (a = []), b = o(b, d, 4);
        var e, f = a.length !== +a.length && n.keys(a), g = (f || a).length;
        if (arguments.length < 3) {
            if (!g)throw new TypeError(p);
            c = a[f ? f[--g] : --g]
        }
        for (; g--;)e = f ? f[g] : g, c = b(c, a[e], e, a);
        return c
    }, n.find = n.detect = function (a, b, c) {
        var d;
        return b = n.iteratee(b, c), n.some(a, function (a, c, e) {
            return b(a, c, e) ? (d = a, !0) : void 0
        }), d
    }, n.filter = n.select = function (a, b, c) {
        var d = [];
        return null == a ? d : (b = n.iteratee(b, c), n.each(a, function (a, c, e) {
            b(a, c, e) && d.push(a)
        }), d)
    }, n.reject = function (a, b, c) {
        return n.filter(a, n.negate(n.iteratee(b)), c)
    }, n.every = n.all = function (a, b, c) {
        if (null == a)return !0;
        b = n.iteratee(b, c);
        var d, e, f = a.length !== +a.length && n.keys(a), g = (f || a).length;
        for (d = 0; g > d; d++)if (e = f ? f[d] : d, !b(a[e], e, a))return !1;
        return !0
    }, n.some = n.any = function (a, b, c) {
        if (null == a)return !1;
        b = n.iteratee(b, c);
        var d, e, f = a.length !== +a.length && n.keys(a), g = (f || a).length;
        for (d = 0; g > d; d++)if (e = f ? f[d] : d, b(a[e], e, a))return !0;
        return !1
    }, n.contains = n.include = function (a, b) {
        return null == a ? !1 : (a.length !== +a.length && (a = n.values(a)), n.indexOf(a, b) >= 0)
    }, n.invoke = function (a, b) {
        var c = g.call(arguments, 2), d = n.isFunction(b);
        return n.map(a, function (a) {
            return (d ? b : a[b]).apply(a, c)
        })
    }, n.pluck = function (a, b) {
        return n.map(a, n.property(b))
    }, n.where = function (a, b) {
        return n.filter(a, n.matches(b))
    }, n.findWhere = function (a, b) {
        return n.find(a, n.matches(b))
    }, n.max = function (a, b, c) {
        var d, e, f = -1 / 0, g = -1 / 0;
        if (null == b && null != a) {
            a = a.length === +a.length ? a : n.values(a);
            for (var h = 0, i = a.length; i > h; h++)d = a[h], d > f && (f = d)
        } else b = n.iteratee(b, c), n.each(a, function (a, c, d) {
            e = b(a, c, d), (e > g || e === -1 / 0 && f === -1 / 0) && (f = a, g = e)
        });
        return f
    }, n.min = function (a, b, c) {
        var d, e, f = 1 / 0, g = 1 / 0;
        if (null == b && null != a) {
            a = a.length === +a.length ? a : n.values(a);
            for (var h = 0, i = a.length; i > h; h++)d = a[h], f > d && (f = d)
        } else b = n.iteratee(b, c), n.each(a, function (a, c, d) {
            e = b(a, c, d), (g > e || 1 / 0 === e && 1 / 0 === f) && (f = a, g = e)
        });
        return f
    }, n.shuffle = function (a) {
        for (var b, c = a && a.length === +a.length ? a : n.values(a), d = c.length, e = Array(d), f = 0; d > f; f++)b = n.random(0, f), b !== f && (e[f] = e[b]), e[b] = c[f];
        return e
    }, n.sample = function (a, b, c) {
        return null == b || c ? (a.length !== +a.length && (a = n.values(a)), a[n.random(a.length - 1)]) : n.shuffle(a).slice(0, Math.max(0, b))
    }, n.sortBy = function (a, b, c) {
        return b = n.iteratee(b, c), n.pluck(n.map(a, function (a, c, d) {
            return {value: a, index: c, criteria: b(a, c, d)}
        }).sort(function (a, b) {
            var c = a.criteria, d = b.criteria;
            if (c !== d) {
                if (c > d || void 0 === c)return 1;
                if (d > c || void 0 === d)return -1
            }
            return a.index - b.index
        }), "value")
    };
    var q = function (a) {
        return function (b, c, d) {
            var e = {};
            return c = n.iteratee(c, d), n.each(b, function (d, f) {
                var g = c(d, f, b);
                a(e, d, g)
            }), e
        }
    };
    n.groupBy = q(function (a, b, c) {
        n.has(a, c) ? a[c].push(b) : a[c] = [b]
    }), n.indexBy = q(function (a, b, c) {
        a[c] = b
    }), n.countBy = q(function (a, b, c) {
        n.has(a, c) ? a[c]++ : a[c] = 1
    }), n.sortedIndex = function (a, b, c, d) {
        c = n.iteratee(c, d, 1);
        for (var e = c(b), f = 0, g = a.length; g > f;) {
            var h = f + g >>> 1;
            c(a[h]) < e ? f = h + 1 : g = h
        }
        return f
    }, n.toArray = function (a) {
        return a ? n.isArray(a) ? g.call(a) : a.length === +a.length ? n.map(a, n.identity) : n.values(a) : []
    }, n.size = function (a) {
        return null == a ? 0 : a.length === +a.length ? a.length : n.keys(a).length
    }, n.partition = function (a, b, c) {
        b = n.iteratee(b, c);
        var d = [], e = [];
        return n.each(a, function (a, c, f) {
            (b(a, c, f) ? d : e).push(a)
        }), [d, e]
    }, n.first = n.head = n.take = function (a, b, c) {
        return null == a ? void 0 : null == b || c ? a[0] : 0 > b ? [] : g.call(a, 0, b)
    }, n.initial = function (a, b, c) {
        return g.call(a, 0, Math.max(0, a.length - (null == b || c ? 1 : b)))
    }, n.last = function (a, b, c) {
        return null == a ? void 0 : null == b || c ? a[a.length - 1] : g.call(a, Math.max(a.length - b, 0))
    }, n.rest = n.tail = n.drop = function (a, b, c) {
        return g.call(a, null == b || c ? 1 : b)
    }, n.compact = function (a) {
        return n.filter(a, n.identity)
    };
    var r = function (a, b, c, d) {
        if (b && n.every(a, n.isArray))return h.apply(d, a);
        for (var e = 0, g = a.length; g > e; e++) {
            var i = a[e];
            n.isArray(i) || n.isArguments(i) ? b ? f.apply(d, i) : r(i, b, c, d) : c || d.push(i)
        }
        return d
    };
    n.flatten = function (a, b) {
        return r(a, b, !1, [])
    }, n.without = function (a) {
        return n.difference(a, g.call(arguments, 1))
    }, n.uniq = n.unique = function (a, b, c, d) {
        if (null == a)return [];
        n.isBoolean(b) || (d = c, c = b, b = !1), null != c && (c = n.iteratee(c, d));
        for (var e = [], f = [], g = 0, h = a.length; h > g; g++) {
            var i = a[g];
            if (b)g && f === i || e.push(i), f = i; else if (c) {
                var j = c(i, g, a);
                n.indexOf(f, j) < 0 && (f.push(j), e.push(i))
            } else n.indexOf(e, i) < 0 && e.push(i)
        }
        return e
    }, n.union = function () {
        return n.uniq(r(arguments, !0, !0, []))
    }, n.intersection = function (a) {
        if (null == a)return [];
        for (var b = [], c = arguments.length, d = 0, e = a.length; e > d; d++) {
            var f = a[d];
            if (!n.contains(b, f)) {
                for (var g = 1; c > g && n.contains(arguments[g], f); g++);
                g === c && b.push(f)
            }
        }
        return b
    }, n.difference = function (a) {
        var b = r(g.call(arguments, 1), !0, !0, []);
        return n.filter(a, function (a) {
            return !n.contains(b, a)
        })
    }, n.zip = function (a) {
        if (null == a)return [];
        for (var b = n.max(arguments, "length").length, c = Array(b), d = 0; b > d; d++)c[d] = n.pluck(arguments, d);
        return c
    }, n.object = function (a, b) {
        if (null == a)return {};
        for (var c = {}, d = 0, e = a.length; e > d; d++)b ? c[a[d]] = b[d] : c[a[d][0]] = a[d][1];
        return c
    }, n.indexOf = function (a, b, c) {
        if (null == a)return -1;
        var d = 0, e = a.length;
        if (c) {
            if ("number" != typeof c)return d = n.sortedIndex(a, b), a[d] === b ? d : -1;
            d = 0 > c ? Math.max(0, e + c) : c
        }
        for (; e > d; d++)if (a[d] === b)return d;
        return -1
    }, n.lastIndexOf = function (a, b, c) {
        if (null == a)return -1;
        var d = a.length;
        for ("number" == typeof c && (d = 0 > c ? d + c + 1 : Math.min(d, c + 1)); --d >= 0;)if (a[d] === b)return d;
        return -1
    }, n.range = function (a, b, c) {
        arguments.length <= 1 && (b = a || 0, a = 0), c = c || 1;
        for (var d = Math.max(Math.ceil((b - a) / c), 0), e = Array(d), f = 0; d > f; f++, a += c)e[f] = a;
        return e
    };
    var s = function () {
    };
    n.bind = function (a, b) {
        var c, d;
        if (m && a.bind === m)return m.apply(a, g.call(arguments, 1));
        if (!n.isFunction(a))throw new TypeError("Bind must be called on a function");
        return c = g.call(arguments, 2), d = function () {
            if (!(this instanceof d))return a.apply(b, c.concat(g.call(arguments)));
            s.prototype = a.prototype;
            var e = new s;
            s.prototype = null;
            var f = a.apply(e, c.concat(g.call(arguments)));
            return n.isObject(f) ? f : e
        }
    }, n.partial = function (a) {
        var b = g.call(arguments, 1);
        return function () {
            for (var c = 0, d = b.slice(), e = 0, f = d.length; f > e; e++)d[e] === n && (d[e] = arguments[c++]);
            for (; c < arguments.length;)d.push(arguments[c++]);
            return a.apply(this, d)
        }
    }, n.bindAll = function (a) {
        var b, c, d = arguments.length;
        if (1 >= d)throw new Error("bindAll must be passed function names");
        for (b = 1; d > b; b++)c = arguments[b], a[c] = n.bind(a[c], a);
        return a
    }, n.memoize = function (a, b) {
        var c = function (d) {
            var e = c.cache, f = b ? b.apply(this, arguments) : d;
            return n.has(e, f) || (e[f] = a.apply(this, arguments)), e[f]
        };
        return c.cache = {}, c
    }, n.delay = function (a, b) {
        var c = g.call(arguments, 2);
        return setTimeout(function () {
            return a.apply(null, c)
        }, b)
    }, n.defer = function (a) {
        return n.delay.apply(n, [a, 1].concat(g.call(arguments, 1)))
    }, n.throttle = function (a, b, c) {
        var d, e, f, g = null, h = 0;
        c || (c = {});
        var i = function () {
            h = c.leading === !1 ? 0 : n.now(), g = null, f = a.apply(d, e), g || (d = e = null)
        };
        return function () {
            var j = n.now();
            h || c.leading !== !1 || (h = j);
            var k = b - (j - h);
            return d = this, e = arguments, 0 >= k || k > b ? (clearTimeout(g), g = null, h = j, f = a.apply(d, e), g || (d = e = null)) : g || c.trailing === !1 || (g = setTimeout(i, k)), f
        }
    }, n.debounce = function (a, b, c) {
        var d, e, f, g, h, i = function () {
            var j = n.now() - g;
            b > j && j > 0 ? d = setTimeout(i, b - j) : (d = null, c || (h = a.apply(f, e), d || (f = e = null)))
        };
        return function () {
            f = this, e = arguments, g = n.now();
            var j = c && !d;
            return d || (d = setTimeout(i, b)), j && (h = a.apply(f, e), f = e = null), h
        }
    }, n.wrap = function (a, b) {
        return n.partial(b, a)
    }, n.negate = function (a) {
        return function () {
            return !a.apply(this, arguments)
        }
    }, n.compose = function () {
        var a = arguments, b = a.length - 1;
        return function () {
            for (var c = b, d = a[b].apply(this, arguments); c--;)d = a[c].call(this, d);
            return d
        }
    }, n.after = function (a, b) {
        return function () {
            return --a < 1 ? b.apply(this, arguments) : void 0
        }
    }, n.before = function (a, b) {
        var c;
        return function () {
            return --a > 0 ? c = b.apply(this, arguments) : b = null, c
        }
    }, n.once = n.partial(n.before, 2), n.keys = function (a) {
        if (!n.isObject(a))return [];
        if (l)return l(a);
        var b = [];
        for (var c in a)n.has(a, c) && b.push(c);
        return b
    }, n.values = function (a) {
        for (var b = n.keys(a), c = b.length, d = Array(c), e = 0; c > e; e++)d[e] = a[b[e]];
        return d
    }, n.pairs = function (a) {
        for (var b = n.keys(a), c = b.length, d = Array(c), e = 0; c > e; e++)d[e] = [b[e], a[b[e]]];
        return d
    }, n.invert = function (a) {
        for (var b = {}, c = n.keys(a), d = 0, e = c.length; e > d; d++)b[a[c[d]]] = c[d];
        return b
    }, n.functions = n.methods = function (a) {
        var b = [];
        for (var c in a)n.isFunction(a[c]) && b.push(c);
        return b.sort()
    }, n.extend = function (a) {
        if (!n.isObject(a))return a;
        for (var b, c, d = 1, e = arguments.length; e > d; d++) {
            b = arguments[d];
            for (c in b)j.call(b, c) && (a[c] = b[c])
        }
        return a
    }, n.pick = function (a, b, c) {
        var d, e = {};
        if (null == a)return e;
        if (n.isFunction(b)) {
            b = o(b, c);
            for (d in a) {
                var f = a[d];
                b(f, d, a) && (e[d] = f)
            }
        } else {
            var i = h.apply([], g.call(arguments, 1));
            a = new Object(a);
            for (var j = 0, k = i.length; k > j; j++)d = i[j], d in a && (e[d] = a[d])
        }
        return e
    }, n.omit = function (a, b, c) {
        if (n.isFunction(b))b = n.negate(b); else {
            var d = n.map(h.apply([], g.call(arguments, 1)), String);
            b = function (a, b) {
                return !n.contains(d, b)
            }
        }
        return n.pick(a, b, c)
    }, n.defaults = function (a) {
        if (!n.isObject(a))return a;
        for (var b = 1, c = arguments.length; c > b; b++) {
            var d = arguments[b];
            for (var e in d)void 0 === a[e] && (a[e] = d[e])
        }
        return a
    }, n.clone = function (a) {
        return n.isObject(a) ? n.isArray(a) ? a.slice() : n.extend({}, a) : a
    }, n.tap = function (a, b) {
        return b(a), a
    };
    var t = function (a, b, c, d) {
        if (a === b)return 0 !== a || 1 / a === 1 / b;
        if (null == a || null == b)return a === b;
        a instanceof n && (a = a._wrapped), b instanceof n && (b = b._wrapped);
        var e = i.call(a);
        if (e !== i.call(b))return !1;
        switch (e) {
            case"[object RegExp]":
            case"[object String]":
                return "" + a == "" + b;
            case"[object Number]":
                return +a !== +a ? +b !== +b : 0 === +a ? 1 / +a === 1 / b : +a === +b;
            case"[object Date]":
            case"[object Boolean]":
                return +a === +b
        }
        if ("object" != typeof a || "object" != typeof b)return !1;
        for (var f = c.length; f--;)if (c[f] === a)return d[f] === b;
        var g = a.constructor, h = b.constructor;
        if (g !== h && "constructor"in a && "constructor"in b && !(n.isFunction(g) && g instanceof g && n.isFunction(h) && h instanceof h))return !1;
        c.push(a), d.push(b);
        var j, k;
        if ("[object Array]" === e) {
            if (j = a.length, k = j === b.length)for (; j-- && (k = t(a[j], b[j], c, d)););
        } else {
            var l, m = n.keys(a);
            if (j = m.length, k = n.keys(b).length === j)for (; j-- && (l = m[j], k = n.has(b, l) && t(a[l], b[l], c, d)););
        }
        return c.pop(), d.pop(), k
    };
    n.isEqual = function (a, b) {
        return t(a, b, [], [])
    }, n.isEmpty = function (a) {
        if (null == a)return !0;
        if (n.isArray(a) || n.isString(a) || n.isArguments(a))return 0 === a.length;
        for (var b in a)if (n.has(a, b))return !1;
        return !0
    }, n.isElement = function (a) {
        return !(!a || 1 !== a.nodeType)
    }, n.isArray = k || function (a) {
        return "[object Array]" === i.call(a)
    }, n.isObject = function (a) {
        var b = typeof a;
        return "function" === b || "object" === b && !!a
    }, n.each(["Arguments", "Function", "String", "Number", "Date", "RegExp"], function (a) {
        n["is" + a] = function (b) {
            return i.call(b) === "[object " + a + "]"
        }
    }), n.isArguments(arguments) || (n.isArguments = function (a) {
        return n.has(a, "callee")
    }), "function" != typeof/./ && (n.isFunction = function (a) {
        return "function" == typeof a || !1
    }), n.isFinite = function (a) {
        return isFinite(a) && !isNaN(parseFloat(a))
    }, n.isNaN = function (a) {
        return n.isNumber(a) && a !== +a
    }, n.isBoolean = function (a) {
        return a === !0 || a === !1 || "[object Boolean]" === i.call(a)
    }, n.isNull = function (a) {
        return null === a
    }, n.isUndefined = function (a) {
        return void 0 === a
    }, n.has = function (a, b) {
        return null != a && j.call(a, b)
    }, n.noConflict = function () {
        return a._ = b, this
    }, n.identity = function (a) {
        return a
    }, n.constant = function (a) {
        return function () {
            return a
        }
    }, n.noop = function () {
    }, n.property = function (a) {
        return function (b) {
            return b[a]
        }
    }, n.matches = function (a) {
        var b = n.pairs(a), c = b.length;
        return function (a) {
            if (null == a)return !c;
            a = new Object(a);
            for (var d = 0; c > d; d++) {
                var e = b[d], f = e[0];
                if (e[1] !== a[f] || !(f in a))return !1
            }
            return !0
        }
    }, n.times = function (a, b, c) {
        var d = Array(Math.max(0, a));
        b = o(b, c, 1);
        for (var e = 0; a > e; e++)d[e] = b(e);
        return d
    }, n.random = function (a, b) {
        return null == b && (b = a, a = 0), a + Math.floor(Math.random() * (b - a + 1))
    }, n.now = Date.now || function () {
        return (new Date).getTime()
    };
    var u = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': "&quot;",
        "'": "&#x27;",
        "`": "&#x60;"
    }, v = n.invert(u), w = function (a) {
        var b = function (b) {
            return a[b]
        }, c = "(?:" + n.keys(a).join("|") + ")", d = RegExp(c), e = RegExp(c, "g");
        return function (a) {
            return a = null == a ? "" : "" + a, d.test(a) ? a.replace(e, b) : a
        }
    };
    n.escape = w(u), n.unescape = w(v), n.result = function (a, b) {
        if (null == a)return void 0;
        var c = a[b];
        return n.isFunction(c) ? a[b]() : c
    };
    var x = 0;
    n.uniqueId = function (a) {
        var b = ++x + "";
        return a ? a + b : b
    }, n.templateSettings = {evaluate: /<%([\s\S]+?)%>/g, interpolate: /<%=([\s\S]+?)%>/g, escape: /<%-([\s\S]+?)%>/g};
    var y = /(.)^/, z = {
        "'": "'",
        "\\": "\\",
        "\r": "r",
        "\n": "n",
        "\u2028": "u2028",
        "\u2029": "u2029"
    }, A = /\\|'|\r|\n|\u2028|\u2029/g, B = function (a) {
        return "\\" + z[a]
    };
    n.template = function (a, b, c) {
        !b && c && (b = c), b = n.defaults({}, b, n.templateSettings);
        var d = RegExp([(b.escape || y).source, (b.interpolate || y).source, (b.evaluate || y).source].join("|") + "|$", "g"), e = 0, f = "__p+='";
        a.replace(d, function (b, c, d, g, h) {
            return f += a.slice(e, h).replace(A, B), e = h + b.length, c ? f += "'+\n((__t=(" + c + "))==null?'':_.escape(__t))+\n'" : d ? f += "'+\n((__t=(" + d + "))==null?'':__t)+\n'" : g && (f += "';\n" + g + "\n__p+='"), b
        }), f += "';\n", b.variable || (f = "with(obj||{}){\n" + f + "}\n"), f = "var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n" + f + "return __p;\n";
        try {
            var g = new Function(b.variable || "obj", "_", f)
        } catch (h) {
            throw h.source = f, h
        }
        var i = function (a) {
            return g.call(this, a, n)
        }, j = b.variable || "obj";
        return i.source = "function(" + j + "){\n" + f + "}", i
    }, n.chain = function (a) {
        var b = n(a);
        return b._chain = !0, b
    };
    var C = function (a) {
        return this._chain ? n(a).chain() : a
    };
    n.mixin = function (a) {
        n.each(n.functions(a), function (b) {
            var c = n[b] = a[b];
            n.prototype[b] = function () {
                var a = [this._wrapped];
                return f.apply(a, arguments), C.call(this, c.apply(n, a))
            }
        })
    }, n.mixin(n), n.each(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function (a) {
        var b = c[a];
        n.prototype[a] = function () {
            var c = this._wrapped;
            return b.apply(c, arguments), "shift" !== a && "splice" !== a || 0 !== c.length || delete c[0], C.call(this, c)
        }
    }), n.each(["concat", "join", "slice"], function (a) {
        var b = c[a];
        n.prototype[a] = function () {
            return C.call(this, b.apply(this._wrapped, arguments))
        }
    }), n.prototype.value = function () {
        return this._wrapped
    }, "function" == typeof define && define.amd && define("underscore", [], function () {
        return n
    })
}.call(this), function (a) {
    "undefined" == typeof a.fn.each2 && a.extend(a.fn, {
        each2: function (b) {
            for (var c = a([0]), d = -1, e = this.length; ++d < e && (c.context = c[0] = this[d]) && b.call(c[0], d, c) !== !1;);
            return this
        }
    })
}(jQuery), function (a, b) {
    "use strict";
    function c(b) {
        var c = a(document.createTextNode(""));
        b.before(c), c.before(b), c.remove()
    }

    function d(a) {
        function b(a) {
            return O[a] || a
        }

        return a.replace(/[^\u0000-\u007E]/g, b)
    }

    function e(a, b) {
        for (var c = 0, d = b.length; d > c; c += 1)if (g(a, b[c]))return c;
        return -1
    }

    function f() {
        var b = a(N);
        b.appendTo(document.body);
        var c = {width: b.width() - b[0].clientWidth, height: b.height() - b[0].clientHeight};
        return b.remove(), c
    }

    function g(a, c) {
        return a === c ? !0 : a === b || c === b ? !1 : null === a || null === c ? !1 : a.constructor === String ? a + "" == c + "" : c.constructor === String ? c + "" == a + "" : !1
    }

    function h(a, b, c) {
        var d, e, f;
        if (null === a || a.length < 1)return [];
        for (d = a.split(b), e = 0, f = d.length; f > e; e += 1)d[e] = c(d[e]);
        return d
    }

    function i(a) {
        return a.outerWidth(!1) - a.width()
    }

    function j(c) {
        var d = "keyup-change-value";
        c.on("keydown", function () {
            a.data(c, d) === b && a.data(c, d, c.val())
        }), c.on("keyup", function () {
            var e = a.data(c, d);
            e !== b && c.val() !== e && (a.removeData(c, d), c.trigger("keyup-change"))
        })
    }

    function k(c) {
        c.on("mousemove", function (c) {
            var d = L;
            (d === b || d.x !== c.pageX || d.y !== c.pageY) && a(c.target).trigger("mousemove-filtered", c)
        })
    }

    function l(a, c, d) {
        d = d || b;
        var e;
        return function () {
            var b = arguments;
            window.clearTimeout(e), e = window.setTimeout(function () {
                c.apply(d, b)
            }, a)
        }
    }

    function m(a, b) {
        var c = l(a, function (a) {
            b.trigger("scroll-debounced", a)
        });
        b.on("scroll", function (a) {
            e(a.target, b.get()) >= 0 && c(a)
        })
    }

    function n(a) {
        a[0] !== document.activeElement && window.setTimeout(function () {
            var b, c = a[0], d = a.val().length;
            a.focus();
            var e = c.offsetWidth > 0 || c.offsetHeight > 0;
            e && c === document.activeElement && (c.setSelectionRange ? c.setSelectionRange(d, d) : c.createTextRange && (b = c.createTextRange(), b.collapse(!1), b.select()))
        }, 0)
    }

    function o(b) {
        b = a(b)[0];
        var c = 0, d = 0;
        if ("selectionStart"in b)c = b.selectionStart, d = b.selectionEnd - c; else if ("selection"in document) {
            b.focus();
            var e = document.selection.createRange();
            d = document.selection.createRange().text.length, e.moveStart("character", -b.value.length), c = e.text.length - d
        }
        return {offset: c, length: d}
    }

    function p(a) {
        a.preventDefault(), a.stopPropagation()
    }

    function q(a) {
        a.preventDefault(), a.stopImmediatePropagation()
    }

    function r(b) {
        if (!I) {
            var c = b[0].currentStyle || window.getComputedStyle(b[0], null);
            I = a(document.createElement("div")).css({
                position: "absolute",
                left: "-10000px",
                top: "-10000px",
                display: "none",
                fontSize: c.fontSize,
                fontFamily: c.fontFamily,
                fontStyle: c.fontStyle,
                fontWeight: c.fontWeight,
                letterSpacing: c.letterSpacing,
                textTransform: c.textTransform,
                whiteSpace: "nowrap"
            }), I.attr("class", "select2-sizer"), a(document.body).append(I)
        }
        return I.text(b.val()), I.width()
    }

    function s(b, c, d) {
        var e, f, g = [];
        e = a.trim(b.attr("class")), e && (e = "" + e, a(e.split(/\s+/)).each2(function () {
            0 === this.indexOf("select2-") && g.push(this)
        })), e = a.trim(c.attr("class")), e && (e = "" + e, a(e.split(/\s+/)).each2(function () {
            0 !== this.indexOf("select2-") && (f = d(this), f && g.push(f))
        })), b.attr("class", g.join(" "))
    }

    function t(a, b, c, e) {
        var f = d(a.toUpperCase()).indexOf(d(b.toUpperCase())), g = b.length;
        return 0 > f ? void c.push(e(a)) : (c.push(e(a.substring(0, f))), c.push("<span class='select2-match'>"), c.push(e(a.substring(f, f + g))), c.push("</span>"), void c.push(e(a.substring(f + g, a.length))))
    }

    function u(a) {
        var b = {"\\": "&#92;", "&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#39;", "/": "&#47;"};
        return String(a).replace(/[&<>"'\/\\]/g, function (a) {
            return b[a]
        })
    }

    function v(c) {
        var d, e = null, f = c.quietMillis || 100, g = c.url, h = this;
        return function (i) {
            window.clearTimeout(d), d = window.setTimeout(function () {
                var d = c.data, f = g, j = c.transport || a.fn.select2.ajaxDefaults.transport, k = {
                    type: c.type || "GET",
                    cache: c.cache || !1,
                    jsonpCallback: c.jsonpCallback || b,
                    dataType: c.dataType || "json"
                }, l = a.extend({}, a.fn.select2.ajaxDefaults.params, k);
                d = d ? d.call(h, i.term, i.page, i.context) : null, f = "function" == typeof f ? f.call(h, i.term, i.page, i.context) : f, e && "function" == typeof e.abort && e.abort(), c.params && (a.isFunction(c.params) ? a.extend(l, c.params.call(h)) : a.extend(l, c.params)), a.extend(l, {
                    url: f,
                    dataType: c.dataType,
                    data: d,
                    success: function (a) {
                        var b = c.results(a, i.page, i);
                        i.callback(b)
                    },
                    error: function (a, b, c) {
                        var d = {hasError: !0, jqXHR: a, textStatus: b, errorThrown: c};
                        i.callback(d)
                    }
                }), e = j.call(h, l)
            }, f)
        }
    }

    function w(b) {
        var c, d, e = b, f = function (a) {
            return "" + a.text
        };
        a.isArray(e) && (d = e, e = {results: d}), a.isFunction(e) === !1 && (d = e, e = function () {
            return d
        });
        var g = e();
        return g.text && (f = g.text, a.isFunction(f) || (c = g.text, f = function (a) {
            return a[c]
        })), function (b) {
            var c, d = b.term, g = {results: []};
            return "" === d ? void b.callback(e()) : (c = function (e, g) {
                var h, i;
                if (e = e[0], e.children) {
                    h = {};
                    for (i in e)e.hasOwnProperty(i) && (h[i] = e[i]);
                    h.children = [], a(e.children).each2(function (a, b) {
                        c(b, h.children)
                    }), (h.children.length || b.matcher(d, f(h), e)) && g.push(h)
                } else b.matcher(d, f(e), e) && g.push(e)
            }, a(e().results).each2(function (a, b) {
                c(b, g.results)
            }), void b.callback(g))
        }
    }

    function x(c) {
        var d = a.isFunction(c);
        return function (e) {
            var f = e.term, g = {results: []}, h = d ? c(e) : c;
            a.isArray(h) && (a(h).each(function () {
                var a = this.text !== b, c = a ? this.text : this;
                ("" === f || e.matcher(f, c)) && g.results.push(a ? this : {id: this, text: this})
            }), e.callback(g))
        }
    }

    function y(b, c) {
        if (a.isFunction(b))return !0;
        if (!b)return !1;
        if ("string" == typeof b)return !0;
        throw new Error(c + " must be a string, function, or falsy value")
    }

    function z(b, c) {
        if (a.isFunction(b)) {
            var d = Array.prototype.slice.call(arguments, 2);
            return b.apply(c, d)
        }
        return b
    }

    function A(b) {
        var c = 0;
        return a.each(b, function (a, b) {
            b.children ? c += A(b.children) : c++
        }), c
    }

    function B(a, c, d, e) {
        var f, h, i, j, k, l = a, m = !1;
        if (!e.createSearchChoice || !e.tokenSeparators || e.tokenSeparators.length < 1)return b;
        for (; ;) {
            for (h = -1, i = 0, j = e.tokenSeparators.length; j > i && (k = e.tokenSeparators[i], h = a.indexOf(k), !(h >= 0)); i++);
            if (0 > h)break;
            if (f = a.substring(0, h), a = a.substring(h + k.length), f.length > 0 && (f = e.createSearchChoice.call(this, f, c), f !== b && null !== f && e.id(f) !== b && null !== e.id(f))) {
                for (m = !1, i = 0, j = c.length; j > i; i++)if (g(e.id(f), e.id(c[i]))) {
                    m = !0;
                    break
                }
                m || d(f)
            }
        }
        return l !== a ? a : void 0
    }

    function C() {
        var b = this;
        a.each(arguments, function (a, c) {
            b[c].remove(), b[c] = null
        })
    }

    function D(b, c) {
        var d = function () {
        };
        return d.prototype = new b, d.prototype.constructor = d, d.prototype.parent = b.prototype, d.prototype = a.extend(d.prototype, c), d
    }

    if (window.Select2 === b) {
        var E, F, G, H, I, J, K, L = {x: 0, y: 0}, M = {
            TAB: 9,
            ENTER: 13,
            ESC: 27,
            SPACE: 32,
            LEFT: 37,
            UP: 38,
            RIGHT: 39,
            DOWN: 40,
            SHIFT: 16,
            CTRL: 17,
            ALT: 18,
            PAGE_UP: 33,
            PAGE_DOWN: 34,
            HOME: 36,
            END: 35,
            BACKSPACE: 8,
            DELETE: 46,
            isArrow: function (a) {
                switch (a = a.which ? a.which : a) {
                    case M.LEFT:
                    case M.RIGHT:
                    case M.UP:
                    case M.DOWN:
                        return !0
                }
                return !1
            },
            isControl: function (a) {
                var b = a.which;
                switch (b) {
                    case M.SHIFT:
                    case M.CTRL:
                    case M.ALT:
                        return !0
                }
                return a.metaKey ? !0 : !1
            },
            isFunctionKey: function (a) {
                return a = a.which ? a.which : a, a >= 112 && 123 >= a
            }
        }, N = "<div class='select2-measure-scrollbar'></div>", O = {
            "Ⓐ": "A",
            "Ａ": "A",
            "À": "A",
            "Á": "A",
            "Â": "A",
            "Ầ": "A",
            "Ấ": "A",
            "Ẫ": "A",
            "Ẩ": "A",
            "Ã": "A",
            "Ā": "A",
            "Ă": "A",
            "Ằ": "A",
            "Ắ": "A",
            "Ẵ": "A",
            "Ẳ": "A",
            "Ȧ": "A",
            "Ǡ": "A",
            "Ä": "A",
            "Ǟ": "A",
            "Ả": "A",
            "Å": "A",
            "Ǻ": "A",
            "Ǎ": "A",
            "Ȁ": "A",
            "Ȃ": "A",
            "Ạ": "A",
            "Ậ": "A",
            "Ặ": "A",
            "Ḁ": "A",
            "Ą": "A",
            "Ⱥ": "A",
            "Ɐ": "A",
            "Ꜳ": "AA",
            "Æ": "AE",
            "Ǽ": "AE",
            "Ǣ": "AE",
            "Ꜵ": "AO",
            "Ꜷ": "AU",
            "Ꜹ": "AV",
            "Ꜻ": "AV",
            "Ꜽ": "AY",
            "Ⓑ": "B",
            "Ｂ": "B",
            "Ḃ": "B",
            "Ḅ": "B",
            "Ḇ": "B",
            "Ƀ": "B",
            "Ƃ": "B",
            "Ɓ": "B",
            "Ⓒ": "C",
            "Ｃ": "C",
            "Ć": "C",
            "Ĉ": "C",
            "Ċ": "C",
            "Č": "C",
            "Ç": "C",
            "Ḉ": "C",
            "Ƈ": "C",
            "Ȼ": "C",
            "Ꜿ": "C",
            "Ⓓ": "D",
            "Ｄ": "D",
            "Ḋ": "D",
            "Ď": "D",
            "Ḍ": "D",
            "Ḑ": "D",
            "Ḓ": "D",
            "Ḏ": "D",
            "Đ": "D",
            "Ƌ": "D",
            "Ɗ": "D",
            "Ɖ": "D",
            "Ꝺ": "D",
            "Ǳ": "DZ",
            "Ǆ": "DZ",
            "ǲ": "Dz",
            "ǅ": "Dz",
            "Ⓔ": "E",
            "Ｅ": "E",
            "È": "E",
            "É": "E",
            "Ê": "E",
            "Ề": "E",
            "Ế": "E",
            "Ễ": "E",
            "Ể": "E",
            "Ẽ": "E",
            "Ē": "E",
            "Ḕ": "E",
            "Ḗ": "E",
            "Ĕ": "E",
            "Ė": "E",
            "Ë": "E",
            "Ẻ": "E",
            "Ě": "E",
            "Ȅ": "E",
            "Ȇ": "E",
            "Ẹ": "E",
            "Ệ": "E",
            "Ȩ": "E",
            "Ḝ": "E",
            "Ę": "E",
            "Ḙ": "E",
            "Ḛ": "E",
            "Ɛ": "E",
            "Ǝ": "E",
            "Ⓕ": "F",
            "Ｆ": "F",
            "Ḟ": "F",
            "Ƒ": "F",
            "Ꝼ": "F",
            "Ⓖ": "G",
            "Ｇ": "G",
            "Ǵ": "G",
            "Ĝ": "G",
            "Ḡ": "G",
            "Ğ": "G",
            "Ġ": "G",
            "Ǧ": "G",
            "Ģ": "G",
            "Ǥ": "G",
            "Ɠ": "G",
            "Ꞡ": "G",
            "Ᵹ": "G",
            "Ꝿ": "G",
            "Ⓗ": "H",
            "Ｈ": "H",
            "Ĥ": "H",
            "Ḣ": "H",
            "Ḧ": "H",
            "Ȟ": "H",
            "Ḥ": "H",
            "Ḩ": "H",
            "Ḫ": "H",
            "Ħ": "H",
            "Ⱨ": "H",
            "Ⱶ": "H",
            "Ɥ": "H",
            "Ⓘ": "I",
            "Ｉ": "I",
            "Ì": "I",
            "Í": "I",
            "Î": "I",
            "Ĩ": "I",
            "Ī": "I",
            "Ĭ": "I",
            "İ": "I",
            "Ï": "I",
            "Ḯ": "I",
            "Ỉ": "I",
            "Ǐ": "I",
            "Ȉ": "I",
            "Ȋ": "I",
            "Ị": "I",
            "Į": "I",
            "Ḭ": "I",
            "Ɨ": "I",
            "Ⓙ": "J",
            "Ｊ": "J",
            "Ĵ": "J",
            "Ɉ": "J",
            "Ⓚ": "K",
            "Ｋ": "K",
            "Ḱ": "K",
            "Ǩ": "K",
            "Ḳ": "K",
            "Ķ": "K",
            "Ḵ": "K",
            "Ƙ": "K",
            "Ⱪ": "K",
            "Ꝁ": "K",
            "Ꝃ": "K",
            "Ꝅ": "K",
            "Ꞣ": "K",
            "Ⓛ": "L",
            "Ｌ": "L",
            "Ŀ": "L",
            "Ĺ": "L",
            "Ľ": "L",
            "Ḷ": "L",
            "Ḹ": "L",
            "Ļ": "L",
            "Ḽ": "L",
            "Ḻ": "L",
            "Ł": "L",
            "Ƚ": "L",
            "Ɫ": "L",
            "Ⱡ": "L",
            "Ꝉ": "L",
            "Ꝇ": "L",
            "Ꞁ": "L",
            "Ǉ": "LJ",
            "ǈ": "Lj",
            "Ⓜ": "M",
            "Ｍ": "M",
            "Ḿ": "M",
            "Ṁ": "M",
            "Ṃ": "M",
            "Ɱ": "M",
            "Ɯ": "M",
            "Ⓝ": "N",
            "Ｎ": "N",
            "Ǹ": "N",
            "Ń": "N",
            "Ñ": "N",
            "Ṅ": "N",
            "Ň": "N",
            "Ṇ": "N",
            "Ņ": "N",
            "Ṋ": "N",
            "Ṉ": "N",
            "Ƞ": "N",
            "Ɲ": "N",
            "Ꞑ": "N",
            "Ꞥ": "N",
            "Ǌ": "NJ",
            "ǋ": "Nj",
            "Ⓞ": "O",
            "Ｏ": "O",
            "Ò": "O",
            "Ó": "O",
            "Ô": "O",
            "Ồ": "O",
            "Ố": "O",
            "Ỗ": "O",
            "Ổ": "O",
            "Õ": "O",
            "Ṍ": "O",
            "Ȭ": "O",
            "Ṏ": "O",
            "Ō": "O",
            "Ṑ": "O",
            "Ṓ": "O",
            "Ŏ": "O",
            "Ȯ": "O",
            "Ȱ": "O",
            "Ö": "O",
            "Ȫ": "O",
            "Ỏ": "O",
            "Ő": "O",
            "Ǒ": "O",
            "Ȍ": "O",
            "Ȏ": "O",
            "Ơ": "O",
            "Ờ": "O",
            "Ớ": "O",
            "Ỡ": "O",
            "Ở": "O",
            "Ợ": "O",
            "Ọ": "O",
            "Ộ": "O",
            "Ǫ": "O",
            "Ǭ": "O",
            "Ø": "O",
            "Ǿ": "O",
            "Ɔ": "O",
            "Ɵ": "O",
            "Ꝋ": "O",
            "Ꝍ": "O",
            "Ƣ": "OI",
            "Ꝏ": "OO",
            "Ȣ": "OU",
            "Ⓟ": "P",
            "Ｐ": "P",
            "Ṕ": "P",
            "Ṗ": "P",
            "Ƥ": "P",
            "Ᵽ": "P",
            "Ꝑ": "P",
            "Ꝓ": "P",
            "Ꝕ": "P",
            "Ⓠ": "Q",
            "Ｑ": "Q",
            "Ꝗ": "Q",
            "Ꝙ": "Q",
            "Ɋ": "Q",
            "Ⓡ": "R",
            "Ｒ": "R",
            "Ŕ": "R",
            "Ṙ": "R",
            "Ř": "R",
            "Ȑ": "R",
            "Ȓ": "R",
            "Ṛ": "R",
            "Ṝ": "R",
            "Ŗ": "R",
            "Ṟ": "R",
            "Ɍ": "R",
            "Ɽ": "R",
            "Ꝛ": "R",
            "Ꞧ": "R",
            "Ꞃ": "R",
            "Ⓢ": "S",
            "Ｓ": "S",
            "ẞ": "S",
            "Ś": "S",
            "Ṥ": "S",
            "Ŝ": "S",
            "Ṡ": "S",
            "Š": "S",
            "Ṧ": "S",
            "Ṣ": "S",
            "Ṩ": "S",
            "Ș": "S",
            "Ş": "S",
            "Ȿ": "S",
            "Ꞩ": "S",
            "Ꞅ": "S",
            "Ⓣ": "T",
            "Ｔ": "T",
            "Ṫ": "T",
            "Ť": "T",
            "Ṭ": "T",
            "Ț": "T",
            "Ţ": "T",
            "Ṱ": "T",
            "Ṯ": "T",
            "Ŧ": "T",
            "Ƭ": "T",
            "Ʈ": "T",
            "Ⱦ": "T",
            "Ꞇ": "T",
            "Ꜩ": "TZ",
            "Ⓤ": "U",
            "Ｕ": "U",
            "Ù": "U",
            "Ú": "U",
            "Û": "U",
            "Ũ": "U",
            "Ṹ": "U",
            "Ū": "U",
            "Ṻ": "U",
            "Ŭ": "U",
            "Ü": "U",
            "Ǜ": "U",
            "Ǘ": "U",
            "Ǖ": "U",
            "Ǚ": "U",
            "Ủ": "U",
            "Ů": "U",
            "Ű": "U",
            "Ǔ": "U",
            "Ȕ": "U",
            "Ȗ": "U",
            "Ư": "U",
            "Ừ": "U",
            "Ứ": "U",
            "Ữ": "U",
            "Ử": "U",
            "Ự": "U",
            "Ụ": "U",
            "Ṳ": "U",
            "Ų": "U",
            "Ṷ": "U",
            "Ṵ": "U",
            "Ʉ": "U",
            "Ⓥ": "V",
            "Ｖ": "V",
            "Ṽ": "V",
            "Ṿ": "V",
            "Ʋ": "V",
            "Ꝟ": "V",
            "Ʌ": "V",
            "Ꝡ": "VY",
            "Ⓦ": "W",
            "Ｗ": "W",
            "Ẁ": "W",
            "Ẃ": "W",
            "Ŵ": "W",
            "Ẇ": "W",
            "Ẅ": "W",
            "Ẉ": "W",
            "Ⱳ": "W",
            "Ⓧ": "X",
            "Ｘ": "X",
            "Ẋ": "X",
            "Ẍ": "X",
            "Ⓨ": "Y",
            "Ｙ": "Y",
            "Ỳ": "Y",
            "Ý": "Y",
            "Ŷ": "Y",
            "Ỹ": "Y",
            "Ȳ": "Y",
            "Ẏ": "Y",
            "Ÿ": "Y",
            "Ỷ": "Y",
            "Ỵ": "Y",
            "Ƴ": "Y",
            "Ɏ": "Y",
            "Ỿ": "Y",
            "Ⓩ": "Z",
            "Ｚ": "Z",
            "Ź": "Z",
            "Ẑ": "Z",
            "Ż": "Z",
            "Ž": "Z",
            "Ẓ": "Z",
            "Ẕ": "Z",
            "Ƶ": "Z",
            "Ȥ": "Z",
            "Ɀ": "Z",
            "Ⱬ": "Z",
            "Ꝣ": "Z",
            "ⓐ": "a",
            "ａ": "a",
            "ẚ": "a",
            "à": "a",
            "á": "a",
            "â": "a",
            "ầ": "a",
            "ấ": "a",
            "ẫ": "a",
            "ẩ": "a",
            "ã": "a",
            "ā": "a",
            "ă": "a",
            "ằ": "a",
            "ắ": "a",
            "ẵ": "a",
            "ẳ": "a",
            "ȧ": "a",
            "ǡ": "a",
            "ä": "a",
            "ǟ": "a",
            "ả": "a",
            "å": "a",
            "ǻ": "a",
            "ǎ": "a",
            "ȁ": "a",
            "ȃ": "a",
            "ạ": "a",
            "ậ": "a",
            "ặ": "a",
            "ḁ": "a",
            "ą": "a",
            "ⱥ": "a",
            "ɐ": "a",
            "ꜳ": "aa",
            "æ": "ae",
            "ǽ": "ae",
            "ǣ": "ae",
            "ꜵ": "ao",
            "ꜷ": "au",
            "ꜹ": "av",
            "ꜻ": "av",
            "ꜽ": "ay",
            "ⓑ": "b",
            "ｂ": "b",
            "ḃ": "b",
            "ḅ": "b",
            "ḇ": "b",
            "ƀ": "b",
            "ƃ": "b",
            "ɓ": "b",
            "ⓒ": "c",
            "ｃ": "c",
            "ć": "c",
            "ĉ": "c",
            "ċ": "c",
            "č": "c",
            "ç": "c",
            "ḉ": "c",
            "ƈ": "c",
            "ȼ": "c",
            "ꜿ": "c",
            "ↄ": "c",
            "ⓓ": "d",
            "ｄ": "d",
            "ḋ": "d",
            "ď": "d",
            "ḍ": "d",
            "ḑ": "d",
            "ḓ": "d",
            "ḏ": "d",
            "đ": "d",
            "ƌ": "d",
            "ɖ": "d",
            "ɗ": "d",
            "ꝺ": "d",
            "ǳ": "dz",
            "ǆ": "dz",
            "ⓔ": "e",
            "ｅ": "e",
            "è": "e",
            "é": "e",
            "ê": "e",
            "ề": "e",
            "ế": "e",
            "ễ": "e",
            "ể": "e",
            "ẽ": "e",
            "ē": "e",
            "ḕ": "e",
            "ḗ": "e",
            "ĕ": "e",
            "ė": "e",
            "ë": "e",
            "ẻ": "e",
            "ě": "e",
            "ȅ": "e",
            "ȇ": "e",
            "ẹ": "e",
            "ệ": "e",
            "ȩ": "e",
            "ḝ": "e",
            "ę": "e",
            "ḙ": "e",
            "ḛ": "e",
            "ɇ": "e",
            "ɛ": "e",
            "ǝ": "e",
            "ⓕ": "f",
            "ｆ": "f",
            "ḟ": "f",
            "ƒ": "f",
            "ꝼ": "f",
            "ⓖ": "g",
            "ｇ": "g",
            "ǵ": "g",
            "ĝ": "g",
            "ḡ": "g",
            "ğ": "g",
            "ġ": "g",
            "ǧ": "g",
            "ģ": "g",
            "ǥ": "g",
            "ɠ": "g",
            "ꞡ": "g",
            "ᵹ": "g",
            "ꝿ": "g",
            "ⓗ": "h",
            "ｈ": "h",
            "ĥ": "h",
            "ḣ": "h",
            "ḧ": "h",
            "ȟ": "h",
            "ḥ": "h",
            "ḩ": "h",
            "ḫ": "h",
            "ẖ": "h",
            "ħ": "h",
            "ⱨ": "h",
            "ⱶ": "h",
            "ɥ": "h",
            "ƕ": "hv",
            "ⓘ": "i",
            "ｉ": "i",
            "ì": "i",
            "í": "i",
            "î": "i",
            "ĩ": "i",
            "ī": "i",
            "ĭ": "i",
            "ï": "i",
            "ḯ": "i",
            "ỉ": "i",
            "ǐ": "i",
            "ȉ": "i",
            "ȋ": "i",
            "ị": "i",
            "į": "i",
            "ḭ": "i",
            "ɨ": "i",
            "ı": "i",
            "ⓙ": "j",
            "ｊ": "j",
            "ĵ": "j",
            "ǰ": "j",
            "ɉ": "j",
            "ⓚ": "k",
            "ｋ": "k",
            "ḱ": "k",
            "ǩ": "k",
            "ḳ": "k",
            "ķ": "k",
            "ḵ": "k",
            "ƙ": "k",
            "ⱪ": "k",
            "ꝁ": "k",
            "ꝃ": "k",
            "ꝅ": "k",
            "ꞣ": "k",
            "ⓛ": "l",
            "ｌ": "l",
            "ŀ": "l",
            "ĺ": "l",
            "ľ": "l",
            "ḷ": "l",
            "ḹ": "l",
            "ļ": "l",
            "ḽ": "l",
            "ḻ": "l",
            "ſ": "l",
            "ł": "l",
            "ƚ": "l",
            "ɫ": "l",
            "ⱡ": "l",
            "ꝉ": "l",
            "ꞁ": "l",
            "ꝇ": "l",
            "ǉ": "lj",
            "ⓜ": "m",
            "ｍ": "m",
            "ḿ": "m",
            "ṁ": "m",
            "ṃ": "m",
            "ɱ": "m",
            "ɯ": "m",
            "ⓝ": "n",
            "ｎ": "n",
            "ǹ": "n",
            "ń": "n",
            "ñ": "n",
            "ṅ": "n",
            "ň": "n",
            "ṇ": "n",
            "ņ": "n",
            "ṋ": "n",
            "ṉ": "n",
            "ƞ": "n",
            "ɲ": "n",
            "ŉ": "n",
            "ꞑ": "n",
            "ꞥ": "n",
            "ǌ": "nj",
            "ⓞ": "o",
            "ｏ": "o",
            "ò": "o",
            "ó": "o",
            "ô": "o",
            "ồ": "o",
            "ố": "o",
            "ỗ": "o",
            "ổ": "o",
            "õ": "o",
            "ṍ": "o",
            "ȭ": "o",
            "ṏ": "o",
            "ō": "o",
            "ṑ": "o",
            "ṓ": "o",
            "ŏ": "o",
            "ȯ": "o",
            "ȱ": "o",
            "ö": "o",
            "ȫ": "o",
            "ỏ": "o",
            "ő": "o",
            "ǒ": "o",
            "ȍ": "o",
            "ȏ": "o",
            "ơ": "o",
            "ờ": "o",
            "ớ": "o",
            "ỡ": "o",
            "ở": "o",
            "ợ": "o",
            "ọ": "o",
            "ộ": "o",
            "ǫ": "o",
            "ǭ": "o",
            "ø": "o",
            "ǿ": "o",
            "ɔ": "o",
            "ꝋ": "o",
            "ꝍ": "o",
            "ɵ": "o",
            "ƣ": "oi",
            "ȣ": "ou",
            "ꝏ": "oo",
            "ⓟ": "p",
            "ｐ": "p",
            "ṕ": "p",
            "ṗ": "p",
            "ƥ": "p",
            "ᵽ": "p",
            "ꝑ": "p",
            "ꝓ": "p",
            "ꝕ": "p",
            "ⓠ": "q",
            "ｑ": "q",
            "ɋ": "q",
            "ꝗ": "q",
            "ꝙ": "q",
            "ⓡ": "r",
            "ｒ": "r",
            "ŕ": "r",
            "ṙ": "r",
            "ř": "r",
            "ȑ": "r",
            "ȓ": "r",
            "ṛ": "r",
            "ṝ": "r",
            "ŗ": "r",
            "ṟ": "r",
            "ɍ": "r",
            "ɽ": "r",
            "ꝛ": "r",
            "ꞧ": "r",
            "ꞃ": "r",
            "ⓢ": "s",
            "ｓ": "s",
            "ß": "s",
            "ś": "s",
            "ṥ": "s",
            "ŝ": "s",
            "ṡ": "s",
            "š": "s",
            "ṧ": "s",
            "ṣ": "s",
            "ṩ": "s",
            "ș": "s",
            "ş": "s",
            "ȿ": "s",
            "ꞩ": "s",
            "ꞅ": "s",
            "ẛ": "s",
            "ⓣ": "t",
            "ｔ": "t",
            "ṫ": "t",
            "ẗ": "t",
            "ť": "t",
            "ṭ": "t",
            "ț": "t",
            "ţ": "t",
            "ṱ": "t",
            "ṯ": "t",
            "ŧ": "t",
            "ƭ": "t",
            "ʈ": "t",
            "ⱦ": "t",
            "ꞇ": "t",
            "ꜩ": "tz",
            "ⓤ": "u",
            "ｕ": "u",
            "ù": "u",
            "ú": "u",
            "û": "u",
            "ũ": "u",
            "ṹ": "u",
            "ū": "u",
            "ṻ": "u",
            "ŭ": "u",
            "ü": "u",
            "ǜ": "u",
            "ǘ": "u",
            "ǖ": "u",
            "ǚ": "u",
            "ủ": "u",
            "ů": "u",
            "ű": "u",
            "ǔ": "u",
            "ȕ": "u",
            "ȗ": "u",
            "ư": "u",
            "ừ": "u",
            "ứ": "u",
            "ữ": "u",
            "ử": "u",
            "ự": "u",
            "ụ": "u",
            "ṳ": "u",
            "ų": "u",
            "ṷ": "u",
            "ṵ": "u",
            "ʉ": "u",
            "ⓥ": "v",
            "ｖ": "v",
            "ṽ": "v",
            "ṿ": "v",
            "ʋ": "v",
            "ꝟ": "v",
            "ʌ": "v",
            "ꝡ": "vy",
            "ⓦ": "w",
            "ｗ": "w",
            "ẁ": "w",
            "ẃ": "w",
            "ŵ": "w",
            "ẇ": "w",
            "ẅ": "w",
            "ẘ": "w",
            "ẉ": "w",
            "ⱳ": "w",
            "ⓧ": "x",
            "ｘ": "x",
            "ẋ": "x",
            "ẍ": "x",
            "ⓨ": "y",
            "ｙ": "y",
            "ỳ": "y",
            "ý": "y",
            "ŷ": "y",
            "ỹ": "y",
            "ȳ": "y",
            "ẏ": "y",
            "ÿ": "y",
            "ỷ": "y",
            "ẙ": "y",
            "ỵ": "y",
            "ƴ": "y",
            "ɏ": "y",
            "ỿ": "y",
            "ⓩ": "z",
            "ｚ": "z",
            "ź": "z",
            "ẑ": "z",
            "ż": "z",
            "ž": "z",
            "ẓ": "z",
            "ẕ": "z",
            "ƶ": "z",
            "ȥ": "z",
            "ɀ": "z",
            "ⱬ": "z",
            "ꝣ": "z",
            "Ά": "Α",
            "Έ": "Ε",
            "Ή": "Η",
            "Ί": "Ι",
            "Ϊ": "Ι",
            "Ό": "Ο",
            "Ύ": "Υ",
            "Ϋ": "Υ",
            "Ώ": "Ω",
            "ά": "α",
            "έ": "ε",
            "ή": "η",
            "ί": "ι",
            "ϊ": "ι",
            "ΐ": "ι",
            "ό": "ο",
            "ύ": "υ",
            "ϋ": "υ",
            "ΰ": "υ",
            "ω": "ω",
            "ς": "σ"
        };
        J = a(document), H = function () {
            var a = 1;
            return function () {
                return a++
            }
        }(), E = D(Object, {
            bind: function (a) {
                var b = this;
                return function () {
                    a.apply(b, arguments)
                }
            }, init: function (c) {
                var d, e, g = ".select2-results";
                this.opts = c = this.prepareOpts(c), this.id = c.id, c.element.data("select2") !== b && null !== c.element.data("select2") && c.element.data("select2").destroy(), this.container = this.createContainer(), this.liveRegion = a(".select2-hidden-accessible"), 0 == this.liveRegion.length && (this.liveRegion = a("<span>", {
                    role: "status",
                    "aria-live": "polite"
                }).addClass("select2-hidden-accessible").appendTo(document.body)), this.containerId = "s2id_" + (c.element.attr("id") || "autogen" + H()), this.containerEventName = this.containerId.replace(/([.])/g, "_").replace(/([;&,\-\.\+\*\~':"\!\^#$%@\[\]\(\)=>\|])/g, "\\$1"), this.container.attr("id", this.containerId), this.container.attr("title", c.element.attr("title")), this.body = a(document.body), s(this.container, this.opts.element, this.opts.adaptContainerCssClass), this.container.attr("style", c.element.attr("style")), this.container.css(z(c.containerCss, this.opts.element)), this.container.addClass(z(c.containerCssClass, this.opts.element)), this.elementTabIndex = this.opts.element.attr("tabindex"), this.opts.element.data("select2", this).attr("tabindex", "-1").before(this.container).on("click.select2", p), this.container.data("select2", this), this.dropdown = this.container.find(".select2-drop"), s(this.dropdown, this.opts.element, this.opts.adaptDropdownCssClass), this.dropdown.addClass(z(c.dropdownCssClass, this.opts.element)), this.dropdown.data("select2", this), this.dropdown.on("click", p), this.results = d = this.container.find(g), this.search = e = this.container.find("input.select2-input"), this.queryCount = 0, this.resultsPage = 0, this.context = null, this.initContainer(), this.container.on("click", p), k(this.results), this.dropdown.on("mousemove-filtered", g, this.bind(this.highlightUnderEvent)), this.dropdown.on("touchstart touchmove touchend", g, this.bind(function (a) {
                    this._touchEvent = !0, this.highlightUnderEvent(a)
                })), this.dropdown.on("touchmove", g, this.bind(this.touchMoved)), this.dropdown.on("touchstart touchend", g, this.bind(this.clearTouchMoved)), this.dropdown.on("click", this.bind(function () {
                    this._touchEvent && (this._touchEvent = !1, this.selectHighlighted())
                })), m(80, this.results), this.dropdown.on("scroll-debounced", g, this.bind(this.loadMoreIfNeeded)), a(this.container).on("change", ".select2-input", function (a) {
                    a.stopPropagation()
                }), a(this.dropdown).on("change", ".select2-input", function (a) {
                    a.stopPropagation()
                }), a.fn.mousewheel && d.mousewheel(function (a, b, c, e) {
                    var f = d.scrollTop();
                    e > 0 && 0 >= f - e ? (d.scrollTop(0), p(a)) : 0 > e && d.get(0).scrollHeight - d.scrollTop() + e <= d.height() && (d.scrollTop(d.get(0).scrollHeight - d.height()), p(a))
                }), j(e), e.on("keyup-change input paste", this.bind(this.updateResults)), e.on("focus", function () {
                    e.addClass("select2-focused")
                }), e.on("blur", function () {
                    e.removeClass("select2-focused")
                }), this.dropdown.on("mouseup", g, this.bind(function (b) {
                    a(b.target).closest(".select2-result-selectable").length > 0 && (this.highlightUnderEvent(b), this.selectHighlighted(b))
                })), this.dropdown.on("click mouseup mousedown touchstart touchend focusin", function (a) {
                    a.stopPropagation()
                }), this.nextSearchTerm = b, a.isFunction(this.opts.initSelection) && (this.initSelection(), this.monitorSource()), null !== c.maximumInputLength && this.search.attr("maxlength", c.maximumInputLength);
                var h = c.element.prop("disabled");
                h === b && (h = !1), this.enable(!h);
                var i = c.element.prop("readonly");
                i === b && (i = !1), this.readonly(i), K = K || f(), this.autofocus = c.element.prop("autofocus"), c.element.prop("autofocus", !1), this.autofocus && this.focus(), this.search.attr("placeholder", c.searchInputPlaceholder)
            }, destroy: function () {
                var a = this.opts.element, c = a.data("select2"), d = this;
                this.close(), a.length && a[0].detachEvent && d._sync && a.each(function () {
                    d._sync && this.detachEvent("onpropertychange", d._sync)
                }), this.propertyObserver && (this.propertyObserver.disconnect(), this.propertyObserver = null), this._sync = null, c !== b && (c.container.remove(), c.liveRegion.remove(), c.dropdown.remove(), a.show().removeData("select2").off(".select2").prop("autofocus", this.autofocus || !1), this.elementTabIndex ? a.attr({tabindex: this.elementTabIndex}) : a.removeAttr("tabindex"), a.show()), C.call(this, "container", "liveRegion", "dropdown", "results", "search")
            }, optionToData: function (a) {
                return a.is("option") ? {
                    id: a.prop("value"),
                    text: a.text(),
                    element: a.get(),
                    css: a.attr("class"),
                    disabled: a.prop("disabled"),
                    locked: g(a.attr("locked"), "locked") || g(a.data("locked"), !0)
                } : a.is("optgroup") ? {
                    text: a.attr("label"),
                    children: [],
                    element: a.get(),
                    css: a.attr("class")
                } : void 0
            }, prepareOpts: function (c) {
                var d, e, f, i, j = this;
                if (d = c.element, "select" === d.get(0).tagName.toLowerCase() && (this.select = e = c.element), e && a.each(["id", "multiple", "ajax", "query", "createSearchChoice", "initSelection", "data", "tags"], function () {
                        if (this in c)throw new Error("Option '" + this + "' is not allowed for Select2 when attached to a <select> element.")
                    }), c = a.extend({}, {
                        populateResults: function (d, e, f) {
                            var g, h = this.opts.id, i = this.liveRegion;
                            (g = function (d, e, k) {
                                var l, m, n, o, p, q, r, s, t, u;
                                d = c.sortResults(d, e, f);
                                var v = [];
                                for (l = 0, m = d.length; m > l; l += 1)n = d[l], p = n.disabled === !0, o = !p && h(n) !== b, q = n.children && n.children.length > 0, r = a("<li></li>"), r.addClass("select2-results-dept-" + k), r.addClass("select2-result"), r.addClass(o ? "select2-result-selectable" : "select2-result-unselectable"), p && r.addClass("select2-disabled"), q && r.addClass("select2-result-with-children"), r.addClass(j.opts.formatResultCssClass(n)), r.attr("role", "presentation"), s = a(document.createElement("div")), s.addClass("select2-result-label"), s.attr("id", "select2-result-label-" + H()), s.attr("role", "option"), u = c.formatResult(n, s, f, j.opts.escapeMarkup), u !== b && (s.html(u), r.append(s)), q && (t = a("<ul></ul>"), t.addClass("select2-result-sub"), g(n.children, t, k + 1), r.append(t)), r.data("select2-data", n), v.push(r[0]);
                                e.append(v), i.text(c.formatMatches(d.length))
                            })(e, d, 0)
                        }
                    }, a.fn.select2.defaults, c), "function" != typeof c.id && (f = c.id, c.id = function (a) {
                        return a[f]
                    }), a.isArray(c.element.data("select2Tags"))) {
                    if ("tags"in c)throw"tags specified as both an attribute 'data-select2-tags' and in options of Select2 " + c.element.attr("id");
                    c.tags = c.element.data("select2Tags")
                }
                if (e ? (c.query = this.bind(function (a) {
                        var c, e, f, g = {results: [], more: !1}, h = a.term;
                        f = function (b, c) {
                            var d;
                            b.is("option") ? a.matcher(h, b.text(), b) && c.push(j.optionToData(b)) : b.is("optgroup") && (d = j.optionToData(b), b.children().each2(function (a, b) {
                                f(b, d.children)
                            }), d.children.length > 0 && c.push(d))
                        }, c = d.children(), this.getPlaceholder() !== b && c.length > 0 && (e = this.getPlaceholderOption(), e && (c = c.not(e))), c.each2(function (a, b) {
                            f(b, g.results)
                        }), a.callback(g)
                    }), c.id = function (a) {
                        return a.id
                    }) : "query"in c || ("ajax"in c ? (i = c.element.data("ajax-url"), i && i.length > 0 && (c.ajax.url = i), c.query = v.call(c.element, c.ajax)) : "data"in c ? c.query = w(c.data) : "tags"in c && (c.query = x(c.tags), c.createSearchChoice === b && (c.createSearchChoice = function (b) {
                        return {id: a.trim(b), text: a.trim(b)}
                    }), c.initSelection === b && (c.initSelection = function (b, d) {
                        var e = [];
                        a(h(b.val(), c.separator, c.transformVal)).each(function () {
                            var b = {id: this, text: this}, d = c.tags;
                            a.isFunction(d) && (d = d()), a(d).each(function () {
                                return g(this.id, b.id) ? (b = this, !1) : void 0
                            }), e.push(b)
                        }), d(e)
                    }))), "function" != typeof c.query)throw"query function not defined for Select2 " + c.element.attr("id");
                if ("top" === c.createSearchChoicePosition)c.createSearchChoicePosition = function (a, b) {
                    a.unshift(b)
                }; else if ("bottom" === c.createSearchChoicePosition)c.createSearchChoicePosition = function (a, b) {
                    a.push(b)
                }; else if ("function" != typeof c.createSearchChoicePosition)throw"invalid createSearchChoicePosition option must be 'top', 'bottom' or a custom function";
                return c
            }, monitorSource: function () {
                var c, d = this.opts.element, e = this;
                d.on("change.select2", this.bind(function () {
                    this.opts.element.data("select2-change-triggered") !== !0 && this.initSelection()
                })), this._sync = this.bind(function () {
                    var a = d.prop("disabled");
                    a === b && (a = !1), this.enable(!a);
                    var c = d.prop("readonly");
                    c === b && (c = !1), this.readonly(c), this.container && (s(this.container, this.opts.element, this.opts.adaptContainerCssClass), this.container.addClass(z(this.opts.containerCssClass, this.opts.element))), this.dropdown && (s(this.dropdown, this.opts.element, this.opts.adaptDropdownCssClass), this.dropdown.addClass(z(this.opts.dropdownCssClass, this.opts.element)))
                }), d.length && d[0].attachEvent && d.each(function () {
                    this.attachEvent("onpropertychange", e._sync)
                }), c = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver, c !== b && (this.propertyObserver && (delete this.propertyObserver, this.propertyObserver = null), this.propertyObserver = new c(function (b) {
                    a.each(b, e._sync)
                }), this.propertyObserver.observe(d.get(0), {attributes: !0, subtree: !1}))
            }, triggerSelect: function (b) {
                var c = a.Event("select2-selecting", {val: this.id(b), object: b, choice: b});
                return this.opts.element.trigger(c), !c.isDefaultPrevented()
            }, triggerChange: function (b) {
                b = b || {}, b = a.extend({}, b, {
                    type: "change",
                    val: this.val()
                }), this.opts.element.data("select2-change-triggered", !0), this.opts.element.trigger(b), this.opts.element.data("select2-change-triggered", !1), this.opts.element.click(), this.opts.blurOnChange && this.opts.element.blur()
            }, isInterfaceEnabled: function () {
                return this.enabledInterface === !0
            }, enableInterface: function () {
                var a = this._enabled && !this._readonly, b = !a;
                return a === this.enabledInterface ? !1 : (this.container.toggleClass("select2-container-disabled", b), this.close(), this.enabledInterface = a, !0)
            }, enable: function (a) {
                a === b && (a = !0), this._enabled !== a && (this._enabled = a, this.opts.element.prop("disabled", !a), this.enableInterface())
            }, disable: function () {
                this.enable(!1)
            }, readonly: function (a) {
                a === b && (a = !1), this._readonly !== a && (this._readonly = a, this.opts.element.prop("readonly", a), this.enableInterface())
            }, opened: function () {
                return this.container ? this.container.hasClass("select2-dropdown-open") : !1
            }, positionDropdown: function () {
                var b, c, d, e, f, g = this.dropdown, h = this.container, i = h.offset(), j = h.outerHeight(!1), k = h.outerWidth(!1), l = g.outerHeight(!1), m = a(window), n = m.width(), o = m.height(), p = m.scrollLeft() + n, q = m.scrollTop() + o, r = i.top + j, s = i.left, t = q >= r + l, u = i.top - l >= m.scrollTop(), v = g.outerWidth(!1), w = function () {
                    return p >= s + v
                }, x = function () {
                    return i.left + p + h.outerWidth(!1) > v
                }, y = g.hasClass("select2-drop-above");
                y ? (c = !0, !u && t && (d = !0, c = !1)) : (c = !1, !t && u && (d = !0, c = !0)), d && (g.hide(), i = this.container.offset(), j = this.container.outerHeight(!1), k = this.container.outerWidth(!1), l = g.outerHeight(!1), p = m.scrollLeft() + n, q = m.scrollTop() + o, r = i.top + j, s = i.left, v = g.outerWidth(!1), g.show(), this.focusSearch()), this.opts.dropdownAutoWidth ? (f = a(".select2-results", g)[0], g.addClass("select2-drop-auto-width"), g.css("width", ""), v = g.outerWidth(!1) + (f.scrollHeight === f.clientHeight ? 0 : K.width), v > k ? k = v : v = k, l = g.outerHeight(!1)) : this.container.removeClass("select2-drop-auto-width"), "static" !== this.body.css("position") && (b = this.body.offset(), r -= b.top, s -= b.left), !w() && x() && (s = i.left + this.container.outerWidth(!1) - v), e = {
                    left: s,
                    width: k
                }, c ? (e.top = i.top - l, e.bottom = "auto", this.container.addClass("select2-drop-above"), g.addClass("select2-drop-above")) : (e.top = r, e.bottom = "auto", this.container.removeClass("select2-drop-above"), g.removeClass("select2-drop-above")), e = a.extend(e, z(this.opts.dropdownCss, this.opts.element)), g.css(e)
            }, shouldOpen: function () {
                var b;
                return this.opened() ? !1 : this._enabled === !1 || this._readonly === !0 ? !1 : (b = a.Event("select2-opening"), this.opts.element.trigger(b), !b.isDefaultPrevented())
            }, clearDropdownAlignmentPreference: function () {
                this.container.removeClass("select2-drop-above"), this.dropdown.removeClass("select2-drop-above")
            }, open: function () {
                return this.shouldOpen() ? (this.opening(), J.on("mousemove.select2Event", function (a) {
                    L.x = a.pageX, L.y = a.pageY
                }), !0) : !1
            }, opening: function () {
                var b, d = this.containerEventName, e = "scroll." + d, f = "resize." + d, g = "orientationchange." + d;
                this.container.addClass("select2-dropdown-open").addClass("select2-container-active"), this.clearDropdownAlignmentPreference(), this.dropdown[0] !== this.body.children().last()[0] && this.dropdown.detach().appendTo(this.body), b = a("#select2-drop-mask"), 0 === b.length && (b = a(document.createElement("div")), b.attr("id", "select2-drop-mask").attr("class", "select2-drop-mask"), b.hide(), b.appendTo(this.body), b.on("mousedown touchstart click", function (d) {
                    c(b);
                    var e, f = a("#select2-drop");
                    f.length > 0 && (e = f.data("select2"), e.opts.selectOnBlur && e.selectHighlighted({noFocus: !0}), e.close(), d.preventDefault(), d.stopPropagation())
                })), this.dropdown.prev()[0] !== b[0] && this.dropdown.before(b), a("#select2-drop").removeAttr("id"), this.dropdown.attr("id", "select2-drop"), b.show(), this.positionDropdown(), this.dropdown.show(), this.positionDropdown(), this.dropdown.addClass("select2-drop-active");
                var h = this;
                this.container.parents().add(window).each(function () {
                    a(this).on(f + " " + e + " " + g, function () {
                        h.opened() && h.positionDropdown()
                    })
                })
            }, close: function () {
                if (this.opened()) {
                    var b = this.containerEventName, c = "scroll." + b, d = "resize." + b, e = "orientationchange." + b;
                    this.container.parents().add(window).each(function () {
                        a(this).off(c).off(d).off(e)
                    }), this.clearDropdownAlignmentPreference(), a("#select2-drop-mask").hide(), this.dropdown.removeAttr("id"), this.dropdown.hide(), this.container.removeClass("select2-dropdown-open").removeClass("select2-container-active"), this.results.empty(), J.off("mousemove.select2Event"), this.clearSearch(), this.search.removeClass("select2-active"), this.opts.element.trigger(a.Event("select2-close"))
                }
            }, externalSearch: function (a) {
                this.open(), this.search.val(a), this.updateResults(!1)
            }, clearSearch: function () {
            }, getMaximumSelectionSize: function () {
                return z(this.opts.maximumSelectionSize, this.opts.element)
            }, ensureHighlightVisible: function () {
                var b, c, d, e, f, g, h, i, j = this.results;
                if (c = this.highlight(), !(0 > c)) {
                    if (0 == c)return void j.scrollTop(0);
                    b = this.findHighlightableChoices().find(".select2-result-label"), d = a(b[c]), i = (d.offset() || {}).top || 0, e = i + d.outerHeight(!0), c === b.length - 1 && (h = j.find("li.select2-more-results"), h.length > 0 && (e = h.offset().top + h.outerHeight(!0))), f = j.offset().top + j.outerHeight(!1), e > f && j.scrollTop(j.scrollTop() + (e - f)), g = i - j.offset().top, 0 > g && "none" != d.css("display") && j.scrollTop(j.scrollTop() + g)
                }
            }, findHighlightableChoices: function () {
                return this.results.find(".select2-result-selectable:not(.select2-disabled):not(.select2-selected)")
            }, moveHighlight: function (b) {
                for (var c = this.findHighlightableChoices(), d = this.highlight(); d > -1 && d < c.length;) {
                    d += b;
                    var e = a(c[d]);
                    if (e.hasClass("select2-result-selectable") && !e.hasClass("select2-disabled") && !e.hasClass("select2-selected")) {
                        this.highlight(d);
                        break
                    }
                }
            }, highlight: function (b) {
                var c, d, f = this.findHighlightableChoices();
                return 0 === arguments.length ? e(f.filter(".select2-highlighted")[0], f.get()) : (b >= f.length && (b = f.length - 1), 0 > b && (b = 0), this.removeHighlight(), c = a(f[b]), c.addClass("select2-highlighted"), this.search.attr("aria-activedescendant", c.find(".select2-result-label").attr("id")), this.ensureHighlightVisible(), this.liveRegion.text(c.text()), d = c.data("select2-data"), void(d && this.opts.element.trigger({
                    type: "select2-highlight",
                    val: this.id(d),
                    choice: d
                })))
            }, removeHighlight: function () {
                this.results.find(".select2-highlighted").removeClass("select2-highlighted")
            }, touchMoved: function () {
                this._touchMoved = !0
            }, clearTouchMoved: function () {
                this._touchMoved = !1
            }, countSelectableResults: function () {
                return this.findHighlightableChoices().length
            }, highlightUnderEvent: function (b) {
                var c = a(b.target).closest(".select2-result-selectable");
                if (c.length > 0 && !c.is(".select2-highlighted")) {
                    var d = this.findHighlightableChoices();
                    this.highlight(d.index(c))
                } else 0 == c.length && this.removeHighlight()
            }, loadMoreIfNeeded: function () {
                var a, b = this.results, c = b.find("li.select2-more-results"), d = this.resultsPage + 1, e = this, f = this.search.val(), g = this.context;
                0 !== c.length && (a = c.offset().top - b.offset().top - b.height(), a <= this.opts.loadMorePadding && (c.addClass("select2-active"), this.opts.query({
                    element: this.opts.element,
                    term: f,
                    page: d,
                    context: g,
                    matcher: this.opts.matcher,
                    callback: this.bind(function (a) {
                        e.opened() && (e.opts.populateResults.call(this, b, a.results, {
                            term: f,
                            page: d,
                            context: g
                        }), e.postprocessResults(a, !1, !1), a.more === !0 ? (c.detach().appendTo(b).html(e.opts.escapeMarkup(z(e.opts.formatLoadMore, e.opts.element, d + 1))), window.setTimeout(function () {
                            e.loadMoreIfNeeded()
                        }, 10)) : c.remove(), e.positionDropdown(), e.resultsPage = d, e.context = a.context, this.opts.element.trigger({
                            type: "select2-loaded",
                            items: a
                        }))
                    })
                })))
            }, tokenize: function () {
            }, updateResults: function (c) {
                function d() {
                    j.removeClass("select2-active"), m.positionDropdown(), m.liveRegion.text(k.find(".select2-no-results,.select2-selection-limit,.select2-searching").length ? k.text() : m.opts.formatMatches(k.find('.select2-result-selectable:not(".select2-selected")').length))
                }

                function e(a) {
                    k.html(a), d()
                }

                var f, h, i, j = this.search, k = this.results, l = this.opts, m = this, n = j.val(), o = a.data(this.container, "select2-last-term");
                if ((c === !0 || !o || !g(n, o)) && (a.data(this.container, "select2-last-term", n), c === !0 || this.showSearchInput !== !1 && this.opened())) {
                    i = ++this.queryCount;
                    var p = this.getMaximumSelectionSize();
                    if (p >= 1 && (f = this.data(), a.isArray(f) && f.length >= p && y(l.formatSelectionTooBig, "formatSelectionTooBig")))return void e("<li class='select2-selection-limit'>" + z(l.formatSelectionTooBig, l.element, p) + "</li>");
                    if (j.val().length < l.minimumInputLength)return e(y(l.formatInputTooShort, "formatInputTooShort") ? "<li class='select2-no-results'>" + z(l.formatInputTooShort, l.element, j.val(), l.minimumInputLength) + "</li>" : ""), void(c && this.showSearch && this.showSearch(!0));
                    if (l.maximumInputLength && j.val().length > l.maximumInputLength)return void e(y(l.formatInputTooLong, "formatInputTooLong") ? "<li class='select2-no-results'>" + z(l.formatInputTooLong, l.element, j.val(), l.maximumInputLength) + "</li>" : "");
                    l.formatSearching && 0 === this.findHighlightableChoices().length && e("<li class='select2-searching'>" + z(l.formatSearching, l.element) + "</li>"), j.addClass("select2-active"), this.removeHighlight(), h = this.tokenize(), h != b && null != h && j.val(h), this.resultsPage = 1, l.query({
                        element: l.element,
                        term: j.val(),
                        page: this.resultsPage,
                        context: null,
                        matcher: l.matcher,
                        callback: this.bind(function (f) {
                            var h;
                            if (i == this.queryCount) {
                                if (!this.opened())return void this.search.removeClass("select2-active");
                                if (f.hasError !== b && y(l.formatAjaxError, "formatAjaxError"))return void e("<li class='select2-ajax-error'>" + z(l.formatAjaxError, l.element, f.jqXHR, f.textStatus, f.errorThrown) + "</li>");
                                if (this.context = f.context === b ? null : f.context, this.opts.createSearchChoice && "" !== j.val() && (h = this.opts.createSearchChoice.call(m, j.val(), f.results), h !== b && null !== h && m.id(h) !== b && null !== m.id(h) && 0 === a(f.results).filter(function () {
                                        return g(m.id(this), m.id(h))
                                    }).length && this.opts.createSearchChoicePosition(f.results, h)), 0 === f.results.length && y(l.formatNoMatches, "formatNoMatches"))return void e("<li class='select2-no-results'>" + z(l.formatNoMatches, l.element, j.val()) + "</li>");
                                k.empty(), m.opts.populateResults.call(this, k, f.results, {
                                    term: j.val(),
                                    page: this.resultsPage,
                                    context: null
                                }), f.more === !0 && y(l.formatLoadMore, "formatLoadMore") && (k.append("<li class='select2-more-results'>" + l.escapeMarkup(z(l.formatLoadMore, l.element, this.resultsPage)) + "</li>"), window.setTimeout(function () {
                                    m.loadMoreIfNeeded()
                                }, 10)), this.postprocessResults(f, c), d(), this.opts.element.trigger({
                                    type: "select2-loaded",
                                    items: f
                                })
                            }
                        })
                    })
                }
            }, cancel: function () {
                this.close()
            }, blur: function () {
                this.opts.selectOnBlur && this.selectHighlighted({noFocus: !0}), this.close(), this.container.removeClass("select2-container-active"), this.search[0] === document.activeElement && this.search.blur(), this.clearSearch(), this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus")
            }, focusSearch: function () {
                n(this.search)
            }, selectHighlighted: function (a) {
                if (this._touchMoved)return void this.clearTouchMoved();
                var b = this.highlight(), c = this.results.find(".select2-highlighted"), d = c.closest(".select2-result").data("select2-data");
                d ? (this.highlight(b), this.onSelect(d, a)) : a && a.noFocus && this.close()
            }, getPlaceholder: function () {
                var a;
                return this.opts.element.attr("placeholder") || this.opts.element.attr("data-placeholder") || this.opts.element.data("placeholder") || this.opts.placeholder || ((a = this.getPlaceholderOption()) !== b ? a.text() : b)
            }, getPlaceholderOption: function () {
                if (this.select) {
                    var c = this.select.children("option").first();
                    if (this.opts.placeholderOption !== b)return "first" === this.opts.placeholderOption && c || "function" == typeof this.opts.placeholderOption && this.opts.placeholderOption(this.select);
                    if ("" === a.trim(c.text()) && "" === c.val())return c
                }
            }, initContainerWidth: function () {
                function c() {
                    var c, d, e, f, g, h;
                    if ("off" === this.opts.width)return null;
                    if ("element" === this.opts.width)return 0 === this.opts.element.outerWidth(!1) ? "auto" : this.opts.element.outerWidth(!1) + "px";
                    if ("copy" === this.opts.width || "resolve" === this.opts.width) {
                        if (c = this.opts.element.attr("style"), c !== b)for (d = c.split(";"), f = 0, g = d.length; g > f; f += 1)if (h = d[f].replace(/\s/g, ""), e = h.match(/^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i), null !== e && e.length >= 1)return e[1];
                        return "resolve" === this.opts.width ? (c = this.opts.element.css("width"), c.indexOf("%") > 0 ? c : 0 === this.opts.element.outerWidth(!1) ? "auto" : this.opts.element.outerWidth(!1) + "px") : null
                    }
                    return a.isFunction(this.opts.width) ? this.opts.width() : this.opts.width
                }

                var d = c.call(this);
                null !== d && this.container.css("width", d)
            }
        }), F = D(E, {
            createContainer: function () {
                var b = a(document.createElement("div")).attr({"class": "select2-container"}).html(["<a href='javascript:void(0)' class='select2-choice' tabindex='-1'>", "   <span class='select2-chosen'>&#160;</span><abbr class='select2-search-choice-close'></abbr>", "   <span class='select2-arrow' role='presentation'><b role='presentation'></b></span>", "</a>", "<label for='' class='select2-offscreen'></label>", "<input class='select2-focusser select2-offscreen' type='text' aria-haspopup='true' role='button' />", "<div class='select2-drop select2-display-none'>", "   <div class='select2-search'>", "       <label for='' class='select2-offscreen'></label>", "       <input type='text' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' class='select2-input' role='combobox' aria-expanded='true'", "       aria-autocomplete='list' />", "   </div>", "   <ul class='select2-results' role='listbox'>", "   </ul>", "</div>"].join(""));
                return b
            }, enableInterface: function () {
                this.parent.enableInterface.apply(this, arguments) && this.focusser.prop("disabled", !this.isInterfaceEnabled())
            }, opening: function () {
                var c, d, e;
                this.opts.minimumResultsForSearch >= 0 && this.showSearch(!0), this.parent.opening.apply(this, arguments), this.showSearchInput !== !1 && this.search.val(this.focusser.val()), this.opts.shouldFocusInput(this) && (this.search.focus(), c = this.search.get(0), c.createTextRange ? (d = c.createTextRange(), d.collapse(!1), d.select()) : c.setSelectionRange && (e = this.search.val().length, c.setSelectionRange(e, e))), "" === this.search.val() && this.nextSearchTerm != b && (this.search.val(this.nextSearchTerm), this.search.select()), this.focusser.prop("disabled", !0).val(""), this.updateResults(!0), this.opts.element.trigger(a.Event("select2-open"))
            }, close: function () {
                this.opened() && (this.parent.close.apply(this, arguments), this.focusser.prop("disabled", !1), this.opts.shouldFocusInput(this) && this.focusser.focus())
            }, focus: function () {
                this.opened() ? this.close() : (this.focusser.prop("disabled", !1), this.opts.shouldFocusInput(this) && this.focusser.focus())
            }, isFocused: function () {
                return this.container.hasClass("select2-container-active")
            }, cancel: function () {
                this.parent.cancel.apply(this, arguments), this.focusser.prop("disabled", !1), this.opts.shouldFocusInput(this) && this.focusser.focus()
            }, destroy: function () {
                a("label[for='" + this.focusser.attr("id") + "']").attr("for", this.opts.element.attr("id")), this.parent.destroy.apply(this, arguments), C.call(this, "selection", "focusser")
            }, initContainer: function () {
                var b, d, e = this.container, f = this.dropdown, g = H();
                this.showSearch(this.opts.minimumResultsForSearch < 0 ? !1 : !0), this.selection = b = e.find(".select2-choice"), this.focusser = e.find(".select2-focusser"), b.find(".select2-chosen").attr("id", "select2-chosen-" + g), this.focusser.attr("aria-labelledby", "select2-chosen-" + g), this.results.attr("id", "select2-results-" + g), this.search.attr("aria-owns", "select2-results-" + g), this.focusser.attr("id", "s2id_autogen" + g), d = a("label[for='" + this.opts.element.attr("id") + "']"), this.opts.element.focus(this.bind(function () {
                    this.focus()
                })), this.focusser.prev().text(d.text()).attr("for", this.focusser.attr("id"));
                var h = this.opts.element.attr("title");
                this.opts.element.attr("title", h || d.text()), this.focusser.attr("tabindex", this.elementTabIndex), this.search.attr("id", this.focusser.attr("id") + "_search"), this.search.prev().text(a("label[for='" + this.focusser.attr("id") + "']").text()).attr("for", this.search.attr("id")), this.search.on("keydown", this.bind(function (a) {
                    if (this.isInterfaceEnabled() && 229 != a.keyCode) {
                        if (a.which === M.PAGE_UP || a.which === M.PAGE_DOWN)return void p(a);
                        switch (a.which) {
                            case M.UP:
                            case M.DOWN:
                                return this.moveHighlight(a.which === M.UP ? -1 : 1), void p(a);
                            case M.ENTER:
                                return this.selectHighlighted(), void p(a);
                            case M.TAB:
                                return void this.selectHighlighted({noFocus: !0});
                            case M.ESC:
                                return this.cancel(a), void p(a)
                        }
                    }
                })), this.search.on("blur", this.bind(function () {
                    document.activeElement === this.body.get(0) && window.setTimeout(this.bind(function () {
                        this.opened() && this.search.focus()
                    }), 0)
                })), this.focusser.on("keydown", this.bind(function (a) {
                    if (this.isInterfaceEnabled() && a.which !== M.TAB && !M.isControl(a) && !M.isFunctionKey(a) && a.which !== M.ESC) {
                        if (this.opts.openOnEnter === !1 && a.which === M.ENTER)return void p(a);
                        if (a.which == M.DOWN || a.which == M.UP || a.which == M.ENTER && this.opts.openOnEnter) {
                            if (a.altKey || a.ctrlKey || a.shiftKey || a.metaKey)return;
                            return this.open(), void p(a)
                        }
                        return a.which == M.DELETE || a.which == M.BACKSPACE ? (this.opts.allowClear && this.clear(), void p(a)) : void 0
                    }
                })), j(this.focusser), this.focusser.on("keyup-change input", this.bind(function (a) {
                    if (this.opts.minimumResultsForSearch >= 0) {
                        if (a.stopPropagation(), this.opened())return;
                        this.open()
                    }
                })), b.on("mousedown touchstart", "abbr", this.bind(function (a) {
                    this.isInterfaceEnabled() && (this.clear(), q(a), this.close(), this.selection && this.selection.focus())
                })), b.on("mousedown touchstart", this.bind(function (d) {
                    c(b), this.container.hasClass("select2-container-active") || this.opts.element.trigger(a.Event("select2-focus")), this.opened() ? this.close() : this.isInterfaceEnabled() && this.open(), p(d)
                })), f.on("mousedown touchstart", this.bind(function () {
                    this.opts.shouldFocusInput(this) && this.search.focus()
                })), b.on("focus", this.bind(function (a) {
                    p(a)
                })), this.focusser.on("focus", this.bind(function () {
                    this.container.hasClass("select2-container-active") || this.opts.element.trigger(a.Event("select2-focus")), this.container.addClass("select2-container-active")
                })).on("blur", this.bind(function () {
                    this.opened() || (this.container.removeClass("select2-container-active"), this.opts.element.trigger(a.Event("select2-blur")))
                })), this.search.on("focus", this.bind(function () {
                    this.container.hasClass("select2-container-active") || this.opts.element.trigger(a.Event("select2-focus")), this.container.addClass("select2-container-active")
                })), this.initContainerWidth(), this.opts.element.hide(), this.setPlaceholder()
            }, clear: function (b) {
                var c = this.selection.data("select2-data");
                if (c) {
                    var d = a.Event("select2-clearing");
                    if (this.opts.element.trigger(d), d.isDefaultPrevented())return;
                    var e = this.getPlaceholderOption();
                    this.opts.element.val(e ? e.val() : ""), this.selection.find(".select2-chosen").empty(), this.selection.removeData("select2-data"), this.setPlaceholder(), b !== !1 && (this.opts.element.trigger({
                        type: "select2-removed",
                        val: this.id(c),
                        choice: c
                    }), this.triggerChange({removed: c}))
                }
            }, initSelection: function () {
                if (this.isPlaceholderOptionSelected())this.updateSelection(null), this.close(), this.setPlaceholder(); else {
                    var a = this;
                    this.opts.initSelection.call(null, this.opts.element, function (c) {
                        c !== b && null !== c && (a.updateSelection(c), a.close(), a.setPlaceholder(), a.nextSearchTerm = a.opts.nextSearchTerm(c, a.search.val()))
                    })
                }
            }, isPlaceholderOptionSelected: function () {
                var a;
                return this.getPlaceholder() === b ? !1 : (a = this.getPlaceholderOption()) !== b && a.prop("selected") || "" === this.opts.element.val() || this.opts.element.val() === b || null === this.opts.element.val()
            }, prepareOpts: function () {
                var b = this.parent.prepareOpts.apply(this, arguments), c = this;
                return "select" === b.element.get(0).tagName.toLowerCase() ? b.initSelection = function (a, b) {
                    var d = a.find("option").filter(function () {
                        return this.selected && !this.disabled
                    });
                    b(c.optionToData(d))
                } : "data"in b && (b.initSelection = b.initSelection || function (c, d) {
                    var e = c.val(), f = null;
                    b.query({
                        matcher: function (a, c, d) {
                            var h = g(e, b.id(d));
                            return h && (f = d), h
                        }, callback: a.isFunction(d) ? function () {
                            d(f)
                        } : a.noop
                    })
                }), b
            }, getPlaceholder: function () {
                return this.select && this.getPlaceholderOption() === b ? b : this.parent.getPlaceholder.apply(this, arguments)
            }, setPlaceholder: function () {
                var a = this.getPlaceholder();
                if (this.isPlaceholderOptionSelected() && a !== b) {
                    if (this.select && this.getPlaceholderOption() === b)return;
                    this.selection.find(".select2-chosen").html(this.opts.escapeMarkup(a)), this.selection.addClass("select2-default"), this.container.removeClass("select2-allowclear")
                }
            }, postprocessResults: function (a, b, c) {
                var d = 0, e = this;
                if (this.findHighlightableChoices().each2(function (a, b) {
                        return g(e.id(b.data("select2-data")), e.opts.element.val()) ? (d = a, !1) : void 0
                    }), c !== !1 && this.highlight(b === !0 && d >= 0 ? d : 0), b === !0) {
                    var f = this.opts.minimumResultsForSearch;
                    f >= 0 && this.showSearch(A(a.results) >= f)
                }
            }, showSearch: function (b) {
                this.showSearchInput !== b && (this.showSearchInput = b, this.dropdown.find(".select2-search").toggleClass("select2-search-hidden", !b), this.dropdown.find(".select2-search").toggleClass("select2-offscreen", !b), a(this.dropdown, this.container).toggleClass("select2-with-searchbox", b))
            }, onSelect: function (a, b) {
                if (this.triggerSelect(a)) {
                    var c = this.opts.element.val(), d = this.data();
                    this.opts.element.val(this.id(a)), this.updateSelection(a), this.opts.element.trigger({
                        type: "select2-selected",
                        val: this.id(a),
                        choice: a
                    }), this.nextSearchTerm = this.opts.nextSearchTerm(a, this.search.val()), this.close(), b && b.noFocus || !this.opts.shouldFocusInput(this) || this.focusser.focus(), g(c, this.id(a)) || this.triggerChange({
                        added: a,
                        removed: d
                    })
                }
            }, updateSelection: function (a) {
                var c, d, e = this.selection.find(".select2-chosen");
                this.selection.data("select2-data", a), e.empty(), null !== a && (c = this.opts.formatSelection(a, e, this.opts.escapeMarkup)), c !== b && e.append(c), d = this.opts.formatSelectionCssClass(a, e), d !== b && e.addClass(d), this.selection.removeClass("select2-default"), this.opts.allowClear && this.getPlaceholder() !== b && this.container.addClass("select2-allowclear")
            }, val: function () {
                var a, c = !1, d = null, e = this, f = this.data();
                if (0 === arguments.length)return this.opts.element.val();
                if (a = arguments[0], arguments.length > 1 && (c = arguments[1]), this.select)this.select.val(a).find("option").filter(function () {
                    return this.selected
                }).each2(function (a, b) {
                    return d = e.optionToData(b), !1
                }), this.updateSelection(d), this.setPlaceholder(), c && this.triggerChange({
                    added: d,
                    removed: f
                }); else {
                    if (!a && 0 !== a)return void this.clear(c);
                    if (this.opts.initSelection === b)throw new Error("cannot call val() if initSelection() is not defined");
                    this.opts.element.val(a), this.opts.initSelection(this.opts.element, function (a) {
                        e.opts.element.val(a ? e.id(a) : ""), e.updateSelection(a), e.setPlaceholder(), c && e.triggerChange({
                            added: a,
                            removed: f
                        })
                    })
                }
            }, clearSearch: function () {
                this.search.val(""), this.focusser.val("")
            }, data: function (a) {
                var c, d = !1;
                return 0 === arguments.length ? (c = this.selection.data("select2-data"), c == b && (c = null), c) : (arguments.length > 1 && (d = arguments[1]), void(a ? (c = this.data(), this.opts.element.val(a ? this.id(a) : ""), this.updateSelection(a), d && this.triggerChange({
                    added: a,
                    removed: c
                })) : this.clear(d)))
            }
        }), G = D(E, {
            createContainer: function () {
                var b = a(document.createElement("div")).attr({"class": "select2-container select2-container-multi"}).html(["<ul class='select2-choices'>", "  <li class='select2-search-field'>", "    <label for='' class='select2-offscreen'></label>", "    <input type='text' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' class='select2-input'>", "  </li>", "</ul>", "<div class='select2-drop select2-drop-multi select2-display-none'>", "   <ul class='select2-results'>", "   </ul>", "</div>"].join(""));
                return b
            }, prepareOpts: function () {
                var b = this.parent.prepareOpts.apply(this, arguments), c = this;
                return "select" === b.element.get(0).tagName.toLowerCase() ? b.initSelection = function (a, b) {
                    var d = [];
                    a.find("option").filter(function () {
                        return this.selected && !this.disabled
                    }).each2(function (a, b) {
                        d.push(c.optionToData(b))
                    }), b(d)
                } : "data"in b && (b.initSelection = b.initSelection || function (c, d) {
                    var e = h(c.val(), b.separator, b.transformVal), f = [];
                    b.query({
                        matcher: function (c, d, h) {
                            var i = a.grep(e, function (a) {
                                return g(a, b.id(h))
                            }).length;
                            return i && f.push(h), i
                        }, callback: a.isFunction(d) ? function () {
                            for (var a = [], c = 0; c < e.length; c++)for (var h = e[c], i = 0; i < f.length; i++) {
                                var j = f[i];
                                if (g(h, b.id(j))) {
                                    a.push(j), f.splice(i, 1);
                                    break
                                }
                            }
                            d(a)
                        } : a.noop
                    })
                }), b
            }, selectChoice: function (a) {
                var b = this.container.find(".select2-search-choice-focus");
                b.length && a && a[0] == b[0] || (b.length && this.opts.element.trigger("choice-deselected", b), b.removeClass("select2-search-choice-focus"), a && a.length && (this.close(), a.addClass("select2-search-choice-focus"), this.opts.element.trigger("choice-selected", a)))
            }, destroy: function () {
                a("label[for='" + this.search.attr("id") + "']").attr("for", this.opts.element.attr("id")), this.parent.destroy.apply(this, arguments), C.call(this, "searchContainer", "selection")
            }, initContainer: function () {
                var b, c = ".select2-choices";
                this.searchContainer = this.container.find(".select2-search-field"), this.selection = b = this.container.find(c);
                var d = this;
                this.selection.on("click", ".select2-container:not(.select2-container-disabled) .select2-search-choice:not(.select2-locked)", function () {
                    d.search[0].focus(), d.selectChoice(a(this))
                }), this.search.attr("id", "s2id_autogen" + H()), this.search.prev().text(a("label[for='" + this.opts.element.attr("id") + "']").text()).attr("for", this.search.attr("id")), this.opts.element.focus(this.bind(function () {
                    this.focus()
                })), this.search.on("input paste", this.bind(function () {
                    this.search.attr("placeholder") && 0 == this.search.val().length || this.isInterfaceEnabled() && (this.opened() || this.open())
                })), this.search.attr("tabindex", this.elementTabIndex), this.keydowns = 0, this.search.on("keydown", this.bind(function (a) {
                    if (this.isInterfaceEnabled()) {
                        ++this.keydowns;
                        var c = b.find(".select2-search-choice-focus"), d = c.prev(".select2-search-choice:not(.select2-locked)"), e = c.next(".select2-search-choice:not(.select2-locked)"), f = o(this.search);
                        if (c.length && (a.which == M.LEFT || a.which == M.RIGHT || a.which == M.BACKSPACE || a.which == M.DELETE || a.which == M.ENTER)) {
                            var g = c;
                            return a.which == M.LEFT && d.length ? g = d : a.which == M.RIGHT ? g = e.length ? e : null : a.which === M.BACKSPACE ? this.unselect(c.first()) && (this.search.width(10), g = d.length ? d : e) : a.which == M.DELETE ? this.unselect(c.first()) && (this.search.width(10), g = e.length ? e : null) : a.which == M.ENTER && (g = null), this.selectChoice(g), p(a), void(g && g.length || this.open())
                        }
                        if ((a.which === M.BACKSPACE && 1 == this.keydowns || a.which == M.LEFT) && 0 == f.offset && !f.length)return this.selectChoice(b.find(".select2-search-choice:not(.select2-locked)").last()), void p(a);
                        if (this.selectChoice(null), this.opened())switch (a.which) {
                            case M.UP:
                            case M.DOWN:
                                return this.moveHighlight(a.which === M.UP ? -1 : 1), void p(a);
                            case M.ENTER:
                                return this.selectHighlighted(), void p(a);
                            case M.TAB:
                                return this.selectHighlighted({noFocus: !0}), void this.close();
                            case M.ESC:
                                return this.cancel(a), void p(a)
                        }
                        if (a.which !== M.TAB && !M.isControl(a) && !M.isFunctionKey(a) && a.which !== M.BACKSPACE && a.which !== M.ESC) {
                            if (a.which === M.ENTER) {
                                if (this.opts.openOnEnter === !1)return;
                                if (a.altKey || a.ctrlKey || a.shiftKey || a.metaKey)return
                            }
                            this.open(), (a.which === M.PAGE_UP || a.which === M.PAGE_DOWN) && p(a), a.which === M.ENTER && p(a)
                        }
                    }
                })), this.search.on("keyup", this.bind(function () {
                    this.keydowns = 0, this.resizeSearch()
                })), this.search.on("blur", this.bind(function (b) {
                    this.container.removeClass("select2-container-active"), this.search.removeClass("select2-focused"), this.selectChoice(null), this.opened() || this.clearSearch(), b.stopImmediatePropagation(), this.opts.element.trigger(a.Event("select2-blur"))
                })), this.container.on("click", c, this.bind(function (b) {
                    this.isInterfaceEnabled() && (a(b.target).closest(".select2-search-choice").length > 0 || (this.selectChoice(null), this.clearPlaceholder(), this.container.hasClass("select2-container-active") || this.opts.element.trigger(a.Event("select2-focus")), this.open(), this.focusSearch(), b.preventDefault()))
                })), this.container.on("focus", c, this.bind(function () {
                    this.isInterfaceEnabled() && (this.container.hasClass("select2-container-active") || this.opts.element.trigger(a.Event("select2-focus")), this.container.addClass("select2-container-active"), this.dropdown.addClass("select2-drop-active"), this.clearPlaceholder())
                })), this.initContainerWidth(), this.opts.element.hide(), this.clearSearch()
            }, enableInterface: function () {
                this.parent.enableInterface.apply(this, arguments) && this.search.prop("disabled", !this.isInterfaceEnabled())
            }, initSelection: function () {
                if ("" === this.opts.element.val() && "" === this.opts.element.text() && (this.updateSelection([]), this.close(), this.clearSearch()), this.select || "" !== this.opts.element.val()) {
                    var a = this;
                    this.opts.initSelection.call(null, this.opts.element, function (c) {
                        c !== b && null !== c && (a.updateSelection(c), a.close(), a.clearSearch())
                    })
                }
            }, clearSearch: function () {
                var a = this.getPlaceholder(), c = this.getMaxSearchWidth();
                a !== b && 0 === this.getVal().length && this.search.hasClass("select2-focused") === !1 ? (this.search.val(a).addClass("select2-default"), this.search.width(c > 0 ? c : this.container.css("width"))) : this.search.val("").width(10)
            }, clearPlaceholder: function () {
                this.search.hasClass("select2-default") && this.search.val("").removeClass("select2-default")
            }, opening: function () {
                this.clearPlaceholder(), this.resizeSearch(), this.parent.opening.apply(this, arguments), this.focusSearch(), "" === this.search.val() && this.nextSearchTerm != b && (this.search.val(this.nextSearchTerm), this.search.select()), this.updateResults(!0), this.opts.shouldFocusInput(this) && this.search.focus(), this.opts.element.trigger(a.Event("select2-open"))
            }, close: function () {
                this.opened() && this.parent.close.apply(this, arguments)
            }, focus: function () {
                this.close(), this.search.focus()
            }, isFocused: function () {
                return this.search.hasClass("select2-focused")
            }, updateSelection: function (b) {
                var c = [], d = [], f = this;
                a(b).each(function () {
                    e(f.id(this), c) < 0 && (c.push(f.id(this)), d.push(this))
                }), b = d, this.selection.find(".select2-search-choice").remove(), a(b).each(function () {
                    f.addSelectedChoice(this)
                }), f.postprocessResults()
            }, tokenize: function () {
                var a = this.search.val();
                a = this.opts.tokenizer.call(this, a, this.data(), this.bind(this.onSelect), this.opts), null != a && a != b && (this.search.val(a), a.length > 0 && this.open())
            }, onSelect: function (a, c) {
                this.triggerSelect(a) && "" !== a.text && (this.addSelectedChoice(a), this.opts.element.trigger({
                    type: "selected",
                    val: this.id(a),
                    choice: a
                }), this.nextSearchTerm = this.opts.nextSearchTerm(a, this.search.val()), this.clearSearch(), this.updateResults(), (this.select || !this.opts.closeOnSelect) && this.postprocessResults(a, !1, this.opts.closeOnSelect === !0), this.opts.closeOnSelect ? (this.close(), this.search.width(10)) : this.countSelectableResults() > 0 ? (this.search.width(10), this.resizeSearch(), this.getMaximumSelectionSize() > 0 && this.val().length >= this.getMaximumSelectionSize() ? this.updateResults(!0) : this.nextSearchTerm != b && (this.search.val(this.nextSearchTerm), this.updateResults(), this.search.select()), this.positionDropdown()) : (this.close(), this.search.width(10)), this.triggerChange({added: a}), c && c.noFocus || this.focusSearch())
            }, cancel: function () {
                this.close(), this.focusSearch()
            }, addSelectedChoice: function (c) {
                var d, e, f = !c.locked, g = a("<li class='select2-search-choice'>    <div></div>    <a href='#' class='select2-search-choice-close' tabindex='-1'></a></li>"), h = a("<li class='select2-search-choice select2-locked'><div></div></li>"), i = f ? g : h, j = this.id(c), k = this.getVal();
                d = this.opts.formatSelection(c, i.find("div"), this.opts.escapeMarkup), d != b && i.find("div").replaceWith(a("<div></div>").html(d)), e = this.opts.formatSelectionCssClass(c, i.find("div")), e != b && i.addClass(e), f && i.find(".select2-search-choice-close").on("mousedown", p).on("click dblclick", this.bind(function (b) {
                    this.isInterfaceEnabled() && (this.unselect(a(b.target)), this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus"), p(b), this.close(), this.focusSearch())
                })).on("focus", this.bind(function () {
                    this.isInterfaceEnabled() && (this.container.addClass("select2-container-active"), this.dropdown.addClass("select2-drop-active"))
                })), i.data("select2-data", c), i.insertBefore(this.searchContainer), k.push(j), this.setVal(k)
            }, unselect: function (b) {
                var c, d, f = this.getVal();
                if (b = b.closest(".select2-search-choice"), 0 === b.length)throw"Invalid argument: " + b + ". Must be .select2-search-choice";
                if (c = b.data("select2-data")) {
                    var g = a.Event("select2-removing");
                    if (g.val = this.id(c), g.choice = c, this.opts.element.trigger(g), g.isDefaultPrevented())return !1;
                    for (; (d = e(this.id(c), f)) >= 0;)f.splice(d, 1), this.setVal(f), this.select && this.postprocessResults();
                    return b.remove(), this.opts.element.trigger({
                        type: "select2-removed",
                        val: this.id(c),
                        choice: c
                    }), this.triggerChange({removed: c}), !0
                }
            }, postprocessResults: function (a, b, c) {
                var d = this.getVal(), f = this.results.find(".select2-result"), g = this.results.find(".select2-result-with-children"), h = this;
                f.each2(function (a, b) {
                    var c = h.id(b.data("select2-data"));
                    e(c, d) >= 0 && (b.addClass("select2-selected"), b.find(".select2-result-selectable").addClass("select2-selected"))
                }), g.each2(function (a, b) {
                    b.is(".select2-result-selectable") || 0 !== b.find(".select2-result-selectable:not(.select2-selected)").length || b.addClass("select2-selected")
                }), -1 == this.highlight() && c !== !1 && this.opts.closeOnSelect === !0 && h.highlight(0), !this.opts.createSearchChoice && !f.filter(".select2-result:not(.select2-selected)").length > 0 && (!a || a && !a.more && 0 === this.results.find(".select2-no-results").length) && y(h.opts.formatNoMatches, "formatNoMatches") && this.results.append("<li class='select2-no-results'>" + z(h.opts.formatNoMatches, h.opts.element, h.search.val()) + "</li>")
            }, getMaxSearchWidth: function () {
                return this.selection.width() - i(this.search)
            }, resizeSearch: function () {
                var a, b, c, d, e, f = i(this.search);
                a = r(this.search) + 10, b = this.search.offset().left, c = this.selection.width(), d = this.selection.offset().left, e = c - (b - d) - f, a > e && (e = c - f), 40 > e && (e = c - f), 0 >= e && (e = a), this.search.width(Math.floor(e))
            }, getVal: function () {
                var a;
                return this.select ? (a = this.select.val(), null === a ? [] : a) : (a = this.opts.element.val(), h(a, this.opts.separator, this.opts.transformVal))
            }, setVal: function (b) {
                var c;
                this.select ? this.select.val(b) : (c = [], a(b).each(function () {
                    e(this, c) < 0 && c.push(this)
                }), this.opts.element.val(0 === c.length ? "" : c.join(this.opts.separator)))
            }, buildChangeDetails: function (a, b) {
                for (var b = b.slice(0), a = a.slice(0), c = 0; c < b.length; c++)for (var d = 0; d < a.length; d++)g(this.opts.id(b[c]), this.opts.id(a[d])) && (b.splice(c, 1), c > 0 && c--, a.splice(d, 1), d--);
                return {added: b, removed: a}
            }, val: function (c, d) {
                var e, f = this;
                if (0 === arguments.length)return this.getVal();
                if (e = this.data(), e.length || (e = []), !c && 0 !== c)return this.opts.element.val(""), this.updateSelection([]), this.clearSearch(), void(d && this.triggerChange({
                    added: this.data(),
                    removed: e
                }));
                if (this.setVal(c), this.select)this.opts.initSelection(this.select, this.bind(this.updateSelection)), d && this.triggerChange(this.buildChangeDetails(e, this.data())); else {
                    if (this.opts.initSelection === b)throw new Error("val() cannot be called if initSelection() is not defined");
                    this.opts.initSelection(this.opts.element, function (b) {
                        var c = a.map(b, f.id);
                        f.setVal(c), f.updateSelection(b), f.clearSearch(), d && f.triggerChange(f.buildChangeDetails(e, f.data()))
                    })
                }
                this.clearSearch()
            }, onSortStart: function () {
                if (this.select)throw new Error("Sorting of elements is not supported when attached to <select>. Attach to <input type='hidden'/> instead.");
                this.search.width(0), this.searchContainer.hide()
            }, onSortEnd: function () {
                var b = [], c = this;
                this.searchContainer.show(), this.searchContainer.appendTo(this.searchContainer.parent()), this.resizeSearch(), this.selection.find(".select2-search-choice").each(function () {
                    b.push(c.opts.id(a(this).data("select2-data")))
                }), this.setVal(b), this.triggerChange()
            }, data: function (b, c) {
                var d, e, f = this;
                return 0 === arguments.length ? this.selection.children(".select2-search-choice").map(function () {
                    return a(this).data("select2-data")
                }).get() : (e = this.data(), b || (b = []), d = a.map(b, function (a) {
                    return f.opts.id(a)
                }), this.setVal(d), this.updateSelection(b), this.clearSearch(), c && this.triggerChange(this.buildChangeDetails(e, this.data())), void 0)
            }
        }), a.fn.select2 = function () {
            var c, d, f, g, h, i = Array.prototype.slice.call(arguments, 0), j = ["val", "destroy", "opened", "open", "close", "focus", "isFocused", "container", "dropdown", "onSortStart", "onSortEnd", "enable", "disable", "readonly", "positionDropdown", "data", "search"], k = ["opened", "isFocused", "container", "dropdown"], l = ["val", "data"], m = {search: "externalSearch"};
            return this.each(function () {
                if (0 === i.length || "object" == typeof i[0])c = 0 === i.length ? {} : a.extend({}, i[0]), c.element = a(this), "select" === c.element.get(0).tagName.toLowerCase() ? h = c.element.prop("multiple") : (h = c.multiple || !1, "tags"in c && (c.multiple = h = !0)), d = h ? new window.Select2["class"].multi : new window.Select2["class"].single, d.init(c); else {
                    if ("string" != typeof i[0])throw"Invalid arguments to select2 plugin: " + i;
                    if (e(i[0], j) < 0)throw"Unknown method: " + i[0];
                    if (g = b, d = a(this).data("select2"), d === b)return;
                    if (f = i[0], "container" === f ? g = d.container : "dropdown" === f ? g = d.dropdown : (m[f] && (f = m[f]), g = d[f].apply(d, i.slice(1))), e(i[0], k) >= 0 || e(i[0], l) >= 0 && 1 == i.length)return !1
                }
            }), g === b ? this : g
        }, a.fn.select2.defaults = {
            width: "copy",
            loadMorePadding: 0,
            closeOnSelect: !0,
            openOnEnter: !0,
            containerCss: {},
            dropdownCss: {},
            containerCssClass: "",
            dropdownCssClass: "",
            formatResult: function (a, b, c, d) {
                var e = [];
                return t(this.text(a), c.term, e, d), e.join("")
            },
            transformVal: function (b) {
                return a.trim(b)
            },
            formatSelection: function (a, c, d) {
                return a ? d(this.text(a)) : b
            },
            sortResults: function (a) {
                return a
            },
            formatResultCssClass: function (a) {
                return a.css
            },
            formatSelectionCssClass: function () {
                return b
            },
            minimumResultsForSearch: 0,
            minimumInputLength: 0,
            maximumInputLength: null,
            maximumSelectionSize: 0,
            id: function (a) {
                return a == b ? null : a.id
            },
            text: function (b) {
                return b && this.data && this.data.text ? a.isFunction(this.data.text) ? this.data.text(b) : b[this.data.text] : b.text
            },
            matcher: function (a, b) {
                return d("" + b).toUpperCase().indexOf(d("" + a).toUpperCase()) >= 0
            },
            separator: ",",
            tokenSeparators: [],
            tokenizer: B,
            escapeMarkup: u,
            blurOnChange: !1,
            selectOnBlur: !1,
            adaptContainerCssClass: function (a) {
                return a
            },
            adaptDropdownCssClass: function () {
                return null
            },
            nextSearchTerm: function () {
                return b
            },
            searchInputPlaceholder: "",
            createSearchChoicePosition: "top",
            shouldFocusInput: function (a) {
                var b = "ontouchstart"in window || navigator.msMaxTouchPoints > 0;
                return b && a.opts.minimumResultsForSearch < 0 ? !1 : !0
            }
        }, a.fn.select2.locales = [], a.fn.select2.locales.en = {
            formatMatches: function (a) {
                return 1 === a ? "One result is available, press enter to select it." : a + " results are available, use up and down arrow keys to navigate."
            }, formatNoMatches: function () {
                return "No matches found"
            }, formatAjaxError: function () {
                return "Loading failed"
            }, formatInputTooShort: function (a, b) {
                var c = b - a.length;
                return "Please enter " + c + " or more character" + (1 == c ? "" : "s")
            }, formatInputTooLong: function (a, b) {
                var c = a.length - b;
                return "Please delete " + c + " character" + (1 == c ? "" : "s")
            }, formatSelectionTooBig: function (a) {
                return "You can only select " + a + " item" + (1 == a ? "" : "s")
            }, formatLoadMore: function () {
                return "Loading more results…"
            }, formatSearching: function () {
                return "Searching…"
            }
        }, a.extend(a.fn.select2.defaults, a.fn.select2.locales.en), a.fn.select2.ajaxDefaults = {
            transport: a.ajax,
            params: {type: "GET", cache: !1, dataType: "json"}
        }, window.Select2 = {
            query: {ajax: v, local: w, tags: x},
            util: {debounce: l, markMatch: t, escapeMarkup: u, stripDiacritics: d},
            "class": {"abstract": E, single: F, multi: G}
        }
    }
}(jQuery), function (a, b, c) {
    "use strict";
    function d(c) {
        if (e = b.documentElement, f = b.body, T(), hb = this, c = c || {}, mb = c.constants || {}, c.easing)for (var d in c.easing)W[d] = c.easing[d];
        tb = c.edgeStrategy || "set", kb = {
            beforerender: c.beforerender,
            render: c.render,
            keyframe: c.keyframe
        }, lb = c.forceHeight !== !1, lb && (Kb = c.scale || 1), nb = c.mobileDeceleration || y, pb = c.smoothScrolling !== !1, qb = c.smoothScrollingDuration || A, rb = {targetTop: hb.getScrollTop()}, Sb = (c.mobileCheck || function () {
            return /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent || navigator.vendor || a.opera)
        })(), Sb ? (jb = b.getElementById(c.skrollrBody || z), jb && gb(), X(), Eb(e, [s, v], [t])) : Eb(e, [s, u], [t]), hb.refresh(), wb(a, "resize orientationchange", function () {
            var a = e.clientWidth, b = e.clientHeight;
            (b !== Pb || a !== Ob) && (Pb = b, Ob = a, Qb = !0)
        });
        var g = U();
        return function h() {
            $(), vb = g(h)
        }(), hb
    }

    var e, f, g = {
        get: function () {
            return hb
        }, init: function (a) {
            return hb || new d(a)
        }, VERSION: "0.6.29"
    }, h = Object.prototype.hasOwnProperty, i = a.Math, j = a.getComputedStyle, k = "touchstart", l = "touchmove", m = "touchcancel", n = "touchend", o = "skrollable", p = o + "-before", q = o + "-between", r = o + "-after", s = "skrollr", t = "no-" + s, u = s + "-desktop", v = s + "-mobile", w = "linear", x = 1e3, y = .004, z = "skrollr-body", A = 200, B = "start", C = "end", D = "center", E = "bottom", F = "___skrollable_id", G = /^(?:input|textarea|button|select)$/i, H = /^\s+|\s+$/g, I = /^data(?:-(_\w+))?(?:-?(-?\d*\.?\d+p?))?(?:-?(start|end|top|center|bottom))?(?:-?(top|center|bottom))?$/, J = /\s*(@?[\w\-\[\]]+)\s*:\s*(.+?)\s*(?:;|$)/gi, K = /^(@?[a-z\-]+)\[(\w+)\]$/, L = /-([a-z0-9_])/g, M = function (a, b) {
        return b.toUpperCase()
    }, N = /[\-+]?[\d]*\.?[\d]+/g, O = /\{\?\}/g, P = /rgba?\(\s*-?\d+\s*,\s*-?\d+\s*,\s*-?\d+/g, Q = /[a-z\-]+-gradient/g, R = "", S = "", T = function () {
        var a = /^(?:O|Moz|webkit|ms)|(?:-(?:o|moz|webkit|ms)-)/;
        if (j) {
            var b = j(f, null);
            for (var c in b)if (R = c.match(a) || +c == c && b[c].match(a))break;
            if (!R)return void(R = S = "");
            R = R[0], "-" === R.slice(0, 1) ? (S = R, R = {
                "-webkit-": "webkit",
                "-moz-": "Moz",
                "-ms-": "ms",
                "-o-": "O"
            }[R]) : S = "-" + R.toLowerCase() + "-"
        }
    }, U = function () {
        var b = a.requestAnimationFrame || a[R.toLowerCase() + "RequestAnimationFrame"], c = Hb();
        return (Sb || !b) && (b = function (b) {
            var d = Hb() - c, e = i.max(0, 1e3 / 60 - d);
            return a.setTimeout(function () {
                c = Hb(), b()
            }, e)
        }), b
    }, V = function () {
        var b = a.cancelAnimationFrame || a[R.toLowerCase() + "CancelAnimationFrame"];
        return (Sb || !b) && (b = function (b) {
            return a.clearTimeout(b)
        }), b
    }, W = {
        begin: function () {
            return 0
        }, end: function () {
            return 1
        }, linear: function (a) {
            return a
        }, quadratic: function (a) {
            return a * a
        }, cubic: function (a) {
            return a * a * a
        }, swing: function (a) {
            return -i.cos(a * i.PI) / 2 + .5
        }, sqrt: function (a) {
            return i.sqrt(a)
        }, outCubic: function (a) {
            return i.pow(a - 1, 3) + 1
        }, bounce: function (a) {
            var b;
            if (.5083 >= a)b = 3; else if (.8489 >= a)b = 9; else if (.96208 >= a)b = 27; else {
                if (!(.99981 >= a))return 1;
                b = 91
            }
            return 1 - i.abs(3 * i.cos(a * b * 1.028) / b)
        }
    };
    d.prototype.refresh = function (a) {
        var d, e, f = !1;
        for (a === c ? (f = !0, ib = [], Rb = 0, a = b.getElementsByTagName("*")) : a.length === c && (a = [a]), d = 0, e = a.length; e > d; d++) {
            var g = a[d], h = g, i = [], j = pb, k = tb, l = !1;
            if (f && F in g && delete g[F], g.attributes) {
                for (var m = 0, n = g.attributes.length; n > m; m++) {
                    var p = g.attributes[m];
                    if ("data-anchor-target" !== p.name)if ("data-smooth-scrolling" !== p.name)if ("data-edge-strategy" !== p.name)if ("data-emit-events" !== p.name) {
                        var q = p.name.match(I);
                        if (null !== q) {
                            var r = {props: p.value, element: g, eventType: p.name.replace(L, M)};
                            i.push(r);
                            var s = q[1];
                            s && (r.constant = s.substr(1));
                            var t = q[2];
                            /p$/.test(t) ? (r.isPercentage = !0, r.offset = (0 | t.slice(0, -1)) / 100) : r.offset = 0 | t;
                            var u = q[3], v = q[4] || u;
                            u && u !== B && u !== C ? (r.mode = "relative", r.anchors = [u, v]) : (r.mode = "absolute", u === C ? r.isEnd = !0 : r.isPercentage || (r.offset = r.offset * Kb))
                        }
                    } else l = !0; else k = p.value; else j = "off" !== p.value; else if (h = b.querySelector(p.value), null === h)throw'Unable to find anchor target "' + p.value + '"'
                }
                if (i.length) {
                    var w, x, y;
                    !f && F in g ? (y = g[F], w = ib[y].styleAttr, x = ib[y].classAttr) : (y = g[F] = Rb++, w = g.style.cssText, x = Db(g)), ib[y] = {
                        element: g,
                        styleAttr: w,
                        classAttr: x,
                        anchorTarget: h,
                        keyFrames: i,
                        smoothScrolling: j,
                        edgeStrategy: k,
                        emitEvents: l,
                        lastFrameIndex: -1
                    }, Eb(g, [o], [])
                }
            }
        }
        for (Ab(), d = 0, e = a.length; e > d; d++) {
            var z = ib[a[d][F]];
            z !== c && (_(z), bb(z))
        }
        return hb
    }, d.prototype.relativeToAbsolute = function (a, b, c) {
        var d = e.clientHeight, f = a.getBoundingClientRect(), g = f.top, h = f.bottom - f.top;
        return b === E ? g -= d : b === D && (g -= d / 2), c === E ? g += h : c === D && (g += h / 2), g += hb.getScrollTop(), g + .5 | 0
    }, d.prototype.animateTo = function (a, b) {
        b = b || {};
        var d = Hb(), e = hb.getScrollTop();
        return ob = {
            startTop: e,
            topDiff: a - e,
            targetTop: a,
            duration: b.duration || x,
            startTime: d,
            endTime: d + (b.duration || x),
            easing: W[b.easing || w],
            done: b.done
        }, ob.topDiff || (ob.done && ob.done.call(hb, !1), ob = c), hb
    }, d.prototype.stopAnimateTo = function () {
        ob && ob.done && ob.done.call(hb, !0), ob = c
    }, d.prototype.isAnimatingTo = function () {
        return !!ob
    }, d.prototype.isMobile = function () {
        return Sb
    }, d.prototype.setScrollTop = function (b, c) {
        return sb = c === !0, Sb ? Tb = i.min(i.max(b, 0), Jb) : a.scrollTo(0, b), hb
    }, d.prototype.getScrollTop = function () {
        return Sb ? Tb : a.pageYOffset || e.scrollTop || f.scrollTop || 0
    }, d.prototype.getMaxScrollTop = function () {
        return Jb
    }, d.prototype.on = function (a, b) {
        return kb[a] = b, hb
    }, d.prototype.off = function (a) {
        return delete kb[a], hb
    }, d.prototype.destroy = function () {
        var a = V();
        a(vb), yb(), Eb(e, [t], [s, u, v]);
        for (var b = 0, d = ib.length; d > b; b++)fb(ib[b].element);
        e.style.overflow = f.style.overflow = "", e.style.height = f.style.height = "", jb && g.setStyle(jb, "transform", "none"), hb = c, jb = c, kb = c, lb = c, Jb = 0, Kb = 1, mb = c, nb = c, Lb = "down", Mb = -1, Ob = 0, Pb = 0, Qb = !1, ob = c, pb = c, qb = c, rb = c, sb = c, Rb = 0, tb = c, Sb = !1, Tb = 0, ub = c
    };
    var X = function () {
        var d, g, h, j, o, p, q, r, s, t, u, v;
        wb(e, [k, l, m, n].join(" "), function (a) {
            var e = a.changedTouches[0];
            for (j = a.target; 3 === j.nodeType;)j = j.parentNode;
            switch (o = e.clientY, p = e.clientX, t = a.timeStamp, G.test(j.tagName) || a.preventDefault(), a.type) {
                case k:
                    d && d.blur(), hb.stopAnimateTo(), d = j, g = q = o, h = p, s = t;
                    break;
                case l:
                    G.test(j.tagName) && b.activeElement !== j && a.preventDefault(), r = o - q, v = t - u, hb.setScrollTop(Tb - r, !0), q = o, u = t;
                    break;
                default:
                case m:
                case n:
                    var f = g - o, w = h - p, x = w * w + f * f;
                    if (49 > x) {
                        if (!G.test(d.tagName)) {
                            d.focus();
                            var y = b.createEvent("MouseEvents");
                            y.initMouseEvent("click", !0, !0, a.view, 1, e.screenX, e.screenY, e.clientX, e.clientY, a.ctrlKey, a.altKey, a.shiftKey, a.metaKey, 0, null), d.dispatchEvent(y)
                        }
                        return
                    }
                    d = c;
                    var z = r / v;
                    z = i.max(i.min(z, 3), -3);
                    var A = i.abs(z / nb), B = z * A + .5 * nb * A * A, C = hb.getScrollTop() - B, D = 0;
                    C > Jb ? (D = (Jb - C) / B, C = Jb) : 0 > C && (D = -C / B, C = 0), A *= 1 - D, hb.animateTo(C + .5 | 0, {
                        easing: "outCubic",
                        duration: A
                    })
            }
        }), a.scrollTo(0, 0), e.style.overflow = f.style.overflow = "hidden"
    }, Y = function () {
        var a, b, c, d, f, g, h, j, k, l, m, n = e.clientHeight, o = Bb();
        for (j = 0, k = ib.length; k > j; j++)for (a = ib[j], b = a.element, c = a.anchorTarget, d = a.keyFrames, f = 0, g = d.length; g > f; f++)h = d[f], l = h.offset, m = o[h.constant] || 0, h.frame = l, h.isPercentage && (l *= n, h.frame = l), "relative" === h.mode && (fb(b), h.frame = hb.relativeToAbsolute(c, h.anchors[0], h.anchors[1]) - l, fb(b, !0)), h.frame += m, lb && !h.isEnd && h.frame > Jb && (Jb = h.frame);
        for (Jb = i.max(Jb, Cb()), j = 0, k = ib.length; k > j; j++) {
            for (a = ib[j], d = a.keyFrames, f = 0, g = d.length; g > f; f++)h = d[f], m = o[h.constant] || 0, h.isEnd && (h.frame = Jb - h.offset + m);
            a.keyFrames.sort(Ib)
        }
    }, Z = function (a, b) {
        for (var c = 0, d = ib.length; d > c; c++) {
            var e, f, i = ib[c], j = i.element, k = i.smoothScrolling ? a : b, l = i.keyFrames, m = l.length, n = l[0], s = l[l.length - 1], t = k < n.frame, u = k > s.frame, v = t ? n : s, w = i.emitEvents, x = i.lastFrameIndex;
            if (t || u) {
                if (t && -1 === i.edge || u && 1 === i.edge)continue;
                switch (t ? (Eb(j, [p], [r, q]), w && x > -1 && (zb(j, n.eventType, Lb), i.lastFrameIndex = -1)) : (Eb(j, [r], [p, q]), w && m > x && (zb(j, s.eventType, Lb), i.lastFrameIndex = m)), i.edge = t ? -1 : 1, i.edgeStrategy) {
                    case"reset":
                        fb(j);
                        continue;
                    case"ease":
                        k = v.frame;
                        break;
                    default:
                    case"set":
                        var y = v.props;
                        for (e in y)h.call(y, e) && (f = eb(y[e].value), 0 === e.indexOf("@") ? j.setAttribute(e.substr(1), f) : g.setStyle(j, e, f));
                        continue
                }
            } else 0 !== i.edge && (Eb(j, [o, q], [p, r]), i.edge = 0);
            for (var z = 0; m - 1 > z; z++)if (k >= l[z].frame && k <= l[z + 1].frame) {
                var A = l[z], B = l[z + 1];
                for (e in A.props)if (h.call(A.props, e)) {
                    var C = (k - A.frame) / (B.frame - A.frame);
                    C = A.props[e].easing(C), f = db(A.props[e].value, B.props[e].value, C), f = eb(f), 0 === e.indexOf("@") ? j.setAttribute(e.substr(1), f) : g.setStyle(j, e, f)
                }
                w && x !== z && ("down" === Lb ? zb(j, A.eventType, Lb) : zb(j, B.eventType, Lb), i.lastFrameIndex = z);
                break
            }
        }
    }, $ = function () {
        Qb && (Qb = !1, Ab());
        var a, b, d = hb.getScrollTop(), e = Hb();
        if (ob)e >= ob.endTime ? (d = ob.targetTop, a = ob.done, ob = c) : (b = ob.easing((e - ob.startTime) / ob.duration), d = ob.startTop + b * ob.topDiff | 0), hb.setScrollTop(d, !0); else if (!sb) {
            var f = rb.targetTop - d;
            f && (rb = {
                startTop: Mb,
                topDiff: d - Mb,
                targetTop: d,
                startTime: Nb,
                endTime: Nb + qb
            }), e <= rb.endTime && (b = W.sqrt((e - rb.startTime) / qb), d = rb.startTop + b * rb.topDiff | 0)
        }
        if (Sb && jb && g.setStyle(jb, "transform", "translate(0, " + -Tb + "px) " + ub), sb || Mb !== d) {
            Lb = d > Mb ? "down" : Mb > d ? "up" : Lb, sb = !1;
            var h = {
                curTop: d,
                lastTop: Mb,
                maxTop: Jb,
                direction: Lb
            }, i = kb.beforerender && kb.beforerender.call(hb, h);
            i !== !1 && (Z(d, hb.getScrollTop()), Mb = d, kb.render && kb.render.call(hb, h)), a && a.call(hb, !1)
        }
        Nb = e
    }, _ = function (a) {
        for (var b = 0, c = a.keyFrames.length; c > b; b++) {
            for (var d, e, f, g, h = a.keyFrames[b], i = {}; null !== (g = J.exec(h.props));)f = g[1], e = g[2], d = f.match(K), null !== d ? (f = d[1], d = d[2]) : d = w, e = e.indexOf("!") ? ab(e) : [e.slice(1)], i[f] = {
                value: e,
                easing: W[d]
            };
            h.props = i
        }
    }, ab = function (a) {
        var b = [];
        return P.lastIndex = 0, a = a.replace(P, function (a) {
            return a.replace(N, function (a) {
                return a / 255 * 100 + "%"
            })
        }), S && (Q.lastIndex = 0, a = a.replace(Q, function (a) {
            return S + a
        })), a = a.replace(N, function (a) {
            return b.push(+a), "{?}"
        }), b.unshift(a), b
    }, bb = function (a) {
        var b, c, d = {};
        for (b = 0, c = a.keyFrames.length; c > b; b++)cb(a.keyFrames[b], d);
        for (d = {}, b = a.keyFrames.length - 1; b >= 0; b--)cb(a.keyFrames[b], d)
    }, cb = function (a, b) {
        var c;
        for (c in b)h.call(a.props, c) || (a.props[c] = b[c]);
        for (c in a.props)b[c] = a.props[c]
    }, db = function (a, b, c) {
        var d, e = a.length;
        if (e !== b.length)throw"Can't interpolate between \"" + a[0] + '" and "' + b[0] + '"';
        var f = [a[0]];
        for (d = 1; e > d; d++)f[d] = a[d] + (b[d] - a[d]) * c;
        return f
    }, eb = function (a) {
        var b = 1;
        return O.lastIndex = 0, a[0].replace(O, function () {
            return a[b++]
        })
    }, fb = function (a, b) {
        a = [].concat(a);
        for (var c, d, e = 0, f = a.length; f > e; e++)d = a[e], c = ib[d[F]], c && (b ? (d.style.cssText = c.dirtyStyleAttr, Eb(d, c.dirtyClassAttr)) : (c.dirtyStyleAttr = d.style.cssText, c.dirtyClassAttr = Db(d), d.style.cssText = c.styleAttr, Eb(d, c.classAttr)))
    }, gb = function () {
        ub = "translateZ(0)", g.setStyle(jb, "transform", ub);
        var a = j(jb), b = a.getPropertyValue("transform"), c = a.getPropertyValue(S + "transform"), d = b && "none" !== b || c && "none" !== c;
        d || (ub = "")
    };
    g.setStyle = function (a, b, c) {
        var d = a.style;
        if (b = b.replace(L, M).replace("-", ""), "zIndex" === b)d[b] = isNaN(c) ? c : "" + (0 | c); else if ("float" === b)d.styleFloat = d.cssFloat = c; else try {
            R && (d[R + b.slice(0, 1).toUpperCase() + b.slice(1)] = c), d[b] = c
        } catch (e) {
        }
    };
    var hb, ib, jb, kb, lb, mb, nb, ob, pb, qb, rb, sb, tb, ub, vb, wb = g.addEvent = function (b, c, d) {
        var e = function (b) {
            return b = b || a.event, b.target || (b.target = b.srcElement), b.preventDefault || (b.preventDefault = function () {
                b.returnValue = !1, b.defaultPrevented = !0
            }), d.call(this, b)
        };
        c = c.split(" ");
        for (var f, g = 0, h = c.length; h > g; g++)f = c[g], b.addEventListener ? b.addEventListener(f, d, !1) : b.attachEvent("on" + f, e), Ub.push({
            element: b,
            name: f,
            listener: d
        })
    }, xb = g.removeEvent = function (a, b, c) {
        b = b.split(" ");
        for (var d = 0, e = b.length; e > d; d++)a.removeEventListener ? a.removeEventListener(b[d], c, !1) : a.detachEvent("on" + b[d], c)
    }, yb = function () {
        for (var a, b = 0, c = Ub.length; c > b; b++)a = Ub[b], xb(a.element, a.name, a.listener);
        Ub = []
    }, zb = function (a, b, c) {
        kb.keyframe && kb.keyframe.call(hb, a, b, c)
    }, Ab = function () {
        var a = hb.getScrollTop();
        Jb = 0, lb && !Sb && (f.style.height = ""), Y(), lb && !Sb && (f.style.height = Jb + e.clientHeight + "px"), Sb ? hb.setScrollTop(i.min(hb.getScrollTop(), Jb)) : hb.setScrollTop(a, !0), sb = !0
    }, Bb = function () {
        var a, b, c = e.clientHeight, d = {};
        for (a in mb)b = mb[a], "function" == typeof b ? b = b.call(hb) : /p$/.test(b) && (b = b.slice(0, -1) / 100 * c), d[a] = b;
        return d
    }, Cb = function () {
        var a, b = 0;
        return jb && (b = i.max(jb.offsetHeight, jb.scrollHeight)), a = i.max(b, f.scrollHeight, f.offsetHeight, e.scrollHeight, e.offsetHeight, e.clientHeight), a - e.clientHeight
    }, Db = function (b) {
        var c = "className";
        return a.SVGElement && b instanceof a.SVGElement && (b = b[c], c = "baseVal"), b[c]
    }, Eb = function (b, d, e) {
        var f = "className";
        if (a.SVGElement && b instanceof a.SVGElement && (b = b[f], f = "baseVal"), e === c)return void(b[f] = d);
        for (var g = b[f], h = 0, i = e.length; i > h; h++)g = Gb(g).replace(Gb(e[h]), " ");
        g = Fb(g);
        for (var j = 0, k = d.length; k > j; j++)-1 === Gb(g).indexOf(Gb(d[j])) && (g += " " + d[j]);
        b[f] = Fb(g)
    }, Fb = function (a) {
        return a.replace(H, "")
    }, Gb = function (a) {
        return " " + a + " "
    }, Hb = Date.now || function () {
            return +new Date
        }, Ib = function (a, b) {
        return a.frame - b.frame
    }, Jb = 0, Kb = 1, Lb = "down", Mb = -1, Nb = Hb(), Ob = 0, Pb = 0, Qb = !1, Rb = 0, Sb = !1, Tb = 0, Ub = [];
    "function" == typeof define && define.amd ? define([], function () {
        return g
    }) : "undefined" != typeof module && module.exports ? module.exports = g : a.skrollr = g
}(window, document), function (a) {
    "use strict";
    a.fn.waterwheelCarousel = function (b) {
        function c() {
            t = {
                itemsContainer: a(r),
                totalItems: a(r).find("img").length,
                containerWidth: a(r).width(),
                containerHeight: a(r).height(),
                currentCenterItem: null,
                previousCenterItem: null,
                items: [],
                calculations: [],
                carouselRotationsLeft: 0,
                currentlyMoving: !1,
                itemsAnimating: 0,
                currentSpeed: s.speed,
                intervalTimer: null,
                currentDirection: "forward",
                leftItemsCount: 0,
                rightItemsCount: 0,
                performingSetup: !0
            }, t.itemsContainer.find("img").removeClass(s.activeClassName)
        }

        function d(a) {
            clearTimeout(t.autoPlayTimer), a || 0 === s.autoPlay || (t.autoPlayTimer = setTimeout(function () {
                q(s.autoPlay > 0 ? "forward" : "backward")
            }, Math.abs(s.autoPlay)))
        }

        function e(b) {
            if (s.preloadImages === !1)return void b();
            var c = t.itemsContainer.find("img"), d = 0, e = c.length;
            c.each(function () {
                a(this).bind("load", function () {
                    return d += 1, d === e ? void b() : void 0
                }), a(this).attr("src", a(this).attr("src")), this.complete && a(this).trigger("load")
            })
        }

        function f() {
            t.itemsContainer.find("img").each(function () {
                (void 0 == a(this).data("original_width") || s.forcedImageWidth > 0) && a(this).data("original_width", a(this).width()), (void 0 == a(this).data("original_height") || s.forcedImageHeight > 0) && a(this).data("original_height", a(this).height())
            })
        }

        function g() {
            s.forcedImageWidth && s.forcedImageHeight && t.itemsContainer.find("img").each(function () {
                a(this).width(s.forcedImageWidth), a(this).height(s.forcedImageHeight)
            })
        }

        function h() {
            t.itemsContainer.find("img:first");
            t.calculations[0] = {distance: 0, offset: 0, opacity: 1};
            for (var a = s.horizonOffset, b = s.separation, c = 1; c <= s.flankingItems + 2; c++)c > 1 && (a *= s.horizonOffsetMultiplier, b *= s.separationMultiplier), t.calculations[c] = {
                distance: t.calculations[c - 1].distance + b,
                offset: t.calculations[c - 1].offset + a,
                opacity: t.calculations[c - 1].opacity * s.opacityMultiplier
            };
            s.edgeFadeEnabled ? t.calculations[s.flankingItems + 1].opacity = 0 : t.calculations[s.flankingItems + 1] = {
                distance: 0,
                offset: 0,
                opacity: 0
            }
        }

        function i() {
            t.items = t.itemsContainer.find("img");
            for (var b = 0; b < t.totalItems; b++)t.items[b] = a(t.items[b]);
            0 === s.horizon && (s.horizon = "horizontal" === s.orientation ? t.containerHeight / 2 : t.containerWidth / 2), t.itemsContainer.css("position", "relative").find("img").each(function () {
                var b, c;
                "horizontal" === s.orientation ? (b = t.containerWidth / 2 - a(this).data("original_width") / 2, c = s.horizon - a(this).data("original_height") / 2) : (b = s.horizon - a(this).data("original_width") / 2, c = t.containerHeight / 2 - a(this).data("original_height") / 2), a(this).css({
                    left: b,
                    top: c,
                    visibility: "visible",
                    position: "absolute",
                    "z-index": 0,
                    opacity: 0
                }).data({top: c, left: b, oldPosition: 0, currentPosition: 0, depth: 0, opacity: 0}).show()
            })
        }

        function j() {
            s.startingItem = 0 === s.startingItem ? Math.round(t.totalItems / 2) : s.startingItem, t.rightItemsCount = Math.ceil((t.totalItems - 1) / 2), t.leftItemsCount = Math.floor((t.totalItems - 1) / 2), t.carouselRotationsLeft = 1, l(t.items[s.startingItem - 1], 0), t.items[s.startingItem - 1].css("opacity", 1);
            for (var a = s.startingItem - 1, b = 1; b <= t.rightItemsCount; b++)a < t.totalItems - 1 ? a += 1 : a = 0, t.items[a].css("opacity", 1), l(t.items[a], b);
            for (var a = s.startingItem - 1, b = -1; b >= -1 * t.leftItemsCount; b--)a > 0 ? a -= 1 : a = t.totalItems - 1, t.items[a].css("opacity", 1), l(t.items[a], b)
        }

        function k(a, b) {
            var c = Math.abs(b);
            if (c < s.flankingItems + 1)var d = t.calculations[c]; else var d = t.calculations[s.flankingItems + 1];
            var e = Math.pow(s.sizeMultiplier, c), f = e * a.data("original_width"), g = e * a.data("original_height"), h = (Math.abs(a.width() - f), Math.abs(a.height() - g), d.offset), i = d.distance;
            if (0 > b && (i *= -1), "horizontal" == s.orientation)var j = t.containerWidth / 2, k = j + i - f / 2, l = s.horizon - h - g / 2; else var j = t.containerHeight / 2, k = s.horizon - h - f / 2, l = j + i - g / 2;
            var m;
            m = 0 === b ? 1 : d.opacity;
            var n = s.flankingItems + 2 - c;
            a.data("width", f), a.data("height", g), a.data("top", l), a.data("left", k), a.data("oldPosition", a.data("currentPosition")), a.data("depth", n), a.data("opacity", m)
        }

        function l(a, b) {
            Math.abs(b) <= s.flankingItems + 1 ? (k(a, b), t.itemsAnimating++, a.css("z-index", a.data().depth).animate({
                left: a.data().left,
                width: a.data().width,
                height: a.data().height,
                top: a.data().top,
                opacity: a.data().opacity
            }, t.currentSpeed, s.animationEasing, function () {
                m(a, b)
            })) : (a.data("currentPosition", b), 0 === a.data("oldPosition") && a.css({
                left: a.data().left,
                width: a.data().width,
                height: a.data().height,
                top: a.data().top,
                opacity: a.data().opacity,
                "z-index": a.data().depth
            }))
        }

        function m(a, b) {
            t.itemsAnimating--, a.data("currentPosition", b), 0 === b && (t.currentCenterItem = a), 0 === t.itemsAnimating && (t.carouselRotationsLeft -= 1, t.currentlyMoving = !1, t.carouselRotationsLeft > 0 ? n(0) : (t.currentSpeed = s.speed, t.currentCenterItem.addClass(s.activeClassName), t.performingSetup === !1 && (s.movedToCenter(t.currentCenterItem), s.movedFromCenter(t.previousCenterItem)), t.performingSetup = !1, d()))
        }

        function n(b) {
            if (t.currentlyMoving === !1) {
                t.currentCenterItem.removeClass(s.activeClassName), t.currentlyMoving = !0, t.itemsAnimating = 0, t.carouselRotationsLeft += b, s.quickerForFurther === !0 && (b > 1 && (t.currentSpeed = s.speed / b), t.currentSpeed = t.currentSpeed < 100 ? 100 : t.currentSpeed);
                for (var c = 0; c < t.totalItems; c++) {
                    var d, e = a(t.items[c]), f = e.data("currentPosition");
                    d = "forward" == t.currentDirection ? f - 1 : f + 1;
                    var g = d > 0 ? t.rightItemsCount : t.leftItemsCount;
                    Math.abs(d) > g && (d = -1 * f, t.totalItems % 2 == 0 && (d += 1)), l(e, d)
                }
            }
        }

        function o() {
            var a = t.currentCenterItem.next();
            return a.length <= 0 && (a = t.currentCenterItem.parent().children().first()), a
        }

        function p() {
            var a = t.currentCenterItem.prev();
            return a.length <= 0 && (a = t.currentCenterItem.parent().children().last()), a
        }

        function q(a) {
            t.currentlyMoving === !1 && (t.previousCenterItem = t.currentCenterItem, s.movingFromCenter(t.currentCenterItem), "backward" == a ? (s.movingToCenter(p()), t.currentDirection = "backward") : "forward" == a && (s.movingToCenter(o()), t.currentDirection = "forward")), n(1)
        }

        if (this.length > 1)return this.each(function () {
            a(this).waterwheelCarousel(b)
        }), this;
        var r = this, s = {}, t = {};
        return a(this).find("img").bind("click", function () {
            var b = a(this).data().currentPosition;
            if (0 != s.imageNav && !(Math.abs(b) >= s.flankingItems + 1 || t.currentlyMoving)) {
                t.previousCenterItem = t.currentCenterItem, d(!0), s.autoPlay = 0;
                var c = Math.abs(b);
                0 == b ? s.clickedCenter(a(this)) : (s.movingFromCenter(t.currentCenterItem), s.movingToCenter(a(this)), 0 > b ? (t.currentDirection = "backward", n(c)) : b > 0 && (t.currentDirection = "forward", n(c)))
            }
        }), a(this).find("a").bind("click", function (b) {
            var c = 0 == a(this).find("img").data("currentPosition");
            return 1 === s.linkHandling || 2 === s.linkHandling && !c ? (b.preventDefault(), !1) : void 0
        }), a(document).keydown(function (a) {
            return s.keyboardNav && (37 === a.which && "horizontal" == s.orientation || 38 === a.which && "vertical" == s.orientation ? (d(!0), s.autoPlay = 0, q("backward")) : (39 === a.which && "horizontal" == s.orientation || 40 === a.which && "vertical" == s.orientation) && (d(!0), s.autoPlay = 0, q("forward")), s.keyboardNavOverride && ("horizontal" == s.orientation && (37 === a.which || 39 === a.which) || "vertical" == s.orientation && (38 === a.which || 40 === a.which))) ? (a.preventDefault(), !1) : void 0
        }), this.reload = function (b) {
            if ("object" == typeof b); else;
            s = a.extend({}, a.fn.waterwheelCarousel.defaults, b), c(), t.itemsContainer.find("img").hide(), g(), e(function () {
                f(), h(), i(), j()
            })
        }, this.next = function () {
            d(!0), s.autoPlay = 0, q("forward")
        }, this.prev = function () {
            d(!0), s.autoPlay = 0, q("backward")
        }, this.reload(b), this
    }, a.fn.waterwheelCarousel.defaults = {
        startingItem: 1,
        separation: 175,
        separationMultiplier: .6,
        horizonOffset: 0,
        horizonOffsetMultiplier: 1,
        sizeMultiplier: .7,
        opacityMultiplier: .8,
        horizon: 0,
        flankingItems: 3,
        speed: 300,
        animationEasing: "linear",
        quickerForFurther: !0,
        edgeFadeEnabled: !1,
        linkHandling: 2,
        autoPlay: 0,
        orientation: "horizontal",
        activeClassName: "carousel-center",
        keyboardNav: !1,
        keyboardNavOverride: !0,
        imageNav: !0,
        preloadImages: !0,
        forcedImageWidth: 0,
        forcedImageHeight: 0,
        movingToCenter: a.noop,
        movedToCenter: a.noop,
        clickedCenter: a.noop,
        movingFromCenter: a.noop,
        movedFromCenter: a.noop
    }
}(jQuery), function () {
    function a(a) {
        this._value = a
    }

    function b(a, b, c, d) {
        var e, f, g = Math.pow(10, b);
        return f = (c(a * g) / g).toFixed(b), d && (e = new RegExp("0{1," + d + "}$"), f = f.replace(e, "")), f
    }

    function c(a, b, c) {
        var d;
        return d = b.indexOf("$") > -1 ? e(a, b, c) : b.indexOf("%") > -1 ? f(a, b, c) : b.indexOf(":") > -1 ? g(a, b) : i(a._value, b, c)
    }

    function d(a, b) {
        var c, d, e, f, g, i = b, j = ["KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"], k = !1;
        if (b.indexOf(":") > -1)a._value = h(b); else if (b === q)a._value = 0; else {
            for ("." !== o[p].delimiters.decimal && (b = b.replace(/\./g, "").replace(o[p].delimiters.decimal, ".")), c = new RegExp("[^a-zA-Z]" + o[p].abbreviations.thousand + "(?:\\)|(\\" + o[p].currency.symbol + ")?(?:\\))?)?$"), d = new RegExp("[^a-zA-Z]" + o[p].abbreviations.million + "(?:\\)|(\\" + o[p].currency.symbol + ")?(?:\\))?)?$"), e = new RegExp("[^a-zA-Z]" + o[p].abbreviations.billion + "(?:\\)|(\\" + o[p].currency.symbol + ")?(?:\\))?)?$"), f = new RegExp("[^a-zA-Z]" + o[p].abbreviations.trillion + "(?:\\)|(\\" + o[p].currency.symbol + ")?(?:\\))?)?$"), g = 0; g <= j.length && !(k = b.indexOf(j[g]) > -1 ? Math.pow(1024, g + 1) : !1); g++);
            a._value = (k ? k : 1) * (i.match(c) ? Math.pow(10, 3) : 1) * (i.match(d) ? Math.pow(10, 6) : 1) * (i.match(e) ? Math.pow(10, 9) : 1) * (i.match(f) ? Math.pow(10, 12) : 1) * (b.indexOf("%") > -1 ? .01 : 1) * ((b.split("-").length + Math.min(b.split("(").length - 1, b.split(")").length - 1)) % 2 ? 1 : -1) * Number(b.replace(/[^0-9\.]+/g, "")), a._value = k ? Math.ceil(a._value) : a._value
        }
        return a._value
    }

    function e(a, b, c) {
        var d, e, f = b.indexOf("$"), g = b.indexOf("("), h = b.indexOf("-"), j = "";
        return b.indexOf(" $") > -1 ? (j = " ", b = b.replace(" $", "")) : b.indexOf("$ ") > -1 ? (j = " ", b = b.replace("$ ", "")) : b = b.replace("$", ""), e = i(a._value, b, c), 1 >= f ? e.indexOf("(") > -1 || e.indexOf("-") > -1 ? (e = e.split(""), d = 1, (g > f || h > f) && (d = 0), e.splice(d, 0, o[p].currency.symbol + j), e = e.join("")) : e = o[p].currency.symbol + j + e : e.indexOf(")") > -1 ? (e = e.split(""), e.splice(-1, 0, j + o[p].currency.symbol), e = e.join("")) : e = e + j + o[p].currency.symbol, e
    }

    function f(a, b, c) {
        var d, e = "", f = 100 * a._value;
        return b.indexOf(" %") > -1 ? (e = " ", b = b.replace(" %", "")) : b = b.replace("%", ""), d = i(f, b, c), d.indexOf(")") > -1 ? (d = d.split(""), d.splice(-1, 0, e + "%"), d = d.join("")) : d = d + e + "%", d
    }

    function g(a) {
        var b = Math.floor(a._value / 60 / 60), c = Math.floor((a._value - 60 * b * 60) / 60), d = Math.round(a._value - 60 * b * 60 - 60 * c);
        return b + ":" + (10 > c ? "0" + c : c) + ":" + (10 > d ? "0" + d : d)
    }

    function h(a) {
        var b = a.split(":"), c = 0;
        return 3 === b.length ? (c += 60 * Number(b[0]) * 60, c += 60 * Number(b[1]), c += Number(b[2])) : 2 === b.length && (c += 60 * Number(b[0]), c += Number(b[1])), Number(c)
    }

    function i(a, c, d) {
        var e, f, g, h, i, j, k = !1, l = !1, m = !1, n = "", r = !1, s = !1, t = !1, u = !1, v = !1, w = "", x = "", y = Math.abs(a), z = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"], A = "", B = !1;
        if (0 === a && null !== q)return q;
        if (c.indexOf("(") > -1 ? (k = !0, c = c.slice(1, -1)) : c.indexOf("+") > -1 && (l = !0, c = c.replace(/\+/g, "")), c.indexOf("a") > -1 && (r = c.indexOf("aK") >= 0, s = c.indexOf("aM") >= 0, t = c.indexOf("aB") >= 0, u = c.indexOf("aT") >= 0, v = r || s || t || u, c.indexOf(" a") > -1 ? (n = " ", c = c.replace(" a", "")) : c = c.replace("a", ""), y >= Math.pow(10, 12) && !v || u ? (n += o[p].abbreviations.trillion, a /= Math.pow(10, 12)) : y < Math.pow(10, 12) && y >= Math.pow(10, 9) && !v || t ? (n += o[p].abbreviations.billion, a /= Math.pow(10, 9)) : y < Math.pow(10, 9) && y >= Math.pow(10, 6) && !v || s ? (n += o[p].abbreviations.million, a /= Math.pow(10, 6)) : (y < Math.pow(10, 6) && y >= Math.pow(10, 3) && !v || r) && (n += o[p].abbreviations.thousand, a /= Math.pow(10, 3))), c.indexOf("b") > -1)for (c.indexOf(" b") > -1 ? (w = " ", c = c.replace(" b", "")) : c = c.replace("b", ""), g = 0; g <= z.length; g++)if (e = Math.pow(1024, g), f = Math.pow(1024, g + 1), a >= e && f > a) {
            w += z[g], e > 0 && (a /= e);
            break
        }
        return c.indexOf("o") > -1 && (c.indexOf(" o") > -1 ? (x = " ", c = c.replace(" o", "")) : c = c.replace("o", ""), x += o[p].ordinal(a)), c.indexOf("[.]") > -1 && (m = !0, c = c.replace("[.]", ".")), h = a.toString().split(".")[0], i = c.split(".")[1], j = c.indexOf(","), i ? (i.indexOf("[") > -1 ? (i = i.replace("]", ""), i = i.split("["), A = b(a, i[0].length + i[1].length, d, i[1].length)) : A = b(a, i.length, d), h = A.split(".")[0], A = A.split(".")[1].length ? o[p].delimiters.decimal + A.split(".")[1] : "", m && 0 === Number(A.slice(1)) && (A = "")) : h = b(a, null, d), h.indexOf("-") > -1 && (h = h.slice(1), B = !0), j > -1 && (h = h.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1" + o[p].delimiters.thousands)), 0 === c.indexOf(".") && (h = ""), (k && B ? "(" : "") + (!k && B ? "-" : "") + (!B && l ? "+" : "") + h + A + (x ? x : "") + (n ? n : "") + (w ? w : "") + (k && B ? ")" : "")
    }

    function j(a, b) {
        o[a] = b
    }

    function k(a) {
        var b = a.toString().split(".");
        return b.length < 2 ? 1 : Math.pow(10, b[1].length)
    }

    function l() {
        var a = Array.prototype.slice.call(arguments);
        return a.reduce(function (a, b) {
            var c = k(a), d = k(b);
            return c > d ? c : d
        }, -1 / 0)
    }

    var m, n = "1.5.3", o = {}, p = "en", q = null, r = "0,0", s = "undefined" != typeof module && module.exports;
    m = function (b) {
        return m.isNumeral(b) ? b = b.value() : 0 === b || "undefined" == typeof b ? b = 0 : Number(b) || (b = m.fn.unformat(b)), new a(Number(b))
    }, m.version = n, m.isNumeral = function (b) {
        return b instanceof a
    }, m.language = function (a, b) {
        if (!a)return p;
        if (a && !b) {
            if (!o[a])throw new Error("Unknown language : " + a);
            p = a
        }
        return (b || !o[a]) && j(a, b), m
    }, m.languageData = function (a) {
        if (!a)return o[p];
        if (!o[a])throw new Error("Unknown language : " + a);
        return o[a]
    }, m.language("en", {
        delimiters: {thousands: ",", decimal: "."},
        abbreviations: {thousand: "k", million: "m", billion: "b", trillion: "t"},
        ordinal: function (a) {
            var b = a % 10;
            return 1 === ~~(a % 100 / 10) ? "th" : 1 === b ? "st" : 2 === b ? "nd" : 3 === b ? "rd" : "th"
        },
        currency: {symbol: "$"}
    }), m.zeroFormat = function (a) {
        q = "string" == typeof a ? a : null
    }, m.defaultFormat = function (a) {
        r = "string" == typeof a ? a : "0.0"
    }, "function" != typeof Array.prototype.reduce && (Array.prototype.reduce = function (a, b) {
        "use strict";
        if (null === this || "undefined" == typeof this)throw new TypeError("Array.prototype.reduce called on null or undefined");
        if ("function" != typeof a)throw new TypeError(a + " is not a function");
        var c, d, e = this.length >>> 0, f = !1;
        for (1 < arguments.length && (d = b, f = !0), c = 0; e > c; ++c)this.hasOwnProperty(c) && (f ? d = a(d, this[c], c, this) : (d = this[c], f = !0));
        if (!f)throw new TypeError("Reduce of empty array with no initial value");
        return d
    }), m.fn = a.prototype = {
        clone: function () {
            return m(this)
        }, format: function (a, b) {
            return c(this, a ? a : r, void 0 !== b ? b : Math.round)
        }, unformat: function (a) {
            return "[object Number]" === Object.prototype.toString.call(a) ? a : d(this, a ? a : r)
        }, value: function () {
            return this._value
        }, valueOf: function () {
            return this._value
        }, set: function (a) {
            return this._value = Number(a), this
        }, add: function (a) {
            function b(a, b) {
                return a + c * b
            }

            var c = l.call(null, this._value, a);
            return this._value = [this._value, a].reduce(b, 0) / c, this
        }, subtract: function (a) {
            function b(a, b) {
                return a - c * b
            }

            var c = l.call(null, this._value, a);
            return this._value = [a].reduce(b, this._value * c) / c, this
        }, multiply: function (a) {
            function b(a, b) {
                var c = l(a, b);
                return a * c * b * c / (c * c)
            }

            return this._value = [this._value, a].reduce(b, 1), this
        }, divide: function (a) {
            function b(a, b) {
                var c = l(a, b);
                return a * c / (b * c)
            }

            return this._value = [this._value, a].reduce(b), this
        }, difference: function (a) {
            return Math.abs(m(this._value).subtract(a).value())
        }
    }, s && (module.exports = m), "undefined" == typeof ender && (this.numeral = m), "function" == typeof define && define.amd && define([], function () {
        return m
    })
}.call(this), !function (a) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], a) : "undefined" != typeof exports ? module.exports = a(require("jquery")) : a(jQuery)
}(function (a) {
    "use strict";
    var b = window.Slick || {};
    b = function () {
        function b(b, d) {
            var e, f, g = this;
            if (g.defaults = {
                    accessibility: !0,
                    adaptiveHeight: !1,
                    appendArrows: a(b),
                    appendDots: a(b),
                    arrows: !0,
                    asNavFor: null,
                    prevArrow: '<button type="button" data-role="none" class="slick-prev">Previous</button>',
                    nextArrow: '<button type="button" data-role="none" class="slick-next">Next</button>',
                    autoplay: !1,
                    autoplaySpeed: 3e3,
                    centerMode: !1,
                    centerPadding: "50px",
                    cssEase: "ease",
                    customPaging: function (a, b) {
                        return '<button type="button" data-role="none">' + (b + 1) + "</button>"
                    },
                    dots: !1,
                    dotsClass: "slick-dots",
                    draggable: !0,
                    easing: "linear",
                    fade: !1,
                    focusOnSelect: !1,
                    infinite: !0,
                    initialSlide: 0,
                    lazyLoad: "ondemand",
                    onBeforeChange: null,
                    onAfterChange: null,
                    onInit: null,
                    onReInit: null,
                    onSetPosition: null,
                    pauseOnHover: !0,
                    pauseOnDotsHover: !1,
                    respondTo: "window",
                    responsive: null,
                    rtl: !1,
                    slide: "div",
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    speed: 500,
                    swipe: !0,
                    swipeToSlide: !1,
                    touchMove: !0,
                    touchThreshold: 5,
                    useCSS: !0,
                    variableWidth: !1,
                    vertical: !1,
                    waitForAnimate: !0
                }, g.initials = {
                    animating: !1,
                    dragging: !1,
                    autoPlayTimer: null,
                    currentDirection: 0,
                    currentLeft: null,
                    currentSlide: 0,
                    direction: 1,
                    $dots: null,
                    listWidth: null,
                    listHeight: null,
                    loadIndex: 0,
                    $nextArrow: null,
                    $prevArrow: null,
                    slideCount: null,
                    slideWidth: null,
                    $slideTrack: null,
                    $slides: null,
                    sliding: !1,
                    slideOffset: 0,
                    swipeLeft: null,
                    $list: null,
                    touchObject: {},
                    transformsEnabled: !1
                }, a.extend(g, g.initials), g.activeBreakpoint = null, g.animType = null, g.animProp = null, g.breakpoints = [], g.breakpointSettings = [], g.cssTransitions = !1, g.paused = !1, g.positionProp = null, g.respondTo = null, g.shouldClick = !0, g.$slider = a(b), g.$slidesCache = null, g.transformType = null, g.transitionType = null, g.windowWidth = 0, g.windowTimer = null, g.options = a.extend({}, g.defaults, d), g.currentSlide = g.options.initialSlide, g.originalSettings = g.options, e = g.options.responsive || null, e && e.length > -1) {
                g.respondTo = g.options.respondTo || "window";
                for (f in e)e.hasOwnProperty(f) && (g.breakpoints.push(e[f].breakpoint), g.breakpointSettings[e[f].breakpoint] = e[f].settings);
                g.breakpoints.sort(function (a, b) {
                    return b - a
                })
            }
            g.autoPlay = a.proxy(g.autoPlay, g), g.autoPlayClear = a.proxy(g.autoPlayClear, g), g.changeSlide = a.proxy(g.changeSlide, g), g.clickHandler = a.proxy(g.clickHandler, g), g.selectHandler = a.proxy(g.selectHandler, g), g.setPosition = a.proxy(g.setPosition, g), g.swipeHandler = a.proxy(g.swipeHandler, g), g.dragHandler = a.proxy(g.dragHandler, g), g.keyHandler = a.proxy(g.keyHandler, g), g.autoPlayIterator = a.proxy(g.autoPlayIterator, g), g.instanceUid = c++, g.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, g.init(), g.checkResponsive()
        }

        var c = 0;
        return b
    }(), b.prototype.addSlide = function (b, c, d) {
        var e = this;
        if ("boolean" == typeof c)d = c, c = null; else if (0 > c || c >= e.slideCount)return !1;
        e.unload(), "number" == typeof c ? 0 === c && 0 === e.$slides.length ? a(b).appendTo(e.$slideTrack) : d ? a(b).insertBefore(e.$slides.eq(c)) : a(b).insertAfter(e.$slides.eq(c)) : d === !0 ? a(b).prependTo(e.$slideTrack) : a(b).appendTo(e.$slideTrack), e.$slides = e.$slideTrack.children(this.options.slide), e.$slideTrack.children(this.options.slide).detach(), e.$slideTrack.append(e.$slides), e.$slides.each(function (b, c) {
            a(c).attr("index", b)
        }), e.$slidesCache = e.$slides, e.reinit()
    }, b.prototype.animateSlide = function (b, c) {
        var d = {}, e = this;
        if (1 === e.options.slidesToShow && e.options.adaptiveHeight === !0 && e.options.vertical === !1) {
            var f = e.$slides.eq(e.currentSlide).outerHeight(!0);
            e.$list.animate({height: f}, e.options.speed)
        }
        e.options.rtl === !0 && e.options.vertical === !1 && (b = -b), e.transformsEnabled === !1 ? e.options.vertical === !1 ? e.$slideTrack.animate({left: b}, e.options.speed, e.options.easing, c) : e.$slideTrack.animate({top: b}, e.options.speed, e.options.easing, c) : e.cssTransitions === !1 ? a({animStart: e.currentLeft}).animate({animStart: b}, {
            duration: e.options.speed,
            easing: e.options.easing,
            step: function (a) {
                e.options.vertical === !1 ? (d[e.animType] = "translate(" + a + "px, 0px)", e.$slideTrack.css(d)) : (d[e.animType] = "translate(0px," + a + "px)", e.$slideTrack.css(d))
            },
            complete: function () {
                c && c.call()
            }
        }) : (e.applyTransition(), d[e.animType] = e.options.vertical === !1 ? "translate3d(" + b + "px, 0px, 0px)" : "translate3d(0px," + b + "px, 0px)", e.$slideTrack.css(d), c && setTimeout(function () {
            e.disableTransition(), c.call()
        }, e.options.speed))
    }, b.prototype.asNavFor = function (b) {
        var c = this, d = null != c.options.asNavFor ? a(c.options.asNavFor).getSlick() : null;
        null != d && d.slideHandler(b, !0)
    }, b.prototype.applyTransition = function (a) {
        var b = this, c = {};
        c[b.transitionType] = b.options.fade === !1 ? b.transformType + " " + b.options.speed + "ms " + b.options.cssEase : "opacity " + b.options.speed + "ms " + b.options.cssEase, b.options.fade === !1 ? b.$slideTrack.css(c) : b.$slides.eq(a).css(c)
    }, b.prototype.autoPlay = function () {
        var a = this;
        a.autoPlayTimer && clearInterval(a.autoPlayTimer), a.slideCount > a.options.slidesToShow && a.paused !== !0 && (a.autoPlayTimer = setInterval(a.autoPlayIterator, a.options.autoplaySpeed))
    }, b.prototype.autoPlayClear = function () {
        var a = this;
        a.autoPlayTimer && clearInterval(a.autoPlayTimer)
    }, b.prototype.autoPlayIterator = function () {
        var a = this;
        a.options.infinite === !1 ? 1 === a.direction ? (a.currentSlide + 1 === a.slideCount - 1 && (a.direction = 0), a.slideHandler(a.currentSlide + a.options.slidesToScroll)) : (0 === a.currentSlide - 1 && (a.direction = 1), a.slideHandler(a.currentSlide - a.options.slidesToScroll)) : a.slideHandler(a.currentSlide + a.options.slidesToScroll)
    }, b.prototype.buildArrows = function () {
        var b = this;
        b.options.arrows === !0 && b.slideCount > b.options.slidesToShow && (b.$prevArrow = a(b.options.prevArrow), b.$nextArrow = a(b.options.nextArrow), b.htmlExpr.test(b.options.prevArrow) && b.$prevArrow.appendTo(b.options.appendArrows), b.htmlExpr.test(b.options.nextArrow) && b.$nextArrow.appendTo(b.options.appendArrows), b.options.infinite !== !0 && b.$prevArrow.addClass("slick-disabled"))
    }, b.prototype.buildDots = function () {
        var b, c, d = this;
        if (d.options.dots === !0 && d.slideCount > d.options.slidesToShow) {
            for (c = '<ul class="' + d.options.dotsClass + '">', b = 0; b <= d.getDotCount(); b += 1)c += "<li>" + d.options.customPaging.call(this, d, b) + "</li>";
            c += "</ul>", d.$dots = a(c).appendTo(d.options.appendDots), d.$dots.find("li").first().addClass("slick-active")
        }
    }, b.prototype.buildOut = function () {
        var b = this;
        b.$slides = b.$slider.children(b.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), b.slideCount = b.$slides.length, b.$slides.each(function (b, c) {
            a(c).attr("index", b)
        }), b.$slidesCache = b.$slides, b.$slider.addClass("slick-slider"), b.$slideTrack = 0 === b.slideCount ? a('<div class="slick-track"/>').appendTo(b.$slider) : b.$slides.wrapAll('<div class="slick-track"/>').parent(), b.$list = b.$slideTrack.wrap('<div class="slick-list"/>').parent(), b.$slideTrack.css("opacity", 0), b.options.centerMode === !0 && (b.options.slidesToScroll = 1), a("img[data-lazy]", b.$slider).not("[src]").addClass("slick-loading"), b.setupInfinite(), b.buildArrows(), b.buildDots(), b.updateDots(), b.options.accessibility === !0 && b.$list.prop("tabIndex", 0), b.setSlideClasses("number" == typeof this.currentSlide ? this.currentSlide : 0), b.options.draggable === !0 && b.$list.addClass("draggable")
    }, b.prototype.checkResponsive = function () {
        var b, c, d, e = this, f = e.$slider.width(), g = window.innerWidth || a(window).width();
        if ("window" === e.respondTo ? d = g : "slider" === e.respondTo ? d = f : "min" === e.respondTo && (d = Math.min(g, f)), e.originalSettings.responsive && e.originalSettings.responsive.length > -1 && null !== e.originalSettings.responsive) {
            c = null;
            for (b in e.breakpoints)e.breakpoints.hasOwnProperty(b) && d < e.breakpoints[b] && (c = e.breakpoints[b]);
            null !== c ? null !== e.activeBreakpoint ? c !== e.activeBreakpoint && (e.activeBreakpoint = c, e.options = a.extend({}, e.originalSettings, e.breakpointSettings[c]), e.refresh()) : (e.activeBreakpoint = c, e.options = a.extend({}, e.originalSettings, e.breakpointSettings[c]), e.refresh()) : null !== e.activeBreakpoint && (e.activeBreakpoint = null, e.options = e.originalSettings, e.refresh())
        }
    }, b.prototype.changeSlide = function (b, c) {
        var d, e, f, g, h, i = this, j = a(b.target);
        switch (j.is("a") && b.preventDefault(), f = 0 !== i.slideCount % i.options.slidesToScroll, d = f ? 0 : (i.slideCount - i.currentSlide) % i.options.slidesToScroll, b.data.message) {
            case"previous":
                e = 0 === d ? i.options.slidesToScroll : i.options.slidesToShow - d, i.slideCount > i.options.slidesToShow && i.slideHandler(i.currentSlide - e, !1, c);
                break;
            case"next":
                e = 0 === d ? i.options.slidesToScroll : d, i.slideCount > i.options.slidesToShow && i.slideHandler(i.currentSlide + e, !1, c);
                break;
            case"index":
                var k = 0 === b.data.index ? 0 : b.data.index || a(b.target).parent().index() * i.options.slidesToScroll;
                if (g = i.getNavigableIndexes(), h = 0, g[k] && g[k] === k)if (k > g[g.length - 1])k = g[g.length - 1]; else for (var l in g) {
                    if (k < g[l]) {
                        k = h;
                        break
                    }
                    h = g[l]
                }
                i.slideHandler(k, !1, c);
            default:
                return
        }
    }, b.prototype.clickHandler = function (a) {
        var b = this;
        b.shouldClick === !1 && (a.stopImmediatePropagation(), a.stopPropagation(), a.preventDefault())
    }, b.prototype.destroy = function () {
        var b = this;
        b.autoPlayClear(), b.touchObject = {}, a(".slick-cloned", b.$slider).remove(), b.$dots && b.$dots.remove(), b.$prevArrow && "object" != typeof b.options.prevArrow && b.$prevArrow.remove(), b.$nextArrow && "object" != typeof b.options.nextArrow && b.$nextArrow.remove(), b.$slides.parent().hasClass("slick-track") && b.$slides.unwrap().unwrap(), b.$slides.removeClass("slick-slide slick-active slick-center slick-visible").removeAttr("index").css({
            position: "",
            left: "",
            top: "",
            zIndex: "",
            opacity: "",
            width: ""
        }), b.$slider.removeClass("slick-slider"), b.$slider.removeClass("slick-initialized"), b.$list.off(".slick"), a(window).off(".slick-" + b.instanceUid), a(document).off(".slick-" + b.instanceUid)
    }, b.prototype.disableTransition = function (a) {
        var b = this, c = {};
        c[b.transitionType] = "", b.options.fade === !1 ? b.$slideTrack.css(c) : b.$slides.eq(a).css(c)
    }, b.prototype.fadeSlide = function (a, b, c) {
        var d = this;
        d.cssTransitions === !1 ? (d.$slides.eq(b).css({zIndex: 1e3}), d.$slides.eq(b).animate({opacity: 1}, d.options.speed, d.options.easing, c), d.$slides.eq(a).animate({opacity: 0}, d.options.speed, d.options.easing)) : (d.applyTransition(b), d.applyTransition(a), d.$slides.eq(b).css({
            opacity: 1,
            zIndex: 1e3
        }), d.$slides.eq(a).css({opacity: 0}), c && setTimeout(function () {
            d.disableTransition(b), d.disableTransition(a), c.call()
        }, d.options.speed))
    }, b.prototype.filterSlides = function (a) {
        var b = this;
        null !== a && (b.unload(), b.$slideTrack.children(this.options.slide).detach(), b.$slidesCache.filter(a).appendTo(b.$slideTrack), b.reinit())
    }, b.prototype.getCurrent = function () {
        var a = this;
        return a.currentSlide
    }, b.prototype.getDotCount = function () {
        var a = this, b = 0, c = 0, d = 0;
        if (a.options.infinite === !0)d = Math.ceil(a.slideCount / a.options.slidesToScroll); else for (; b < a.slideCount;)++d, b = c + a.options.slidesToShow, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow;
        return d - 1
    }, b.prototype.getLeft = function (a) {
        var b, c, d, e = this, f = 0;
        return e.slideOffset = 0, c = e.$slides.first().outerHeight(), e.options.infinite === !0 ? (e.slideCount > e.options.slidesToShow && (e.slideOffset = -1 * e.slideWidth * e.options.slidesToShow, f = -1 * c * e.options.slidesToShow), 0 !== e.slideCount % e.options.slidesToScroll && a + e.options.slidesToScroll > e.slideCount && e.slideCount > e.options.slidesToShow && (a > e.slideCount ? (e.slideOffset = -1 * (e.options.slidesToShow - (a - e.slideCount)) * e.slideWidth, f = -1 * (e.options.slidesToShow - (a - e.slideCount)) * c) : (e.slideOffset = -1 * e.slideCount % e.options.slidesToScroll * e.slideWidth, f = -1 * e.slideCount % e.options.slidesToScroll * c))) : a + e.options.slidesToShow > e.slideCount && (e.slideOffset = (a + e.options.slidesToShow - e.slideCount) * e.slideWidth, f = (a + e.options.slidesToShow - e.slideCount) * c), e.slideCount <= e.options.slidesToShow && (e.slideOffset = 0, f = 0), e.options.centerMode === !0 && e.options.infinite === !0 ? e.slideOffset += e.slideWidth * Math.floor(e.options.slidesToShow / 2) - e.slideWidth : e.options.centerMode === !0 && (e.slideOffset = 0, e.slideOffset += e.slideWidth * Math.floor(e.options.slidesToShow / 2)), b = e.options.vertical === !1 ? -1 * a * e.slideWidth + e.slideOffset : -1 * a * c + f, e.options.variableWidth === !0 && (d = e.$slideTrack.children(".slick-slide").eq(e.slideCount <= e.options.slidesToShow || e.options.infinite === !1 ? a : a + e.options.slidesToShow), b = d[0] ? -1 * d[0].offsetLeft : 0, e.options.centerMode === !0 && (d = e.$slideTrack.children(".slick-slide").eq(e.options.infinite === !1 ? a : a + e.options.slidesToShow + 1), b = d[0] ? -1 * d[0].offsetLeft : 0, b += (e.$list.width() - d.outerWidth()) / 2)), b
    }, b.prototype.getNavigableIndexes = function () {
        for (var a = this, b = 0, c = 0, d = []; b < a.slideCount;)d.push(b), b = c + a.options.slidesToScroll, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow;
        return d
    }, b.prototype.getSlideCount = function () {
        var b, c = this;
        if (c.options.swipeToSlide === !0) {
            var d = null;
            return c.$slideTrack.find(".slick-slide").each(function (b, e) {
                return e.offsetLeft + a(e).outerWidth() / 2 > -1 * c.swipeLeft ? (d = e, !1) : void 0
            }), b = Math.abs(a(d).attr("index") - c.currentSlide)
        }
        return c.options.slidesToScroll
    }, b.prototype.init = function () {
        var b = this;
        a(b.$slider).hasClass("slick-initialized") || (a(b.$slider).addClass("slick-initialized"), b.buildOut(), b.setProps(), b.startLoad(), b.loadSlider(), b.initializeEvents(), b.updateArrows(), b.updateDots()), null !== b.options.onInit && b.options.onInit.call(this, b)
    }, b.prototype.initArrowEvents = function () {
        var a = this;
        a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && (a.$prevArrow.on("click.slick", {message: "previous"}, a.changeSlide), a.$nextArrow.on("click.slick", {message: "next"}, a.changeSlide))
    }, b.prototype.initDotEvents = function () {
        var b = this;
        b.options.dots === !0 && b.slideCount > b.options.slidesToShow && a("li", b.$dots).on("click.slick", {message: "index"}, b.changeSlide), b.options.dots === !0 && b.options.pauseOnDotsHover === !0 && b.options.autoplay === !0 && a("li", b.$dots).on("mouseenter.slick", function () {
            b.paused = !0, b.autoPlayClear()
        }).on("mouseleave.slick", function () {
            b.paused = !1, b.autoPlay()
        })
    }, b.prototype.initializeEvents = function () {
        var b = this;
        b.initArrowEvents(), b.initDotEvents(), b.$list.on("touchstart.slick mousedown.slick", {action: "start"}, b.swipeHandler), b.$list.on("touchmove.slick mousemove.slick", {action: "move"}, b.swipeHandler), b.$list.on("touchend.slick mouseup.slick", {action: "end"}, b.swipeHandler), b.$list.on("touchcancel.slick mouseleave.slick", {action: "end"}, b.swipeHandler), b.$list.on("click.slick", b.clickHandler), b.options.pauseOnHover === !0 && b.options.autoplay === !0 && (b.$list.on("mouseenter.slick", function () {
            b.paused = !0, b.autoPlayClear()
        }), b.$list.on("mouseleave.slick", function () {
            b.paused = !1, b.autoPlay()
        })), b.options.accessibility === !0 && b.$list.on("keydown.slick", b.keyHandler), b.options.focusOnSelect === !0 && a(b.options.slide, b.$slideTrack).on("click.slick", b.selectHandler), a(window).on("orientationchange.slick.slick-" + b.instanceUid, function () {
            b.checkResponsive(), b.setPosition()
        }), a(window).on("resize.slick.slick-" + b.instanceUid, function () {
            a(window).width() !== b.windowWidth && (clearTimeout(b.windowDelay), b.windowDelay = window.setTimeout(function () {
                b.windowWidth = a(window).width(), b.checkResponsive(), b.setPosition()
            }, 50))
        }), a("*[draggable!=true]", b.$slideTrack).on("dragstart", function (a) {
            a.preventDefault()
        }), a(window).on("load.slick.slick-" + b.instanceUid, b.setPosition), a(document).on("ready.slick.slick-" + b.instanceUid, b.setPosition)
    }, b.prototype.initUI = function () {
        var a = this;
        a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && (a.$prevArrow.show(), a.$nextArrow.show()), a.options.dots === !0 && a.slideCount > a.options.slidesToShow && a.$dots.show(), a.options.autoplay === !0 && a.autoPlay()
    }, b.prototype.keyHandler = function (a) {
        var b = this;
        37 === a.keyCode && b.options.accessibility === !0 ? b.changeSlide({data: {message: "previous"}}) : 39 === a.keyCode && b.options.accessibility === !0 && b.changeSlide({data: {message: "next"}})
    }, b.prototype.lazyLoad = function () {
        function b(b) {
            a("img[data-lazy]", b).each(function () {
                var b = a(this), c = a(this).attr("data-lazy");
                b.load(function () {
                    b.animate({opacity: 1}, 200)
                }).css({opacity: 0}).attr("src", c).removeAttr("data-lazy").removeClass("slick-loading")
            })
        }

        var c, d, e, f, g = this;
        g.options.centerMode === !0 ? g.options.infinite === !0 ? (e = g.currentSlide + (g.options.slidesToShow / 2 + 1), f = e + g.options.slidesToShow + 2) : (e = Math.max(0, g.currentSlide - (g.options.slidesToShow / 2 + 1)), f = 2 + (g.options.slidesToShow / 2 + 1) + g.currentSlide) : (e = g.options.infinite ? g.options.slidesToShow + g.currentSlide : g.currentSlide, f = e + g.options.slidesToShow, g.options.fade === !0 && (e > 0 && e--, f <= g.slideCount && f++)), c = g.$slider.find(".slick-slide").slice(e, f), b(c), g.slideCount <= g.options.slidesToShow ? (d = g.$slider.find(".slick-slide"), b(d)) : g.currentSlide >= g.slideCount - g.options.slidesToShow ? (d = g.$slider.find(".slick-cloned").slice(0, g.options.slidesToShow), b(d)) : 0 === g.currentSlide && (d = g.$slider.find(".slick-cloned").slice(-1 * g.options.slidesToShow), b(d))
    }, b.prototype.loadSlider = function () {
        var a = this;
        a.setPosition(), a.$slideTrack.css({opacity: 1}), a.$slider.removeClass("slick-loading"), a.initUI(), "progressive" === a.options.lazyLoad && a.progressiveLazyLoad()
    }, b.prototype.postSlide = function (a) {
        var b = this;
        null !== b.options.onAfterChange && b.options.onAfterChange.call(this, b, a), b.animating = !1, b.setPosition(), b.swipeLeft = null, b.options.autoplay === !0 && b.paused === !1 && b.autoPlay()
    }, b.prototype.progressiveLazyLoad = function () {
        var b, c, d = this;
        b = a("img[data-lazy]", d.$slider).length, b > 0 && (c = a("img[data-lazy]", d.$slider).first(), c.attr("src", c.attr("data-lazy")).removeClass("slick-loading").load(function () {
            c.removeAttr("data-lazy"), d.progressiveLazyLoad()
        }).error(function () {
            c.removeAttr("data-lazy"), d.progressiveLazyLoad()
        }))
    }, b.prototype.refresh = function () {
        var b = this, c = b.currentSlide;
        b.destroy(), a.extend(b, b.initials), b.init(), b.changeSlide({data: {message: "index", index: c}}, !0)
    }, b.prototype.reinit = function () {
        var b = this;
        b.$slides = b.$slideTrack.children(b.options.slide).addClass("slick-slide"), b.slideCount = b.$slides.length, b.currentSlide >= b.slideCount && 0 !== b.currentSlide && (b.currentSlide = b.currentSlide - b.options.slidesToScroll), b.slideCount <= b.options.slidesToShow && (b.currentSlide = 0), b.setProps(), b.setupInfinite(), b.buildArrows(), b.updateArrows(), b.initArrowEvents(), b.buildDots(), b.updateDots(), b.initDotEvents(), b.options.focusOnSelect === !0 && a(b.options.slide, b.$slideTrack).on("click.slick", b.selectHandler), b.setSlideClasses(0), b.setPosition(), null !== b.options.onReInit && b.options.onReInit.call(this, b)
    }, b.prototype.removeSlide = function (a, b, c) {
        var d = this;
        return "boolean" == typeof a ? (b = a, a = b === !0 ? 0 : d.slideCount - 1) : a = b === !0 ? --a : a, d.slideCount < 1 || 0 > a || a > d.slideCount - 1 ? !1 : (d.unload(), c === !0 ? d.$slideTrack.children().remove() : d.$slideTrack.children(this.options.slide).eq(a).remove(), d.$slides = d.$slideTrack.children(this.options.slide), d.$slideTrack.children(this.options.slide).detach(), d.$slideTrack.append(d.$slides), d.$slidesCache = d.$slides, void d.reinit())
    }, b.prototype.setCSS = function (a) {
        var b, c, d = this, e = {};
        d.options.rtl === !0 && (a = -a), b = "left" == d.positionProp ? a + "px" : "0px", c = "top" == d.positionProp ? a + "px" : "0px", e[d.positionProp] = a, d.transformsEnabled === !1 ? d.$slideTrack.css(e) : (e = {}, d.cssTransitions === !1 ? (e[d.animType] = "translate(" + b + ", " + c + ")", d.$slideTrack.css(e)) : (e[d.animType] = "translate3d(" + b + ", " + c + ", 0px)", d.$slideTrack.css(e)))
    }, b.prototype.setDimensions = function () {
        var b = this;
        if (b.options.vertical === !1 ? b.options.centerMode === !0 && b.$list.css({padding: "0px " + b.options.centerPadding}) : (b.$list.height(b.$slides.first().outerHeight(!0) * b.options.slidesToShow), b.options.centerMode === !0 && b.$list.css({padding: b.options.centerPadding + " 0px"})), b.listWidth = b.$list.width(), b.listHeight = b.$list.height(), b.options.vertical === !1 && b.options.variableWidth === !1)b.slideWidth = Math.ceil(b.listWidth / b.options.slidesToShow), b.$slideTrack.width(Math.ceil(b.slideWidth * b.$slideTrack.children(".slick-slide").length)); else if (b.options.variableWidth === !0) {
            var c = 0;
            b.slideWidth = Math.ceil(b.listWidth / b.options.slidesToShow), b.$slideTrack.children(".slick-slide").each(function () {
                c += Math.ceil(a(this).outerWidth(!0))
            }), b.$slideTrack.width(Math.ceil(c) + 1)
        } else b.slideWidth = Math.ceil(b.listWidth), b.$slideTrack.height(Math.ceil(b.$slides.first().outerHeight(!0) * b.$slideTrack.children(".slick-slide").length));
        var d = b.$slides.first().outerWidth(!0) - b.$slides.first().width();
        b.options.variableWidth === !1 && b.$slideTrack.children(".slick-slide").width(b.slideWidth - d)
    }, b.prototype.setFade = function () {
        var b, c = this;
        c.$slides.each(function (d, e) {
            b = -1 * c.slideWidth * d, a(e).css(c.options.rtl === !0 ? {
                position: "relative",
                right: b,
                top: 0,
                zIndex: 800,
                opacity: 0
            } : {position: "relative", left: b, top: 0, zIndex: 800, opacity: 0})
        }), c.$slides.eq(c.currentSlide).css({zIndex: 900, opacity: 1})
    }, b.prototype.setHeight = function () {
        var a = this;
        if (1 === a.options.slidesToShow && a.options.adaptiveHeight === !0 && a.options.vertical === !1) {
            var b = a.$slides.eq(a.currentSlide).outerHeight(!0);
            a.$list.css("height", b)
        }
    }, b.prototype.setPosition = function () {
        var a = this;
        a.setDimensions(), a.setHeight(), a.options.fade === !1 ? a.setCSS(a.getLeft(a.currentSlide)) : a.setFade(), null !== a.options.onSetPosition && a.options.onSetPosition.call(this, a)
    }, b.prototype.setProps = function () {
        var a = this, b = document.body.style;
        a.positionProp = a.options.vertical === !0 ? "top" : "left", "top" === a.positionProp ? a.$slider.addClass("slick-vertical") : a.$slider.removeClass("slick-vertical"), (void 0 !== b.WebkitTransition || void 0 !== b.MozTransition || void 0 !== b.msTransition) && a.options.useCSS === !0 && (a.cssTransitions = !0), void 0 !== b.OTransform && (a.animType = "OTransform", a.transformType = "-o-transform", a.transitionType = "OTransition", void 0 === b.perspectiveProperty && void 0 === b.webkitPerspective && (a.animType = !1)), void 0 !== b.MozTransform && (a.animType = "MozTransform", a.transformType = "-moz-transform", a.transitionType = "MozTransition", void 0 === b.perspectiveProperty && void 0 === b.MozPerspective && (a.animType = !1)), void 0 !== b.webkitTransform && (a.animType = "webkitTransform", a.transformType = "-webkit-transform", a.transitionType = "webkitTransition", void 0 === b.perspectiveProperty && void 0 === b.webkitPerspective && (a.animType = !1)), void 0 !== b.msTransform && (a.animType = "msTransform", a.transformType = "-ms-transform", a.transitionType = "msTransition", void 0 === b.msTransform && (a.animType = !1)), void 0 !== b.transform && a.animType !== !1 && (a.animType = "transform", a.transformType = "transform", a.transitionType = "transition"), a.transformsEnabled = null !== a.animType && a.animType !== !1
    }, b.prototype.setSlideClasses = function (a) {
        var b, c, d, e, f = this;
        f.$slider.find(".slick-slide").removeClass("slick-active").removeClass("slick-center"), c = f.$slider.find(".slick-slide"), f.options.centerMode === !0 ? (b = Math.floor(f.options.slidesToShow / 2), f.options.infinite === !0 && (a >= b && a <= f.slideCount - 1 - b ? f.$slides.slice(a - b, a + b + 1).addClass("slick-active") : (d = f.options.slidesToShow + a, c.slice(d - b + 1, d + b + 2).addClass("slick-active")), 0 === a ? c.eq(c.length - 1 - f.options.slidesToShow).addClass("slick-center") : a === f.slideCount - 1 && c.eq(f.options.slidesToShow).addClass("slick-center")), f.$slides.eq(a).addClass("slick-center")) : a >= 0 && a <= f.slideCount - f.options.slidesToShow ? f.$slides.slice(a, a + f.options.slidesToShow).addClass("slick-active") : c.length <= f.options.slidesToShow ? c.addClass("slick-active") : (e = f.slideCount % f.options.slidesToShow, d = f.options.infinite === !0 ? f.options.slidesToShow + a : a, f.options.slidesToShow == f.options.slidesToScroll && f.slideCount - a < f.options.slidesToShow ? c.slice(d - (f.options.slidesToShow - e), d + e).addClass("slick-active") : c.slice(d, d + f.options.slidesToShow).addClass("slick-active")), "ondemand" === f.options.lazyLoad && f.lazyLoad()
    }, b.prototype.setupInfinite = function () {
        var b, c, d, e = this;
        if (e.options.fade === !0 && (e.options.centerMode = !1), e.options.infinite === !0 && e.options.fade === !1 && (c = null, e.slideCount > e.options.slidesToShow)) {
            for (d = e.options.centerMode === !0 ? e.options.slidesToShow + 1 : e.options.slidesToShow, b = e.slideCount; b > e.slideCount - d; b -= 1)c = b - 1, a(e.$slides[c]).clone(!0).attr("id", "").attr("index", c - e.slideCount).prependTo(e.$slideTrack).addClass("slick-cloned");
            for (b = 0; d > b; b += 1)c = b, a(e.$slides[c]).clone(!0).attr("id", "").attr("index", c + e.slideCount).appendTo(e.$slideTrack).addClass("slick-cloned");
            e.$slideTrack.find(".slick-cloned").find("[id]").each(function () {
                a(this).attr("id", "")
            })
        }
    }, b.prototype.selectHandler = function (b) {
        var c = this, d = parseInt(a(b.target).parents(".slick-slide").attr("index"));
        return d || (d = 0), c.slideCount <= c.options.slidesToShow ? (c.$slider.find(".slick-slide").removeClass("slick-active"), c.$slides.eq(d).addClass("slick-active"), c.options.centerMode === !0 && (c.$slider.find(".slick-slide").removeClass("slick-center"), c.$slides.eq(d).addClass("slick-center")), void c.asNavFor(d)) : void c.slideHandler(d)
    }, b.prototype.slideHandler = function (a, b, c) {
        var d, e, f, g, h = null, i = this;
        return b = b || !1, i.animating === !0 && i.options.waitForAnimate === !0 || i.options.fade === !0 && i.currentSlide === a || i.slideCount <= i.options.slidesToShow ? void 0 : (b === !1 && i.asNavFor(a), d = a, h = i.getLeft(d), g = i.getLeft(i.currentSlide), i.currentLeft = null === i.swipeLeft ? g : i.swipeLeft, i.options.infinite === !1 && i.options.centerMode === !1 && (0 > a || a > i.getDotCount() * i.options.slidesToScroll) ? void(i.options.fade === !1 && (d = i.currentSlide, c !== !0 ? i.animateSlide(g, function () {
            i.postSlide(d)
        }) : i.postSlide(d))) : i.options.infinite === !1 && i.options.centerMode === !0 && (0 > a || a > i.slideCount - i.options.slidesToScroll) ? void(i.options.fade === !1 && (d = i.currentSlide, c !== !0 ? i.animateSlide(g, function () {
            i.postSlide(d)
        }) : i.postSlide(d))) : (i.options.autoplay === !0 && clearInterval(i.autoPlayTimer), e = 0 > d ? 0 !== i.slideCount % i.options.slidesToScroll ? i.slideCount - i.slideCount % i.options.slidesToScroll : i.slideCount + d : d >= i.slideCount ? 0 !== i.slideCount % i.options.slidesToScroll ? 0 : d - i.slideCount : d, i.animating = !0, null !== i.options.onBeforeChange && a !== i.currentSlide && i.options.onBeforeChange.call(this, i, i.currentSlide, e), f = i.currentSlide, i.currentSlide = e, i.setSlideClasses(i.currentSlide), i.updateDots(), i.updateArrows(), i.options.fade === !0 ? void(c !== !0 ? i.fadeSlide(f, e, function () {
            i.postSlide(e)
        }) : i.postSlide(e)) : void(c !== !0 ? i.animateSlide(h, function () {
            i.postSlide(e)
        }) : i.postSlide(e))))
    }, b.prototype.startLoad = function () {
        var a = this;
        a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && (a.$prevArrow.hide(), a.$nextArrow.hide()), a.options.dots === !0 && a.slideCount > a.options.slidesToShow && a.$dots.hide(), a.$slider.addClass("slick-loading")
    }, b.prototype.swipeDirection = function () {
        var a, b, c, d, e = this;
        return a = e.touchObject.startX - e.touchObject.curX, b = e.touchObject.startY - e.touchObject.curY, c = Math.atan2(b, a), d = Math.round(180 * c / Math.PI), 0 > d && (d = 360 - Math.abs(d)), 45 >= d && d >= 0 ? e.options.rtl === !1 ? "left" : "right" : 360 >= d && d >= 315 ? e.options.rtl === !1 ? "left" : "right" : d >= 135 && 225 >= d ? e.options.rtl === !1 ? "right" : "left" : "vertical"
    }, b.prototype.swipeEnd = function () {
        var a = this;
        if (a.dragging = !1, a.shouldClick = a.touchObject.swipeLength > 10 ? !1 : !0, void 0 === a.touchObject.curX)return !1;
        if (a.touchObject.swipeLength >= a.touchObject.minSwipe)switch (a.swipeDirection()) {
            case"left":
                a.slideHandler(a.currentSlide + a.getSlideCount()), a.currentDirection = 0, a.touchObject = {};
                break;
            case"right":
                a.slideHandler(a.currentSlide - a.getSlideCount()), a.currentDirection = 1, a.touchObject = {}
        } else a.touchObject.startX !== a.touchObject.curX && (a.slideHandler(a.currentSlide), a.touchObject = {})
    }, b.prototype.swipeHandler = function (a) {
        var b = this;
        if (!(b.options.swipe === !1 || "ontouchend"in document && b.options.swipe === !1 || b.options.draggable === !1 && -1 !== a.type.indexOf("mouse")))switch (b.touchObject.fingerCount = a.originalEvent && void 0 !== a.originalEvent.touches ? a.originalEvent.touches.length : 1, b.touchObject.minSwipe = b.listWidth / b.options.touchThreshold, a.data.action) {
            case"start":
                b.swipeStart(a);
                break;
            case"move":
                b.swipeMove(a);
                break;
            case"end":
                b.swipeEnd(a)
        }
    }, b.prototype.swipeMove = function (a) {
        var b, c, d, e, f = this;
        return e = void 0 !== a.originalEvent ? a.originalEvent.touches : null, !f.dragging || e && 1 !== e.length ? !1 : (b = f.getLeft(f.currentSlide), f.touchObject.curX = void 0 !== e ? e[0].pageX : a.clientX, f.touchObject.curY = void 0 !== e ? e[0].pageY : a.clientY, f.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(f.touchObject.curX - f.touchObject.startX, 2))), c = f.swipeDirection(), "vertical" !== c ? (void 0 !== a.originalEvent && f.touchObject.swipeLength > 4 && a.preventDefault(), d = (f.options.rtl === !1 ? 1 : -1) * (f.touchObject.curX > f.touchObject.startX ? 1 : -1), f.swipeLeft = f.options.vertical === !1 ? b + f.touchObject.swipeLength * d : b + f.touchObject.swipeLength * (f.$list.height() / f.listWidth) * d, f.options.fade === !0 || f.options.touchMove === !1 ? !1 : f.animating === !0 ? (f.swipeLeft = null, !1) : void f.setCSS(f.swipeLeft)) : void 0)
    }, b.prototype.swipeStart = function (a) {
        var b, c = this;
        return 1 !== c.touchObject.fingerCount || c.slideCount <= c.options.slidesToShow ? (c.touchObject = {}, !1) : (void 0 !== a.originalEvent && void 0 !== a.originalEvent.touches && (b = a.originalEvent.touches[0]), c.touchObject.startX = c.touchObject.curX = void 0 !== b ? b.pageX : a.clientX, c.touchObject.startY = c.touchObject.curY = void 0 !== b ? b.pageY : a.clientY, void(c.dragging = !0))
    }, b.prototype.unfilterSlides = function () {
        var a = this;
        null !== a.$slidesCache && (a.unload(), a.$slideTrack.children(this.options.slide).detach(), a.$slidesCache.appendTo(a.$slideTrack), a.reinit())
    }, b.prototype.unload = function () {
        var b = this;
        a(".slick-cloned", b.$slider).remove(), b.$dots && b.$dots.remove(), b.$prevArrow && "object" != typeof b.options.prevArrow && b.$prevArrow.remove(), b.$nextArrow && "object" != typeof b.options.nextArrow && b.$nextArrow.remove(), b.$slides.removeClass("slick-slide slick-active slick-visible").css("width", "")
    }, b.prototype.updateArrows = function () {
        var a, b = this;
        a = Math.floor(b.options.slidesToShow / 2), b.options.arrows === !0 && b.options.infinite !== !0 && b.slideCount > b.options.slidesToShow && (b.$prevArrow.removeClass("slick-disabled"), b.$nextArrow.removeClass("slick-disabled"), 0 === b.currentSlide ? (b.$prevArrow.addClass("slick-disabled"), b.$nextArrow.removeClass("slick-disabled")) : b.currentSlide >= b.slideCount - b.options.slidesToShow && b.options.centerMode === !1 ? (b.$nextArrow.addClass("slick-disabled"), b.$prevArrow.removeClass("slick-disabled")) : b.currentSlide > b.slideCount - b.options.slidesToShow + a && b.options.centerMode === !0 && (b.$nextArrow.addClass("slick-disabled"), b.$prevArrow.removeClass("slick-disabled")))
    }, b.prototype.updateDots = function () {
        var a = this;
        null !== a.$dots && (a.$dots.find("li").removeClass("slick-active"), a.$dots.find("li").eq(Math.floor(a.currentSlide / a.options.slidesToScroll)).addClass("slick-active"))
    }, a.fn.slick = function (a) {
        var c = this;
        return c.each(function (c, d) {
            d.slick = new b(d, a)
        })
    }, a.fn.slickAdd = function (a, b, c) {
        var d = this;
        return d.each(function (d, e) {
            e.slick.addSlide(a, b, c)
        })
    }, a.fn.slickCurrentSlide = function () {
        var a = this;
        return a.get(0).slick.getCurrent()
    }, a.fn.slickFilter = function (a) {
        var b = this;
        return b.each(function (b, c) {
            c.slick.filterSlides(a)
        })
    }, a.fn.slickGoTo = function (a, b) {
        var c = this;
        return c.each(function (c, d) {
            d.slick.changeSlide({data: {message: "index", index: parseInt(a)}}, b)
        })
    }, a.fn.slickNext = function () {
        var a = this;
        return a.each(function (a, b) {
            b.slick.changeSlide({data: {message: "next"}})
        })
    }, a.fn.slickPause = function () {
        var a = this;
        return a.each(function (a, b) {
            b.slick.autoPlayClear(), b.slick.paused = !0
        })
    }, a.fn.slickPlay = function () {
        var a = this;
        return a.each(function (a, b) {
            b.slick.paused = !1, b.slick.autoPlay()
        })
    }, a.fn.slickPrev = function () {
        var a = this;
        return a.each(function (a, b) {
            b.slick.changeSlide({data: {message: "previous"}})
        })
    }, a.fn.slickRemove = function (a, b) {
        var c = this;
        return c.each(function (c, d) {
            d.slick.removeSlide(a, b)
        })
    }, a.fn.slickRemoveAll = function () {
        var a = this;
        return a.each(function (a, b) {
            b.slick.removeSlide(null, null, !0)
        })
    }, a.fn.slickGetOption = function (a) {
        var b = this;
        return b.get(0).slick.options[a]
    }, a.fn.slickSetOption = function (a, b, c) {
        var d = this;
        return d.each(function (d, e) {
            e.slick.options[a] = b, c === !0 && (e.slick.unload(), e.slick.reinit())
        })
    }, a.fn.slickUnfilter = function () {
        var a = this;
        return a.each(function (a, b) {
            b.slick.unfilterSlides()
        })
    }, a.fn.unslick = function () {
        var a = this;
        return a.each(function (a, b) {
            b.slick && b.slick.destroy()
        })
    }, a.fn.getSlick = function () {
        var a = null, b = this;
        return b.each(function (b, c) {
            a = c.slick
        }), a
    }
}), function (a) {
    function b() {
        f = !1;
        for (var b = 0; b < d.length; b++) {
            var e = a(d[b]).filter(function () {
                return a(this).is(":appeared")
            });
            if (e.trigger("appear", [e]), c) {
                var g = c.not(e);
                g.trigger("disappear", [g])
            }
            c = e
        }
    }

    var c, d = [], e = !1, f = !1, g = {interval: 250, force_process: !1}, h = a(window);
    a.expr[":"].appeared = function (b) {
        var c = a(b);
        if (!c.is(":visible"))return !1;
        var d = h.scrollLeft(), e = h.scrollTop(), f = c.offset(), g = f.left, i = f.top;
        return i + c.height() >= e && i - (c.data("appear-top-offset") || 0) <= e + h.height() && g + c.width() >= d && g - (c.data("appear-left-offset") || 0) <= d + h.width() ? !0 : !1
    }, a.fn.extend({
        appear: function (c) {
            var h = a.extend({}, g, c || {}), i = this.selector || this;
            if (!e) {
                var j = function () {
                    f || (f = !0, setTimeout(b, h.interval))
                };
                a(window).scroll(j).resize(j), e = !0
            }
            return h.force_process && setTimeout(b, h.interval), d.push(i), a(i)
        }
    }), a.extend({
        force_appear: function () {
            return e ? (b(), !0) : !1
        }
    })
}(jQuery), function (a, b, c) {
    !function (b) {
        var d = "function" == typeof define && define.amd, e = "https:" == c.location.protocol ? "https:" : "http:", f = "cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.12/jquery.mousewheel.min.js";
        d || a.event.special.mousewheel || a("head").append(decodeURI("%3Cscript src=" + e + "//" + f + "%3E%3C/script%3E")), b()
    }(function () {
        var d, e = "mCustomScrollbar", f = "mCS", g = ".mCustomScrollbar", h = {
            setTop: 0,
            setLeft: 0,
            axis: "y",
            scrollbarPosition: "inside",
            scrollInertia: 950,
            autoDraggerLength: !0,
            alwaysShowScrollbar: 0,
            snapOffset: 0,
            mouseWheel: {
                enable: !0,
                scrollAmount: "auto",
                axis: "y",
                deltaFactor: "auto",
                disableOver: ["select", "option", "keygen", "datalist", "textarea"]
            },
            scrollButtons: {scrollType: "stepless", scrollAmount: "auto"},
            keyboard: {enable: !0, scrollType: "stepless", scrollAmount: "auto"},
            contentTouchScroll: 25,
            advanced: {
                autoScrollOnFocus: "input,textarea,select,button,datalist,keygen,a[tabindex],area,object,[contenteditable='true']",
                updateOnContentResize: !0,
                updateOnImageLoad: !0
            },
            theme: "light",
            callbacks: {onTotalScrollOffset: 0, onTotalScrollBackOffset: 0, alwaysTriggerOffsets: !0}
        }, i = 0, j = {}, k = b.attachEvent && !b.addEventListener ? 1 : 0, l = !1, m = ["mCSB_dragger_onDrag", "mCSB_scrollTools_onDrag", "mCS_img_loaded", "mCS_disabled", "mCS_destroyed", "mCS_no_scrollbar", "mCS-autoHide", "mCS-dir-rtl", "mCS_no_scrollbar_y", "mCS_no_scrollbar_x", "mCS_y_hidden", "mCS_x_hidden", "mCSB_draggerContainer", "mCSB_buttonUp", "mCSB_buttonDown", "mCSB_buttonLeft", "mCSB_buttonRight"], n = {
            init: function (b) {
                var b = a.extend(!0, {}, h, b), c = o.call(this);
                if (b.live) {
                    var d = b.liveSelector || this.selector || g, e = a(d);
                    if ("off" === b.live)return void q(d);
                    j[d] = setTimeout(function () {
                        e.mCustomScrollbar(b), "once" === b.live && e.length && q(d)
                    }, 500)
                } else q(d);
                return b.setWidth = b.set_width ? b.set_width : b.setWidth, b.setHeight = b.set_height ? b.set_height : b.setHeight, b.axis = b.horizontalScroll ? "x" : r(b.axis), b.scrollInertia = b.scrollInertia > 0 && b.scrollInertia < 17 ? 17 : b.scrollInertia, "object" != typeof b.mouseWheel && 1 == b.mouseWheel && (b.mouseWheel = {
                    enable: !0,
                    scrollAmount: "auto",
                    axis: "y",
                    preventDefault: !1,
                    deltaFactor: "auto",
                    normalizeDelta: !1,
                    invert: !1
                }), b.mouseWheel.scrollAmount = b.mouseWheelPixels ? b.mouseWheelPixels : b.mouseWheel.scrollAmount, b.mouseWheel.normalizeDelta = b.advanced.normalizeMouseWheelDelta ? b.advanced.normalizeMouseWheelDelta : b.mouseWheel.normalizeDelta, b.scrollButtons.scrollType = s(b.scrollButtons.scrollType), p(b), a(c).each(function () {
                    var c = a(this);
                    if (!c.data(f)) {
                        c.data(f, {
                            idx: ++i,
                            opt: b,
                            scrollRatio: {y: null, x: null},
                            overflowed: null,
                            contentReset: {y: null, x: null},
                            bindEvents: !1,
                            tweenRunning: !1,
                            sequential: {},
                            langDir: c.css("direction"),
                            cbOffsets: null,
                            trigger: null
                        });
                        var d = c.data(f), e = d.opt, g = c.data("mcs-axis"), h = c.data("mcs-scrollbar-position"), j = c.data("mcs-theme");
                        g && (e.axis = g), h && (e.scrollbarPosition = h), j && (e.theme = j, p(e)), t.call(this), a("#mCSB_" + d.idx + "_container img:not(." + m[2] + ")").addClass(m[2]), n.update.call(null, c)
                    }
                })
            }, update: function (b, c) {
                var d = b || o.call(this);
                return a(d).each(function () {
                    var b = a(this);
                    if (b.data(f)) {
                        var d = b.data(f), e = d.opt, g = a("#mCSB_" + d.idx + "_container"), h = [a("#mCSB_" + d.idx + "_dragger_vertical"), a("#mCSB_" + d.idx + "_dragger_horizontal")];
                        if (!g.length)return;
                        d.tweenRunning && W(b), b.hasClass(m[3]) && b.removeClass(m[3]), b.hasClass(m[4]) && b.removeClass(m[4]), x.call(this), v.call(this), "y" === e.axis || e.advanced.autoExpandHorizontalScroll || g.css("width", u(g.children())), d.overflowed = B.call(this), F.call(this), e.autoDraggerLength && y.call(this), z.call(this), D.call(this);
                        var i = [Math.abs(g[0].offsetTop), Math.abs(g[0].offsetLeft)];
                        "x" !== e.axis && (d.overflowed[0] ? h[0].height() > h[0].parent().height() ? C.call(this) : (X(b, i[0].toString(), {
                            dir: "y",
                            dur: 0,
                            overwrite: "none"
                        }), d.contentReset.y = null) : (C.call(this), "y" === e.axis ? E.call(this) : "yx" === e.axis && d.overflowed[1] && X(b, i[1].toString(), {
                            dir: "x",
                            dur: 0,
                            overwrite: "none"
                        }))), "y" !== e.axis && (d.overflowed[1] ? h[1].width() > h[1].parent().width() ? C.call(this) : (X(b, i[1].toString(), {
                            dir: "x",
                            dur: 0,
                            overwrite: "none"
                        }), d.contentReset.x = null) : (C.call(this), "x" === e.axis ? E.call(this) : "yx" === e.axis && d.overflowed[0] && X(b, i[0].toString(), {
                            dir: "y",
                            dur: 0,
                            overwrite: "none"
                        }))), c && d && (2 === c && e.callbacks.onImageLoad && "function" == typeof e.callbacks.onImageLoad ? e.callbacks.onImageLoad.call(this) : 3 === c && e.callbacks.onSelectorChange && "function" == typeof e.callbacks.onSelectorChange ? e.callbacks.onSelectorChange.call(this) : e.callbacks.onUpdate && "function" == typeof e.callbacks.onUpdate && e.callbacks.onUpdate.call(this)), U.call(this)
                    }
                })
            }, scrollTo: function (b, c) {
                if ("undefined" != typeof b && null != b) {
                    var d = o.call(this);
                    return a(d).each(function () {
                        var d = a(this);
                        if (d.data(f)) {
                            var e = d.data(f), g = e.opt, h = {
                                trigger: "external",
                                scrollInertia: g.scrollInertia,
                                scrollEasing: "mcsEaseInOut",
                                moveDragger: !1,
                                timeout: 60,
                                callbacks: !0,
                                onStart: !0,
                                onUpdate: !0,
                                onComplete: !0
                            }, i = a.extend(!0, {}, h, c), j = S.call(this, b), k = i.scrollInertia > 0 && i.scrollInertia < 17 ? 17 : i.scrollInertia;
                            j[0] = T.call(this, j[0], "y"), j[1] = T.call(this, j[1], "x"), i.moveDragger && (j[0] *= e.scrollRatio.y, j[1] *= e.scrollRatio.x), i.dur = k, setTimeout(function () {
                                null !== j[0] && "undefined" != typeof j[0] && "x" !== g.axis && e.overflowed[0] && (i.dir = "y", i.overwrite = "all", X(d, j[0].toString(), i)), null !== j[1] && "undefined" != typeof j[1] && "y" !== g.axis && e.overflowed[1] && (i.dir = "x", i.overwrite = "none", X(d, j[1].toString(), i))
                            }, i.timeout)
                        }
                    })
                }
            }, stop: function () {
                var b = o.call(this);
                return a(b).each(function () {
                    var b = a(this);
                    b.data(f) && W(b)
                })
            }, disable: function (b) {
                var c = o.call(this);
                return a(c).each(function () {
                    var c = a(this);
                    if (c.data(f)) {
                        {
                            c.data(f)
                        }
                        U.call(this, "remove"), E.call(this), b && C.call(this), F.call(this, !0), c.addClass(m[3])
                    }
                })
            }, destroy: function () {
                var b = o.call(this);
                return a(b).each(function () {
                    var c = a(this);
                    if (c.data(f)) {
                        var d = c.data(f), g = d.opt, h = a("#mCSB_" + d.idx), i = a("#mCSB_" + d.idx + "_container"), j = a(".mCSB_" + d.idx + "_scrollbar");
                        g.live && q(g.liveSelector || a(b).selector), U.call(this, "remove"), E.call(this), C.call(this), c.removeData(f), _(this, "mcs"), j.remove(), i.find("img." + m[2]).removeClass(m[2]), h.replaceWith(i.contents()), c.removeClass(e + " _" + f + "_" + d.idx + " " + m[6] + " " + m[7] + " " + m[5] + " " + m[3]).addClass(m[4])
                    }
                })
            }
        }, o = function () {
            return "object" != typeof a(this) || a(this).length < 1 ? g : this
        }, p = function (b) {
            var c = ["rounded", "rounded-dark", "rounded-dots", "rounded-dots-dark"], d = ["rounded-dots", "rounded-dots-dark", "3d", "3d-dark", "3d-thick", "3d-thick-dark", "inset", "inset-dark", "inset-2", "inset-2-dark", "inset-3", "inset-3-dark"], e = ["minimal", "minimal-dark"], f = ["minimal", "minimal-dark"], g = ["minimal", "minimal-dark"];
            b.autoDraggerLength = a.inArray(b.theme, c) > -1 ? !1 : b.autoDraggerLength, b.autoExpandScrollbar = a.inArray(b.theme, d) > -1 ? !1 : b.autoExpandScrollbar, b.scrollButtons.enable = a.inArray(b.theme, e) > -1 ? !1 : b.scrollButtons.enable, b.autoHideScrollbar = a.inArray(b.theme, f) > -1 ? !0 : b.autoHideScrollbar, b.scrollbarPosition = a.inArray(b.theme, g) > -1 ? "outside" : b.scrollbarPosition
        }, q = function (a) {
            j[a] && (clearTimeout(j[a]), _(j, a))
        }, r = function (a) {
            return "yx" === a || "xy" === a || "auto" === a ? "yx" : "x" === a || "horizontal" === a ? "x" : "y"
        }, s = function (a) {
            return "stepped" === a || "pixels" === a || "step" === a || "click" === a ? "stepped" : "stepless"
        }, t = function () {
            var b = a(this), c = b.data(f), d = c.opt, g = d.autoExpandScrollbar ? " " + m[1] + "_expand" : "", h = ["<div id='mCSB_" + c.idx + "_scrollbar_vertical' class='mCSB_scrollTools mCSB_" + c.idx + "_scrollbar mCS-" + d.theme + " mCSB_scrollTools_vertical" + g + "'><div class='" + m[12] + "'><div id='mCSB_" + c.idx + "_dragger_vertical' class='mCSB_dragger' style='position:absolute;' oncontextmenu='return false;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>", "<div id='mCSB_" + c.idx + "_scrollbar_horizontal' class='mCSB_scrollTools mCSB_" + c.idx + "_scrollbar mCS-" + d.theme + " mCSB_scrollTools_horizontal" + g + "'><div class='" + m[12] + "'><div id='mCSB_" + c.idx + "_dragger_horizontal' class='mCSB_dragger' style='position:absolute;' oncontextmenu='return false;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>"], i = "yx" === d.axis ? "mCSB_vertical_horizontal" : "x" === d.axis ? "mCSB_horizontal" : "mCSB_vertical", j = "yx" === d.axis ? h[0] + h[1] : "x" === d.axis ? h[1] : h[0], k = "yx" === d.axis ? "<div id='mCSB_" + c.idx + "_container_wrapper' class='mCSB_container_wrapper' />" : "", l = d.autoHideScrollbar ? " " + m[6] : "", n = "x" !== d.axis && "rtl" === c.langDir ? " " + m[7] : "";
            d.setWidth && b.css("width", d.setWidth), d.setHeight && b.css("height", d.setHeight), d.setLeft = "y" !== d.axis && "rtl" === c.langDir ? "989999px" : d.setLeft, b.addClass(e + " _" + f + "_" + c.idx + l + n).wrapInner("<div id='mCSB_" + c.idx + "' class='mCustomScrollBox mCS-" + d.theme + " " + i + "'><div id='mCSB_" + c.idx + "_container' class='mCSB_container' style='position:relative; top:" + d.setTop + "; left:" + d.setLeft + ";' dir=" + c.langDir + " /></div>");
            var o = a("#mCSB_" + c.idx), p = a("#mCSB_" + c.idx + "_container");
            "y" === d.axis || d.advanced.autoExpandHorizontalScroll || p.css("width", u(p.children())), "outside" === d.scrollbarPosition ? ("static" === b.css("position") && b.css("position", "relative"), b.css("overflow", "visible"), o.addClass("mCSB_outside").after(j)) : (o.addClass("mCSB_inside").append(j), p.wrap(k)), w.call(this);
            var q = [a("#mCSB_" + c.idx + "_dragger_vertical"), a("#mCSB_" + c.idx + "_dragger_horizontal")];
            q[0].css("min-height", q[0].height()), q[1].css("min-width", q[1].width())
        }, u = function (b) {
            return Math.max.apply(Math, b.map(function () {
                return a(this).outerWidth(!0)
            }).get())
        }, v = function () {
            var b = a(this), c = b.data(f), d = c.opt, e = a("#mCSB_" + c.idx + "_container");
            d.advanced.autoExpandHorizontalScroll && "y" !== d.axis && e.css({
                position: "absolute",
                width: "auto"
            }).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({
                width: Math.ceil(e[0].getBoundingClientRect().right + .4) - Math.floor(e[0].getBoundingClientRect().left),
                position: "relative"
            }).unwrap()
        }, w = function () {
            var b = a(this), c = b.data(f), d = c.opt, e = a(".mCSB_" + c.idx + "_scrollbar:first"), g = cb(d.scrollButtons.tabindex) ? "tabindex='" + d.scrollButtons.tabindex + "'" : "", h = ["<a href='#' class='" + m[13] + "' oncontextmenu='return false;' " + g + " />", "<a href='#' class='" + m[14] + "' oncontextmenu='return false;' " + g + " />", "<a href='#' class='" + m[15] + "' oncontextmenu='return false;' " + g + " />", "<a href='#' class='" + m[16] + "' oncontextmenu='return false;' " + g + " />"], i = ["x" === d.axis ? h[2] : h[0], "x" === d.axis ? h[3] : h[1], h[2], h[3]];
            d.scrollButtons.enable && e.prepend(i[0]).append(i[1]).next(".mCSB_scrollTools").prepend(i[2]).append(i[3])
        }, x = function () {
            var b = a(this), c = b.data(f), d = a("#mCSB_" + c.idx), e = b.css("max-height") || "none", g = -1 !== e.indexOf("%"), h = b.css("box-sizing");
            if ("none" !== e) {
                var i = g ? b.parent().height() * parseInt(e) / 100 : parseInt(e);
                "border-box" === h && (i -= b.innerHeight() - b.height() + (b.outerHeight() - b.innerHeight())), d.css("max-height", Math.round(i))
            }
        }, y = function () {
            var b = a(this), c = b.data(f), d = a("#mCSB_" + c.idx), e = a("#mCSB_" + c.idx + "_container"), g = [a("#mCSB_" + c.idx + "_dragger_vertical"), a("#mCSB_" + c.idx + "_dragger_horizontal")], h = [d.height() / e.outerHeight(!1), d.width() / e.outerWidth(!1)], i = [parseInt(g[0].css("min-height")), Math.round(h[0] * g[0].parent().height()), parseInt(g[1].css("min-width")), Math.round(h[1] * g[1].parent().width())], j = k && i[1] < i[0] ? i[0] : i[1], l = k && i[3] < i[2] ? i[2] : i[3];
            g[0].css({
                height: j,
                "max-height": g[0].parent().height() - 10
            }).find(".mCSB_dragger_bar").css({"line-height": i[0] + "px"}), g[1].css({
                width: l,
                "max-width": g[1].parent().width() - 10
            })
        }, z = function () {
            var b = a(this), c = b.data(f), d = a("#mCSB_" + c.idx), e = a("#mCSB_" + c.idx + "_container"), g = [a("#mCSB_" + c.idx + "_dragger_vertical"), a("#mCSB_" + c.idx + "_dragger_horizontal")], h = [e.outerHeight(!1) - d.height(), e.outerWidth(!1) - d.width()], i = [h[0] / (g[0].parent().height() - g[0].height()), h[1] / (g[1].parent().width() - g[1].width())];
            c.scrollRatio = {y: i[0], x: i[1]}
        }, A = function (a, b, c) {
            var d = c ? m[0] + "_expanded" : "", e = a.closest(".mCSB_scrollTools");
            "active" === b ? (a.toggleClass(m[0] + " " + d), e.toggleClass(m[1]), a[0]._draggable = a[0]._draggable ? 0 : 1) : a[0]._draggable || ("hide" === b ? (a.removeClass(m[0]), e.removeClass(m[1])) : (a.addClass(m[0]), e.addClass(m[1])))
        }, B = function () {
            var b = a(this), c = b.data(f), d = a("#mCSB_" + c.idx), e = a("#mCSB_" + c.idx + "_container"), g = null == c.overflowed ? e.height() : e.outerHeight(!1), h = null == c.overflowed ? e.width() : e.outerWidth(!1);
            return [g > d.height(), h > d.width()]
        }, C = function () {
            var b = a(this), c = b.data(f), d = c.opt, e = a("#mCSB_" + c.idx), g = a("#mCSB_" + c.idx + "_container"), h = [a("#mCSB_" + c.idx + "_dragger_vertical"), a("#mCSB_" + c.idx + "_dragger_horizontal")];
            if (W(b), ("x" !== d.axis && !c.overflowed[0] || "y" === d.axis && c.overflowed[0]) && (h[0].add(g).css("top", 0), X(b, "_resetY")), "y" !== d.axis && !c.overflowed[1] || "x" === d.axis && c.overflowed[1]) {
                var i = dx = 0;
                "rtl" === c.langDir && (i = e.width() - g.outerWidth(!1), dx = Math.abs(i / c.scrollRatio.x)), g.css("left", i), h[1].css("left", dx), X(b, "_resetX")
            }
        }, D = function () {
            function b() {
                g = setTimeout(function () {
                    a.event.special.mousewheel ? (clearTimeout(g), K.call(c[0])) : b()
                }, 100)
            }

            var c = a(this), d = c.data(f), e = d.opt;
            if (!d.bindEvents) {
                if (H.call(this), e.contentTouchScroll && I.call(this), J.call(this), e.mouseWheel.enable) {
                    var g;
                    b()
                }
                M.call(this), O.call(this), e.advanced.autoScrollOnFocus && N.call(this), e.scrollButtons.enable && P.call(this), e.keyboard.enable && Q.call(this), d.bindEvents = !0
            }
        }, E = function () {
            var b = a(this), d = b.data(f), e = d.opt, g = f + "_" + d.idx, h = ".mCSB_" + d.idx + "_scrollbar", i = a("#mCSB_" + d.idx + ",#mCSB_" + d.idx + "_container,#mCSB_" + d.idx + "_container_wrapper," + h + " ." + m[12] + ",#mCSB_" + d.idx + "_dragger_vertical,#mCSB_" + d.idx + "_dragger_horizontal," + h + ">a"), j = a("#mCSB_" + d.idx + "_container");
            e.advanced.releaseDraggableSelectors && i.add(a(e.advanced.releaseDraggableSelectors)), d.bindEvents && (a(c).unbind("." + g), i.each(function () {
                a(this).unbind("." + g)
            }), clearTimeout(b[0]._focusTimeout), _(b[0], "_focusTimeout"), clearTimeout(d.sequential.step), _(d.sequential, "step"), clearTimeout(j[0].onCompleteTimeout), _(j[0], "onCompleteTimeout"), d.bindEvents = !1)
        }, F = function (b) {
            var c = a(this), d = c.data(f), e = d.opt, g = a("#mCSB_" + d.idx + "_container_wrapper"), h = g.length ? g : a("#mCSB_" + d.idx + "_container"), i = [a("#mCSB_" + d.idx + "_scrollbar_vertical"), a("#mCSB_" + d.idx + "_scrollbar_horizontal")], j = [i[0].find(".mCSB_dragger"), i[1].find(".mCSB_dragger")];
            "x" !== e.axis && (d.overflowed[0] && !b ? (i[0].add(j[0]).add(i[0].children("a")).css("display", "block"), h.removeClass(m[8] + " " + m[10])) : (e.alwaysShowScrollbar ? (2 !== e.alwaysShowScrollbar && j[0].css("display", "none"), h.removeClass(m[10])) : (i[0].css("display", "none"), h.addClass(m[10])), h.addClass(m[8]))), "y" !== e.axis && (d.overflowed[1] && !b ? (i[1].add(j[1]).add(i[1].children("a")).css("display", "block"), h.removeClass(m[9] + " " + m[11])) : (e.alwaysShowScrollbar ? (2 !== e.alwaysShowScrollbar && j[1].css("display", "none"), h.removeClass(m[11])) : (i[1].css("display", "none"), h.addClass(m[11])), h.addClass(m[9]))), d.overflowed[0] || d.overflowed[1] ? c.removeClass(m[5]) : c.addClass(m[5])
        }, G = function (a) {
            var b = a.type;
            switch (b) {
                case"pointerdown":
                case"MSPointerDown":
                case"pointermove":
                case"MSPointerMove":
                case"pointerup":
                case"MSPointerUp":
                    return [a.originalEvent.pageY, a.originalEvent.pageX, !1];
                case"touchstart":
                case"touchmove":
                case"touchend":
                    var c = a.originalEvent.touches[0] || a.originalEvent.changedTouches[0], d = a.originalEvent.touches.length || a.originalEvent.changedTouches.length;
                    return [c.pageY, c.pageX, d > 1];
                default:
                    return [a.pageY, a.pageX, !1]
            }
        }, H = function () {
            function b(a) {
                var b = p.find("iframe");
                if (b.length) {
                    var c = a ? "auto" : "none";
                    b.css("pointer-events", c)
                }
            }

            function d(a, b, c, d) {
                if (p[0].idleTimer = m.scrollInertia < 233 ? 250 : 0, e.attr("id") === o[1])var f = "x", g = (e[0].offsetLeft - b + d) * j.scrollRatio.x; else var f = "y", g = (e[0].offsetTop - a + c) * j.scrollRatio.y;
                X(i, g.toString(), {dir: f, drag: !0})
            }

            var e, g, h, i = a(this), j = i.data(f), m = j.opt, n = f + "_" + j.idx, o = ["mCSB_" + j.idx + "_dragger_vertical", "mCSB_" + j.idx + "_dragger_horizontal"], p = a("#mCSB_" + j.idx + "_container"), q = a("#" + o[0] + ",#" + o[1]), r = m.advanced.releaseDraggableSelectors ? q.add(a(m.advanced.releaseDraggableSelectors)) : q;
            q.bind("mousedown." + n + " touchstart." + n + " pointerdown." + n + " MSPointerDown." + n, function (d) {
                if (d.stopImmediatePropagation(), d.preventDefault(), ab(d)) {
                    l = !0, k && (c.onselectstart = function () {
                        return !1
                    }), b(!1), W(i), e = a(this);
                    var f = e.offset(), j = G(d)[0] - f.top, n = G(d)[1] - f.left, o = e.height() + f.top, p = e.width() + f.left;
                    o > j && j > 0 && p > n && n > 0 && (g = j, h = n), A(e, "active", m.autoExpandScrollbar)
                }
            }).bind("touchmove." + n, function (a) {
                a.stopImmediatePropagation(), a.preventDefault();
                var b = e.offset(), c = G(a)[0] - b.top, f = G(a)[1] - b.left;
                d(g, h, c, f)
            }), a(c).bind("mousemove." + n + " pointermove." + n + " MSPointerMove." + n, function (a) {
                if (e) {
                    var b = e.offset(), c = G(a)[0] - b.top, f = G(a)[1] - b.left;
                    if (g === c)return;
                    d(g, h, c, f)
                }
            }).add(r).bind("mouseup." + n + " touchend." + n + " pointerup." + n + " MSPointerUp." + n, function () {
                e && (A(e, "active", m.autoExpandScrollbar), e = null), l = !1, k && (c.onselectstart = null), b(!0)
            })
        }, I = function () {
            function b(a, b) {
                var c = [1.5 * b, 2 * b, b / 1.5, b / 2];
                return a > 90 ? b > 4 ? c[0] : c[3] : a > 60 ? b > 3 ? c[3] : c[2] : a > 30 ? b > 8 ? c[1] : b > 6 ? c[0] : b > 4 ? b : c[2] : b > 8 ? b : c[3]
            }

            function c(a, b, c, d, e, f) {
                a && X(t, a.toString(), {dur: b, scrollEasing: c, dir: d, overwrite: e, drag: f})
            }

            var e, g, h, i, j, k, m, n, o, p, q, r, s, t = a(this), u = t.data(f), v = u.opt, w = f + "_" + u.idx, x = a("#mCSB_" + u.idx), y = a("#mCSB_" + u.idx + "_container"), z = [a("#mCSB_" + u.idx + "_dragger_vertical"), a("#mCSB_" + u.idx + "_dragger_horizontal")], A = [], B = [], C = 0, D = "yx" === v.axis ? "none" : "all", E = [];
            y.bind("touchstart." + w + " pointerdown." + w + " MSPointerDown." + w, function (a) {
                if (!bb(a) || l || G(a)[2])return void(d = 0);
                d = 1, r = 0, s = 0;
                var b = y.offset();
                e = G(a)[0] - b.top, g = G(a)[1] - b.left, E = [G(a)[0], G(a)[1]]
            }).bind("touchmove." + w + " pointermove." + w + " MSPointerMove." + w, function (a) {
                if (bb(a) && !l && !G(a)[2] && (a.stopImmediatePropagation(), !s || r)) {
                    k = Z();
                    var b = x.offset(), d = G(a)[0] - b.top, f = G(a)[1] - b.left, h = "mcsLinearOut";
                    if (A.push(d), B.push(f), E[2] = Math.abs(G(a)[0] - E[0]), E[3] = Math.abs(G(a)[1] - E[1]), u.overflowed[0])var i = z[0].parent().height() - z[0].height(), j = e - d > 0 && d - e > -(i * u.scrollRatio.y) && (2 * E[3] < E[2] || "yx" === v.axis);
                    if (u.overflowed[1])var m = z[1].parent().width() - z[1].width(), n = g - f > 0 && f - g > -(m * u.scrollRatio.x) && (2 * E[2] < E[3] || "yx" === v.axis);
                    j || n ? (a.preventDefault(), r = 1) : s = 1, p = "yx" === v.axis ? [e - d, g - f] : "x" === v.axis ? [null, g - f] : [e - d, null], y[0].idleTimer = 250, u.overflowed[0] && c(p[0], C, h, "y", "all", !0), u.overflowed[1] && c(p[1], C, h, "x", D, !0)
                }
            }), x.bind("touchstart." + w + " pointerdown." + w + " MSPointerDown." + w, function (a) {
                if (!bb(a) || l || G(a)[2])return void(d = 0);
                d = 1, a.stopImmediatePropagation(), W(t), j = Z();
                var b = x.offset();
                h = G(a)[0] - b.top, i = G(a)[1] - b.left, A = [], B = []
            }).bind("touchend." + w + " pointerup." + w + " MSPointerUp." + w, function (a) {
                if (bb(a) && !l && !G(a)[2]) {
                    a.stopImmediatePropagation(), r = 0, s = 0, m = Z();
                    var d = x.offset(), e = G(a)[0] - d.top, f = G(a)[1] - d.left;
                    if (!(m - k > 30)) {
                        o = 1e3 / (m - j);
                        var g = "mcsEaseOut", t = 2.5 > o, w = t ? [A[A.length - 2], B[B.length - 2]] : [0, 0];
                        n = t ? [e - w[0], f - w[1]] : [e - h, f - i];
                        var z = [Math.abs(n[0]), Math.abs(n[1])];
                        o = t ? [Math.abs(n[0] / 4), Math.abs(n[1] / 4)] : [o, o];
                        var C = [Math.abs(y[0].offsetTop) - n[0] * b(z[0] / o[0], o[0]), Math.abs(y[0].offsetLeft) - n[1] * b(z[1] / o[1], o[1])];
                        p = "yx" === v.axis ? [C[0], C[1]] : "x" === v.axis ? [null, C[1]] : [C[0], null], q = [4 * z[0] + v.scrollInertia, 4 * z[1] + v.scrollInertia];
                        var E = parseInt(v.contentTouchScroll) || 0;
                        p[0] = z[0] > E ? p[0] : 0, p[1] = z[1] > E ? p[1] : 0, u.overflowed[0] && c(p[0], q[0], g, "y", D, !1), u.overflowed[1] && c(p[1], q[1], g, "x", D, !1)
                    }
                }
            })
        }, J = function () {
            function e() {
                return b.getSelection ? b.getSelection().toString() : c.selection && "Control" != c.selection.type ? c.selection.createRange().text : 0
            }

            function g(a, b, c) {
                m.type = c && h ? "stepped" : "stepless", m.scrollAmount = 10, R(i, a, b, "mcsLinearOut", c ? 60 : null)
            }

            var h, i = a(this), j = i.data(f), k = j.opt, m = j.sequential, n = f + "_" + j.idx, o = a("#mCSB_" + j.idx + "_container"), p = o.parent();
            o.bind("mousedown." + n, function () {
                d || h || (h = 1, l = !0)
            }).add(c).bind("mousemove." + n, function (a) {
                if (!d && h && e()) {
                    var b = o.offset(), c = G(a)[0] - b.top + o[0].offsetTop, f = G(a)[1] - b.left + o[0].offsetLeft;
                    c > 0 && c < p.height() && f > 0 && f < p.width() ? m.step && g("off", null, "stepped") : ("x" !== k.axis && j.overflowed[0] && (0 > c ? g("on", 38) : c > p.height() && g("on", 40)), "y" !== k.axis && j.overflowed[1] && (0 > f ? g("on", 37) : f > p.width() && g("on", 39)))
                }
            }).bind("mouseup." + n, function () {
                d || (h && (h = 0, g("off", null)), l = !1)
            })
        }, K = function () {
            function b(a) {
                var b = null;
                try {
                    var c = a.contentDocument || a.contentWindow.document;
                    b = c.body.innerHTML
                } catch (d) {
                }
                return null !== b
            }

            var c = a(this), d = c.data(f);
            if (d) {
                var e = d.opt, g = f + "_" + d.idx, h = a("#mCSB_" + d.idx), i = [a("#mCSB_" + d.idx + "_dragger_vertical"), a("#mCSB_" + d.idx + "_dragger_horizontal")], j = a("#mCSB_" + d.idx + "_container").find("iframe"), l = h;
                j.length && j.each(function () {
                    var c = this;
                    b(c) && (l = l.add(a(c).contents().find("body")))
                }), l.bind("mousewheel." + g, function (b, f) {
                    if (W(c), !L(c, b.target)) {
                        var g = "auto" !== e.mouseWheel.deltaFactor ? parseInt(e.mouseWheel.deltaFactor) : k && b.deltaFactor < 100 ? 100 : b.deltaFactor || 100;
                        if ("x" === e.axis || "x" === e.mouseWheel.axis)var j = "x", l = [Math.round(g * d.scrollRatio.x), parseInt(e.mouseWheel.scrollAmount)], m = "auto" !== e.mouseWheel.scrollAmount ? l[1] : l[0] >= h.width() ? .9 * h.width() : l[0], n = Math.abs(a("#mCSB_" + d.idx + "_container")[0].offsetLeft), o = i[1][0].offsetLeft, p = i[1].parent().width() - i[1].width(), q = b.deltaX || b.deltaY || f; else var j = "y", l = [Math.round(g * d.scrollRatio.y), parseInt(e.mouseWheel.scrollAmount)], m = "auto" !== e.mouseWheel.scrollAmount ? l[1] : l[0] >= h.height() ? .9 * h.height() : l[0], n = Math.abs(a("#mCSB_" + d.idx + "_container")[0].offsetTop), o = i[0][0].offsetTop, p = i[0].parent().height() - i[0].height(), q = b.deltaY || f;
                        "y" === j && !d.overflowed[0] || "x" === j && !d.overflowed[1] || (e.mouseWheel.invert && (q = -q), e.mouseWheel.normalizeDelta && (q = 0 > q ? -1 : 1), (q > 0 && 0 !== o || 0 > q && o !== p || e.mouseWheel.preventDefault) && (b.stopImmediatePropagation(), b.preventDefault()), X(c, (n - q * m).toString(), {dir: j}))
                    }
                })
            }
        }, L = function (b, c) {
            var d = c.nodeName.toLowerCase(), e = b.data(f).opt.mouseWheel.disableOver, g = ["select", "textarea"];
            return a.inArray(d, e) > -1 && !(a.inArray(d, g) > -1 && !a(c).is(":focus"))
        }, M = function () {
            var b = a(this), c = b.data(f), d = f + "_" + c.idx, e = a("#mCSB_" + c.idx + "_container"), g = e.parent(), h = a(".mCSB_" + c.idx + "_scrollbar ." + m[12]);
            h.bind("touchstart." + d + " pointerdown." + d + " MSPointerDown." + d, function () {
                l = !0
            }).bind("touchend." + d + " pointerup." + d + " MSPointerUp." + d, function () {
                l = !1
            }).bind("click." + d, function (d) {
                if (a(d.target).hasClass(m[12]) || a(d.target).hasClass("mCSB_draggerRail")) {
                    W(b);
                    var f = a(this), h = f.find(".mCSB_dragger");
                    if (f.parent(".mCSB_scrollTools_horizontal").length > 0) {
                        if (!c.overflowed[1])return;
                        var i = "x", j = d.pageX > h.offset().left ? -1 : 1, k = Math.abs(e[0].offsetLeft) - .9 * j * g.width()
                    } else {
                        if (!c.overflowed[0])return;
                        var i = "y", j = d.pageY > h.offset().top ? -1 : 1, k = Math.abs(e[0].offsetTop) - .9 * j * g.height()
                    }
                    X(b, k.toString(), {dir: i, scrollEasing: "mcsEaseInOut"})
                }
            })
        }, N = function () {
            var b = a(this), d = b.data(f), e = d.opt, g = f + "_" + d.idx, h = a("#mCSB_" + d.idx + "_container"), i = h.parent();
            h.bind("focusin." + g, function () {
                var d = a(c.activeElement), f = h.find(".mCustomScrollBox").length, g = 0;
                d.is(e.advanced.autoScrollOnFocus) && (W(b), clearTimeout(b[0]._focusTimeout), b[0]._focusTimer = f ? (g + 17) * f : 0, b[0]._focusTimeout = setTimeout(function () {
                    var a = [db(d)[0], db(d)[1]], c = [h[0].offsetTop, h[0].offsetLeft], f = [c[0] + a[0] >= 0 && c[0] + a[0] < i.height() - d.outerHeight(!1), c[1] + a[1] >= 0 && c[0] + a[1] < i.width() - d.outerWidth(!1)], j = "yx" !== e.axis || f[0] || f[1] ? "all" : "none";
                    "x" === e.axis || f[0] || X(b, a[0].toString(), {
                        dir: "y",
                        scrollEasing: "mcsEaseInOut",
                        overwrite: j,
                        dur: g
                    }), "y" === e.axis || f[1] || X(b, a[1].toString(), {
                        dir: "x",
                        scrollEasing: "mcsEaseInOut",
                        overwrite: j,
                        dur: g
                    })
                }, b[0]._focusTimer))
            })
        }, O = function () {
            var b = a(this), c = b.data(f), d = f + "_" + c.idx, e = a("#mCSB_" + c.idx + "_container").parent();
            e.bind("scroll." + d, function () {
                (0 !== e.scrollTop() || 0 !== e.scrollLeft()) && a(".mCSB_" + c.idx + "_scrollbar").css("visibility", "hidden")
            })
        }, P = function () {
            var b = a(this), c = b.data(f), d = c.opt, e = c.sequential, g = f + "_" + c.idx, h = ".mCSB_" + c.idx + "_scrollbar", i = a(h + ">a");
            i.bind("mousedown." + g + " touchstart." + g + " pointerdown." + g + " MSPointerDown." + g + " mouseup." + g + " touchend." + g + " pointerup." + g + " MSPointerUp." + g + " mouseout." + g + " pointerout." + g + " MSPointerOut." + g + " click." + g, function (f) {
                function g(a, c) {
                    e.scrollAmount = d.snapAmount || d.scrollButtons.scrollAmount, R(b, a, c)
                }

                if (f.preventDefault(), ab(f)) {
                    var h = a(this).attr("class");
                    switch (e.type = d.scrollButtons.scrollType, f.type) {
                        case"mousedown":
                        case"touchstart":
                        case"pointerdown":
                        case"MSPointerDown":
                            if ("stepped" === e.type)return;
                            l = !0, c.tweenRunning = !1, g("on", h);
                            break;
                        case"mouseup":
                        case"touchend":
                        case"pointerup":
                        case"MSPointerUp":
                        case"mouseout":
                        case"pointerout":
                        case"MSPointerOut":
                            if ("stepped" === e.type)return;
                            l = !1, e.dir && g("off", h);
                            break;
                        case"click":
                            if ("stepped" !== e.type || c.tweenRunning)return;
                            g("on", h)
                    }
                }
            })
        }, Q = function () {
            var b = a(this), d = b.data(f), e = d.opt, g = d.sequential, h = f + "_" + d.idx, i = a("#mCSB_" + d.idx), j = a("#mCSB_" + d.idx + "_container"), k = j.parent(), l = "input,textarea,select,datalist,keygen,[contenteditable='true']";
            i.attr("tabindex", "0").bind("blur." + h + " keydown." + h + " keyup." + h, function (f) {
                function h(a, c) {
                    g.type = e.keyboard.scrollType, g.scrollAmount = e.snapAmount || e.keyboard.scrollAmount, "stepped" === g.type && d.tweenRunning || R(b, a, c)
                }

                switch (f.type) {
                    case"blur":
                        d.tweenRunning && g.dir && h("off", null);
                        break;
                    case"keydown":
                    case"keyup":
                        var i = f.keyCode ? f.keyCode : f.which, m = "on";
                        if ("x" !== e.axis && (38 === i || 40 === i) || "y" !== e.axis && (37 === i || 39 === i)) {
                            if ((38 === i || 40 === i) && !d.overflowed[0] || (37 === i || 39 === i) && !d.overflowed[1])return;
                            "keyup" === f.type && (m = "off"), a(c.activeElement).is(l) || (f.preventDefault(), f.stopImmediatePropagation(), h(m, i))
                        } else if (33 === i || 34 === i) {
                            if ((d.overflowed[0] || d.overflowed[1]) && (f.preventDefault(), f.stopImmediatePropagation()), "keyup" === f.type) {
                                W(b);
                                var n = 34 === i ? -1 : 1;
                                if ("x" === e.axis || "yx" === e.axis && d.overflowed[1] && !d.overflowed[0])var o = "x", p = Math.abs(j[0].offsetLeft) - .9 * n * k.width(); else var o = "y", p = Math.abs(j[0].offsetTop) - .9 * n * k.height();
                                X(b, p.toString(), {dir: o, scrollEasing: "mcsEaseInOut"})
                            }
                        } else if ((35 === i || 36 === i) && !a(c.activeElement).is(l) && ((d.overflowed[0] || d.overflowed[1]) && (f.preventDefault(), f.stopImmediatePropagation()), "keyup" === f.type)) {
                            if ("x" === e.axis || "yx" === e.axis && d.overflowed[1] && !d.overflowed[0])var o = "x", p = 35 === i ? Math.abs(k.width() - j.outerWidth(!1)) : 0; else var o = "y", p = 35 === i ? Math.abs(k.height() - j.outerHeight(!1)) : 0;
                            X(b, p.toString(), {dir: o, scrollEasing: "mcsEaseInOut"})
                        }
                }
            })
        }, R = function (b, c, d, e, g) {
            function h(a) {
                var c = "stepped" !== l.type, d = g ? g : a ? c ? p / 1.5 : q : 1e3 / 60, f = a ? c ? 7.5 : 40 : 2.5, i = [Math.abs(n[0].offsetTop), Math.abs(n[0].offsetLeft)], k = [j.scrollRatio.y > 10 ? 10 : j.scrollRatio.y, j.scrollRatio.x > 10 ? 10 : j.scrollRatio.x], m = "x" === l.dir[0] ? i[1] + l.dir[1] * k[1] * f : i[0] + l.dir[1] * k[0] * f, o = "x" === l.dir[0] ? i[1] + l.dir[1] * parseInt(l.scrollAmount) : i[0] + l.dir[1] * parseInt(l.scrollAmount), r = "auto" !== l.scrollAmount ? o : m, s = e ? e : a ? c ? "mcsLinearOut" : "mcsEaseInOut" : "mcsLinear", t = a ? !0 : !1;
                return a && 17 > d && (r = "x" === l.dir[0] ? i[1] : i[0]), X(b, r.toString(), {
                    dir: l.dir[0],
                    scrollEasing: s,
                    dur: d,
                    onComplete: t
                }), a ? void(l.dir = !1) : (clearTimeout(l.step), void(l.step = setTimeout(function () {
                    h()
                }, d)))
            }

            function i() {
                clearTimeout(l.step), _(l, "step"), W(b)
            }

            var j = b.data(f), k = j.opt, l = j.sequential, n = a("#mCSB_" + j.idx + "_container"), o = "stepped" === l.type ? !0 : !1, p = k.scrollInertia < 26 ? 26 : k.scrollInertia, q = k.scrollInertia < 1 ? 17 : k.scrollInertia;
            switch (c) {
                case"on":
                    if (l.dir = [d === m[16] || d === m[15] || 39 === d || 37 === d ? "x" : "y", d === m[13] || d === m[15] || 38 === d || 37 === d ? -1 : 1], W(b), cb(d) && "stepped" === l.type)return;
                    h(o);
                    break;
                case"off":
                    i(), (o || j.tweenRunning && l.dir) && h(!0)
            }
        }, S = function (b) {
            var c = a(this).data(f).opt, d = [];
            return "function" == typeof b && (b = b()), b instanceof Array ? d = b.length > 1 ? [b[0], b[1]] : "x" === c.axis ? [null, b[0]] : [b[0], null] : (d[0] = b.y ? b.y : b.x || "x" === c.axis ? null : b, d[1] = b.x ? b.x : b.y || "y" === c.axis ? null : b), "function" == typeof d[0] && (d[0] = d[0]()), "function" == typeof d[1] && (d[1] = d[1]()), d
        }, T = function (b, c) {
            if (null != b && "undefined" != typeof b) {
                var d = a(this), e = d.data(f), g = e.opt, h = a("#mCSB_" + e.idx + "_container"), i = h.parent(), j = typeof b;
                c || (c = "x" === g.axis ? "x" : "y");
                var k = "x" === c ? h.outerWidth(!1) : h.outerHeight(!1), l = "x" === c ? h[0].offsetLeft : h[0].offsetTop, m = "x" === c ? "left" : "top";
                switch (j) {
                    case"function":
                        return b();
                    case"object":
                        var o = b.jquery ? b : a(b);
                        if (!o.length)return;
                        return "x" === c ? db(o)[1] : db(o)[0];
                    case"string":
                    case"number":
                        if (cb(b))return Math.abs(b);
                        if (-1 !== b.indexOf("%"))return Math.abs(k * parseInt(b) / 100);
                        if (-1 !== b.indexOf("-="))return Math.abs(l - parseInt(b.split("-=")[1]));
                        if (-1 !== b.indexOf("+=")) {
                            var p = l + parseInt(b.split("+=")[1]);
                            return p >= 0 ? 0 : Math.abs(p)
                        }
                        if (-1 !== b.indexOf("px") && cb(b.split("px")[0]))return Math.abs(b.split("px")[0]);
                        if ("top" === b || "left" === b)return 0;
                        if ("bottom" === b)return Math.abs(i.height() - h.outerHeight(!1));
                        if ("right" === b)return Math.abs(i.width() - h.outerWidth(!1));
                        if ("first" === b || "last" === b) {
                            var o = h.find(":" + b);
                            return "x" === c ? db(o)[1] : db(o)[0]
                        }
                        return a(b).length ? "x" === c ? db(a(b))[1] : db(a(b))[0] : (h.css(m, b), void n.update.call(null, d[0]))
                }
            }
        }, U = function (b) {
            function c() {
                clearTimeout(l[0].autoUpdate), l[0].autoUpdate = setTimeout(function () {
                    return k.advanced.updateOnSelectorChange && (o = g(), o !== u) ? (h(3), void(u = o)) : (k.advanced.updateOnContentResize && (p = [l.outerHeight(!1), l.outerWidth(!1), r.height(), r.width(), t()[0], t()[1]], (p[0] !== v[0] || p[1] !== v[1] || p[2] !== v[2] || p[3] !== v[3] || p[4] !== v[4] || p[5] !== v[5]) && (h(p[0] !== v[0] || p[1] !== v[1]), v = p)), k.advanced.updateOnImageLoad && (q = d(), q !== w && (l.find("img").each(function () {
                        e(this)
                    }), w = q)), void((k.advanced.updateOnSelectorChange || k.advanced.updateOnContentResize || k.advanced.updateOnImageLoad) && c()))
                }, 60)
            }

            function d() {
                var a = 0;
                return k.advanced.updateOnImageLoad && (a = l.find("img").length), a
            }

            function e(b) {
                function c(a, b) {
                    return function () {
                        return b.apply(a, arguments)
                    }
                }

                function d() {
                    this.onload = null, a(b).addClass(m[2]), h(2)
                }

                if (a(b).hasClass(m[2]))return void h();
                var e = new Image;
                e.onload = c(e, d), e.src = b.src
            }

            function g() {
                k.advanced.updateOnSelectorChange === !0 && (k.advanced.updateOnSelectorChange = "*");
                var b = 0, c = l.find(k.advanced.updateOnSelectorChange);
                return k.advanced.updateOnSelectorChange && c.length > 0 && c.each(function () {
                    b += a(this).height() + a(this).width()
                }), b
            }

            function h(a) {
                clearTimeout(l[0].autoUpdate), n.update.call(null, i[0], a)
            }

            var i = a(this), j = i.data(f), k = j.opt, l = a("#mCSB_" + j.idx + "_container");
            if (b)return clearTimeout(l[0].autoUpdate), void _(l[0], "autoUpdate");
            var o, p, q, r = l.parent(), s = [a("#mCSB_" + j.idx + "_scrollbar_vertical"), a("#mCSB_" + j.idx + "_scrollbar_horizontal")], t = function () {
                return [s[0].is(":visible") ? s[0].outerHeight(!0) : 0, s[1].is(":visible") ? s[1].outerWidth(!0) : 0]
            }, u = g(), v = [l.outerHeight(!1), l.outerWidth(!1), r.height(), r.width(), t()[0], t()[1]], w = d();
            c()
        }, V = function (a, b, c) {
            return Math.round(a / b) * b - c
        }, W = function (b) {
            var c = b.data(f), d = a("#mCSB_" + c.idx + "_container,#mCSB_" + c.idx + "_container_wrapper,#mCSB_" + c.idx + "_dragger_vertical,#mCSB_" + c.idx + "_dragger_horizontal");
            d.each(function () {
                $.call(this)
            })
        }, X = function (b, c, d) {
            function e(a) {
                return i && j.callbacks[a] && "function" == typeof j.callbacks[a]
            }

            function g() {
                return [j.callbacks.alwaysTriggerOffsets || t >= u[0] + w, j.callbacks.alwaysTriggerOffsets || -x >= t]
            }

            function h() {
                var a = [n[0].offsetTop, n[0].offsetLeft], c = [r[0].offsetTop, r[0].offsetLeft], e = [n.outerHeight(!1), n.outerWidth(!1)], f = [m.height(), m.width()];
                b[0].mcs = {
                    content: n,
                    top: a[0],
                    left: a[1],
                    draggerTop: c[0],
                    draggerLeft: c[1],
                    topPct: Math.round(100 * Math.abs(a[0]) / (Math.abs(e[0]) - f[0])),
                    leftPct: Math.round(100 * Math.abs(a[1]) / (Math.abs(e[1]) - f[1])),
                    direction: d.dir
                }
            }

            var i = b.data(f), j = i.opt, k = {
                trigger: "internal",
                dir: "y",
                scrollEasing: "mcsEaseOut",
                drag: !1,
                dur: j.scrollInertia,
                overwrite: "all",
                callbacks: !0,
                onStart: !0,
                onUpdate: !0,
                onComplete: !0
            }, d = a.extend(k, d), l = [d.dur, d.drag ? 0 : d.dur], m = a("#mCSB_" + i.idx), n = a("#mCSB_" + i.idx + "_container"), o = n.parent(), p = j.callbacks.onTotalScrollOffset ? S.call(b, j.callbacks.onTotalScrollOffset) : [0, 0], q = j.callbacks.onTotalScrollBackOffset ? S.call(b, j.callbacks.onTotalScrollBackOffset) : [0, 0];
            if (i.trigger = d.trigger, (0 !== o.scrollTop() || 0 !== o.scrollLeft()) && (a(".mCSB_" + i.idx + "_scrollbar").css("visibility", "visible"), o.scrollTop(0).scrollLeft(0)), "_resetY" !== c || i.contentReset.y || (e("onOverflowYNone") && j.callbacks.onOverflowYNone.call(b[0]), i.contentReset.y = 1), "_resetX" !== c || i.contentReset.x || (e("onOverflowXNone") && j.callbacks.onOverflowXNone.call(b[0]), i.contentReset.x = 1), "_resetY" !== c && "_resetX" !== c) {
                switch (!i.contentReset.y && b[0].mcs || !i.overflowed[0] || (e("onOverflowY") && j.callbacks.onOverflowY.call(b[0]), i.contentReset.x = null), !i.contentReset.x && b[0].mcs || !i.overflowed[1] || (e("onOverflowX") && j.callbacks.onOverflowX.call(b[0]), i.contentReset.x = null), j.snapAmount && (c = V(c, j.snapAmount, j.snapOffset)), d.dir) {
                    case"x":
                        var r = a("#mCSB_" + i.idx + "_dragger_horizontal"), s = "left", t = n[0].offsetLeft, u = [m.width() - n.outerWidth(!1), r.parent().width() - r.width()], v = [c, 0 === c ? 0 : c / i.scrollRatio.x], w = p[1], x = q[1], y = w > 0 ? w / i.scrollRatio.x : 0, z = x > 0 ? x / i.scrollRatio.x : 0;
                        break;
                    case"y":
                        var r = a("#mCSB_" + i.idx + "_dragger_vertical"), s = "top", t = n[0].offsetTop, u = [m.height() - n.outerHeight(!1), r.parent().height() - r.height()], v = [c, 0 === c ? 0 : c / i.scrollRatio.y], w = p[0], x = q[0], y = w > 0 ? w / i.scrollRatio.y : 0, z = x > 0 ? x / i.scrollRatio.y : 0
                }
                v[1] < 0 || 0 === v[0] && 0 === v[1] ? v = [0, 0] : v[1] >= u[1] ? v = [u[0], u[1]] : v[0] = -v[0], b[0].mcs || (h(), e("onInit") && j.callbacks.onInit.call(b[0])), clearTimeout(n[0].onCompleteTimeout), (i.tweenRunning || !(0 === t && v[0] >= 0 || t === u[0] && v[0] <= u[0])) && (Y(r[0], s, Math.round(v[1]), l[1], d.scrollEasing), Y(n[0], s, Math.round(v[0]), l[0], d.scrollEasing, d.overwrite, {
                    onStart: function () {
                        d.callbacks && d.onStart && !i.tweenRunning && (e("onScrollStart") && (h(), j.callbacks.onScrollStart.call(b[0])), i.tweenRunning = !0, A(r), i.cbOffsets = g())
                    }, onUpdate: function () {
                        d.callbacks && d.onUpdate && e("whileScrolling") && (h(), j.callbacks.whileScrolling.call(b[0]))
                    }, onComplete: function () {
                        if (d.callbacks && d.onComplete) {
                            "yx" === j.axis && clearTimeout(n[0].onCompleteTimeout);
                            var a = n[0].idleTimer || 0;
                            n[0].onCompleteTimeout = setTimeout(function () {
                                e("onScroll") && (h(), j.callbacks.onScroll.call(b[0])), e("onTotalScroll") && v[1] >= u[1] - y && i.cbOffsets[0] && (h(), j.callbacks.onTotalScroll.call(b[0])), e("onTotalScrollBack") && v[1] <= z && i.cbOffsets[1] && (h(), j.callbacks.onTotalScrollBack.call(b[0])), i.tweenRunning = !1, n[0].idleTimer = 0, A(r, "hide")
                            }, a)
                        }
                    }
                }))
            }
        }, Y = function (a, c, d, e, f, g, h) {
            function i() {
                w.stop || (t || p.call(), t = Z() - s, j(), t >= w.time && (w.time = t > w.time ? t + n - (t - w.time) : t + n - 1, w.time < t + 1 && (w.time = t + 1)), w.time < e ? w.id = o(i) : r.call())
            }

            function j() {
                e > 0 ? (w.currVal = m(w.time, u, x, e, f), v[c] = Math.round(w.currVal) + "px") : v[c] = d + "px", q.call()
            }

            function k() {
                n = 1e3 / 60, w.time = t + n, o = b.requestAnimationFrame ? b.requestAnimationFrame : function (a) {
                    return j(), setTimeout(a, .01)
                }, w.id = o(i)
            }

            function l() {
                null != w.id && (b.requestAnimationFrame ? b.cancelAnimationFrame(w.id) : clearTimeout(w.id), w.id = null)
            }

            function m(a, b, c, d, e) {
                switch (e) {
                    case"linear":
                    case"mcsLinear":
                        return c * a / d + b;
                    case"mcsLinearOut":
                        return a /= d, a--, c * Math.sqrt(1 - a * a) + b;
                    case"easeInOutSmooth":
                        return a /= d / 2, 1 > a ? c / 2 * a * a + b : (a--, -c / 2 * (a * (a - 2) - 1) + b);
                    case"easeInOutStrong":
                        return a /= d / 2, 1 > a ? c / 2 * Math.pow(2, 10 * (a - 1)) + b : (a--, c / 2 * (-Math.pow(2, -10 * a) + 2) + b);
                    case"easeInOut":
                    case"mcsEaseInOut":
                        return a /= d / 2, 1 > a ? c / 2 * a * a * a + b : (a -= 2, c / 2 * (a * a * a + 2) + b);
                    case"easeOutSmooth":
                        return a /= d, a--, -c * (a * a * a * a - 1) + b;
                    case"easeOutStrong":
                        return c * (-Math.pow(2, -10 * a / d) + 1) + b;
                    case"easeOut":
                    case"mcsEaseOut":
                    default:
                        var f = (a /= d) * a, g = f * a;
                        return b + c * (.499999999999997 * g * f + -2.5 * f * f + 5.5 * g + -6.5 * f + 4 * a)
                }
            }

            a._mTween || (a._mTween = {top: {}, left: {}});
            var n, o, h = h || {}, p = h.onStart || function () {
                }, q = h.onUpdate || function () {
                }, r = h.onComplete || function () {
                }, s = Z(), t = 0, u = a.offsetTop, v = a.style, w = a._mTween[c];
            "left" === c && (u = a.offsetLeft);
            var x = d - u;
            w.stop = 0, "none" !== g && l(), k()
        }, Z = function () {
            return b.performance && b.performance.now ? b.performance.now() : b.performance && b.performance.webkitNow ? b.performance.webkitNow() : Date.now ? Date.now() : (new Date).getTime()
        }, $ = function () {
            var a = this;
            a._mTween || (a._mTween = {top: {}, left: {}});
            for (var c = ["top", "left"], d = 0; d < c.length; d++) {
                var e = c[d];
                a._mTween[e].id && (b.requestAnimationFrame ? b.cancelAnimationFrame(a._mTween[e].id) : clearTimeout(a._mTween[e].id), a._mTween[e].id = null, a._mTween[e].stop = 1)
            }
        }, _ = function (a, b) {
            try {
                delete a[b]
            } catch (c) {
                a[b] = null
            }
        }, ab = function (a) {
            return !(a.which && 1 !== a.which)
        }, bb = function (a) {
            var b = a.originalEvent.pointerType;
            return !(b && "touch" !== b && 2 !== b)
        }, cb = function (a) {
            return !isNaN(parseFloat(a)) && isFinite(a)
        }, db = function (a) {
            var b = a.parents(".mCSB_container");
            return [a.offset().top - b.offset().top, a.offset().left - b.offset().left]
        };
        a.fn[e] = function (b) {
            return n[b] ? n[b].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof b && b ? void a.error("Method " + b + " does not exist") : n.init.apply(this, arguments)
        }, a[e] = function (b) {
            return n[b] ? n[b].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof b && b ? void a.error("Method " + b + " does not exist") : n.init.apply(this, arguments)
        }, a[e].defaults = h, b[e] = !0, a(b).load(function () {
            a(g)[e](), a.extend(a.expr[":"], {
                mcsInView: a.expr[":"].mcsInView || function (b) {
                    var c, d, e = a(b), f = e.parents(".mCSB_container");
                    if (f.length)return c = f.parent(), d = [f[0].offsetTop, f[0].offsetLeft], d[0] + db(e)[0] >= 0 && d[0] + db(e)[0] < c.height() - e.outerHeight(!1) && d[1] + db(e)[1] >= 0 && d[1] + db(e)[1] < c.width() - e.outerWidth(!1)
                }, mcsOverflow: a.expr[":"].mcsOverflow || function (b) {
                    var c = a(b).data(f);
                    if (c)return c.overflowed[0] || c.overflowed[1]
                }
            })
        })
    })
}(jQuery, window, document), function (a, b) {
    "function" == typeof define && define.amd ? define(b) : a.BackgroundCheck = b(a)
}(this, function () {
    "use strict";
    function a(a) {
        if (void 0 === a || void 0 === a.targets)throw"Missing attributes";
        H.debug = d(a.debug, !1), H.debugOverlay = d(a.debugOverlay, !1), H.targets = g(a.targets), H.image = g(a.image || "img", !0), H.changeParent = d(a.changeParent, !1), H.threshold = d(a.threshold, 50), H.minComplexity = d(a.minComplexity, 30), H.minOverlap = d(a.minOverlap, 50), H.windowEvents = d(a.windowEvents, !0), H.maxDuration = d(a.maxDuration, 500), H.mask = d(a.mask, {
            r: 0,
            g: 255,
            b: 0
        }), H.classes = d(a.classes, {
            dark: "background--dark",
            light: "background--light",
            complex: "background--complex"
        }), void 0 === B && (h(), B && (C.style.position = "fixed", C.style.top = "0px", C.style.left = "0px", C.style.width = "100%", C.style.height = "100%", window.addEventListener(G, x.bind(null, function () {
            k(), w()
        })), window.addEventListener("scroll", x.bind(null, w)), k(), w()))
    }

    function b() {
        B = null, C = null, D = null, H = {}, E && clearTimeout(E)
    }

    function c(a) {
        z("debug") && console.log(a)
    }

    function d(a, b) {
        return e(a, typeof b), void 0 === a ? b : a
    }

    function e(a, b) {
        if (void 0 !== a && typeof a !== b)throw"Incorrect attribute type"
    }

    function f(a) {
        for (var b, d, e = [], f = 0; f < a.length; f++)if (b = a[f], e.push(b), "IMG" !== b.tagName) {
            if (d = window.getComputedStyle(b).backgroundImage, d.split(/,url|, url/).length > 1)throw"Multiple backgrounds are not supported";
            if (!d || "none" === d)throw"Element is not an <img> but does not have a background-image";
            e[f] = {
                img: new Image,
                el: e[f]
            }, d = d.slice(4, -1), d = d.replace(/"/g, ""), e[f].img.src = d, c("CSS Image - " + d)
        }
        return e
    }

    function g(a, b) {
        var c = a;
        if ("string" == typeof a ? c = document.querySelectorAll(a) : a && 1 === a.nodeType && (c = [a]), !c || 0 === c.length || void 0 === c.length)throw"Elements not found";
        return b && (c = f(c)), c = Array.prototype.slice.call(c)
    }

    function h() {
        C = document.createElement("canvas"), C && C.getContext ? (D = C.getContext("2d"), B = !0) : B = !1, i()
    }

    function i() {
        z("debugOverlay") ? (C.style.opacity = .5, C.style.pointerEvents = "none", document.body.appendChild(C)) : C.parentNode && C.parentNode.removeChild(C)
    }

    function j(a) {
        var d = (new Date).getTime() - a;
        c("Duration: " + d + "ms"), d > z("maxDuration") && (console.log("BackgroundCheck - Killed"), q(), b())
    }

    function k() {
        F = {
            left: 0,
            top: 0,
            right: document.body.clientWidth,
            bottom: window.innerHeight
        }, C.width = document.body.clientWidth, C.height = window.innerHeight
    }

    function l(a, b, c) {
        var d, e;
        return -1 !== a.indexOf("px") ? d = parseFloat(a) : -1 !== a.indexOf("%") ? (d = parseFloat(a), e = d / 100, d = e * b, c && (d -= c * e)) : d = b, d
    }

    function m(a) {
        var b = window.getComputedStyle(a.el);
        a.el.style.backgroundRepeat = "no-repeat", a.el.style.backgroundOrigin = "padding-box";
        var c = b.backgroundSize.split(" "), d = c[0], e = void 0 === c[1] ? "auto" : c[1], f = a.el.clientWidth / a.el.clientHeight, g = a.img.naturalWidth / a.img.naturalHeight;
        "cover" === d ? f >= g ? (d = "100%", e = "auto") : (d = "auto", c[0] = "auto", e = "100%") : "contain" === d && (1 / g > 1 / f ? (d = "auto", c[0] = "auto", e = "100%") : (d = "100%", e = "auto")), d = "auto" === d ? a.img.naturalWidth : l(d, a.el.clientWidth), e = "auto" === e ? d / a.img.naturalWidth * a.img.naturalHeight : l(e, a.el.clientHeight), "auto" === c[0] && "auto" !== c[1] && (d = e / a.img.naturalHeight * a.img.naturalWidth);
        var h = b.backgroundPosition;
        "top" === h ? h = "50% 0%" : "left" === h ? h = "0% 50%" : "right" === h ? h = "100% 50%" : "bottom" === h ? h = "50% 100%" : "center" === h && (h = "50% 50%"), h = h.split(" ");
        var i, j;
        return 4 === h.length ? (i = h[1], j = h[3]) : (i = h[0], j = h[1]), j = j || "50%", i = l(i, a.el.clientWidth, d), j = l(j, a.el.clientHeight, e), 4 === h.length && ("right" === h[0] && (i = a.el.clientWidth - a.img.naturalWidth - i), "bottom" === h[2] && (j = a.el.clientHeight - a.img.naturalHeight - j)), i += a.el.getBoundingClientRect().left, j += a.el.getBoundingClientRect().top, {
            left: Math.floor(i),
            right: Math.floor(i + d),
            top: Math.floor(j),
            bottom: Math.floor(j + e),
            width: Math.floor(d),
            height: Math.floor(e)
        }
    }

    function n(a) {
        var b, c, d;
        if (a.nodeType) {
            var e = a.getBoundingClientRect();
            b = {
                left: e.left,
                right: e.right,
                top: e.top,
                bottom: e.bottom,
                width: e.width,
                height: e.height
            }, d = a.parentNode, c = a
        } else b = m(a), d = a.el, c = a.img;
        d = d.getBoundingClientRect(), b.imageTop = 0, b.imageLeft = 0, b.imageWidth = c.naturalWidth, b.imageHeight = c.naturalHeight;
        var f, g = b.imageHeight / b.height;
        return b.top < d.top && (f = d.top - b.top, b.imageTop = g * f, b.imageHeight -= g * f, b.top += f, b.height -= f), b.left < d.left && (f = d.left - b.left, b.imageLeft += g * f, b.imageWidth -= g * f, b.width -= f, b.left += f), b.bottom > d.bottom && (f = b.bottom - d.bottom, b.imageHeight -= g * f, b.height -= f), b.right > d.right && (f = b.right - d.right, b.imageWidth -= g * f, b.width -= f), b.imageTop = Math.floor(b.imageTop), b.imageLeft = Math.floor(b.imageLeft), b.imageHeight = Math.floor(b.imageHeight), b.imageWidth = Math.floor(b.imageWidth), b
    }

    function o(a) {
        var b = n(a);
        a = a.nodeType ? a : a.img, b.imageWidth > 0 && b.imageHeight > 0 && b.width > 0 && b.height > 0 ? D.drawImage(a, b.imageLeft, b.imageTop, b.imageWidth, b.imageHeight, b.left, b.top, b.width, b.height) : c("Skipping image - " + a.src + " - area too small")
    }

    function p(a, b, c) {
        var d = a.className;
        switch (c) {
            case"add":
                d += " " + b;
                break;
            case"remove":
                var e = new RegExp("(?:^|\\s)" + b + "(?!\\S)", "g");
                d = d.replace(e, "")
        }
        a.className = d.trim()
    }

    function q(a) {
        for (var b, c = a ? [a] : z("targets"), d = 0; d < c.length; d++)b = c[d], b = z("changeParent") ? b.parentNode : b, p(b, z("classes").light, "remove"), p(b, z("classes").dark, "remove"), p(b, z("classes").complex, "remove")
    }

    function r(a) {
        var b, d, e, f, g = a.getBoundingClientRect(), h = 0, i = 0, j = 0, k = 0, l = z("mask");
        if (g.width > 0 && g.height > 0) {
            q(a), a = z("changeParent") ? a.parentNode : a, d = D.getImageData(g.left, g.top, g.width, g.height).data;
            for (var m = 0; m < d.length; m += 4)d[m] === l.r && d[m + 1] === l.g && d[m + 2] === l.b ? k++ : (h++, b = .2126 * d[m] + .7152 * d[m + 1] + .0722 * d[m + 2], e = b - j, i += e * e, j += e / h);
            k <= d.length / 4 * (1 - z("minOverlap") / 100) && (f = Math.sqrt(i / h) / 255, j /= 255, c("Target: " + a.className + " lum: " + j + " var: " + f), p(a, j <= z("threshold") / 100 ? z("classes").dark : z("classes").light, "add"), f > z("minComplexity") / 100 && p(a, z("classes").complex, "add"))
        }
    }

    function s(a, b) {
        return a = (a.nodeType ? a : a.el).getBoundingClientRect(), b = b === F ? b : (b.nodeType ? b : b.el).getBoundingClientRect(), !(a.right < b.left || a.left > b.right || a.top > b.bottom || a.bottom < b.top)
    }

    function t(a) {
        for (var b, c = (new Date).getTime(), d = a && ("IMG" === a.tagName || a.img) ? "image" : "targets", e = a ? !1 : !0, f = z("targets").length, g = 0; f > g; g++)b = z("targets")[g], s(b, F) && ("targets" !== d || a && a !== b ? "image" === d && s(b, a) && r(b) : (e = !0, r(b)));
        if ("targets" === d && !e)throw a + " is not a target";
        j(c)
    }

    function u(a) {
        var b = function (a) {
            var b = 0;
            return "static" !== window.getComputedStyle(a).position && (b = parseInt(window.getComputedStyle(a).zIndex, 10) || 0, b >= 0 && b++), b
        }, c = a.parentNode, d = c ? b(c) : 0, e = b(a);
        return 1e5 * d + e
    }

    function v(a) {
        var b = !1;
        return a.sort(function (a, c) {
            a = a.nodeType ? a : a.el, c = c.nodeType ? c : c.el;
            var d = a.compareDocumentPosition(c), e = 0;
            return a = u(a), c = u(c), a > c && (b = !0), a === c && 2 === d ? e = 1 : a === c && 4 === d && (e = -1), e || a - c
        }), c("Sorted: " + b), b && c(a), b
    }

    function w(a, b, d) {
        if (B) {
            var e = z("mask");
            c("--- BackgroundCheck ---"), c("onLoad event: " + (d && d.src)), b !== !0 && (D.clearRect(0, 0, C.width, C.height), D.fillStyle = "rgb(" + e.r + ", " + e.g + ", " + e.b + ")", D.fillRect(0, 0, C.width, C.height));
            for (var f, g, h = d ? [d] : z("image"), i = v(h), j = !1, k = 0; k < h.length; k++)f = h[k], s(f, F) && (g = f.nodeType ? f : f.img, 0 === g.naturalWidth ? (j = !0, c("Loading... " + f.src), g.removeEventListener("load", w), i ? g.addEventListener("load", w.bind(null, null, !1, null)) : g.addEventListener("load", w.bind(null, a, !0, f))) : (c("Drawing: " + f.src), o(f)));
            d || j ? d && t(d) : t(a)
        }
    }

    function x(a) {
        z("windowEvents") === !0 && (E && clearTimeout(E), E = setTimeout(a, 200))
    }

    function y(a, b) {
        if (void 0 === H[a])throw"Unknown property - " + a;
        if (void 0 === b)throw"Missing value for " + a;
        if ("targets" === a || "image" === a)try {
            b = g("image" !== a || b ? b : "img", "image" === a ? !0 : !1)
        } catch (c) {
            throw b = [], c
        } else e(b, typeof H[a]);
        q(), H[a] = b, w(), "debugOverlay" === a && i()
    }

    function z(a) {
        if (void 0 === H[a])throw"Unknown property - " + a;
        return H[a]
    }

    function A() {
        for (var a, b = z("image"), c = [], d = 0; d < b.length; d++)a = n(b[d]), c.push(a);
        return c
    }

    var B, C, D, E, F, G = void 0 !== window.orientation ? "orientationchange" : "resize", H = {};
    return {init: a, destroy: b, refresh: w, set: y, get: z, getImageData: A}
}), function (a) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], a) : a(jQuery)
}(function (a) {
    "use strict";
    function b(a) {
        if (a instanceof Date)return a;
        if (String(a).match(g))return String(a).match(/^[0-9]*$/) && (a = Number(a)), String(a).match(/\-/) && (a = String(a).replace(/\-/g, "/")), new Date(a);
        throw new Error("Couldn't cast `" + a + "` to a date object.")
    }

    function c(a) {
        return function (b) {
            var c = b.match(/%(-|!)?[A-Z]{1}(:[^;]+;)?/gi);
            if (c)for (var e = 0, f = c.length; f > e; ++e) {
                var g = c[e].match(/%(-|!)?([a-zA-Z]{1})(:[^;]+;)?/), i = new RegExp(g[0]), j = g[1] || "", k = g[3] || "", l = null;
                g = g[2], h.hasOwnProperty(g) && (l = h[g], l = Number(a[l])), null !== l && ("!" === j && (l = d(k, l)), "" === j && 10 > l && (l = "0" + l.toString()), b = b.replace(i, l.toString()))
            }
            return b = b.replace(/%%/, "%")
        }
    }

    function d(a, b) {
        var c = "s", d = "";
        return a && (a = a.replace(/(:|;|\s)/gi, "").split(/\,/), 1 === a.length ? c = a[0] : (d = a[0], c = a[1])), 1 === Math.abs(b) ? d : c
    }

    var e = 100, f = [], g = [];
    g.push(/^[0-9]*$/.source), g.push(/([0-9]{1,2}\/){2}[0-9]{4}( [0-9]{1,2}(:[0-9]{2}){2})?/.source), g.push(/[0-9]{4}([\/\-][0-9]{1,2}){2}( [0-9]{1,2}(:[0-9]{2}){2})?/.source), g = new RegExp(g.join("|"));
    var h = {
        Y: "years",
        m: "months",
        w: "weeks",
        d: "days",
        D: "totalDays",
        H: "hours",
        M: "minutes",
        S: "seconds"
    }, i = function (b, c, d) {
        this.el = b, this.$el = a(b), this.interval = null, this.offset = {}, this.instanceNumber = f.length, f.push(this), this.$el.data("countdown-instance", this.instanceNumber), d && (this.$el.on("update.countdown", d), this.$el.on("stoped.countdown", d), this.$el.on("finish.countdown", d)), this.setFinalDate(c), this.start()
    };
    a.extend(i.prototype, {
        start: function () {
            null !== this.interval && clearInterval(this.interval);
            var a = this;
            this.update(), this.interval = setInterval(function () {
                a.update.call(a)
            }, e)
        }, stop: function () {
            clearInterval(this.interval), this.interval = null, this.dispatchEvent("stoped")
        }, pause: function () {
            this.stop.call(this)
        }, resume: function () {
            this.start.call(this)
        }, remove: function () {
            this.stop(), f[this.instanceNumber] = null, delete this.$el.data().countdownInstance
        }, setFinalDate: function (a) {
            this.finalDate = b(a)
        }, update: function () {
            return 0 === this.$el.closest("html").length ? void this.remove() : (this.totalSecsLeft = this.finalDate.getTime() - (new Date).getTime(), this.totalSecsLeft = Math.ceil(this.totalSecsLeft / 1e3), this.totalSecsLeft = this.totalSecsLeft < 0 ? 0 : this.totalSecsLeft, this.offset = {
                seconds: this.totalSecsLeft % 60,
                minutes: Math.floor(this.totalSecsLeft / 60) % 60,
                hours: Math.floor(this.totalSecsLeft / 60 / 60) % 24,
                days: Math.floor(this.totalSecsLeft / 60 / 60 / 24) % 7,
                totalDays: Math.floor(this.totalSecsLeft / 60 / 60 / 24),
                weeks: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 7),
                months: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 30),
                years: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 365)
            }, void(0 === this.totalSecsLeft ? (this.stop(), this.dispatchEvent("finish")) : this.dispatchEvent("update")))
        }, dispatchEvent: function (b) {
            var d = a.Event(b + ".countdown");
            d.finalDate = this.finalDate, d.offset = a.extend({}, this.offset), d.strftime = c(this.offset), this.$el.trigger(d)
        }
    }), a.fn.countdown = function () {
        var b = Array.prototype.slice.call(arguments, 0);
        return this.each(function () {
            var c = a(this).data("countdown-instance");
            if (void 0 !== c) {
                var d = f[c], e = b[0];
                i.prototype.hasOwnProperty(e) ? d[e].apply(d, b.slice(1)) : null === String(e).match(/^[$A-Z_][0-9A-Z_$]*$/i) ? (d.setFinalDate.call(d, e), d.start()) : a.error("Method %s does not exist on jQuery.countdown".replace(/\%s/gi, e))
            } else new i(this, b[0], b[1])
        })
    }
}), !function (a) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], a) : a(jQuery)
}(function (a) {
    "use strict";
    function b(a) {
        if (a instanceof Date)return a;
        if (String(a).match(g))return String(a).match(/^[0-9]*$/) && (a = Number(a)), String(a).match(/\-/) && (a = String(a).replace(/\-/g, "/")), new Date(a);
        throw new Error("Couldn't cast `" + a + "` to a date object.")
    }

    function c(a) {
        return function (b) {
            var c = b.match(/%(-|!)?[A-Z]{1}(:[^;]+;)?/gi);
            if (c)for (var e = 0, f = c.length; f > e; ++e) {
                var g = c[e].match(/%(-|!)?([a-zA-Z]{1})(:[^;]+;)?/), i = new RegExp(g[0]), j = g[1] || "", k = g[3] || "", l = null;
                g = g[2], h.hasOwnProperty(g) && (l = h[g], l = Number(a[l])), null !== l && ("!" === j && (l = d(k, l)), "" === j && 10 > l && (l = "0" + l.toString()), b = b.replace(i, l.toString()))
            }
            return b = b.replace(/%%/, "%")
        }
    }

    function d(a, b) {
        var c = "s", d = "";
        return a && (a = a.replace(/(:|;|\s)/gi, "").split(/\,/), 1 === a.length ? c = a[0] : (d = a[0], c = a[1])), 1 === Math.abs(b) ? d : c
    }

    var e = 100, f = [], g = [];
    g.push(/^[0-9]*$/.source), g.push(/([0-9]{1,2}\/){2}[0-9]{4}( [0-9]{1,2}(:[0-9]{2}){2})?/.source), g.push(/[0-9]{4}([\/\-][0-9]{1,2}){2}( [0-9]{1,2}(:[0-9]{2}){2})?/.source), g = new RegExp(g.join("|"));
    var h = {
        Y: "years",
        m: "months",
        w: "weeks",
        d: "days",
        D: "totalDays",
        H: "hours",
        M: "minutes",
        S: "seconds"
    }, i = function (b, c, d) {
        this.el = b, this.$el = a(b), this.interval = null, this.offset = {}, this.instanceNumber = f.length, f.push(this), this.$el.data("countdown-instance", this.instanceNumber), d && (this.$el.on("update.countdown", d), this.$el.on("stoped.countdown", d), this.$el.on("finish.countdown", d)), this.setFinalDate(c), this.start()
    };
    a.extend(i.prototype, {
        start: function () {
            null !== this.interval && clearInterval(this.interval);
            var a = this;
            this.update(), this.interval = setInterval(function () {
                a.update.call(a)
            }, e)
        }, stop: function () {
            clearInterval(this.interval), this.interval = null, this.dispatchEvent("stoped")
        }, pause: function () {
            this.stop.call(this)
        }, resume: function () {
            this.start.call(this)
        }, remove: function () {
            this.stop(), f[this.instanceNumber] = null, delete this.$el.data().countdownInstance
        }, setFinalDate: function (a) {
            this.finalDate = b(a)
        }, update: function () {
            return 0 === this.$el.closest("html").length ? void this.remove() : (this.totalSecsLeft = this.finalDate.getTime() - (new Date).getTime(), this.totalSecsLeft = Math.ceil(this.totalSecsLeft / 1e3), this.totalSecsLeft = this.totalSecsLeft < 0 ? 0 : this.totalSecsLeft, this.offset = {
                seconds: this.totalSecsLeft % 60,
                minutes: Math.floor(this.totalSecsLeft / 60) % 60,
                hours: Math.floor(this.totalSecsLeft / 60 / 60) % 24,
                days: Math.floor(this.totalSecsLeft / 60 / 60 / 24) % 7,
                totalDays: Math.floor(this.totalSecsLeft / 60 / 60 / 24),
                weeks: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 7),
                months: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 30),
                years: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 365)
            }, void(0 === this.totalSecsLeft ? (this.stop(), this.dispatchEvent("finish")) : this.dispatchEvent("update")))
        }, dispatchEvent: function (b) {
            var d = a.Event(b + ".countdown");
            d.finalDate = this.finalDate, d.offset = a.extend({}, this.offset), d.strftime = c(this.offset), this.$el.trigger(d)
        }
    }), a.fn.countdown = function () {
        var b = Array.prototype.slice.call(arguments, 0);
        return this.each(function () {
            var c = a(this).data("countdown-instance");
            if (void 0 !== c) {
                var d = f[c], e = b[0];
                i.prototype.hasOwnProperty(e) ? d[e].apply(d, b.slice(1)) : null === String(e).match(/^[$A-Z_][0-9A-Z_$]*$/i) ? (d.setFinalDate.call(d, e), d.start()) : a.error("Method %s does not exist on jQuery.countdown".replace(/\%s/gi, e))
            } else new i(this, b[0], b[1])
        })
    }
}), function (a) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : a(jQuery)
}(function (a) {
    function b(b, d) {
        var e, f, g, h = b.nodeName.toLowerCase();
        return "area" === h ? (e = b.parentNode, f = e.name, b.href && f && "map" === e.nodeName.toLowerCase() ? (g = a("img[usemap='#" + f + "']")[0], !!g && c(g)) : !1) : (/input|select|textarea|button|object/.test(h) ? !b.disabled : "a" === h ? b.href || d : d) && c(b)
    }

    function c(b) {
        return a.expr.filters.visible(b) && !a(b).parents().addBack().filter(function () {
            return "hidden" === a.css(this, "visibility")
        }).length
    }

    a.ui = a.ui || {}, a.extend(a.ui, {
        version: "1.11.2",
        keyCode: {
            BACKSPACE: 8,
            COMMA: 188,
            DELETE: 46,
            DOWN: 40,
            END: 35,
            ENTER: 13,
            ESCAPE: 27,
            HOME: 36,
            LEFT: 37,
            PAGE_DOWN: 34,
            PAGE_UP: 33,
            PERIOD: 190,
            RIGHT: 39,
            SPACE: 32,
            TAB: 9,
            UP: 38
        }
    }), a.fn.extend({
        scrollParent: function (b) {
            var c = this.css("position"), d = "absolute" === c, e = b ? /(auto|scroll|hidden)/ : /(auto|scroll)/, f = this.parents().filter(function () {
                var b = a(this);
                return d && "static" === b.css("position") ? !1 : e.test(b.css("overflow") + b.css("overflow-y") + b.css("overflow-x"))
            }).eq(0);
            return "fixed" !== c && f.length ? f : a(this[0].ownerDocument || document)
        }, uniqueId: function () {
            var a = 0;
            return function () {
                return this.each(function () {
                    this.id || (this.id = "ui-id-" + ++a)
                })
            }
        }(), removeUniqueId: function () {
            return this.each(function () {
                /^ui-id-\d+$/.test(this.id) && a(this).removeAttr("id")
            })
        }
    }), a.extend(a.expr[":"], {
        data: a.expr.createPseudo ? a.expr.createPseudo(function (b) {
            return function (c) {
                return !!a.data(c, b)
            }
        }) : function (b, c, d) {
            return !!a.data(b, d[3])
        }, focusable: function (c) {
            return b(c, !isNaN(a.attr(c, "tabindex")))
        }, tabbable: function (c) {
            var d = a.attr(c, "tabindex"), e = isNaN(d);
            return (e || d >= 0) && b(c, !e)
        }
    }), a("<a>").outerWidth(1).jquery || a.each(["Width", "Height"], function (b, c) {
        function d(b, c, d, f) {
            return a.each(e, function () {
                c -= parseFloat(a.css(b, "padding" + this)) || 0, d && (c -= parseFloat(a.css(b, "border" + this + "Width")) || 0), f && (c -= parseFloat(a.css(b, "margin" + this)) || 0)
            }), c
        }

        var e = "Width" === c ? ["Left", "Right"] : ["Top", "Bottom"], f = c.toLowerCase(), g = {
            innerWidth: a.fn.innerWidth,
            innerHeight: a.fn.innerHeight,
            outerWidth: a.fn.outerWidth,
            outerHeight: a.fn.outerHeight
        };
        a.fn["inner" + c] = function (b) {
            return void 0 === b ? g["inner" + c].call(this) : this.each(function () {
                a(this).css(f, d(this, b) + "px")
            })
        }, a.fn["outer" + c] = function (b, e) {
            return "number" != typeof b ? g["outer" + c].call(this, b) : this.each(function () {
                a(this).css(f, d(this, b, !0, e) + "px")
            })
        }
    }), a.fn.addBack || (a.fn.addBack = function (a) {
        return this.add(null == a ? this.prevObject : this.prevObject.filter(a))
    }), a("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (a.fn.removeData = function (b) {
        return function (c) {
            return arguments.length ? b.call(this, a.camelCase(c)) : b.call(this)
        }
    }(a.fn.removeData)), a.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()), a.fn.extend({
        focus: function (b) {
            return function (c, d) {
                return "number" == typeof c ? this.each(function () {
                    var b = this;
                    setTimeout(function () {
                        a(b).focus(), d && d.call(b)
                    }, c)
                }) : b.apply(this, arguments)
            }
        }(a.fn.focus), disableSelection: function () {
            var a = "onselectstart"in document.createElement("div") ? "selectstart" : "mousedown";
            return function () {
                return this.bind(a + ".ui-disableSelection", function (a) {
                    a.preventDefault()
                })
            }
        }(), enableSelection: function () {
            return this.unbind(".ui-disableSelection")
        }, zIndex: function (b) {
            if (void 0 !== b)return this.css("zIndex", b);
            if (this.length)for (var c, d, e = a(this[0]); e.length && e[0] !== document;) {
                if (c = e.css("position"), ("absolute" === c || "relative" === c || "fixed" === c) && (d = parseInt(e.css("zIndex"), 10), !isNaN(d) && 0 !== d))return d;
                e = e.parent()
            }
            return 0
        }
    }), a.ui.plugin = {
        add: function (b, c, d) {
            var e, f = a.ui[b].prototype;
            for (e in d)f.plugins[e] = f.plugins[e] || [], f.plugins[e].push([c, d[e]])
        }, call: function (a, b, c, d) {
            var e, f = a.plugins[b];
            if (f && (d || a.element[0].parentNode && 11 !== a.element[0].parentNode.nodeType))for (e = 0; f.length > e; e++)a.options[f[e][0]] && f[e][1].apply(a.element, c)
        }
    }
}), function (a) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : a(jQuery)
}(function (a) {
    var b = 0, c = Array.prototype.slice;
    return a.cleanData = function (b) {
        return function (c) {
            var d, e, f;
            for (f = 0; null != (e = c[f]); f++)try {
                d = a._data(e, "events"), d && d.remove && a(e).triggerHandler("remove")
            } catch (g) {
            }
            b(c)
        }
    }(a.cleanData), a.widget = function (b, c, d) {
        var e, f, g, h, i = {}, j = b.split(".")[0];
        return b = b.split(".")[1], e = j + "-" + b, d || (d = c, c = a.Widget), a.expr[":"][e.toLowerCase()] = function (b) {
            return !!a.data(b, e)
        }, a[j] = a[j] || {}, f = a[j][b], g = a[j][b] = function (a, b) {
            return this._createWidget ? void(arguments.length && this._createWidget(a, b)) : new g(a, b)
        }, a.extend(g, f, {
            version: d.version,
            _proto: a.extend({}, d),
            _childConstructors: []
        }), h = new c, h.options = a.widget.extend({}, h.options), a.each(d, function (b, d) {
            return a.isFunction(d) ? void(i[b] = function () {
                var a = function () {
                    return c.prototype[b].apply(this, arguments)
                }, e = function (a) {
                    return c.prototype[b].apply(this, a)
                };
                return function () {
                    var b, c = this._super, f = this._superApply;
                    return this._super = a, this._superApply = e, b = d.apply(this, arguments), this._super = c, this._superApply = f, b
                }
            }()) : void(i[b] = d)
        }), g.prototype = a.widget.extend(h, {widgetEventPrefix: f ? h.widgetEventPrefix || b : b}, i, {
            constructor: g,
            namespace: j,
            widgetName: b,
            widgetFullName: e
        }), f ? (a.each(f._childConstructors, function (b, c) {
            var d = c.prototype;
            a.widget(d.namespace + "." + d.widgetName, g, c._proto)
        }), delete f._childConstructors) : c._childConstructors.push(g), a.widget.bridge(b, g), g
    }, a.widget.extend = function (b) {
        for (var d, e, f = c.call(arguments, 1), g = 0, h = f.length; h > g; g++)for (d in f[g])e = f[g][d], f[g].hasOwnProperty(d) && void 0 !== e && (b[d] = a.isPlainObject(e) ? a.isPlainObject(b[d]) ? a.widget.extend({}, b[d], e) : a.widget.extend({}, e) : e);
        return b
    }, a.widget.bridge = function (b, d) {
        var e = d.prototype.widgetFullName || b;
        a.fn[b] = function (f) {
            var g = "string" == typeof f, h = c.call(arguments, 1), i = this;
            return f = !g && h.length ? a.widget.extend.apply(null, [f].concat(h)) : f, this.each(g ? function () {
                var c, d = a.data(this, e);
                return "instance" === f ? (i = d, !1) : d ? a.isFunction(d[f]) && "_" !== f.charAt(0) ? (c = d[f].apply(d, h), c !== d && void 0 !== c ? (i = c && c.jquery ? i.pushStack(c.get()) : c, !1) : void 0) : a.error("no such method '" + f + "' for " + b + " widget instance") : a.error("cannot call methods on " + b + " prior to initialization; attempted to call method '" + f + "'")
            } : function () {
                var b = a.data(this, e);
                b ? (b.option(f || {}), b._init && b._init()) : a.data(this, e, new d(f, this))
            }), i
        }
    }, a.Widget = function () {
    }, a.Widget._childConstructors = [], a.Widget.prototype = {
        widgetName: "widget",
        widgetEventPrefix: "",
        defaultElement: "<div>",
        options: {disabled: !1, create: null},
        _createWidget: function (c, d) {
            d = a(d || this.defaultElement || this)[0], this.element = a(d), this.uuid = b++, this.eventNamespace = "." + this.widgetName + this.uuid, this.bindings = a(), this.hoverable = a(), this.focusable = a(), d !== this && (a.data(d, this.widgetFullName, this), this._on(!0, this.element, {
                remove: function (a) {
                    a.target === d && this.destroy()
                }
            }), this.document = a(d.style ? d.ownerDocument : d.document || d), this.window = a(this.document[0].defaultView || this.document[0].parentWindow)), this.options = a.widget.extend({}, this.options, this._getCreateOptions(), c), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init()
        },
        _getCreateOptions: a.noop,
        _getCreateEventData: a.noop,
        _create: a.noop,
        _init: a.noop,
        destroy: function () {
            this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(a.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")
        },
        _destroy: a.noop,
        widget: function () {
            return this.element
        },
        option: function (b, c) {
            var d, e, f, g = b;
            if (0 === arguments.length)return a.widget.extend({}, this.options);
            if ("string" == typeof b)if (g = {}, d = b.split("."), b = d.shift(), d.length) {
                for (e = g[b] = a.widget.extend({}, this.options[b]), f = 0; d.length - 1 > f; f++)e[d[f]] = e[d[f]] || {}, e = e[d[f]];
                if (b = d.pop(), 1 === arguments.length)return void 0 === e[b] ? null : e[b];
                e[b] = c
            } else {
                if (1 === arguments.length)return void 0 === this.options[b] ? null : this.options[b];
                g[b] = c
            }
            return this._setOptions(g), this
        },
        _setOptions: function (a) {
            var b;
            for (b in a)this._setOption(b, a[b]);
            return this
        },
        _setOption: function (a, b) {
            return this.options[a] = b, "disabled" === a && (this.widget().toggleClass(this.widgetFullName + "-disabled", !!b), b && (this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus"))), this
        },
        enable: function () {
            return this._setOptions({disabled: !1})
        },
        disable: function () {
            return this._setOptions({disabled: !0})
        },
        _on: function (b, c, d) {
            var e, f = this;
            "boolean" != typeof b && (d = c, c = b, b = !1), d ? (c = e = a(c), this.bindings = this.bindings.add(c)) : (d = c, c = this.element, e = this.widget()), a.each(d, function (d, g) {
                function h() {
                    return b || f.options.disabled !== !0 && !a(this).hasClass("ui-state-disabled") ? ("string" == typeof g ? f[g] : g).apply(f, arguments) : void 0
                }

                "string" != typeof g && (h.guid = g.guid = g.guid || h.guid || a.guid++);
                var i = d.match(/^([\w:-]*)\s*(.*)$/), j = i[1] + f.eventNamespace, k = i[2];
                k ? e.delegate(k, j, h) : c.bind(j, h)
            })
        },
        _off: function (b, c) {
            c = (c || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, b.unbind(c).undelegate(c), this.bindings = a(this.bindings.not(b).get()), this.focusable = a(this.focusable.not(b).get()), this.hoverable = a(this.hoverable.not(b).get())
        },
        _delay: function (a, b) {
            function c() {
                return ("string" == typeof a ? d[a] : a).apply(d, arguments)
            }

            var d = this;
            return setTimeout(c, b || 0)
        },
        _hoverable: function (b) {
            this.hoverable = this.hoverable.add(b), this._on(b, {
                mouseenter: function (b) {
                    a(b.currentTarget).addClass("ui-state-hover")
                }, mouseleave: function (b) {
                    a(b.currentTarget).removeClass("ui-state-hover")
                }
            })
        },
        _focusable: function (b) {
            this.focusable = this.focusable.add(b), this._on(b, {
                focusin: function (b) {
                    a(b.currentTarget).addClass("ui-state-focus")
                }, focusout: function (b) {
                    a(b.currentTarget).removeClass("ui-state-focus")
                }
            })
        },
        _trigger: function (b, c, d) {
            var e, f, g = this.options[b];
            if (d = d || {}, c = a.Event(c), c.type = (b === this.widgetEventPrefix ? b : this.widgetEventPrefix + b).toLowerCase(), c.target = this.element[0], f = c.originalEvent)for (e in f)e in c || (c[e] = f[e]);
            return this.element.trigger(c, d), !(a.isFunction(g) && g.apply(this.element[0], [c].concat(d)) === !1 || c.isDefaultPrevented())
        }
    }, a.each({show: "fadeIn", hide: "fadeOut"}, function (b, c) {
        a.Widget.prototype["_" + b] = function (d, e, f) {
            "string" == typeof e && (e = {effect: e});
            var g, h = e ? e === !0 || "number" == typeof e ? c : e.effect || c : b;
            e = e || {}, "number" == typeof e && (e = {duration: e}), g = !a.isEmptyObject(e), e.complete = f, e.delay && d.delay(e.delay), g && a.effects && a.effects.effect[h] ? d[b](e) : h !== b && d[h] ? d[h](e.duration, e.easing, f) : d.queue(function (c) {
                a(this)[b](), f && f.call(d[0]), c()
            })
        }
    }), a.widget
}), function (a) {
    "function" == typeof define && define.amd ? define(["jquery", "/widget"], a) : a(jQuery)
}(function (a) {
    var b = !1;
    return a(document).mouseup(function () {
        b = !1
    }), a.widget("ui.mouse", {
        version: "1.11.2",
        options: {cancel: "input,textarea,button,select,option", distance: 1, delay: 0},
        _mouseInit: function () {
            var b = this;
            this.element.bind("mousedown." + this.widgetName, function (a) {
                return b._mouseDown(a)
            }).bind("click." + this.widgetName, function (c) {
                return !0 === a.data(c.target, b.widgetName + ".preventClickEvent") ? (a.removeData(c.target, b.widgetName + ".preventClickEvent"), c.stopImmediatePropagation(), !1) : void 0
            }), this.started = !1
        },
        _mouseDestroy: function () {
            this.element.unbind("." + this.widgetName), this._mouseMoveDelegate && this.document.unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate)
        },
        _mouseDown: function (c) {
            if (!b) {
                this._mouseMoved = !1, this._mouseStarted && this._mouseUp(c), this._mouseDownEvent = c;
                var d = this, e = 1 === c.which, f = "string" == typeof this.options.cancel && c.target.nodeName ? a(c.target).closest(this.options.cancel).length : !1;
                return e && !f && this._mouseCapture(c) ? (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function () {
                    d.mouseDelayMet = !0
                }, this.options.delay)), this._mouseDistanceMet(c) && this._mouseDelayMet(c) && (this._mouseStarted = this._mouseStart(c) !== !1, !this._mouseStarted) ? (c.preventDefault(), !0) : (!0 === a.data(c.target, this.widgetName + ".preventClickEvent") && a.removeData(c.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function (a) {
                    return d._mouseMove(a)
                }, this._mouseUpDelegate = function (a) {
                    return d._mouseUp(a)
                }, this.document.bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate), c.preventDefault(), b = !0, !0)) : !0
            }
        },
        _mouseMove: function (b) {
            if (this._mouseMoved) {
                if (a.ui.ie && (!document.documentMode || 9 > document.documentMode) && !b.button)return this._mouseUp(b);
                if (!b.which)return this._mouseUp(b)
            }
            return (b.which || b.button) && (this._mouseMoved = !0), this._mouseStarted ? (this._mouseDrag(b), b.preventDefault()) : (this._mouseDistanceMet(b) && this._mouseDelayMet(b) && (this._mouseStarted = this._mouseStart(this._mouseDownEvent, b) !== !1, this._mouseStarted ? this._mouseDrag(b) : this._mouseUp(b)), !this._mouseStarted)
        },
        _mouseUp: function (c) {
            return this.document.unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, c.target === this._mouseDownEvent.target && a.data(c.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(c)), b = !1, !1
        },
        _mouseDistanceMet: function (a) {
            return Math.max(Math.abs(this._mouseDownEvent.pageX - a.pageX), Math.abs(this._mouseDownEvent.pageY - a.pageY)) >= this.options.distance
        },
        _mouseDelayMet: function () {
            return this.mouseDelayMet
        },
        _mouseStart: function () {
        },
        _mouseDrag: function () {
        },
        _mouseStop: function () {
        },
        _mouseCapture: function () {
            return !0
        }
    })
}), function (a) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : a(jQuery)
}(function (a) {
    return function () {
        function b(a, b, c) {
            return [parseFloat(a[0]) * (n.test(a[0]) ? b / 100 : 1), parseFloat(a[1]) * (n.test(a[1]) ? c / 100 : 1)]
        }

        function c(b, c) {
            return parseInt(a.css(b, c), 10) || 0
        }

        function d(b) {
            var c = b[0];
            return 9 === c.nodeType ? {
                width: b.width(),
                height: b.height(),
                offset: {top: 0, left: 0}
            } : a.isWindow(c) ? {
                width: b.width(),
                height: b.height(),
                offset: {top: b.scrollTop(), left: b.scrollLeft()}
            } : c.preventDefault ? {
                width: 0,
                height: 0,
                offset: {top: c.pageY, left: c.pageX}
            } : {width: b.outerWidth(), height: b.outerHeight(), offset: b.offset()}
        }

        a.ui = a.ui || {};
        var e, f, g = Math.max, h = Math.abs, i = Math.round, j = /left|center|right/, k = /top|center|bottom/, l = /[\+\-]\d+(\.[\d]+)?%?/, m = /^\w+/, n = /%$/, o = a.fn.position;
        a.position = {
            scrollbarWidth: function () {
                if (void 0 !== e)return e;
                var b, c, d = a("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"), f = d.children()[0];
                return a("body").append(d), b = f.offsetWidth, d.css("overflow", "scroll"), c = f.offsetWidth, b === c && (c = d[0].clientWidth), d.remove(), e = b - c
            }, getScrollInfo: function (b) {
                var c = b.isWindow || b.isDocument ? "" : b.element.css("overflow-x"), d = b.isWindow || b.isDocument ? "" : b.element.css("overflow-y"), e = "scroll" === c || "auto" === c && b.width < b.element[0].scrollWidth, f = "scroll" === d || "auto" === d && b.height < b.element[0].scrollHeight;
                return {width: f ? a.position.scrollbarWidth() : 0, height: e ? a.position.scrollbarWidth() : 0}
            }, getWithinInfo: function (b) {
                var c = a(b || window), d = a.isWindow(c[0]), e = !!c[0] && 9 === c[0].nodeType;
                return {
                    element: c,
                    isWindow: d,
                    isDocument: e,
                    offset: c.offset() || {left: 0, top: 0},
                    scrollLeft: c.scrollLeft(),
                    scrollTop: c.scrollTop(),
                    width: d || e ? c.width() : c.outerWidth(),
                    height: d || e ? c.height() : c.outerHeight()
                }
            }
        }, a.fn.position = function (e) {
            if (!e || !e.of)return o.apply(this, arguments);
            e = a.extend({}, e);
            var n, p, q, r, s, t, u = a(e.of), v = a.position.getWithinInfo(e.within), w = a.position.getScrollInfo(v), x = (e.collision || "flip").split(" "), y = {};
            return t = d(u), u[0].preventDefault && (e.at = "left top"), p = t.width, q = t.height, r = t.offset, s = a.extend({}, r), a.each(["my", "at"], function () {
                var a, b, c = (e[this] || "").split(" ");
                1 === c.length && (c = j.test(c[0]) ? c.concat(["center"]) : k.test(c[0]) ? ["center"].concat(c) : ["center", "center"]), c[0] = j.test(c[0]) ? c[0] : "center", c[1] = k.test(c[1]) ? c[1] : "center", a = l.exec(c[0]), b = l.exec(c[1]), y[this] = [a ? a[0] : 0, b ? b[0] : 0], e[this] = [m.exec(c[0])[0], m.exec(c[1])[0]]
            }), 1 === x.length && (x[1] = x[0]), "right" === e.at[0] ? s.left += p : "center" === e.at[0] && (s.left += p / 2), "bottom" === e.at[1] ? s.top += q : "center" === e.at[1] && (s.top += q / 2), n = b(y.at, p, q), s.left += n[0], s.top += n[1], this.each(function () {
                var d, j, k = a(this), l = k.outerWidth(), m = k.outerHeight(), o = c(this, "marginLeft"), t = c(this, "marginTop"), z = l + o + c(this, "marginRight") + w.width, A = m + t + c(this, "marginBottom") + w.height, B = a.extend({}, s), C = b(y.my, k.outerWidth(), k.outerHeight());
                "right" === e.my[0] ? B.left -= l : "center" === e.my[0] && (B.left -= l / 2), "bottom" === e.my[1] ? B.top -= m : "center" === e.my[1] && (B.top -= m / 2), B.left += C[0], B.top += C[1], f || (B.left = i(B.left), B.top = i(B.top)), d = {
                    marginLeft: o,
                    marginTop: t
                }, a.each(["left", "top"], function (b, c) {
                    a.ui.position[x[b]] && a.ui.position[x[b]][c](B, {
                        targetWidth: p,
                        targetHeight: q,
                        elemWidth: l,
                        elemHeight: m,
                        collisionPosition: d,
                        collisionWidth: z,
                        collisionHeight: A,
                        offset: [n[0] + C[0], n[1] + C[1]],
                        my: e.my,
                        at: e.at,
                        within: v,
                        elem: k
                    })
                }), e.using && (j = function (a) {
                    var b = r.left - B.left, c = b + p - l, d = r.top - B.top, f = d + q - m, i = {
                        target: {
                            element: u,
                            left: r.left,
                            top: r.top,
                            width: p,
                            height: q
                        },
                        element: {element: k, left: B.left, top: B.top, width: l, height: m},
                        horizontal: 0 > c ? "left" : b > 0 ? "right" : "center",
                        vertical: 0 > f ? "top" : d > 0 ? "bottom" : "middle"
                    };
                    l > p && p > h(b + c) && (i.horizontal = "center"), m > q && q > h(d + f) && (i.vertical = "middle"), i.important = g(h(b), h(c)) > g(h(d), h(f)) ? "horizontal" : "vertical", e.using.call(this, a, i)
                }), k.offset(a.extend(B, {using: j}))
            })
        }, a.ui.position = {
            fit: {
                left: function (a, b) {
                    var c, d = b.within, e = d.isWindow ? d.scrollLeft : d.offset.left, f = d.width, h = a.left - b.collisionPosition.marginLeft, i = e - h, j = h + b.collisionWidth - f - e;
                    b.collisionWidth > f ? i > 0 && 0 >= j ? (c = a.left + i + b.collisionWidth - f - e, a.left += i - c) : a.left = j > 0 && 0 >= i ? e : i > j ? e + f - b.collisionWidth : e : i > 0 ? a.left += i : j > 0 ? a.left -= j : a.left = g(a.left - h, a.left)
                }, top: function (a, b) {
                    var c, d = b.within, e = d.isWindow ? d.scrollTop : d.offset.top, f = b.within.height, h = a.top - b.collisionPosition.marginTop, i = e - h, j = h + b.collisionHeight - f - e;
                    b.collisionHeight > f ? i > 0 && 0 >= j ? (c = a.top + i + b.collisionHeight - f - e, a.top += i - c) : a.top = j > 0 && 0 >= i ? e : i > j ? e + f - b.collisionHeight : e : i > 0 ? a.top += i : j > 0 ? a.top -= j : a.top = g(a.top - h, a.top)
                }
            }, flip: {
                left: function (a, b) {
                    var c, d, e = b.within, f = e.offset.left + e.scrollLeft, g = e.width, i = e.isWindow ? e.scrollLeft : e.offset.left, j = a.left - b.collisionPosition.marginLeft, k = j - i, l = j + b.collisionWidth - g - i, m = "left" === b.my[0] ? -b.elemWidth : "right" === b.my[0] ? b.elemWidth : 0, n = "left" === b.at[0] ? b.targetWidth : "right" === b.at[0] ? -b.targetWidth : 0, o = -2 * b.offset[0];
                    0 > k ? (c = a.left + m + n + o + b.collisionWidth - g - f, (0 > c || h(k) > c) && (a.left += m + n + o)) : l > 0 && (d = a.left - b.collisionPosition.marginLeft + m + n + o - i, (d > 0 || l > h(d)) && (a.left += m + n + o))
                }, top: function (a, b) {
                    var c, d, e = b.within, f = e.offset.top + e.scrollTop, g = e.height, i = e.isWindow ? e.scrollTop : e.offset.top, j = a.top - b.collisionPosition.marginTop, k = j - i, l = j + b.collisionHeight - g - i, m = "top" === b.my[1], n = m ? -b.elemHeight : "bottom" === b.my[1] ? b.elemHeight : 0, o = "top" === b.at[1] ? b.targetHeight : "bottom" === b.at[1] ? -b.targetHeight : 0, p = -2 * b.offset[1];
                    0 > k ? (d = a.top + n + o + p + b.collisionHeight - g - f, a.top + n + o + p > k && (0 > d || h(k) > d) && (a.top += n + o + p)) : l > 0 && (c = a.top - b.collisionPosition.marginTop + n + o + p - i, a.top + n + o + p > l && (c > 0 || l > h(c)) && (a.top += n + o + p))
                }
            }, flipfit: {
                left: function () {
                    a.ui.position.flip.left.apply(this, arguments), a.ui.position.fit.left.apply(this, arguments)
                }, top: function () {
                    a.ui.position.flip.top.apply(this, arguments), a.ui.position.fit.top.apply(this, arguments)
                }
            }
        }, function () {
            var b, c, d, e, g, h = document.getElementsByTagName("body")[0], i = document.createElement("div");
            b = document.createElement(h ? "div" : "body"), d = {
                visibility: "hidden",
                width: 0,
                height: 0,
                border: 0,
                margin: 0,
                background: "none"
            }, h && a.extend(d, {position: "absolute", left: "-1000px", top: "-1000px"});
            for (g in d)b.style[g] = d[g];
            b.appendChild(i), c = h || document.documentElement, c.insertBefore(b, c.firstChild), i.style.cssText = "position: absolute; left: 10.7432222px;", e = a(i).offset().left, f = e > 10 && 11 > e, b.innerHTML = "", c.removeChild(b)
        }()
    }(), a.ui.position
}), function (a) {
    "function" == typeof define && define.amd ? define(["jquery", "/core", "/mouse", "/widget"], a) : a(jQuery)
}(function (a) {
    return a.widget("ui.draggable", a.ui.mouse, {
        version: "1.11.2",
        widgetEventPrefix: "drag",
        options: {
            addClasses: !0,
            appendTo: "parent",
            axis: !1,
            connectToSortable: !1,
            containment: !1,
            cursor: "auto",
            cursorAt: !1,
            grid: !1,
            handle: !1,
            helper: "original",
            iframeFix: !1,
            opacity: !1,
            refreshPositions: !1,
            revert: !1,
            revertDuration: 500,
            scope: "default",
            scroll: !0,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            snap: !1,
            snapMode: "both",
            snapTolerance: 20,
            stack: !1,
            zIndex: !1,
            drag: null,
            start: null,
            stop: null
        },
        _create: function () {
            "original" === this.options.helper && this._setPositionRelative(), this.options.addClasses && this.element.addClass("ui-draggable"), this.options.disabled && this.element.addClass("ui-draggable-disabled"), this._setHandleClassName(), this._mouseInit()
        },
        _setOption: function (a, b) {
            this._super(a, b), "handle" === a && (this._removeHandleClassName(), this._setHandleClassName())
        },
        _destroy: function () {
            return (this.helper || this.element).is(".ui-draggable-dragging") ? void(this.destroyOnClear = !0) : (this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled"), this._removeHandleClassName(), void this._mouseDestroy())
        },
        _mouseCapture: function (b) {
            var c = this.options;
            return this._blurActiveElement(b), this.helper || c.disabled || a(b.target).closest(".ui-resizable-handle").length > 0 ? !1 : (this.handle = this._getHandle(b), this.handle ? (this._blockFrames(c.iframeFix === !0 ? "iframe" : c.iframeFix), !0) : !1)
        },
        _blockFrames: function (b) {
            this.iframeBlocks = this.document.find(b).map(function () {
                var b = a(this);
                return a("<div>").css("position", "absolute").appendTo(b.parent()).outerWidth(b.outerWidth()).outerHeight(b.outerHeight()).offset(b.offset())[0]
            })
        },
        _unblockFrames: function () {
            this.iframeBlocks && (this.iframeBlocks.remove(), delete this.iframeBlocks)
        },
        _blurActiveElement: function (b) {
            var c = this.document[0];
            if (this.handleElement.is(b.target))try {
                c.activeElement && "body" !== c.activeElement.nodeName.toLowerCase() && a(c.activeElement).blur()
            } catch (d) {
            }
        },
        _mouseStart: function (b) {
            var c = this.options;
            return this.helper = this._createHelper(b), this.helper.addClass("ui-draggable-dragging"), this._cacheHelperProportions(), a.ui.ddmanager && (a.ui.ddmanager.current = this), this._cacheMargins(), this.cssPosition = this.helper.css("position"), this.scrollParent = this.helper.scrollParent(!0), this.offsetParent = this.helper.offsetParent(), this.hasFixedAncestor = this.helper.parents().filter(function () {
                return "fixed" === a(this).css("position")
            }).length > 0, this.positionAbs = this.element.offset(), this._refreshOffsets(b), this.originalPosition = this.position = this._generatePosition(b, !1), this.originalPageX = b.pageX, this.originalPageY = b.pageY, c.cursorAt && this._adjustOffsetFromHelper(c.cursorAt), this._setContainment(), this._trigger("start", b) === !1 ? (this._clear(), !1) : (this._cacheHelperProportions(), a.ui.ddmanager && !c.dropBehaviour && a.ui.ddmanager.prepareOffsets(this, b), this._normalizeRightBottom(), this._mouseDrag(b, !0), a.ui.ddmanager && a.ui.ddmanager.dragStart(this, b), !0)
        },
        _refreshOffsets: function (a) {
            this.offset = {
                top: this.positionAbs.top - this.margins.top,
                left: this.positionAbs.left - this.margins.left,
                scroll: !1,
                parent: this._getParentOffset(),
                relative: this._getRelativeOffset()
            }, this.offset.click = {left: a.pageX - this.offset.left, top: a.pageY - this.offset.top}
        },
        _mouseDrag: function (b, c) {
            if (this.hasFixedAncestor && (this.offset.parent = this._getParentOffset()), this.position = this._generatePosition(b, !0), this.positionAbs = this._convertPositionTo("absolute"), !c) {
                var d = this._uiHash();
                if (this._trigger("drag", b, d) === !1)return this._mouseUp({}), !1;
                this.position = d.position
            }
            return this.helper[0].style.left = this.position.left + "px", this.helper[0].style.top = this.position.top + "px", a.ui.ddmanager && a.ui.ddmanager.drag(this, b), !1
        },
        _mouseStop: function (b) {
            var c = this, d = !1;
            return a.ui.ddmanager && !this.options.dropBehaviour && (d = a.ui.ddmanager.drop(this, b)), this.dropped && (d = this.dropped, this.dropped = !1), "invalid" === this.options.revert && !d || "valid" === this.options.revert && d || this.options.revert === !0 || a.isFunction(this.options.revert) && this.options.revert.call(this.element, d) ? a(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function () {
                c._trigger("stop", b) !== !1 && c._clear()
            }) : this._trigger("stop", b) !== !1 && this._clear(), !1
        },
        _mouseUp: function (b) {
            return this._unblockFrames(), a.ui.ddmanager && a.ui.ddmanager.dragStop(this, b), this.handleElement.is(b.target) && this.element.focus(), a.ui.mouse.prototype._mouseUp.call(this, b)
        },
        cancel: function () {
            return this.helper.is(".ui-draggable-dragging") ? this._mouseUp({}) : this._clear(), this
        },
        _getHandle: function (b) {
            return this.options.handle ? !!a(b.target).closest(this.element.find(this.options.handle)).length : !0
        },
        _setHandleClassName: function () {
            this.handleElement = this.options.handle ? this.element.find(this.options.handle) : this.element, this.handleElement.addClass("ui-draggable-handle")
        },
        _removeHandleClassName: function () {
            this.handleElement.removeClass("ui-draggable-handle")
        },
        _createHelper: function (b) {
            var c = this.options, d = a.isFunction(c.helper), e = d ? a(c.helper.apply(this.element[0], [b])) : "clone" === c.helper ? this.element.clone().removeAttr("id") : this.element;
            return e.parents("body").length || e.appendTo("parent" === c.appendTo ? this.element[0].parentNode : c.appendTo), d && e[0] === this.element[0] && this._setPositionRelative(), e[0] === this.element[0] || /(fixed|absolute)/.test(e.css("position")) || e.css("position", "absolute"), e
        },
        _setPositionRelative: function () {
            /^(?:r|a|f)/.test(this.element.css("position")) || (this.element[0].style.position = "relative")
        },
        _adjustOffsetFromHelper: function (b) {
            "string" == typeof b && (b = b.split(" ")), a.isArray(b) && (b = {
                left: +b[0],
                top: +b[1] || 0
            }), "left"in b && (this.offset.click.left = b.left + this.margins.left), "right"in b && (this.offset.click.left = this.helperProportions.width - b.right + this.margins.left), "top"in b && (this.offset.click.top = b.top + this.margins.top), "bottom"in b && (this.offset.click.top = this.helperProportions.height - b.bottom + this.margins.top)
        },
        _isRootNode: function (a) {
            return /(html|body)/i.test(a.tagName) || a === this.document[0]
        },
        _getParentOffset: function () {
            var b = this.offsetParent.offset(), c = this.document[0];
            return "absolute" === this.cssPosition && this.scrollParent[0] !== c && a.contains(this.scrollParent[0], this.offsetParent[0]) && (b.left += this.scrollParent.scrollLeft(), b.top += this.scrollParent.scrollTop()), this._isRootNode(this.offsetParent[0]) && (b = {
                top: 0,
                left: 0
            }), {
                top: b.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                left: b.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
            }
        },
        _getRelativeOffset: function () {
            if ("relative" !== this.cssPosition)return {top: 0, left: 0};
            var a = this.element.position(), b = this._isRootNode(this.scrollParent[0]);
            return {
                top: a.top - (parseInt(this.helper.css("top"), 10) || 0) + (b ? 0 : this.scrollParent.scrollTop()),
                left: a.left - (parseInt(this.helper.css("left"), 10) || 0) + (b ? 0 : this.scrollParent.scrollLeft())
            }
        },
        _cacheMargins: function () {
            this.margins = {
                left: parseInt(this.element.css("marginLeft"), 10) || 0,
                top: parseInt(this.element.css("marginTop"), 10) || 0,
                right: parseInt(this.element.css("marginRight"), 10) || 0,
                bottom: parseInt(this.element.css("marginBottom"), 10) || 0
            }
        },
        _cacheHelperProportions: function () {
            this.helperProportions = {width: this.helper.outerWidth(), height: this.helper.outerHeight()}
        },
        _setContainment: function () {
            var b, c, d, e = this.options, f = this.document[0];
            return this.relativeContainer = null, e.containment ? "window" === e.containment ? void(this.containment = [a(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, a(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, a(window).scrollLeft() + a(window).width() - this.helperProportions.width - this.margins.left, a(window).scrollTop() + (a(window).height() || f.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : "document" === e.containment ? void(this.containment = [0, 0, a(f).width() - this.helperProportions.width - this.margins.left, (a(f).height() || f.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : e.containment.constructor === Array ? void(this.containment = e.containment) : ("parent" === e.containment && (e.containment = this.helper[0].parentNode), c = a(e.containment), d = c[0], void(d && (b = /(scroll|auto)/.test(c.css("overflow")), this.containment = [(parseInt(c.css("borderLeftWidth"), 10) || 0) + (parseInt(c.css("paddingLeft"), 10) || 0), (parseInt(c.css("borderTopWidth"), 10) || 0) + (parseInt(c.css("paddingTop"), 10) || 0), (b ? Math.max(d.scrollWidth, d.offsetWidth) : d.offsetWidth) - (parseInt(c.css("borderRightWidth"), 10) || 0) - (parseInt(c.css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (b ? Math.max(d.scrollHeight, d.offsetHeight) : d.offsetHeight) - (parseInt(c.css("borderBottomWidth"), 10) || 0) - (parseInt(c.css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom], this.relativeContainer = c))) : void(this.containment = null)
        },
        _convertPositionTo: function (a, b) {
            b || (b = this.position);
            var c = "absolute" === a ? 1 : -1, d = this._isRootNode(this.scrollParent[0]);
            return {
                top: b.top + this.offset.relative.top * c + this.offset.parent.top * c - ("fixed" === this.cssPosition ? -this.offset.scroll.top : d ? 0 : this.offset.scroll.top) * c,
                left: b.left + this.offset.relative.left * c + this.offset.parent.left * c - ("fixed" === this.cssPosition ? -this.offset.scroll.left : d ? 0 : this.offset.scroll.left) * c
            }
        },
        _generatePosition: function (a, b) {
            var c, d, e, f, g = this.options, h = this._isRootNode(this.scrollParent[0]), i = a.pageX, j = a.pageY;
            return h && this.offset.scroll || (this.offset.scroll = {
                top: this.scrollParent.scrollTop(),
                left: this.scrollParent.scrollLeft()
            }), b && (this.containment && (this.relativeContainer ? (d = this.relativeContainer.offset(), c = [this.containment[0] + d.left, this.containment[1] + d.top, this.containment[2] + d.left, this.containment[3] + d.top]) : c = this.containment, a.pageX - this.offset.click.left < c[0] && (i = c[0] + this.offset.click.left), a.pageY - this.offset.click.top < c[1] && (j = c[1] + this.offset.click.top), a.pageX - this.offset.click.left > c[2] && (i = c[2] + this.offset.click.left), a.pageY - this.offset.click.top > c[3] && (j = c[3] + this.offset.click.top)), g.grid && (e = g.grid[1] ? this.originalPageY + Math.round((j - this.originalPageY) / g.grid[1]) * g.grid[1] : this.originalPageY, j = c ? e - this.offset.click.top >= c[1] || e - this.offset.click.top > c[3] ? e : e - this.offset.click.top >= c[1] ? e - g.grid[1] : e + g.grid[1] : e, f = g.grid[0] ? this.originalPageX + Math.round((i - this.originalPageX) / g.grid[0]) * g.grid[0] : this.originalPageX, i = c ? f - this.offset.click.left >= c[0] || f - this.offset.click.left > c[2] ? f : f - this.offset.click.left >= c[0] ? f - g.grid[0] : f + g.grid[0] : f), "y" === g.axis && (i = this.originalPageX), "x" === g.axis && (j = this.originalPageY)), {
                top: j - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.offset.scroll.top : h ? 0 : this.offset.scroll.top),
                left: i - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.offset.scroll.left : h ? 0 : this.offset.scroll.left)
            }
        },
        _clear: function () {
            this.helper.removeClass("ui-draggable-dragging"), this.helper[0] === this.element[0] || this.cancelHelperRemoval || this.helper.remove(), this.helper = null, this.cancelHelperRemoval = !1, this.destroyOnClear && this.destroy()
        },
        _normalizeRightBottom: function () {
            "y" !== this.options.axis && "auto" !== this.helper.css("right") && (this.helper.width(this.helper.width()), this.helper.css("right", "auto")), "x" !== this.options.axis && "auto" !== this.helper.css("bottom") && (this.helper.height(this.helper.height()), this.helper.css("bottom", "auto"))
        },
        _trigger: function (b, c, d) {
            return d = d || this._uiHash(), a.ui.plugin.call(this, b, [c, d, this], !0), /^(drag|start|stop)/.test(b) && (this.positionAbs = this._convertPositionTo("absolute"), d.offset = this.positionAbs), a.Widget.prototype._trigger.call(this, b, c, d)
        },
        plugins: {},
        _uiHash: function () {
            return {
                helper: this.helper,
                position: this.position,
                originalPosition: this.originalPosition,
                offset: this.positionAbs
            }
        }
    }), a.ui.plugin.add("draggable", "connectToSortable", {
        start: function (b, c, d) {
            var e = a.extend({}, c, {item: d.element});
            d.sortables = [], a(d.options.connectToSortable).each(function () {
                var c = a(this).sortable("instance");
                c && !c.options.disabled && (d.sortables.push(c), c.refreshPositions(), c._trigger("activate", b, e))
            })
        }, stop: function (b, c, d) {
            var e = a.extend({}, c, {item: d.element});
            d.cancelHelperRemoval = !1, a.each(d.sortables, function () {
                var a = this;
                a.isOver ? (a.isOver = 0, d.cancelHelperRemoval = !0, a.cancelHelperRemoval = !1, a._storedCSS = {
                    position: a.placeholder.css("position"),
                    top: a.placeholder.css("top"),
                    left: a.placeholder.css("left")
                }, a._mouseStop(b), a.options.helper = a.options._helper) : (a.cancelHelperRemoval = !0, a._trigger("deactivate", b, e))
            })
        }, drag: function (b, c, d) {
            a.each(d.sortables, function () {
                var e = !1, f = this;
                f.positionAbs = d.positionAbs, f.helperProportions = d.helperProportions, f.offset.click = d.offset.click, f._intersectsWith(f.containerCache) && (e = !0, a.each(d.sortables, function () {
                    return this.positionAbs = d.positionAbs, this.helperProportions = d.helperProportions, this.offset.click = d.offset.click, this !== f && this._intersectsWith(this.containerCache) && a.contains(f.element[0], this.element[0]) && (e = !1), e
                })), e ? (f.isOver || (f.isOver = 1, f.currentItem = c.helper.appendTo(f.element).data("ui-sortable-item", !0), f.options._helper = f.options.helper, f.options.helper = function () {
                    return c.helper[0]
                }, b.target = f.currentItem[0], f._mouseCapture(b, !0), f._mouseStart(b, !0, !0), f.offset.click.top = d.offset.click.top, f.offset.click.left = d.offset.click.left, f.offset.parent.left -= d.offset.parent.left - f.offset.parent.left, f.offset.parent.top -= d.offset.parent.top - f.offset.parent.top, d._trigger("toSortable", b), d.dropped = f.element, a.each(d.sortables, function () {
                    this.refreshPositions()
                }), d.currentItem = d.element, f.fromOutside = d), f.currentItem && (f._mouseDrag(b), c.position = f.position)) : f.isOver && (f.isOver = 0, f.cancelHelperRemoval = !0, f.options._revert = f.options.revert, f.options.revert = !1, f._trigger("out", b, f._uiHash(f)), f._mouseStop(b, !0), f.options.revert = f.options._revert, f.options.helper = f.options._helper, f.placeholder && f.placeholder.remove(), d._refreshOffsets(b), c.position = d._generatePosition(b, !0), d._trigger("fromSortable", b), d.dropped = !1, a.each(d.sortables, function () {
                    this.refreshPositions()
                }))
            })
        }
    }), a.ui.plugin.add("draggable", "cursor", {
        start: function (b, c, d) {
            var e = a("body"), f = d.options;
            e.css("cursor") && (f._cursor = e.css("cursor")), e.css("cursor", f.cursor)
        }, stop: function (b, c, d) {
            var e = d.options;
            e._cursor && a("body").css("cursor", e._cursor)
        }
    }), a.ui.plugin.add("draggable", "opacity", {
        start: function (b, c, d) {
            var e = a(c.helper), f = d.options;
            e.css("opacity") && (f._opacity = e.css("opacity")), e.css("opacity", f.opacity)
        }, stop: function (b, c, d) {
            var e = d.options;
            e._opacity && a(c.helper).css("opacity", e._opacity)
        }
    }), a.ui.plugin.add("draggable", "scroll", {
        start: function (a, b, c) {
            c.scrollParentNotHidden || (c.scrollParentNotHidden = c.helper.scrollParent(!1)), c.scrollParentNotHidden[0] !== c.document[0] && "HTML" !== c.scrollParentNotHidden[0].tagName && (c.overflowOffset = c.scrollParentNotHidden.offset())
        }, drag: function (b, c, d) {
            var e = d.options, f = !1, g = d.scrollParentNotHidden[0], h = d.document[0];
            g !== h && "HTML" !== g.tagName ? (e.axis && "x" === e.axis || (d.overflowOffset.top + g.offsetHeight - b.pageY < e.scrollSensitivity ? g.scrollTop = f = g.scrollTop + e.scrollSpeed : b.pageY - d.overflowOffset.top < e.scrollSensitivity && (g.scrollTop = f = g.scrollTop - e.scrollSpeed)), e.axis && "y" === e.axis || (d.overflowOffset.left + g.offsetWidth - b.pageX < e.scrollSensitivity ? g.scrollLeft = f = g.scrollLeft + e.scrollSpeed : b.pageX - d.overflowOffset.left < e.scrollSensitivity && (g.scrollLeft = f = g.scrollLeft - e.scrollSpeed))) : (e.axis && "x" === e.axis || (b.pageY - a(h).scrollTop() < e.scrollSensitivity ? f = a(h).scrollTop(a(h).scrollTop() - e.scrollSpeed) : a(window).height() - (b.pageY - a(h).scrollTop()) < e.scrollSensitivity && (f = a(h).scrollTop(a(h).scrollTop() + e.scrollSpeed))), e.axis && "y" === e.axis || (b.pageX - a(h).scrollLeft() < e.scrollSensitivity ? f = a(h).scrollLeft(a(h).scrollLeft() - e.scrollSpeed) : a(window).width() - (b.pageX - a(h).scrollLeft()) < e.scrollSensitivity && (f = a(h).scrollLeft(a(h).scrollLeft() + e.scrollSpeed)))), f !== !1 && a.ui.ddmanager && !e.dropBehaviour && a.ui.ddmanager.prepareOffsets(d, b)
        }
    }), a.ui.plugin.add("draggable", "snap", {
        start: function (b, c, d) {
            var e = d.options;
            d.snapElements = [], a(e.snap.constructor !== String ? e.snap.items || ":data(ui-draggable)" : e.snap).each(function () {
                var b = a(this), c = b.offset();
                this !== d.element[0] && d.snapElements.push({
                    item: this,
                    width: b.outerWidth(),
                    height: b.outerHeight(),
                    top: c.top,
                    left: c.left
                })
            })
        }, drag: function (b, c, d) {
            var e, f, g, h, i, j, k, l, m, n, o = d.options, p = o.snapTolerance, q = c.offset.left, r = q + d.helperProportions.width, s = c.offset.top, t = s + d.helperProportions.height;
            for (m = d.snapElements.length - 1; m >= 0; m--)i = d.snapElements[m].left - d.margins.left, j = i + d.snapElements[m].width, k = d.snapElements[m].top - d.margins.top, l = k + d.snapElements[m].height, i - p > r || q > j + p || k - p > t || s > l + p || !a.contains(d.snapElements[m].item.ownerDocument, d.snapElements[m].item) ? (d.snapElements[m].snapping && d.options.snap.release && d.options.snap.release.call(d.element, b, a.extend(d._uiHash(), {snapItem: d.snapElements[m].item})), d.snapElements[m].snapping = !1) : ("inner" !== o.snapMode && (e = p >= Math.abs(k - t), f = p >= Math.abs(l - s), g = p >= Math.abs(i - r), h = p >= Math.abs(j - q), e && (c.position.top = d._convertPositionTo("relative", {
                top: k - d.helperProportions.height,
                left: 0
            }).top), f && (c.position.top = d._convertPositionTo("relative", {
                top: l,
                left: 0
            }).top), g && (c.position.left = d._convertPositionTo("relative", {
                top: 0,
                left: i - d.helperProportions.width
            }).left), h && (c.position.left = d._convertPositionTo("relative", {
                top: 0,
                left: j
            }).left)), n = e || f || g || h, "outer" !== o.snapMode && (e = p >= Math.abs(k - s), f = p >= Math.abs(l - t), g = p >= Math.abs(i - q), h = p >= Math.abs(j - r), e && (c.position.top = d._convertPositionTo("relative", {
                top: k,
                left: 0
            }).top), f && (c.position.top = d._convertPositionTo("relative", {
                top: l - d.helperProportions.height,
                left: 0
            }).top), g && (c.position.left = d._convertPositionTo("relative", {
                top: 0,
                left: i
            }).left), h && (c.position.left = d._convertPositionTo("relative", {
                top: 0,
                left: j - d.helperProportions.width
            }).left)), !d.snapElements[m].snapping && (e || f || g || h || n) && d.options.snap.snap && d.options.snap.snap.call(d.element, b, a.extend(d._uiHash(), {snapItem: d.snapElements[m].item})), d.snapElements[m].snapping = e || f || g || h || n)
        }
    }), a.ui.plugin.add("draggable", "stack", {
        start: function (b, c, d) {
            var e, f = d.options, g = a.makeArray(a(f.stack)).sort(function (b, c) {
                return (parseInt(a(b).css("zIndex"), 10) || 0) - (parseInt(a(c).css("zIndex"), 10) || 0)
            });
            g.length && (e = parseInt(a(g[0]).css("zIndex"), 10) || 0, a(g).each(function (b) {
                a(this).css("zIndex", e + b)
            }), this.css("zIndex", e + g.length))
        }
    }), a.ui.plugin.add("draggable", "zIndex", {
        start: function (b, c, d) {
            var e = a(c.helper), f = d.options;
            e.css("zIndex") && (f._zIndex = e.css("zIndex")), e.css("zIndex", f.zIndex)
        }, stop: function (b, c, d) {
            var e = d.options;
            e._zIndex && a(c.helper).css("zIndex", e._zIndex)
        }
    }), a.ui.draggable
}), function (a) {
    "function" == typeof define && define.amd ? define(["jquery", "/core", "/widget", "/mouse", "/draggable"], a) : a(jQuery)
}(function (a) {
    return a.widget("ui.droppable", {
        version: "1.11.2",
        widgetEventPrefix: "drop",
        options: {
            accept: "*",
            activeClass: !1,
            addClasses: !0,
            greedy: !1,
            hoverClass: !1,
            scope: "default",
            tolerance: "intersect",
            activate: null,
            deactivate: null,
            drop: null,
            out: null,
            over: null
        },
        _create: function () {
            var b, c = this.options, d = c.accept;
            this.isover = !1, this.isout = !0, this.accept = a.isFunction(d) ? d : function (a) {
                return a.is(d)
            }, this.proportions = function () {
                return arguments.length ? void(b = arguments[0]) : b ? b : b = {
                    width: this.element[0].offsetWidth,
                    height: this.element[0].offsetHeight
                }
            }, this._addToManager(c.scope), c.addClasses && this.element.addClass("ui-droppable")
        },
        _addToManager: function (b) {
            a.ui.ddmanager.droppables[b] = a.ui.ddmanager.droppables[b] || [], a.ui.ddmanager.droppables[b].push(this)
        },
        _splice: function (a) {
            for (var b = 0; a.length > b; b++)a[b] === this && a.splice(b, 1)
        },
        _destroy: function () {
            var b = a.ui.ddmanager.droppables[this.options.scope];
            this._splice(b), this.element.removeClass("ui-droppable ui-droppable-disabled")
        },
        _setOption: function (b, c) {
            if ("accept" === b)this.accept = a.isFunction(c) ? c : function (a) {
                return a.is(c)
            }; else if ("scope" === b) {
                var d = a.ui.ddmanager.droppables[this.options.scope];
                this._splice(d), this._addToManager(c)
            }
            this._super(b, c)
        },
        _activate: function (b) {
            var c = a.ui.ddmanager.current;
            this.options.activeClass && this.element.addClass(this.options.activeClass), c && this._trigger("activate", b, this.ui(c))
        },
        _deactivate: function (b) {
            var c = a.ui.ddmanager.current;
            this.options.activeClass && this.element.removeClass(this.options.activeClass), c && this._trigger("deactivate", b, this.ui(c))
        },
        _over: function (b) {
            var c = a.ui.ddmanager.current;
            c && (c.currentItem || c.element)[0] !== this.element[0] && this.accept.call(this.element[0], c.currentItem || c.element) && (this.options.hoverClass && this.element.addClass(this.options.hoverClass), this._trigger("over", b, this.ui(c)))
        },
        _out: function (b) {
            var c = a.ui.ddmanager.current;
            c && (c.currentItem || c.element)[0] !== this.element[0] && this.accept.call(this.element[0], c.currentItem || c.element) && (this.options.hoverClass && this.element.removeClass(this.options.hoverClass), this._trigger("out", b, this.ui(c)))
        },
        _drop: function (b, c) {
            var d = c || a.ui.ddmanager.current, e = !1;
            return d && (d.currentItem || d.element)[0] !== this.element[0] ? (this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function () {
                var c = a(this).droppable("instance");
                return c.options.greedy && !c.options.disabled && c.options.scope === d.options.scope && c.accept.call(c.element[0], d.currentItem || d.element) && a.ui.intersect(d, a.extend(c, {offset: c.element.offset()}), c.options.tolerance, b) ? (e = !0, !1) : void 0
            }), e ? !1 : this.accept.call(this.element[0], d.currentItem || d.element) ? (this.options.activeClass && this.element.removeClass(this.options.activeClass), this.options.hoverClass && this.element.removeClass(this.options.hoverClass), this._trigger("drop", b, this.ui(d)), this.element) : !1) : !1
        },
        ui: function (a) {
            return {
                draggable: a.currentItem || a.element,
                helper: a.helper,
                position: a.position,
                offset: a.positionAbs
            }
        }
    }), a.ui.intersect = function () {
        function a(a, b, c) {
            return a >= b && b + c > a
        }

        return function (b, c, d, e) {
            if (!c.offset)return !1;
            var f = (b.positionAbs || b.position.absolute).left + b.margins.left, g = (b.positionAbs || b.position.absolute).top + b.margins.top, h = f + b.helperProportions.width, i = g + b.helperProportions.height, j = c.offset.left, k = c.offset.top, l = j + c.proportions().width, m = k + c.proportions().height;
            switch (d) {
                case"fit":
                    return f >= j && l >= h && g >= k && m >= i;
                case"intersect":
                    return f + b.helperProportions.width / 2 > j && l > h - b.helperProportions.width / 2 && g + b.helperProportions.height / 2 > k && m > i - b.helperProportions.height / 2;
                case"pointer":
                    return a(e.pageY, k, c.proportions().height) && a(e.pageX, j, c.proportions().width);
                case"touch":
                    return (g >= k && m >= g || i >= k && m >= i || k > g && i > m) && (f >= j && l >= f || h >= j && l >= h || j > f && h > l);
                default:
                    return !1
            }
        }
    }(), a.ui.ddmanager = {
        current: null, droppables: {"default": []}, prepareOffsets: function (b, c) {
            var d, e, f = a.ui.ddmanager.droppables[b.options.scope] || [], g = c ? c.type : null, h = (b.currentItem || b.element).find(":data(ui-droppable)").addBack();
            a:for (d = 0; f.length > d; d++)if (!(f[d].options.disabled || b && !f[d].accept.call(f[d].element[0], b.currentItem || b.element))) {
                for (e = 0; h.length > e; e++)if (h[e] === f[d].element[0]) {
                    f[d].proportions().height = 0;
                    continue a
                }
                f[d].visible = "none" !== f[d].element.css("display"), f[d].visible && ("mousedown" === g && f[d]._activate.call(f[d], c), f[d].offset = f[d].element.offset(), f[d].proportions({
                    width: f[d].element[0].offsetWidth,
                    height: f[d].element[0].offsetHeight
                }))
            }
        }, drop: function (b, c) {
            var d = !1;
            return a.each((a.ui.ddmanager.droppables[b.options.scope] || []).slice(), function () {
                this.options && (!this.options.disabled && this.visible && a.ui.intersect(b, this, this.options.tolerance, c) && (d = this._drop.call(this, c) || d), !this.options.disabled && this.visible && this.accept.call(this.element[0], b.currentItem || b.element) && (this.isout = !0, this.isover = !1, this._deactivate.call(this, c)))
            }), d
        }, dragStart: function (b, c) {
            b.element.parentsUntil("body").bind("scroll.droppable", function () {
                b.options.refreshPositions || a.ui.ddmanager.prepareOffsets(b, c)
            })
        }, drag: function (b, c) {
            b.options.refreshPositions && a.ui.ddmanager.prepareOffsets(b, c), a.each(a.ui.ddmanager.droppables[b.options.scope] || [], function () {
                if (!this.options.disabled && !this.greedyChild && this.visible) {
                    var d, e, f, g = a.ui.intersect(b, this, this.options.tolerance, c), h = !g && this.isover ? "isout" : g && !this.isover ? "isover" : null;
                    h && (this.options.greedy && (e = this.options.scope, f = this.element.parents(":data(ui-droppable)").filter(function () {
                        return a(this).droppable("instance").options.scope === e
                    }), f.length && (d = a(f[0]).droppable("instance"), d.greedyChild = "isover" === h)), d && "isover" === h && (d.isover = !1, d.isout = !0, d._out.call(d, c)), this[h] = !0, this["isout" === h ? "isover" : "isout"] = !1, this["isover" === h ? "_over" : "_out"].call(this, c), d && "isout" === h && (d.isout = !1, d.isover = !0, d._over.call(d, c)))
                }
            })
        }, dragStop: function (b, c) {
            b.element.parentsUntil("body").unbind("scroll.droppable"), b.options.refreshPositions || a.ui.ddmanager.prepareOffsets(b, c)
        }
    }, a.ui.droppable
}), function (a) {
    "function" == typeof define && define.amd ? define(["jquery", "/core", "/mouse", "/widget"], a) : a(jQuery)
}(function (a) {
    return a.widget("ui.resizable", a.ui.mouse, {
        version: "1.11.2",
        widgetEventPrefix: "resize",
        options: {
            alsoResize: !1,
            animate: !1,
            animateDuration: "slow",
            animateEasing: "swing",
            aspectRatio: !1,
            autoHide: !1,
            containment: !1,
            ghost: !1,
            grid: !1,
            handles: "e,s,se",
            helper: !1,
            maxHeight: null,
            maxWidth: null,
            minHeight: 10,
            minWidth: 10,
            zIndex: 90,
            resize: null,
            start: null,
            stop: null
        },
        _num: function (a) {
            return parseInt(a, 10) || 0
        },
        _isNumber: function (a) {
            return !isNaN(parseInt(a, 10))
        },
        _hasScroll: function (b, c) {
            if ("hidden" === a(b).css("overflow"))return !1;
            var d = c && "left" === c ? "scrollLeft" : "scrollTop", e = !1;
            return b[d] > 0 ? !0 : (b[d] = 1, e = b[d] > 0, b[d] = 0, e)
        },
        _create: function () {
            var b, c, d, e, f, g = this, h = this.options;
            if (this.element.addClass("ui-resizable"), a.extend(this, {
                    _aspectRatio: !!h.aspectRatio,
                    aspectRatio: h.aspectRatio,
                    originalElement: this.element,
                    _proportionallyResizeElements: [],
                    _helper: h.helper || h.ghost || h.animate ? h.helper || "ui-resizable-helper" : null
                }), this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i) && (this.element.wrap(a("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({
                    position: this.element.css("position"),
                    width: this.element.outerWidth(),
                    height: this.element.outerHeight(),
                    top: this.element.css("top"),
                    left: this.element.css("left")
                })), this.element = this.element.parent().data("ui-resizable", this.element.resizable("instance")), this.elementIsWrapper = !0, this.element.css({
                    marginLeft: this.originalElement.css("marginLeft"),
                    marginTop: this.originalElement.css("marginTop"),
                    marginRight: this.originalElement.css("marginRight"),
                    marginBottom: this.originalElement.css("marginBottom")
                }), this.originalElement.css({
                    marginLeft: 0,
                    marginTop: 0,
                    marginRight: 0,
                    marginBottom: 0
                }), this.originalResizeStyle = this.originalElement.css("resize"), this.originalElement.css("resize", "none"), this._proportionallyResizeElements.push(this.originalElement.css({
                    position: "static",
                    zoom: 1,
                    display: "block"
                })), this.originalElement.css({margin: this.originalElement.css("margin")}), this._proportionallyResize()), this.handles = h.handles || (a(".ui-resizable-handle", this.element).length ? {
                    n: ".ui-resizable-n",
                    e: ".ui-resizable-e",
                    s: ".ui-resizable-s",
                    w: ".ui-resizable-w",
                    se: ".ui-resizable-se",
                    sw: ".ui-resizable-sw",
                    ne: ".ui-resizable-ne",
                    nw: ".ui-resizable-nw"
                } : "e,s,se"), this.handles.constructor === String)for ("all" === this.handles && (this.handles = "n,e,s,w,se,sw,ne,nw"), b = this.handles.split(","), this.handles = {}, c = 0; b.length > c; c++)d = a.trim(b[c]), f = "ui-resizable-" + d, e = a("<div class='ui-resizable-handle " + f + "'></div>"), e.css({zIndex: h.zIndex}), "se" === d && e.addClass("ui-icon ui-icon-gripsmall-diagonal-se"), this.handles[d] = ".ui-resizable-" + d, this.element.append(e);
            this._renderAxis = function (b) {
                var c, d, e, f;
                b = b || this.element;
                for (c in this.handles)this.handles[c].constructor === String && (this.handles[c] = this.element.children(this.handles[c]).first().show()), this.elementIsWrapper && this.originalElement[0].nodeName.match(/textarea|input|select|button/i) && (d = a(this.handles[c], this.element), f = /sw|ne|nw|se|n|s/.test(c) ? d.outerHeight() : d.outerWidth(), e = ["padding", /ne|nw|n/.test(c) ? "Top" : /se|sw|s/.test(c) ? "Bottom" : /^e$/.test(c) ? "Right" : "Left"].join(""), b.css(e, f), this._proportionallyResize()), a(this.handles[c]).length
            }, this._renderAxis(this.element), this._handles = a(".ui-resizable-handle", this.element).disableSelection(), this._handles.mouseover(function () {
                g.resizing || (this.className && (e = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)), g.axis = e && e[1] ? e[1] : "se")
            }), h.autoHide && (this._handles.hide(), a(this.element).addClass("ui-resizable-autohide").mouseenter(function () {
                h.disabled || (a(this).removeClass("ui-resizable-autohide"), g._handles.show())
            }).mouseleave(function () {
                h.disabled || g.resizing || (a(this).addClass("ui-resizable-autohide"), g._handles.hide())
            })), this._mouseInit()
        },
        _destroy: function () {
            this._mouseDestroy();
            var b, c = function (b) {
                a(b).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove()
            };
            return this.elementIsWrapper && (c(this.element), b = this.element, this.originalElement.css({
                position: b.css("position"),
                width: b.outerWidth(),
                height: b.outerHeight(),
                top: b.css("top"),
                left: b.css("left")
            }).insertAfter(b), b.remove()), this.originalElement.css("resize", this.originalResizeStyle), c(this.originalElement), this
        },
        _mouseCapture: function (b) {
            var c, d, e = !1;
            for (c in this.handles)d = a(this.handles[c])[0], (d === b.target || a.contains(d, b.target)) && (e = !0);
            return !this.options.disabled && e
        },
        _mouseStart: function (b) {
            var c, d, e, f = this.options, g = this.element;
            return this.resizing = !0, this._renderProxy(), c = this._num(this.helper.css("left")), d = this._num(this.helper.css("top")), f.containment && (c += a(f.containment).scrollLeft() || 0, d += a(f.containment).scrollTop() || 0), this.offset = this.helper.offset(), this.position = {
                left: c,
                top: d
            }, this.size = this._helper ? {
                width: this.helper.width(),
                height: this.helper.height()
            } : {width: g.width(), height: g.height()}, this.originalSize = this._helper ? {
                width: g.outerWidth(),
                height: g.outerHeight()
            } : {width: g.width(), height: g.height()}, this.sizeDiff = {
                width: g.outerWidth() - g.width(),
                height: g.outerHeight() - g.height()
            }, this.originalPosition = {left: c, top: d}, this.originalMousePosition = {
                left: b.pageX,
                top: b.pageY
            }, this.aspectRatio = "number" == typeof f.aspectRatio ? f.aspectRatio : this.originalSize.width / this.originalSize.height || 1, e = a(".ui-resizable-" + this.axis).css("cursor"), a("body").css("cursor", "auto" === e ? this.axis + "-resize" : e), g.addClass("ui-resizable-resizing"), this._propagate("start", b), !0
        },
        _mouseDrag: function (b) {
            var c, d, e = this.originalMousePosition, f = this.axis, g = b.pageX - e.left || 0, h = b.pageY - e.top || 0, i = this._change[f];
            return this._updatePrevProperties(), i ? (c = i.apply(this, [b, g, h]), this._updateVirtualBoundaries(b.shiftKey), (this._aspectRatio || b.shiftKey) && (c = this._updateRatio(c, b)), c = this._respectSize(c, b), this._updateCache(c), this._propagate("resize", b), d = this._applyChanges(), !this._helper && this._proportionallyResizeElements.length && this._proportionallyResize(), a.isEmptyObject(d) || (this._updatePrevProperties(), this._trigger("resize", b, this.ui()), this._applyChanges()), !1) : !1
        },
        _mouseStop: function (b) {
            this.resizing = !1;
            var c, d, e, f, g, h, i, j = this.options, k = this;
            return this._helper && (c = this._proportionallyResizeElements, d = c.length && /textarea/i.test(c[0].nodeName), e = d && this._hasScroll(c[0], "left") ? 0 : k.sizeDiff.height, f = d ? 0 : k.sizeDiff.width, g = {
                width: k.helper.width() - f,
                height: k.helper.height() - e
            }, h = parseInt(k.element.css("left"), 10) + (k.position.left - k.originalPosition.left) || null, i = parseInt(k.element.css("top"), 10) + (k.position.top - k.originalPosition.top) || null, j.animate || this.element.css(a.extend(g, {
                top: i,
                left: h
            })), k.helper.height(k.size.height), k.helper.width(k.size.width), this._helper && !j.animate && this._proportionallyResize()), a("body").css("cursor", "auto"), this.element.removeClass("ui-resizable-resizing"), this._propagate("stop", b), this._helper && this.helper.remove(), !1
        },
        _updatePrevProperties: function () {
            this.prevPosition = {
                top: this.position.top,
                left: this.position.left
            }, this.prevSize = {width: this.size.width, height: this.size.height}
        },
        _applyChanges: function () {
            var a = {};
            return this.position.top !== this.prevPosition.top && (a.top = this.position.top + "px"), this.position.left !== this.prevPosition.left && (a.left = this.position.left + "px"), this.size.width !== this.prevSize.width && (a.width = this.size.width + "px"), this.size.height !== this.prevSize.height && (a.height = this.size.height + "px"), this.helper.css(a), a
        },
        _updateVirtualBoundaries: function (a) {
            var b, c, d, e, f, g = this.options;
            f = {
                minWidth: this._isNumber(g.minWidth) ? g.minWidth : 0,
                maxWidth: this._isNumber(g.maxWidth) ? g.maxWidth : 1 / 0,
                minHeight: this._isNumber(g.minHeight) ? g.minHeight : 0,
                maxHeight: this._isNumber(g.maxHeight) ? g.maxHeight : 1 / 0
            }, (this._aspectRatio || a) && (b = f.minHeight * this.aspectRatio, d = f.minWidth / this.aspectRatio, c = f.maxHeight * this.aspectRatio, e = f.maxWidth / this.aspectRatio, b > f.minWidth && (f.minWidth = b), d > f.minHeight && (f.minHeight = d), f.maxWidth > c && (f.maxWidth = c), f.maxHeight > e && (f.maxHeight = e)), this._vBoundaries = f
        },
        _updateCache: function (a) {
            this.offset = this.helper.offset(), this._isNumber(a.left) && (this.position.left = a.left), this._isNumber(a.top) && (this.position.top = a.top), this._isNumber(a.height) && (this.size.height = a.height), this._isNumber(a.width) && (this.size.width = a.width)
        },
        _updateRatio: function (a) {
            var b = this.position, c = this.size, d = this.axis;
            return this._isNumber(a.height) ? a.width = a.height * this.aspectRatio : this._isNumber(a.width) && (a.height = a.width / this.aspectRatio), "sw" === d && (a.left = b.left + (c.width - a.width), a.top = null), "nw" === d && (a.top = b.top + (c.height - a.height), a.left = b.left + (c.width - a.width)), a
        },
        _respectSize: function (a) {
            var b = this._vBoundaries, c = this.axis, d = this._isNumber(a.width) && b.maxWidth && b.maxWidth < a.width, e = this._isNumber(a.height) && b.maxHeight && b.maxHeight < a.height, f = this._isNumber(a.width) && b.minWidth && b.minWidth > a.width, g = this._isNumber(a.height) && b.minHeight && b.minHeight > a.height, h = this.originalPosition.left + this.originalSize.width, i = this.position.top + this.size.height, j = /sw|nw|w/.test(c), k = /nw|ne|n/.test(c);
            return f && (a.width = b.minWidth), g && (a.height = b.minHeight), d && (a.width = b.maxWidth), e && (a.height = b.maxHeight), f && j && (a.left = h - b.minWidth), d && j && (a.left = h - b.maxWidth), g && k && (a.top = i - b.minHeight), e && k && (a.top = i - b.maxHeight), a.width || a.height || a.left || !a.top ? a.width || a.height || a.top || !a.left || (a.left = null) : a.top = null, a
        },
        _getPaddingPlusBorderDimensions: function (a) {
            for (var b = 0, c = [], d = [a.css("borderTopWidth"), a.css("borderRightWidth"), a.css("borderBottomWidth"), a.css("borderLeftWidth")], e = [a.css("paddingTop"), a.css("paddingRight"), a.css("paddingBottom"), a.css("paddingLeft")]; 4 > b; b++)c[b] = parseInt(d[b], 10) || 0, c[b] += parseInt(e[b], 10) || 0;
            return {height: c[0] + c[2], width: c[1] + c[3]}
        },
        _proportionallyResize: function () {
            if (this._proportionallyResizeElements.length)for (var a, b = 0, c = this.helper || this.element; this._proportionallyResizeElements.length > b; b++)a = this._proportionallyResizeElements[b], this.outerDimensions || (this.outerDimensions = this._getPaddingPlusBorderDimensions(a)), a.css({
                height: c.height() - this.outerDimensions.height || 0,
                width: c.width() - this.outerDimensions.width || 0
            })
        },
        _renderProxy: function () {
            var b = this.element, c = this.options;
            this.elementOffset = b.offset(), this._helper ? (this.helper = this.helper || a("<div style='overflow:hidden;'></div>"), this.helper.addClass(this._helper).css({
                width: this.element.outerWidth() - 1,
                height: this.element.outerHeight() - 1,
                position: "absolute",
                left: this.elementOffset.left + "px",
                top: this.elementOffset.top + "px",
                zIndex: ++c.zIndex
            }), this.helper.appendTo("body").disableSelection()) : this.helper = this.element
        },
        _change: {
            e: function (a, b) {
                return {width: this.originalSize.width + b}
            }, w: function (a, b) {
                var c = this.originalSize, d = this.originalPosition;
                return {left: d.left + b, width: c.width - b}
            }, n: function (a, b, c) {
                var d = this.originalSize, e = this.originalPosition;
                return {top: e.top + c, height: d.height - c}
            }, s: function (a, b, c) {
                return {height: this.originalSize.height + c}
            }, se: function (b, c, d) {
                return a.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [b, c, d]))
            }, sw: function (b, c, d) {
                return a.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [b, c, d]))
            }, ne: function (b, c, d) {
                return a.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [b, c, d]))
            }, nw: function (b, c, d) {
                return a.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [b, c, d]))
            }
        },
        _propagate: function (b, c) {
            a.ui.plugin.call(this, b, [c, this.ui()]), "resize" !== b && this._trigger(b, c, this.ui())
        },
        plugins: {},
        ui: function () {
            return {
                originalElement: this.originalElement,
                element: this.element,
                helper: this.helper,
                position: this.position,
                size: this.size,
                originalSize: this.originalSize,
                originalPosition: this.originalPosition
            }
        }
    }), a.ui.plugin.add("resizable", "animate", {
        stop: function (b) {
            var c = a(this).resizable("instance"), d = c.options, e = c._proportionallyResizeElements, f = e.length && /textarea/i.test(e[0].nodeName), g = f && c._hasScroll(e[0], "left") ? 0 : c.sizeDiff.height, h = f ? 0 : c.sizeDiff.width, i = {
                width: c.size.width - h,
                height: c.size.height - g
            }, j = parseInt(c.element.css("left"), 10) + (c.position.left - c.originalPosition.left) || null, k = parseInt(c.element.css("top"), 10) + (c.position.top - c.originalPosition.top) || null;
            c.element.animate(a.extend(i, k && j ? {top: k, left: j} : {}), {
                duration: d.animateDuration,
                easing: d.animateEasing,
                step: function () {
                    var d = {
                        width: parseInt(c.element.css("width"), 10),
                        height: parseInt(c.element.css("height"), 10),
                        top: parseInt(c.element.css("top"), 10),
                        left: parseInt(c.element.css("left"), 10)
                    };
                    e && e.length && a(e[0]).css({
                        width: d.width,
                        height: d.height
                    }), c._updateCache(d), c._propagate("resize", b)
                }
            })
        }
    }), a.ui.plugin.add("resizable", "containment", {
        start: function () {
            var b, c, d, e, f, g, h, i = a(this).resizable("instance"), j = i.options, k = i.element, l = j.containment, m = l instanceof a ? l.get(0) : /parent/.test(l) ? k.parent().get(0) : l;
            m && (i.containerElement = a(m), /document/.test(l) || l === document ? (i.containerOffset = {
                left: 0,
                top: 0
            }, i.containerPosition = {left: 0, top: 0}, i.parentData = {
                element: a(document),
                left: 0,
                top: 0,
                width: a(document).width(),
                height: a(document).height() || document.body.parentNode.scrollHeight
            }) : (b = a(m), c = [], a(["Top", "Right", "Left", "Bottom"]).each(function (a, d) {
                c[a] = i._num(b.css("padding" + d))
            }), i.containerOffset = b.offset(), i.containerPosition = b.position(), i.containerSize = {
                height: b.innerHeight() - c[3],
                width: b.innerWidth() - c[1]
            }, d = i.containerOffset, e = i.containerSize.height, f = i.containerSize.width, g = i._hasScroll(m, "left") ? m.scrollWidth : f, h = i._hasScroll(m) ? m.scrollHeight : e, i.parentData = {
                element: m,
                left: d.left,
                top: d.top,
                width: g,
                height: h
            }))
        }, resize: function (b) {
            var c, d, e, f, g = a(this).resizable("instance"), h = g.options, i = g.containerOffset, j = g.position, k = g._aspectRatio || b.shiftKey, l = {
                top: 0,
                left: 0
            }, m = g.containerElement, n = !0;
            m[0] !== document && /static/.test(m.css("position")) && (l = i), j.left < (g._helper ? i.left : 0) && (g.size.width = g.size.width + (g._helper ? g.position.left - i.left : g.position.left - l.left), k && (g.size.height = g.size.width / g.aspectRatio, n = !1), g.position.left = h.helper ? i.left : 0), j.top < (g._helper ? i.top : 0) && (g.size.height = g.size.height + (g._helper ? g.position.top - i.top : g.position.top), k && (g.size.width = g.size.height * g.aspectRatio, n = !1), g.position.top = g._helper ? i.top : 0), e = g.containerElement.get(0) === g.element.parent().get(0), f = /relative|absolute/.test(g.containerElement.css("position")), e && f ? (g.offset.left = g.parentData.left + g.position.left, g.offset.top = g.parentData.top + g.position.top) : (g.offset.left = g.element.offset().left, g.offset.top = g.element.offset().top), c = Math.abs(g.sizeDiff.width + (g._helper ? g.offset.left - l.left : g.offset.left - i.left)), d = Math.abs(g.sizeDiff.height + (g._helper ? g.offset.top - l.top : g.offset.top - i.top)), c + g.size.width >= g.parentData.width && (g.size.width = g.parentData.width - c, k && (g.size.height = g.size.width / g.aspectRatio, n = !1)), d + g.size.height >= g.parentData.height && (g.size.height = g.parentData.height - d, k && (g.size.width = g.size.height * g.aspectRatio, n = !1)), n || (g.position.left = g.prevPosition.left, g.position.top = g.prevPosition.top, g.size.width = g.prevSize.width, g.size.height = g.prevSize.height)
        }, stop: function () {
            var b = a(this).resizable("instance"), c = b.options, d = b.containerOffset, e = b.containerPosition, f = b.containerElement, g = a(b.helper), h = g.offset(), i = g.outerWidth() - b.sizeDiff.width, j = g.outerHeight() - b.sizeDiff.height;
            b._helper && !c.animate && /relative/.test(f.css("position")) && a(this).css({
                left: h.left - e.left - d.left,
                width: i,
                height: j
            }), b._helper && !c.animate && /static/.test(f.css("position")) && a(this).css({
                left: h.left - e.left - d.left,
                width: i,
                height: j
            })
        }
    }), a.ui.plugin.add("resizable", "alsoResize", {
        start: function () {
            var b = a(this).resizable("instance"), c = b.options, d = function (b) {
                a(b).each(function () {
                    var b = a(this);
                    b.data("ui-resizable-alsoresize", {
                        width: parseInt(b.width(), 10),
                        height: parseInt(b.height(), 10),
                        left: parseInt(b.css("left"), 10),
                        top: parseInt(b.css("top"), 10)
                    })
                })
            };
            "object" != typeof c.alsoResize || c.alsoResize.parentNode ? d(c.alsoResize) : c.alsoResize.length ? (c.alsoResize = c.alsoResize[0], d(c.alsoResize)) : a.each(c.alsoResize, function (a) {
                d(a)
            })
        }, resize: function (b, c) {
            var d = a(this).resizable("instance"), e = d.options, f = d.originalSize, g = d.originalPosition, h = {
                height: d.size.height - f.height || 0,
                width: d.size.width - f.width || 0,
                top: d.position.top - g.top || 0,
                left: d.position.left - g.left || 0
            }, i = function (b, d) {
                a(b).each(function () {
                    var b = a(this), e = a(this).data("ui-resizable-alsoresize"), f = {}, g = d && d.length ? d : b.parents(c.originalElement[0]).length ? ["width", "height"] : ["width", "height", "top", "left"];
                    a.each(g, function (a, b) {
                        var c = (e[b] || 0) + (h[b] || 0);
                        c && c >= 0 && (f[b] = c || null)
                    }), b.css(f)
                })
            };
            "object" != typeof e.alsoResize || e.alsoResize.nodeType ? i(e.alsoResize) : a.each(e.alsoResize, function (a, b) {
                i(a, b)
            })
        }, stop: function () {
            a(this).removeData("resizable-alsoresize")
        }
    }), a.ui.plugin.add("resizable", "ghost", {
        start: function () {
            var b = a(this).resizable("instance"), c = b.options, d = b.size;
            b.ghost = b.originalElement.clone(), b.ghost.css({
                opacity: .25,
                display: "block",
                position: "relative",
                height: d.height,
                width: d.width,
                margin: 0,
                left: 0,
                top: 0
            }).addClass("ui-resizable-ghost").addClass("string" == typeof c.ghost ? c.ghost : ""), b.ghost.appendTo(b.helper)
        }, resize: function () {
            var b = a(this).resizable("instance");
            b.ghost && b.ghost.css({position: "relative", height: b.size.height, width: b.size.width})
        }, stop: function () {
            var b = a(this).resizable("instance");
            b.ghost && b.helper && b.helper.get(0).removeChild(b.ghost.get(0))
        }
    }), a.ui.plugin.add("resizable", "grid", {
        resize: function () {
            var b, c = a(this).resizable("instance"), d = c.options, e = c.size, f = c.originalSize, g = c.originalPosition, h = c.axis, i = "number" == typeof d.grid ? [d.grid, d.grid] : d.grid, j = i[0] || 1, k = i[1] || 1, l = Math.round((e.width - f.width) / j) * j, m = Math.round((e.height - f.height) / k) * k, n = f.width + l, o = f.height + m, p = d.maxWidth && n > d.maxWidth, q = d.maxHeight && o > d.maxHeight, r = d.minWidth && d.minWidth > n, s = d.minHeight && d.minHeight > o;
            d.grid = i, r && (n += j), s && (o += k), p && (n -= j), q && (o -= k), /^(se|s|e)$/.test(h) ? (c.size.width = n, c.size.height = o) : /^(ne)$/.test(h) ? (c.size.width = n, c.size.height = o, c.position.top = g.top - m) : /^(sw)$/.test(h) ? (c.size.width = n, c.size.height = o, c.position.left = g.left - l) : ((0 >= o - k || 0 >= n - j) && (b = c._getPaddingPlusBorderDimensions(this)), o - k > 0 ? (c.size.height = o, c.position.top = g.top - m) : (o = k - b.height, c.size.height = o, c.position.top = g.top + f.height - o), n - j > 0 ? (c.size.width = n, c.position.left = g.left - l) : (n = k - b.height, c.size.width = n, c.position.left = g.left + f.width - n))
        }
    }), a.ui.resizable
}), function (a) {
    "function" == typeof define && define.amd ? define(["jquery", "/core", "/mouse", "/widget"], a) : a(jQuery)
}(function (a) {
    return a.widget("ui.selectable", a.ui.mouse, {
        version: "1.11.2",
        options: {
            appendTo: "body",
            autoRefresh: !0,
            distance: 0,
            filter: "*",
            tolerance: "touch",
            selected: null,
            selecting: null,
            start: null,
            stop: null,
            unselected: null,
            unselecting: null
        },
        _create: function () {
            var b, c = this;
            this.element.addClass("ui-selectable"), this.dragged = !1, this.refresh = function () {
                b = a(c.options.filter, c.element[0]), b.addClass("ui-selectee"), b.each(function () {
                    var b = a(this), c = b.offset();
                    a.data(this, "selectable-item", {
                        element: this,
                        $element: b,
                        left: c.left,
                        top: c.top,
                        right: c.left + b.outerWidth(),
                        bottom: c.top + b.outerHeight(),
                        startselected: !1,
                        selected: b.hasClass("ui-selected"),
                        selecting: b.hasClass("ui-selecting"),
                        unselecting: b.hasClass("ui-unselecting")
                    })
                })
            }, this.refresh(), this.selectees = b.addClass("ui-selectee"), this._mouseInit(), this.helper = a("<div class='ui-selectable-helper'></div>")
        },
        _destroy: function () {
            this.selectees.removeClass("ui-selectee").removeData("selectable-item"), this.element.removeClass("ui-selectable ui-selectable-disabled"), this._mouseDestroy()
        },
        _mouseStart: function (b) {
            var c = this, d = this.options;
            this.opos = [b.pageX, b.pageY], this.options.disabled || (this.selectees = a(d.filter, this.element[0]), this._trigger("start", b), a(d.appendTo).append(this.helper), this.helper.css({
                left: b.pageX,
                top: b.pageY,
                width: 0,
                height: 0
            }), d.autoRefresh && this.refresh(), this.selectees.filter(".ui-selected").each(function () {
                var d = a.data(this, "selectable-item");
                d.startselected = !0, b.metaKey || b.ctrlKey || (d.$element.removeClass("ui-selected"), d.selected = !1, d.$element.addClass("ui-unselecting"), d.unselecting = !0, c._trigger("unselecting", b, {unselecting: d.element}))
            }), a(b.target).parents().addBack().each(function () {
                var d, e = a.data(this, "selectable-item");
                return e ? (d = !b.metaKey && !b.ctrlKey || !e.$element.hasClass("ui-selected"), e.$element.removeClass(d ? "ui-unselecting" : "ui-selected").addClass(d ? "ui-selecting" : "ui-unselecting"), e.unselecting = !d, e.selecting = d, e.selected = d, d ? c._trigger("selecting", b, {selecting: e.element}) : c._trigger("unselecting", b, {unselecting: e.element}), !1) : void 0
            }))
        },
        _mouseDrag: function (b) {
            if (this.dragged = !0, !this.options.disabled) {
                var c, d = this, e = this.options, f = this.opos[0], g = this.opos[1], h = b.pageX, i = b.pageY;
                return f > h && (c = h, h = f, f = c), g > i && (c = i, i = g, g = c), this.helper.css({
                    left: f,
                    top: g,
                    width: h - f,
                    height: i - g
                }), this.selectees.each(function () {
                    var c = a.data(this, "selectable-item"), j = !1;
                    c && c.element !== d.element[0] && ("touch" === e.tolerance ? j = !(c.left > h || f > c.right || c.top > i || g > c.bottom) : "fit" === e.tolerance && (j = c.left > f && h > c.right && c.top > g && i > c.bottom), j ? (c.selected && (c.$element.removeClass("ui-selected"), c.selected = !1), c.unselecting && (c.$element.removeClass("ui-unselecting"), c.unselecting = !1), c.selecting || (c.$element.addClass("ui-selecting"), c.selecting = !0, d._trigger("selecting", b, {selecting: c.element}))) : (c.selecting && ((b.metaKey || b.ctrlKey) && c.startselected ? (c.$element.removeClass("ui-selecting"), c.selecting = !1, c.$element.addClass("ui-selected"), c.selected = !0) : (c.$element.removeClass("ui-selecting"), c.selecting = !1, c.startselected && (c.$element.addClass("ui-unselecting"), c.unselecting = !0), d._trigger("unselecting", b, {unselecting: c.element}))), c.selected && (b.metaKey || b.ctrlKey || c.startselected || (c.$element.removeClass("ui-selected"), c.selected = !1, c.$element.addClass("ui-unselecting"), c.unselecting = !0, d._trigger("unselecting", b, {unselecting: c.element})))))
                }), !1
            }
        },
        _mouseStop: function (b) {
            var c = this;
            return this.dragged = !1, a(".ui-unselecting", this.element[0]).each(function () {
                var d = a.data(this, "selectable-item");
                d.$element.removeClass("ui-unselecting"), d.unselecting = !1, d.startselected = !1, c._trigger("unselected", b, {unselected: d.element})
            }), a(".ui-selecting", this.element[0]).each(function () {
                var d = a.data(this, "selectable-item");
                d.$element.removeClass("ui-selecting").addClass("ui-selected"), d.selecting = !1, d.selected = !0, d.startselected = !0, c._trigger("selected", b, {selected: d.element})
            }), this._trigger("stop", b), this.helper.remove(), !1
        }
    })
}), function (a) {
    "function" == typeof define && define.amd ? define(["jquery", "/core", "/mouse", "/widget"], a) : a(jQuery)
}(function (a) {
    return a.widget("ui.sortable", a.ui.mouse, {
        version: "1.11.2",
        widgetEventPrefix: "sort",
        ready: !1,
        options: {
            appendTo: "parent",
            axis: !1,
            connectWith: !1,
            containment: !1,
            cursor: "auto",
            cursorAt: !1,
            dropOnEmpty: !0,
            forcePlaceholderSize: !1,
            forceHelperSize: !1,
            grid: !1,
            handle: !1,
            helper: "original",
            items: "> *",
            opacity: !1,
            placeholder: !1,
            revert: !1,
            scroll: !0,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            scope: "default",
            tolerance: "intersect",
            zIndex: 1e3,
            activate: null,
            beforeStop: null,
            change: null,
            deactivate: null,
            out: null,
            over: null,
            receive: null,
            remove: null,
            sort: null,
            start: null,
            stop: null,
            update: null
        },
        _isOverAxis: function (a, b, c) {
            return a >= b && b + c > a
        },
        _isFloating: function (a) {
            return /left|right/.test(a.css("float")) || /inline|table-cell/.test(a.css("display"))
        },
        _create: function () {
            var a = this.options;
            this.containerCache = {}, this.element.addClass("ui-sortable"), this.refresh(), this.floating = this.items.length ? "x" === a.axis || this._isFloating(this.items[0].item) : !1, this.offset = this.element.offset(), this._mouseInit(), this._setHandleClassName(), this.ready = !0
        },
        _setOption: function (a, b) {
            this._super(a, b), "handle" === a && this._setHandleClassName()
        },
        _setHandleClassName: function () {
            this.element.find(".ui-sortable-handle").removeClass("ui-sortable-handle"), a.each(this.items, function () {
                (this.instance.options.handle ? this.item.find(this.instance.options.handle) : this.item).addClass("ui-sortable-handle")
            })
        },
        _destroy: function () {
            this.element.removeClass("ui-sortable ui-sortable-disabled").find(".ui-sortable-handle").removeClass("ui-sortable-handle"), this._mouseDestroy();
            for (var a = this.items.length - 1; a >= 0; a--)this.items[a].item.removeData(this.widgetName + "-item");
            return this
        },
        _mouseCapture: function (b, c) {
            var d = null, e = !1, f = this;
            return this.reverting ? !1 : this.options.disabled || "static" === this.options.type ? !1 : (this._refreshItems(b), a(b.target).parents().each(function () {
                return a.data(this, f.widgetName + "-item") === f ? (d = a(this), !1) : void 0
            }), a.data(b.target, f.widgetName + "-item") === f && (d = a(b.target)), d && (!this.options.handle || c || (a(this.options.handle, d).find("*").addBack().each(function () {
                this === b.target && (e = !0)
            }), e)) ? (this.currentItem = d, this._removeCurrentsFromItems(), !0) : !1)
        },
        _mouseStart: function (b, c, d) {
            var e, f, g = this.options;
            if (this.currentContainer = this, this.refreshPositions(), this.helper = this._createHelper(b), this._cacheHelperProportions(), this._cacheMargins(), this.scrollParent = this.helper.scrollParent(), this.offset = this.currentItem.offset(), this.offset = {
                    top: this.offset.top - this.margins.top,
                    left: this.offset.left - this.margins.left
                }, a.extend(this.offset, {
                    click: {left: b.pageX - this.offset.left, top: b.pageY - this.offset.top},
                    parent: this._getParentOffset(),
                    relative: this._getRelativeOffset()
                }), this.helper.css("position", "absolute"), this.cssPosition = this.helper.css("position"), this.originalPosition = this._generatePosition(b), this.originalPageX = b.pageX, this.originalPageY = b.pageY, g.cursorAt && this._adjustOffsetFromHelper(g.cursorAt), this.domPosition = {
                    prev: this.currentItem.prev()[0],
                    parent: this.currentItem.parent()[0]
                }, this.helper[0] !== this.currentItem[0] && this.currentItem.hide(), this._createPlaceholder(), g.containment && this._setContainment(), g.cursor && "auto" !== g.cursor && (f = this.document.find("body"), this.storedCursor = f.css("cursor"), f.css("cursor", g.cursor), this.storedStylesheet = a("<style>*{ cursor: " + g.cursor + " !important; }</style>").appendTo(f)), g.opacity && (this.helper.css("opacity") && (this._storedOpacity = this.helper.css("opacity")), this.helper.css("opacity", g.opacity)), g.zIndex && (this.helper.css("zIndex") && (this._storedZIndex = this.helper.css("zIndex")), this.helper.css("zIndex", g.zIndex)), this.scrollParent[0] !== document && "HTML" !== this.scrollParent[0].tagName && (this.overflowOffset = this.scrollParent.offset()), this._trigger("start", b, this._uiHash()), this._preserveHelperProportions || this._cacheHelperProportions(), !d)for (e = this.containers.length - 1; e >= 0; e--)this.containers[e]._trigger("activate", b, this._uiHash(this));
            return a.ui.ddmanager && (a.ui.ddmanager.current = this), a.ui.ddmanager && !g.dropBehaviour && a.ui.ddmanager.prepareOffsets(this, b), this.dragging = !0, this.helper.addClass("ui-sortable-helper"), this._mouseDrag(b), !0
        },
        _mouseDrag: function (b) {
            var c, d, e, f, g = this.options, h = !1;
            for (this.position = this._generatePosition(b), this.positionAbs = this._convertPositionTo("absolute"), this.lastPositionAbs || (this.lastPositionAbs = this.positionAbs), this.options.scroll && (this.scrollParent[0] !== document && "HTML" !== this.scrollParent[0].tagName ? (this.overflowOffset.top + this.scrollParent[0].offsetHeight - b.pageY < g.scrollSensitivity ? this.scrollParent[0].scrollTop = h = this.scrollParent[0].scrollTop + g.scrollSpeed : b.pageY - this.overflowOffset.top < g.scrollSensitivity && (this.scrollParent[0].scrollTop = h = this.scrollParent[0].scrollTop - g.scrollSpeed), this.overflowOffset.left + this.scrollParent[0].offsetWidth - b.pageX < g.scrollSensitivity ? this.scrollParent[0].scrollLeft = h = this.scrollParent[0].scrollLeft + g.scrollSpeed : b.pageX - this.overflowOffset.left < g.scrollSensitivity && (this.scrollParent[0].scrollLeft = h = this.scrollParent[0].scrollLeft - g.scrollSpeed)) : (b.pageY - a(document).scrollTop() < g.scrollSensitivity ? h = a(document).scrollTop(a(document).scrollTop() - g.scrollSpeed) : a(window).height() - (b.pageY - a(document).scrollTop()) < g.scrollSensitivity && (h = a(document).scrollTop(a(document).scrollTop() + g.scrollSpeed)), b.pageX - a(document).scrollLeft() < g.scrollSensitivity ? h = a(document).scrollLeft(a(document).scrollLeft() - g.scrollSpeed) : a(window).width() - (b.pageX - a(document).scrollLeft()) < g.scrollSensitivity && (h = a(document).scrollLeft(a(document).scrollLeft() + g.scrollSpeed))), h !== !1 && a.ui.ddmanager && !g.dropBehaviour && a.ui.ddmanager.prepareOffsets(this, b)), this.positionAbs = this._convertPositionTo("absolute"), this.options.axis && "y" === this.options.axis || (this.helper[0].style.left = this.position.left + "px"), this.options.axis && "x" === this.options.axis || (this.helper[0].style.top = this.position.top + "px"), c = this.items.length - 1; c >= 0; c--)if (d = this.items[c], e = d.item[0], f = this._intersectsWithPointer(d), f && d.instance === this.currentContainer && e !== this.currentItem[0] && this.placeholder[1 === f ? "next" : "prev"]()[0] !== e && !a.contains(this.placeholder[0], e) && ("semi-dynamic" === this.options.type ? !a.contains(this.element[0], e) : !0)) {
                if (this.direction = 1 === f ? "down" : "up", "pointer" !== this.options.tolerance && !this._intersectsWithSides(d))break;
                this._rearrange(b, d), this._trigger("change", b, this._uiHash());
                break
            }
            return this._contactContainers(b), a.ui.ddmanager && a.ui.ddmanager.drag(this, b), this._trigger("sort", b, this._uiHash()), this.lastPositionAbs = this.positionAbs, !1
        },
        _mouseStop: function (b, c) {
            if (b) {
                if (a.ui.ddmanager && !this.options.dropBehaviour && a.ui.ddmanager.drop(this, b), this.options.revert) {
                    var d = this, e = this.placeholder.offset(), f = this.options.axis, g = {};
                    f && "x" !== f || (g.left = e.left - this.offset.parent.left - this.margins.left + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollLeft)), f && "y" !== f || (g.top = e.top - this.offset.parent.top - this.margins.top + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollTop)), this.reverting = !0, a(this.helper).animate(g, parseInt(this.options.revert, 10) || 500, function () {
                        d._clear(b)
                    })
                } else this._clear(b, c);
                return !1
            }
        },
        cancel: function () {
            if (this.dragging) {
                this._mouseUp({target: null}), "original" === this.options.helper ? this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper") : this.currentItem.show();
                for (var b = this.containers.length - 1; b >= 0; b--)this.containers[b]._trigger("deactivate", null, this._uiHash(this)), this.containers[b].containerCache.over && (this.containers[b]._trigger("out", null, this._uiHash(this)), this.containers[b].containerCache.over = 0)
            }
            return this.placeholder && (this.placeholder[0].parentNode && this.placeholder[0].parentNode.removeChild(this.placeholder[0]), "original" !== this.options.helper && this.helper && this.helper[0].parentNode && this.helper.remove(), a.extend(this, {
                helper: null,
                dragging: !1,
                reverting: !1,
                _noFinalSort: null
            }), this.domPosition.prev ? a(this.domPosition.prev).after(this.currentItem) : a(this.domPosition.parent).prepend(this.currentItem)), this
        },
        serialize: function (b) {
            var c = this._getItemsAsjQuery(b && b.connected), d = [];
            return b = b || {}, a(c).each(function () {
                var c = (a(b.item || this).attr(b.attribute || "id") || "").match(b.expression || /(.+)[\-=_](.+)/);
                c && d.push((b.key || c[1] + "[]") + "=" + (b.key && b.expression ? c[1] : c[2]))
            }), !d.length && b.key && d.push(b.key + "="), d.join("&")
        },
        toArray: function (b) {
            var c = this._getItemsAsjQuery(b && b.connected), d = [];
            return b = b || {}, c.each(function () {
                d.push(a(b.item || this).attr(b.attribute || "id") || "")
            }), d
        },
        _intersectsWith: function (a) {
            var b = this.positionAbs.left, c = b + this.helperProportions.width, d = this.positionAbs.top, e = d + this.helperProportions.height, f = a.left, g = f + a.width, h = a.top, i = h + a.height, j = this.offset.click.top, k = this.offset.click.left, l = "x" === this.options.axis || d + j > h && i > d + j, m = "y" === this.options.axis || b + k > f && g > b + k, n = l && m;
            return "pointer" === this.options.tolerance || this.options.forcePointerForContainers || "pointer" !== this.options.tolerance && this.helperProportions[this.floating ? "width" : "height"] > a[this.floating ? "width" : "height"] ? n : b + this.helperProportions.width / 2 > f && g > c - this.helperProportions.width / 2 && d + this.helperProportions.height / 2 > h && i > e - this.helperProportions.height / 2
        },
        _intersectsWithPointer: function (a) {
            var b = "x" === this.options.axis || this._isOverAxis(this.positionAbs.top + this.offset.click.top, a.top, a.height), c = "y" === this.options.axis || this._isOverAxis(this.positionAbs.left + this.offset.click.left, a.left, a.width), d = b && c, e = this._getDragVerticalDirection(), f = this._getDragHorizontalDirection();
            return d ? this.floating ? f && "right" === f || "down" === e ? 2 : 1 : e && ("down" === e ? 2 : 1) : !1
        },
        _intersectsWithSides: function (a) {
            var b = this._isOverAxis(this.positionAbs.top + this.offset.click.top, a.top + a.height / 2, a.height), c = this._isOverAxis(this.positionAbs.left + this.offset.click.left, a.left + a.width / 2, a.width), d = this._getDragVerticalDirection(), e = this._getDragHorizontalDirection();
            return this.floating && e ? "right" === e && c || "left" === e && !c : d && ("down" === d && b || "up" === d && !b)
        },
        _getDragVerticalDirection: function () {
            var a = this.positionAbs.top - this.lastPositionAbs.top;
            return 0 !== a && (a > 0 ? "down" : "up")
        },
        _getDragHorizontalDirection: function () {
            var a = this.positionAbs.left - this.lastPositionAbs.left;
            return 0 !== a && (a > 0 ? "right" : "left")
        },
        refresh: function (a) {
            return this._refreshItems(a), this._setHandleClassName(), this.refreshPositions(), this
        },
        _connectWith: function () {
            var a = this.options;
            return a.connectWith.constructor === String ? [a.connectWith] : a.connectWith
        },
        _getItemsAsjQuery: function (b) {
            function c() {
                h.push(this)
            }

            var d, e, f, g, h = [], i = [], j = this._connectWith();
            if (j && b)for (d = j.length - 1; d >= 0; d--)for (f = a(j[d]), e = f.length - 1; e >= 0; e--)g = a.data(f[e], this.widgetFullName), g && g !== this && !g.options.disabled && i.push([a.isFunction(g.options.items) ? g.options.items.call(g.element) : a(g.options.items, g.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), g]);
            for (i.push([a.isFunction(this.options.items) ? this.options.items.call(this.element, null, {
                options: this.options,
                item: this.currentItem
            }) : a(this.options.items, this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), this]), d = i.length - 1; d >= 0; d--)i[d][0].each(c);
            return a(h)
        },
        _removeCurrentsFromItems: function () {
            var b = this.currentItem.find(":data(" + this.widgetName + "-item)");
            this.items = a.grep(this.items, function (a) {
                for (var c = 0; b.length > c; c++)if (b[c] === a.item[0])return !1;
                return !0
            })
        },
        _refreshItems: function (b) {
            this.items = [], this.containers = [this];
            var c, d, e, f, g, h, i, j, k = this.items, l = [[a.isFunction(this.options.items) ? this.options.items.call(this.element[0], b, {item: this.currentItem}) : a(this.options.items, this.element), this]], m = this._connectWith();
            if (m && this.ready)for (c = m.length - 1; c >= 0; c--)for (e = a(m[c]), d = e.length - 1; d >= 0; d--)f = a.data(e[d], this.widgetFullName), f && f !== this && !f.options.disabled && (l.push([a.isFunction(f.options.items) ? f.options.items.call(f.element[0], b, {item: this.currentItem}) : a(f.options.items, f.element), f]), this.containers.push(f));
            for (c = l.length - 1; c >= 0; c--)for (g = l[c][1], h = l[c][0], d = 0, j = h.length; j > d; d++)i = a(h[d]), i.data(this.widgetName + "-item", g), k.push({
                item: i,
                instance: g,
                width: 0,
                height: 0,
                left: 0,
                top: 0
            })
        },
        refreshPositions: function (b) {
            this.offsetParent && this.helper && (this.offset.parent = this._getParentOffset());
            var c, d, e, f;
            for (c = this.items.length - 1; c >= 0; c--)d = this.items[c], d.instance !== this.currentContainer && this.currentContainer && d.item[0] !== this.currentItem[0] || (e = this.options.toleranceElement ? a(this.options.toleranceElement, d.item) : d.item, b || (d.width = e.outerWidth(), d.height = e.outerHeight()), f = e.offset(), d.left = f.left, d.top = f.top);
            if (this.options.custom && this.options.custom.refreshContainers)this.options.custom.refreshContainers.call(this); else for (c = this.containers.length - 1; c >= 0; c--)f = this.containers[c].element.offset(), this.containers[c].containerCache.left = f.left, this.containers[c].containerCache.top = f.top, this.containers[c].containerCache.width = this.containers[c].element.outerWidth(), this.containers[c].containerCache.height = this.containers[c].element.outerHeight();
            return this
        },
        _createPlaceholder: function (b) {
            b = b || this;
            var c, d = b.options;
            d.placeholder && d.placeholder.constructor !== String || (c = d.placeholder, d.placeholder = {
                element: function () {
                    var d = b.currentItem[0].nodeName.toLowerCase(), e = a("<" + d + ">", b.document[0]).addClass(c || b.currentItem[0].className + " ui-sortable-placeholder").removeClass("ui-sortable-helper");
                    return "tr" === d ? b.currentItem.children().each(function () {
                        a("<td>&#160;</td>", b.document[0]).attr("colspan", a(this).attr("colspan") || 1).appendTo(e)
                    }) : "img" === d && e.attr("src", b.currentItem.attr("src")), c || e.css("visibility", "hidden"), e
                }, update: function (a, e) {
                    (!c || d.forcePlaceholderSize) && (e.height() || e.height(b.currentItem.innerHeight() - parseInt(b.currentItem.css("paddingTop") || 0, 10) - parseInt(b.currentItem.css("paddingBottom") || 0, 10)), e.width() || e.width(b.currentItem.innerWidth() - parseInt(b.currentItem.css("paddingLeft") || 0, 10) - parseInt(b.currentItem.css("paddingRight") || 0, 10)))
                }
            }), b.placeholder = a(d.placeholder.element.call(b.element, b.currentItem)), b.currentItem.after(b.placeholder), d.placeholder.update(b, b.placeholder)
        },
        _contactContainers: function (b) {
            var c, d, e, f, g, h, i, j, k, l, m = null, n = null;
            for (c = this.containers.length - 1; c >= 0; c--)if (!a.contains(this.currentItem[0], this.containers[c].element[0]))if (this._intersectsWith(this.containers[c].containerCache)) {
                if (m && a.contains(this.containers[c].element[0], m.element[0]))continue;
                m = this.containers[c], n = c
            } else this.containers[c].containerCache.over && (this.containers[c]._trigger("out", b, this._uiHash(this)), this.containers[c].containerCache.over = 0);
            if (m)if (1 === this.containers.length)this.containers[n].containerCache.over || (this.containers[n]._trigger("over", b, this._uiHash(this)), this.containers[n].containerCache.over = 1); else {
                for (e = 1e4, f = null, k = m.floating || this._isFloating(this.currentItem), g = k ? "left" : "top", h = k ? "width" : "height", l = k ? "clientX" : "clientY", d = this.items.length - 1; d >= 0; d--)a.contains(this.containers[n].element[0], this.items[d].item[0]) && this.items[d].item[0] !== this.currentItem[0] && (i = this.items[d].item.offset()[g], j = !1, b[l] - i > this.items[d][h] / 2 && (j = !0), e > Math.abs(b[l] - i) && (e = Math.abs(b[l] - i), f = this.items[d], this.direction = j ? "up" : "down"));
                if (!f && !this.options.dropOnEmpty)return;
                if (this.currentContainer === this.containers[n])return void(this.currentContainer.containerCache.over || (this.containers[n]._trigger("over", b, this._uiHash()), this.currentContainer.containerCache.over = 1));
                f ? this._rearrange(b, f, null, !0) : this._rearrange(b, null, this.containers[n].element, !0), this._trigger("change", b, this._uiHash()), this.containers[n]._trigger("change", b, this._uiHash(this)), this.currentContainer = this.containers[n], this.options.placeholder.update(this.currentContainer, this.placeholder), this.containers[n]._trigger("over", b, this._uiHash(this)), this.containers[n].containerCache.over = 1
            }
        },
        _createHelper: function (b) {
            var c = this.options, d = a.isFunction(c.helper) ? a(c.helper.apply(this.element[0], [b, this.currentItem])) : "clone" === c.helper ? this.currentItem.clone() : this.currentItem;
            return d.parents("body").length || a("parent" !== c.appendTo ? c.appendTo : this.currentItem[0].parentNode)[0].appendChild(d[0]), d[0] === this.currentItem[0] && (this._storedCSS = {
                width: this.currentItem[0].style.width,
                height: this.currentItem[0].style.height,
                position: this.currentItem.css("position"),
                top: this.currentItem.css("top"),
                left: this.currentItem.css("left")
            }), (!d[0].style.width || c.forceHelperSize) && d.width(this.currentItem.width()), (!d[0].style.height || c.forceHelperSize) && d.height(this.currentItem.height()), d
        },
        _adjustOffsetFromHelper: function (b) {
            "string" == typeof b && (b = b.split(" ")), a.isArray(b) && (b = {
                left: +b[0],
                top: +b[1] || 0
            }), "left"in b && (this.offset.click.left = b.left + this.margins.left), "right"in b && (this.offset.click.left = this.helperProportions.width - b.right + this.margins.left), "top"in b && (this.offset.click.top = b.top + this.margins.top), "bottom"in b && (this.offset.click.top = this.helperProportions.height - b.bottom + this.margins.top)
        },
        _getParentOffset: function () {
            this.offsetParent = this.helper.offsetParent();
            var b = this.offsetParent.offset();
            return "absolute" === this.cssPosition && this.scrollParent[0] !== document && a.contains(this.scrollParent[0], this.offsetParent[0]) && (b.left += this.scrollParent.scrollLeft(), b.top += this.scrollParent.scrollTop()), (this.offsetParent[0] === document.body || this.offsetParent[0].tagName && "html" === this.offsetParent[0].tagName.toLowerCase() && a.ui.ie) && (b = {
                top: 0,
                left: 0
            }), {
                top: b.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                left: b.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
            }
        },
        _getRelativeOffset: function () {
            if ("relative" === this.cssPosition) {
                var a = this.currentItem.position();
                return {
                    top: a.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
                    left: a.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
                }
            }
            return {top: 0, left: 0}
        },
        _cacheMargins: function () {
            this.margins = {
                left: parseInt(this.currentItem.css("marginLeft"), 10) || 0,
                top: parseInt(this.currentItem.css("marginTop"), 10) || 0
            }
        },
        _cacheHelperProportions: function () {
            this.helperProportions = {width: this.helper.outerWidth(), height: this.helper.outerHeight()}
        },
        _setContainment: function () {
            var b, c, d, e = this.options;
            "parent" === e.containment && (e.containment = this.helper[0].parentNode), ("document" === e.containment || "window" === e.containment) && (this.containment = [0 - this.offset.relative.left - this.offset.parent.left, 0 - this.offset.relative.top - this.offset.parent.top, a("document" === e.containment ? document : window).width() - this.helperProportions.width - this.margins.left, (a("document" === e.containment ? document : window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]), /^(document|window|parent)$/.test(e.containment) || (b = a(e.containment)[0], c = a(e.containment).offset(), d = "hidden" !== a(b).css("overflow"), this.containment = [c.left + (parseInt(a(b).css("borderLeftWidth"), 10) || 0) + (parseInt(a(b).css("paddingLeft"), 10) || 0) - this.margins.left, c.top + (parseInt(a(b).css("borderTopWidth"), 10) || 0) + (parseInt(a(b).css("paddingTop"), 10) || 0) - this.margins.top, c.left + (d ? Math.max(b.scrollWidth, b.offsetWidth) : b.offsetWidth) - (parseInt(a(b).css("borderLeftWidth"), 10) || 0) - (parseInt(a(b).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left, c.top + (d ? Math.max(b.scrollHeight, b.offsetHeight) : b.offsetHeight) - (parseInt(a(b).css("borderTopWidth"), 10) || 0) - (parseInt(a(b).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top])
        },
        _convertPositionTo: function (b, c) {
            c || (c = this.position);
            var d = "absolute" === b ? 1 : -1, e = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && a.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent, f = /(html|body)/i.test(e[0].tagName);
            return {
                top: c.top + this.offset.relative.top * d + this.offset.parent.top * d - ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : f ? 0 : e.scrollTop()) * d,
                left: c.left + this.offset.relative.left * d + this.offset.parent.left * d - ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : f ? 0 : e.scrollLeft()) * d
            }
        },
        _generatePosition: function (b) {
            var c, d, e = this.options, f = b.pageX, g = b.pageY, h = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && a.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent, i = /(html|body)/i.test(h[0].tagName);
            return "relative" !== this.cssPosition || this.scrollParent[0] !== document && this.scrollParent[0] !== this.offsetParent[0] || (this.offset.relative = this._getRelativeOffset()), this.originalPosition && (this.containment && (b.pageX - this.offset.click.left < this.containment[0] && (f = this.containment[0] + this.offset.click.left), b.pageY - this.offset.click.top < this.containment[1] && (g = this.containment[1] + this.offset.click.top), b.pageX - this.offset.click.left > this.containment[2] && (f = this.containment[2] + this.offset.click.left), b.pageY - this.offset.click.top > this.containment[3] && (g = this.containment[3] + this.offset.click.top)), e.grid && (c = this.originalPageY + Math.round((g - this.originalPageY) / e.grid[1]) * e.grid[1], g = this.containment ? c - this.offset.click.top >= this.containment[1] && c - this.offset.click.top <= this.containment[3] ? c : c - this.offset.click.top >= this.containment[1] ? c - e.grid[1] : c + e.grid[1] : c, d = this.originalPageX + Math.round((f - this.originalPageX) / e.grid[0]) * e.grid[0], f = this.containment ? d - this.offset.click.left >= this.containment[0] && d - this.offset.click.left <= this.containment[2] ? d : d - this.offset.click.left >= this.containment[0] ? d - e.grid[0] : d + e.grid[0] : d)), {
                top: g - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : i ? 0 : h.scrollTop()),
                left: f - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : i ? 0 : h.scrollLeft())
            }
        },
        _rearrange: function (a, b, c, d) {
            c ? c[0].appendChild(this.placeholder[0]) : b.item[0].parentNode.insertBefore(this.placeholder[0], "down" === this.direction ? b.item[0] : b.item[0].nextSibling), this.counter = this.counter ? ++this.counter : 1;
            var e = this.counter;
            this._delay(function () {
                e === this.counter && this.refreshPositions(!d)
            })
        },
        _clear: function (a, b) {
            function c(a, b, c) {
                return function (d) {
                    c._trigger(a, d, b._uiHash(b))
                }
            }

            this.reverting = !1;
            var d, e = [];
            if (!this._noFinalSort && this.currentItem.parent().length && this.placeholder.before(this.currentItem), this._noFinalSort = null, this.helper[0] === this.currentItem[0]) {
                for (d in this._storedCSS)("auto" === this._storedCSS[d] || "static" === this._storedCSS[d]) && (this._storedCSS[d] = "");
                this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")
            } else this.currentItem.show();
            for (this.fromOutside && !b && e.push(function (a) {
                this._trigger("receive", a, this._uiHash(this.fromOutside))
            }), !this.fromOutside && this.domPosition.prev === this.currentItem.prev().not(".ui-sortable-helper")[0] && this.domPosition.parent === this.currentItem.parent()[0] || b || e.push(function (a) {
                this._trigger("update", a, this._uiHash())
            }), this !== this.currentContainer && (b || (e.push(function (a) {
                this._trigger("remove", a, this._uiHash())
            }), e.push(function (a) {
                return function (b) {
                    a._trigger("receive", b, this._uiHash(this))
                }
            }.call(this, this.currentContainer)), e.push(function (a) {
                return function (b) {
                    a._trigger("update", b, this._uiHash(this))
                }
            }.call(this, this.currentContainer)))), d = this.containers.length - 1; d >= 0; d--)b || e.push(c("deactivate", this, this.containers[d])), this.containers[d].containerCache.over && (e.push(c("out", this, this.containers[d])), this.containers[d].containerCache.over = 0);
            if (this.storedCursor && (this.document.find("body").css("cursor", this.storedCursor), this.storedStylesheet.remove()), this._storedOpacity && this.helper.css("opacity", this._storedOpacity), this._storedZIndex && this.helper.css("zIndex", "auto" === this._storedZIndex ? "" : this._storedZIndex), this.dragging = !1, b || this._trigger("beforeStop", a, this._uiHash()), this.placeholder[0].parentNode.removeChild(this.placeholder[0]), this.cancelHelperRemoval || (this.helper[0] !== this.currentItem[0] && this.helper.remove(), this.helper = null), !b) {
                for (d = 0; e.length > d; d++)e[d].call(this, a);
                this._trigger("stop", a, this._uiHash())
            }
            return this.fromOutside = !1, !this.cancelHelperRemoval
        },
        _trigger: function () {
            a.Widget.prototype._trigger.apply(this, arguments) === !1 && this.cancel()
        },
        _uiHash: function (b) {
            var c = b || this;
            return {
                helper: c.helper,
                placeholder: c.placeholder || a([]),
                position: c.position,
                originalPosition: c.originalPosition,
                offset: c.positionAbs,
                item: c.currentItem,
                sender: b ? b.element : null
            }
        }
    })
}), function (a) {
    "function" == typeof define && define.amd ? define(["jquery", "/core", "/mouse", "/widget"], a) : a(jQuery)
}(function (a) {
    return a.widget("ui.slider", a.ui.mouse, {
        version: "1.11.2",
        widgetEventPrefix: "slide",
        options: {
            animate: !1,
            distance: 0,
            max: 100,
            min: 0,
            orientation: "horizontal",
            range: !1,
            step: 1,
            value: 0,
            values: null,
            change: null,
            slide: null,
            start: null,
            stop: null
        },
        numPages: 5,
        _create: function () {
            this._keySliding = !1, this._mouseSliding = !1, this._animateOff = !0, this._handleIndex = null, this._detectOrientation(), this._mouseInit(), this._calculateNewMax(), this.element.addClass("ui-slider ui-slider-" + this.orientation + " ui-widget ui-widget-content ui-corner-all"), this._refresh(), this._setOption("disabled", this.options.disabled), this._animateOff = !1
        },
        _refresh: function () {
            this._createRange(), this._createHandles(), this._setupEvents(), this._refreshValue()
        },
        _createHandles: function () {
            var b, c, d = this.options, e = this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"), f = "<span class='ui-slider-handle ui-state-default ui-corner-all' tabindex='0'></span>", g = [];
            for (c = d.values && d.values.length || 1, e.length > c && (e.slice(c).remove(), e = e.slice(0, c)), b = e.length; c > b; b++)g.push(f);
            this.handles = e.add(a(g.join("")).appendTo(this.element)), this.handle = this.handles.eq(0), this.handles.each(function (b) {
                a(this).data("ui-slider-handle-index", b)
            })
        },
        _createRange: function () {
            var b = this.options, c = "";
            b.range ? (b.range === !0 && (b.values ? b.values.length && 2 !== b.values.length ? b.values = [b.values[0], b.values[0]] : a.isArray(b.values) && (b.values = b.values.slice(0)) : b.values = [this._valueMin(), this._valueMin()]), this.range && this.range.length ? this.range.removeClass("ui-slider-range-min ui-slider-range-max").css({
                left: "",
                bottom: ""
            }) : (this.range = a("<div></div>").appendTo(this.element), c = "ui-slider-range ui-widget-header ui-corner-all"), this.range.addClass(c + ("min" === b.range || "max" === b.range ? " ui-slider-range-" + b.range : ""))) : (this.range && this.range.remove(), this.range = null)
        },
        _setupEvents: function () {
            this._off(this.handles), this._on(this.handles, this._handleEvents), this._hoverable(this.handles), this._focusable(this.handles)
        },
        _destroy: function () {
            this.handles.remove(), this.range && this.range.remove(), this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-widget ui-widget-content ui-corner-all"), this._mouseDestroy()
        },
        _mouseCapture: function (b) {
            var c, d, e, f, g, h, i, j, k = this, l = this.options;
            return l.disabled ? !1 : (this.elementSize = {
                width: this.element.outerWidth(),
                height: this.element.outerHeight()
            }, this.elementOffset = this.element.offset(), c = {
                x: b.pageX,
                y: b.pageY
            }, d = this._normValueFromMouse(c), e = this._valueMax() - this._valueMin() + 1, this.handles.each(function (b) {
                var c = Math.abs(d - k.values(b));
                (e > c || e === c && (b === k._lastChangedValue || k.values(b) === l.min)) && (e = c, f = a(this), g = b)
            }), h = this._start(b, g), h === !1 ? !1 : (this._mouseSliding = !0, this._handleIndex = g, f.addClass("ui-state-active").focus(), i = f.offset(), j = !a(b.target).parents().addBack().is(".ui-slider-handle"), this._clickOffset = j ? {
                left: 0,
                top: 0
            } : {
                left: b.pageX - i.left - f.width() / 2,
                top: b.pageY - i.top - f.height() / 2 - (parseInt(f.css("borderTopWidth"), 10) || 0) - (parseInt(f.css("borderBottomWidth"), 10) || 0) + (parseInt(f.css("marginTop"), 10) || 0)
            }, this.handles.hasClass("ui-state-hover") || this._slide(b, g, d), this._animateOff = !0, !0))
        },
        _mouseStart: function () {
            return !0
        },
        _mouseDrag: function (a) {
            var b = {x: a.pageX, y: a.pageY}, c = this._normValueFromMouse(b);
            return this._slide(a, this._handleIndex, c), !1
        },
        _mouseStop: function (a) {
            return this.handles.removeClass("ui-state-active"), this._mouseSliding = !1, this._stop(a, this._handleIndex), this._change(a, this._handleIndex), this._handleIndex = null, this._clickOffset = null, this._animateOff = !1, !1
        },
        _detectOrientation: function () {
            this.orientation = "vertical" === this.options.orientation ? "vertical" : "horizontal"
        },
        _normValueFromMouse: function (a) {
            var b, c, d, e, f;
            return "horizontal" === this.orientation ? (b = this.elementSize.width, c = a.x - this.elementOffset.left - (this._clickOffset ? this._clickOffset.left : 0)) : (b = this.elementSize.height, c = a.y - this.elementOffset.top - (this._clickOffset ? this._clickOffset.top : 0)), d = c / b, d > 1 && (d = 1), 0 > d && (d = 0), "vertical" === this.orientation && (d = 1 - d), e = this._valueMax() - this._valueMin(), f = this._valueMin() + d * e, this._trimAlignValue(f)
        },
        _start: function (a, b) {
            var c = {handle: this.handles[b], value: this.value()};
            return this.options.values && this.options.values.length && (c.value = this.values(b), c.values = this.values()), this._trigger("start", a, c)
        },
        _slide: function (a, b, c) {
            var d, e, f;
            this.options.values && this.options.values.length ? (d = this.values(b ? 0 : 1), 2 === this.options.values.length && this.options.range === !0 && (0 === b && c > d || 1 === b && d > c) && (c = d), c !== this.values(b) && (e = this.values(), e[b] = c, f = this._trigger("slide", a, {
                handle: this.handles[b],
                value: c,
                values: e
            }), d = this.values(b ? 0 : 1), f !== !1 && this.values(b, c))) : c !== this.value() && (f = this._trigger("slide", a, {
                handle: this.handles[b],
                value: c
            }), f !== !1 && this.value(c))
        },
        _stop: function (a, b) {
            var c = {handle: this.handles[b], value: this.value()};
            this.options.values && this.options.values.length && (c.value = this.values(b), c.values = this.values()), this._trigger("stop", a, c)
        },
        _change: function (a, b) {
            if (!this._keySliding && !this._mouseSliding) {
                var c = {handle: this.handles[b], value: this.value()};
                this.options.values && this.options.values.length && (c.value = this.values(b), c.values = this.values()), this._lastChangedValue = b, this._trigger("change", a, c)
            }
        },
        value: function (a) {
            return arguments.length ? (this.options.value = this._trimAlignValue(a), this._refreshValue(), void this._change(null, 0)) : this._value()
        },
        values: function (b, c) {
            var d, e, f;
            if (arguments.length > 1)return this.options.values[b] = this._trimAlignValue(c), this._refreshValue(), void this._change(null, b);
            if (!arguments.length)return this._values();
            if (!a.isArray(arguments[0]))return this.options.values && this.options.values.length ? this._values(b) : this.value();
            for (d = this.options.values, e = arguments[0], f = 0; d.length > f; f += 1)d[f] = this._trimAlignValue(e[f]), this._change(null, f);
            this._refreshValue()
        },
        _setOption: function (b, c) {
            var d, e = 0;
            switch ("range" === b && this.options.range === !0 && ("min" === c ? (this.options.value = this._values(0), this.options.values = null) : "max" === c && (this.options.value = this._values(this.options.values.length - 1), this.options.values = null)), a.isArray(this.options.values) && (e = this.options.values.length), "disabled" === b && this.element.toggleClass("ui-state-disabled", !!c), this._super(b, c), b) {
                case"orientation":
                    this._detectOrientation(), this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-" + this.orientation), this._refreshValue(), this.handles.css("horizontal" === c ? "bottom" : "left", "");
                    break;
                case"value":
                    this._animateOff = !0, this._refreshValue(), this._change(null, 0), this._animateOff = !1;
                    break;
                case"values":
                    for (this._animateOff = !0, this._refreshValue(), d = 0; e > d; d += 1)this._change(null, d);
                    this._animateOff = !1;
                    break;
                case"step":
                case"min":
                case"max":
                    this._animateOff = !0, this._calculateNewMax(), this._refreshValue(), this._animateOff = !1;
                    break;
                case"range":
                    this._animateOff = !0, this._refresh(), this._animateOff = !1
            }
        },
        _value: function () {
            var a = this.options.value;
            return a = this._trimAlignValue(a)
        },
        _values: function (a) {
            var b, c, d;
            if (arguments.length)return b = this.options.values[a], b = this._trimAlignValue(b);
            if (this.options.values && this.options.values.length) {
                for (c = this.options.values.slice(), d = 0; c.length > d; d += 1)c[d] = this._trimAlignValue(c[d]);
                return c
            }
            return []
        },
        _trimAlignValue: function (a) {
            if (this._valueMin() >= a)return this._valueMin();
            if (a >= this._valueMax())return this._valueMax();
            var b = this.options.step > 0 ? this.options.step : 1, c = (a - this._valueMin()) % b, d = a - c;
            return 2 * Math.abs(c) >= b && (d += c > 0 ? b : -b), parseFloat(d.toFixed(5))
        },
        _calculateNewMax: function () {
            var a = (this.options.max - this._valueMin()) % this.options.step;
            this.max = this.options.max - a
        },
        _valueMin: function () {
            return this.options.min
        },
        _valueMax: function () {
            return this.max
        },
        _refreshValue: function () {
            var b, c, d, e, f, g = this.options.range, h = this.options, i = this, j = this._animateOff ? !1 : h.animate, k = {};
            this.options.values && this.options.values.length ? this.handles.each(function (d) {
                c = 100 * ((i.values(d) - i._valueMin()) / (i._valueMax() - i._valueMin())), k["horizontal" === i.orientation ? "left" : "bottom"] = c + "%", a(this).stop(1, 1)[j ? "animate" : "css"](k, h.animate), i.options.range === !0 && ("horizontal" === i.orientation ? (0 === d && i.range.stop(1, 1)[j ? "animate" : "css"]({left: c + "%"}, h.animate), 1 === d && i.range[j ? "animate" : "css"]({width: c - b + "%"}, {
                    queue: !1,
                    duration: h.animate
                })) : (0 === d && i.range.stop(1, 1)[j ? "animate" : "css"]({bottom: c + "%"}, h.animate), 1 === d && i.range[j ? "animate" : "css"]({height: c - b + "%"}, {
                    queue: !1,
                    duration: h.animate
                }))), b = c
            }) : (d = this.value(), e = this._valueMin(), f = this._valueMax(), c = f !== e ? 100 * ((d - e) / (f - e)) : 0, k["horizontal" === this.orientation ? "left" : "bottom"] = c + "%", this.handle.stop(1, 1)[j ? "animate" : "css"](k, h.animate), "min" === g && "horizontal" === this.orientation && this.range.stop(1, 1)[j ? "animate" : "css"]({width: c + "%"}, h.animate), "max" === g && "horizontal" === this.orientation && this.range[j ? "animate" : "css"]({width: 100 - c + "%"}, {
                queue: !1,
                duration: h.animate
            }), "min" === g && "vertical" === this.orientation && this.range.stop(1, 1)[j ? "animate" : "css"]({height: c + "%"}, h.animate), "max" === g && "vertical" === this.orientation && this.range[j ? "animate" : "css"]({height: 100 - c + "%"}, {
                queue: !1,
                duration: h.animate
            }))
        },
        _handleEvents: {
            keydown: function (b) {
                var c, d, e, f, g = a(b.target).data("ui-slider-handle-index");
                switch (b.keyCode) {
                    case a.ui.keyCode.HOME:
                    case a.ui.keyCode.END:
                    case a.ui.keyCode.PAGE_UP:
                    case a.ui.keyCode.PAGE_DOWN:
                    case a.ui.keyCode.UP:
                    case a.ui.keyCode.RIGHT:
                    case a.ui.keyCode.DOWN:
                    case a.ui.keyCode.LEFT:
                        if (b.preventDefault(), !this._keySliding && (this._keySliding = !0, a(b.target).addClass("ui-state-active"), c = this._start(b, g), c === !1))return
                }
                switch (f = this.options.step, d = e = this.options.values && this.options.values.length ? this.values(g) : this.value(), b.keyCode) {
                    case a.ui.keyCode.HOME:
                        e = this._valueMin();
                        break;
                    case a.ui.keyCode.END:
                        e = this._valueMax();
                        break;
                    case a.ui.keyCode.PAGE_UP:
                        e = this._trimAlignValue(d + (this._valueMax() - this._valueMin()) / this.numPages);
                        break;
                    case a.ui.keyCode.PAGE_DOWN:
                        e = this._trimAlignValue(d - (this._valueMax() - this._valueMin()) / this.numPages);
                        break;
                    case a.ui.keyCode.UP:
                    case a.ui.keyCode.RIGHT:
                        if (d === this._valueMax())return;
                        e = this._trimAlignValue(d + f);
                        break;
                    case a.ui.keyCode.DOWN:
                    case a.ui.keyCode.LEFT:
                        if (d === this._valueMin())return;
                        e = this._trimAlignValue(d - f)
                }
                this._slide(b, g, e)
            }, keyup: function (b) {
                var c = a(b.target).data("ui-slider-handle-index");
                this._keySliding && (this._keySliding = !1, this._stop(b, c), this._change(b, c), a(b.target).removeClass("ui-state-active"))
            }
        }
    })
});