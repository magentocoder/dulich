<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package base-wp
 */
$base_url = get_site_url();
global $wp;
$current_url = home_url($wp->request);
?>
<script>
    jQuery(document).ready(function ($) {
        <?php if (!is_user_logged_in()): ?>
        $(".addtowishlist").live('click', function () {
            var w = window.innerWidth;
            if(w < 991){
                window.location.href = "<?php echo $base_url.'/dang-nhap-mobile' ?>";
            }else{
                var parent = $(".form-login").closest('.selectric-items');
                parent.show().find('.register-form').hide().end();
                $("html, body").animate({scrollTop: 0}, "slow");
                return false;
            }
        });
        <?php else:?>
        $(".addtowishlist").live('click', function () {
            var product_id = $(this).attr('data-id');
            var product_type = $(this).attr('data-type');
            var action = $(this).attr('data-action');
            <?php

            $user_id = get_current_user_id();

            $wishlists = YITH_WCWL()->get_wishlists(array('user_id' => $user_id, 'is_default' => 1));
            if (!empty($wishlists) && isset($wishlists[0])) {
                $wishlist_id = $wishlists[0]['wishlist_token'];
            } else {
                $wishlist_id = false;
            }
            ?>
            $.ajax({
                method: 'post',
                data: {
                    add_to_wishlist: product_id,
                    product_type: product_type,
                    action: action,
                    wishlist_id: '<?php echo $wishlists[0]->ID?>',
                    wishlist_token: '<?php echo $wishlist_id?>'
                },
                url: '<?php echo $base_url . "/wp-admin/admin-ajax.php";?>',
                success: function (re) {
                    if (re.result == "true" && action == 'add_to_wishlist') {
                        alert('Thêm vào mục ưu thích thành công');
                        $(this).find('.text-button').text('Xóa khỏi danh sách');
                        $(this).attr('data-action', 'remove_from_wishlist');
                        $(this).removeClass('addtowishlist').addClass('rmtowishlist');
                        window.location.reload();
                    }
                }
            });
        });

        $(".rmtowishlist").on('click', function () {
            var product_id = $(this).attr('data-id');
            var product_type = $(this).attr('data-type');
            var action = $(this).attr('data-action');
            <?php

            $user_id = get_current_user_id();

            $wishlists = YITH_WCWL()->get_wishlists(array('user_id' => $user_id, 'is_default' => 1));

            if (!empty($wishlists) && isset($wishlists[0])) {
                $wishlist_id = $wishlists[0]['wishlist_token'];
            } else {
                $wishlist_id = false;
            }
            ?>
            $.ajax({
                method: 'post',
                data: {
                    remove_from_wishlist: product_id,
                    product_type: product_type,
                    action: action,
                    wishlist_id: '<?php echo $wishlists[0]->ID?>',
                    wishlist_token: '<?php echo $wishlist_id?>'
                },
                url: '<?php echo $base_url . "/wp-admin/admin-ajax.php";?>',
                success: function (re) {
                    alert(' Xóa thành công');
                    $(this).attr('data-action', 'add_to_wishlist');
                    $(this).removeClass('rmtowishlist').addClass('addtowishlist');
                    $(this).find('.text-button').text('Lưu vào danh sách');
                    window.location.reload();
                }
            });
        });
        <?php endif;?>
    });
</script>
<footer>
    <div class="footer-top-contain col-xs-12 col-lg-12">
        <div class="container">
            <div class="newsLetter-warrap">
                <div class="col-lg-6 col-xs-12">
                    <div class="row">
                        <h6 class="text-title-newslatter"><?php _e('[:en]Want to be notified special price? Sign up now to receive newsletters from us[:vi]Bạn muốn được thông báo giá đặc biệt? Đăng kí ngay để nhận bản tin từ chúng tôi[:]') ?></h6>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="row">
                        <form action="<?php echo $base_url ?>/?na=s" method="post" class="form-horizontal"
                              id="ajax-newsletter-footer"
                              role="form">
                            <div class="form-group">
                                <div class="col-sm-9 form-newslatter">
                                    <div class="row">
                                        <input type="email" placeholder="Địa chỉ Email..." name="ne"
                                               class="email-newslatter" required>
                                        <input type="submit" value="Đăng ký"
                                               class="btn btn-register-newslatter ajax-newsletter-footer">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="category-bottom">
                <div class="col-lg-12 col-xs-12">
                    <div class="row">
                        <div class="locator-dropdown">
                            <?php

                            $current_language = get_bloginfo('language');
                            if ($current_language == 'vi') {
                                $label = 'VND';
                                $img = '/image/Flag-of-VN.ico';
                            } else {
                                $label = 'EN';
                                $img = '/image/british-flag.jpg';
                            }
                            ?>
                            <div class="selectric">
                                <span class="label-language">
                                    <img src="<?php echo get_stylesheet_directory_uri() . $img; ?>" alt="#"
                                         title="#" class="image-logo">
                                    <?php echo $label; ?>
                                </span>
                                <i class="fas fa-angle-down"></i>
                            </div>
                            <div class="selectric-items" tabindex="-1" style="display: none">
                                <div class="selectric-scroll">
                                    <ul>
                                        <li data-index="0" class="selected highlighted">
                                            <a href="<?php echo $base_url . DIRECTORY_SEPARATOR . 'vi'; ?>" title="#">
                                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/Flag-of-VN.ico"
                                                     alt="#" title="#" class="image-logo">
                                                <span>Việt Nam</span>
                                            </a>
                                        </li>
                                        <li data-index="1" class="last">
                                            <a href="<?php echo $base_url . DIRECTORY_SEPARATOR . 'en'; ?>" title="">
                                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/british-flag.jpg"
                                                     alt="#" title="#" class="image-logo">
                                                <span>England</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="social-muti col-xs-12 col-lg-12">
                    <div class="row">
                        <ul class="list-social-login">
                            <li class="social-item" title=""><a href="#" title="#"><i
                                            class="fab fa-facebook-f icon-social"></i></a></li>
                            <li class="social-item" title=""><a href="#" title="#"><i
                                            class="fas fa-phone-square icon-social"></i></a></li>
                            <li class="social-item" title=""><a href="#" title="#"><i
                                            class="fab fa-instagram icon-social"></i></a></li>
                            <li class="social-item" title=""><a href="#" title="#"><i
                                            class="fab fa-google-plus-g icon-social"></i></a></li>
                            <li class="social-item" title=""><a href="#" title="#"><i
                                            class="fab fa-pinterest-p icon-social"></i></a></li>
                            <li class="social-item" title=""><a href="#" title="#"><i
                                            class="fab fa-youtube icon-social"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="category-bottom-list col-lg-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-4 col-xs-12">
                            <div class="row">
                                <ul class="category-col-left">
                                    <li class="item-sub"><a href="<?php echo $base_url; ?>/2018/06/05/cong-ty/"
                                                            title="#" alt="">Công ty</a></li>
                                    <li class="item-sub"><a href="<?php echo $base_url; ?>/2018/06/05/nhan-su/"
                                                            title="Nhân sự">Nhân sự</a></li>
                                    <li class="item-sub"><a href="<?php echo $base_url; ?>/2018/06/05/co-hoi-viec-lam/"
                                                            title="Cơ hội việc làm">Cơ hội việc làm</a></li>
                                    <li class="item-sub"><a
                                                href="<?php echo $base_url; ?>/2018/06/05/ho-tro-nguoi-dung/"
                                                title="Hỗ trợ người dùng">Hỗ trợ người dùng</a>
                                    </li>
                                    <li class="item-sub"><a
                                                href="<?php echo $base_url; ?>/2018/06/05/intestor-relations/"
                                                title="Intestor Relations">Intestor Relations</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-12">
                            <div class="row">
                                <ul class="category-col-middle">
                                    <li class="item-sub"><a
                                                href="<?php echo $base_url; ?>/2018/06/05/ung-dung-di-dong-tim-kiem-moi-luc-moi-noi/"
                                                title="Ứng dụng di động - Tìm kiếm mọi lúc mọi nơi">Ứng dụng
                                            di động - Tìm kiếm mọi lúc mọi nơi</a></li>
                                    <li class="item-sub"><a
                                                href="<?php echo $base_url; ?>/2018/06/05/trekvn-hotel-manager-chu-khach-san/"
                                                title="Trekvn Hotel Manager - chủ khách sạn">Trekvn
                                            Hotel Manager - chủ khách sạn</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-12">
                            <div class="row">
                                <ul class="category-col-right">
                                    <li class="item-sub"><a
                                                href="<?php echo $base_url; ?>/2018/06/05/dieu-chinh-su-dung/"
                                                title="Điều chỉnh sử dụng">Điều chỉnh sử dụng</a>
                                    </li>
                                    <li class="item-sub"><a
                                                href="<?php echo $base_url; ?>/2018/06/05/thong-tin-phap-ly/"
                                                title="Thông tin pháp lý">Thông tin pháp lý</a>
                                    </li>
                                    <li class="item-sub"><a
                                                href="<?php echo $base_url; ?>/2018/06/05/chinh-sach-bao-mat/"
                                                title="Chính sách bảo mật">Chính sách bảo mật</a>
                                    </li>
                                    <li class="item-sub"><a href="<?php echo $base_url; ?>/2018/06/05/so-do-website/"
                                                            title="Sơ đồ website">Sơ đồ website</a></li>
                                    <li class="item-sub"><a
                                                href="<?php echo $base_url; ?>/2018/06/05/chinh-sach-cookie/"
                                                title="Chính sách cookie">Chính sách cookie</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom col-lg-12 col-xs-12">
        <div class="container">
            <div class="row">
                <div class="logo-image-footer">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/logo.jpg" title="" class="">
                </div>
                <div class="copyright">
                    <div class="col-xs-6 col-lg-6">
                        <div class="row">
                            <h6 class="copyright-left">Copyright 2018 trekvn</h6>
                        </div>
                    </div>
                    <div class="col-xs-6 col-lg-6">
                        <div class="row">
                            <h6 class="copyright-right"><?php _e('[:en]Reservation of copyright.[:vi]Bảo lưu bản quyền.[:]') ?></h6>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>
<div id="loading-image"></div>
<script>
    jQuery(window).on('load', function () {
        <?php if(isset($_GET['comment']) && $_GET['comment'] == 's'): ?>
        alert("<?php _e('[:en]Thank you for the review! Please wait for feedback from the system[:vi]Cảm ơn bạn đã nhận xét. Xin chờ phản hồi của hệ thống[:]')?>");
        <?php endif;?>
    });

    jQuery(document).ready(function ($) {
        $('#ajax-newsletter-footer').on('submit', function () {
            var data = $(this).serialize();
            $.ajax({
                method: 'post',
                data: data,
                url: '<?php echo $base_url ?>/check_newsletter.php',
                success: function (re) {
                    if (parseInt(re) == 0) {
                        alert('Email nãy đã được sử dụng! Hãy nhập một email khác để đăng kí nhận thông tin.');
                        // jQuery('main  .container').prepend("<p class='message-error'>Oh! Email này đã được đăng ký.</p>");
                    } else {
                        $.ajax({
                            method: 'post',
                            data: data,
                            url: '<?php echo $base_url ?>/?na=ajaxsub',
                            success: function (re) {
                                if (re == '1') {
                                    alert('<?php echo "Bạn đã đăng ký thàng công"?>');
                                } else {
                                    alert('<?php echo "Có lỗi vui lòng thử lại"?>');
                                }
                                window.location.reload();
                            }
                        });
                    }
                }
            });
            return false;
        });


        $('#ajax-newsletter-left').on('submit', function () {
            var data = $(this).serialize();
            $.ajax({
                method: 'post',
                data: data,
                url: '<?php echo $base_url ?>/check_newsletter.php',
                success: function (re) {
                    if (parseInt(re) == 0) {
                        alert('Email nãy đã được sử dụng! Hãy nhập một email khác để đăng kí nhận thông tin.');
                        // jQuery('main  .container').prepend("<p class='message-error'>Oh! Email này đã được đăng ký.</p>");
                    } else {
                        $.ajax({
                            method: 'post',
                            data: data,
                            url: '<?php echo $base_url ?>/?na=ajaxsub',
                            success: function (re) {
                                if (re == '1') {
                                    alert('<?php echo "Bạn đã đăng ký thàng công"?>');
                                } else {
                                    alert('<?php echo "Có lỗi vui lòng thử lại"?>');
                                }
                                window.location.reload();
                            }
                        });
                    }
                }
            });
            return false;
        });

        $(".comment_tab_product").live( 'click',function () {
            var tab = $(this).attr('rel');
            var id = $(this).attr('id');
            var url = $(this).attr('data-url');
            $(".comment_contect_product").empty();
            $.ajax({
                method: 'post',
                data: {
                    id: id,
                    url: url,
                },
                url: '<?php echo $base_url ?>/comment_content.php',
                beforeSend: function() {
                    $("#loading-image").show();
                },
                success: function (re) {
                    $("#loading-image").hide();
                    $("#comment_contect_product_" + id).html(re);
                    $('.hotel_star > .stars > span > a').on('click',function () {
                        var index = $(this).index();
                        var parent_div = $(this).closest('.hotel_star');
                        parent_div.find('p').addClass('selected').end();
                        parent_div.find('a').removeClass('active').end();
                        parent_div.find('select').val(index +1);
                        $(this).addClass('active');
                        return false;
                    });
                    $('#commentform'+id).live('submit',function () {
                        var rate = $('#rating'+id).val();
                        var comment = $('#comment'+id).val();
                        var check = true;
                        if(rate ==''){
                            alert('Bạn chưa lựa chon sao');
                            check = false;
                        }
                        if(comment.trim() ==''){
                            alert('Bạn nhập bình luận');
                            check = false;
                        }
                        if (check){
                            var data =$(this).serialize();
                            $.ajax({
                                method:'post',
                                data:data,
                                url:'<?php echo get_site_url()?>/wp-comments-post-ajax.php',
                                beforeSend: function() {
                                    $("#loading-image").show();
                                },
                                success:function (re) {
                                    $("#loading-image").hide();
                                    if (re){
                                        alert(re);
                                    }else{
                                        alert("<?php _e('[:en]Thank you for the review! Please wait for feedback from the system[:vi]Cảm ơn bạn đã nhận xét. Xin chờ phản hồi của hệ thống[:]')?>");
                                        window.location.href = '<?php echo $current_url ?>';
                                    }
                                }
                            });
                        }
                        return false;
                    });

                }
            });
        });

        $("#form-login-ajax,#form-login-ajax-mobile,#form-login-ajax-company").on('submit', function () {
            var redirect_to = $(this).find('input.redirect_to').val();
            $.post({
                type: 'post',
                dataType: 'json',
                url: ADMIN_AJAX,
                data: $(this).serialize(),
                success: function (data) {
                    if (data.loggedin == true) {
                        document.location.href = redirect_to;
                    }else{
                        alert('<?php _e('[:en]Wrong username or password.[:vi]Mật khẩu hoặc tên đăng nhập không đúng[:]')?>');
                    }
                }
            });
            return false;
        });

        $("#reg-form-ajax,#reg-form-ajax-mobile").on('submit', function () {
            var redirect_to = $(this).find('input.redirect_to').val();
            $.post({
                type: 'post',
                dataType: 'json',
                url: ADMIN_AJAX,
                data: $(this).serialize(),
                success: function (data) {
                    if (data == 1) {
                        alert("<?php _e('[:en]Sign Up Success[:vi]Đăng ký thành công[:]')?>");
                        document.location.href = redirect_to;
                    }else if(data==2){
                        alert('<?php _e('[:en]Password and password incorrect[:vi]Mật khẩu không khớp[:]')?>');
                    }else{
                        alert(data);
                    }
                }
            });
            return false;
        });
        
        
        $(".review-comment-border  .pagination-border .pagination .page-numbers li > a").live('click',function () {
            var id_content = $(this).closest('.review-comment-border').find('.review-comment-content').attr('id');
            var id = id_content.split("_")[2];
            var page = $(this).text();
            $.ajax({
                method: 'post',
                data: {
                    id: id,
                    paged: page,
                },
                dataType:'json',
                url: '<?php echo $base_url ?>/comment_list.php',
                beforeSend: function() {
                    $("#loading-image").show();
                },
                success: function (re) {
                    $("#"+id_content).empty();
                    $("#"+id_content).html(re.list);
                    var pagination = $("#"+id_content).closest('.review-comment-border').find('ul.pagination');
                    pagination.empty();
                    pagination.html(re.paginate);
                    $("#loading-image").hide();
                }
            });
            return false;
        });
    });

</script>
</div>
</div>
<?php wp_footer(); ?>

</body>
</html>
