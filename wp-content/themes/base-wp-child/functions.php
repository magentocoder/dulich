<?php
// Exit if accessed directly
if (!defined('ABSPATH')) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if (!function_exists('chld_thm_cfg_parent_css')):
    function chld_thm_cfg_parent_css()
    {
        wp_enqueue_style('chld_thm_cfg_parent', trailingslashit(get_template_directory_uri()) . 'style.css', array());
    }
endif;
add_action('wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10);

if (!function_exists('child_theme_configurator_css')):
    function child_theme_configurator_css()
    {


        wp_enqueue_style('chld_thm_cfg_separate', trailingslashit(get_stylesheet_directory_uri()) . 'ctc-style.css', array('chld_thm_cfg_parent', 'base-wp-style', 'dynamic-style', 'custom-style'));
        wp_enqueue_style('base-wp-bootstrap-theme', trailingslashit(get_stylesheet_directory_uri()) . 'bootstrap/css/bootstrap-theme.css');
        wp_enqueue_style('base-wp-bootstrap-min', trailingslashit(get_stylesheet_directory_uri()) . 'bootstrap/css/bootstrap.min.css');
        wp_enqueue_style('base-wp-bootstrap-theme-min', trailingslashit(get_stylesheet_directory_uri()) . 'bootstrap/css/bootstrap-theme.min.css');
        wp_enqueue_style('base-wp-mystyles-min', trailingslashit(get_stylesheet_directory_uri()) . 'css/mystyles-min.css');
        wp_enqueue_style('base-wp-style-Slide', trailingslashit(get_stylesheet_directory_uri()) . 'css/styleSlide.css');
        wp_enqueue_style('base-wp-style-tourist', trailingslashit(get_stylesheet_directory_uri()) . 'css/style.css');
        wp_enqueue_style('base-wp-bootstrap-theme', trailingslashit(get_stylesheet_directory_uri()) . 'css/fontawesome-all.css');
        wp_enqueue_style('base-wp-bootstrap-theme', trailingslashit(get_stylesheet_directory_uri()) . 'css/fontawesome-all.min.css');

    }
endif;
add_action('wp_enqueue_scripts', 'child_theme_configurator_css');
if (!function_exists('child_theme_configurator_js')):
    function child_theme_configurator_js()
    {


        wp_enqueue_script('base-wp-bootstrap-min', trailingslashit(get_stylesheet_directory_uri()) . 'bootstrap/js/bootstrap.min.js', array('jquery'), '20151215', true);
        wp_enqueue_script('base-wp-vendor', trailingslashit(get_stylesheet_directory_uri()) . 'js/vendor.js', array(), '20151215', true);
        wp_enqueue_script('base-wp-mutiljs', trailingslashit(get_stylesheet_directory_uri()) . 'js/mutiljs.js', array(), '20151215', true);
        wp_enqueue_script('base-wp-fontawesome-all', trailingslashit(get_stylesheet_directory_uri()) . 'js/fontawesome-all.js', array(), '20151215', true);
        wp_enqueue_script('base-wp-main', trailingslashit(get_stylesheet_directory_uri()) . 'js/main.js', array(), '20151215', true);
        wp_enqueue_script('base-wp-flexisel', trailingslashit(get_stylesheet_directory_uri()) . 'js/jquery.flexisel.js', array('jquery'), '20151215', true);
        wp_enqueue_script('base-wp-js', trailingslashit(get_stylesheet_directory_uri()) . 'js/js.js', array('jquery', 'base-wp-flexisel'), '20151215', true);
        wp_enqueue_script('base-wp-th_js', trailingslashit(get_stylesheet_directory_uri()) . 'js/th_js.js', array('jquery', 'base-wp-flexisel'), '20151215', true);
    }
endif;
add_action('wp_enqueue_scripts', 'child_theme_configurator_js');
// END ENQUEUE PARENT ACTION
add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);
function special_nav_class($classes, $item)
{
    $classes[] = 'introduct_menu menu-item';
    $classes[] = 'menu-item-type-custom ';
    $classes[] = 'menu-item-object-custom';
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}

function widget_customs()
{

    register_sidebar(array(
        'name' => esc_html__('Header widget area', 'base-wp'),
        'id' => 'translate-widget',
        'description' => esc_html__('Add widgets here.', 'base-wp'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}

add_action('widgets_init', 'widget_customs');

add_filter('loop_shop_per_page', 'new_loop_shop_per_page', 20);

function new_loop_shop_per_page($cols)
{
    $cols = 5;
    return $cols;
}

function woocommerce_pagination_revert()
{
    if (!wc_get_loop_prop('is_paginated') || !woocommerce_products_will_display()) {
        return;
    }
    $args = array(
        'total' => wc_get_loop_prop('total_pages'),
        'current' => wc_get_loop_prop('current_page'),
    );

    if (wc_get_loop_prop('is_shortcode')) {
        $args['base'] = esc_url_raw(add_query_arg('product-page', '%#%', false));
        $args['format'] = '?product-page = %#%';
    } else {
        $args['base'] = esc_url_raw(str_replace(999999999, '%#%', remove_query_arg('add-to-cart', get_pagenum_link(999999999, false))));
        $args['format'] = '';
    }

    wc_get_template('loop/pagination.php', $args);

}

add_action('woocommerce_pagination', 'woocommerce_pagination_revert');
$url = $_SERVER['REQUEST_URI'];
if (strpos($url, 'category') !== false) {
    add_action('parse_query', 'changept');
    function changept()
    {
        $category = get_query_var('cat');
        if (trim($category) == '') {
            $cat = $cat_obj = get_queried_object();
            $category = $cat->term_id;
        }

        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        if (is_main_query()) {
            set_query_var('cat', $category);
            set_query_var('paged', $paged);
            set_query_var('posts_per_page', 5);
            set_query_var('orderby', 'term_id');
            set_query_var('order', 'DESC');
        }
        return;
    }
}
function wpse_58613_comment_redirect($location)
{
    if (isset($_POST['my_redirect_to'])) // Don't use "redirect_to", internal WP var
        $location = $_POST['my_redirect_to'];

    return $location;
}

add_filter('comment_post_redirect', 'wpse_58613_comment_redirect');
add_action('user_register', 'myplugin_registration_save', 10, 1);

function myplugin_registration_save($user_id)
{

    if (isset($_POST['user_pass'])) {
        wp_set_password($_POST['user_pass'], $user_id);
    }

}

function add_cart_item_data($cart_item_data, $product_id, $variation_id)
{

    // Has our option been selected?
    if (isset($_POST['tour_product'])) {
        $product = wc_get_product($product_id);
        $gia_nl = $product->get_price();
        $gia_tr = (int)get_field('gia_tre_em', $product_id);
        $n_ngl = (int)(isset($_POST['n_ngl']) ? $_POST['n_ngl'] : 0);
        $n_tr = (int)(isset($_POST['n_tr']) ? $_POST['n_tr'] : 0);
        // Store the overall price for the product, including the cost of the warranty
        $cart_item_data['total_price'] = $gia_nl * $n_ngl + $gia_tr + $n_tr;
    }
    return $cart_item_data;
}

add_filter('woocommerce_add_cart_item_data', 'add_cart_item_data', 10, 3);

function before_calculate_totals($cart_obj)
{
    if (is_admin() && !defined('DOING_AJAX')) {
        return;
    }
    // Iterate through each cart item
    foreach ($cart_obj->get_cart() as $key => $value) {
        if (isset($value['total_price'])) {
            $price = $value['total_price'];
            $value['data']->set_price(($price));
        }
    }
}

add_action('woocommerce_before_calculate_totals', 'before_calculate_totals', 10, 1);
function cms_rest_cookies()
{
    unset($_COOKIE['comment_author_' . COOKIEHASH]);
    unset($_COOKIE['comment_author_email_' . COOKIEHASH]);
    unset($_COOKIE['comment_author_url_' . COOKIEHASH]);
    //unset($_COOKIE[]);
    // empty value and expiration one hour before
    setcookie('comment_author_' . COOKIEHASH, '', time() - 3600);
    setcookie('comment_author_email_' . COOKIEHASH, '', time() - 3600);
    setcookie('comment_author_url_' . COOKIEHASH, '', time() - 3600);
}

add_action('init', 'cms_rest_cookies');

add_action('wp_ajax_ajax_login', 'ajax_login');
add_action('wp_ajax_nopriv_ajax_login', 'ajax_login');
function ajax_login()
{
    $info = array();
    $info['user_login'] = $_POST['log'];
    $info['user_password'] = $_POST['pwd'];
    if (isset($_POST['type']) && $_POST['type'] == 'company') {
        $user = get_user_by('email', $info['user_login']);
        if (!$user) {
            echo json_encode(array('loggedin' => false));
            die();
        }
        if ( !in_array( 'author', (array) $user->roles ) ) {
            echo json_encode(array('loggedin' => false));
            die();
        }
    }
    $user_signon = wp_signon($info, false);
    if (is_wp_error($user_signon)) {
        echo json_encode(array('loggedin' => false));
    } else {
        echo json_encode(array('loggedin' => true));
    }
    die();
}

function reg_new_user_ajax()
{

    $email = $_POST['user_email'];
    $username = $_POST['user_login'];
    $password = $_POST['user_pass'];
    if ($password != $_POST['pass1_re']) {
        echo $password . 'sss';
        echo $_POST['pass1_re'];
        echo '2';
    } else {
        $userdata = array(
            'user_login' => $username,
            'user_pass' => $password,
            'user_email' => $email
        );

        $user_id = wp_insert_user($userdata);

        // Return
        if (!is_wp_error($user_id)) {
            echo '1';
        } else {
            echo $user_id->get_error_message();
        }
    }

    die();

}

add_action('wp_ajax_reg_new_user_ajax', 'reg_new_user_ajax');
add_action('wp_ajax_nopriv_reg_new_user_ajax', 'reg_new_user_ajax');

add_action('igthemes_single_page', 'remove_igthemes_page_footer');
function remove_igthemes_page_footer()
{
    remove_action('igthemes_single_page', 'igthemes_page_footer', 40);
    remove_action('igthemes_single_page', 'igthemes_page_header', 20);
}

if ( ! function_exists( 'bcdonline_breadcrumbs' ) ){
    /**
     * Prints HTML.
     */
    function bcdonline_breadcrumbs() {
        $delimiter = '';
        $name =  _x('[:en]Home[:vi]Trang chủ[:]','noun'); //text for the 'Home' link
        $currentBefore = '<span class="current">';
        $currentAfter = '</span>';

        global $post;
        $home = get_bloginfo('url');
        if (is_product_category()){
            echo ' <a href="'.$home.'" title="Trang chủ" class="icon-homepage">
                        <span class="glyphicon glyphicon-home"></span>
                    </a>'. $delimiter . ' ';
            $cat = get_queried_object();
            echo $currentBefore;
            echo $cat->name;
            echo $currentAfter;
        }elseif(is_category()){
            echo ' <a href="'.$home.'" title="Trang chủ" class="icon-homepage">
                        <span class="glyphicon glyphicon-home"></span>
                    </a>'. $delimiter . ' ';
            global $wp_query;
            $cat_obj = $wp_query->get_queried_object();
            $thisCat = $cat_obj->term_id;
            $thisCat = get_category($thisCat);
            $parentCat = get_category($thisCat->parent);
            if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
            echo $currentBefore;
            single_cat_title();
            echo $currentAfter;
        }elseif ( is_page() && !$post->post_parent ) {
            echo ' <a href="'.$home.'" title="Trang chủ" class="icon-homepage">
                        <span class="glyphicon glyphicon-home"></span>
                    </a>'. $delimiter . ' ';
            if(isset($_GET['id'])){
                $cat_parent = get_the_terms($_GET['id'], 'product_cat');
                $cat_parent_id = $cat_parent[0]->term_id;
                echo '<a href="' . esc_url( get_term_link($cat_parent_id ) )  . '">'. $cat_parent[0]->name . '</a> '. $delimiter . ' ';
            }
            echo $currentBefore;
            the_title();
            echo $currentAfter;

        } elseif ( is_page() && $post->post_parent ) {
            echo ' <a href="'.$home.'" title="Trang chủ" class="icon-homepage">
                        <span class="glyphicon glyphicon-home"></span>
                    </a>'. $delimiter . ' ';
            $parent_id  = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_post( $parent_id, $output = OBJECT, $filter = 'raw');
                $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
                $parent_id  = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
            echo $currentBefore;
            the_title();
            echo $currentAfter;

        }elseif ( is_singular('product') ) {
            echo ' <a href="'.$home.'" title="Trang chủ" class="icon-homepage">
                        <span class="glyphicon glyphicon-home"></span>
                    </a>'. $delimiter . ' ';
            $cat_parent = get_the_terms(get_the_ID(), 'product_cat');
            $cat_parent_id = $cat_parent[0]->term_id;
            echo '<a href="' . esc_url( get_term_link($cat_parent_id ) )  . '">'. $cat_parent[0]->name . '</a> '. $delimiter . ' ';
            echo $currentBefore;
            the_title();
            echo $currentAfter;

        }elseif(( is_single() )){
            echo ' <a href="'.$home.'" title="Trang chủ" class="icon-homepage">
                        <span class="glyphicon glyphicon-home"></span>
                    </a>'. $delimiter . ' ';
            $cat = get_the_category(); $cat = $cat[0];
            echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
            echo $currentBefore;
            the_title();
            echo $currentAfter;
        }
    }
}