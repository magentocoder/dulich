<?php
// Template Name: Home
get_header();
global $wpdb;
$cityFoundFistItem = null;
if (!empty($_GET['timkiem'])) {
    $cityFoundFistItem = $wpdb->get_row('SELECT DISTINCT meta_value FROM wp_postmeta WHERE meta_key LIKE "city" AND meta_value LIKE "%' . $_GET['timkiem'] . '%"', ARRAY_A);
}
$city = $wpdb->get_results('SELECT DISTINCT meta_value FROM wp_postmeta WHERE meta_key LIKE "city" AND meta_value IS NOT NULL', ARRAY_A);
$base_url = get_site_url();
?>
    <main>
        <div class="row" id="search-custom" style="display: none;">
            <main class="col-xs-12 col-xs-12 listing-page-border">
                <div class="row">
                    <div class="container">
                        <div class="row">
                            <div class="main-content col-lg-12 col-xs-12">
                                <div class="row">

                                    <aside class="col-lg-3 col-xs-12 ">
                                        <div class="row">
                                            <div class="menu-left-mobile">
                                                <div class="col-xs-12 hidden-lg hidden-md">
                                                    <div class="button-filter">Bộ lọc</div>
                                                </div>
                                                <div class="all-province-city">
                                                    <div class="menu-left">
                                                        <div class="sidebar hidden-sm hidden-xs">
                                                            <div class="mapp-wappar">
                                                                <div class="map-place">
                                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703"
                                                                            width="100%" height="150px" frameborder="0"
                                                                            style="border:0"
                                                                            allowfullscreen></iframe>
                                                                </div>
                                                            </div>
                                                            <div class="map-sticker">
                                                                <span>Hiển thị bản đồ</span>
                                                            </div>
                                                        </div>
                                                        <div class="filter-border">
                                                            <div class="filter-border-sub">
                                                                <div class="filter-title">Chọn lọc theo</div>
                                                                <br/>
                                                                <select id="city" class="form-control">
                                                                    <option>Thành Phố</option>
                                                                    <?php
                                                                    foreach ($city as $item) {
                                                                        ?>
                                                                        <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                            <option
                                                                                <?php if (!empty($cityFoundFistItem) && $cityFoundFistItem['meta_value'] != null && $item['meta_value'] == $cityFoundFistItem['meta_value']) { ?>
                                                                                    selected
                                                                                <?php } ?>><?= $item['meta_value']; ?></option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <br/>
                                                                <select id="district" class="form-control">
                                                                    <option>Quận - Huyện</option>
                                                                </select>
                                                                <br/>
                                                                <select id="commune" class="form-control">
                                                                    <option>Phường - Xã</option>
                                                                </select>
                                                                <br/>
                                                                <span>Giá tiền</span><br/>
                                                                <input class="price" type="checkbox" value="0 đ - 1.000.000 đ">0 đ
                                                                - 1.000.000 đ<br>
                                                                <input class="price" type="checkbox"
                                                                       value="1.000.000 đ - 2.000.000 đ">1.000.000 đ -
                                                                2.000.000 đ<br>
                                                                <input class="price" type="checkbox"
                                                                       value="2.000.000 đ - 3.000.000 đ">2.000.000 đ -
                                                                3.000.000 đ<br>
                                                                <input class="price" type="checkbox"
                                                                       value="3.000.000 đ - 4.000.000 đ">3.000.000 đ -
                                                                4.000.000đ<br>
                                                                <input class="price" type="checkbox" value="4.000.000 đ trở lên">4.000.000 đ
                                                                trở lên<br>
                                                                <br/>
                                                                <button type="button" class="btn btn-default" id="search-list">
                                                                    <span class="glyphicon glyphicon-search"></span> Tìm
                                                                </button>
                                                                <br/><br/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </aside>

<!--                                    $template == 1-->
                                    <aside class="col-lg-3 col-xs-12 " style="display: none">
                                        <div class="row">
                                            <div class="menu-left-mobile">
                                                <div class="col-xs-12 hidden-lg hidden-md">
                                                    <div class="button-filter">Bộ lọc</div>
                                                </div>
                                                <div class="all-province-city">
                                                    <div class="menu-left">
                                                        <div class="filter-border">
                                                            <div class="filter-border">
                                                                <div class="filter-title">Tìm xe cho thuê</div>
                                                                <div class=""><br/>
                                                                    <select id="city" class="form-control">
                                                                        <option>Thành Phố</option>
                                                                        <?php
                                                                        foreach ($city as $item) {
                                                                            ?>
                                                                            <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                                <option><?= $item['meta_value']; ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                    <br/>
                                                                    <select id="district" class="form-control">
                                                                        <option>Quận - Huyện</option>
                                                                    </select>
                                                                    <br/>
                                                                    <select id="commune" class="form-control">
                                                                        <option>Phường - Xã</option>
                                                                    </select>
                                                                    <br/>
                                                                    <span>Ngày nhận xe</span>
                                                                    <input id="time_start" type="date" class="form-control">
                                                                    <br/>
                                                                    <span>Ngày trả xe</span>
                                                                    <input id="time_end" type="date" class="form-control">
                                                                    <br/>
                                                                    <span>Kiểu thuê xe</span><br/>
                                                                    <input class="kieu_thue" type="checkbox" value="Xe tự lái"/>Xe
                                                                    tự lái<br/>
                                                                    <input class="kieu_thue" type="checkbox" value="Xe tự lái"/>Xe
                                                                    có người lái<br/>
                                                                    <br/>
                                                                    <button type="button" class="btn btn-default"
                                                                            id="search-list-transport">
                                                                        <span class="glyphicon glyphicon-search"></span> Tìm kiếm
                                                                    </button>
                                                                    <br/></div>
                                                                <br/>
                                                                <div class="filter-title">Tìm kiếm</div>
                                                                <span>Dòng xe</span><br/>
                                                                <input class="dong_xe" type="checkbox" value="Xe thường"/>Xe
                                                                thường<br/>
                                                                <input class="dong_xe" type="checkbox" value="Dcar Limousine"/>Dcar
                                                                Limousine<br/>
                                                                <br/>
                                                                <span>Loại xe</span><br/>
                                                                <input class="loai_xe" type="checkbox" value="4 chỗ"/>4 chỗ<br/>
                                                                <input class="loai_xe" type="checkbox" value="7 chỗ"/>7 chỗ<br/>
                                                                <input class="loai_xe" type="checkbox" value="9 chỗ"/>9 chỗ<br/>
                                                                <input class="loai_xe" type="checkbox" value="16 chỗ"/>16 chỗ<br/>
                                                                <input class="loai_xe" type="checkbox" value="29 chỗ"/>29 chỗ<br/>
                                                                <input class="loai_xe" type="checkbox" value="45 chỗ"/>45 chỗ<br/>
                                                                <br/>
                                                                <span>Xếp hạng</span><br/>
                                                                <input class="xep_hang_xe" type="checkbox" value="Tuyệt vời"/>Tuyệt
                                                                vời<br/>
                                                                <input class="xep_hang_xe" type="checkbox" value="Rất tôt"/>Rât
                                                                tốt<br/>
                                                                <input class="xep_hang_xe" type="checkbox" value="Tốt"/>Tốt<br/>
                                                                <br/>
                                                                <span>Tìm theo tên nhà cung cấp xe</span><br/>
                                                                <input type="text" class="form-control" id="nha_cung_cap"
                                                                       placeholder="Tên nhà cung cấp ..."/>
                                                                <div class="filter-content"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </aside>
<!--                                    $filter_template == 1-->
                                    <aside class="col-lg-3 col-xs-12 " style="display: none">
                                        <div class="row">
                                            <div class="menu-left-mobile">
                                                <div class="col-xs-12 hidden-lg hidden-md">
                                                    <div class="button-filter">Bộ lọc</div>
                                                </div>
                                                <div class="all-province-city">
                                                    <div class="menu-left filter_food">
                                                        <div class="sidebar hidden-sm hidden-xs">
                                                            <div class="mapp-wappar">
                                                                <div class="map-place">
                                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703"
                                                                            width="100%" height="150px" frameborder="0"
                                                                            style="border:0"
                                                                            allowfullscreen></iframe>
                                                                </div>
                                                            </div>
                                                            <div class="map-sticker">
                                                                <span>Hiển thị bản đồ</span>
                                                            </div>
                                                        </div>
                                                        <div class="filter-border">
                                                            <div class="filter-title">Chọn lọc theo</div>
                                                            <br/>
                                                            <select id="city" class="form-control">
                                                                <option>Thành Phố</option>
                                                                <?php
                                                                foreach ($city as $item) {
                                                                    ?>
                                                                    <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                        <option><?= $item['meta_value']; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                            <br/>
                                                            <select id="district" class="form-control">
                                                                <option>Quận - Huyện</option>
                                                            </select>
                                                            <br/>
                                                            <select id="commune" class="form-control">
                                                                <option>Phường - Xã</option>
                                                            </select>
                                                            <br/>
                                                            <span>LOẠI CƠ SỞ</span><br/>
                                                            <input class="loai_co_so" type="checkbox" value="Nhà hàng"/>Nhà
                                                            hàng<br/>
                                                            <input class="loai_co_so" type="checkbox" value="Món tráng miệng"/>Món
                                                            tráng miệng<br/>
                                                            <input class="loai_co_so" type="checkbox" value="Cà phê và trà"/>Cà phê
                                                            và trà<br/>
                                                            <input class="loai_co_so" type="checkbox" value="Bar và quán rượu"/>Bar
                                                            và quán rượu<br/>
                                                            <br/>
                                                            <span>ẨM THỰC VÀ MÓN ĂN</span><br/>
                                                            <input class="food" type="checkbox" value="Món Á"/>Món Á<br/>
                                                            <input class="food" type="checkbox" value="Món Âu"/>Món Âu<br/>
                                                            <input class="food" type="checkbox" value="Món Trung"/>Món Trung<br/>
                                                            <input class="food" type="checkbox" value="Món Thái"/>Món Thái<br/>
                                                            <br/>
                                                            <span>PHÙ HỢP VỚI</span><br/>
                                                            <input class="fit" type="checkbox" value="Gia đình có trẻ em"/>Gia đình
                                                            có trẻ em<br/>
                                                            <input class="fit" type="checkbox" value="Hội họp"/>Hội họp<br/>
                                                            <input class="fit" type="checkbox" value="Tổ chức sự kiện"/>Tổ chức sự
                                                            kiện<br/>
                                                            <input class="fit" type="checkbox" value="Món ăn địa phương"/>Món ăn địa
                                                            phương<br/>
                                                            <br/>
                                                            <span>GÍA CẢ</span><br/>
                                                            <input class="food_price" type="checkbox" value="Đồ ăn giá rẻ"/>Đồ ăn
                                                            giá rẻ<br/>
                                                            <input class="food_price" type="checkbox" value="Giá trung bình"/>Giá
                                                            trung bình<br/>
                                                            <input class="food_price" type="checkbox" value="Cao cấp"/>Cao cấp<br/>
                                                            <br/>
                                                            <button type="button" class="btn btn-default" id="search-list-food">
                                                                <span class="glyphicon glyphicon-search"></span> Tìm
                                                            </button>
                                                            <br/><br/>
                                                            <span>Tìm theo tên</span><br/>
                                                            <input type="text" class="form-control" id="ten-nha-hang"
                                                                   placeholder="Tìm theo tên ..."/>
                                                            <!--                                                <div class="filter-content"></div>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </aside>

<!--                                    $filter_template == 2 && $template == 0-->
                                    <aside class="col-lg-3 col-xs-12 " style="display: none">
                                        <div class="row">
                                            <div class="menu-left-mobile">
                                                <div class="col-xs-12 hidden-lg hidden-md">
                                                    <div class="button-filter">Bộ lọc</div>
                                                </div>
                                                <div class="all-province-city">
                                                    <div class="menu-left">
                                                        <div class="sidebar hidden-sm hidden-xs">
                                                            <div class="mapp-wappar">
                                                                <div class="map-place">
                                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703"
                                                                            width="100%" height="150px" frameborder="0"
                                                                            style="border:0"
                                                                            allowfullscreen></iframe>
                                                                </div>
                                                            </div>
                                                            <div class="map-sticker">
                                                                <span>Hiển thị bản đồ</span>
                                                            </div>
                                                        </div>
                                                        <div class="filter-border">
                                                            <div class="filter-title">Chọn lọc theo</div>
                                                            <br/>
                                                            <select id="city" class="form-control">
                                                                <option>Thành Phố</option>
                                                                <?php
                                                                foreach ($city as $item) {
                                                                    ?>
                                                                    <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                        <option><?= $item['meta_value']; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                            <br/>
                                                            <select id="district" class="form-control">
                                                                <option>Quận - Huyện</option>
                                                            </select>
                                                            <br/>
                                                            <select id="commune" class="form-control">
                                                                <option>Phường - Xã</option>
                                                            </select>
                                                            <br/>
                                                            <select id="price" class="form-control">
                                                                <option>Giá tiền</option>
                                                                <option>0 đ - 1.000.000 đ</option>
                                                                <option>1.000.000 đ - 2.000.000 đ</option>
                                                                <option>2.000.000 đ - 3.000.000 đ</option>
                                                                <option>3.000.000 đ - 4.000.000 đ</option>
                                                                <option>4.000.000 đ trở lên</option>
                                                            </select>
                                                            <br/>
                                                            <select id="rate" class="form-control">
                                                                <option value="0">Đánh giá</option>
                                                                <option>Tuyệt hảo</option>
                                                                <option>Rất tốt</option>
                                                                <option>Tốt</option>
                                                                <option>Dẽ chịu</option>
                                                            </select>
                                                            <br/>
                                                            <select id="rank" class="form-control">
                                                                <option>Hạng</option>
                                                                <option>1 sao</option>
                                                                <option>2 sao</option>
                                                                <option>3 sao</option>
                                                                <option>4 sao</option>
                                                                <option>5 sao</option>
                                                            </select>
                                                            <br/>
                                                            <button type="button" class="btn btn-default" id="search-list-company">
                                                                <span class="glyphicon glyphicon-search"></span> Tìm
                                                            </button>
                                                            <br/><br/>
                                                            <span>Tìm theo tên</span><br/>
                                                            <input type="text" class="form-control" id="ten-tour"
                                                                   placeholder="Tìm theo tên ..."/>
                                                            <!--                                                <div class="filter-content"></div>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </aside>
<!--                                    $filter_template == 2-->
                                    <aside class="col-lg-3 col-xs-12 " style="display: none">
                                        <div class="row">
                                            <div class="menu-left-mobile">
                                                <div class="col-xs-12 hidden-lg hidden-md">
                                                    <div class="button-filter">Bộ lọc</div>
                                                </div>
                                                <div class="all-province-city">
                                                    <div class="menu-left">
                                                        <div class="sidebar  hidden-sm hidden-xs">
                                                            <div class="mapp-wappar">
                                                                <div class="map-place">
                                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703"
                                                                            width="100%" height="150px" frameborder="0"
                                                                            style="border:0"
                                                                            allowfullscreen></iframe>
                                                                </div>
                                                            </div>
                                                            <div class="map-sticker">
                                                                <span>Hiển thị bản đồ</span>
                                                            </div>
                                                        </div>
                                                        <div class="filter-border">
                                                            <div class="filter-title">Chọn lọc theo</div>
                                                            <br/>
                                                            <select id="city" class="form-control">
                                                                <option>Thành Phố</option>
                                                                <?php
                                                                foreach ($city as $item) {
                                                                    ?>
                                                                    <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                        <option><?= $item['meta_value']; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                            <br/>
                                                            <select id="district" class="form-control">
                                                                <option>Quận - Huyện</option>
                                                            </select>
                                                            <br/>
                                                            <select id="commune" class="form-control">
                                                                <option>Phường - Xã</option>
                                                            </select>
                                                            <br/>
                                                            <select id="price" class="form-control">
                                                                <option>Giá tiền</option>
                                                                <option>0 đ - 1.000.000 đ</option>
                                                                <option>1.000.000 đ - 2.000.000 đ</option>
                                                                <option>2.000.000 đ - 3.000.000 đ</option>
                                                                <option>3.000.000 đ - 4.000.000 đ</option>
                                                                <option>4.000.000 đ trở lên</option>
                                                            </select>
                                                            <br/>
                                                            <select id="rate" class="form-control">
                                                                <option value="0">Đánh giá</option>
                                                                <option>Tuyệt hảo</option>
                                                                <option>Rất tốt</option>
                                                                <option>Tốt</option>
                                                                <option>Dẽ chịu</option>
                                                            </select>
                                                            <br/>
                                                            <select id="rank" class="form-control">
                                                                <option>Hạng</option>
                                                                <option>1 sao</option>
                                                                <option>2 sao</option>
                                                                <option>3 sao</option>
                                                                <option>4 sao</option>
                                                                <option>5 sao</option>
                                                            </select>
                                                            <br/>
                                                            <button type="button" class="btn btn-default" id="search-list-tour">
                                                                <span class="glyphicon glyphicon-search"></span> Tìm
                                                            </button>
                                                            <br/><br/>
                                                            <span>Tìm theo tên</span><br/>
                                                            <input type="text" class="form-control" id="ten-tour"
                                                                   placeholder="Tìm theo tên ..."/>
                                                            <!--                                                <div class="filter-content"></div>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </aside>
<!--                                    $filter_template == 0-->

                                    <aside class="col-lg-3 col-xs-12 " style="display: none">
                                        <div class="row">
                                            <div class="menu-left-mobile">
                                                <div class="col-xs-12 hidden-lg hidden-md">
                                                    <div class="button-filter">Bộ lọc</div>
                                                </div>
                                                <div class="all-province-city">
                                                    <div class="menu-left">
                                                        <div class="sidebar  hidden-sm hidden-xs">
                                                            <div class="mapp-wappar">
                                                                <div class="map-place">
                                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703"
                                                                            width="100%" height="150px" frameborder="0"
                                                                            style="border:0"
                                                                            allowfullscreen></iframe>
                                                                </div>
                                                            </div>
                                                            <div class="map-sticker">
                                                                <span>Hiển thị bản đồ</span>
                                                            </div>
                                                        </div>
                                                        <div class="filter-border">
                                                            <div class="filter-title">Chọn lọc theo</div>
                                                            <br/>
                                                            <select id="city" class="form-control">
                                                                <option>Thành Phố</option>
                                                                <?php
                                                                foreach ($city as $item) {
                                                                    ?>
                                                                    <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                        <option><?= $item['meta_value']; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                            <br/>
                                                            <select id="district" class="form-control">
                                                                <option>Quận - Huyện</option>
                                                            </select>
                                                            <br/>
                                                            <select id="commune" class="form-control">
                                                                <option>Phường - Xã</option>
                                                            </select>
                                                            <br/>
                                                            <span>Giá tiền</span><br/>
                                                            <input class="price" type="checkbox" value="0 đ - 1.000.000 đ">0 đ -
                                                            1.000.000 đ<br>
                                                            <input class="price" type="checkbox" value="1.000.000 đ - 2.000.000 đ">1.000.000 đ
                                                            - 2.000.000 đ<br>
                                                            <input class="price" type="checkbox" value="2.000.000 đ - 3.000.000 đ">2.000.000 đ
                                                            - 3.000.000 đ<br>
                                                            <input class="price" type="checkbox" value="3.000.000 đ - 4.000.000 đ">3.000.000 đ
                                                            - 4.000.000 đ<br>
                                                            <input class="price" type="checkbox" value="4.000.000 đ trở lên">4.000.000 đ
                                                            trở lên<br>
                                                            <br/>
                                                            <span>Đánh giá của khách sạn</span><br/>
                                                            <input class="rate" type="checkbox" value="Tuyệt hảo"/>Tuyệt hảo<br/>
                                                            <input class="rate" type="checkbox" value="Tuyệt hảo"/>Rất tốt<br/>
                                                            <input class="rate" type="checkbox" value="Tuyệt hảo"/>Tốt<br/>
                                                            <input class="rate" type="checkbox" value="Tuyệt hảo"/>Dễ chịu<br/>
                                                            <br/>
                                                            <span>Hạng của khách sạn</span><br/>
                                                            <input class="rank" type="checkbox" value="1 sao"/>1 sao<br/>
                                                            <input class="rank" type="checkbox" value="2 sao"/>2 sao<br/>
                                                            <input class="rank" type="checkbox" value="3 sao"/>3 sao<br/>
                                                            <input class="rank" type="checkbox" value="4 sao"/>4 sao<br/>
                                                            <input class="rank" type="checkbox" value="5 sao"/>5 sao<br/>
                                                            <br/>
                                                            <button type="button" class="btn btn-default" id="search-list-hotel">
                                                                <span class="glyphicon glyphicon-search"></span> Tìm
                                                            </button>
                                                            <br/><br/>
                                                            <span>Tìm theo tên khách sạn</span><br/>
                                                            <input type="text" class="form-control" id="ten-khach-san"
                                                                   placeholder="Tìm theo tên nhà hàng ..."/>
                                                            <!--                                                <div class="filter-content"></div>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </aside>

                                    <section class="col-lg-9 col-xs-12">
                                        <div class="row">
                                            <div class="listing-page">
                                                <div class="toolbar hidden-sm hidden-xs">
                                                </div>
                                                <div class="border-list-item-tourist" id="custom-search-result">
                                                </div>
                                                <div class="note-bottom"><?php _e('[:en]The room price show on Trakevn calculate by each night for all room choosen[:vi]Giá phòng hiển thị trên Trekvn tính theo từng đêm cho
                                            tổng số phòng đã chọn[:]') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>



        <div class="container">
            <div class="row">
                <div id="main-container">
                    <section>
                        <div class="section-content col-lg-12  col-xs-12">
                            <div class="row">
                                <h3 class="title-section" title="<?php _e('[:en]HOTEL[:vi]Khách sạn[:]') ?>">
                                    <a href="#"
                                       title="<?php _e('[:en]HOTEL[:vi]Khách sạn[:]') ?>"><?php _e('[:en]HOTEL[:vi]Khách sạn[:]') ?></a>
                                </h3>
                                <?php
                                $args = array(
                                    'post_type' => 'product',
                                    'orderby' => 'term_id',
                                    'order' => 'DESC',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'product_cat',
                                            'field' => 'id',
                                            'terms' => 18
                                        ))
                                );
                                $pro = new WP_Query($args);
                                $j = 0;
                                ?>
                                <div class="slider-wappar-content">
                                    <ul id="flexiselDemo1">
                                        <?php while ($pro->have_posts()): ?>
                                            <?php $pro->the_post();
                                            global $product;
                                            $pro_data = $product->get_data('name');
                                            $image = wp_get_attachment_image_src($pro_data['image_id'], 'single-post-thumbnail');
                                            $name = $pro_data['name'];
                                            $base_name = qtranxf_use('vi', $name, false);
                                            $price = $pro_data['price'];
                                            ?>
                                            <li>
                                                <div class="item-content-section">
                                                    <div class="image-item">
                                                        <a href="<?php echo $base_url . '/danh-muc/hotel/?timkiem-khachsan=' . $base_name; ?>"
                                                           title="<?php _e($name); ?>">
                                                            <img src="<?php echo $image[0] ?>" alt="<?php _e($name) ?>"
                                                                 title="<?php _e($name); ?>">
                                                        </a>
                                                    </div>
                                                    <div class="item-title">
                                                        <a href="<?php echo $base_url . '/danh-muc/hotel/?timkiem-khachsan=' . $base_name; ?>"
                                                           title="<?php _e($name); ?>"><?php _e($name); ?></a>
                                                    </div>
                                                    <div class="content-place">
                                                        <div class="price-place"><?php echo number_format($price, 0, '', ','); ?> đ
                                                        </div>
                                                        <div class="review-place">
                                                            <?php
                                                            $rate  = round($product->get_average_rating() * 2,1);
                                                            $comments_count = wp_count_comments(get_the_ID());
                                                            $label = '';
                                                            if($rate>8){
                                                                $label = '[:en]Excellent[:vi]Xuất sắc[:]';
                                                            }elseif($rate>5){
                                                                $label = '[:en]Rather[:vi]Khá[:]';
                                                            }elseif($rate>0){
                                                                $label = '[:en]Medium[:vi]Bình thường[:]';
                                                            }
                                                            ?>
                                                            <span class="review-percent"><?php echo number_format($rate,1)?></span>
                                                            <span class="review-comment"><?php _e($label)?> (<?php echo (int)$comments_count->total_comments; ?> <?php _e('[:en]comment[:vi]nhận xét[:]')?>)</span>
                                                        </div>
                                                    </div>
                                                    <div class="background-hover"></div>
                                                </div>
                                            </li>


                                            <?php $j++;
                                            if ($j >= 10) break;
                                            ?>
                                        <?php endwhile; ?>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </section>
                    <section>
                        <div class="section-content col-lg-12  col-xs-12">
                            <div class="row">
                                <h3 class="title-section" title="<?php _e('[:en]TRANSPORT[:vi]Xe[:]') ?>">
                                    <a href="#"
                                       title="<?php _e('[:en]TRANSPORT[:vi]Xe[:]') ?>"><?php _e('[:en]TRANSPORT[:vi]Xe[:]') ?></a>
                                </h3>
                                <?php
                                $args = array(
                                    'post_type' => 'product',
                                    'orderby' => 'term_id',
                                    'order' => 'DESC',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'product_cat',
                                            'field' => 'id',
                                            'terms' => 19
                                        ))
                                );
                                $pro = new WP_Query($args);
                                $j = 0;
                                ?>
                                <div class="slider-wappar-content">
                                    <ul id="flexiselDemo2">
                                        <?php while ($pro->have_posts()): ?>
                                            <?php $pro->the_post();
                                            global $product;
                                            $pro_data = $product->get_data('name');
                                            $image = wp_get_attachment_image_src($pro_data['image_id'], 'single-post-thumbnail');
                                            $name = $pro_data['name'];
                                            $base_name = qtranxf_use('vi', $name, false);
                                            $price = $pro_data['price'];
                                            ?>

                                            <li>
                                                <div class="item-content-section">
                                                    <div class="image-item">
                                                        <a href="<?php echo $base_url . '/danh-muc/van-chuyen/?timkiem-vanchuyen=' . $base_name; ?>"
                                                           title="<?php _e($name); ?>">
                                                            <img src="<?php echo $image[0] ?>" alt="<?php _e($name) ?>"
                                                                 title="<?php _e($name); ?>">
                                                        </a>
                                                    </div>
                                                    <div class="item-title">
                                                        <a href="<?php echo $base_url . '/danh-muc/van-chuyen/?timkiem-vanchuyen=' . $base_name; ?>"
                                                           title="<?php _e($name); ?>"><?php _e($name); ?></a>
                                                    </div>
                                                    <div class="content-place">
                                                        <div class="price-place"><?php echo number_format($price, 0, '', ','); ?> đ
                                                        </div>
                                                        <div class="review-place">
                                                            <?php
                                                            $rate  = round($product->get_average_rating() * 2,1);
                                                            $comments_count = wp_count_comments(get_the_ID());
                                                            $label = '';
                                                            if($rate>8){
                                                                $label = '[:en]Excellent[:vi]Xuất sắc[:]';
                                                            }elseif($rate>5){
                                                                $label = '[:en]Rather[:vi]Khá[:]';
                                                            }elseif($rate>0){
                                                                $label = '[:en]Medium[:vi]Bình thường[:]';
                                                            }
                                                            ?>
                                                            <span class="review-percent"><?php echo number_format($rate,1)?></span>
                                                            <span class="review-comment"><?php _e($label)?> (<?php echo (int)$comments_count->total_comments; ?> <?php _e('[:en]comment[:vi]nhận xét[:]')?>)</span>
                                                        </div>
                                                    </div>
                                                    <div class="background-hover"></div>
                                                </div>
                                            </li>
                                            <?php $j++;
                                            if ($j >= 10) break;
                                            ?>
                                        <?php endwhile; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section>
                        <div class="section-content col-lg-12  col-xs-12">
                            <div class="row">
                                <h3 class="title-section" title="<?php _e('[:en]ENTERTAINMENT[:vi]GIẢI TRÍ[:]') ?>">
                                    <a href="#"
                                       title="<?php _e('[:en]ENTERTAINMENT[:vi]GIẢI TRÍ[:]') ?>"><?php _e('[:en]ENTERTAINMENT[:vi]GIẢI TRÍ[:]') ?></a>
                                </h3>
                                <?php
                                $arg = array(
                                    'orderby' => 'term_id',
                                    'posts_per_page' => 10,
                                    'order' => 'DESC',
                                    'cat' => 22);
                                $r = new WP_Query($arg);
                                ?>
                                <div class="slider-wappar-content">
                                    <ul id="flexiselDemo3">
                                        <?php if ($r->have_posts()): ?>
                                            <?php foreach ($r->posts as $p): ?>
                                                <?php
                                                $id = $p->ID;
                                                $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'single-post-thumbnail');
                                                $link = get_permalink($p->ID);
                                                $title = $p->post_title;
                                                ?>
                                                <li>
                                                    <div class="item-content-section">
                                                        <div class="image-item">
                                                            <a href="<?php echo $link ?>" title="<?php echo $title; ?>">
                                                                <img src="<?php echo $image[0] ?>"
                                                                     alt="<?php echo $title; ?>"
                                                                     title="<?php echo $title; ?>">
                                                            </a>
                                                        </div>
                                                        <div class="item-title">
                                                            <a href="<?php echo $link ?>"
                                                               title="<?php echo $title; ?>"><?php echo $title; ?></a>
                                                        </div>
                                                        <div class="background-hover"></div>
                                                    </div>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section>
                        <div class="section-content col-lg-12  col-xs-12">
                            <div class="row">
                                <h3 class="title-section" title="<?php _e('[:en]FOOD[:vi]Ăn Uống[:]') ?>">
                                    <a href="#"
                                       title="<?php _e('[:en]FOOD[:vi]Ăn Uống[:]') ?>"><?php _e('[:en]FOOD[:vi]Ăn Uống[:]') ?></a>
                                </h3>
                                <?php
                                $args = array(
                                    'post_type' => 'product',
                                    'orderby' => 'term_id',
                                    'order' => 'DESC',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'product_cat',
                                            'field' => 'id',
                                            'terms' => 37
                                        ))
                                );
                                $pro = new WP_Query($args);
                                $j = 0;
                                ?>
                                <div class="slider-wappar-content">
                                    <ul id="flexiselDemo4">
                                        <?php while ($pro->have_posts()): ?>
                                            <?php $pro->the_post();
                                            global $product;
                                            $pro_data = $product->get_data('name');
                                            $image = wp_get_attachment_image_src($pro_data['image_id'], 'single-post-thumbnail');
                                            $name = $pro_data['name'];
                                            $base_name = qtranxf_use('vi', $name, false);
                                            $price = $pro_data['price'];
                                            ?>
                                            <li>
                                                <div class="item-content-section">
                                                    <div class="image-item">
                                                        <a href="<?php echo $base_url . '/danh-muc/an-uong/?timkiem-anuong=' . $base_name; ?>"
                                                           title="<?php _e($name); ?>">
                                                            <img src="<?php echo $image[0] ?>" alt="<?php _e($name) ?>"
                                                                 title="<?php _e($name); ?>">
                                                        </a>
                                                    </div>
                                                    <div class="item-title">
                                                        <a href="<?php echo $base_url . '/danh-muc/an-uong/?timkiem-anuong=' . $base_name; ?>"
                                                           title="<?php _e($name); ?>"><?php _e($name); ?></a>
                                                    </div>
                                                    <div class="content-place">
                                                        <div class="price-place"><?php echo number_format($price, 0, '', ','); ?> đ
                                                        </div>
                                                        <div class="review-place">
                                                            <?php
                                                            $rate  = round($product->get_average_rating() * 2,1);
                                                            $comments_count = wp_count_comments(get_the_ID());
                                                            $label = '';
                                                            if($rate>8){
                                                                $label = '[:en]Excellent[:vi]Xuất sắc[:]';
                                                            }elseif($rate>5){
                                                                $label = '[:en]Rather[:vi]Khá[:]';
                                                            }elseif($rate>0){
                                                                $label = '[:en]Medium[:vi]Bình thường[:]';
                                                            }
                                                            ?>
                                                            <span class="review-percent"><?php echo number_format($rate,1)?></span>
                                                            <span class="review-comment"><?php _e($label)?> (<?php echo (int)$comments_count->total_comments; ?> <?php _e('[:en]comment[:vi]nhận xét[:]')?>)</span>
                                                        </div>
                                                    </div>
                                                    <div class="background-hover"></div>
                                                </div>
                                            </li>
                                            <?php $j++;
                                            if ($j >= 10) break;
                                            ?>
                                        <?php endwhile; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div id="container-search">
                </div>
            </div>
        </div>
    </main>
<?php
get_footer();
