<?php
get_header();
$cat_obj = get_queried_object();
$is_company_template = (int)get_field('page_template', $cat_obj);
?>
    <main class="col-xs-12 col-xs-12 listing-page-border <?php echo($is_company_template ? 'company-page' : '') ?>">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="main-content col-lg-12 col-xs-12">
                        <div class="row">
                            <aside class="col-lg-3 col-xs-12 ">
                                <div class="row">
                                    <div class="menu-left-mobile">
                                        <div class="col-xs-12 hidden-lg hidden-md">
                                            <div class="button-filter">Bộ lọc</div>
                                        </div>
                                        <div class="all-province-city">
                                            <div class="menu-left">
                                                <?php if ($page_template == 1): ?>
                                                    <div class="where-do-you-go register-information">
                                                        <div class="where-go-label register-info-title">Bạn muốn đi đâu?</div>
                                                        <div class="country">
                                                            <div class="label-where">Quốc gia:</div>
                                                            <div class="select-where">
                                                                <select class="form-control" name="country">
                                                                    <option value="#">Việt Nam</option>
                                                                    <option value="#">Thái Lan</option>
                                                                    <option value="#">Hàn Quốc</option>
                                                                    <option value="#">Anh</option>
                                                                    <option value="#">Pháp</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="city">
                                                            <div class="label-where">Thành phố:</div>
                                                            <div class="select-where">
                                                                <select class="form-control" name="country">
                                                                    <option value="#">Hà Nội</option>
                                                                    <option value="#">Hồ Chí Minh</option>
                                                                    <option value="#">Đà Nẵng</option>
                                                                    <option value="#">Đà Lạt</option>
                                                                    <option value="#">Sapa</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="search-name-company">
                                                            <input type="text" class="form-control"
                                                                   placeholder="Tìm theo tên công ty">
                                                            <div class="icon-search-company"><i class="fas fa-search"></i></div>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="sidebar customer-care hidden-sm hidden-xs">
                                                    <div class="customer-care-call-text">
                                                        Gọi điện để tư vấn
                                                    </div>
                                                    <div class="customer-number-phone">
                                                        <span class="icon-telephone"><i class="fas fa-phone"></i></span>
                                                        090 123 456
                                                    </div>
                                                    <div class="customer-care-call-text">
                                                        Hoặc để lại số điện thoại Trekvn sẽ gọi cho bạn
                                                    </div>
                                                    <input type="text" value="" placeholder="Số điện thoại của bạn...">
                                                </div>
                                                <div class="register-information hidden-sm hidden-xs">
                                                    <div class="register-info-title">Đăng kí nhận thông tin</div>
                                                    <div class="register-info-text">Hãy là người đầu tiên nhận được những tin
                                                        tức thú vị và những chương trình khuyến mãi nóng hổi nhất
                                                    </div>
                                                    <div class="form-register-info">
                                                        <?php $base_url = get_site_url(); ?>
                                                        <form role="form" method="post" action="<?php echo $base_url ?>/?na=s"
                                                              id="ajax-newsletter-left">
                                                            <div class="form-group">
                                                                <label for="name">Họ và tên</label>
                                                                <input type="text" class="form-control" id="name"
                                                                       placeholder="Name" name="nn" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="email">Email</label>
                                                                <input type="email" class="form-control" id="email"
                                                                       placeholder="Email" name="ne" required>
                                                            </div>
                                                            <button type="submit"
                                                                    class="btn btn-default register ajax-newsletter-left">Đăng
                                                                kí
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <section
                                    class=" col-lg-9  col-xs-12 <?php echo(!$is_company_template ? 'col-md-9 col-sm-12' : '') ?>">
                                <div class="row">

                                    <?php if ($is_company_template): ?>
                                    <div class="listing-page">
                                        <div class="border-list-item-tourist">
                                            <?php else: ?>
                                            <div class="entertainment-page-content news-page col-lg-12 col-xs-12">
                                                <div class="row">
                                                    <?php endif; ?>
                                                    <?php
                                                    $catID = $cat_obj->term_id;
                                                    $paged = (get_query_var('paged') ? get_query_var('paged') : 1);
                                                    $arg = array(
                                                        'orderby' => 'term_id',
                                                        'posts_per_page' => 5,
                                                        'order' => 'DESC',
                                                        'paged' => $paged,
                                                        'cat' => $catID);
                                                    $r = new WP_Query($arg);
                                                    ?>
                                                    <?php while ($r->have_posts()): ?>
                                                        <?php $r->the_post(); ?>
                                                        <?php if ($is_company_template==1): ?>
                                                            <article class="list-tourist col-lg-12 col-xs-12">
                                                                <div class="item-tourist ">
                                                                    <div class="item-content-tourist col-lg-4 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="image-item-tour-warrap">
                                                                                <div class="image-item">
                                                                                    <a href="<?php echo get_permalink(); ?>"
                                                                                       title="<?php the_title(); ?>">
                                                                                        <?php the_post_thumbnail(); ?>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="item-list-price-tourist col-lg-8 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="company-information">
                                                                                <div class="company-label">
                                                                                    <a href="<?php echo get_permalink(); ?>"
                                                                                       title="<?php the_title(); ?>"><?php the_title(); ?>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="company-description">
                                                                                    <?php
                                                                                    $short_description = get_field('short_description', get_the_ID());
                                                                                    if (strlen($short_description) > 100) {
                                                                                        $short_description = mb_substr($short_description, 0, 100) . '...';
                                                                                    }
                                                                                    echo($short_description);
                                                                                    ?>
                                                                                </div>
                                                                                <div class="button-view-detail">
                                                                                    <a href="<?php echo get_permalink(); ?>"
                                                                                       title="<?php the_title(); ?>">[<?php _e('[:en]See details[:vi]Xem chi tiế t[:]')?>]</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </article>
                                                        <?php else: ?>
                                                            <article
                                                                    class="entertainment-main col-lg-12 col-sm-12 col-xs-12">
                                                                <div class="row">
                                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="entertainment-image">
                                                                                <a href="<?php echo get_permalink(); ?>"
                                                                                   title="<?php the_title(); ?>">
                                                                                    <?php the_post_thumbnail(); ?>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="entertainment-content">
                                                                                <div class="entertainment-content-label">
                                                                                    <a href="<?php echo get_permalink(); ?>"
                                                                                       title="<?php the_title(); ?>"><?php the_title(); ?></a>
                                                                                </div>
                                                                                <div class="entertainment-content-detail">
                                                                                    <div class="entertainment-short-description">
                                                                                        <?php
                                                                                        $short_description = get_field('short_description', get_the_ID());
                                                                                        if (strlen($short_description) > 100) {
                                                                                            $short_description = mb_substr($short_description, 0, 100) . '...';
                                                                                        }
                                                                                        echo($short_description);
                                                                                        ?>
                                                                                    </div>
                                                                                    <div class="news-page-button">
                                                                            <span class="news-time">
                                                                                <i class="far fa-calendar-alt"></i>
                                                                                <?php $post = get_post(); ?>
                                                                                <?php echo date('d/m/Y', strtotime($post->post_date)) ?>
                                                                            </span>
                                                                                        <span class="news-button-view-detail">
                                                                                <a href="<?php echo get_permalink(); ?>"
                                                                                   title="<?php the_title(); ?>">[<?php _e('[:en]See details[:vi]Xem chi tiết[:]')?>]</a>
                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </article>
                                                        <?php endif; ?>
                                                        <?php wp_reset_postdata(); ?>
                                                    <?php endwhile; ?>
                                                    <?php if ($is_company_template): ?>
                                                </div>
                                                <?php endif; ?>
                                                <?php if (!$is_company_template): ?>
                                            </div>
                                        <?php endif; ?>

                                        </div>
                                        <div class="product-detail-page entertainment-page col-xs-12 col-lg-12">
                                            <div class="pagination-border">
                                                <ul class="pagination">
                                                    <?php

                                                    $total = $r->max_num_pages;
                                                    $current = get_query_var('paged');
                                                    $base = isset($base) ? $base : esc_url_raw(str_replace(999999999, '%#%', remove_query_arg('add-to-cart', get_pagenum_link(999999999, false))));
                                                    $format = isset($format) ? $format : '';

                                                    echo paginate_links(apply_filters('woocommerce_pagination_args', array( // WPCS: XSS ok.
                                                        'base' => $base,
                                                        'format' => $format,
                                                        'add_args' => false,
                                                        'current' => max(1, $current),
                                                        'total' => $total,
                                                        'prev_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                                                        'next_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                                                        'type' => 'list',
                                                        'end_size' => 3,
                                                        'mid_size' => 3,
                                                    )));

                                                    ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
get_footer(); ?>