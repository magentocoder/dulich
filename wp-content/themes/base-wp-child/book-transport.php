<?php
// Template Name: Book Transport
get_header(); ?>
<?php
$id = isset($_GET['id']) ? $_GET['id'] : '';
if ($id == '') {
    echo '<script> window.location.href= "' . home_url() . '";</script>';
}
$product = wc_get_product($id);
$name = $product->name;
$price = (int)get_post_meta($id, '_regular_price', true);
$qty = $product->get_stock_quantity();
$image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'single-post-thumbnail');
$imageUrl = $image[0];
$address = get_field('address', $id);
$phone_number = get_field('phone_number', $id);
$email = get_field('email', $id);
$website = get_field('website', $id);
global $wp;
$current_url = home_url($wp->request).'/?id='.$id.'&comment=s';
global $wpdb;
$city = $wpdb->get_results('SELECT DISTINCT meta_value FROM wp_postmeta WHERE meta_key LIKE "city" AND meta_value IS NOT NULL', ARRAY_A);
?>
    <main class="col-xs-12 col-xs-12 listing-page-border transport-page book-transport">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="main-content col-lg-12 col-xs-12">
                        <div class="row">
                            <aside class="col-lg-3 col-xs-12 ">
                                <div class="row">
                                    <div class="menu-left-mobile">
                                        <div class="col-xs-12 hidden-lg hidden-md">
                                            <div class="button-filter">Bộ lọc</div>
                                        </div>
                                        <div class="all-province-city">
                                            <div class="menu-left">
                                                <div class="filter-border">
                                                    <div class="filter-border">
                                                        <div class="filter-title">Tìm xe cho thuê</div>
                                                        <div class=""><br/>
                                                            <select id="city" class="form-control">
                                                                <option>Thành Phố</option>
                                                                <?php
                                                                foreach ($city as $item) {
                                                                    ?>
                                                                    <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                        <option><?= $item['meta_value']; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                            <br/>
                                                            <select id="district" class="form-control">
                                                                <option>Quận - Huyện</option>
                                                            </select>
                                                            <br/>
                                                            <select id="commune" class="form-control">
                                                                <option>Phường - Xã</option>
                                                            </select>
                                                            <br/>
                                                            <span>Ngày nhận xe</span>
                                                            <input id="time_start" type="date" class="form-control">
                                                            <br/>
                                                            <span>Ngày trả xe</span>
                                                            <input id="time_end" type="date" class="form-control">
                                                            <br/>
                                                            <span>Kiểu thuê xe</span><br/>
                                                            <input class="kieu_thue" type="checkbox" value="Xe tự lái"/>Xe
                                                            tự lái<br/>
                                                            <input class="kieu_thue" type="checkbox" value="Xe tự lái"/>Xe
                                                            có người lái<br/>
                                                            <br/>
                                                            <button type="button" class="btn btn-default"
                                                                    id="search-list-transport">
                                                                <span class="glyphicon glyphicon-search"></span> Tìm kiếm
                                                            </button>
                                                            <br/></div>
                                                        <br/>
                                                        <div class="filter-title">Tìm kiếm</div>
                                                        <span>Dòng xe</span><br/>
                                                        <input class="dong_xe" type="checkbox" value="Xe thường"/>Xe
                                                        thường<br/>
                                                        <input class="dong_xe" type="checkbox" value="Dcar Limousine"/>Dcar
                                                        Limousine<br/>
                                                        <br/>
                                                        <span>Loại xe</span><br/>
                                                        <input class="loai_xe" type="checkbox" value="4 chỗ"/>4 chỗ<br/>
                                                        <input class="loai_xe" type="checkbox" value="7 chỗ"/>7 chỗ<br/>
                                                        <input class="loai_xe" type="checkbox" value="9 chỗ"/>9 chỗ<br/>
                                                        <input class="loai_xe" type="checkbox" value="16 chỗ"/>16 chỗ<br/>
                                                        <input class="loai_xe" type="checkbox" value="29 chỗ"/>29 chỗ<br/>
                                                        <input class="loai_xe" type="checkbox" value="45 chỗ"/>45 chỗ<br/>
                                                        <br/>
                                                        <span>Xếp hạng</span><br/>
                                                        <input class="xep_hang_xe" type="checkbox" value="Tuyệt vời"/>Tuyệt
                                                        vời<br/>
                                                        <input class="xep_hang_xe" type="checkbox" value="Rất tôt"/>Rât
                                                        tốt<br/>
                                                        <input class="xep_hang_xe" type="checkbox" value="Tốt"/>Tốt<br/>
                                                        <br/>
                                                        <span>Tìm theo tên nhà cung cấp xe</span><br/>
                                                        <input type="text" class="form-control" id="nha_cung_cap"
                                                               placeholder="Tên nhà cung cấp ..."/>
                                                        <div class="filter-content"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <section class="col-lg-9 col-xs-12">
                                <div class="row">
                                    <div class="listing-page" id="old-listing-page">
                                        <div class="border-list-item-tourist">
                                            <?php if (isset($_POST) && !empty($_POST)): ?>
                                                <?php
                                                if (isset($_POST['use_cc_payment'])) {
                                                    $address = array(
                                                        'first_name' => $_POST['ten'],
                                                        'last_name' => $_POST['ten'],
                                                        'email' => $_POST['mail'],
                                                        'phone' => $_POST['sdt'],
                                                    );
                                                    $custom_field = array(
                                                        'ngay_di' => 'fromdate',
                                                        'ngay_ve' => 'todate',
                                                        'dia_chi_đon' => 'addresss_don',
                                                        'thoi_gian_don' => 'timefrom',
                                                        'thoi_gian_ve' => 'timeto',
                                                        'yeu_cau_khac' => 'note',
                                                    );
                                                    $order = wc_create_order();
                                                    $order->add_product($product, 1); //(get_product with id and next is for quantity)
                                                    $order->set_address($address, 'billing');
                                                    $order->set_address($address, 'shipping');
                                                    $order->set_payment_method('cod');
                                                    $order->calculate_totals();
                                                    foreach ($custom_field as $k => $f) {
                                                        update_field($k, $_POST[$f], $order->get_id());
                                                    }

                                                    WC()->cart->empty_cart();
                                                    echo "<p class='message-success'>Bạn đặt xe thành công</p>";
                                                    echo '<script>setTimeout(function () {window.location.href= "' . get_site_url() . '";},5000);</script>';
                                                }
                                                ?>
                                            <?php else: ?>
                                                <article class="list-tourist col-lg-12 col-xs-12">
                                                    <div class="item-tourist ">
                                                        <div class="item-content-tourist col-lg-4 col-xs-12">
                                                            <div class="row">
                                                                <div class="image-item-tour-warrap">
                                                                    <div class="image-item">
                                                                        <!--                                                                    <a href="#" title="#">-->
                                                                        <img src="<?php echo $imageUrl ?>"
                                                                             title="<?php echo $name ?>"
                                                                             alt="<?php echo $name ?>">
                                                                        <!--                                                                    </a>-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item-list-price-tourist col-lg-8 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-lg-5 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="item-detail-tourist">
                                                                            <div class="item-name-tourist item-detail-sub">
                                                                                <!--                                                                            <a href="#" title="#">-->
                                                                                <span class="name-tourist"><?php _e($name) ?></span>
                                                                                <!--                                                                            </a>-->
                                                                            </div>
                                                                            <div class="item-address item-detail-sub hidden-sm hidden-xs">
                                                                                <i class="fa fa-map-marker icon"
                                                                                   aria-hidden="true"></i>
                                                                                <?php echo $address ?>
                                                                            </div>
                                                                            <div class="item-phone-number item-detail-sub hidden-sm hidden-xs">
                                                                                <span class="glyphicon glyphicon-phone-alt icon"></span>
                                                                                <?php echo $phone_number ?>
                                                                            </div>
                                                                            <div class="item-email item-detail-sub hidden-sm hidden-xs">
                                                                                <i class="fa fa-envelope icon"
                                                                                   aria-hidden="true"></i>
                                                                                <?php echo $email ?>
                                                                            </div>
                                                                            <div class="item-website item-detail-sub hidden-sm hidden-xs">
                                                                                <span class="glyphicon glyphicon-globe icon"></span></i>
                                                                                <?php echo $website ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-7 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="item-detail-price-border">
                                                                            <div class="item-detail-price-label">Giá
                                                                            </div>
                                                                            <div class="item-detail-price price-not-special"><?php echo number_format($price, 0, '.', ','); ?> đ
                                                                            </div>
                                                                            <div class="review">
                                                                                <?php $rate =  round($product->get_average_rating() *2,1)?>
                                                                                <span class="review-percent"><?php echo number_format($rate,1)?></span>
                                                                                <?php $comments_count = wp_count_comments($id); ?>
                                                                                <span class="review-text">(<b><?php echo (int)$comments_count->total_comments; ?></b> nhận xét)</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item-list-price-tourist col-lg-8 col-xs-12">
                                                            <div class="row">
                                                                <div class="infor-free">
                                                                    <h4 class="title"><?php _e('[:en]We are free[:vi]Chúng tôi miễn phí[:]')?></h4>
                                                                    <ul>
                                                                        <li>
                                                                            <ul>
                                                                                <li>- <?php _e('[:en]Cancel booking[:vi]Chúng tôi miễn phí[:]')?></li>
                                                                                <li>- <?php _e('[:en]Change rental order[:vi]Thay đổi đặt thuê[:]')?></li>
                                                                            </ul>
                                                                        </li>
                                                                        <li>
                                                                            <ul>
                                                                                <li>- <?php _e('[:en]Theft insurance[:vi]Bảo hiểm mất trộm[:]')?></li>
                                                                                <li>- <?php _e('[:en]Insurance for damage and collision[:vi]Bảo hiểm thiệt hại và va chạm[:]')?></li>
                                                                            </ul>
                                                                        </li>
                                                                        <li>
                                                                            <ul>
                                                                                <li>- <?php _e('[:en]Airport surcharge[:vi]Phụ phí sân bay[:]')?></li>
                                                                                <li>- <?php _e('[:en]Local tax[:vi]Thuế địa phương[:]')?></li>
                                                                            </ul>
                                                                        </li>
                                                                        <li>
                                                                            <ul>
                                                                                <li>- <?php _e('[:en]Road fee / road permit fee[:vi]Phí đường độ/ Phí giấy phép đường bộ[:]')?>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="row">
                                                                <div class="form-book-transport">
                                                                    <h4>Bổ sung</h4>
                                                                    <?php echo do_shortcode('[contact-form-7 id="433" title="Book transport"]')?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php if ($qty < 5): ?>
<!--                                                        <div class="sales-percent quantity-low">Số lượng ít</div>-->
                                                    <?php elseif ($qty > 15): ?>
<!--                                                        <div class="sales-percent customer-like">Khách hàng ưu thích-->
<!--                                                        </div>-->
                                                    <?php endif; ?>
                                                </article>
                                            <?php endif; ?>
                                        </div>
                                        <div class="review-comment-border">
                                            <h4 class="review-conmment-label">Nhận xét của khách hàng</h4>
                                            <ul class="review-comment-content woocommerce">
                                                <?php
                                                $comments_args = array(
                                                    // Change the title of send button
                                                    'label_submit' => __('Send', 'textdomain'),
                                                    // Remove "Text or HTML to be displayed after the set of comment fields".
                                                    'comment_notes_after' => '',
                                                    // Redefine your own textarea (the comment body).
                                                    'comment_field' => '
                                                                <div class="comment-form-rating hotel_star"><label for="rating">' . _x('Đánh giá của bạn', 'noun') . '</label>
                                                                    <p class="stars"><span>
                                                                    <a class="star-1" href="#">1</a>
                                                                    <a class="star-2" href="#">2</a>
                                                                    <a class="star-3" href="#">3</a>
                                                                    <a class="star-4" href="#">4</a>
                                                                    <a class="star-5" href="#">5</a>
                                                                </span>
                                                                </p>
                                                                <select name="rating" id="rating' . $id . '" aria-required="true" required="" style="display: none;">
                                                                    <option value="">Xếp hạng…</option>
                                                                    <option value="5">5</option>
                                                                    <option value="4">4</option>
                                                                    <option value="3">3</option>
                                                                    <option value="2">2</option>
                                                                    <option value="1">1</option>
                                                                </select></div>
                                                                    <p class="comment-form-comment">
                                                                        <label for="comment">' . _x('Comment', 'noun') . '</label><br />
                                                                        <textarea id="comment' . $id . '" name="comment" aria-required="true"></textarea>
                                                                    </p>
                                                                    <input type="hidden" name="my_redirect_to" id="my_redirect_to' . $id . '" value="' . $current_url . '">',
                                                    'submit_button' => '<input name="%1$s" type="submit" id="%2$s' . $id . '" class="%3$s" value="Gửi" />',
                                                    'id_form' => 'commentform' . $id
                                                );
                                                comment_form($comments_args, $id);
                                                ?>

                                            </ul>
                                        </div>
                                    </div>
                                    <div class="listing-page woocommerce" id="search-listing-page" style="display:none;">
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $("#search-list").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var price = $(".price:checked").val();
                var url = '<?php echo get_site_url(); ?>/search-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-transport").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var time_start = null;
                if ($("#time_start").val()) {
                    var time_start_date = new Date($("#time_start").val());
                    var moth = time_start_date.getMonth()+1;
                    time_start = moth+ "/" + time_start_date.getDate() + "/" + time_start_date.getFullYear();
                }
                var time_end = null;
                if ($("#time_end").val()) {
                    var time_end_date = new Date($("#time_end").val());
                    var month = time_end_date.getMonth()+1;
                    time_end = month+ "/" + time_end_date.getDate() + "/" + time_end_date.getFullYear();
                    ;
                }

                var kieu_thue = $(".kieu_thue:checked").val();
                var dong_xe = $(".dong_xe:checked").val();
                var loai_xe = $(".loai_xe:checked").val();
                var xep_hang_xe = $(".xep_hang_xe:checked").val();
                var nha_cung_cap = $("#nha_cung_cap").val();
                var url = '<?php echo get_site_url(); ?>/search-transport-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'time_start': time_start,
                            'time_end': time_end,
                            'kieu_thue': kieu_thue,
                            'dong_xe': dong_xe,
                            'loai_xe': loai_xe,
                            'xep_hang_xe': xep_hang_xe,
                            'nha_cung_cap': nha_cung_cap
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-transport-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-food").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var loai_co_so = $(".loai_co_so:checked").val();
                var food = $(".food:checked").val();
                var fit = $(".fit:checked").val();
                var food_price = $(".food_price:checked").val();
                var ten_nha_hang = $("#ten_nha_hang").val();
                var url = '<?php echo get_site_url(); ?>/search-food-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'loai_co_so': loai_co_so,
                            'food': food,
                            'fit': fit,
                            'food_price': food_price,
                            'ten_nha_hang': ten_nha_hang
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-transport-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-hotel").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#ten-khach-san").val();
                var price = $(".price:checked").val();
                var rate = $(".rate:checked").val();
                var rank = $(".rank:checked").val();
                var url = '<?php echo get_site_url(); ?>/search-hotel-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                            'rate': rate,
                            'rank': rank,
                            'ten': ten
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $(".pagination").hide();
                            $(".review-comment-border").find('ul.pagination').show();
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-tour").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#ten-khach-san").val();
                var price = $("#price").val();
                var rate = $("#rate").val();
                var rank = $("#rank").val();
                var url = '<?php echo get_site_url(); ?>/search-tour-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                            'rate': rate,
                            'rank': rank,
                            'ten': ten
                        },
                        success: function (response) {
                            $(".entertainment-page-content").remove();
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-company").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#ten-khach-san").val();
                var price = $("#price").val();
                var rate = $("#rate").val();
                var rank = $("#rank").val();
                var url = '<?php echo get_site_url(); ?>/search-company-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                            'rate': rate,
                            'rank': rank,
                            'ten': ten
                        },
                        success: function (response) {
                            $(".entertainment-page-content").remove();
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
        });
    </script>
<?php
get_footer();
