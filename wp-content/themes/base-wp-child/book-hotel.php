<?php
// Template Name: Book Hotel
get_header(); ?>
<?php
$id = isset($_GET['id']) ? $_GET['id'] : '';
if ($id == '') {
    echo '<script> window.location.href= "' . home_url() . '";</script>';
}
$product = wc_get_product($id);
$name = $product->name;
$price = (int)get_post_meta($id, '_regular_price', true);
$sale = (int)get_post_meta($id, '_sale_price', true);
$has_sale = false;
if ($sale != 0 && $sale < $price) {
    $has_sale = true;
}
$image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'single-post-thumbnail');
$imageUrl = $image[0];
$address = get_field('address', $id);
$phone_number = get_field('phone_number', $id);
$email = get_field('email', $id);
$website = get_field('website', $id);
global $wpdb;
$city = $wpdb->get_results('SELECT DISTINCT meta_value FROM wp_postmeta WHERE meta_key LIKE "city" AND meta_value IS NOT NULL', ARRAY_A);

$default_wishlists = is_user_logged_in() ? YITH_WCWL()->get_wishlists(array('is_default' => true)) : false;
if (!empty($default_wishlists)) {
    $default_wishlist = $default_wishlists[0]['ID'];
} else {
    $default_wishlist = false;
}

// exists in default wishlist
$exists = YITH_WCWL()->is_product_in_wishlist($id, $default_wishlist);


if ($exists) {
    $action = 'remove_from_wishlist';
    $la = _x('[:en]Delete from list[:vi]Xóa khỏi danh sách[:]', 'noun');
    $cl = 'rmtowishlist';
} else {
    $action = 'add_to_wishlist';
    $la = _x('[:en]Save to after[:vi]Lưu vào để sau[:]', 'noun');
    $cl = 'addtowishlist';
}

?>
    <main class="col-xs-12 col-xs-12 listing-page-border book-hotel-form">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="main-content col-lg-12 col-xs-12">
                        <div class="row">
                            <aside class="col-lg-3 col-xs-12 ">
                                <div class="row">
                                    <div class="menu-left-mobile">
                                        <div class="col-xs-12 hidden-lg hidden-md">
                                            <div class="button-filter">Bộ lọc</div>
                                        </div>
                                        <div class="all-province-city">
                                            <div class="menu-left">
                                                <div class="menu-left-mobile">
                                                    <div class="col-xs-12 hidden-lg hidden-md">
                                                        <div class="button-filter">Bộ lọc</div>
                                                    </div>
                                                    <div class="all-province-city">
                                                        <div class="menu-left">
                                                            <div class="sidebar hidden-sm hidden-xs">
                                                                <div class="mapp-wappar">
                                                                    <div class="map-place">
                                                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6759412419965!2d105.84115881430768!3d21.00562329394503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2sHanoi+University+of+Science+and+Technology!5e0!3m2!1sen!2s!4v1523983980703"
                                                                                width="100%" height="150px" frameborder="0"
                                                                                style="border:0"
                                                                                allowfullscreen></iframe>
                                                                    </div>
                                                                </div>
                                                                <div class="map-sticker">
                                                                    <span>Hiển thị bản đồ</span>
                                                                </div>
                                                            </div>
                                                            <div class="filter-border">
                                                                <div class="filter-title">Chọn lọc theo</div>
                                                                <br/>
                                                                <select id="city" class="form-control">
                                                                    <option>Thành Phố</option>
                                                                    <?php
                                                                    foreach ($city as $item) {
                                                                        ?>
                                                                        <?php if (!empty($item['meta_value']) && $item['meta_value'] != '') { ?>
                                                                            <option><?= $item['meta_value']; ?></option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <br/>
                                                                <select id="district" class="form-control">
                                                                    <option>Quận - Huyện</option>
                                                                </select>
                                                                <br/>
                                                                <select id="commune" class="form-control">
                                                                    <option>Phường - Xã</option>
                                                                </select>
                                                                <br/>
                                                                <span>Giá tiền</span><br/>
                                                                <input class="price" type="checkbox" value="0 đ - 1.000.000 đ">0 đ -
                                                                1.000.000 đ<br>
                                                                <input class="price" type="checkbox" value="1.000.000 đ - 2.000.000 đ">1.000.000 đ
                                                                - 2.000.000 đ<br>
                                                                <input class="price" type="checkbox" value="2.000.000 đ - 3.000.000 đ">2.000.000 đ
                                                                - 3.000.000 đ<br>
                                                                <input class="price" type="checkbox" value="3.000.000 đ - 4.000.000 đ">3.000.000 đ
                                                                - 4.000.000 đ<br>
                                                                <input class="price" type="checkbox" value="4.000.000 đ trở lên">4.000.000 đ
                                                                trở lên<br>
                                                                <br/>
                                                                <span>Đánh giá của khách sạn</span><br/>
                                                                <input class="rate" type="checkbox" value="Tuyệt hảo"/>Tuyệt hảo<br/>
                                                                <input class="rate" type="checkbox" value="Tuyệt hảo"/>Rất tốt<br/>
                                                                <input class="rate" type="checkbox" value="Tuyệt hảo"/>Tốt<br/>
                                                                <input class="rate" type="checkbox" value="Tuyệt hảo"/>Dễ chịu<br/>
                                                                <br/>
                                                                <span>Hạng của khách sạn</span><br/>
                                                                <input class="rank" type="checkbox" value="1 sao"/>1 sao<br/>
                                                                <input class="rank" type="checkbox" value="2 sao"/>2 sao<br/>
                                                                <input class="rank" type="checkbox" value="3 sao"/>3 sao<br/>
                                                                <input class="rank" type="checkbox" value="4 sao"/>4 sao<br/>
                                                                <input class="rank" type="checkbox" value="5 sao"/>5 sao<br/>
                                                                <br/>
                                                                <button type="button" class="btn btn-default" id="search-list-hotel">
                                                                    <span class="glyphicon glyphicon-search"></span> Tìm
                                                                </button>
                                                                <br/><br/>
                                                                <span>Tìm theo tên khách sạn</span><br/>
                                                                <input type="text" class="form-control" id="ten-khach-san"
                                                                       placeholder="Tìm theo tên nhà hàng ..."/>
                                                                <!--                                                <div class="filter-content"></div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <section class="col-lg-9 col-xs-12">
                                <div class="row">
                                    <div class="listing-page" id="old-listing-page">
                                        <div class="border-list-item-tourist">
                                            <?php if (isset($_POST) && !empty($_POST)): ?>
                                                <?php
//                                                if (isset($_POST['use_cc_payment'])) {
                                                    $address = array(
                                                        'first_name' => $_POST['ten'],
                                                        'last_name' => $_POST['ten'],
                                                        'email' => $_POST['mail'],
                                                        'phone' => $_POST['sdt'],
                                                    );
                                                    $custom_field = array(
                                                            'di_lam'=>'di_lam'
                                                    );
                                                    $order = wc_create_order();
                                                    $order->add_product($product, 1); //(get_product with id and next is for quantity)
                                                    $order->set_address($address, 'billing');
                                                    $order->set_address($address, 'shipping');
                                                    $order->set_payment_method('cod');
                                                    $order->calculate_totals();
                                                    foreach ($custom_field as $k=>$f){
                                                        update_field($k,$_POST[$f],$order->get_id());
                                                    }

                                                    WC()->cart->empty_cart();
                                                    echo "<p class='message-success'>Bạn đặt khách sạn thành công</p>";
                                                    echo '<script>setTimeout(function () {window.location.href= "' . get_site_url() . '";},5000);</script>';
//                                                }
                                                ?>
                                            <?php else: ?>
                                                <article class="list-tourist col-lg-12 col-xs-12">
                                                    <div class="item-tourist ">
                                                        <div class="item-content-tourist col-lg-3 col-xs-12">
                                                            <div class="row">
                                                                <div class="image-item-tour-warrap">
                                                                    <div class="image-item">
                                                                        <!--                                                                    <a href="#" title="#">-->
                                                                        <img src="<?php echo $imageUrl ?>"
                                                                             title="<?php echo $name ?>"
                                                                             alt="<?php echo $name ?>">
                                                                        <!--                                                                    </a>-->
                                                                    </div>
                                                                    <div class="wishlist <?php echo $cl ?>" data-id="<?php echo $id; ?>" data-type="simple"
                                                                         data-action="<?php echo $action; ?>">
                                                                        <i class="fa fa-heart <?php echo($exists ? 'active' : '') ?>" aria-hidden="true"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item-list-price-tourist col-lg-9 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="item-detail-tourist">
                                                                            <div class="item-name-tourist item-detail-sub">
                                                                                <!--                                                                            <a href="#" title="#">-->
                                                                                <span class="name-tourist"><?php _e($name) ?></span>
                                                                                <!--                                                                                <span class="best-sale">Bán chạy nhất</span>-->
                                                                                <!--                                                                            </a>-->
                                                                            </div>
                                                                            <div class="item-address item-detail-sub hidden-sm hidden-xs">
                                                                                <i class="fa fa-map-marker icon"
                                                                                   aria-hidden="true"></i>
                                                                                <?php echo $address ?>
                                                                            </div>
                                                                            <div class="item-phone-number item-detail-sub hidden-sm hidden-xs">
                                                                                <span class="glyphicon glyphicon-phone-alt icon"></span>
                                                                                <?php echo $phone_number ?>
                                                                            </div>
                                                                            <div class="item-email item-detail-sub hidden-sm hidden-xs">
                                                                                <i class="fa fa-envelope icon"
                                                                                   aria-hidden="true"></i>
                                                                                <?php echo $email ?>
                                                                            </div>
                                                                            <div class="item-website item-detail-sub hidden-sm hidden-xs">
                                                                                <span class="glyphicon glyphicon-globe icon"></span></i>
                                                                                <?php echo $website ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="item-detail-price-border">
                                                                            <div class="item-detail-price-label">Giá
                                                                            </div>
                                                                            <div class="price-special-border">
                                                                                <?php if ($has_sale): ?>
                                                                                    <div class="item-detail-price-special"><?php echo number_format($price, 0, '.', ','); ?> đ
                                                                                    </div>
                                                                                    <div class="item-detail-price"><?php echo number_format($sale, 0, '.', ','); ?> đ
                                                                                    </div>
                                                                                <?php else: ?>
                                                                                    <div class="item-detail-price price-not-special"><?php echo number_format($price, 0, '.', ',') ?> đ
                                                                                    </div>
                                                                                <?php endif; ?>
                                                                            </div>
                                                                            <div class="review">
                                                                                <span class="review-percent">7.0</span>
                                                                                <?php $comments_count = wp_count_comments($id); ?>
                                                                                <span class="review-text">(<b><?php echo (int)$comments_count->total_comments; ?></b> nhận xét)</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-book-hotel">
                                                        <?php echo do_shortcode('[contact-form-7 id="429" title="Book Hotel"]')?>


                                                    </div>
                                                    <?php if ($has_sale): ?>
                                                        <div class="sales-percent">
                                                            -<?php echo (int)(100 - ($sale * 100 / $price)) ?>%
                                                        </div>
                                                    <?php endif; ?>
                                                </article>
                                            <?php endif; ?>

                                        </div>
                                    </div>
                                    <div class="listing-page woocommerce" id="search-listing-page" style="display:none;">
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $("#search-list").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var price = $(".price:checked").val();
                var url = '<?php echo get_site_url(); ?>/search-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-transport").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var time_start = null;
                if ($("#time_start").val()) {
                    var time_start_date = new Date($("#time_start").val());
                    var moth = time_start_date.getMonth()+1;
                    time_start = moth+ "/" + time_start_date.getDate() + "/" + time_start_date.getFullYear();
                }
                var time_end = null;
                if ($("#time_end").val()) {
                    var time_end_date = new Date($("#time_end").val());
                    var month = time_end_date.getMonth()+1;
                    time_end = month+ "/" + time_end_date.getDate() + "/" + time_end_date.getFullYear();
                    ;
                }

                var kieu_thue = $(".kieu_thue:checked").val();
                var dong_xe = $(".dong_xe:checked").val();
                var loai_xe = $(".loai_xe:checked").val();
                var xep_hang_xe = $(".xep_hang_xe:checked").val();
                var nha_cung_cap = $("#nha_cung_cap").val();
                var url = '<?php echo get_site_url(); ?>/search-transport-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'time_start': time_start,
                            'time_end': time_end,
                            'kieu_thue': kieu_thue,
                            'dong_xe': dong_xe,
                            'loai_xe': loai_xe,
                            'xep_hang_xe': xep_hang_xe,
                            'nha_cung_cap': nha_cung_cap
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-transport-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-food").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var loai_co_so = $(".loai_co_so:checked").val();
                var food = $(".food:checked").val();
                var fit = $(".fit:checked").val();
                var food_price = $(".food_price:checked").val();
                var ten_nha_hang = $("#ten_nha_hang").val();
                var url = '<?php echo get_site_url(); ?>/search-food-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'loai_co_so': loai_co_so,
                            'food': food,
                            'fit': fit,
                            'food_price': food_price,
                            'ten_nha_hang': ten_nha_hang
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-transport-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-hotel").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#ten-khach-san").val();
                var price = $(".price:checked").val();
                var rate = $(".rate:checked").val();
                var rank = $(".rank:checked").val();
                var url = '<?php echo get_site_url(); ?>/search-hotel-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                            'rate': rate,
                            'rank': rank,
                            'ten': ten
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $(".pagination").hide();
                            $(".review-comment-border").find('ul.pagination').show();
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-tour").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#ten-khach-san").val();
                var price = $("#price").val();
                var rate = $("#rate").val();
                var rank = $("#rank").val();
                var url = '<?php echo get_site_url(); ?>/search-tour-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                            'rate': rate,
                            'rank': rank,
                            'ten': ten
                        },
                        success: function (response) {
                            $(".entertainment-page-content").remove();
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-company").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#ten-khach-san").val();
                var price = $("#price").val();
                var rate = $("#rate").val();
                var rank = $("#rank").val();
                var url = '<?php echo get_site_url(); ?>/search-company-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                            'rate': rate,
                            'rank': rank,
                            'ten': ten
                        },
                        success: function (response) {
                            $(".entertainment-page-content").remove();
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if(w < 991){
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
        });
    </script>
<?php
get_footer();
