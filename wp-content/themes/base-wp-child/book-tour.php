<?php
// Template Name: Book tour
get_header(); ?>
<?php
$id = isset($_GET['id']) ? $_GET['id'] : '';
if ($id == '') {
    echo '<script> window.location.href= "' . home_url() . '";</script>';
}
$product = wc_get_product($id);
$name = $product->name;
$gia_ngl = (int)get_post_meta($id, '_regular_price', true);;
$gia_tr = (int)get_field('gia_tre_em', $id);
$thoi_han = get_field('thoi_han', $id);

?>
    <main class="col-xs-12 col-xs-12 listing-page-border form-book-tour">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="main-content col-lg-12 col-xs-12">
                        <div class="row">

                            <aside class="col-lg-3 col-xs-12 ">
                                <div class="row">
                                    <div class="menu-left hidden-sm hidden-xs">
                                        <div class="sidebar customer-care">
                                            <div class="customer-care-call-text">
                                                <?php _e('[:en]Call for advice[:vi]Gọi điện để tư vấn[:]') ?>
                                            </div>
                                            <div class="customer-number-phone">
                                                <span class="icon-telephone"><i class="fas fa-phone"></i></span>
                                                090 123 456
                                            </div>
                                            <div class="customer-care-call-text">
                                                <?php _e('[:en]Or leave a phone number Trekvn will call you[:vi]Hoặc để lại số điện thoại Trekvn sẽ gọi cho bạn[:]'); ?>

                                            </div>
                                            <input type="text" value=""
                                                   placeholder="<?php _e('[:en]Your phone . . .[:vi]Số điện thoại của bạn...[:]') ?>">
                                        </div>
                                        <div class="register-information">
                                            <div class="register-info-title"><?php _e('[:en]Sign up for information[:vi]Đăng ký nhận thông tin[:]') ?></div>
                                            <div class="register-info-text"><?php _e('[:en]Be the first to receive exciting news and the hottest promotions[:vi]Hãy là người đầu tiên nhận được những tin
                                                tức thú vị và những chương trình khuyến mãi nóng hổi nhất[:]') ?>
                                            </div>
                                            <div class="form-register-info">
                                                <?php $base_url = get_site_url(); ?>
                                                <form role="form" method="post" action="<?php echo $base_url ?>/?na=s"
                                                      id="ajax-newsletter-left">
                                                    <div class="form-group">
                                                        <label for="name"><?php _e('[:en]Full name[:vi]Họ và tên[:]') ?></label>
                                                        <input type="text" class="form-control" id="name"
                                                               placeholder="<?php _e('[:en]Full name[:vi]Họ và tên[:]') ?>"
                                                               required name="nn">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input type="email" class="form-control" id="email"
                                                               placeholder="Email" required name="ne">
                                                    </div>
                                                    <button type="submit"
                                                            class="btn btn-default register ajax-newsletter-left">
                                                        <?php _e('[:en]Sign up[:vi]Đăng kí[:]') ?>
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <section class="col-lg-9 col-xs-12">
                                <div class="row">
                                    <div class="listing-page" id="old-listing-page">
                                        <div class="border-list-item-tourist form-book-tour-wapper">
                                            <?php if (isset($_POST) && !empty($_POST)): ?>
                                                <?php
//    if (isset($_POST['use_cc_payment'])) {
                                                $address = array(
                                                    'first_name' => $_POST['ten'],
                                                    'last_name' => $_POST['ten'],
                                                    'email' => $_POST['mail'],
                                                    'phone' => $_POST['sdt'],
                                                );
                                                $custom_field = array(
                                                    'ngay_di' => 'ngay_di',
                                                    'so_nguoi_lon' => 'n_ngl',
                                                    'so_tre_em' => 'n_tr',
                                                    'dia_chi_đon' => 'dia_chi_đon',
                                                    'gia_nguoi_lon' => 'gia_ngl',
                                                    'gia_tre_em' => 'gia_tr',
                                                    'yeu_cau_khac' => 'yeu_cau_khac',
                                                    'ma_khuyen_mai' => 'ma_khuyen_mai',
                                                );
                                                $order = wc_create_order();
                                                $tong_c = $_POST['gia_ngl'] * $_POST['n_ngl'] + $_POST['gia_tr'] + $_POST['n_tr'];
                                                $product->set_price($tong_c);
                                                $order->add_product($product, 1); //(get_product with id and next is for quantity)
                                                $order->set_address($address, 'billing');
                                                $order->set_address($address, 'shipping');
                                                $order->set_payment_method('cod');
//        $order->add_coupon('Fresher','10','2');
                                                $order->calculate_totals();
                                                foreach ($custom_field as $k => $f) {
                                                    update_field($k, $_POST[$f], $order->get_id());
                                                }

                                                WC()->cart->empty_cart();
                                                echo "Bạn đặt tour thành công";
                                                echo '<script>setTimeout(function () {window.location.href= "' . get_site_url() . '";},5000);</script>';
//    }
                                                ?>
                                            <?php else: ?>
                                                <article class="list-tourist col-lg-12 col-xs-12">
                                                    <h4 class="title-form-book-tour">
                                                        <?php _e('[:en]Reclaim number of seats and contact information[:vi]Xác nhận lại số chỗ và thông tin liên hệ[:]') ?>
                                                    </h4>
                                                    <?php echo do_shortcode('[contact-form-7 id="432" title="Book Tour"]') ?>

                                                </article>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="listing-page woocommerce" id="search-listing-page"
                                         style="display:none;">
                                    </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script>
        jQuery('.name-tour.cf7').text('<?php _e($name)?>');
        jQuery('.time-tour.cf7').html('<label>Thời gian:</label><?php echo $thoi_han; ?>');
        jQuery('.price-adults.cf7').append('<label>Giá người lớn:</label><?php echo number_format($gia_ngl, 0, '.', ',') ?>đ');
        jQuery('.price-youth.cf7').append('<label>Giá trẻ em:</label><?php echo number_format($gia_tr, 0, '.', ',') ?>đ');
        jQuery('.price-adult-sub.cf7').append('<label id="t_ngl">0 Người lớn x </label> <?php echo number_format($gia_ngl, 0, '.', ',') ?>đ');
        jQuery('.price-youth-sub.cf7').append('<label id="t_tr">0 Trẻ em x </label> <?php echo number_format($gia_tr, 0, '.', ',') ?>đ');
        jQuery('#gia_ngl').val('<?php echo $gia_ngl ?>');
        jQuery('#gia_tr').val('<?php echo $gia_tr ?>');
        jQuery(document).ready(function ($) {
            $('body').on('click', function () {
                var n_ngl = parseInt($("#n_ngl").val());
                var n_tr = parseInt($("#n_tr").val());
                if (n_ngl < 0 || n_tr < 0) {
                    alert("Số người không nhỏ hơn không");
                    $("#n_tr").val(0);
                    $("#n_ngl").val(0);
                    return false;
                }
                var p_tr = parseInt($("#gia_tr").val());
                var p_ngl = parseInt($("#gia_ngl").val());
                $("#t_tr").text(n_tr + ' Trẻ em x');
                $("#t_ngl").text(n_ngl + ' Người lớn x');
                var tong = n_ngl * p_ngl + n_tr * p_tr;
                $("#total_p").val(tong);
                tong = number_format(tong, 0, '.', ',');
                $("#total_t").text(tong + ' đ');
            });

            function number_format(number, decimals, dec_point, thousands_sep) {
                var n = number,
                    c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
                var d = dec_point == undefined ? "," : dec_point;
                var t = thousands_sep == undefined ? "." : thousands_sep,
                    s = n < 0 ? "-" : "";
                var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                    j = (j = i.length) > 3 ? j % 3 : 0;
                return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
            }
        });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $("#search-list").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var price = $(".price:checked").val();
                var url = '<?php echo get_site_url(); ?>/search-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if (w < 991) {
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-transport").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var time_start = null;
                if ($("#time_start").val()) {
                    var time_start_date = new Date($("#time_start").val());
                    var moth = time_start_date.getMonth() + 1;
                    time_start = moth + "/" + time_start_date.getDate() + "/" + time_start_date.getFullYear();
                }
                var time_end = null;
                if ($("#time_end").val()) {
                    var time_end_date = new Date($("#time_end").val());
                    var month = time_end_date.getMonth() + 1;
                    time_end = month + "/" + time_end_date.getDate() + "/" + time_end_date.getFullYear();
                    ;
                }

                var kieu_thue = $(".kieu_thue:checked").val();
                var dong_xe = $(".dong_xe:checked").val();
                var loai_xe = $(".loai_xe:checked").val();
                var xep_hang_xe = $(".xep_hang_xe:checked").val();
                var nha_cung_cap = $("#nha_cung_cap").val();
                var url = '<?php echo get_site_url(); ?>/search-transport-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'time_start': time_start,
                            'time_end': time_end,
                            'kieu_thue': kieu_thue,
                            'dong_xe': dong_xe,
                            'loai_xe': loai_xe,
                            'xep_hang_xe': xep_hang_xe,
                            'nha_cung_cap': nha_cung_cap
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-transport-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if (w < 991) {
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-food").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var loai_co_so = $(".loai_co_so:checked").val();
                var food = $(".food:checked").val();
                var fit = $(".fit:checked").val();
                var food_price = $(".food_price:checked").val();
                var ten_nha_hang = $("#ten_nha_hang").val();
                var url = '<?php echo get_site_url(); ?>/search-food-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'loai_co_so': loai_co_so,
                            'food': food,
                            'fit': fit,
                            'food_price': food_price,
                            'ten_nha_hang': ten_nha_hang
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-transport-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if (w < 991) {
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-hotel").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#ten-khach-san").val();
                var price = $(".price:checked").val();
                var rate = $(".rate:checked").val();
                var rank = $(".rank:checked").val();
                var url = '<?php echo get_site_url(); ?>/search-hotel-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                            'rate': rate,
                            'rank': rank,
                            'ten': ten
                        },
                        success: function (response) {
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $(".pagination").hide();
                            $(".review-comment-border").find('ul.pagination').show();
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if (w < 991) {
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-tour").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#ten-khach-san").val();
                var price = $("#price").val();
                var rate = $("#rate").val();
                var rank = $("#rank").val();
                var url = '<?php echo get_site_url(); ?>/search-tour-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                            'rate': rate,
                            'rank': rank,
                            'ten': ten
                        },
                        success: function (response) {
                            $(".entertainment-page-content").remove();
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if (w < 991) {
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
            $("#search-list-company").click(function () {
                var city = $("#city").val();
                var district = $("#district").val();
                var commune = $("#commune").val();
                var ten = $("#ten-khach-san").val();
                var price = $("#price").val();
                var rate = $("#rate").val();
                var rank = $("#rank").val();
                var url = '<?php echo get_site_url(); ?>/search-company-listing.php';
                $.ajax({
                        'type': 'post',
                        'url': url,
                        'data': {
                            'city': city,
                            'district': district,
                            'commune': commune,
                            'price': price,
                            'rate': rate,
                            'rank': rank,
                            'ten': ten
                        },
                        success: function (response) {
                            $(".entertainment-page-content").remove();
                            $("#old-listing-page").remove();
                            $(".toolbar").hide();
                            var result = $('<div />').append(response).find('#search-result').html();
                            $("#search-listing-page").html(result);
                            $("#search-listing-page").show();
                            var w = window.innerWidth;
                            if (w < 991) {
                                $('.menu-left-mobile .all-province-city').hide();
                            }
                        }
                    }
                );
            });
        });
    </script>
<?php
get_footer();
