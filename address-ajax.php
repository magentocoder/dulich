<?php
global $wpdb;
require('wp-load.php');
if ($_POST) {
    if (!empty($_POST['city'])) {
        $posts = $wpdb->get_results('SELECT DISTINCT post_id FROM wp_postmeta WHERE meta_key LIKE "city" AND meta_value="' . $_POST['city'] . '"', ARRAY_A);
//    $posts = $wpdb->get_results('SELECT DISTINCT post_id FROM wp_postmeta WHERE meta_key LIKE "city" AND meta_value="Hà Nội"', ARRAY_A);
        $postId = array();
        foreach ($posts as $post) {
            $postId[] = $post['post_id'];
        }
        $postIdText = implode(',', $postId);
        $district = $wpdb->get_results('SELECT DISTINCT meta_value FROM wp_postmeta WHERE meta_key LIKE "district" AND post_id IN (' . $postIdText . ')', ARRAY_A);
    }

    if (!empty($_POST['district'])) {
        $posts = $wpdb->get_results('SELECT DISTINCT post_id FROM wp_postmeta WHERE meta_key LIKE "district" AND meta_value="' . $_POST['district'] . '"', ARRAY_A);
        $postId = array();
        foreach ($posts as $post) {
            $postId[] = $post['post_id'];
        }
        $postIdText = implode(',', $postId);
        $commune = $wpdb->get_results('SELECT DISTINCT meta_value FROM wp_postmeta WHERE meta_key LIKE "commune" AND post_id IN (' . $postIdText . ')', ARRAY_A);
    }
}
?>
<div class="option-search">
    <div id="district">
        <?php
        if (!empty($district) && is_array($district) && count($district) > 0) {
            ?>
            <option>Quận - Huyện</option>
            <?php
            foreach ($district as $dist) {
                if (!empty($dist['meta_value']) && $dist['meta_value'] != '') {
                    ?>
                    <option><?= $dist['meta_value']; ?></option>
                    <?php
                }
            }
        }
        ?>
    </div>
    <div id="commnune">
        <?php
        if (!empty($commune) && is_array($commune) && count($commune) > 0) {
            ?>
            <option>Phường - Xã</option>
            <?php
            foreach ($commune as $comm) {
                if (!empty($comm['meta_value']) && $comm['meta_value'] != '') {
                    ?>
                    <option><?= $comm['meta_value']; ?></option>
                    <?php
                }
            }
        }
        ?>
    </div>
</div>

