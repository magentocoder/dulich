<?php
if (!empty($_POST['s'])) {
?>
<?php
require('wp-load.php');
$params = array();
$params[] = array(
    'post_type' => 'product',
    'meta_query' => [
        [
            'key' => 'city',
            'value' => $_POST['s'],
            'compare' => 'LIKE'
        ],
        [
            'key' => 'district',
            'value' => $_POST['s'],
            'compare' => 'LIKE'
        ],
        [
            'key' => 'commune',
            'value' => $_POST['s'],
            'compare' => 'LIKE'
        ],
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'term_id', //This is optional, as it defaults to 'term_id'
                'terms' => 19,
                'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
            )
        )
    ]
);
$params[] = array(
    'post_type' => 'product',
    'meta_query' => [
        [
            'key' => 'city',
            'value' => $_POST['s'],
            'compare' => 'LIKE'
        ],
        [
            'key' => 'district',
            'value' => $_POST['s'],
            'compare' => 'LIKE'
        ],
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'term_id', //This is optional, as it defaults to 'term_id'
                'terms' => 19,
                'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
            )
        )
    ]
);
$params[] = array(
    'post_type' => 'product',
    'meta_query' => [
        [
            'key' => 'city',
            'value' => $_POST['s'],
            'compare' => 'LIKE'
        ]
    ],
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id', //This is optional, as it defaults to 'term_id'
            'terms' => 19,
            'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
        )
    )
);
$params[] = array(
    'post_type' => 'product',
    'meta_query' => [
        [
            'key' => 'district',
            'value' => $_POST['s'],
            'compare' => 'LIKE'
        ]
    ],
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id', //This is optional, as it defaults to 'term_id'
            'terms' => 19,
            'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
        )
    )
);
$params[] = array(
    'post_type' => 'product',
    'meta_query' => [
        [
            'key' => 'commune',
            'value' => $_POST['s'],
            'compare' => 'LIKE'
        ]
    ],
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id', //This is optional, as it defaults to 'term_id'
            'terms' => 19,
            'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
        )
    )
);
$params[] = array(
    'post_type' => 'product',
    's' => $_POST['s'],
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id', //This is optional, as it defaults to 'term_id'
            'terms' => 19,
            'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
        )
    )
);
$result = array();
foreach ($params as $key => $value) {
    $wc_query = new WP_Query($value);
    if ($wc_query->have_posts()) {
        $posts = $wc_query->posts;
        foreach ($posts as $post) {
            $id = $post->ID;
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'single-post-thumbnail');
            $imageUrl = $image[0];
            $city = get_field('city', $id);
            $district = get_field('district', $id);
            $commune = get_field('commune', $id);
            $address = get_field('address', $id);
            $phone_number = get_field('phone_number', $id);
            $email = get_field('email', $id);
            $website = get_field('website', $id);
            $price = get_field('_regular_price', $id);
            $title = $post->post_title;
            $content = $post->post_content;
            $productLink = get_permalink($id);
            $map = get_field('google_map_url', $id);
            $result[] = [
                'id' => $post->ID,
                'image' => $imageUrl,
                'city' => $city,
                'district' => $district,
                'commune' => $commune,
                'address' => $address,
                'phone_number' => $phone_number,
                'email' => $email,
                'website' => $website,
                'price' => $price,
                'title' => $title,
                'content' => $content,
                'productlink' => $productLink,
                'map' => $map
            ];
        }
        break;
    }
}

?>
<div id="search-transport-result">
    <?php if (!empty($result) && count($result) > 0) { ?>
        <?php

        if (count($result) % 5 == 0) {
            $pageNumber = round(count($result) / 5);
        } else if(count($result) > 5){
            $pageNumber = round(count($result) / 5) + 1;
        }else{
            $pageNumber = 1;
        }

        $default_wishlists = is_user_logged_in() ? YITH_WCWL()->get_wishlists(array('is_default' => true)) : false;
        if (!empty($default_wishlists)) {
            $default_wishlist = $default_wishlists[0]['ID'];
        } else {
            $default_wishlist = false;
        }
        $pageDetail = [];
        $firstStart = true;
        $end = 0;
        for ($i = 1; $i <= $pageNumber; $i++) {
            $pageDetail[$i]["id"] = "tab" . $i;
            $start = $end + 1;
            if ($firstStart) {
                $start = 0;
                $firstStart = false;
            }
            $end = $i * 5 - 1;
            $end = ($end > 0) ? $end : 0;
            $end = ($end >= count($result) - 1) ? count($result) - 1 : $end;
            for ($pageItem = $start; $start <= $end; $start++) {
                $pageDetail[$i]["data"][] = $result[$start];
            }
        }
        ?>
        <?php
        $firstPage = true;
    foreach ($pageDetail as $pages):
        $stylePage = "display:none";
        if ($firstPage) {
            $stylePage = "display:block";
            $firstPage = false;
        }
        ?>
        <div id="<?= $pages['id']; ?>" class="tab" style="<?= $stylePage; ?>">
            <?php
            if (is_array($pages['data']) && count($pages['data']) > 0) {
                foreach ($pages['data'] as $item) {
                    $product = wc_get_product($item['id']);
                    $avg_rate = WC_Comments::get_average_rating_for_product($product) * 2;
                    $avg_rate = number_format($avg_rate, 1);
                    // exists in default wishlist
                    $exists = YITH_WCWL()->is_product_in_wishlist($item['id'], $default_wishlist);
                    if ($exists) {
                        $action = 'remove_from_wishlist';
                        $la = _x('[:en]Delete from list[:vi]Xóa khỏi danh sách[:]', 'noun');
                        $cl = 'rmtowishlist';
                    } else {
                        $action = 'add_to_wishlist';
                        $la = _x('[:en]Save to after[:vi]Lưu vào để sau[:]', 'noun');
                        $cl = 'addtowishlist';
                    }
                    $cat_parent = get_the_terms($item['id'], 'product_cat');
                    $cat_parent_id = $cat_parent[0]->term_id;
                    $use_book_link = get_field('use_book_link', 'product_cat_' . $cat_parent_id);
                    $book_link = get_field('book_link', 'product_cat_' . $cat_parent_id);
                    $base_url = get_site_url();
                    ?>
                    <article class="list-tourist col-lg-12 col-xs-12">
                        <div class="item-tourist ">
                            <div class="item-content-tourist col-lg-3 col-xs-6">
                                <div class="row">
                                    <div class="image-item-tour-warrap">
                                        <div class="image-item">

                                            <img src="<?= $item['image']; ?>">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-list-price-tourist col-lg-9 col-xs-6">
                                <div class="row">
                                    <div class="col-lg-5 col-xs-12">
                                        <div class="row">
                                            <div class="item-detail-tourist">
                                                <div class="item-name-tourist item-detail-sub">

                                                    <span class="name-tourist"><?= $item['title']; ?></span>

                                                </div>
                                                <div class="item-address item-detail-sub hidden-sm hidden-xs">
                                                    <i class="fa fa-map-marker icon" aria-hidden="true"></i>
                                                    <?php echo get_field('address', $item['id']); ?>
                                                </div>
                                                <div class="item-phone-number item-detail-sub hidden-sm hidden-xs">
                                                    <span class="glyphicon glyphicon-phone-alt icon"></span>
                                                    <?php echo get_field('phone_number', $item['id']); ?>
                                                </div>
                                                <div class="item-email item-detail-sub hidden-sm hidden-xs">
                                                    <i class="fa fa-envelope icon" aria-hidden="true"></i>
                                                    <?php echo get_field('email', $item['id']); ?>
                                                </div>
                                                <div class="item-website item-detail-sub hidden-sm hidden-xs">
                                                    <span class="glyphicon glyphicon-globe icon"></span></i>
                                                    <?php echo get_field('website', $item['id']); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-xs-12">
                                        <div class="row">
                                            <div class="item-detail-price-border">
                                                <div class="item-detail-price-label">Giá</div>
                                                <div class="item-detail-price price-not-special"><?=number_format($item['price'], 0, '.', ','); ?> đ</div>
                                                <div class="review">
                                                    <span class="review-percent"><?=$avg_rate;?></span>
                                                    <?php $comments_count = wp_count_comments($item['id']); ?>
                                                    <span class="review-text">(<b><?php echo (int)$comments_count->total_comments; ?></b> nhận xét)</span>
                                                </div>
                                                <?php
                                                $cat_parent =get_the_terms( $item['id'], 'product_cat' );
                                                $cat_parent_id =  $cat_parent[0]->term_id ;
                                                $use_book_link = get_field('use_book_link','product_cat_'.$cat_parent_id);
                                                $book_link = get_field('book_link','product_cat_'.$cat_parent_id);
                                                $base_url = get_site_url();
                                                ?>
                                                <div class="button-order-service order-service">
                                                    <?php if ($use_book_link): ?>
                                                        <a href="<?php echo $base_url ?>/<?php echo $book_link; ?>/?id=<?php echo $item['id'] ?>"
                                                           title=" <?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?>">
                                                            <div class="text-button"> <?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?></div>
                                                        </a>
                                                    <?php else: ?>

                                                        <a data-toggle="modal" data-target="#order_s_<?php echo $item['id'] ;?>" title="Đặt Xe">
                                                            <div class="text-button">Đặt Xe</div>
                                                        </a>

                                                    <?php endif; ?>
                                                </div>
                                                <?php
                                                $default_wishlists = is_user_logged_in() ? YITH_WCWL()->get_wishlists(array('is_default' => true)) : false;
                                                $exists = YITH_WCWL()->is_product_in_wishlist($item['id'], $default_wishlist);
                                                if ($exists) {
                                                    $action = 'remove_from_wishlist';
                                                    $la = _x('[:en]Delete from list[:vi]Xóa khỏi danh sách[:]', 'noun');
                                                    $cl = 'rmtowishlist';
                                                } else {
                                                    $action = 'add_to_wishlist';
                                                    $la = _x('[:en]Save to after[:vi]Lưu vào để sau[:]', 'noun');
                                                    $cl = 'addtowishlist';
                                                }
                                                ?>
                                                <div class="button-order-service">
                                                    <a class="<?php echo $cl ?>" data-id="<?php echo $item['id'] ?>" data-type="simple"
                                                       data-action="<?php echo $action; ?>">
                                                        <div class="text-button"><?php echo $la; ?></div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="order_s_<?php echo $item['id'] ;?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3 class="modal-title"><?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?></h3>
                                        </div>
                                        <div class="modal-body">
                                            <h4 class="modal-title"><?php echo $item['title'] ?></h4>
                                            <p><?php echo $item['address'] ?></p>
                                            <p><?php echo $item['phone_number'] ?></p>
                                            <p><?php echo $item['email'] ?></p>
                                            <p><?php echo $item['website'] ?></p>
                                            <?php echo do_shortcode('[contact-form-7 id="292" title="Đặt dịch vụ"]')?>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </article>
                    <?php
                }
            }
            ?>
        </div>
    <?php endforeach; ?>
        <div class="product-detail-page entertainment-page col-xs-12 col-lg-12">
            <div class="pagination-border">
                <ul class="page-numbers">
                    <?php
                    $firstPagination = true;
                    $pageNb = 0;
                    if(count($pageDetail) > 1):
                        foreach ($pageDetail as $pagination):
                            $pageNb++;
                            $stylePagination = "display: block;text-decoration: none;padding: 7px 14px;";
                            $classPagintaion = "page-numbers custom-pagination";
                            if ($firstPagination) {
                                $stylePagination = "display: block;text-decoration: underline;padding: 7px 14px;";
                                $classPagintaion = "page-numbers current custom-pagination";
                                $firstPagination = false;
                            }
                            ?>
                            <li><span id="<?= $pagination['id']; ?>" class="<?= $classPagintaion; ?>"
                                      style="<?= $stylePagination ?>"><?= $pageNb; ?></span>
                            </li>
                        <?php endforeach;endif; ?>
                </ul>
            </div>
        </div>
        <script type="text/javascript">
            $("ul.tabs-quickview li").on('click', function () {
                // gỡ bỏ class="active" cho tất cả các thẻ <li>
                $("ul.tabs-quickview li").removeClass("active");
                // chèn class="active" vào phần tử <li> vừa được click
                $(this).addClass("active");
                // ẩn tất cả thẻ <div> với class="tab_content-quickview"
                $(".tab_content-quickview").hide();
                //Hiển thị nội dung thẻ tab được click với hiệu ứng Fade In
                var activeTab = $(this).attr("rel");
                $("." + activeTab).fadeIn();
            });
            $(".custom-pagination").click(function () {
                var value = $(this).attr("id");
                $("html, body").animate({scrollTop: 0}, "slow");
                $(".page-numbers").removeClass("current");
                $(".page-numbers").css('text-decoration', 'none');
                $(this).css('text-decoration', 'underline');
                $(this).addClass("current");
                $(".tab").hide();
                $("#" + value).show();

            });
        </script>
    <?php }else{ ?>
        <div class="message-error">Không tìm thấy thông tin nào</div>
    <?php } ?>
    <?php
    } else {
        ?>
        <div class="message-error">Không tìm thấy sản phẩm nào</div>
        <?php
    }
    ?>
</div>
