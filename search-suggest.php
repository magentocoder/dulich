<?php
if (isset($_POST['suggest'])) {
require('wp-load.php');
wp_reset_query();
$params = array();
$params[] = array(
    'post_type' => 'product',
    'meta_query' => [
        [
            'key' => 'city',
            'value' => $_POST['suggest'],
            'compare' => 'LIKE'
        ],
        [
            'key' => 'provine',
            'value' => $_POST['suggest'],
            'compare' => 'LIKE'
        ]
    ]
);
$params[] = array(
    'post_type' => 'product',
    'meta_query' => [
        [
            'key' => 'city',
            'value' => $_POST['suggest'],
            'compare' => 'LIKE'
        ]
    ]
);
$params[] = array(
    'post_type' => 'product',
    'meta_query' => [
        [
            'key' => 'provine',
            'value' => $_POST['suggest'],
            'compare' => 'LIKE'
        ]
    ]
);
$params[] = array(
    'post_type' => 'product',
    's' => $_POST['suggest']
);
$result = array();
foreach ($params as $key => $value) {
    $wc_query = new WP_Query($value);
    if ($wc_query->have_posts()) {
        $posts = $wc_query->posts;
        foreach ($posts as $post) {
            $id = $post->ID;
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'single-post-thumbnail');
            $imageUrl = $image[0];
            $city = get_field('city', $id);
            $district = get_field('district', $id);
            $commune = get_field('commune', $id);
            $address = get_field('address',$id);
            $phone_number = get_field('phone_number',$id);
            $email = get_field('email',$id);
            $website = get_field('website',$id);
            $price = get_field('_regular_price', $id);
            $title = $post->post_title;
            $content = $post->post_content;
            $productLink = get_permalink($id);
            $result[] = [
                'id' => $post->ID,
                'image' => $imageUrl,
                'city' => $city,
                'district' => $district,
                'commune' => $commune,
                'address' => $address,
                'phone_number' => $phone_number,
                'email' => $email,
                'website' => $website,
                'price' => $price,
                'title' => $title,
                'content' => $content,
                'productlink' => $productLink
            ];
        }
        break;
    }
}
?>
<div id="search-result">
    <?php
    foreach ($result as $item) {
        ?>
        <div class="search-suggest-item">
            <a href="<?= $item['productlink']; ?>"><?= $item['city'].",".$item['district'].",".$item['commune']; ?><a/><br/>
        </div>
        <?php
    }
    ?>
    <?php
    } else {
        ?>
        <div class="message-error">Không có hiển thị nào phù hợp</div>
        <?php
    }
    ?>
</div>
