<?php
/** Sets up the WordPress Environment. */
require( dirname(__FILE__) . '/wp-load.php' );


$comment = wp_handle_comment_submission( wp_unslash( $_POST ) );
if ( is_wp_error( $comment ) ) {
	$data = intval( $comment->get_error_data() );
	if ( ! empty( $data ) ) {
		echo $comment->get_error_message();
	} else {
		exit;
	}
}

$user = wp_get_current_user();

do_action( 'set_comment_cookies', $comment, $user );

