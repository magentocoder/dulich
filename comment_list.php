<?php
/**
 * Created by PhpStorm.
 * User: usau9
 * Date: 5/13/2018
 * Time: 7:10 AM
 */
ini_set('upload_max_filesize', '10M');
require('wp-load.php');
wp_reset_query();
$id = $_POST['id'];
$page = isset($_POST['paged']) ? $_POST['paged'] : 1;
$offset = ($page - 1) * 3;
?>
<?php
$args = array(
    'status' => 'approve',
    'number' => 3,
    'offset' => $offset,
    'post_id' => $id, // use post_id, not post_ID
    'paged' => ($page - 1)
);
$comments = get_comments($args);
$data_re = array();
$data_re['list'] = '';
$comments_count = wp_count_comments($id);
?>
<?php foreach ($comments as $cm): ?>
    <?php
    $rate = ((int)get_comment_meta($cm->comment_ID, 'rating', true) * 2);
    $data_re['list'] .= '<li class="review-comment-item">
 <ul>

            <li class="review-comment-rating">
                <p><span class="review-percent">' . number_format($rate, 1) . '</span>
                </p>
                <p class="name-customer">
                <b>' . (trim($cm->comment_author) == '' ? _x('[:en]Tourists[:vi]Khách du lịch[:]', 'noun') : $cm->comment_author) . '</b>
                </p>
                <p class="review-date"> ' . ($cm->comment_date) . '</p>
            </li>
            <li class="review-comment-description">
                <p>
                    ' . _x($cm->comment_content, 'noun') . '
                </p>
            </li>

        </ul>
    </li>
'; ?>
<?php endforeach; ?>
<?php
$total = ceil($comments_count->total_comments / 3);
$current = $page;
$base = isset($base) ? $base : esc_url_raw(str_replace(999999999, '%#%', remove_query_arg('add-to-cart', get_pagenum_link(999999999, false))));
$format = isset($format) ? $format : '';
$data_re['paginate'] = paginate_links(apply_filters('woocommerce_pagination_args', array( // WPCS: XSS ok.
    'base' => $base,
    'format' => $format,
    'add_args' => false,
    'current' => max(1, $current),
    'total' => $total,
    'show_all' => false,
    'prev_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
    'next_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
    'type' => 'list',
    'end_size' => 2,
    'mid_size' => 1,
)));
//var_dump($data_re);
?>
<?php
echo json_encode($data_re);
?>