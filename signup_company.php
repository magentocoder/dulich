<?php
/**
 * Created by PhpStorm.
 * User: usau9
 * Date: 5/13/2018
 * Time: 7:10 AM
 */
ini_set('upload_max_filesize', '10M');
require('wp-load.php');
require_once(ABSPATH . 'wp-admin/includes/image.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');
require_once(ABSPATH . 'wp-admin/includes/media.php');
wp_reset_query(); ?>
<?php
$home_url = home_url();
?>
<?php
if (!($_POST) || empty($_POST)) {
    wp_redirect($home_url);
}
?>
<?php if ($_POST): ?>
    <?php
    $err = '';
    $base_url = get_site_url();
    if (!is_user_logged_in()) {
        $email = $_POST['thu_dt'];
        $pw = $_POST['mk'];
        $user_id = username_exists($email);
        if (!$user_id and email_exists($email) == false) {
            $user_id = wp_create_user($email, $pw, $email);
        } else {
            $err = __('Email đã đc sử dụng');
        }
    } else {
        $user_id = get_current_user_id();
    }
    if ($err != '') {
        echo $err;
    }else{
        $data = $_POST;
        if (isset($_FILES['logo_cong_ty'])) {
            $time = current_time('mysql');
            $overrides = array('test_form' => false);
            $file = wp_handle_upload($_FILES['logo_cong_ty'], $overrides, $time);
            $filetype = wp_check_filetype(basename($file['file']), null);
            $wp_upload_dir = wp_upload_dir();
            $attachment = array(
                'guid' => $wp_upload_dir['url'] . '/' . basename($file['file']),
                'post_mime_type' => $filetype['type'],
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($file['file'])),
                'post_content' => '',
                'post_status' => 'inherit'
            );
            $attach_id = wp_insert_attachment($attachment, $file['file']);
            $attach_data = wp_generate_attachment_metadata($attach_id, $file['file']);
            wp_update_attachment_metadata($attach_id, $attach_data);
            $data['logo_cong_ty'] = $attach_id;
        }
        foreach ($data as $key => $value) {
            update_user_meta($user_id, $key, $value);
        }
        $theUser = new WP_User($user_id);
        $theUser->add_role('author');
        echo '1';
    }
    ?>
<?php endif; ?>

