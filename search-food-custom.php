<?php
if (!empty($_POST['s'])) {
?>
<?php
require('wp-load.php');
$params = array();
$params[] = array(
    'post_type' => 'product',
    'meta_query' => [
        [
            'key' => 'city',
            'value' => $_POST['s'],
            'compare' => 'LIKE'
        ],
        [
            'key' => 'district',
            'value' => $_POST['s'],
            'compare' => 'LIKE'
        ],
        [
            'key' => 'commune',
            'value' => $_POST['s'],
            'compare' => 'LIKE'
        ],
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'term_id', //This is optional, as it defaults to 'term_id'
                'terms' => 37,
                'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
            )
        )
    ]
);
$params[] = array(
    'post_type' => 'product',
    'meta_query' => [
        [
            'key' => 'city',
            'value' => $_POST['s'],
            'compare' => 'LIKE'
        ],
        [
            'key' => 'district',
            'value' => $_POST['s'],
            'compare' => 'LIKE'
        ],
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'term_id', //This is optional, as it defaults to 'term_id'
                'terms' => 37,
                'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
            )
        )
    ]
);
$params[] = array(
    'post_type' => 'product',
    'meta_query' => [
        [
            'key' => 'city',
            'value' => $_POST['s'],
            'compare' => 'LIKE'
        ]
    ],
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id', //This is optional, as it defaults to 'term_id'
            'terms' => 37,
            'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
        )
    )
);
$params[] = array(
    'post_type' => 'product',
    'meta_query' => [
        [
            'key' => 'district',
            'value' => $_POST['s'],
            'compare' => 'LIKE'
        ]
    ],
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id', //This is optional, as it defaults to 'term_id'
            'terms' => 37,
            'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
        )
    )
);
$params[] = array(
    'post_type' => 'product',
    'meta_query' => [
        [
            'key' => 'commune',
            'value' => $_POST['s'],
            'compare' => 'LIKE'
        ]
    ],
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id', //This is optional, as it defaults to 'term_id'
            'terms' => 37,
            'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
        )
    )
);
$params[] = array(
    'post_type' => 'product',
    's' => $_POST['s'],
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id', //This is optional, as it defaults to 'term_id'
            'terms' => 37,
            'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
        )
    )
);
$result = array();
foreach ($params as $key => $value) {
    $wc_query = new WP_Query($value);
    if ($wc_query->have_posts()) {
        $posts = $wc_query->posts;
        foreach ($posts as $post) {
            $id = $post->ID;
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'single-post-thumbnail');
            $imageUrl = $image[0];
            $city = get_field('city', $id);
            $district = get_field('district', $id);
            $commune = get_field('commune', $id);
            $address = get_field('address', $id);
            $phone_number = get_field('phone_number', $id);
            $email = get_field('email', $id);
            $website = get_field('website', $id);
            $price = get_field('_regular_price', $id);
            $title = $post->post_title;
            $content = $post->post_content;
            $productLink = get_permalink($id);
            $map = get_field('google_map_url', $id);
            $result[] = [
                'id' => $post->ID,
                'image' => $imageUrl,
                'city' => $city,
                'district' => $district,
                'commune' => $commune,
                'address' => $address,
                'phone_number' => $phone_number,
                'email' => $email,
                'website' => $website,
                'price' => $price,
                'title' => $title,
                'content' => $content,
                'productlink' => $productLink,
                'map' => $map
            ];
        }
        break;
    }
}

?>
<div id="search-result">
    <?php if (!empty($result) && count($result) > 0) { ?>
        <?php

        if (count($result) % 5 == 0) {
            $pageNumber = round(count($result) / 5);
        } else if(count($result) > 5){
            $pageNumber = round(count($result) / 5) + 1;
        }else{
            $pageNumber = 1;
        }

        $default_wishlists = is_user_logged_in() ? YITH_WCWL()->get_wishlists(array('is_default' => true)) : false;
        if (!empty($default_wishlists)) {
            $default_wishlist = $default_wishlists[0]['ID'];
        } else {
            $default_wishlist = false;
        }
        $pageDetail = [];
        $firstStart = true;
        $end = 0;
        for ($i = 1; $i <= $pageNumber; $i++) {
            $pageDetail[$i]["id"] = "tab" . $i;
            $start = $end + 1;
            if ($firstStart) {
                $start = 0;
                $firstStart = false;
            }
            $end = $i * 5 - 1;
            $end = ($end > 0) ? $end : 0;
            $end = ($end >= count($result) - 1) ? count($result) - 1 : $end;
            for ($pageItem = $start; $start <= $end; $start++) {
                $pageDetail[$i]["data"][] = $result[$start];
            }
        }
        ?>
        <?php
        $firstPage = true;
    foreach ($pageDetail as $pages):
        $stylePage = "display:none";
        if ($firstPage) {
            $stylePage = "display:block";
            $firstPage = false;
        }
        ?>
        <div id="<?= $pages['id']; ?>" class="tab" style="<?= $stylePage; ?>">
            <?php
            if (is_array($pages['data']) && count($pages['data']) > 0) {
                foreach ($pages['data'] as $item) {
                    $firtMap = true;
                    $product = wc_get_product( $item['id'] );
                    $avg_rate = WC_Comments::get_average_rating_for_product( $product )*2;
                    $avg_rate = number_format($avg_rate,1);
                    // exists in default wishlist
                    $exists = YITH_WCWL()->is_product_in_wishlist($item['id'], $default_wishlist);
                    if ($exists) {
                        $action = 'remove_from_wishlist';
                        $la = _x('[:en]Delete from list[:vi]Xóa khỏi danh sách[:]', 'noun');
                        $cl = 'rmtowishlist';
                    } else {
                        $action = 'add_to_wishlist';
                        $la = _x('[:en]Save to after[:vi]Lưu vào để sau[:]', 'noun');
                        $cl = 'addtowishlist';
                    }
                    $cat_parent =get_the_terms( $item['id'], 'product_cat' );
                    $cat_parent_id =  $cat_parent[0]->term_id ;
                    $use_book_link = get_field('use_book_link','product_cat_'.$cat_parent_id);
                    $book_link = get_field('book_link','product_cat_'.$cat_parent_id);
                    $base_url = get_site_url();
                    ?>
                    <article class="list-tourist col-lg-12 col-xs-12">
                        <?php if($firtMap): ?>
                            <div data-map="<?=$item['map'];?>" class="first-map <?=$pages['id'];?>" style="display: none;">
                            </div>
                        <?php endif; $firtMap = false; ?>
                        <div class="item-tourist ">
                            <div class="item-content-tourist col-lg-3 col-xs-12">
                                <div class="row">
                                    <div class="image-item-tour-warrap">
                                        <div class="image-item">
                                            <?php if ($use_book_link): ?>
                                                <a href="<?php echo $base_url ?>/<?php echo $book_link; ?>/?id=<?php echo $item['id']; ?>"
                                                   title="<?=$item['title']; ?>">
                                                    <img src="<?= $item['image']; ?>">
                                                </a>
                                            <?php else: ?>
                                                <img src="<?= $item['image']; ?>">
                                            <?php endif; ?>
                                        </div>
                                        <div class="wishlist <?php echo $cl ?>" data-id="<?php echo $item['id']; ?>" data-type="simple"
                                             data-action="<?php echo $action; ?>">
                                            <i class="fa fa-heart <?php echo($exists ? 'active' : '') ?>" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-list-price-tourist col-lg-9 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-6 col-xs-12">
                                        <div class="row">
                                            <div class="item-detail-tourist">
                                                <div class="item-name-tourist item-detail-sub">

                                                    <?php if ($use_book_link): ?>
                                                        <a href="<?php echo $base_url ?>/<?php echo $book_link; ?>/?id=<?php echo get_the_ID() ?>"
                                                           title="">
                                                            <?=$item['title']; ?>
                                                        </a>
                                                    <?php else: ?>
                                                        <?=$item['title']; ?>
                                                    <?php endif; ?>
                                                </div>
                                                <?php
                                                $address = get_field('address', $item['id']);
                                                $phone_number = get_field('phone_number', $item['id']);
                                                $email = get_field('email', $item['id']);
                                                $website = get_field('website', $item['id']);
                                                ?>
                                                <?php if (trim($address) != ''): ?>
                                                    <div class="item-address item-detail-sub hidden-sm hidden-xs">
                                                        <i class="fa fa-map-marker icon" aria-hidden="true"></i>
                                                        <?php echo $address ?>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if (trim($phone_number) != ''): ?>
                                                    <div class="item-phone-number item-detail-sub hidden-sm hidden-xs">
                                                        <span class="glyphicon glyphicon-phone-alt icon"></span>
                                                        <?php echo $phone_number ?>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if (trim($email) != ''): ?>
                                                    <div class="item-email item-detail-sub hidden-sm hidden-xs">
                                                        <i class="fa fa-envelope icon" aria-hidden="true"></i>
                                                        <?php echo $email; ?>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if (trim($website) != ''): ?>
                                                    <div class="item-website item-detail-sub hidden-sm hidden-xs">
                                                        <span class="glyphicon glyphicon-globe icon"></span></i>
                                                        <?php echo $website ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xs-12">
                                        <div class="row">
                                            <div class="item-detail-price-border">
                                                <div class="item-detail-price-label"><?php _e('[:en]Price[:vi]Giá[:]') ?></div>
                                                <div class="price-special-border">
                                                    <div class="item-detail-price price-not-special">
                                                        <?php echo number_format($item['price'],0,'.',',')?> đ</div>
                                                </div>
                                                <div class="review">
                                                    <span class="review-percent"><?=$avg_rate;?></span>
                                                    <?php $comments_count = wp_count_comments($item['id']); ?>
                                                    <span class="review-text">(<b><?php echo (int)$comments_count->total_comments; ?></b> <?php _e('[:en]comment[:vi]nhận xét[:]') ?>
                                                        )</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-xs-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-xs-6">
                                                <div class="row">
                                                    <div class="button-order-service order-service">
                                                        <?php if ($use_book_link): ?>
                                                            <a href="<?php echo $base_url ?>/<?php echo $book_link; ?>/?id=<?php echo get_the_ID() ?>"
                                                               title=" <?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?>">
                                                                <div class="text-button"> <?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?></div>
                                                            </a>
                                                        <?php else: ?>
                                                            <a data-toggle="modal" data-target="#order_<?=$item['id']; ?>"
                                                               title=" <?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?>">
                                                                <div class="text-button"> <?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?></div>
                                                            </a>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-xs-6">
                                                <div class="row">
                                                    <div class="button-order-service">
                                                        <div class="text-button" onclick="showQuickView(this.id)"
                                                             id="quickview-<?=$item['id'];?>">
                                                            <?php _e('[:en]Read more[:vi]Xem thêm[:]'); ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="quickview col-lg-12 col-xs-12" data-index="quickview-<?=$item['id']?>">
                                <div class="row">
                                    <div class="container-tab-quickview">

                                        <ul class="tabs-quickview">
                                            <li class="" rel="tab1"><?php _e('[:en]Image[:vi]Hình ảnh[:]') ?></li>
                                            <li class="" rel="tab2"><?php _e('[:en]Details[:vi]Thông tin chi tiết[:]') ?></li>
                                            <li rel="tab3"><?php _e('[:en]Map[:vi]Bản đồ[:]') ?></li>
                                            <li rel="tab4" class="comment_tab_product"
                                                id="<?=$item['id'];?>"
                                                data-url="<?php echo $current_url?>"
                                            ><?php _e('[:en]Comment[:vi]Nhận xét[:]') ?></li>
                                        </ul>
                                        <div class="tab_container-quickview">
                                            <div class="tab1 tab_content-quickview">

                                                <div class="show-image">
                                                    <?php
                                                    $i = 0;
                                                    $attachment_ids = $product->get_gallery_attachment_ids(); ?>

                                                    <div class="carousel slide" data-ride="carousel">
                                                        <div class="carousel-inner" role="listbox">
                                                            <div class="item <?php echo $active; ?>">
                                                                <?php woocommerce_template_loop_product_thumbnail(); ?>
                                                            </div>
                                                            <?php if (!empty($attachment_ids)): ?>
                                                                <?php foreach ($attachment_ids as $attachment_id): ?>
                                                                    <?php
                                                                    $image_link = wp_get_attachment_url($attachment_id);
                                                                    $active = (($i == 0 ? ' active' : ''));
                                                                    $i++;
                                                                    ?>
                                                                    <div class="item <?php echo $active; ?>">
                                                                        <img src="<?php echo $image_link; ?>" alt="" title="#">
                                                                    </div>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                        <?php if (!empty($attachment_ids)): ?>
                                                            <a class="right carousel-control"
                                                               href="#carousel-simple-<?=$item['id']; ?>" role="button"
                                                               data-slide="next">
                                                                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                                            </a>
                                                        <?php endif; ?>
                                                    </div>

                                                </div>


                                            </div><!-- #tab1 -->
                                            <div class="tab2 tab_content-quickview">
                                                <div class="information-company">
                                                    <?php echo get_field('thong_tin_dai_ly', $item['id']); ?>
                                                </div>

                                            </div><!-- #tab2 -->

                                            <div class="tab3 tab_content-quickview">
                                                <div class="place-map">
                                                    <div class="map-place">
                                                        <?php if (trim(get_field('google_map_url', $item['id'])) != ''): ?>
                                                            <iframe src="<?php echo get_field('google_map_url', $item['id']); ?>"
                                                                    width="100%" height="300px" frameborder="0" style="border:0"
                                                                    allowfullscreen></iframe>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="info-hotel-wappar">
                                                        <?php $thong_tin = get_field('thong_tin_gioi_thieu_ve_vi_tri', $item['id']); ?>
                                                        <?php if (trim($thong_tin) != ''): ?>
                                                            <h4 class="info-hotel"><?php _e('[:en]Hotel Information[:vi]Thông tin khách sạn[:]') ?></h4>
                                                            <div class="info-hotel-content">
                                                                <ul>
                                                                    <?php
                                                                    $thong_tin = explode(PHP_EOL, $thong_tin);
                                                                    $size = ceil(count($thong_tin) / 4);
                                                                    $thong_tin = array_chunk($thong_tin, $size);
                                                                    ?>
                                                                    <?php foreach ($thong_tin as $tins): ?>
                                                                        <li class="info-hotel-columns">
                                                                            <ul class="">
                                                                                <?php foreach ($tins as $tin): ?>
                                                                                    <li class=""><a href="#"
                                                                                                    title="#"><?php _e($tin) ?></a>
                                                                                    </li>                                                        </li>
                                                                                <?php endforeach; ?>
                                                                            </ul>
                                                                        </li>
                                                                    <?php endforeach; ?>
                                                                </ul>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div><!-- #tab3 -->

                                            <div class="tab4 tab_content-quickview">
                                                <div class="review-comment-border">
                                                    <h4 class="review-conmment-label"><?php _e('[:en]Customer Reviews[:vi]Nhận xét của khách hàng[:]') ?></h4>
                                                    <ul class="review-comment-content" id="comment_content_<?php echo $item['id'];?>">
                                                        <?php
                                                        $args = array(
                                                            'status' => 'approve',
                                                            'number' => '3',
                                                            'post_id' => $item['id'], // use post_id, not post_ID
                                                        );
                                                        $comments = get_comments($args);
                                                        ?>
                                                        <?php foreach ($comments as $cm): ?>
                                                            <li class="review-comment-item">
                                                                <ul>

                                                                    <li class="review-comment-rating">
                                                                        <p>
                                                                            <?php $rate = ((int)get_comment_meta($cm->comment_ID, 'rating', true) * 2); ?>
                                                                            <span class="review-percent"><?php echo number_format($rate, 1); ?></span>
                                                                        </p>
                                                                        <p class="name-customer">
                                                                            <b><?php echo(trim($cm->comment_author) == '' ? _x('[:en]Tourists[:vi]Khách du lịch[:]', 'noun') : $cm->comment_author) ?></b>
                                                                        </p>
                                                                        <p class="review-date"> <?php echo($cm->comment_date) ?></p>
                                                                    </li>
                                                                    <li class="review-comment-description">
                                                                        <p>
                                                                            <?php _e($cm->comment_content) ?>
                                                                        </p>
                                                                    </li>

                                                                </ul>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                    <div class="pagination-border">
                                                        <ul class="pagination">
                                                            <?php
                                                            $total = ceil($comments_count->total_comments/3);
                                                            $current = '';
                                                            $base = isset($base) ? $base : esc_url_raw(str_replace(999999999, '%#%', remove_query_arg('add-to-cart', get_pagenum_link(999999999, false))));
                                                            $format = isset($format) ? $format : '';
                                                            echo paginate_links(apply_filters('woocommerce_pagination_args', array( // WPCS: XSS ok.
                                                                'base' => $base,
                                                                'format' => $format,
                                                                'add_args' => false,
                                                                'current' => max(1, 1),
                                                                'total' => $total,
                                                                'show_all'           => false,
                                                                'prev_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                                                                'next_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                                                                'type' => 'list',
                                                                'end_size' => 2,
                                                                'mid_size' => 1,
                                                            )));
                                                            ?>
                                                        </ul>
                                                    </div>
                                                    <div class="comment_contect_product" id="comment_contect_product_<?=$item['id']; ?>">

                                                    </div>

                                                </div>
                                            </div><!-- #tab4 -->

                                        </div> <!-- .tab_container -->
                                        <div class="button-close"
                                             onclick="closeQuickView(this)"><?php _e('[:en]Close[:vi]Đóng[:]') ?></div>
                                    </div>
                                </div>
                            </div>
                            <div id="order_<?=$item['id']; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3 class="modal-title"><?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?></h3>
                                        </div>
                                        <div class="modal-body">
                                            <h4 class="modal-title"><?=$item['title'];?></h4>
                                            <p><?php echo get_field('address', $item['id']); ?></p>
                                            <p><?php echo get_field('phone_number', $item['id']); ?></p>
                                            <p><?php echo get_field('email', $item['id']); ?></p>
                                            <p><?php echo get_field('website', $item['id']); ?></p>
                                            <?php echo do_shortcode('[contact-form-7 id="292" title="Đặt dịch vụ"]') ?>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default"
                                                    data-dismiss="modal"><?php _e('[:en]Close[:vi]Đóng[:]') ?></button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <?php if ($item['has_sale']): ?>
                            <div class="sales-percent">-<?php echo (int)(100 - ($item['sale_prie'] * 100 / $item['price'])) ?>%</div>
                        <?php endif; ?>
                    </article>
                    <?php
                }
            }
            ?>
        </div>
    <?php endforeach; ?>
        <div class="product-detail-page entertainment-page col-xs-12 col-lg-12">
            <div class="pagination-border">
                <ul class="page-numbers">
                    <?php
                    $firstPagination = true;
                    $pageNb = 0;
                    if(count($pageDetail) > 1):
                        foreach ($pageDetail as $pagination):
                            $pageNb++;
                            $stylePagination = "display: block;text-decoration: none;padding: 7px 14px;";
                            $classPagintaion = "page-numbers custom-pagination";
                            if ($firstPagination) {
                                $stylePagination = "display: block;text-decoration: underline;padding: 7px 14px;";
                                $classPagintaion = "page-numbers current custom-pagination";
                                $firstPagination = false;
                            }
                            ?>
                            <li><span id="<?= $pagination['id']; ?>" class="<?= $classPagintaion; ?>"
                                      style="<?= $stylePagination ?>"><?= $pageNb; ?></span>
                            </li>
                        <?php endforeach;endif; ?>
                </ul>
            </div>
        </div>
        <script type="text/javascript">
            $("ul.tabs-quickview li").on('click', function () {
                // gỡ bỏ class="active" cho tất cả các thẻ <li>
                $("ul.tabs-quickview li").removeClass("active");
                // chèn class="active" vào phần tử <li> vừa được click
                $(this).addClass("active");
                // ẩn tất cả thẻ <div> với class="tab_content-quickview"
                $(".tab_content-quickview").hide();
                //Hiển thị nội dung thẻ tab được click với hiệu ứng Fade In
                var activeTab = $(this).attr("rel");
                $("." + activeTab).fadeIn();
            });
            $(".custom-pagination").click(function () {
                var value = $(this).attr("id");
                $("html, body").animate({scrollTop: 0}, "slow");
                $(".page-numbers").removeClass("current");
                $(".page-numbers").css('text-decoration', 'none');
                $(this).css('text-decoration', 'underline');
                $(this).addClass("current");
                $(".tab").hide();
                $("#" + value).show();
            });
            var firstMap = $(".first-map.tab1").data("map");
            if(firstMap.length > 0) {
                $(".sidebar.hidden-sm.hidden-xs").show();
                $($(".mapp-wappar").find(".map-place")).find("iframe").attr("src", firstMap);
            } else {
                $(".sidebar.hidden-sm.hidden-xs").hide();
            }
        </script>
    <?php }else{ ?>
        <div class="message-error">Không tìm thấy thông tin nào</div>
    <?php } ?>
    <?php
    } else {
        ?>
        <div class="message-error">Không tìm thấy sản phẩm nào</div>
        <?php
    }
    ?>
</div>
