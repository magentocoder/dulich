<?php
if (isset($_POST)) {
require('wp-load.php');
wp_reset_query();
$city = isset($_POST['city']) ? $_POST['city'] : null;
$district = isset($_POST['district']) ? $_POST['district'] : null;
$commune = isset($_POST['commune']) ? $_POST['commune'] : null;
$time_start = !empty($_POST['time_start']) ? $_POST['time_start'] : null;
$time_end = !empty($_POST['time_end']) ? $_POST['time_end'] : null;
$kieu_thue = isset($_POST['kieu_thue']) ? $_POST['kieu_thue'] : null;
$dong_xe = isset($_POST['dong_xe']) ? $_POST['dong_xe'] : null;
$loai_xe = isset($_POST['loai_xe']) ? $_POST['loai_xe'] : null;
$xep_hang_xe = isset($_POST['xep_hang_xe']) ? $_POST['xep_hang_xe'] : null;
$nha_cung_cap = isset($_POST['nha_cung_cap']) ? $_POST['nha_cung_cap'] : null;

if ($city == 'Thành Phố') {
    $city = null;
}
if ($district == 'Quận - Huyện') {
    $district = null;
}
if ($commune == 'Phường - Xã') {
    $commune = null;
}

if ($commune == 'Phường - Xã') {
    $commune = null;
}

if ($kieu_thue == 'Chọn kiểu thuê xe') {
    $kieu_thue = null;
}

if ($dong_xe == 'Chọn dòng xe') {
    $dong_xe = null;
}

if ($loai_xe == 'Chọn loại xe') {
    $loai_xe = null;
}

if ($xep_hang_xe == 'Chọn theo xếp hạng') {
    $xep_hang_xe = null;
}

$search = [];
if(!empty($city)){
    $search[]= [
        'key' => 'city',
        'value' => $city,
        'compare' => '='
    ];
}

if(!empty($district)){
    $search[]= [
        'key' => 'district',
        'value' => $district,
        'compare' => '='
    ];
}

if(!empty($commune)){
    $search[]= [
        'key' => 'commune',
        'value' => $commune,
        'compare' => '='
    ];
}

if(!empty($time_start)){
    $search[]= [
        'key' => 'time_start',
        'value' => $time_start,
        'compare' => '>='
    ];
}
if(!empty($time_end)){
    $search[]= [
        'key' => 'time_end',
        'value' => $time_end,
        'compare' => '<='
    ];
}

if(!empty($kieu_thue)){
    $search[]= [
        'key' => 'kieu_thue',
        'value' => $kieu_thue,
        'compare' => '='
    ];
}

if(!empty($dong_xe)){
    $search[]= [
        'key' => 'dong_xe',
        'value' => $dong_xe,
        'compare' => '='
    ];
}

if(!empty($loai_xe)){
    $search[]= [
        'key' => 'loai_xe',
        'value' => $loai_xe,
        'compare' => '='
    ];
}

if(!empty($nha_cung_cap)){
    $search[]= [
        'key' => 'nha_cung_cap',
        'value' => $nha_cung_cap,
        'compare' => 'LIKE'
    ];
}
$finalSearch = [
    'post_type' => 'product',
    'meta_query' => [
        [
            $search
        ]
    ],
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id', //This is optional, as it defaults to 'term_id'
            'terms' => 19,
            'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
        )
    )
];
$result = array();
$wc_query = new WP_Query($finalSearch);
if ($wc_query->have_posts()) {
    $posts = $wc_query->posts;
    foreach ($posts as $post) {
        $id = $post->ID;
        $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'single-post-thumbnail');
        $imageUrl = $image[0];
        $city = get_field('city', $id);
        $district = get_field('district', $id);
        $commune = get_field('commune', $id);
        $address = get_field('address', $id);
        $phone_number = get_field('phone_number', $id);
        $email = get_field('email', $id);
        $website = get_field('website', $id);
        $price = get_field('_regular_price', $id);
        $title = $post->post_title;
        $content = $post->post_content;
        $productLink = get_permalink($id);
        $map = get_field('google_map_url', $id);
        $result[] = [
            'id' => $post->ID,
            'image' => $imageUrl,
            'city' => $city,
            'district' => $district,
            'commune' => $commune,
            'address' => $address,
            'phone_number' => $phone_number,
            'email' => $email,
            'website' => $website,
            'price' => $price,
            'title' => $title,
            'content' => $content,
            'productlink' => $productLink,
            'map' => $map
        ];
    }
}

?>
<div id="search-transport-result">
        <?php if (!empty($result) && count($result) > 0) { ?>
            <?php

            if (count($result) % 5 == 0) {
                $pageNumber = round(count($result) / 5);
            } else if(count($result) > 5){
                $pageNumber = round(count($result) / 5) + 1;
            }else{
                $pageNumber = 1;
            }

            $default_wishlists = is_user_logged_in() ? YITH_WCWL()->get_wishlists(array('is_default' => true)) : false;
            if (!empty($default_wishlists)) {
                $default_wishlist = $default_wishlists[0]['ID'];
            } else {
                $default_wishlist = false;
            }
            $pageDetail = [];
            $firstStart = true;
            $end = 0;
            for ($i = 1; $i <= $pageNumber; $i++) {
                $pageDetail[$i]["id"] = "tab" . $i;
                $start = $end + 1;
                if ($firstStart) {
                    $start = 0;
                    $firstStart = false;
                }
                $end = $i * 5 - 1;
                $end = ($end > 0) ? $end : 0;
                $end = ($end >= count($result) - 1) ? count($result) - 1 : $end;
                for ($pageItem = $start; $start <= $end; $start++) {
                    $pageDetail[$i]["data"][] = $result[$start];
                }
            }
            ?>
            <?php
            $firstPage = true;
        foreach ($pageDetail as $pages):
            $stylePage = "display:none";
            if ($firstPage) {
                $stylePage = "display:block";
                $firstPage = false;
            }
            ?>
            <div id="<?= $pages['id']; ?>" class="tab" style="<?= $stylePage; ?>">
                <?php
                if (is_array($pages['data']) && count($pages['data']) > 0) {
                    foreach ($pages['data'] as $item) {
                        $product = wc_get_product($item['id']);
                        $avg_rate = WC_Comments::get_average_rating_for_product($product) * 2;
                        $avg_rate = number_format($avg_rate, 1);
                        // exists in default wishlist
                        $exists = YITH_WCWL()->is_product_in_wishlist($item['id'], $default_wishlist);
                        if ($exists) {
                            $action = 'remove_from_wishlist';
                            $la = _x('[:en]Delete from list[:vi]Xóa khỏi danh sách[:]', 'noun');
                            $cl = 'rmtowishlist';
                        } else {
                            $action = 'add_to_wishlist';
                            $la = _x('[:en]Save to after[:vi]Lưu vào để sau[:]', 'noun');
                            $cl = 'addtowishlist';
                        }
                        $cat_parent = get_the_terms($item['id'], 'product_cat');
                        $cat_parent_id = $cat_parent[0]->term_id;
                        $use_book_link = get_field('use_book_link', 'product_cat_' . $cat_parent_id);
                        $book_link = get_field('book_link', 'product_cat_' . $cat_parent_id);
                        $base_url = get_site_url();
                        ?>
                        <article class="list-tourist col-lg-12 col-xs-12">
                            <div class="item-tourist ">
                                <div class="item-content-tourist col-lg-3 col-xs-6">
                                    <div class="row">
                                        <div class="image-item-tour-warrap">
                                            <div class="image-item">

                                                <img src="<?= $item['image']; ?>">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-list-price-tourist col-lg-9 col-xs-6">
                                    <div class="row">
                                        <div class="col-lg-5 col-xs-12">
                                            <div class="row">
                                                <div class="item-detail-tourist">
                                                    <div class="item-name-tourist item-detail-sub">

                                                        <span class="name-tourist"><?= $item['title']; ?></span>

                                                    </div>
                                                    <div class="item-address item-detail-sub hidden-sm hidden-xs">
                                                        <i class="fa fa-map-marker icon" aria-hidden="true"></i>
                                                        <?php echo get_field('address', $item['id']); ?>
                                                    </div>
                                                    <div class="item-phone-number item-detail-sub hidden-sm hidden-xs">
                                                        <span class="glyphicon glyphicon-phone-alt icon"></span>
                                                        <?php echo get_field('phone_number', $item['id']); ?>
                                                    </div>
                                                    <div class="item-email item-detail-sub hidden-sm hidden-xs">
                                                        <i class="fa fa-envelope icon" aria-hidden="true"></i>
                                                        <?php echo get_field('email', $item['id']); ?>
                                                    </div>
                                                    <div class="item-website item-detail-sub hidden-sm hidden-xs">
                                                        <span class="glyphicon glyphicon-globe icon"></span></i>
                                                        <?php echo get_field('website', $item['id']); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-xs-12">
                                            <div class="row">
                                                <div class="item-detail-price-border">
                                                    <div class="item-detail-price-label">Giá</div>
                                                    <div class="item-detail-price price-not-special"><?=number_format($item['price'], 0, '.', ','); ?> đ</div>
                                                    <div class="review">
                                                        <span class="review-percent"><?=$avg_rate;?></span>
                                                        <?php $comments_count = wp_count_comments($item['id']); ?>
                                                        <span class="review-text">(<b><?php echo (int)$comments_count->total_comments; ?></b> nhận xét)</span>
                                                    </div>
                                                    <?php
                                                    $cat_parent =get_the_terms( $item['id'], 'product_cat' );
                                                    $cat_parent_id =  $cat_parent[0]->term_id ;
                                                    $use_book_link = get_field('use_book_link','product_cat_'.$cat_parent_id);
                                                    $book_link = get_field('book_link','product_cat_'.$cat_parent_id);
                                                    $base_url = get_site_url();
                                                    ?>
                                                    <div class="button-order-service order-service">
                                                        <?php if ($use_book_link): ?>
                                                            <a href="<?php echo $base_url ?>/<?php echo $book_link; ?>/?id=<?php echo $item['id'] ?>"
                                                               title=" <?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?>">
                                                                <div class="text-button"> <?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?></div>
                                                            </a>
                                                        <?php else: ?>

                                                            <a data-toggle="modal" data-target="#order_s_<?php echo $item['id'] ;?>" title="Đặt Xe">
                                                                <div class="text-button">Đặt Xe</div>
                                                            </a>

                                                        <?php endif; ?>
                                                    </div>
                                                    <?php
                                                    $default_wishlists = is_user_logged_in() ? YITH_WCWL()->get_wishlists(array('is_default' => true)) : false;
                                                    $exists = YITH_WCWL()->is_product_in_wishlist($item['id'], $default_wishlist);
                                                    if ($exists) {
                                                        $action = 'remove_from_wishlist';
                                                        $la = _x('[:en]Delete from list[:vi]Xóa khỏi danh sách[:]', 'noun');
                                                        $cl = 'rmtowishlist';
                                                    } else {
                                                        $action = 'add_to_wishlist';
                                                        $la = _x('[:en]Save to after[:vi]Lưu vào để sau[:]', 'noun');
                                                        $cl = 'addtowishlist';
                                                    }
                                                    ?>
                                                    <div class="button-order-service">
                                                        <a class="<?php echo $cl ?>" data-id="<?php echo $item['id'] ?>" data-type="simple"
                                                           data-action="<?php echo $action; ?>">
                                                            <div class="text-button"><?php echo $la; ?></div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="order_s_<?php echo $item['id'] ;?>" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h3 class="modal-title"><?php _e('[:en]Order[:vi]Đặt Dịch Vụ[:]'); ?></h3>
                                            </div>
                                            <div class="modal-body">
                                                <h4 class="modal-title"><?php echo $item['title'] ?></h4>
                                                <p><?php echo $item['address'] ?></p>
                                                <p><?php echo $item['phone_number'] ?></p>
                                                <p><?php echo $item['email'] ?></p>
                                                <p><?php echo $item['website'] ?></p>
                                                <?php echo do_shortcode('[contact-form-7 id="292" title="Đặt dịch vụ"]')?>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </article>
                        <?php
                    }
                }
                ?>
            </div>
        <?php endforeach; ?>
            <div class="product-detail-page entertainment-page col-xs-12 col-lg-12">
                <div class="pagination-border">
                    <ul class="page-numbers">
                        <?php
                        $firstPagination = true;
                        $pageNb = 0;
                        if(count($pageDetail) > 1):
                            foreach ($pageDetail as $pagination):
                                $pageNb++;
                                $stylePagination = "display: block;text-decoration: none;padding: 7px 14px;";
                                $classPagintaion = "page-numbers custom-pagination";
                                if ($firstPagination) {
                                    $stylePagination = "display: block;text-decoration: underline;padding: 7px 14px;";
                                    $classPagintaion = "page-numbers current custom-pagination";
                                    $firstPagination = false;
                                }
                                ?>
                                <li><span id="<?= $pagination['id']; ?>" class="<?= $classPagintaion; ?>"
                                          style="<?= $stylePagination ?>"><?= $pageNb; ?></span>
                                </li>
                            <?php endforeach;endif; ?>
                    </ul>
                </div>
            </div>
            <script type="text/javascript">
                $("ul.tabs-quickview li").on('click', function () {
                    // gỡ bỏ class="active" cho tất cả các thẻ <li>
                    $("ul.tabs-quickview li").removeClass("active");
                    // chèn class="active" vào phần tử <li> vừa được click
                    $(this).addClass("active");
                    // ẩn tất cả thẻ <div> với class="tab_content-quickview"
                    $(".tab_content-quickview").hide();
                    //Hiển thị nội dung thẻ tab được click với hiệu ứng Fade In
                    var activeTab = $(this).attr("rel");
                    $("." + activeTab).fadeIn();
                });
                $(".custom-pagination").click(function () {
                    var value = $(this).attr("id");
                    $("html, body").animate({scrollTop: 0}, "slow");
                    $(".page-numbers").removeClass("current");
                    $(".page-numbers").css('text-decoration', 'none');
                    $(this).css('text-decoration', 'underline');
                    $(this).addClass("current");
                    $(".tab").hide();
                    $("#" + value).show();

                });
            </script>
        <?php }else{ ?>
            <div class="message-error">Không tìm thấy thông tin nào</div>
        <?php } ?>
        <?php
        } else {
            ?>
            <div class="message-error">Không tìm thấy sản phẩm nào</div>
            <?php
        }
        ?>
    </div>
