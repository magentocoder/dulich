<?php
/**
 * Created by PhpStorm.
 * User: usau9
 * Date: 5/13/2018
 * Time: 7:10 AM
 */
ini_set('upload_max_filesize', '10M');
require('wp-load.php');
wp_reset_query();
$id = $_POST['id'];
$url = $_POST['url'];
$comments_args = array(
    // Change the title of send button
    'label_submit' => __('Send', 'textdomain'),
    // Remove "Text or HTML to be displayed after the set of comment fields".
    'comment_notes_after' => '',
    // Redefine your own textarea (the comment body).
    'comment_field' => '
                        <div class="comment-form-rating hotel_star"><label for="rating">' . _x('[:en]Your review[:vi]Đánh giá của bạn[:]', 'noun') . '</label>
                            <p class="stars"><span>
                            <a class="star-1" href="#">1</a>
                            <a class="star-2" href="#">2</a>
                            <a class="star-3" href="#">3</a>
                            <a class="star-4" href="#">4</a>
                            <a class="star-5" href="#">5</a>
                        </span>
                        </p>
                        <select name="rating" id="rating' . $id . '" aria-required="true" required="" style="display: none;">
                            <option value="">' . _x('[:en]Rank ...[:vi]Xếp hạng…[:]', 'noun') . '</option>
                            <option value="5">5</option>
                            <option value="4">4</option>
                            <option value="3">3</option>
                            <option value="2">2</option>
                            <option value="1">1</option>
                        </select></div>
                            <p class="comment-form-comment">
                                <label for="comment">' . _x('[:en]Comment[:vi]Nhận xét[:]', 'noun') . '</label><br />
                                <textarea id="comment' . $id . '" name="comment" aria-required="true"></textarea>
                            </p>
                            <input type="hidden" name="my_redirect_to" id="my_redirect_to' . $id . '" value="' . $url . '">',
    'submit_button' => '<input name="%1$s" type="submit" id="%2$s' . $id . '" class="%3$s" value="' . _x('[:en]Send[:vi]Gửi[:]', 'noun') . '" />',
    'id_form' => 'commentform' . $id
);
comment_form($comments_args, $id);
?>
