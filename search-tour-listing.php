<?php
if (isset($_POST)) {
require('wp-load.php');
wp_reset_query();
$city = isset($_POST['city']) ? $_POST['city'] : null;
$district = isset($_POST['district']) ? $_POST['district'] : null;
$commune = isset($_POST['commune']) ? $_POST['commune'] : null;
$ten = isset($_POST['ten']) ? $_POST['ten'] : null;
$price = isset($_POST['price']) ? $_POST['price'] : null;
$rate = isset($_POST['rate']) ? $_POST['rate'] : null;
$rank = isset($_POST['rank']) ? $_POST['rank'] : null;
if ($city == 'Thành Phố') {
    $city = null;
}
if ($district == 'Quận - Huyện') {
    $district = null;
}
if ($commune == 'Phường - Xã') {
    $commune = null;
}

$searh = [];

if (!empty($city)) {
    $search[] = [
        'key' => 'city',
        'value' => $city,
        'compare' => '='
    ];
}

if (!empty($district)) {
    $search[] = [
        'key' => 'district',
        'value' => $district,
        'compare' => '='
    ];
}

if (!empty($commune)) {
    $search[] = [
        'key' => 'commune',
        'value' => $commune,
        'compare' => '='
    ];
}

if (!empty($price) && $price != 'Giá tiền') {
    if ($price == '0 đ - 1.000.000 đ') {
        $search[] = [
            'key' => '_regular_price',
            'value' => array(1, 1000000),
            'type' => 'numeric',
            'compare' => 'BETWEEN'
        ];
    } else if ($price == '1.000.000 đ - 2.000.000 đ') {
        $search[] = [
            'key' => '_regular_price',
            'value' => array(1000000, 2000000),
            'type' => 'numeric',
            'compare' => 'BETWEEN'
        ];
    } else if ($price == '2.000.000 đ - 3.000.000 đ') {
        $search[] = [
            'key' => '_regular_price',
            'value' => array(2000000, 3000000),
            'type' => 'numeric',
            'compare' => 'BETWEEN'
        ];
    } else if ($price == '3.000.000 đ - 4.000.000 đ') {
        $search[] = [
            'key' => '_regular_price',
            'value' => array(3000000, 4000000),
            'type' => 'numeric',
            'compare' => 'BETWEEN'
        ];
    } else if ($price == '4.000.000 đ trở lên') {
        $search[] = [
            'key' => '_regular_price',
            'value' => 4000000,
            'compare' => '>='
        ];
    }
}

if (!empty($rate) && $rate != 'Đánh giá') {
    $search[] = [
        'key' => 'rate',
        'value' => $rate,
        'compare' => '='
    ];
}
if (!empty($rank) && $rank != 'Hạng') {
    $search[] = [
        'key' => 'rank',
        'value' => $rank,
        'compare' => '='
    ];
}

$finalQuery = [
    'post_type' => 'product',
    'meta_query' => [
        $search
    ],
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id', //This is optional, as it defaults to 'term_id'
            'terms' => 40,
            'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
        )
    )
];

$result = array();
$wc_query = new WP_Query($finalQuery);
if ($wc_query->have_posts()) {
    $posts = $wc_query->posts;
    foreach ($posts as $post) {
        if(!empty($ten) && $ten != ""){
            if(stripos($post->post_title,$ten) !== false){
                $id = $post->ID;
                $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'single-post-thumbnail');
                $imageUrl = $image[0];
                $city = get_field('city', $id);
                $district = get_field('district', $id);
                $commune = get_field('commune', $id);
                $address = get_field('address', $id);
                $phone_number = get_field('phone_number', $id);
                $email = get_field('email', $id);
                $website = get_field('website', $id);
                $price = get_field('_regular_price', $id);
                $title = $post->post_title;
                $content = $post->post_content;
                $productLink = get_permalink($id);
                $map = get_field('google_map_url', $id);
                $result[] = [
                    'id' => $post->ID,
                    'image' => $imageUrl,
                    'city' => $city,
                    'district' => $district,
                    'commune' => $commune,
                    'address' => $address,
                    'phone_number' => $phone_number,
                    'email' => $email,
                    'website' => $website,
                    'price' => $price,
                    'title' => $title,
                    'content' => $content,
                    'productlink' => $productLink,
                    'map' => $map
                ];
            }
        }else{
            $id = $post->ID;
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'single-post-thumbnail');
            $imageUrl = $image[0];
            $city = get_field('city', $id);
            $district = get_field('district', $id);
            $commune = get_field('commune', $id);
            $address = get_field('address', $id);
            $phone_number = get_field('phone_number', $id);
            $email = get_field('email', $id);
            $website = get_field('website', $id);
            $price = get_field('_regular_price', $id);
            $title = $post->post_title;
            $content = $post->post_content;
            $productLink = get_permalink($id);
            $map = get_field('google_map_url', $id);
            $result[] = [
                'id' => $post->ID,
                'image' => $imageUrl,
                'city' => $city,
                'district' => $district,
                'commune' => $commune,
                'address' => $address,
                'phone_number' => $phone_number,
                'email' => $email,
                'website' => $website,
                'price' => $price,
                'title' => $title,
                'content' => $content,
                'productlink' => $productLink,
                'map' => $map
            ];
        }
    }
}

?>
<div id="search-result">
    <?php if (!empty($result) && count($result) > 0) { ?>
        <?php

        if (count($result) % 5 == 0) {
            $pageNumber = round(count($result) / 5);
        } else if(count($result) > 5){
            $pageNumber = round(count($result) / 5) + 1;
        }else{
            $pageNumber = 1;
        }

        $default_wishlists = is_user_logged_in() ? YITH_WCWL()->get_wishlists(array('is_default' => true)) : false;
        if (!empty($default_wishlists)) {
            $default_wishlist = $default_wishlists[0]['ID'];
        } else {
            $default_wishlist = false;
        }
        $pageDetail = [];
        $firstStart = true;
        $end = 0;
        for ($i = 1; $i <= $pageNumber; $i++) {
            $pageDetail[$i]["id"] = "tab" . $i;
            $start = $end + 1;
            if ($firstStart) {
                $start = 0;
                $firstStart = false;
            }
            $end = $i * 5 - 1;
            $end = ($end > 0) ? $end : 0;
            $end = ($end >= count($result) - 1) ? count($result) - 1 : $end;
            for ($pageItem = $start; $start <= $end; $start++) {
                $pageDetail[$i]["data"][] = $result[$start];
            }
        }
        ?>
        <?php
        $firstPage = true;
    foreach ($pageDetail as $pages):
        $stylePage = "display:none";
        if ($firstPage) {
            $stylePage = "display:block";
            $firstPage = false;
        }
        ?>
        <div id="<?= $pages['id']; ?>" class="tab" style="<?= $stylePage; ?>">
            <?php
            if (is_array($pages['data']) && count($pages['data']) > 0) {
                foreach ($pages['data'] as $item) {
                    $firtMap = true;
                    $product = wc_get_product($item['id']);
                    $avg_rate = WC_Comments::get_average_rating_for_product($product) * 2;
                    $avg_rate = number_format($avg_rate, 1);
                    // exists in default wishlist
                    $exists = YITH_WCWL()->is_product_in_wishlist($item['id'], $default_wishlist);
                    if ($exists) {
                        $action = 'remove_from_wishlist';
                        $la = _x('[:en]Delete from list[:vi]Xóa khỏi danh sách[:]', 'noun');
                        $cl = 'rmtowishlist';
                    } else {
                        $action = 'add_to_wishlist';
                        $la = _x('[:en]Save to after[:vi]Lưu vào để sau[:]', 'noun');
                        $cl = 'addtowishlist';
                    }
                    $cat_parent = get_the_terms($item['id'], 'product_cat');
                    $cat_parent_id = $cat_parent[0]->term_id;
                    $use_book_link = get_field('use_book_link', 'product_cat_' . $cat_parent_id);
                    $book_link = get_field('book_link', 'product_cat_' . $cat_parent_id);
                    $base_url = get_site_url();
                    ?>
                    <div class="border-list-item-tourist entertainment-page-content news-page">
                        <?php if($firtMap): ?>
                            <div data-map="<?=$item['map'];?>" class="first-map <?=$pages['id'];?>" style="display: none;">
                            </div>
                        <?php endif; $firtMap = false; ?>
                        <article class="entertainment-main col-lg-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="row">
                                    <div class="entertainment-image">
                                        <a href="<?=$item['productlink']; ?>"
                                           title="<?=$item['title']; ?>">
                                            <img src="<?=$item['image']; ?>" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div class="row">
                                    <div class="entertainment-content">
                                        <div class="entertainment-content-label">
                                            <a href="<?=$item['productlink'];?>" title="<?=$item['title'];?>">
                                                <?=$item['title'];?>
                                            </a>
                                            <div class="short-description">
                                <span class="short-information">
                                    <?=$dd_th; ?>
                                </span>
                                                <span class="review-icon">
                                    <?php if ($starEx):
                                        for ($i = 0; $i < $starEx; $i++) {
                                            ?>
                                            <i class="fas fa-star"></i>
                                        <?php }
                                    endif;
                                    ?>
                                </span>
                                            </div>
                                        </div>
                                        <div class="entertainment-content-detail">
                                            <div class="col-lg-9 col-xs-12">
                                                <div class="row">
                                                    <div class="entertainment-short-description">
                                                        <?=substr($item['content'],0,200); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-xs-12">
                                                <div class="row">
                                                    <div class="price-tour"><?php echo number_format($price,0,'.',',')?> đ</div>
                                                    <div class="button-order-tour">
                                                        <?php
                                                        $cat_parent =get_the_terms( $item['id'], 'product_cat' );
                                                        $cat_parent_id =  $cat_parent[0]->term_id ;
                                                        $book_link = get_field('book_link','product_cat_'.$cat_parent_id);
                                                        $base_url = get_site_url();
                                                        ?>
                                                        <a type="submit"
                                                           href="<?php echo $base_url ?>/<?php echo $book_link; ?>/?id=<?php echo $item['id'] ?>"
                                                           class="btn btn-default" name="tour-book" value="<?php echo $item['id'] ?>">Đặt
                                                            tour</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    <?php endforeach; ?>
        <div class="product-detail-page entertainment-page col-xs-12 col-lg-12 ">
            <div class="pagination-border">
                <ul class="page-numbers">
                    <?php
                    $firstPagination = true;
                    $pageNb = 0;
                    if(count($pageDetail) > 1):
                        foreach ($pageDetail as $pagination):
                            $pageNb++;
                            $stylePagination = "display: block;text-decoration: none;padding: 7px 14px;";
                            $classPagintaion = "page-numbers custom-pagination";
                            if ($firstPagination) {
                                $stylePagination = "display: block;text-decoration: underline;padding: 7px 14px;";
                                $classPagintaion = "page-numbers current custom-pagination";
                                $firstPagination = false;
                            }
                            ?>
                            <li><span id="<?= $pagination['id']; ?>" class="<?= $classPagintaion; ?>"
                                      style="<?= $stylePagination ?>"><?= $pageNb; ?></span>
                            </li>
                        <?php endforeach;endif; ?>
                </ul>
            </div>
        </div>
        <script type="text/javascript">
            $("ul.tabs-quickview li").on('click', function () {
                // gỡ bỏ class="active" cho tất cả các thẻ <li>
                $("ul.tabs-quickview li").removeClass("active");
                // chèn class="active" vào phần tử <li> vừa được click
                $(this).addClass("active");
                // ẩn tất cả thẻ <div> với class="tab_content-quickview"
                $(".tab_content-quickview").hide();
                //Hiển thị nội dung thẻ tab được click với hiệu ứng Fade In
                var activeTab = $(this).attr("rel");
                $("." + activeTab).fadeIn();
            });
            $(".custom-pagination").click(function () {
                var value = $(this).attr("id");
                $("html, body").animate({scrollTop: 0}, "slow");
                $(".page-numbers").removeClass("current");
                $(".page-numbers").css('text-decoration', 'none');
                $(this).css('text-decoration', 'underline');
                $(this).addClass("current");
                $(".tab").hide();
                $("#" + value).show();
            });
            var firstMap = $(".first-map.tab1").data("map");
            if(firstMap.length > 0) {
                $(".sidebar.hidden-sm.hidden-xs").show();
                $($(".mapp-wappar").find(".map-place")).find("iframe").attr("src", firstMap);
            } else {
                $(".sidebar.hidden-sm.hidden-xs").hide();
            }
        </script>
    <?php }else{ ?>
        <div class="message-error">Không tìm thấy thông tin nào</div>
    <?php } ?>
    <?php
    } else {
        ?>
        <div class="message-error">Không tìm thấy sản phẩm nào</div>
        <?php
    }
    ?>
</div>